package com.java.floatcare.controller;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.joda.time.DateTime;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.context.ApplicationContext;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.java.floatcare.api.response.pojo.RestResponse;
import com.java.floatcare.api.response.pojo.ShiftPlanningDraftPojo;
import com.java.floatcare.api.response.pojo.ShiftPlanningLayout;
import com.java.floatcare.api.response.pojo.StaffGroups;
import com.java.floatcare.api.response.pojo.StaffLayout;
import com.java.floatcare.dao.AvailabilityDAO;
import com.java.floatcare.dao.CountersDAO;
import com.java.floatcare.dao.ScheduleDAO;
import com.java.floatcare.dao.UserWorkspacesDAO;
import com.java.floatcare.model.Department;
import com.java.floatcare.model.DepartmentSettings;
import com.java.floatcare.model.DepartmentShifts;
import com.java.floatcare.model.ScheduleLayout;
import com.java.floatcare.model.ScheduleLayoutGroups;
import com.java.floatcare.model.Schedules;
import com.java.floatcare.model.ShiftPlanningDrafts;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.model.UserNotification;
import com.java.floatcare.model.UserWorkspaces;
import com.java.floatcare.repository.BusinessesRepository;
import com.java.floatcare.repository.DepartmentRepository;
import com.java.floatcare.repository.DepartmentSettingsRepository;
import com.java.floatcare.repository.DepartmentShiftsRepository;
import com.java.floatcare.repository.ScheduleLayoutGroupsRepository;
import com.java.floatcare.repository.ScheduleLayoutRepository;
import com.java.floatcare.repository.SchedulesRepository;
import com.java.floatcare.repository.ShiftPlanningDraftsRepository;
import com.java.floatcare.repository.UserBasicInformationRepository;
import com.java.floatcare.repository.UserNotificationRepository;
import com.java.floatcare.repository.UserWorkspacesRepository;
import com.java.floatcare.utils.ConstantUtils;
import com.java.floatcare.utils.EmailThread;
import com.java.floatcare.utils.GoogleFirebaseUtilityDAO;
import com.java.floatcare.utils.InAppNotifications;
import com.java.floatcare.utils.SMSThread;
import com.java.floatcare.utils.StringUtils;
import com.java.floatcare.utils.ZonedDateTimeWriteConverter;
import com.twilio.exception.ApiException;

@RestController
@RequestMapping("/shift")
public class ShiftPlanningController {

	@Autowired
	private DepartmentSettingsRepository departmentSettingsRepository;
	@Autowired
	private ScheduleLayoutRepository scheduleLayoutRepository;
	@Autowired
	private DepartmentShiftsRepository departmentShiftsRepository;
	@Autowired
	private AvailabilityDAO availabilityDAO;
	@Autowired
	private UserWorkspacesDAO userWorkspacesDAO;
	@Autowired
	private ScheduleLayoutGroupsRepository scheduleLayoutGroupsRepository;
	@Autowired
	private UserBasicInformationRepository userBasicInformationRepository;
	@Autowired
	private ScheduleDAO scheduleDAO;
	@Autowired
	private CountersDAO sequence;
	@Autowired
	private UserWorkspacesRepository userWorkspacesRepository;
	@Autowired
	private ShiftPlanningDraftsRepository shiftPlanningDraftsRepository;
	@Autowired
	private SchedulesRepository schedulesRepository;
	@Autowired
	private DepartmentRepository departmentRepository;
	@Autowired
	private BusinessesRepository businessesRepository;
	@Autowired
	private InAppNotifications inAppNotifications;
	@Autowired
	private UserNotificationRepository userNotificationRepository;
	@Autowired
	private GoogleFirebaseUtilityDAO googleFirebaseUtilityDAO;
	@Autowired
	private ApplicationContext applicationContext;

	@RequestMapping(value = "/planning/{departmentId}/{scheduleLayoutId}", method = RequestMethod.GET)
	public ResponseEntity<Object> shiftPlanning(@PathVariable long departmentId, @PathVariable long scheduleLayoutId)
			throws Exception, JSONException {

		List<ShiftPlanningDraftPojo> listObj = new ArrayList<ShiftPlanningDraftPojo>();

		Optional<ShiftPlanningDrafts> existingShiftPlanning = shiftPlanningDraftsRepository
				.findByScheduleLayoutId(scheduleLayoutId);

		if (existingShiftPlanning.isPresent()) {

			String existing = existingShiftPlanning.get().getShiftPlanningDraftPojo();

			RestResponse restResponse = new RestResponse();

			restResponse.setStatus(200);
			restResponse.setSuccess(true);
			restResponse.setMessage("");
			restResponse.setData(existing);
			return ResponseEntity.ok(restResponse);
		} else {

			DepartmentSettings departmentSettings = departmentSettingsRepository.findByDepartmentId(departmentId);

			Optional<ScheduleLayout> scheduleLayout = scheduleLayoutRepository.findById(scheduleLayoutId);

			if (departmentSettings != null && scheduleLayout.isPresent()) {

				DateTime startDate = scheduleLayout.get().getScheduleStartDate(); // fetch start date
				DateTime endDate = scheduleLayout.get().getScheduleEndDate(); // fetch end date
				LocalDate localStartDate = Instant.ofEpochMilli(startDate.getMillis()).atZone(ZoneId.systemDefault())
						.toLocalDate();
				LocalDate localEndDate = Instant.ofEpochMilli(endDate.getMillis()).atZone(ZoneId.systemDefault())
						.toLocalDate();
				// String payPeriod = scheduleLayout.get().getScheduleLength();
				// String payPeriod = departmentSettings.getWorkPayPeriod(); // fetch the
				// payPeriod from departmentSettings

				String[] output = scheduleLayout.get().getScheduleLength().split("(?<=\\d)(?=\\D)"); // dividing Number and Character[ eg. 10W => 10W]
				int numOfDaysPayPeriod = 1;
				if (output[1].equalsIgnoreCase("m")) {

					numOfDaysPayPeriod = startDate.dayOfMonth().getMaximumValue();

				} else {
					numOfDaysPayPeriod = 7; // calculate number of days between payPeriod
				}
				// Days days = Days.daysBetween(startDate, endDate);

				// int scheduleDuration = startDate.dayOfMonth().getMaximumValue();

				int iterations = Integer.parseInt(output[0]);// scheduleDuration / numOfDaysPayPeriod;

				List<ScheduleLayoutGroups> scheduleLayoutGroups = scheduleLayoutGroupsRepository
						.findByScheduleLayoutId(scheduleLayoutId);

				List<Long> usersList = new ArrayList<>();

				for (ScheduleLayoutGroups eachUser : scheduleLayoutGroups) {
					usersList
							.addAll(userWorkspacesDAO.findByUserIdAndDepartmentId(eachUser.getUserIds(), departmentId)); // returns
																															// list
																															// of
																															// active
																															// users
																															// in
																															// department
				}

				Iterable<UserBasicInformation> userBasicInformationList = userBasicInformationRepository
						.findAllById(usersList);

				List<String> scheduleList = scheduleDAO.findUserScheduleByWorkspaceIdsUserIdsListAndOnDate(departmentId,
						usersList, localStartDate, localEndDate);

				List<String> usersAvailabilityList = availabilityDAO.findAvailableUserByShiftDateAndScheduleLayoutId(
						usersList, localStartDate, localEndDate, scheduleLayoutId); // returns of available users

				if (iterations >= 1) {

					for (int k = 1; k <= iterations; k++) {

						String pattern = "MM-dd-yyyy";
						// SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

						// String startDateFormat = simpleDateFormat.format(startDate.toDate());
						// String endDateFormat = simpleDateFormat
						// .format(startDate.plusDays(numOfDaysPayPeriod - 1).toDate());
						// String each = startDateFormat + " to " + endDateFormat;
						ShiftPlanningDraftPojo eachPayPeriod = new ShiftPlanningDraftPojo();

						if (output[1].equalsIgnoreCase("m")) {
							eachPayPeriod.setPeriod(startDate.monthOfYear().getAsShortText()+"-"+startDate.year().getAsShortText());
							
						} else {
							eachPayPeriod.setPeriod("Week " + k +"("+startDate.monthOfYear().getAsShortText()+"-"+startDate.year().getAsShortText()+")");
						}

						// the staff members groups by department id

						for (ScheduleLayoutGroups eachGroup : scheduleLayoutGroups) {
							// set the group information into response pojo
							StaffGroups staffGroups = new StaffGroups();

							staffGroups.setGroupName(eachGroup.getName());
							// Create the staff layout for staff members in each group
							List<StaffLayout> staffLayout = new ArrayList<StaffLayout>();

							for (UserBasicInformation eachUser : userBasicInformationList) {

								StaffLayout eachUserLayout = new StaffLayout();

								// fetch the userBasicInformation object for each user based on userId

								int totalSlots = 0;
								int assignedShifts = 0;

								List<ShiftPlanningLayout> shiftPlanningLayout = new ArrayList<ShiftPlanningLayout>();
								// calculate the user shiftPlanning layout object based on pay period

								List<Long> departmentShiftId = new ArrayList<Long>();
								List<DepartmentShifts> shiftObjs = new ArrayList<DepartmentShifts>();
								UserWorkspaces userWorkspace = userWorkspacesRepository
										.findByUserIdAndWorkspaceIdAndStatusAndIsJoined(eachUser.getUserId(),
												departmentId, "Active", true);

								if (userWorkspace != null) {
									departmentShiftId = userWorkspace.getDepartmentShiftIds();
									eachUser.setFteStatus(userWorkspace.getFteStatus());
									if(departmentShiftId!=null) {
									shiftObjs = (List<DepartmentShifts>) departmentShiftsRepository
											.findAllById(departmentShiftId);
									}

								} else
									eachUser.setFteStatus("not joined yet");

								

								for (int j = 0; j < numOfDaysPayPeriod; j++) {

									ShiftPlanningLayout shiftPlanningLayoutObj = new ShiftPlanningLayout();

									DateTime nextDate = startDate.plusDays(j);

									shiftPlanningLayoutObj.setShiftDate(nextDate);
									// fetch all info based on shiftDate
									// check his or she's availability on the shiftDate

									LocalDate shiftDate = Instant.ofEpochMilli(nextDate.getMillis())
											.atZone(ZoneId.systemDefault()).toLocalDate();

									if (scheduleList.contains(departmentId + " " + eachUser.getUserId() + " "
											+ shiftDate + " " + "Active")) { // check in schedules collection by passing
																				// userId and shiftDate
										shiftPlanningLayoutObj.setAssigned(true);
										assignedShifts++;
									} else
										shiftPlanningLayoutObj.setAssigned(false);

									if (usersAvailabilityList.contains(eachUser.getUserId() + " " + shiftDate)) { // check
																													// in
																													// availability
																													// collection
																													// by
																													// passing
																													// userId
																													// and
																													// shift
										shiftPlanningLayoutObj.setAvailable(true);
										totalSlots++;
									} else
										shiftPlanningLayoutObj.setAvailable(false);
									


									shiftPlanningLayoutObj.setDepartmentShifts(shiftObjs);
									shiftPlanningLayoutObj.setUserId(eachUser.getUserId());

									shiftPlanningLayout.add(shiftPlanningLayoutObj);
								}

								eachUser.setFirstName(StringUtils.toCamelCase(eachUser.getFirstName()));
								eachUser.setTotalSlots(totalSlots);
								eachUser.setAssignedSlots(assignedShifts);

								eachUserLayout.setUserBasicInformation(eachUser);
								eachUserLayout.setShiftPlanningLayout(shiftPlanningLayout);
								staffLayout.add(eachUserLayout);
							}
							staffGroups.setStaffLayout(staffLayout);
							eachPayPeriod.setStaffGroups(staffGroups);
						}

						listObj.add(eachPayPeriod);

						startDate = startDate.plusDays(numOfDaysPayPeriod);
						if (output[1].equalsIgnoreCase("m")) {

							numOfDaysPayPeriod = startDate.dayOfMonth().getMaximumValue();

						}
						
					}
				}

				RestResponse restResponse = new RestResponse();
				restResponse.setStatus(200);
				restResponse.setSuccess(true);
				restResponse.setMessage("");
				restResponse.setData(listObj);
				return ResponseEntity.ok(restResponse);
			} else {
				RestResponse restResponse = new RestResponse();
				restResponse.setStatus(400);
				restResponse.setSuccess(false);
				restResponse.setMessage("Data not available.");
				restResponse.setData(null);
				return ResponseEntity.ok(restResponse);
			}
		}
	}

	@RequestMapping(value = "/planning/save/{scheduleLayoutId}", method = RequestMethod.POST, consumes = {
			"application/json" })
	public ResponseEntity<Object> saveShiftPlanning(@RequestBody String listObj,
			@PathVariable("scheduleLayoutId") long scheduleLayoutId) throws Exception, JSONException {

		Optional<ShiftPlanningDrafts> shiftPlanningDrafts = shiftPlanningDraftsRepository
				.findByScheduleLayoutId(scheduleLayoutId);

		if (shiftPlanningDrafts.isPresent()) {

			ShiftPlanningDrafts shiftPlanningDraftsObj = shiftPlanningDrafts.get();
			shiftPlanningDraftsObj.setShiftPlanningDraftPojo(listObj);
			shiftPlanningDraftsRepository.save(shiftPlanningDraftsObj);

			RestResponse restResponse = new RestResponse();
			restResponse.setStatus(200);
			restResponse.setSuccess(true);
			restResponse.setMessage("");
			restResponse.setData(shiftPlanningDrafts);
			return ResponseEntity.ok(restResponse);

		} else {
			ShiftPlanningDrafts shiftPlanningDraftsNew = new ShiftPlanningDrafts();

			shiftPlanningDraftsNew.setShiftPlanningDraftId(sequence.getNextSequenceValue("shiftPlanningDraftId"));
			shiftPlanningDraftsNew.setShiftPlanningDraftPojo(listObj);
			shiftPlanningDraftsNew.setScheduleLayoutId(scheduleLayoutId);

			shiftPlanningDraftsRepository.save(shiftPlanningDraftsNew);

			RestResponse restResponse = new RestResponse();
			restResponse.setStatus(200);
			restResponse.setSuccess(true);
			restResponse.setMessage("");
			restResponse.setData(shiftPlanningDraftsNew);
			return ResponseEntity.ok(restResponse);
		}
	}

	@RequestMapping(value = "/publish/{departmentId}/{scheduleLayoutId}", method = RequestMethod.GET)
	public ResponseEntity<Object> publishShiftPlanning(@PathVariable long departmentId,
			@PathVariable long scheduleLayoutId) throws Exception, JSONException {

		Optional<ShiftPlanningDrafts> shiftPlanningDrafts = shiftPlanningDraftsRepository
				.findByScheduleLayoutIdAndStatusNot(scheduleLayoutId, "Published");
		ZonedDateTimeWriteConverter zonedTimeWriteConverter = new ZonedDateTimeWriteConverter(); // fetches the exact
																									// date according to
																									// zone
		long worksiteId = 0;
		TreeSet<Long> userIds = new TreeSet<Long>();
		if (shiftPlanningDrafts.isPresent()) {

			shiftPlanningDrafts.get().setStatus("Published");
			shiftPlanningDraftsRepository.save(shiftPlanningDrafts.get());

			Optional<Department> department = departmentRepository.findById(departmentId);
			if (department.isPresent()) {
				worksiteId = department.get().getBusinessId();
			}
			JSONParser parser = new JSONParser();
			JSONArray jsonArray = (JSONArray) parser.parse(shiftPlanningDrafts.get().getShiftPlanningDraftPojo());

			for (Object eachObject : jsonArray) {
				JSONObject jsonObj = (JSONObject) eachObject;
				JSONArray staffGroups = (JSONArray) jsonObj.get("staffGroups");
				for (Object eachGroupL : staffGroups) {
					JSONObject jsonObjL = (JSONObject) eachGroupL;
					JSONArray staffLayout = (JSONArray) jsonObjL.get("staffLayout");
					for (Object eachObjectS : staffLayout) {
						JSONObject jsonObjS = (JSONObject) eachObjectS;
						JSONArray shiftPlanningLayout = (JSONArray) jsonObjS.get("shiftPlanningLayout");

						for (Object eachShiftO : shiftPlanningLayout) {

							JSONObject jsonObjF = (JSONObject) eachShiftO;

							userIds.add((Long) jsonObjF.get("userId"));

							if ((boolean) jsonObjF.get("assigned")) {

								Schedules schedules = new Schedules();

								schedules.setScheduleId(sequence.getNextSequenceValue("scheduleId"));
								schedules.setDepartmentId(departmentId);
								schedules.setStartTime(java.time.LocalTime.parse(jsonObjF.get("startTime").toString()));
								schedules.setEndTime(java.time.LocalTime.parse(jsonObjF.get("endTime").toString()));
								JSONObject shiftDateObj = (JSONObject) jsonObjF.get("shiftDate");
								String monthOfYear = "";
								String dayOfMonth = "";

								if ((long) shiftDateObj.get("monthOfYear") <= 9)
									monthOfYear = "0" + (long) shiftDateObj.get("monthOfYear");
								else
									monthOfYear = "" + (long) shiftDateObj.get("monthOfYear");

								if ((long) shiftDateObj.get("dayOfMonth") <= 9)
									dayOfMonth = "0" + (long) shiftDateObj.get("dayOfMonth");
								else
									dayOfMonth = "" + (long) shiftDateObj.get("dayOfMonth");

								String shiftDateStr = (long) shiftDateObj.get("year") + "-" + monthOfYear + "-"
										+ dayOfMonth;

								LocalDate localDate = LocalDate.parse(shiftDateStr);

								schedules.setShiftDate(localDate);
								schedules.setWorksiteId(worksiteId);
								schedules.setUserId((long) jsonObjF.get("userId"));
								schedules.setOnCallRequest(false);
								schedules.setEventType("Regular");
								schedules.setStatus("Active");
								schedules.setShiftTypeId((long) jsonObjF.get("departmentShiftId"));
								schedules.setSelfScheduleId(scheduleLayoutId);

								if (scheduleDAO.findScheduleByUserIdAndShiftDateAndDepartmentId(
										(long) jsonObjF.get("userId"), localDate, departmentId) == null)
									schedulesRepository.save(schedules);
							}
						}
					}
				}
			}
			// Triggering the required notification as your schedule published
			for (Long userId : userIds) {

				UserBasicInformation userDetail = userBasicInformationRepository.findUsersByUserId(userId);

				String departmentName = "";
				String businessName = "";

				try {
					departmentName = departmentRepository.findById(departmentId).get().getDepartmentTypeName();
					businessName = businessesRepository.findById(worksiteId).get().getName();
				} catch (Exception e) {
					departmentName = "";
					businessName = "";
				}

				String message = "New schedule is live";
				String messageBody = "The schedule for " + departmentName + " @ " + businessName
						+ " has been published.";
				String receiverFcmToken = userDetail.getFcmToken();
				String deepLink = "7"; // deep link for redirecting user to schedule/calendar screen
				// Google Push notification

				if (receiverFcmToken != null)
					googleFirebaseUtilityDAO.sendEventNotificationToUser(userId, receiverFcmToken, message, messageBody,
							deepLink, userDetail.getDeviceType(), null); // send Push Notification
				// InApp notification
				long notificationTypeId = 6;
				inAppNotifications.deleteBySourceId(Arrays.asList(scheduleLayoutId));
				inAppNotifications.createInAppNotifications(userId, notificationTypeId, message, messageBody, 7, false,
						scheduleLayoutId); // send InApp Notification

				// Fetch the user notifications based on settings
				UserNotification userNotificationSetting = userNotificationRepository
						.findByNotificationTypeIdAndUserId(6, userId);

				messageBody = messageBody
						+ ". Click on the link below to see the changes in the Floatcare Application. \n"
						+ ConstantUtils.branchIOMySchedule;

				if (userNotificationSetting != null) {
					if (userNotificationSetting.isEnableEmail() == true) {
						try {
							EmailThread emailThread = new EmailThread();
							emailThread.setEmails(userDetail.getEmail());
							emailThread.setDynamicName(userDetail.getFirstName());
							emailThread.setMessage(message);
							emailThread.setMessageBody(messageBody);
							if (departmentName.length() > 1)
								emailThread.setDepartmentName(departmentName);
							if (businessName.length() > 1)
								emailThread.setBusinessName(businessName);
							emailThread.setTemplate(ConstantUtils.approvedSchedule);
							emailThread.setTemplate(true);

							applicationContext.getAutowireCapableBeanFactory().autowireBean(emailThread);

							Thread emailthread = new Thread(emailThread); // send email on separate thread.

							emailthread.start(); // start the email thread
						} catch (ApiException apiException) {
							continue;
						}
					}
					if (userNotificationSetting.isEnableSMS() == true) {
						try {
							SMSThread smsThread = new SMSThread();
							smsThread.setMessageBody(messageBody);
							smsThread.setPhone(userDetail.getPhone());

							applicationContext.getAutowireCapableBeanFactory().autowireBean(smsThread);

							Thread smsthread = new Thread(smsThread);
							smsthread.start(); // start the sms thread
						} catch (Exception e) {

						}
					}
				}
			}

			Optional<ScheduleLayout> scheduleLayoutOptional = scheduleLayoutRepository.findById(scheduleLayoutId);

			if (scheduleLayoutOptional.isPresent()) {
				ScheduleLayout scheduleLayout = scheduleLayoutOptional.get();

				LocalDate publishedDate = zonedTimeWriteConverter.convert(ZonedDateTime.now());
				scheduleLayout.setPublishedOn(publishedDate);
				scheduleLayout.setStatus("Published");

				scheduleLayoutRepository.save(scheduleLayout);

			}

			RestResponse restResponse = new RestResponse();

			restResponse.setStatus(200);
			restResponse.setSuccess(true);
			restResponse.setMessage("Fully Published");
			restResponse.setData("");
			return ResponseEntity.ok(restResponse);
		} else {
			RestResponse restResponse = new RestResponse();

			restResponse.setStatus(500);
			restResponse.setSuccess(true);
			restResponse.setMessage("Something went wrong !");
			restResponse.setData("");
			return ResponseEntity.ok(restResponse);
		}
	}
}