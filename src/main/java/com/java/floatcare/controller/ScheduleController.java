package com.java.floatcare.controller;

import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneId;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.java.floatcare.api.request.pojo.RequestScheduleDuration;
import com.java.floatcare.api.request.pojo.RequestSchedulesByDate;
import com.java.floatcare.api.request.pojo.RequestSchedulesByMonth;
import com.java.floatcare.api.request.pojo.RequestUserSchedule;
import com.java.floatcare.api.response.pojo.RestResponse;
import com.java.floatcare.api.response.pojo.ScheduleMonthLayout;
import com.java.floatcare.api.response.pojo.ScheduleMonthResponse;
import com.java.floatcare.api.response.pojo.ScheduleResponse;
import com.java.floatcare.api.response.pojo.SchedulesByDateResponse;
import com.java.floatcare.api.response.pojo.ShiftPlanningLayout;
import com.java.floatcare.api.response.pojo.ShiftwiseSchedules;
import com.java.floatcare.api.response.pojo.StaffLayout;
import com.java.floatcare.api.response.pojo.UserBasicResponse;
import com.java.floatcare.api.response.pojo.UserScheduleResponse;
import com.java.floatcare.api.response.pojo.UserStaffLayout;
import com.java.floatcare.api.response.pojo.WorkingStaffCountByCoreType;
import com.java.floatcare.dao.AssignmentInviteesDAO;
import com.java.floatcare.dao.AssignmentSchedulesDAO;
import com.java.floatcare.dao.AssignmentsDAO;
import com.java.floatcare.dao.AvailabilityDAO;
import com.java.floatcare.dao.OpenShiftInviteesDAO;
import com.java.floatcare.dao.OpenShiftsDAO;
import com.java.floatcare.dao.RequestsDAO;
import com.java.floatcare.dao.ScheduleDAO;
import com.java.floatcare.dao.ScheduleLayoutDAO;
import com.java.floatcare.dao.ScheduleLayoutGroupsDAO;
import com.java.floatcare.dao.SetScheduleLayoutDAO;
import com.java.floatcare.dao.StaffWeekOffsDAO;
import com.java.floatcare.dao.UserBasicInformationRepositoryDAO;
import com.java.floatcare.dao.UserOrganizationDAO;
import com.java.floatcare.dao.UserWorkspacesDAO;
import com.java.floatcare.dao.WorksiteSettingsDAO;
import com.java.floatcare.model.AssignmentInvitees;
import com.java.floatcare.model.AssignmentSchedules;
import com.java.floatcare.model.Assignments;
import com.java.floatcare.model.Availability;
import com.java.floatcare.model.DepartmentShifts;
import com.java.floatcare.model.OpenShiftInvitees;
import com.java.floatcare.model.OpenShifts;
import com.java.floatcare.model.ScheduleLayout;
import com.java.floatcare.model.ScheduleLayoutGroups;
import com.java.floatcare.model.Schedules;
import com.java.floatcare.model.SetScheduleLayouts;
import com.java.floatcare.model.StaffUserCoreTypes;
import com.java.floatcare.model.StaffWeekOffs;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.model.UserOrganizations;
import com.java.floatcare.model.UserWorkspaces;
import com.java.floatcare.model.WorksiteSettings;
import com.java.floatcare.repository.BusinessesRepository;
import com.java.floatcare.repository.DepartmentShiftsRepository;
import com.java.floatcare.repository.OrganizationRepository;
import com.java.floatcare.repository.StaffUserCoreTypesRepository;
import com.java.floatcare.repository.UserWorkspacesRepository;
import com.java.floatcare.utils.ConstantUtils;

@RestController
@RequestMapping("/schedule")
public class ScheduleController {

	@Autowired
	private UserWorkspacesRepository userWorkspacesRepository;
	@Autowired
	private AvailabilityDAO availabilityDAO;
	@Autowired
	private ScheduleDAO scheduleDAO;
	@Autowired
	private DepartmentShiftsRepository departmentShiftsRepository;
	@Autowired
	private RequestsDAO requestsDAO;
	@Autowired
	private OpenShiftsDAO openShiftDAO;
	@Autowired
	private UserWorkspacesDAO userWorkspacesDAO;
	@Autowired
	private UserBasicInformationRepositoryDAO userRepository;
	@Autowired
	private UserBasicInformationRepositoryDAO userBasicInformationDAO;
	@Autowired
	private OpenShiftInviteesDAO openShiftInviteesDAO;
	@Autowired
	private OrganizationRepository organizationRepository;
	@Autowired
	private BusinessesRepository businessesRepository;
	@Autowired
	private WorksiteSettingsDAO worksiteSettingsDAO;
	@Autowired
	private StaffWeekOffsDAO staffWeekOffsDAO;
	@Autowired
	private SetScheduleLayoutDAO setScheduleLayoutDAO;
	@Autowired
	private AssignmentsDAO assignmentDAO;
	@Autowired
	private AssignmentSchedulesDAO assignmentSchedulesDAO;
	@Autowired
	private AssignmentInviteesDAO assignmentInviteesDAO;
	@Autowired
	private UserOrganizationDAO userOrganizationDAO;
	@Autowired
	private StaffUserCoreTypesRepository staffUserCoreTypesRepository;
	@Autowired
	private ScheduleLayoutDAO scheduleLayoutDAO;
	@Autowired
	private ScheduleLayoutGroupsDAO scheduleLayoutGroupsDAO;

	@RequestMapping(value = "/duration", method = RequestMethod.POST)
	public ResponseEntity<Object> getSchedulesByDuration(@RequestBody RequestScheduleDuration requestScheduleDuration) {

		DateTime startDate = DateTime.parse(requestScheduleDuration.getStartDate());
		DateTime endDate = DateTime.parse(requestScheduleDuration.getEndDate());
		Days days = Days.daysBetween(startDate, endDate);
		int duration = days.getDays(); // calculate the duration between startDate and endDate
		DateFormatSymbols dateFormatSymbol = new DateFormatSymbols();
		String[] months = dateFormatSymbol.getMonths(); // fetches the month name
		LocalDate localStartDate = Instant.ofEpochMilli(startDate.getMillis()).atZone(ZoneId.systemDefault())
				.toLocalDate();
		LocalDate localEndDate = Instant.ofEpochMilli(endDate.getMillis()).atZone(ZoneId.systemDefault()).toLocalDate();

		ScheduleResponse scheduleResponse = new ScheduleResponse();

		scheduleResponse
				.setPeriod(requestScheduleDuration.getStartDate() + " - " + requestScheduleDuration.getEndDate());
		scheduleResponse.setYear("" + startDate.getYear());
		scheduleResponse.setWeek("" + startDate.getWeekOfWeekyear());
		scheduleResponse.setMonth(months[startDate.getMonthOfYear() - 1]);

		Map<Long, Long> userWorkspaceList = userWorkspacesDAO
				.findByDepartmentId(requestScheduleDuration.getDepartmentId()); // fetch users information through
																				// departmentId from userWorkspace
																				// collection

		List<Long> userIds = userWorkspaceList.keySet().stream().collect(Collectors.toList());

		List<UserBasicInformation> users = userBasicInformationDAO.findByUserId(userIds);

		List<String> userRequest = requestsDAO.findRequestsByUserIdAndOnDate(userIds, localStartDate, localEndDate);

//		List<String> userAvailabilityList = availabilityDAO.findAvailableUserByShiftDate(userIds, localStartDate, localEndDate);

		List<String> scheduleList = scheduleDAO.findUserScheduleByWorkspaceIdsUserIdsListAndOnDate(
				requestScheduleDuration.getDepartmentId(), userIds, localStartDate, localEndDate);

		List<StaffLayout> staffLayout = new ArrayList<StaffLayout>(); // Create the staff layout for staff members in
																		// each group

		List<SetScheduleLayouts> setScheduleLayouts = setScheduleLayoutDAO
				.findByDepartmentId(requestScheduleDuration.getDepartmentId());
		List<Long> setScheduleRoles = setScheduleLayouts.stream()
				.flatMap(setSchedule -> setSchedule.getRoles().stream()).distinct().collect(Collectors.toList()); // getListOffStaffWeekOffsByDepartmentId

		for (UserBasicInformation eachUser : users) {

			StaffLayout eachUserLayout = new StaffLayout();
			// fetch the userBasicInformation object for each user based on userId

			UserOrganizations userOrganization = userOrganizationDAO.findByUserIdAndOrganizationId(eachUser.getUserId(),
					userWorkspaceList.get(eachUser.getUserId()));

			if (userOrganization != null)
				eachUser.setStaffUserCoreTypeId((userOrganization).getStaffUserCoreTypeId());
			else
				continue;

			StaffUserCoreTypes staffUserCoreTypes = staffUserCoreTypesRepository
					.findStaffUserCoreTypesById(eachUser.getStaffUserCoreTypeId());

			if (staffUserCoreTypes != null)
				eachUser.setStaffUserCoreTypeName(staffUserCoreTypes.getLabel());
			else
				continue;

			eachUserLayout.setUserBasicInformation(eachUser);

			UserWorkspaces userWorkspace = userWorkspacesRepository.findByUserIdAndWorkspaceId(eachUser.getUserId(),
					requestScheduleDuration.getDepartmentId());

			List<Long> departmentShiftId = userWorkspace.getDepartmentShiftIds();
			List<DepartmentShifts> departmentShifts = new ArrayList<DepartmentShifts>();
			if(departmentShiftId!=null) {
				departmentShifts = (List<DepartmentShifts>) departmentShiftsRepository.findAllById(departmentShiftId);
			}
			List<ShiftPlanningLayout> shiftPlanningLayout = new ArrayList<ShiftPlanningLayout>();
			// calculate the user shiftPlanning layout object based on pay period
			for (int j = 0; j <= duration; j++) {

				ShiftPlanningLayout shiftPlanningLayoutObj = new ShiftPlanningLayout();

				DateTime nextDate = startDate.plusDays(j);
				// fetch all info based on shiftDate check his or her availability based on the
				// shiftDate
				LocalDate shiftDate = Instant.ofEpochMilli(nextDate.getMillis()).atZone(ZoneId.systemDefault())
						.toLocalDate();

				// calculate the weekday of each nextDate

				// check the eachUser-> userId in getListOffStaffWeekOffsByDepartmentId
				// if user exists in the list then consider that StaffWeekOff object and check
				// the calculated weekday is a week off for that user or not

				// based on weekday and user weekoffs comparison if it is true then user is off
				// and don't consider for further calcuation
				// if not then create shiftPlanningLayoutObj for that date and user.

				shiftPlanningLayoutObj.setShiftDate(nextDate);

				if (userRequest.contains(eachUser.getUserId() + " " + shiftDate + " "
						+ requestScheduleDuration.getDepartmentId() + " " + "Approved")) {

					shiftPlanningLayoutObj.setAssigned(false);
					shiftPlanningLayoutObj.setAvailable(false);
					shiftPlanningLayoutObj.setDepartmentId(requestScheduleDuration.getDepartmentId());
					shiftPlanningLayoutObj.setDepartmentShifts(departmentShifts);
					shiftPlanningLayoutObj.setOnDate(null);
					shiftPlanningLayoutObj.setRequest(true);
					shiftPlanningLayoutObj.setUserId(eachUser.getUserId());
					shiftPlanningLayoutObj.setOpenShift(false);

					shiftPlanningLayout.add(shiftPlanningLayoutObj);
					continue;
				}

				String formattedDate = nextDate.getDayOfMonth() + "-" + nextDate.getMonthOfYear() + "-"
						+ nextDate.getYear();
				SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("dd-MM-yyyy");
				Date date = null;
				try {
					date = simpleDateFormat1.parse(formattedDate);
				} catch (ParseException e) {

					RestResponse restResponse = new RestResponse();

					restResponse.setStatus(e.getErrorOffset());
					restResponse.setSuccess(false);
					restResponse.setMessage("Exception occured");
					restResponse.setData(e.getCause());
					return ResponseEntity.ok(restResponse);
				}

				DateFormat weekDayFormatter = new SimpleDateFormat("EEEE"); // change the date in weekday
				String weekDay = weekDayFormatter.format(date).toLowerCase();

				if (setScheduleLayouts != null && setScheduleLayouts.size() > 0) {

					if (setScheduleRoles.contains(userWorkspace.getStaffUserCoreTypeId())
							&& userWorkspace.isPrimaryDepartment() == true) {

						SetScheduleLayouts setSchedule = setScheduleLayouts.stream().filter(eachSchedule -> eachSchedule
								.getRoles().contains(userWorkspace.getStaffUserCoreTypeId())).findAny().orElse(null);
						List<Long> setScheduleLayoutIds = setScheduleLayouts.stream()
								.filter(emitter -> emitter.getRoles().contains(eachUser.getStaffUserCoreTypeId()))
								.map(eachSchedule -> eachSchedule.getSetScheduleLayoutId())
								.collect(Collectors.toList());

						// check if the user is involved in setScheduleLayout
						if (staffWeekOffsDAO
								.getStaffWeekOffByUserIdAndSetScheduleLayoutIdAndDepartmentId(eachUser.getUserId(),
										setScheduleLayoutIds, requestScheduleDuration.getDepartmentId())
								.size() > 0) {

							// verify the weekoffs based on date
							StaffWeekOffs staffWeekOff = staffWeekOffsDAO
									.getStaffWeekOffByUserIdAndDepartmentIdAndStaffWeekOffDay(eachUser.getUserId(),
											requestScheduleDuration.getDepartmentId(), weekDay);
							SetScheduleLayouts weekOffDay = setScheduleLayoutDAO
									.findBySetScheduleLayoutIdsAndUserIdAndWeekDay(setScheduleLayoutIds,
											eachUser.getUserId(), weekDay);
							// end of verify weekoffs

							LocalDate localNextDate = shiftDate;
							int tempDiffStart = setSchedule.getStartDate().compareTo(localNextDate);
							int tempDiffEnd = 0;

							if (setSchedule.getEndDate() != null)
								tempDiffEnd = setSchedule.getEndDate().compareTo(localNextDate);

							// System.out.println("tempDiff" + tempDiff);
							if (staffWeekOff == null && tempDiffStart <= 0 && weekOffDay == null
									&& (setSchedule.getScheduleLength().equals("INF") || tempDiffEnd >= 0)) {

								shiftPlanningLayoutObj.setAssigned(true);
								shiftPlanningLayoutObj.setDepartmentShifts(departmentShifts);
								shiftPlanningLayoutObj.setAvailable(true);
								shiftPlanningLayoutObj.setDepartmentId(userWorkspace.getWorkspaceId());

								/*if (departmentShifts.isPresent()) {

									shiftPlanningLayoutObj.setShiftTypeName(departmentShifts.get().getLabel());
									shiftPlanningLayoutObj.setStartTime(departmentShifts.get().getStartTime()); // get
																												// this
																												// value
																												// from
																												// shift
																												// types
																												// collection
									shiftPlanningLayoutObj.setEndTime(departmentShifts.get().getEndTime()); // get this
																											// value
																											// from
																											// shift
																											// types
																											// collection
								} else {
									shiftPlanningLayoutObj.setStartTime(null);
									shiftPlanningLayoutObj.setEndTime(null);
									shiftPlanningLayoutObj.setShiftTypeName(null);
								}
*/
								shiftPlanningLayout.add(shiftPlanningLayoutObj);
								continue;
							}
						}
					}
				}

				if (scheduleList.contains(requestScheduleDuration.getDepartmentId() + " " + eachUser.getUserId() + " "
						+ shiftDate + " " + "Active")) {

					shiftPlanningLayoutObj.setAssigned(true);
					shiftPlanningLayoutObj.setAvailable(true);

					Schedules schedule = scheduleDAO.findScheduleByUserIdAndShiftDateAndDepartmentId(
							eachUser.getUserId(), shiftDate, requestScheduleDuration.getDepartmentId());
					
					Optional<DepartmentShifts> departmentShiftsOptional = departmentShiftsRepository
							.findById(schedule.getShiftTypeId());

					if (schedule.getEventType().equals("OpenShift")) {

						
						shiftPlanningLayoutObj
								.setDepartmentShifts(departmentShifts);//(departmentShiftsOptional.get().getDepartmentShiftId()); // shiftTypeId
																												// from
																												// user
																												// workspace
																												// and
																												// if
																												// its
																												// not
																												// available
																												// in
																												// user
																												// workspace
																												// please
																												// add
																												// it

						if (departmentShiftsOptional.isPresent()) {

							shiftPlanningLayoutObj.setShiftTypeName(departmentShiftsOptional.get().getLabel());
							shiftPlanningLayoutObj.setStartTime(departmentShiftsOptional.get().getStartTime()); // get
																												// this
																												// value
																												// from
																												// shift
																												// types
																												// collection
							shiftPlanningLayoutObj.setEndTime(departmentShiftsOptional.get().getEndTime()); // get this
																											// value
																											// from
																											// shift
																											// types
																											// collection
						} else {
							shiftPlanningLayoutObj.setStartTime(null);
							shiftPlanningLayoutObj.setEndTime(null);
							shiftPlanningLayoutObj.setShiftTypeName(null);
						}
					} else {
						if (departmentShiftsOptional.isPresent()) {

							shiftPlanningLayoutObj.setShiftTypeName(departmentShiftsOptional.get().getLabel());
							shiftPlanningLayoutObj.setStartTime(departmentShiftsOptional.get().getStartTime()); // get this
																										// value from
																										// shift types
																										// collection
							shiftPlanningLayoutObj.setEndTime(departmentShiftsOptional.get().getEndTime()); // get this value
																									// from shift types
																									// collection
						} else {
							shiftPlanningLayoutObj.setStartTime(null);
							shiftPlanningLayoutObj.setEndTime(null);
							shiftPlanningLayoutObj.setShiftTypeName(null);
						}
					}
				}
				shiftPlanningLayout.add(shiftPlanningLayoutObj);

				shiftPlanningLayoutObj.setUserId(eachUser.getUserId());

			}
			eachUserLayout.setShiftPlanningLayout(shiftPlanningLayout);
			staffLayout.add(eachUserLayout);
		}

		scheduleResponse.setStaffLayout(staffLayout);

		RestResponse restResponse = new RestResponse();

		restResponse.setStatus(200);
		restResponse.setSuccess(true);
		restResponse.setMessage("");
		restResponse.setData(scheduleResponse);
		return ResponseEntity.ok(restResponse);
	}

	@RequestMapping(value = "/month", method = RequestMethod.POST)
	public ResponseEntity<Object> getSchedulesByMonth(@RequestBody RequestSchedulesByMonth requestSchedulesByMonth) {

		LocalDate date = LocalDate.of(requestSchedulesByMonth.getYear(), requestSchedulesByMonth.getMonth(), 1);
		LocalDate localStartDate = date.withDayOfMonth(1);
		LocalDate localEndDate = date.withDayOfMonth(date.lengthOfMonth());

		ScheduleMonthResponse scheduleResponse = new ScheduleMonthResponse();
		scheduleResponse.setYear("" + localStartDate.getYear());
		Month month = date.getMonth();
		scheduleResponse.setMonth(month.getDisplayName(TextStyle.SHORT, Locale.US));

		List<Schedules> listOfSchedules = scheduleDAO.findSchedulesByDepartmentIdAndDuration(
				requestSchedulesByMonth.getDepartmentId(), localStartDate, localEndDate);
		List<Long> userIds = listOfSchedules.stream().map(each -> each.getUserId()).distinct()
				.collect(Collectors.toList());

		List<UserWorkspaces> userWorkspaceList = userWorkspacesDAO.findByUserIdsAndDepartmentId(userIds,
				requestSchedulesByMonth.getDepartmentId());
		List<StaffUserCoreTypes> staffUserCoreTypes = new ArrayList<StaffUserCoreTypes>();
		if (requestSchedulesByMonth.getStaffCoreType() > 0) {
			Optional<StaffUserCoreTypes> staffUserCoreType=staffUserCoreTypesRepository.findById(requestSchedulesByMonth.getStaffCoreType());
			if(staffUserCoreType.isPresent())
				staffUserCoreTypes.add(staffUserCoreType.get());
		} else {
			staffUserCoreTypes = staffUserCoreTypesRepository.findAll();
		}
		List<ScheduleMonthLayout> monthLayoutList = new ArrayList<ScheduleMonthLayout>();
		for (int i = 1; i <= date.lengthOfMonth(); i++) {
			ScheduleMonthLayout monthLayout = new ScheduleMonthLayout();

			LocalDate shiftDate = date.withDayOfMonth(i);
			monthLayout.setShiftDate(shiftDate);
			List<WorkingStaffCountByCoreType> workingStaffCountByCoreTypeList = new ArrayList<WorkingStaffCountByCoreType>();
			if(listOfSchedules.stream().anyMatch(schedule -> schedule.getShiftDate().isEqual(shiftDate))) {
			
			for (StaffUserCoreTypes coreType : staffUserCoreTypes) {
				WorkingStaffCountByCoreType workingStaffCountByCoreType = new WorkingStaffCountByCoreType();
				workingStaffCountByCoreType.setCoreTypeShortName(coreType.getShortName());
				int count = 0;
				List<UserWorkspaces> tempuserWorkspaceList = userWorkspaceList.stream()
						.filter(each -> each.getStaffUserCoreTypeId() == coreType.getStaffUserCoreTypesId())
						.collect(Collectors.toList());
				for (UserWorkspaces userWorkspace : tempuserWorkspaceList) {
					//System.out.println(shiftDate + " == "+userWorkspace.getUserId() + " @ "+userWorkspace.getStaffUserCoreTypeId() );
					count += listOfSchedules.stream()
							.filter(schedule -> schedule.getUserId() == userWorkspace.getUserId()
									&& schedule.getShiftDate().isEqual(shiftDate))
							.count();
				}
				workingStaffCountByCoreType.setCount(count);
				workingStaffCountByCoreTypeList.add(workingStaffCountByCoreType);
			}
			} else {
				
			}
			monthLayout.setStaffLayout(workingStaffCountByCoreTypeList);

			monthLayoutList.add(monthLayout);
		}
		scheduleResponse.setMonthLayout(monthLayoutList);
		RestResponse restResponse = new RestResponse();

		restResponse.setStatus(200);
		restResponse.setSuccess(true);
		restResponse.setMessage("");
		restResponse.setData(scheduleResponse);
		return ResponseEntity.ok(restResponse);
	}

	@RequestMapping(value = "/duration/onDate", method = RequestMethod.POST)
	public ResponseEntity<Object> getSchedulesOnDate(@RequestBody RequestSchedulesByDate requestSchedulesByDate) {

		DateTime onDate = DateTime.parse(requestSchedulesByDate.getOnDate());
		LocalDate localStartDate = Instant.ofEpochMilli(onDate.getMillis()).atZone(ZoneId.systemDefault())
				.toLocalDate();

		List<Schedules> scheduleList = scheduleDAO.findSchedulesByShiftDateAndDepartmentId(localStartDate,
				requestSchedulesByDate.getDepartmentId());
		//System.out.println(scheduleList);
		List<Long> userIds = scheduleList.stream().map(Schedules::getUserId).collect(Collectors.toList());

		List<UserBasicInformation> users = userBasicInformationDAO.findByUserId(userIds);
		List<DepartmentShifts> departmentShifts = departmentShiftsRepository.findByDepartmentIdAndStatus(requestSchedulesByDate.getDepartmentId(),"Active");

		// List<StaffLayout> staffLayout = new ArrayList<StaffLayout>(); // Create the
		// staff layout for staff members in
		// each group

		// List<SetScheduleLayouts> setScheduleLayouts = setScheduleLayoutDAO
		// .findByDepartmentId(requestSchedulesByDate.getDepartmentId());

		List<StaffUserCoreTypes> staffUserCoreTypes = new ArrayList<StaffUserCoreTypes>();
		if (requestSchedulesByDate.getStaffCoreType() > 0) {
			Optional<StaffUserCoreTypes> staffUserCoreType=staffUserCoreTypesRepository.findById(requestSchedulesByDate.getStaffCoreType());
			if(staffUserCoreType.isPresent())
				staffUserCoreTypes.add(staffUserCoreType.get());
		} else {
			staffUserCoreTypes = staffUserCoreTypesRepository.findAll();
		}
		SchedulesByDateResponse schedulesByDateResponse = new SchedulesByDateResponse();
		
		DateTimeFormatter dtf = DateTimeFormat.forPattern("MM/dd/yyyy");
		
		schedulesByDateResponse.setShiftDate(dtf.print(onDate));
		List<ShiftwiseSchedules> shiftwiseSchedules = new ArrayList<ShiftwiseSchedules>();
		for (DepartmentShifts shift : departmentShifts) {

			ShiftwiseSchedules shiftwiseSchedule = new ShiftwiseSchedules();
			shiftwiseSchedule.setShiftName(shift.getLabel());
			shiftwiseSchedule.setStartTime(shift.getStartTime().format(java.time.format.DateTimeFormatter.ISO_LOCAL_TIME.ofPattern("h:m a")));
			shiftwiseSchedule.setEndTime(shift.getEndTime().format(java.time.format.DateTimeFormatter.ofPattern("h:m a")));
			shiftwiseSchedule.setIcon(shift.getIcon());

			List<Schedules> scheduleListTemp = scheduleList.stream().filter(each -> each.getShiftTypeId() == shift.getDepartmentShiftId())
					.collect(Collectors.toList());
			List<UserBasicResponse> userBasicInfoList = new ArrayList<UserBasicResponse>();
			for (Schedules eachSchedule : scheduleListTemp) {

				Optional<UserBasicInformation> user = users.stream().filter(eachUser -> eachUser.getUserId() == eachSchedule.getUserId()).findFirst();

				if (user.isPresent()) {

					Optional<StaffUserCoreTypes> staffUserCoreType = staffUserCoreTypes.stream()
							.filter(each -> each.getStaffUserCoreTypesId() == user.get().getStaffUserCoreTypeId())
							.findFirst();
					UserBasicResponse userBasic = new UserBasicResponse();
					userBasic.setFirstName(user.get().getFirstName());
					userBasic.setLastName(user.get().getLastName());
					userBasic.setCoreTypeName(staffUserCoreType.isPresent() ? staffUserCoreType.get().getLabel() : null);
					if (userBasic.getCoreTypeName() != null)
						userBasicInfoList.add(userBasic);
				}

			}
			shiftwiseSchedule.setUserList(userBasicInfoList);
			shiftwiseSchedules.add(shiftwiseSchedule);
		}
		schedulesByDateResponse.setShiftwiseSchedules(shiftwiseSchedules);

		RestResponse restResponse = new RestResponse();

		restResponse.setStatus(200);
		restResponse.setSuccess(true);
		restResponse.setMessage("");
		restResponse.setData(schedulesByDateResponse);
		return ResponseEntity.ok(restResponse);
	}

	@RequestMapping(value = "/duration/user", method = RequestMethod.POST)
	public ResponseEntity<Object> getSchedulesByDurationAndUser(
			@RequestBody RequestUserSchedule requestScheduleDuration) {

		DateTime startDate = DateTime.parse(requestScheduleDuration.getStartDate());
		DateTime endDate = DateTime.parse(requestScheduleDuration.getEndDate());
		LocalDate localStartDate = LocalDate.parse(requestScheduleDuration.getStartDate());
		LocalDate localEndDate = LocalDate.parse(requestScheduleDuration.getEndDate());
		Days days = Days.daysBetween(startDate, endDate);
		int duration = days.getDays(); // calculate the duration between startDate and endDate
		DateFormatSymbols dateFormatSymbol = new DateFormatSymbols();
		String[] months = dateFormatSymbol.getMonths(); // fetches the month name

		UserScheduleResponse scheduleResponse = new UserScheduleResponse();

		scheduleResponse
				.setPeriod(requestScheduleDuration.getStartDate() + " - " + requestScheduleDuration.getEndDate());
		scheduleResponse.setYear("" + startDate.getYear());
		scheduleResponse.setWeek("" + startDate.getWeekOfWeekyear());
		scheduleResponse
				.setMonth(months[startDate.getMonthOfYear() - 1] + " - " + months[endDate.getMonthOfYear() - 1]);

		List<UserWorkspaces> userWorkspace = new ArrayList<>();

		if (requestScheduleDuration.getWorkspaces().size() == 0) {
			userWorkspace = userWorkspacesRepository.findUserWorkspacesByUserIdAndStatusAndIsJoined(
					requestScheduleDuration.getUserId(), "Active", true);
		} else {
			userWorkspace = userWorkspacesDAO.findByUserIdAndBusinessIds(requestScheduleDuration.getUserId(),
					requestScheduleDuration.getWorkspaces());
		}

		if (userWorkspace.size() > 0) {

			List<UserStaffLayout> staffLayout = new ArrayList<UserStaffLayout>(); // Create the staff layout for staff
																					// members in each group
			UserStaffLayout eachUserLayout = new UserStaffLayout();

			// fetch the userBasicInformation object for each user based on userId

			List<ShiftPlanningLayout> shiftPlanningLayout = new ArrayList<ShiftPlanningLayout>();
			// calculate the user shiftPlanning layout object based on pay period

			List<Long> workspaceIds = userWorkspace.stream().map(emitter -> emitter.getWorkspaceId())
					.collect(Collectors.toList());

			List<Schedules> userSchedules = scheduleDAO.findUserScheduleByWorkspaceIdsUserIdAndStartDateAndEndDate(
					workspaceIds, requestScheduleDuration.getUserId(), localStartDate, localEndDate);
			

			List<String> userRequest = requestsDAO.findRequestsByUserIdAndOnDate(
					Arrays.asList(requestScheduleDuration.getUserId()), localStartDate, localEndDate);

			List<Long> worksiteIds = userWorkspace.stream().map(emitter -> emitter.getBusinessId())
					.collect(Collectors.toList());

			Map<Long, WorksiteSettings> worksiteSettings = worksiteSettingsDAO
					.findWorksiteSettingsByUserIdAndWorksiteIds(requestScheduleDuration.getUserId(), worksiteIds);
			List<DepartmentShifts> departmentShifts = new ArrayList<DepartmentShifts>();
			if(userWorkspace.get(0).getDepartmentShiftIds()!=null) {
				departmentShifts = (List<DepartmentShifts>) departmentShiftsRepository.findAllById(userWorkspace.get(0).getDepartmentShiftIds());	
			}
			
			for (UserWorkspaces eachWorkspace : userWorkspace) {

				// fetch set scheduling list
				List<SetScheduleLayouts> setScheduleLayouts = setScheduleLayoutDAO.findByDepartmentIdAndCoreTypeId(
						eachWorkspace.getWorkspaceId(), eachWorkspace.getStaffUserCoreTypeId());

				List<Long> setScheduleRoles = setScheduleLayouts.stream()
						.flatMap(setSchedule -> setSchedule.getRoles().stream()).collect(Collectors.toList());
				// end of set schedule list

				for (int j = 0; j <= duration; j++) {

					ShiftPlanningLayout shiftPlanningLayoutObj = new ShiftPlanningLayout();

					DateTime nextDate = startDate.plusDays(j);

					// fetch all info based on shiftDate check his or her availability based on the
					// shiftDate

					LocalDate shiftDate = Instant.ofEpochMilli(nextDate.getMillis()).atZone(ZoneId.systemDefault())
							.toLocalDate();

					shiftPlanningLayoutObj.setOnDate(shiftDate.toString());

					boolean assigned;
					boolean openShift;

					String weekDay = ConstantUtils.convertDateToWeekDay(nextDate); // fetch weekday

					if (userRequest.contains(requestScheduleDuration.getUserId() + " " + shiftDate + " "
							+ eachWorkspace.getWorkspaceId() + " " + "Approved")) {

						shiftPlanningLayoutObj.setAssigned(false);
						shiftPlanningLayoutObj.setAvailable(false);
						shiftPlanningLayoutObj.setDepartmentId(eachWorkspace.getWorkspaceId());
						shiftPlanningLayoutObj.setDepartmentShifts(departmentShifts);//(eachWorkspace.getDepartmentShiftId());
						shiftPlanningLayoutObj.setOnDate(shiftDate.toString());
						shiftPlanningLayoutObj.setRequest(true);
						shiftPlanningLayoutObj.setUserId(requestScheduleDuration.getUserId());
						shiftPlanningLayoutObj.setOpenShift(false);

						shiftPlanningLayout.add(shiftPlanningLayoutObj);
						continue;
					}

					// Identify schedules for set scheduling
					/*if (setScheduleLayouts != null && setScheduleLayouts.size() > 0) {

						List<Long> setScheduleLayoutIds = setScheduleLayouts.stream()
								.map(e -> e.getSetScheduleLayoutId()).collect(Collectors.toList());

						if (staffWeekOffsDAO.getStaffWeekOffByUserIdAndSetScheduleLayoutIdAndDepartmentId(
								requestScheduleDuration.getUserId(), setScheduleLayoutIds,
								eachWorkspace.getWorkspaceId()).size() > 0) {

							if (setScheduleRoles.contains(eachWorkspace.getStaffUserCoreTypeId())
									&& eachWorkspace.isPrimaryDepartment() == true) {

								SetScheduleLayouts setSchedule = setScheduleLayouts.stream()
										.filter(eachSchedule -> eachSchedule.getRoles()
												.contains(eachWorkspace.getStaffUserCoreTypeId()))
										.findAny().orElse(null);
								// System.out.println("setSchedule:" + setSchedule.toString());
								// verify the weekoffs based on date

								StaffWeekOffs staffWeekOff = null;

								staffWeekOff = staffWeekOffsDAO
										.getStaffWeekOffByUserIdAndDepartmentIdAndStaffWeekOffDay(
												eachWorkspace.getUserId(), eachWorkspace.getWorkspaceId(), weekDay);
								// end of verify weekoffs
								LocalDate localNextDate = Instant.ofEpochMilli(nextDate.getMillis())
										.atZone(ZoneId.systemDefault()).toLocalDate();
								int tempDiffStart = setSchedule.getStartDate().compareTo(localNextDate);

								int tempDiffEnd = 0;

								if (setSchedule.getEndDate() != null)
									tempDiffEnd = setSchedule.getEndDate().compareTo(localNextDate);

								SetScheduleLayouts weekOffDay = setScheduleLayoutDAO
										.findBySetScheduleLayoutIdsAndUserIdAndWeekDay(setScheduleLayoutIds,
												requestScheduleDuration.getUserId(), weekDay);

								if (staffWeekOff == null && tempDiffStart <= 0 && weekOffDay == null
										&& (setSchedule.getScheduleLength().equals("indefinite") || tempDiffEnd >= 0)) {

									assigned = true;

									shiftPlanningLayoutObj.setUserId(requestScheduleDuration.getUserId());
									shiftPlanningLayoutObj.setAssigned(true);
									shiftPlanningLayoutObj.setDepartmentShifts(departmentShifts);//(eachWorkspace.getDepartmentShiftId());
									shiftPlanningLayoutObj.setAvailable(true);
									shiftPlanningLayoutObj.setRequest(false);
									shiftPlanningLayoutObj.setDepartmentId(eachWorkspace.getWorkspaceId());

									

									// WorksiteSettings worksiteSettings =
									// worksiteSettingsRepository.findByUserIdAndWorksiteId(requestScheduleDuration.getUserId(),
									// eachWorkspace.getBusinessId());

									if (worksiteSettings != null && worksiteSettings.size() > 0
											&& worksiteSettings.keySet().contains(eachWorkspace.getBusinessId())) {

										WorksiteSettings worksiteSetting = worksiteSettings
												.get(eachWorkspace.getBusinessId());

										worksiteSetting.setOrganizationId(eachWorkspace.getOrganizationId());
										worksiteSetting.setOrganizationName(organizationRepository
												.findById(eachWorkspace.getOrganizationId()).get().getName());
										worksiteSetting.setWorksiteId(eachWorkspace.getBusinessId());
										worksiteSetting.setWorksiteName(businessesRepository
												.findById(eachWorkspace.getBusinessId()).get().getName());

										shiftPlanningLayoutObj.setWorksiteSettings(worksiteSetting);
									} else
										shiftPlanningLayoutObj.setWorksiteSettings(null);

									shiftPlanningLayout.add(shiftPlanningLayoutObj);
									continue;
								}
							}
						}
					} */// end of set schedulings

					// find assignment schedules of user

					/*if (worksiteIds.size() > 0 && (worksiteIds.contains(eachWorkspace.getBusinessId()))) { // to avoid
																											// repetition
																											// of
																											// assignments

						List<Assignments> assignment = assignmentDAO
								.findAssignmentByBusinessIdAndGivenDate(eachWorkspace.getBusinessId(), shiftDate);

						if (assignment.size() > 0) {

							List<Long> assignmentIds = assignment.stream().map(Assignments::getAssignmentId)
									.collect(Collectors.toList());

							List<AssignmentSchedules> assignmentSchedules = assignmentSchedulesDAO
									.findAssignmentSchedulesByAssignmentIdsAndUserIdAndWeekDay(assignmentIds,
											requestScheduleDuration.getUserId(), weekDay);

							if (assignmentSchedules.size() > 0) {

								assignmentIds = assignmentSchedules.stream().map(AssignmentSchedules::getAssignmentId)
										.collect(Collectors.toList());
								List<AssignmentInvitees> invitee = assignmentInviteesDAO
										.findByUserIdAndAssignmentIdsAndIsApproved(requestScheduleDuration.getUserId(),
												assignmentIds, true);

								if (invitee.size() > 0) {

									shiftPlanningLayoutObj.setAssigned(true);
									shiftPlanningLayoutObj.setDepartmentShifts(departmentShifts);//(eachWorkspace.getDepartmentShiftId());
									shiftPlanningLayoutObj.setAvailable(true);

									if (worksiteSettings != null && worksiteSettings.size() > 0
											&& worksiteSettings.containsKey(eachWorkspace.getBusinessId())) {

										WorksiteSettings worksiteSetting = worksiteSettings
												.get(eachWorkspace.getBusinessId());

										worksiteSetting.setOrganizationId(eachWorkspace.getOrganizationId());
										worksiteSetting.setOrganizationName(organizationRepository
												.findById(eachWorkspace.getOrganizationId()).get().getName());
										worksiteSetting.setWorksiteId(eachWorkspace.getBusinessId());
										worksiteSetting.setWorksiteName(businessesRepository
												.findById(eachWorkspace.getBusinessId()).get().getName());

										shiftPlanningLayoutObj.setWorksiteSettings(worksiteSetting);
									} else
										shiftPlanningLayoutObj.setWorksiteSettings(null);

									shiftPlanningLayout.add(shiftPlanningLayoutObj);
									continue;
								}
							}
						}
					}*/

					assigned = false;
					// end of assigment schedules

					if (userSchedules.size() > 0) { // check in schedules collection by passing userId and shiftDate

						
						Optional<Schedules> filteredSchedule = userSchedules.stream().filter(eachSchedule -> eachSchedule.getShiftDate().isEqual(shiftDate) && eachWorkspace.getWorkspaceId() == eachSchedule.getDepartmentId()).findFirst();
						
						if(filteredSchedule.isPresent()) {
							
							if (filteredSchedule.get().getStatus().equals("Active")) {
								
								Optional<DepartmentShifts> departmentShiftsOptionalSetschedule = departmentShiftsRepository
									.findById(filteredSchedule.get().getShiftTypeId());

							

							if (departmentShiftsOptionalSetschedule.isPresent()) {
								shiftPlanningLayoutObj
										.setShiftTypeName(departmentShiftsOptionalSetschedule.get().getLabel());
								shiftPlanningLayoutObj
										.setStartTime(departmentShiftsOptionalSetschedule.get().getStartTime()); // get
																													// this
																													// value
																													// from
																													// shift
																													// types
																													// collection
								shiftPlanningLayoutObj
										.setEndTime(departmentShiftsOptionalSetschedule.get().getEndTime()); // get
																												// this
																												// value
																												// from
																												// shift
																												// types
																												// collection

							} else {
								shiftPlanningLayoutObj.setStartTime(null);
								shiftPlanningLayoutObj.setEndTime(null);
								shiftPlanningLayoutObj.setShiftTypeName(null);
							}
								
								//fetching shift details
	
								shiftPlanningLayoutObj.setAssigned(true);
								assigned = true;
							} else {
								shiftPlanningLayoutObj.setAssigned(false);
								assigned = false;
							}
						}
					} else {
						shiftPlanningLayoutObj.setAssigned(false);
						assigned = false;
					}

					OpenShifts findOpenShift = openShiftDAO.findByUserIdAndOnDateAndDepartmentId(
							requestScheduleDuration.getUserId(), shiftDate, eachWorkspace.getWorkspaceId());

					if (findOpenShift != null) {

						OpenShiftInvitees openShiftInvitee = openShiftInviteesDAO
								.findByOpenShiftIdAndUserIdAndIsApproved(findOpenShift.getOpenShiftId(),
										requestScheduleDuration.getUserId(), true);

						if (openShiftInvitee != null) {
							openShift = true;
							shiftPlanningLayoutObj.setOpenShift(true);
						} else {
							openShift = false;
							shiftPlanningLayoutObj.setOpenShift(false);
						}
					} else {
						openShift = false;
						shiftPlanningLayoutObj.setOpenShift(false);
					}

					if (openShift == false && shiftPlanningLayoutObj.isRequest() == false && assigned == false)
						continue;

					if (openShift == true || shiftPlanningLayoutObj.isRequest() == true || assigned == true) {

						if (worksiteSettings != null && worksiteSettings.size() > 0
								&& worksiteSettings.containsKey(eachWorkspace.getBusinessId())) {

							WorksiteSettings worksiteSetting = worksiteSettings.get(eachWorkspace.getBusinessId());

							worksiteSetting.setOrganizationName(
									organizationRepository.findById(eachWorkspace.getOrganizationId()).get().getName());
							worksiteSetting.setWorksiteName(
									businessesRepository.findById(eachWorkspace.getBusinessId()).get().getName());

							shiftPlanningLayoutObj.setWorksiteSettings(worksiteSetting);
						} else
							shiftPlanningLayoutObj.setWorksiteSettings(null);
					}

					/*if (userSchedules.contains(eachWorkspace.getWorkspaceId() + " "
							+ requestScheduleDuration.getUserId() + " " + shiftDate + " " + "Active")) {

						Schedules foundSchedule = scheduleDAO.findScheduleByUserIdAndShiftDateAndDepartmentId(
								requestScheduleDuration.getUserId(), shiftDate, eachWorkspace.getWorkspaceId());

						if (foundSchedule != null) {
							if (foundSchedule.getEventType().equals("OpenShift")) {

								Optional<DepartmentShifts> departmentShiftsOptional = departmentShiftsRepository
										.findById(foundSchedule.getShiftTypeId());

								shiftPlanningLayoutObj
										.setDepartmentShiftId(departmentShiftsOptional.get().getDepartmentShiftId()); // shiftTypeId
																														// from
																														// user
																														// workspace
																														// and
																														// if
																														// its
																														// not
																														// available
																														// in
																														// user
																														// workspace
																														// please
																														// add
																														// it

								if (departmentShiftsOptional.isPresent()) {

									shiftPlanningLayoutObj.setShiftTypeName(departmentShiftsOptional.get().getLabel());
									shiftPlanningLayoutObj.setStartTime(departmentShiftsOptional.get().getStartTime()); // get
																														// this
																														// value
																														// from
																														// shift
																														// types
																														// collection
									shiftPlanningLayoutObj.setEndTime(departmentShiftsOptional.get().getEndTime()); // get
																													// this
																													// value
																													// from
																													// shift
																													// types
																													// collection
								} else {
									shiftPlanningLayoutObj.setStartTime(null);
									shiftPlanningLayoutObj.setEndTime(null);
									shiftPlanningLayoutObj.setShiftTypeName(null);
								}
							} else {
								Optional<DepartmentShifts> departmentShiftsOptional = departmentShiftsRepository
										.findById(foundSchedule.getShiftTypeId());

								if (departmentShiftsOptional.isPresent()) {

									shiftPlanningLayoutObj.setShiftTypeName(departmentShiftsOptional.get().getLabel());
									shiftPlanningLayoutObj.setStartTime(departmentShiftsOptional.get().getStartTime()); // get
																														// this
																														// value
																														// from
																														// shift
																														// types
																														// collection
									shiftPlanningLayoutObj.setEndTime(departmentShiftsOptional.get().getEndTime()); // get
																													// this
																													// value
																													// from
																													// shift
																													// types
																													// collection
								} else {
									shiftPlanningLayoutObj.setStartTime(null);
									shiftPlanningLayoutObj.setEndTime(null);
									shiftPlanningLayoutObj.setShiftTypeName(null);
								}
							}
						}
					} else {

						Optional<DepartmentShifts> departmentShiftsOptional = departmentShiftsRepository
								.findById(eachWorkspace.getDepartmentShiftId());

						if (departmentShiftsOptional.isPresent()) {

							shiftPlanningLayoutObj.setShiftTypeName(departmentShiftsOptional.get().getLabel());
							shiftPlanningLayoutObj.setStartTime(departmentShiftsOptional.get().getStartTime()); // get
																												// this
																												// value
																												// from
																												// shift
																												// types
																												// collection
							shiftPlanningLayoutObj.setEndTime(departmentShiftsOptional.get().getEndTime()); // get this
																											// value
																											// from
																											// shift
																											// types
																											// collection
						} else {
							shiftPlanningLayoutObj.setStartTime(null);
							shiftPlanningLayoutObj.setEndTime(null);
							shiftPlanningLayoutObj.setShiftTypeName(null);
						}
					}

					shiftPlanningLayoutObj.setDepartmentId(eachWorkspace.getWorkspaceId());

					shiftPlanningLayoutObj.setUserId(requestScheduleDuration.getUserId());

					
				}*/
					shiftPlanningLayout.add(shiftPlanningLayoutObj);
				worksiteIds.remove(eachWorkspace.getBusinessId()); // remove the businessId which is used
			}
			}
			Map<String, List<ShiftPlanningLayout>> shiftPlanning = shiftPlanningLayout.stream().collect(
					Collectors.groupingBy(ShiftPlanningLayout::getOnDate, LinkedHashMap::new, Collectors.toList()));

			eachUserLayout.setShiftPlanningLayout(shiftPlanning);
			staffLayout.add(eachUserLayout);

			scheduleResponse.setStaffLayout(staffLayout);

			RestResponse restResponse = new RestResponse();

			restResponse.setStatus(200);
			restResponse.setSuccess(true);
			restResponse.setMessage("");
			restResponse.setData(scheduleResponse);
			return ResponseEntity.ok(restResponse);
		} else {

			RestResponse restResponse = new RestResponse();

			restResponse.setStatus(404);
			restResponse.setSuccess(false);
			restResponse.setMessage("User Id does not exist");
			restResponse.setData(null);
			return ResponseEntity.status(404).body(restResponse);

		}
	}

	@GetMapping("/missed")
	public ResponseEntity<Object> fetchMissedSchedulesOfUsers() {

		List<ScheduleLayout> scheduleLayouts = scheduleLayoutDAO
				.findByAvailabilityDeadlineDateAndIsPublised(DateTime.now(), true, "lte"); // fetch the schedule layouts
																							// which have passed the
																							// date

		List<Long> scheduleLayoutIds = scheduleLayouts.stream().map(ScheduleLayout::getScheduleLayoutId)
				.collect(Collectors.toList()); // converting object list to ids Long list

		List<ScheduleLayoutGroups> staffUsers = scheduleLayoutGroupsDAO.findUsersByScheduleLayoutId(scheduleLayoutIds); // find
																														// the
																														// users
																														// which
																														// are
																														// there
																														// in
																														// the
																														// schedule
																														// layout

		Set<Long> userIds = staffUsers.stream().map(ScheduleLayoutGroups::getUserIds).flatMap(Collection::stream)
				.collect(Collectors.toSet()); // fetching the unique users which are associated to the schedule layouts

		List<Availability> userAvailability = availabilityDAO.findByUserIdsAndSchedulelayoutId(userIds,
				scheduleLayoutIds); // to check which user has given the availability

		userIds.removeAll(userAvailability.stream().map(e -> e.getUserId()).collect(Collectors.toList())); // fetch the
																											// users
																											// which
																											// have not
																											// given the
																											// availability

		if (userIds.size() > 0) {

			RestResponse restResponse = new RestResponse();

			restResponse.setStatus(200);
			restResponse.setSuccess(true);
			restResponse.setMessage("You have missed the deadline to publish your schedule availability.");
			restResponse.setData(userRepository.findByListOfUserIds(new ArrayList<>(userIds)));
			return ResponseEntity.status(200).body(restResponse);
		} else {

			RestResponse restResponse = new RestResponse();

			restResponse.setStatus(200);
			restResponse.setSuccess(true);
			restResponse.setMessage("No missed schedule as of now");
			restResponse.setData(null);
			return ResponseEntity.status(200).body(restResponse);
		}
	}

	@GetMapping("/reminder")
	public ResponseEntity<Object> sendReminderToUsers() {

		List<ScheduleLayout> scheduleLayouts = scheduleLayoutDAO
				.findByAvailabilityDeadlineDateAndIsPublised(DateTime.now(), false, "gte"); // fetch the schedule
																							// layouts which have passed
																							// the date

		List<Long> scheduleLayoutIds = scheduleLayouts.stream().map(ScheduleLayout::getScheduleLayoutId)
				.collect(Collectors.toList()); // converting object list to ids Long list

		List<ScheduleLayoutGroups> staffUsers = scheduleLayoutGroupsDAO.findUsersByScheduleLayoutId(scheduleLayoutIds); // find
																														// the
																														// users
																														// which
																														// are
																														// there
																														// in
																														// the
																														// schedule
																														// layout

		Set<Long> userIds = staffUsers.stream().map(ScheduleLayoutGroups::getUserIds).flatMap(Collection::stream)
				.collect(Collectors.toSet()); // fetching the unique users which are associated to the schedule layouts

		List<Availability> userAvailability = availabilityDAO.findByUserIdsAndSchedulelayoutId(userIds,
				scheduleLayoutIds); // to check which user has given the availability

		userIds.removeAll(userAvailability.stream().map(e -> e.getUserId()).collect(Collectors.toList()));

		if (userIds.size() > 0) {

			RestResponse restResponse = new RestResponse();

			restResponse.setStatus(200);
			restResponse.setSuccess(true);
			restResponse.setMessage("Reminder: Time is running out to to set your availability via Shift Planning.");
			restResponse.setData(userRepository.findByListOfUserIds(new ArrayList<>(userIds)));
			return ResponseEntity.status(200).body(restResponse);
		} else {

			RestResponse restResponse = new RestResponse();

			restResponse.setStatus(200);
			restResponse.setSuccess(true);
			restResponse.setMessage("No schedule missed.");
			restResponse.setData(null);
			return ResponseEntity.status(200).body(restResponse);
		}
	}
}