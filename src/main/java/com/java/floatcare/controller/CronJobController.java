package com.java.floatcare.controller;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import com.java.floatcare.api.response.pojo.ActiveSchedulesAndRequests;
import com.java.floatcare.api.response.pojo.RestResponse;
import com.java.floatcare.dao.CountersDAO;
import com.java.floatcare.dao.RequestsDAO;
import com.java.floatcare.dao.ScheduleDAO;
import com.java.floatcare.dao.ScheduleLayoutDAO;
import com.java.floatcare.dao.ScheduleLayoutGroupsDAO;
import com.java.floatcare.model.ScheduleLayout;
import com.java.floatcare.model.ScheduleLayoutGroups;
import com.java.floatcare.model.Schedules;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.model.UserNotification;
import com.java.floatcare.model.UserOrganizations;
import com.java.floatcare.model.UserWorkspaces;
import com.java.floatcare.model.WorksiteSettings;
import com.java.floatcare.repository.UserBasicInformationRepository;
import com.java.floatcare.repository.UserNotificationRepository;
import com.java.floatcare.repository.UserOrganizationsRepository;
import com.java.floatcare.repository.UserWorkspacesRepository;
import com.java.floatcare.repository.WorksiteSettingsRepository;

@Controller
@RequestMapping("/cron")
public class CronJobController {

	@Autowired
	private UserOrganizationsRepository userOrganizationsRepository;
	@Autowired
	private UserBasicInformationRepository userBasicInformationRepository;
	@Autowired
	private UserNotificationRepository userNotificationRepository;
	@Autowired
	private WorksiteSettingsRepository worksiteSettingsRepository;
	@Autowired
	private UserWorkspacesRepository userWorkspacesDAO;
	@Autowired
	private CountersDAO sequence;
	@Autowired
	private ScheduleDAO scheduleDAO;
	@Autowired
	private ScheduleLayoutDAO scheduleLayoutDAO;
	@Autowired
	private ScheduleLayoutGroupsDAO scheduleLayoutGroupsDAO;
	@Autowired
	private RequestsDAO requestsDAO;
	
	private long notifiationCount;

	public long getNotifiationCount() {
		return notifiationCount;
	}

	public void setNotifiationCount(long notifiationCount) {
		this.notifiationCount = notifiationCount;
	}
	
	@RequestMapping(value ="/sendnotification", method = RequestMethod.GET)
	public ResponseEntity<Object> sendBulkNotifictions() {
		
		if (notifiationCount == notifiationCount+1) {
		
			
			
			RestResponse restResponse = new RestResponse();
			
			restResponse.setMessage("");
			restResponse.setData("");
			restResponse.setStatus(200);
			restResponse.setSuccess(true);
			
			return ResponseEntity.ok(restResponse);
		}
		else {
			
			RestResponse restResponse = new RestResponse();
			
			restResponse.setMessage("");
			restResponse.setData("");
			restResponse.setStatus(200);
			restResponse.setSuccess(true);
			
			return ResponseEntity.ok(restResponse);
		}
	}
	
	@RequestMapping(value ="/updateUserOrganization", method = RequestMethod.GET)
	public ResponseEntity<Object> updateUserOrganizations() {
		
		List<UserBasicInformation> usersList = userBasicInformationRepository.findByUserTypeId(3);
		
		for (UserBasicInformation eachUser: usersList) {
			
			if (eachUser.getOrganizationId() > 0) {
				UserOrganizations user = userOrganizationsRepository.findByUserIdAndOrganizationIdAndStatus(eachUser.getUserId(), eachUser.getOrganizationId(), "Active");
				
				if (user == null) {
					
					UserOrganizations userOrganization = new UserOrganizations();
					
					userOrganization.setUserOrganizationId(sequence.getNextSequenceValue("userOrganizationId"));
					userOrganization.setOrganizationId(eachUser.getOrganizationId());
					userOrganization.setUserId(eachUser.getUserId());
					userOrganization.setStaffUserCoreTypeId(eachUser.getStaffUserCoreTypeId());
					userOrganization.setStatus("Active");
					
					userOrganizationsRepository.save(userOrganization);
				}
					
			}
			
		}

		RestResponse restResponse = new RestResponse();
		
		restResponse.setMessage("");
		restResponse.setData("");
		restResponse.setStatus(200);
		restResponse.setSuccess(true);
		
		return ResponseEntity.ok(restResponse);
	}
	
	@RequestMapping(value ="/addNotificationSettings", method = RequestMethod.GET)
	public ResponseEntity<Object> addUserNotification() {
		
		List<UserBasicInformation> usersList = userBasicInformationRepository.findByUserTypeId(3);
		
		for (UserBasicInformation eachUser: usersList) {
			
			if (userNotificationRepository.findByUserId(eachUser.getUserId()).size() == 0) {
			
				for	(int i = 1; i <= 6 ; i++) {
					
					UserNotification userNotification = new UserNotification();
					
					userNotification.setUserNotificationId(sequence.getNextSequenceValue("userNotificationId"));
					userNotification.setUserId(eachUser.getUserId());
					userNotification.setNotificationTypeId(i);
					userNotification.setEnableEmail(false);
					userNotification.setEnablePush(false);
					userNotification.setEnableSMS(false);		
				
					userNotificationRepository.save(userNotification);
				}
			}
		}
		
		
		RestResponse restResponse = new RestResponse();
		
		restResponse.setMessage("updated successfully");
		restResponse.setData("");
		restResponse.setStatus(200);
		restResponse.setSuccess(true);
		
		return ResponseEntity.ok(restResponse);
	}
	
	@RequestMapping(value ="/addWorksiteSettings", method = RequestMethod.GET)
	public ResponseEntity<Object> addWorksiteSetings() {
		
		List<UserBasicInformation> usersList = userBasicInformationRepository.findByUserTypeId(3);
		
		for (UserBasicInformation eachUser: usersList) {
			
			if (worksiteSettingsRepository.findByUserId(eachUser.getUserId()).size() == 0) {
				
				WorksiteSettings worksiteSettings = new WorksiteSettings();
				
				List<UserWorkspaces> userWorkspaces = userWorkspacesDAO.findUserWorkspacesByUserIdAndOrganizationId(eachUser.getUserId(), eachUser.getOrganizationId());
				if (userWorkspaces.size() > 0) {
					worksiteSettings.setWorksiteId(userWorkspaces.get(0).getBusinessId());
				}
				else
					continue;
				
				worksiteSettings.setColor("#FA83837");
				worksiteSettings.setUserId(eachUser.getUserId());
				worksiteSettings.setOrganizationId(eachUser.getOrganizationId());
				worksiteSettings.setWorksiteSettingId(sequence.getNextSequenceValue("worksiteSettingId"));
				
				worksiteSettingsRepository.save(worksiteSettings);
			}
		}
		
		RestResponse restResponse = new RestResponse();
		
		restResponse.setMessage("updated successfully");
		restResponse.setData("");
		restResponse.setStatus(200);
		restResponse.setSuccess(true);
		
		return ResponseEntity.ok(restResponse);
	}
	
	@GetMapping(path = "/format/phone")
	public ResponseEntity<RestResponse> formatUserPhone() {

		List<UserBasicInformation> users = userBasicInformationRepository.findAll();

		for (UserBasicInformation eachUser: users) {

			String phone = eachUser.getPhone();
			
			if (phone != null && phone.length() > 0) {
				PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
				PhoneNumber modifiedNumber = null;
				try {
					modifiedNumber = phoneUtil.parse(phone, "US");
	
				} catch (NumberParseException e) {
					continue;
				}
				phone = phoneUtil.format(modifiedNumber, PhoneNumberFormat.NATIONAL);
				eachUser.setPhone(phone);
			}else
				continue;
		}

		userBasicInformationRepository.saveAll(users);

		RestResponse response = new RestResponse();
		
		response.setMessage("phone changed successfully.");
		response.setData(null);
		response.setSuccess(true);
		response.setStatus(200);
		
		return ResponseEntity.ok(response);
	}
	
	@GetMapping(value = "/user/date")
	public ResponseEntity<RestResponse> updateCreatedOnDateOfUsers() {
		
		List<UserBasicInformation> users = userBasicInformationRepository.findAll();
		
		for (UserBasicInformation eachUser : users) {
			
			if (eachUser.getCreatedOn() == null) {
				List<UserWorkspaces> user = userWorkspacesDAO.findUserWorkspacesByUserIdAndOrganizationId(eachUser.getUserId(), eachUser.getOrganizationId());
				
				if (user.size()>0) {
					eachUser.setCreatedOn(user.get(0).getHiredOn());
					userBasicInformationRepository.save(eachUser);
				}
			}
		}

		RestResponse response = new RestResponse();
		
		response.setMessage("createOn changed successfully.");
		response.setData(users.size());
		response.setSuccess(true);
		response.setStatus(200);
		
		return ResponseEntity.ok(response);
	}

	@GetMapping(value = "/user/validate/shiftType")
	public ResponseEntity<RestResponse> validateShiftTypeOfUser(@RequestParam(value = "userId", required = true)  Long userId, 
				@RequestParam(value = "departmentShiftId", required = true) Long departmentShiftId) {

		List<Schedules> schedulesAndShiftPlanning = scheduleDAO.findByUserIdAndShiftTypeIdOrDepartmentId(userId, departmentShiftId, 0);

		List<ScheduleLayoutGroups> scheduleLayoutGroups = scheduleLayoutGroupsDAO.findScheduleLayoutByUserId(userId);
		
		List<ScheduleLayout> scheduleLayout = scheduleLayoutDAO.findByScheduleLayoutIdsAndShiftPlanningDate(scheduleLayoutGroups.stream()
									.map(e->e.getScheduleLayoutId()).collect(Collectors.toList()), LocalDate.now(), 0, departmentShiftId);
		
		ActiveSchedulesAndRequests activeSchedulesAndRequests = new ActiveSchedulesAndRequests();

		activeSchedulesAndRequests.setActiveSchedules(schedulesAndShiftPlanning);
		activeSchedulesAndRequests.setActiveScheduleLayouts(scheduleLayout);
		activeSchedulesAndRequests.setActiveRequests(requestsDAO.findByUserIdAndShiftTypeIdOrDepartmentId(userId, departmentShiftId, Long.valueOf(0)));

		RestResponse response = new RestResponse();

		response.setSuccess(true);
		response.setStatus(200);
		response.setData(activeSchedulesAndRequests);

		return ResponseEntity.ok(response);
	}

	@GetMapping(value = "/user/validate/department")
	public ResponseEntity<RestResponse> validateDepartmentOfUser(@RequestParam(value = "userId", required = true)  Long userId, 
				@RequestParam(value = "departmentId", required = true) Long departmentId) {

		List<Schedules> schedulesAndShiftPlanning = scheduleDAO.findByUserIdAndShiftTypeIdOrDepartmentId(userId, 0, departmentId); // fetch the schedules

		List<ScheduleLayoutGroups> scheduleLayoutGroups = scheduleLayoutGroupsDAO.findScheduleLayoutByUserId(userId); // fetch the shiftPlanning

		List<ScheduleLayout> scheduleLayout = scheduleLayoutDAO.findByScheduleLayoutIdsAndShiftPlanningDate(scheduleLayoutGroups.stream()
				.map(e->e.getScheduleLayoutId()).collect(Collectors.toList()), LocalDate.now(), departmentId, 0);

		ActiveSchedulesAndRequests activeSchedulesAndRequests = new ActiveSchedulesAndRequests();

		activeSchedulesAndRequests.setActiveSchedules(schedulesAndShiftPlanning);
		activeSchedulesAndRequests.setActiveScheduleLayouts(scheduleLayout);
		activeSchedulesAndRequests.setActiveRequests(requestsDAO.findByUserIdAndShiftTypeIdOrDepartmentId(userId, Long.valueOf(0), departmentId));

		RestResponse response = new RestResponse();

		response.setSuccess(true);
		response.setStatus(200);
		response.setData(activeSchedulesAndRequests);

		return ResponseEntity.ok(response);
	}
}