package com.java.floatcare.controller;

import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.i18n.phonenumbers.NumberParseException;
import com.java.floatcare.api.request.pojo.ChangeEmailRequest;
import com.java.floatcare.api.request.pojo.EmailPhoneVerificationRequest;
import com.java.floatcare.api.response.pojo.OtpEmailResponse;
import com.java.floatcare.api.response.pojo.RestResponse;
import com.java.floatcare.communication.MailUtils;
import com.java.floatcare.dao.CountersDAO;
import com.java.floatcare.dao.EmailVerificationDAO;

import com.java.floatcare.dao.UserBasicInformationRepositoryDAO;
import com.java.floatcare.model.EmailVerification;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.repository.EmailVerificationRepository;
import com.java.floatcare.repository.UserBasicInformationRepository;
import com.twilio.exception.ApiException;

@RestController
@RequestMapping("/verify")
public class VerifyUserEmail {

	@Autowired
	private UserBasicInformationRepository userBasicInformationRepository;

	@Autowired
	private MailUtils mailUtils;

	@Autowired
	private EmailVerificationRepository emailVerificationRepository;

	@Autowired
	private CountersDAO sequence;
	
	@Autowired
	EmailVerificationDAO emailVerificationDAO;
	
	@Autowired
	UserBasicInformationRepositoryDAO findUserDAO;

	@RequestMapping(value = "/verifyEmail", method = RequestMethod.POST)
	public ResponseEntity<Object> sendOTP(@RequestBody EmailPhoneVerificationRequest emailRequest)
			throws JSONException {

		EmailVerification emailVerification = new EmailVerification();
		emailVerification.setEmailId(sequence.getNextSequenceValue("emailId"));
		emailVerification.setEmail(emailRequest.getEmail());

		Random random = new Random();
		String id = String.format("%04d", random.nextInt(10000));

		emailVerification.setOtp(id);
		emailVerification.setExpiryTime(System.currentTimeMillis() + 600000);

		OtpEmailResponse otpEmailResponse = new OtpEmailResponse();
		
		RestResponse restResponse = new RestResponse();
		
		if (emailRequest.getEmail() != null) {
			
			otpEmailResponse.setEmail(emailRequest.getEmail());
			otpEmailResponse.setOtp(id);
			
			try {
				mailUtils.sendMail(emailRequest.getEmail(), "Change Primary Email", "OTP to change your primary email is: " +id);
				
				restResponse.setStatus(200);
				restResponse.setSuccess(true);
				restResponse.setMessage("OTP Sent Successfully");
				restResponse.setData(otpEmailResponse);
				
				emailVerificationRepository.save(emailVerification);
				return ResponseEntity.ok(restResponse);
				
			} catch (ApiException ae) {
				
				restResponse.setStatus(400);
				restResponse.setSuccess(false);
				restResponse.setMessage(ae.getMessage());
				restResponse.setData("");
				
				return ResponseEntity.status(400).body(restResponse);
				
			}
		} else {
			restResponse.setStatus(400);
			restResponse.setSuccess(false);
			restResponse.setMessage("OTP Not Generated, Check Your Email Address");
			restResponse.setData("");
			return ResponseEntity.status(400).body(restResponse);
		}
	}

	@RequestMapping(value = "/verifyEmailOtp", method = RequestMethod.POST)
	public ResponseEntity<Object> verifyOTP(@RequestBody OtpEmailResponse verifyOtpRequest) throws JSONException {

		List<EmailVerification> email= null;
		RestResponse restResponse = new RestResponse();
		
		try {
		email = emailVerificationDAO.findByOtpAndEmail(verifyOtpRequest.getEmail(), verifyOtpRequest.getOtp());
		} catch(NumberParseException np) {
			restResponse.setStatus(200);
			restResponse.setSuccess(true);
			restResponse.setMessage("Invalid phone number has given.");
			restResponse.setData("");
			return ResponseEntity.ok(restResponse);
		}
		
			if (email.size() > 0) {
				if (email.get(0).getExpiryTime() >= System.currentTimeMillis()) {
					if (email.get(0).getOtp().equals(verifyOtpRequest.getOtp())) {

						UserBasicInformation user = findUserDAO.findByEmailOrPhone(email.get(0).getEmail());
						restResponse.setStatus(200);
						restResponse.setSuccess(true);
						restResponse.setMessage("OTP verified successfully.");
						restResponse.setData(user.getUserId());
						return ResponseEntity.ok(restResponse);
					} else {
						restResponse.setStatus(400);
						restResponse.setSuccess(false);
						restResponse.setMessage("Invalid OTP given.");
						restResponse.setData("");
						return ResponseEntity.status(403).body(restResponse.toString());
					}
				} else {
					emailVerificationRepository.delete(email.get(0));
					restResponse.setStatus(403);
					restResponse.setSuccess(false);
					restResponse.setMessage("OTP Expired, Please Send The OTP Again.");
					restResponse.setData("");
					return ResponseEntity.status(403).body(restResponse);
				}

			} else {
				restResponse.setStatus(400);
				restResponse.setSuccess(false);
				restResponse.setMessage("Invalid OTP given.");
				restResponse.setData("");
				return ResponseEntity.status(403).body(restResponse);
		}
	}
	
	@RequestMapping(value = "/changeEmail", method = RequestMethod.POST)
	public ResponseEntity<Object> changePhoneNumber(@RequestBody ChangeEmailRequest changeEmailRequest) throws Exception {

		List<EmailVerification> email = emailVerificationDAO.findByOtpAndEmail(changeEmailRequest.getEmail(), changeEmailRequest.getOtp());
		RestResponse restResponse = new RestResponse();
		
		if (email.size() > 0) {
			if (email.get(0).getExpiryTime() >= System.currentTimeMillis()) {
				if (email.get(0).getOtp().equals(changeEmailRequest.getOtp())) {
					
					emailVerificationRepository.delete(email.get(0));
					
					List<UserBasicInformation> userOptional = findUserDAO.findUserById(changeEmailRequest.getUserId());
						if(userOptional.size() > 0) {
							userOptional.get(0).setEmail(changeEmailRequest.getEmail());
							userBasicInformationRepository.save(userOptional.get(0));
					}
					  else 
						  throw new Exception("User Id does not exist in Database");
					 
					
					restResponse.setStatus(200);
					restResponse.setSuccess(true);
					restResponse.setMessage("Email changed successfully.");
					restResponse.setData("");
					return ResponseEntity.ok(restResponse);
				} else {
					restResponse.setStatus(400);
					restResponse.setSuccess(false);
					restResponse.setMessage("Invalid OTP given.");
					restResponse.setData("");
					return ResponseEntity.status(403).body(restResponse.toString());
				}
			} else {
				emailVerificationRepository.delete(email.get(0));
				restResponse.setStatus(403);
				restResponse.setSuccess(false);
				restResponse.setMessage("OTP Expired, Please Send The OTP Again.");
				restResponse.setData("");
				return ResponseEntity.status(403).body(restResponse);
			}

		} else {
			restResponse.setStatus(403);
			restResponse.setSuccess(false);
			restResponse.setMessage("Given Email doesn't exist.");
			restResponse.setData("");
			return ResponseEntity.status(403).body(restResponse);
		}
	}
}
