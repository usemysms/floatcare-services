package com.java.floatcare.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.java.floatcare.api.request.pojo.EmailPhoneVerificationRequest;
import com.java.floatcare.api.request.pojo.EmailWithOrganiationVerificationRequest;
import com.java.floatcare.api.request.pojo.SMSRequest;
import com.java.floatcare.api.request.pojo.SendMailRequest;
import com.java.floatcare.api.response.pojo.ResponseError;
import com.java.floatcare.api.response.pojo.RestResponse;
import com.java.floatcare.communication.MailUtils;
import com.java.floatcare.communication.TwilioCommunication;
import com.java.floatcare.model.Businesses;
import com.java.floatcare.model.Department;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.repository.BusinessesRepository;
import com.java.floatcare.repository.DepartmentRepository;
import com.java.floatcare.repository.UserBasicInformationRepository;
import com.java.floatcare.utils.EmailDomainValidation;

@RestController
@RequestMapping("/verify")
public class VerifyEmail {
	
	@Autowired
	private UserBasicInformationRepository userBasicInformationRepository;
	@Autowired
	private BusinessesRepository businessesRepository;
	@Autowired
	private DepartmentRepository departmentRepository;
	@Autowired
	private MailUtils mailUtils;
	@Autowired
	private TwilioCommunication twilioCommunication;
	
	@RequestMapping(value = "/email", method = RequestMethod.POST)
	public ResponseEntity<Object> verifyUserEmail(@RequestBody EmailPhoneVerificationRequest emailRequest){
		
		if (emailRequest.getEmail().contains("@") && new EmailDomainValidation().validateEmailDomain(emailRequest.getEmail().split("@")[1])) {
			
			Optional<UserBasicInformation> user = userBasicInformationRepository.findUserByEmail(emailRequest.getEmail());
			
			if(user.isPresent()) {
				RestResponse restResponse = new RestResponse();
				restResponse.setMessage(emailRequest.getEmail() +" is already exists.");
				restResponse.setData(user);
				restResponse.setStatus(403);
				restResponse.setSuccess(false);
				return ResponseEntity.status(403).body(restResponse);
			}
			else {
				RestResponse restResponse = new RestResponse();
				restResponse.setMessage(emailRequest.getEmail() +" is available to use.");
				restResponse.setStatus(200);
				restResponse.setSuccess(true);
				return ResponseEntity.status(200).body(restResponse);
			}
		}else {
			RestResponse restResponse = new RestResponse();
			restResponse.setMessage(emailRequest.getEmail() +" is not a valid email.");
			restResponse.setData(null);
			restResponse.setStatus(403);
			restResponse.setSuccess(false);
			return ResponseEntity.status(403).body(restResponse);
		}
		
	}

	@RequestMapping(value = "/checkUser", method = RequestMethod.POST)
	public ResponseEntity<Object> checkUser(@RequestBody EmailPhoneVerificationRequest emailRequest){
				Optional<UserBasicInformation> user = userBasicInformationRepository.findUserByEmail(emailRequest.getEmail());
		if(user.isPresent()) {
			RestResponse restResponse = new RestResponse();
			restResponse.setMessage(emailRequest.getEmail() +" is available");
			restResponse.setData(user);
			restResponse.setStatus(200);
			restResponse.setSuccess(false);
			return ResponseEntity.status(200).body(restResponse);
		}
		else {
			RestResponse restResponse = new RestResponse();
			restResponse.setMessage(emailRequest.getEmail() +" is not available to use.");
			restResponse.setStatus(403);
			restResponse.setSuccess(true);
			return ResponseEntity.status(403).body(restResponse);
		}
			
	}
	@RequestMapping(value = "/checkUserByOrganization", method = RequestMethod.POST)
	public ResponseEntity<Object> checkUserByOrganization(@RequestBody EmailWithOrganiationVerificationRequest emailRequest){
		List<UserBasicInformation> user = userBasicInformationRepository.findUsersByEmailAndOrganizationId(emailRequest.getEmail(), emailRequest.getOrganizationId());
		
		if(user.size()>0) {
			RestResponse restResponse = new RestResponse();
			restResponse.setMessage(emailRequest.getEmail() +" is available");
			restResponse.setData(user.get(0));
			restResponse.setStatus(200);
			restResponse.setSuccess(false);
			return ResponseEntity.status(200).body(restResponse);
		}
		else {
			RestResponse restResponse = new RestResponse();
			restResponse.setMessage(emailRequest.getEmail() +" is not available to use.");
			restResponse.setStatus(403);
			restResponse.setSuccess(true);
			return ResponseEntity.status(403).body(restResponse);
		}
			
	}
	@RequestMapping(value = "/business/email", method = RequestMethod.POST)
	public ResponseEntity<Object> verifyBusinessEmail(@RequestBody EmailPhoneVerificationRequest emailRequest){
		List<Businesses> businesses = businessesRepository.findBusinessByEmail(emailRequest.getEmail());
		if(businesses.size()>0) {
			return ResponseEntity.status(400).body(new ResponseError("Business : " +businesses.get(0).getEmail()+ " Exists in Database",""));
		}
		else
			return new ResponseEntity<>("", HttpStatus.OK);
	}
	@RequestMapping(value = "/department/email", method = RequestMethod.POST)
	public ResponseEntity<Object> verifyDepartmentEmail(@RequestBody EmailPhoneVerificationRequest emailRequest){
				List<Department> department = departmentRepository.findDepartmentByEmail(emailRequest.getEmail());
		if(department.size()>0) {
			return ResponseEntity.status(400).body(new ResponseError("Department: " +department.get(0).getEmail()+ " Exists in Database",""));
		}
		else
			return new ResponseEntity<>("", HttpStatus.OK);
	}
	
	@RequestMapping(value = "/sendEmail", method = RequestMethod.POST)
	public ResponseEntity<Object> sendMail(@RequestBody SendMailRequest emailRequest){
		if(emailRequest.getTo()!="" && emailRequest.getFrom()!="") {
			
			mailUtils.sendMail(emailRequest.getTo(), emailRequest.getSubject(), emailRequest.getBody());
			RestResponse restResponse = new RestResponse();
			restResponse.setMessage("Mail sent successully.");
			restResponse.setStatus(200);
			restResponse.setSuccess(true);
			return ResponseEntity.status(200).body(restResponse);
		}
		else {
			RestResponse restResponse = new RestResponse();
			restResponse.setMessage(emailRequest.getTo() +" is available to use.");
			restResponse.setStatus(400);
			restResponse.setSuccess(true);
			return ResponseEntity.status(400).body(restResponse);
		}
		}
	@RequestMapping(value = "/sendSMS", method = RequestMethod.POST)
	public ResponseEntity<Object> sendSMS(@RequestBody SMSRequest smsRequest){
		if(smsRequest.getMessageText()!="" && smsRequest.getPhoneNumber()!="") {
			String sid = twilioCommunication.sendSMS(smsRequest.getPhoneNumber(), smsRequest.getMessageText());
			if(sid!="") {
			RestResponse restResponse = new RestResponse();
			restResponse.setMessage("SMS sent successully.");
			restResponse.setStatus(200);
			restResponse.setSuccess(true);
			return ResponseEntity.status(200).body(restResponse);

			} else {
				RestResponse restResponse = new RestResponse();
				restResponse.setMessage("Unable to send SMS due to invalid details are provided");
				restResponse.setStatus(400);
				restResponse.setSuccess(true);
				return ResponseEntity.status(400).body(restResponse);
				
			}
		}
		else {
			RestResponse restResponse = new RestResponse();
			restResponse.setMessage("Invalid details are provided");
			restResponse.setStatus(400);
			restResponse.setSuccess(true);
			return ResponseEntity.status(400).body(restResponse);
		}
	}
}
