package com.java.floatcare.controller;

import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import com.java.floatcare.api.request.pojo.ChangePhoneNumberRequest;
import com.java.floatcare.api.request.pojo.OtpNumberResponse;
import com.java.floatcare.api.request.pojo.PhoneNumberVerificationRequest;
import com.java.floatcare.api.response.pojo.RestResponse;
import com.java.floatcare.communication.TwilioCommunication;
import com.java.floatcare.dao.CountersDAO;
import com.java.floatcare.dao.PhoneVerificationDAO;

import com.java.floatcare.dao.UserBasicInformationRepositoryDAO;
import com.java.floatcare.model.PhoneVerification;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.repository.PhoneVerificationRepository;
import com.java.floatcare.repository.UserBasicInformationRepository;
import com.twilio.exception.ApiException;

@RestController
@RequestMapping("/verify")
public class VerifyPhoneNumber {

	@Autowired
	private UserBasicInformationRepository userBasicInformationRepository;

	@Autowired
	private TwilioCommunication twilioCommunication;

	@Autowired
	private PhoneVerificationRepository phoneVerificationRepository;

	@Autowired
	private CountersDAO sequence;
	
	@Autowired
	PhoneVerificationDAO phoneVerificationDAO;
	
	@Autowired
	UserBasicInformationRepositoryDAO findUserDAO;

	@RequestMapping(value = "/phone/{phoneNumber}", method = RequestMethod.GET)
	public ResponseEntity<RestResponse> verifyUserPhoneNumber(@PathVariable("phoneNumber") String phoneNumber) {

		String phone = phoneNumber;

		PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
		PhoneNumber modifiedNumber = null;
		try {
			modifiedNumber = phoneUtil.parse(phone, "US");

		} catch (NumberParseException e) {
			
			RestResponse response = new RestResponse();
			
			response.setMessage(e.getLocalizedMessage());
			response.setData(null);
			response.setStatus(422);
			response.setSuccess(false);
			
			return ResponseEntity.status(422).body(response);
		}

		phone = phoneUtil.format(modifiedNumber, PhoneNumberFormat.NATIONAL);

		UserBasicInformation user = userBasicInformationRepository.findFirstUserByPhone(phone);
		
		if (user != null) {
			
			RestResponse response = new RestResponse();
			
			response.setMessage("User already exist with the number "+phoneNumber);
			response.setData(null);
			response.setStatus(409);
			response.setSuccess(false);

			return ResponseEntity.status(409).body(response);
		} else {
			RestResponse response = new RestResponse();
			
			response.setMessage("Phone number can be used.");
			response.setData(null);
			response.setStatus(200);
			response.setSuccess(true);
			return ResponseEntity.ok(response);
		}
	}

	@RequestMapping(value = "/verifyPhone", method = RequestMethod.POST)
	public ResponseEntity<Object> sendOTP(@RequestBody PhoneNumberVerificationRequest phoneNumberRequest)
			throws JSONException {

		PhoneVerification phoneVerification = new PhoneVerification();
		phoneVerification.setPhoneId(sequence.getNextSequenceValue("phoneId"));
		phoneVerification.setPhoneNumber(phoneNumberRequest.getPhoneNumber());

		Random random = new Random();
		String id = String.format("%04d", random.nextInt(10000));

		phoneVerification.setOtp(id);
		phoneVerification.setExpiryTime(System.currentTimeMillis() + 600000);

		OtpNumberResponse otpNumberResponse = new OtpNumberResponse();
		
		RestResponse restResponse = new RestResponse();
		if (phoneNumberRequest.getPhoneNumber().length() > 10) {
			otpNumberResponse.setPhoneNumber(phoneNumberRequest.getPhoneNumber());
			otpNumberResponse.setOtp(id);
			restResponse.setStatus(200);
			restResponse.setSuccess(true);
			restResponse.setMessage("OTP Sent Successfully");
			restResponse.setData(otpNumberResponse);
			try {
			twilioCommunication.sendSMS(phoneNumberRequest.getPhoneNumber(), "Your OTP is " + id);
			} catch (ApiException ae) {
				restResponse.setStatus(400);
				restResponse.setSuccess(false);
				restResponse.setMessage(ae.getMessage());
				restResponse.setData("");
				
			}
			phoneVerificationRepository.save(phoneVerification);
			return ResponseEntity.ok(restResponse);

		} else {
			restResponse.setStatus(400);
			restResponse.setSuccess(false);
			restResponse.setMessage("OTP Not Generated, Check Your Phone Number");
			restResponse.setData("");
			return ResponseEntity.status(400).body(restResponse);
		}

	}

	@RequestMapping(value = "/verifyOtp", method = RequestMethod.POST)
	public ResponseEntity<Object> verifyOTP(@RequestBody OtpNumberResponse verifyOtpRequest) throws JSONException {

		List<PhoneVerification> phone = phoneVerificationDAO.findByOtpAndPhoneNumber(verifyOtpRequest.getPhoneNumber(), verifyOtpRequest.getOtp());
		RestResponse restResponse = new RestResponse();
		
			if (phone.size() > 0) {
				if (phone.get(0).getExpiryTime() >= System.currentTimeMillis()) {
					if (phone.get(0).getOtp().equals(verifyOtpRequest.getOtp())) {
						phoneVerificationRepository.delete(phone.get(0));

						restResponse.setStatus(200);
						restResponse.setSuccess(true);
						restResponse.setMessage("OTP verified successfully.");
						restResponse.setData("");
						return ResponseEntity.ok(restResponse);
					} else {
						restResponse.setStatus(400);
						restResponse.setSuccess(false);
						restResponse.setMessage("Invalid OTP given.");
						restResponse.setData("");
						return ResponseEntity.status(403).body(restResponse.toString());
					}
				} else {
					phoneVerificationRepository.delete(phone.get(0));
					restResponse.setStatus(403);
					restResponse.setSuccess(false);
					restResponse.setMessage("OTP Expired, Please Send The OTP Again.");
					restResponse.setData("");
					return ResponseEntity.status(403).body(restResponse);
				}

			} else {
				restResponse.setStatus(403);
				restResponse.setSuccess(false);
				restResponse.setMessage("Given mobile number doesn't exist.");
				restResponse.setData("");
				return ResponseEntity.status(403).body(restResponse);
			}
		
	}
	
	@RequestMapping(value = "/changePhoneNumber", method = RequestMethod.POST)
	public ResponseEntity<Object> changePhoneNumber(@RequestBody ChangePhoneNumberRequest changePhoneNumberRequest) throws Exception {

		List<PhoneVerification> phone = phoneVerificationDAO.findByOtpAndPhoneNumber(changePhoneNumberRequest.getPhoneNumber(), changePhoneNumberRequest.getOtpString());
		RestResponse restResponse = new RestResponse();
		
			if (phone.size() > 0) {
				if (phone.get(0).getExpiryTime() >= System.currentTimeMillis()) {
					if (phone.get(0).getOtp().equals(changePhoneNumberRequest.getOtpString())) {
						
						phoneVerificationRepository.delete(phone.get(0));
						
						List<UserBasicInformation> userOptional = findUserDAO.findUserById(changePhoneNumberRequest.getUserId());
						
						if(userOptional.size()>0) {
							userOptional.get(0).setPhone(changePhoneNumberRequest.getPhoneNumber());
							userBasicInformationRepository.save(userOptional.get(0));
						}
						 else 
							throw new Exception("User Id does not exist in Database");
						 
						
						restResponse.setStatus(200);
						restResponse.setSuccess(true);
						restResponse.setMessage("Phone number changed successfully.");
						restResponse.setData("");
						return ResponseEntity.ok(restResponse);
					} else {
						restResponse.setStatus(400);
						restResponse.setSuccess(false);
						restResponse.setMessage("Invalid OTP given.");
						restResponse.setData("");
						return ResponseEntity.status(403).body(restResponse.toString());
					}
				} else {
					phoneVerificationRepository.delete(phone.get(0));
					restResponse.setStatus(403);
					restResponse.setSuccess(false);
					restResponse.setMessage("OTP Expired, Please Send The OTP Again.");
					restResponse.setData("");
					return ResponseEntity.status(403).body(restResponse);
				}

			} else {
				restResponse.setStatus(403);
				restResponse.setSuccess(false);
				restResponse.setMessage("Given mobile number doesn't exist.");
				restResponse.setData("");
				return ResponseEntity.status(403).body(restResponse);
			}
		
	}

}
