package com.java.floatcare.controller;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.chrono.ChronoLocalDate;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.java.floatcare.api.response.pojo.CredentialsReportResponse;
import com.java.floatcare.api.response.pojo.ProfessionalCredentialReport;
import com.java.floatcare.api.response.pojo.RestResponse;
import com.java.floatcare.dao.UserBasicInformationRepositoryDAO;
import com.java.floatcare.dao.UserCredentialsDAO;
import com.java.floatcare.dao.UserWorkspacesDAO;
import com.java.floatcare.model.Department;
import com.java.floatcare.model.StaffUserCoreTypes;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.model.UserCredentials;
import com.java.floatcare.model.UserWorkspaces;
import com.java.floatcare.repository.DepartmentRepository;
import com.java.floatcare.repository.StaffUserCoreTypesRepository;
import com.java.floatcare.repository.UserBasicInformationRepository;
import com.java.floatcare.utils.EmailThread;
import com.java.floatcare.utils.GoogleFirebaseUtilityDAO;
import com.java.floatcare.utils.InAppNotifications;

@RestController
@RequestMapping("/credentials")
public class CredentialsController {

	@Autowired
	UserWorkspacesDAO userWorkspaceDAO;
	@Autowired
	UserBasicInformationRepositoryDAO userBasicInformationDAO;
	@Autowired
	UserCredentialsDAO userCredentialsDAO;
	@Autowired
	StaffUserCoreTypesRepository staffUserCoreTypesRepository;
	@Autowired
	DepartmentRepository departmentRepository;
	@Autowired
	private InAppNotifications inAppNotification;
	@Autowired
	private GoogleFirebaseUtilityDAO pushNotification;
	@Autowired
	private UserBasicInformationRepository userRepository;
	@Autowired
	private ApplicationContext applicationContext;

	@RequestMapping(value = "/report/{byType}/{byId}", method = RequestMethod.GET)
	public ResponseEntity<Object> getCredentialReport(@PathVariable String byType, @PathVariable long byId) {

		CredentialsReportResponse report = new CredentialsReportResponse();

		List<UserWorkspaces> users = userWorkspaceDAO.findUserIdsByDepartmentIdAndNonPrimary(byId);
		List<StaffUserCoreTypes> staffCoreTypes = staffUserCoreTypesRepository.findAll();

		Optional<Department> departmentOptional = departmentRepository.findById(byId);
		Department deparmentObj = (departmentOptional.isPresent()) ? departmentOptional.get() : null;
		String deparment = (deparmentObj != null) ? deparmentObj.getDepartmentTypeName() : "";

		List<Long> userIds = users.stream().map(UserWorkspaces::getUserId).collect(Collectors.toList());
		// System.out.println("List of users :"+byId+" : "+userIds);
		Set<Long> setWithUniqueValues = new HashSet<>(userIds);
		userIds = new ArrayList<>(setWithUniqueValues);
		// System.out.println(userIds);
		// user basic information objects
		List<UserBasicInformation> userBasicInformation = userBasicInformationDAO.findByListOfUserIds(userIds);
		// user workspace objects
		// user credentials objects
		List<UserCredentials> userCredentials = userCredentialsDAO.findUserCredentials(userIds);
		// System.out.println(userCredentials);
		long verified = 0;
		long pending = 0;
		long expiringSoon = 0;
		long expired = 0;
		long lastYearVerified = 0;
		long lastYearPending = 0;
		long lastYearExpiringSoon = 0;
		long lastYearExpired = 0;
		ChronoLocalDate dt = LocalDate.from(ZonedDateTime.now());
		List<ProfessionalCredentialReport> professionalCredentialReportList = new ArrayList<ProfessionalCredentialReport>();
		for (UserWorkspaces user : users) {
			ProfessionalCredentialReport professionalCredentialReport = new ProfessionalCredentialReport();
			professionalCredentialReport.setDepartment(deparment);
			professionalCredentialReport.setStaffUserCoreTypeName(staffCoreTypes.stream()
					.filter(each -> each.getStaffUserCoreTypesId() == user.getStaffUserCoreTypeId()).findFirst().get()
					.getLabel());

			Optional<UserBasicInformation> optionalUser = userBasicInformation.stream()
					.filter(eachUserBasicInformation -> eachUserBasicInformation.getUserId() == user.getUserId())
					.findFirst();
			if (optionalUser.isPresent()) {
				professionalCredentialReport
						.setName(optionalUser.get().getFirstName() + " " + optionalUser.get().getLastName());
				professionalCredentialReport.setProfilePhoto(optionalUser.get().getProfilePhoto());
				professionalCredentialReport.setUserId(optionalUser.get().getUserId());
			}

			List<UserCredentials> eachUserCredentialList = userCredentials.stream()
					.filter(eachCredentials -> eachCredentials.getUserId() == user.getUserId())
					.collect(Collectors.toList());

			if (eachUserCredentialList != null && eachUserCredentialList.size() > 0) {

				long tempv = eachUserCredentialList.stream().filter(each -> each.isVerified() == true).count();
				professionalCredentialReport.setVerified(tempv);
				verified = verified + tempv;
				List<UserCredentials> eachUserCredentialList1 = eachUserCredentialList.stream()
						.filter(each -> each.getVerifiedOn() != null).collect(Collectors.toList());

                lastYearVerified = lastYearVerified
                        + eachUserCredentialList1.stream().filter(each -> each.isVerified() == true
                                && each.getVerifiedOn().getYear() == dt.get(ChronoField.YEAR) - 1).count();

				long tempp = eachUserCredentialList.stream().filter(each -> each.isVerified() == false).count();
				professionalCredentialReport.setPending(tempp);
				pending = pending + tempp;
				List<UserCredentials> eachUserCredentialList2 = eachUserCredentialList.stream()
						.filter(each -> each.getCreatedOn() != null).collect(Collectors.toList());
				lastYearPending = lastYearPending
						+ eachUserCredentialList2.stream().filter(each -> each.isVerified() == false
								&& each.getCreatedOn().getYear() == dt.get(ChronoField.YEAR) - 1).count();

				List<UserCredentials> eachUserCredentialList3 = eachUserCredentialList.stream()
						.filter(each -> each.getExpirationDate() != null).collect(Collectors.toList());
				long tempe = eachUserCredentialList3.stream().filter(each -> each.getExpirationDate().isBefore(dt))
						.count();
				professionalCredentialReport.setExpired(tempe);
				expired = expired + tempe;
				if (eachUserCredentialList3 != null) {
					lastYearExpired = lastYearExpired + eachUserCredentialList3.stream()
							.filter(each -> (each != null && each.getCreatedOn() != null
									&& each.getExpirationDate() != null) && each.getExpirationDate().isBefore(dt)
									&& each.getCreatedOn().getYear() == dt.get(ChronoField.YEAR) - 1)
							.count();
				}
				long tempes = eachUserCredentialList3.stream().filter(each -> each.getExpirationDate().isAfter(dt))
						.count();
				professionalCredentialReport.setExpiringSoon(tempes);
				expiringSoon = expiringSoon + tempes;

				lastYearExpiringSoon = lastYearExpiringSoon + eachUserCredentialList3.stream()
						.filter(each -> each.getExpirationDate().getYear() == dt.get(ChronoField.YEAR) - 1).count();
			}
			professionalCredentialReportList.add(professionalCredentialReport);
		}

		report.setExpired(expired);
		report.setExpiringSoon(expiringSoon);
		report.setPending(pending);
		report.setVerified(verified);
		report.setLastYearExpired(lastYearExpired);
		report.setLastYearExpiringSoon(lastYearExpiringSoon);
		report.setLastYearPending(lastYearPending);
		report.setLastYearVerified(lastYearVerified);

		if (expired > lastYearExpired && lastYearExpired > 0)
			report.setExpiredDiff(Math.round(((expired - lastYearExpired) / lastYearExpired) * 100));
		else if (expired < lastYearExpired)
			report.setExpiredDiff(Math.round(lastYearExpired - expired / lastYearExpired * 100));
		else
			report.setExpiredDiff(0);

		if (expiringSoon > lastYearExpiringSoon && lastYearExpiringSoon > 0)
			report.setExpiringSoonDiff(
					Math.round(((expiringSoon - lastYearExpiringSoon) / lastYearExpiringSoon) * 100));
		else if (expiringSoon < lastYearExpiringSoon)
			report.setExpiringSoonDiff(Math.round(lastYearExpiringSoon - expired / lastYearExpiringSoon * 100));
		else
			report.setExpiringSoonDiff(0);

		if (pending > lastYearPending && lastYearPending > 0)
			report.setPendingDiff(Math.round(((pending - lastYearPending) / lastYearPending) * 100));
		else if (pending < lastYearPending && lastYearPending > 0)
			report.setPendingDiff(Math.round(lastYearPending - pending / lastYearPending * 100));
		else
			report.setPendingDiff(0);

		if (verified > lastYearVerified && lastYearVerified > 0)
			report.setVerifiedDiff(Math.round(((verified - lastYearVerified) / lastYearVerified) * 100));
		else if (verified < lastYearVerified && lastYearVerified > 0)
			report.setVerifiedDiff(Math.round(lastYearVerified - verified / lastYearVerified * 100));
		else
			report.setVerifiedDiff(0);

		report.setProfessionalCredentialReport(professionalCredentialReportList);

		RestResponse restResponse = new RestResponse();

		restResponse.setMessage("");
		restResponse.setData(report);
		restResponse.setStatus(200);
		restResponse.setSuccess(true);

		return ResponseEntity.ok(restResponse);
	}

	@GetMapping("/expiry/{noOfDays}")
	public ResponseEntity<RestResponse> findExpiredCrdentialsOfUsers(@PathVariable(name = "noOfDays") int noOfDays) {

		LocalDate checkExpiryDate = LocalDate.now().plusDays(noOfDays);

		Set<Long> userExpiredCredentials = userCredentialsDAO.findUserCredentialsByExpiredDate(checkExpiryDate).stream().map(e->e.getUserId()).collect(Collectors.toSet());

		if (userExpiredCredentials.size() > 0) {
			for (Long eachUser : userExpiredCredentials) {

				String message = "Don't forget to add your credentials!";
				String messageBody = ", please update your credentials in Float Care!";
				String deepLink = "10	";
				
				UserBasicInformation user = userRepository.findUsersByUserId(eachUser);

				if (user != null) {
					messageBody = user.getFirstName()+messageBody;
					
					if(user.getFcmToken() != null)
						pushNotification.sendEventNotificationToUser(eachUser, user.getFcmToken(), message, messageBody, deepLink, user.getDeviceType(), null);
					
					inAppNotification.createInAppNotifications(eachUser, 3, message, messageBody, Long.getLong(deepLink), false, 0);

					EmailThread emailThread = new EmailThread(); // sending email
					emailThread.setEmails(user.getEmail());
					emailThread.setMessage(message);
					emailThread.setMessageBody(messageBody);
					applicationContext.getAutowireCapableBeanFactory().autowireBean(emailThread);

					Thread thread = new Thread(emailThread); // send email on separate thread.
					thread.start(); // start the thread
				}
			}
			
			RestResponse response = new RestResponse();
			response.setData(null);
			response.setMessage("List of user's expired credentials");
			response.setStatus(200);
			response.setSuccess(true);

			return ResponseEntity.ok(response);
		} else {

			RestResponse response = new RestResponse();
			response.setData(null);
			response.setMessage("No expired credentials found.");
			response.setStatus(200);
			response.setSuccess(true);

			return ResponseEntity.ok(response);
		}
	}

	@GetMapping("/lastDays") // fetching credentials getting expired after 15 till 1 day.
	public ResponseEntity<RestResponse> findExpireCredentialsWithinDaysOfUser() {

		Iterator<UserCredentials> userCredentials = userCredentialsDAO.findUserCredentialsByCurrentDate();
		List<UserCredentials> userExpiredCredentials = new ArrayList<>();
		userCredentials.forEachRemaining(userExpiredCredentials::add);
		
		Set<Long> userIds = userExpiredCredentials.stream().map(e->e.getUserId()).collect(Collectors.toSet());
		
		if (userExpiredCredentials.size() > 0) {
			
			String message = "Don't forget to add your credentials!";
			String messageBody = ", please update your credentials in Float Care!";
			String deepLink = "10";

			for (Long eachUser : userIds) {

				UserBasicInformation user = userRepository.findUsersByUserId(eachUser);

				if (user != null) {
					messageBody = user.getFirstName()+messageBody;
					
					if(user.getFcmToken() != null)
						pushNotification.sendEventNotificationToUser(eachUser, user.getFcmToken(), message, messageBody, deepLink, user.getDeviceType(), null);
					
					inAppNotification.createInAppNotifications(eachUser, 3, messageBody, deepLink, Long.getLong(deepLink), false, 0);

					EmailThread emailThread = new EmailThread(); // sending email
					emailThread.setEmails(user.getEmail());
					emailThread.setMessage(message);
					emailThread.setMessageBody(messageBody);
					applicationContext.getAutowireCapableBeanFactory().autowireBean(emailThread);

					Thread thread = new Thread(emailThread); // send email on separate thread.
					thread.start(); // start the thread
				}
			}

			RestResponse response = new RestResponse();
			response.setData(null);
			response.setMessage(userExpiredCredentials.size() + " user(s) expired credential(s) found.");
			response.setStatus(200);
			response.setSuccess(true);

			return ResponseEntity.ok(response);
		} else {

			RestResponse response = new RestResponse();
			response.setData(null);
			response.setMessage("No expired credentials found.");
			response.setStatus(200);
			response.setSuccess(true);

			return ResponseEntity.ok(response);
		}
	}

	@GetMapping("/verify/required") // fetching required credentials which were not uploaded.
	public ResponseEntity<RestResponse> fetchNotFilledRequiredCredentialsOfUser() {
		
		Set<Long> userCredentials = userCredentialsDAO.findUserCredentialsByIsRequiredAndIsPending(true, true).stream().map(e->e.getUserId()).collect(Collectors.toSet());

		if (userCredentials.size() > 0) {
			
			String message = "Don't forget to add your credentials!";
			String messageBody = ", please update your credentials in Float Care!";
			String deepLink = "10";

			for (Long eachUser : userCredentials) {

				UserBasicInformation user = userRepository.findUsersByUserId(eachUser);
				
				if (user != null && user.getFcmToken() != null) {

					messageBody = user.getFirstName()+messageBody;
					pushNotification.sendEventNotificationToUser(eachUser, user.getFcmToken(), message, messageBody, deepLink, user.getDeviceType(), null);
					inAppNotification.createInAppNotifications(eachUser, 3, message, messageBody, Long.getLong(deepLink), false, 0);
					
					EmailThread emailThread = new EmailThread(); // sending email
					emailThread.setEmails(user.getEmail());
					emailThread.setMessage(message);
					emailThread.setMessageBody(messageBody);
					applicationContext.getAutowireCapableBeanFactory().autowireBean(emailThread);

					Thread thread = new Thread(emailThread); // send email on separate thread.
					thread.start(); // start the thread
				}
			}
			RestResponse response = new RestResponse();
			response.setData(null);
			response.setMessage(userCredentials.size() + " user(s) expired credential(s) found.");
			response.setStatus(200);
			response.setSuccess(true);

			return ResponseEntity.ok(response);
		} else {

			RestResponse response = new RestResponse();
			response.setData(null);
			response.setMessage("No pending required credentials found.");
			response.setStatus(200);
			response.setSuccess(true);

			return ResponseEntity.ok(response);
		}
	}
	
	@GetMapping("/verify/expired") // fetching credentials which were expired.
	public ResponseEntity<RestResponse> fetchExpiredCredentials() {

		Set<Long> expiredUserCredentials = userCredentialsDAO.findUserCredentialsByExpiredDate(LocalDate.now()).stream().map(e->e.getUserId()).collect(Collectors.toSet());

		if (expiredUserCredentials.size() > 0) {
			
			String message = "Alert: Your Credentials have Expired ";
			String messageBody = ", one or more of your credentials have expired. Please upload the renewed credentials to your Float Care account.";
			String deepLink = "10";
			
			for (Long eachUser : expiredUserCredentials) {

				UserBasicInformation user = userRepository.findUsersByUserId(eachUser);

				if (user != null && user.getFcmToken() != null) {
					messageBody = user.getFirstName()+messageBody;
					pushNotification.sendEventNotificationToUser(eachUser, user.getFcmToken(), message, messageBody, deepLink, user.getDeviceType(), null);
					inAppNotification.createInAppNotifications(eachUser, 3, message, messageBody, Long.getLong(deepLink), false, 0);

					EmailThread emailThread = new EmailThread(); // sending email
					emailThread.setEmails(user.getEmail());
					emailThread.setMessage(message);
					emailThread.setMessageBody(messageBody);
					applicationContext.getAutowireCapableBeanFactory().autowireBean(emailThread);

					Thread thread = new Thread(emailThread); // send email on separate thread.
					thread.start(); // start the thread
				}
			}
				RestResponse response = new RestResponse();
				response.setData(null);
				response.setMessage(expiredUserCredentials.size() + " user(s) expired credential(s) found.");
				response.setStatus(200);
				response.setSuccess(true);
	
				return ResponseEntity.ok(response);
		} else {

			RestResponse response = new RestResponse();
			response.setData(null);
			response.setMessage("No expired credentials found.");
			response.setStatus(200);
			response.setSuccess(true);

			return ResponseEntity.ok(response);
		}
	}
	@GetMapping("/test")
	public ResponseEntity<Object> sendTestNotification(){
		
		String message = "Alert: Your Credentials have Expired ";
		String messageBody = ", one or more of your credentials have expired. Please upload the renewed credentials to your Float Care account.";
		String deepLink = "10";
		
		messageBody = "Samika"+messageBody;
		pushNotification.sendEventNotificationToUser(3, "dV0Zl-EROkvkkKfBtgFQnG:APA91bG-ND1LF8U-e2byNBz7021F1LQ92W0fULT2Qy0nByTJuE7GNgrrDRjn6kYAVku2JAfQZ3k87sk8hPOYfDwaud9mSxh7W_VCDemhgXoVaKuWi7hwHLvHfWDtdlOGKR1WJnej_LBs"+ "", message, messageBody, deepLink,
				null, null);
//		inAppNotification.createInAppNotifications(eachUser, 3, messageBody, deepLink, Long.getLong(deepLink), false);
		
		EmailThread emailThread = new EmailThread(); // sending email
//		emailThread.setEmails(user.getEmail());
//		emailThread.setMessage(message);
//		emailThread.setMessageBody(messageBody);
		applicationContext.getAutowireCapableBeanFactory().autowireBean(emailThread);
		
		RestResponse response = new RestResponse();
		response.setData(null);
		response.setMessage("No expired credentials found.");
		response.setStatus(200);
		response.setSuccess(true);

		return ResponseEntity.ok(response);

	}
}
