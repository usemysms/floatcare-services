package com.java.floatcare.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.java.floatcare.api.response.pojo.RestResponse;
import com.java.floatcare.model.UserPlan;
import com.java.floatcare.service.UserPlanService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/userPlan")
@Validated
@Slf4j
public class UserPlanController {

	@Autowired
	private UserPlanService userPlanService;

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public ResponseEntity<Object> registerUserPlan(@RequestBody UserPlan userPlan) {

		log.info("UserPlanController -> registerUserPlan {}");

		RestResponse restResponse = userPlanService.registerPlan(userPlan);

		return ResponseEntity.status(restResponse.getStatus()).body(restResponse);
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseEntity<Object> updateUserPlan(@RequestBody UserPlan userPlan) {

		log.info("UserPlanController -> updateUserPlan {}");

		RestResponse restResponse = userPlanService.updatePlan(userPlan);
		return ResponseEntity.status(restResponse.getStatus()).body(restResponse);
	}

	@Operation(summary = "Get a plan details of User by UserId")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Plan is active for provided User", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = RestResponse.class)) }),
			@ApiResponse(responseCode = "403", description = "Plan is expired for provided User", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = RestResponse.class)) }),
			@ApiResponse(responseCode = "404", description = "User is not registered for any plan", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = RestResponse.class)) }) })
	@GetMapping(value = "/getUserPlanDetails/{userId}")
	public ResponseEntity<Object> getUserPlanDetails(@PathVariable Long userId) {

		log.info("UserPlanController -> getUserPlanDetails {}");

		RestResponse restResponse = userPlanService.getUserPlanDetails(userId);
		return ResponseEntity.status(restResponse.getStatus()).body(restResponse);
	}
}
