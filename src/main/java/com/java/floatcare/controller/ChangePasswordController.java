package com.java.floatcare.controller;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import com.java.floatcare.api.request.pojo.ChangePasswordRequest;
import com.java.floatcare.api.request.pojo.EmailPhoneVerificationRequest;
import com.java.floatcare.api.request.pojo.ForgotPasswordRequest;
import com.java.floatcare.api.response.pojo.RestResponse;
import com.java.floatcare.communication.SendGridEmailService;
import com.java.floatcare.communication.TwilioCommunication;
import com.java.floatcare.dao.CountersDAO;
import com.java.floatcare.dao.UserBasicInformationRepositoryDAO;
import com.java.floatcare.model.EmailVerification;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.repository.EmailVerificationRepository;
import com.java.floatcare.repository.UserBasicInformationRepository;
import com.java.floatcare.security.JwtTokenUtil;
import com.java.floatcare.utils.EmailDomainValidation;
import com.twilio.exception.ApiException;

@RestController
@RequestMapping("/user")
public class ChangePasswordController {

	@Autowired
	private UserBasicInformationRepositoryDAO userBasicInformationRepositoryDAO;
	@Autowired
	private UserBasicInformationRepository userBasicInformationRepository;
	@Autowired
	private EmailVerificationRepository emailVerificationRepository;
	@Autowired
	private CountersDAO sequence;
	@Autowired
	private TwilioCommunication sendSms;
	@Autowired
	private SendGridEmailService sendEmail;
	
	@RequestMapping(value = "/sendOTP", method = RequestMethod.POST)
	public ResponseEntity<Object> sendOTP(@RequestBody EmailPhoneVerificationRequest emailPhoneVerificationRequest)
			throws Exception, JSONException {

		UserBasicInformation user = null;
		
		String parameter = null;
		
		if (emailPhoneVerificationRequest.getEmail() != null) {
			
			parameter = emailPhoneVerificationRequest.getEmail();
			
			if (parameter.contains("@")) // to check whether the given field is number or an email
				user = userBasicInformationRepositoryDAO.findUserByEmail(parameter);
			else
			{
				PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
				PhoneNumber numberProto = phoneUtil.parse(parameter, "US");
				parameter = phoneUtil.format(numberProto, PhoneNumberFormat.NATIONAL);
				user = userBasicInformationRepositoryDAO.findUserByPhone(parameter);
			}
		}

		RestResponse restResponse = new RestResponse();

		if (user != null) {
			
			emailVerificationRepository.deleteAllByEmail(user.getEmail());
			
			Random random = new Random();
			String otpId = String.format("%05d", random.nextInt(100000));
			
			String message = "OTP to change your password is "+otpId;

			if (new EmailDomainValidation().validateEmailDomain(user.getEmail().split("@")[1])) // check if the email is correct or not
				try {
					sendEmail.sendText(user.getEmail(), "OTP for verification", message);
				} catch (ApiException e) {
					
				}
			else {

				restResponse.setStatus(404);
				restResponse.setSuccess(false);
				restResponse.setMessage("Given email is not valid. ");
				restResponse.setData("");
				return ResponseEntity.status(404).body(restResponse);
			}
			try {
				if (user.getPhone()!=null) {
					if (user.getPhone().length() >= 10)
						try {
							sendSms.sendSMS(user.getPhone(), message);
						}catch (Exception e) {
							
						}
				}
			} catch (ApiException e) {
				restResponse.setStatus(e.getStatusCode());
				restResponse.setSuccess(false);
				restResponse.setMessage("Given mobile/phone number is not valid to send OTP.");
				restResponse.setData("");
				
				return ResponseEntity.status(e.getStatusCode()).body(restResponse);
			}
			
			EmailVerification emailVerification = new EmailVerification();
			emailVerification.setEmailId(sequence.getNextSequenceValue("emailId"));
			emailVerification.setEmail(parameter);

			emailVerification.setOtp(otpId);
			emailVerification.setExpiryTime(System.currentTimeMillis() + 600000);

			emailVerificationRepository.save(emailVerification);

			restResponse.setStatus(200);
			restResponse.setSuccess(true);
			restResponse.setMessage("OTP sent successfully to "+user.getEmail()+" and "+user.getPhone());
			restResponse.setData(user.getEmail()+", "+user.getPhone());
			return ResponseEntity.ok(restResponse);
		}
			else {
				restResponse.setStatus(403);
				restResponse.setSuccess(false);
				restResponse.setMessage("Email or Phone does not exist in database.");
				restResponse.setData("");
				return ResponseEntity.status(403).body(restResponse);
			}
		}

	@RequestMapping(value = "/changePassword", method = RequestMethod.POST)
	public ResponseEntity<Object> changePassword(@RequestBody ChangePasswordRequest changePasswordRequest)
			throws Exception, JSONException {

		Optional<UserBasicInformation> user = userBasicInformationRepository.findById(changePasswordRequest.getUserId());
		
		RestResponse restResponse = new RestResponse();

		if (user.isPresent()) {
			if (JwtTokenUtil.validatePassword(changePasswordRequest.getOldPassword(), user.get().getPassword())) {
				try {
					String generatedSecuredPasswordHash = JwtTokenUtil
							.generateStrongPasswordHash(changePasswordRequest.getNewPassword());
					user.get().setPassword(generatedSecuredPasswordHash);
					user.get().setFirstLogin(false); // set first login value false
				} catch (NoSuchAlgorithmException e) {
					e.printStackTrace();
				} catch (InvalidKeySpecException e) {
					e.printStackTrace();
				}
				userBasicInformationRepository.save(user.get());
				restResponse.setStatus(200);
				restResponse.setSuccess(true);
				restResponse.setMessage("Password changed successfully.");
				restResponse.setData("");
				return ResponseEntity.ok(restResponse);
			} else {
				restResponse.setStatus(403);
				restResponse.setSuccess(false);
				restResponse.setMessage("Wrong Password given.");
				restResponse.setData("");
				return ResponseEntity.status(403).body(restResponse);
			}
		} else {
			restResponse.setStatus(404);
			restResponse.setSuccess(false);
			restResponse.setMessage("User Not Found.");
			restResponse.setData("");
			return ResponseEntity.status(404).body(restResponse);
		}
	}
	
	@RequestMapping(value = "/setPassword", method = RequestMethod.POST)
	public ResponseEntity<Object> setPassword(@RequestBody ChangePasswordRequest changePasswordRequest)
			throws Exception, JSONException {

		Optional<UserBasicInformation> user = userBasicInformationRepository.findById(changePasswordRequest.getUserId());
		
		RestResponse restResponse = new RestResponse();

		if (user.isPresent()) {
			if (JwtTokenUtil.validatePassword(changePasswordRequest.getOldPassword(), user.get().getPassword())) {
				try {
					String generatedSecuredPasswordHash = JwtTokenUtil.generateStrongPasswordHash(changePasswordRequest.getNewPassword());
					user.get().setPassword(generatedSecuredPasswordHash);
				} catch (NoSuchAlgorithmException e) {
					e.printStackTrace();
				} catch (InvalidKeySpecException e) {
					e.printStackTrace();
				}
				userBasicInformationRepository.save(user.get());
				restResponse.setStatus(200);
				restResponse.setSuccess(true);
				restResponse.setMessage("Password changed successfully.");
				restResponse.setData("");
				return ResponseEntity.ok(restResponse);
			} else {
				restResponse.setStatus(403);
				restResponse.setSuccess(false);
				restResponse.setMessage("Wrong Password given.");
				restResponse.setData("");
				return ResponseEntity.status(403).body(restResponse);
			}
		} else {
			restResponse.setStatus(404);
			restResponse.setSuccess(false);
			restResponse.setMessage("User Not Found.");
			restResponse.setData("");
			return ResponseEntity.status(404).body(restResponse);
		}
	}
	
	@RequestMapping(value = "/forgotPassword", method = RequestMethod.POST)
	public ResponseEntity<RestResponse> forgotPassword(@RequestBody ForgotPasswordRequest forgotPasswordRequest)
			throws Exception, JSONException {

		RestResponse restResponse = new RestResponse();

		EmailVerification emailVerificationObj = emailVerificationRepository.findByOtp(forgotPasswordRequest.getOtp());
		
		if (emailVerificationObj != null) {

			try {

				UserBasicInformation user = null; 
					
				if (emailVerificationObj.getEmail().contains("@"))
					user = userBasicInformationRepositoryDAO.findUserByEmail(emailVerificationObj.getEmail());
				else {
					PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
					PhoneNumber numberProto = phoneUtil.parse(emailVerificationObj.getEmail(), "US");
					String phone = phoneUtil.format(numberProto, PhoneNumberFormat.NATIONAL);
					user = userBasicInformationRepositoryDAO.findUserByPhone(phone);
				}

				if (user != null) {

					if (JwtTokenUtil.validatePassword(forgotPasswordRequest.getNewPassword(), user.getPassword())){
						
						restResponse.setStatus(403);
						restResponse.setSuccess(false);
						restResponse.setMessage("You entered an old password. Please enter a new password.");
						restResponse.setData("");
						
						return ResponseEntity.status(403).body(restResponse);
						
					}else {

						String generatedPassword = JwtTokenUtil.generateStrongPasswordHash(forgotPasswordRequest.getNewPassword());
						
						user.setPassword(generatedPassword);
						user.setFirstLogin(false);
						userBasicInformationRepository.save(user);
						
						emailVerificationRepository.delete(emailVerificationObj);
						
						restResponse.setStatus(200);
						restResponse.setSuccess(true);
						restResponse.setMessage("Password changed successfully.");
						restResponse.setData("");
						
						return ResponseEntity.ok(restResponse);
					}
				}else {
					
					emailVerificationRepository.delete(emailVerificationObj);
					
					restResponse.setStatus(404);
					restResponse.setSuccess(false);
					restResponse.setMessage("User not found.");
					restResponse.setData("");
					
					return ResponseEntity.status(404).body(restResponse);
				}

			} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {

				restResponse.setStatus(401);
				restResponse.setSuccess(false);
				restResponse.setMessage(e.getMessage());
				restResponse.setData(null);
				
				return ResponseEntity.status(401).body(restResponse);
				
			}
		} else {
			restResponse.setStatus(403);
			restResponse.setSuccess(false);
			restResponse.setMessage("Unable to set the password. Could not found the user.");
			restResponse.setData("");
			
			return ResponseEntity.status(403).body(restResponse);
		}
	}
	@RequestMapping(value = "/changePasswordRequest", method = RequestMethod.POST)
	public ResponseEntity<Object> changePasswordForFirstTimeLogin(@RequestBody ChangePasswordRequest changePasswordRequest)
			throws Exception, JSONException {

		Optional<UserBasicInformation> user = userBasicInformationRepository.findById(changePasswordRequest.getUserId());
		
		RestResponse restResponse = new RestResponse();

		if (user.isPresent()) {
				try {
					String generatedSecuredPasswordHash = JwtTokenUtil
							.generateStrongPasswordHash(changePasswordRequest.getNewPassword());
					user.get().setPassword(generatedSecuredPasswordHash);
					user.get().setFirstLogin(false); // set first login value false
				} catch (NoSuchAlgorithmException e) {
					e.printStackTrace();
				} catch (InvalidKeySpecException e) {
					e.printStackTrace();
				}
				userBasicInformationRepository.save(user.get());
				restResponse.setStatus(200);
				restResponse.setSuccess(true);
				restResponse.setMessage("Password changed successfully.");
				restResponse.setData("");
				return ResponseEntity.ok(restResponse);
		} else {
			restResponse.setStatus(404);
			restResponse.setSuccess(false);
			restResponse.setMessage("User Not Found.");
			restResponse.setData("");
			return ResponseEntity.status(404).body(restResponse);
		}
	}
}