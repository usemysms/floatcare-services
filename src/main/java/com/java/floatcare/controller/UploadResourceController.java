package com.java.floatcare.controller;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.io.Files;
import com.java.floatcare.api.response.pojo.RestResponse;
import com.java.floatcare.s3bitbucket.AWSService;

@RestController
@RequestMapping(value= "/upload")
public class UploadResourceController {

	@Autowired
	private AWSService service;
	
	@PostMapping(value= "/resource/{resourceName}/{resourceId}")
	public ResponseEntity<RestResponse> uploadFile(@RequestPart(value= "file") MultipartFile file, @PathVariable("resourceName") String resourceName, @PathVariable("resourceId") int resourceId) {
		
		//name of file//resourcename_resourceId_fileName_timeStamp.fileExtenion
		
		RestResponse response = new RestResponse();
			
		try {
			
			String fileName = file.getOriginalFilename();
			
			/*File files = new File("floatcare/" + fileName);
			
			files.createNewFile();
			FileOutputStream fout = new FileOutputStream(files);
			fout.write(file.getBytes());
			fout.close();*/
			
			String finalResourceName = resourceName+"_"+resourceId+"_"+Files.getNameWithoutExtension(fileName)+"_"+LocalDateTime.now()+"."+Files.getFileExtension(fileName);
		
			if (file.getSize()/1000000 < 11) {
				
				service.uploadFile(file, finalResourceName, 0);
				
				response.setData(finalResourceName);
				response.setSuccess(true);
				response.setMessage("File uploaded successfully");
				response.setStatus(200);
				
				//files.delete(); //delete the file from local
				File fileToDelete = FileUtils.getFile(fileName);
				FileUtils.deleteQuietly(fileToDelete); // delete the file
				
				return ResponseEntity.status(HttpStatus.OK).body(response);
			}else {
				
				//files.delete(); //delete the file from local
				File fileToDelete = FileUtils.getFile(fileName);
				FileUtils.deleteQuietly(fileToDelete); // delete the file
				
				response.setData(null);
				response.setSuccess(true);
				response.setMessage("File size exceeded. Please upload a file of size below 10 MB.");
				response.setStatus(400);
				
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
			}
			
			
		} catch (Exception e) {
		
			response.setData(null);
			response.setSuccess(true);
			response.setMessage(e.getMessage());
			response.setStatus(500);
			
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
	}
}
