package com.java.floatcare.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.naming.AuthenticationException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import com.java.floatcare.api.request.pojo.LoginUser;
import com.java.floatcare.api.request.pojo.OtpRequest;
import com.java.floatcare.api.response.pojo.AuthToken;
import com.java.floatcare.api.response.pojo.ResponseError;
import com.java.floatcare.api.response.pojo.RestResponse;
import com.java.floatcare.communication.MailUtils;
import com.java.floatcare.communication.TwilioCommunication;
import com.java.floatcare.dao.CountersDAO;
import com.java.floatcare.dao.LoginOtpDAO;
import com.java.floatcare.dao.UserBasicInformationRepositoryDAO;
import com.java.floatcare.dao.UserLoginDetailsDAO;
import com.java.floatcare.model.Businesses;
import com.java.floatcare.model.Department;
import com.java.floatcare.model.LoginOtp;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.model.UserLoginDetails;
import com.java.floatcare.model.UserPermissions;
import com.java.floatcare.model.UserWorkspaces;
import com.java.floatcare.repository.BusinessesRepository;
import com.java.floatcare.repository.DepartmentRepository;
import com.java.floatcare.repository.LoginOtpRepository;
import com.java.floatcare.repository.UserBasicInformationRepository;
import com.java.floatcare.repository.UserLoginDetailsRepository;
import com.java.floatcare.repository.UserPermissionsRepository;
import com.java.floatcare.repository.UserWorkspacesRepository;
import com.java.floatcare.security.Constants;
import com.java.floatcare.security.JwtTokenUtil;
import com.java.floatcare.security.RSAUtil;
import com.java.floatcare.utils.ConstantUtils;

@RestController
@RequestMapping("/auth")
public class AuthenticationController {

	@SuppressWarnings("unused")
	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private UserBasicInformationRepositoryDAO userBasicInformationRepositoryDAO;

	@Autowired
	private UserBasicInformationRepository userBasicInformationRepository;

	@SuppressWarnings("unused")
	@Autowired
	private UserLoginDetailsDAO userLoginDetails;

	@Autowired
	private UserLoginDetailsRepository userLoginDetailsRepository;

	@Autowired
	private LoginOtpRepository loginOtpRepository;

	@Autowired
	private LoginOtpDAO loginOtpDAO;

	@Autowired
	private HttpServletRequest httpServletRequest;

	@Autowired
	private HttpServletResponse httpServletResponse;

	@SuppressWarnings("unused")
	@Autowired
	private MailUtils mailUtils;

	@SuppressWarnings("unused")
	@Autowired
	private CountersDAO sequence;
	
	@Autowired
	private UserWorkspacesRepository userWorkspacesRepository;
	
	@Autowired
	private MongoTemplate mongoTemplate;

	@SuppressWarnings("unused")
	@Autowired
	private TwilioCommunication twilioCommunication;
	
	@Autowired
	private UserPermissionsRepository userPermissionsRepository;

	@Autowired
	private BusinessesRepository businessesRepository;

	@Autowired
	private DepartmentRepository departmentRepository;

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ResponseEntity login(@RequestBody LoginUser loginUser) throws Exception {

		UserBasicInformation user = null;
		
		if (loginUser.getDeviceType() == null && loginUser.getEmail().length() > 100) // encrypted
			loginUser.setDeviceType("browser");
		else if (loginUser.getDeviceType() == null && loginUser.getEmail().length() < 100) // non-encrypted in case of production build
			loginUser.setDeviceType("mobile");

		if(loginUser.getDeviceType() != null && !loginUser.getDeviceType().equals("mobile")) {
			String decryptedEmail = RSAUtil.decrypt(loginUser.getEmail(), ConstantUtils.privateKey);
			String decryptedPassword = RSAUtil.decrypt(loginUser.getPassword(), ConstantUtils.privateKey);
			loginUser.setEmail(decryptedEmail);
			loginUser.setPassword(decryptedPassword);
		}

		if (loginUser.getEmail().contains("(") && loginUser.getEmail().contains("-")) { //phone

			String phone = loginUser.getEmail() ;
			PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
			PhoneNumber numberProto = phoneUtil.parse(phone, "US");
			phone = phoneUtil.format(numberProto, PhoneNumberFormat.NATIONAL);
			user = userBasicInformationRepositoryDAO.findAllUsersByPhone(phone);

		} else if (loginUser.getEmail().contains("@") && loginUser.getEmail().contains(".")) { //email
			user = userBasicInformationRepositoryDAO.findUserByEmail(loginUser.getEmail());
		}else { // no email or phone provided
			httpServletResponse.setStatus(403);
			return ResponseEntity.status(403).body(new ResponseError("please provide a valid e-mail or phone.","email"));
		}

		if (user != null) {

			UserBasicInformation userBasic = user;
			List<UserWorkspaces> userWorkspaces = userWorkspacesRepository.findUserWorkspacesByUserId(userBasic.getUserId());
			for(UserWorkspaces eachWorkspace:userWorkspaces) {
				Optional<Businesses> businessOptional =  businessesRepository.findById(eachWorkspace.getBusinessId());
					if(businessOptional.isPresent()) {
						eachWorkspace.setBusiness(businessOptional.get());
					}
					Optional<Department> departmentOptional =  departmentRepository.findById(eachWorkspace.getWorkspaceId());
					if(departmentOptional.isPresent()) {
						eachWorkspace.setWorkspaceName(departmentOptional.get().getDepartmentTypeName());
					}
			}

			
			Optional<UserPermissions> userPermission = userPermissionsRepository.findById(user.getUserId()); // fetch user permission by userid
			
			userBasic.setUserWorkspaces(userWorkspaces);
			
			if (userPermission.isPresent())
				userBasic.setUserPermissions(userPermission.get());

			boolean isValid = JwtTokenUtil.validatePassword(loginUser.getPassword(), userBasic.getPassword());
			if (isValid) {
				// fetch logged in device details
				//List<UserLoginDetails> userLoginDetailsList = userLoginDetails.findByUserIdAndDevice(userBasic.getUserId(), httpServletRequest.getRemoteAddr());
				// commented otp logic for disabling temparary
				/*if ((userLoginDetailsList == null || userLoginDetailsList.size() == 0) && loginUser.getDeviceType()==null) {
					LoginOtp loginOtp = new LoginOtp();
					loginOtp.setLoginOtpId(sequence.getNextSequenceValue("loginOtpId"));
					loginOtp.setUserId(userBasic.getUserId());
					Random random = new Random();
					String id = String.format("%06d", random.nextInt(1000000));
					loginOtp.setOtpString(id);
					loginOtp.setExpired(false);
					loginOtp.setCreatedTime(new Date());
					loginOtpRepository.save(loginOtp);
					//System.out.println("loginOTP :" + id);
					String description = "OTP for Floatcare login verification : " + id;
					mailUtils.sendMail(userBasic.getEmail(), "Floatcare : Login OTP verification", description);
					//twilioCommunication.sendSMS(userBasic.getPhone(),description);
					httpServletResponse.setStatus(200);
					return ResponseEntity.ok(new OtpResponse(userBasic.getUserId(),
							"OTP sent to registered email and phone number.", "OTP"));
				} else {*/
				//if(!userBasic.isFirstLogin()) {
					final String token = jwtTokenUtil.generateToken(userBasic);
					UserLoginDetails userLoginDetails = new UserLoginDetails();
					userLoginDetails.setUserId(userBasic.getUserId());
					userLoginDetails.setAuthToken(token);
					if(loginUser.getUuid()!="") {
						userLoginDetails.setDevice(loginUser.getUuid());	
						userLoginDetails.setDeviceType(loginUser.getDeviceType());
					} else {
					userLoginDetails.setDevice(httpServletRequest.getRemoteAddr());
					userLoginDetails.setDeviceType("desktop");
					}
					userLoginDetails.setLasttouch(new Date());
					userLoginDetails.setLoggedinTime(new Date());
					userLoginDetails.setLoggedOut(false);
					// userLoginDetails.setLoginOtpId(loginOtp.ge);
					userLoginDetailsRepository.save(userLoginDetails);

					// final Authentication authentication = authenticationManager
					// .authenticate(new UsernamePasswordAuthenticationToken(loginUser.getEmail(),
					// token));
					// System.out.println(" Details : "+authentication.getName());
					// SecurityContextHolder.getContext().setAuthentication(authentication);
					RestResponse restResponse = new RestResponse();
					restResponse.setMessage("Logged in successfully");
					restResponse.setStatus(200);
					restResponse.setSuccess(true);
					restResponse.setData(new AuthToken(token,userBasic, "Token"));
					return ResponseEntity.ok(restResponse);
				//}
				/*}else {
					return ResponseEntity.ok(new OtpResponse(userBasic.getUserId(),
							"This was your first time login.", "FirstLogin"));
				}*/
			} else {
				return ResponseEntity.status(403).body(new ResponseError("Oops, that wasn't the right password. Please try again.","password"));
			}

		} else {
			httpServletResponse.setStatus(403);
			return ResponseEntity.status(403).body(new ResponseError("This email does not exist in our records","email"));
		}

	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/validateOTP", method = RequestMethod.POST)
	public ResponseEntity validateOTPToken(@RequestBody OtpRequest otpRequest) throws AuthenticationException {
//System.out.println(otpRequest.toString());
		List<LoginOtp> loginOtpList = loginOtpDAO.findByUserIdAndOtp(otpRequest.getUserId(), otpRequest.getOtpString());
		//System.out.println(loginOtpList.size());
		if (loginOtpList.size() > 0) {
			LoginOtp loginOtp = loginOtpList.get(0);
			//System.out.println(loginOtp.toString());
			loginOtp.setExpired(true);
			loginOtpRepository.save(loginOtp);
			Optional<UserBasicInformation> userBasicInformation = userBasicInformationRepository.findById(otpRequest.getUserId());
			//System.out.println(userBasicInformation.toString());
			if(userBasicInformation.isPresent()) {
			final String token = jwtTokenUtil.generateToken(userBasicInformation.get());
			UserLoginDetails userLoginDetails = new UserLoginDetails();
			userLoginDetails.setUserId(otpRequest.getUserId());
			userLoginDetails.setAuthToken(token);
			userLoginDetails.setDevice(httpServletRequest.getRemoteAddr());
			userLoginDetails.setLasttouch(new Date());
			userLoginDetails.setLoggedinTime(new Date());
			userLoginDetails.setLoggedOut(false);
			userLoginDetails.setLoginOtpId(loginOtp.getLoginOtpId());
			userLoginDetailsRepository.save(userLoginDetails);
			return ResponseEntity.ok(new AuthToken(token, userBasicInformation.get(),"Token"));
			} else {
				return ResponseEntity.status(400).body(new ResponseError("Invalid OTP",""));
			}

		} else {
			return ResponseEntity.status(400).body(new ResponseError("Invalid OTP",""));
		}
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public ResponseEntity logout() throws AuthenticationException {

		String header = httpServletRequest.getHeader(Constants.HEADER_STRING);
		String username = null;
		String authToken = null;
		if (header != null && header.startsWith(Constants.TOKEN_PREFIX)) {
			authToken = header.replace(Constants.TOKEN_PREFIX, "");
			username = jwtTokenUtil.getUsernameFromToken(authToken);
			
			//setting fcm token to null
			UserBasicInformation userBasicInformation = mongoTemplate.findOne(new Query().addCriteria(Criteria.where("email").is(username)), UserBasicInformation.class);
			userBasicInformation.setFcmToken(null);
			userBasicInformationRepository.save(userBasicInformation);
			
			UserLoginDetails userLoginDetails = mongoTemplate.findOne(new Query().addCriteria(Criteria.where("userId").is(userBasicInformation.getUserId())), UserLoginDetails.class);
			
			userLoginDetails.setLasttouch(new Date());
			userLoginDetails.setLoggedOut(true);
			userLoginDetailsRepository.save(userLoginDetails);
			
			return ResponseEntity.ok("Successfully logged out!!!");

		} else {
			return ResponseEntity.ok("Invalid OTP.");
		}
	}
}