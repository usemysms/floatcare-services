package com.java.floatcare.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.java.floatcare.api.response.pojo.RequiredType;
import com.java.floatcare.api.response.pojo.RestResponse;
import com.java.floatcare.dao.CountersDAO;
import com.java.floatcare.model.CredentialSubTypes;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.model.UserCredentials;
import com.java.floatcare.repository.CredentialSubTypeRepository;
import com.java.floatcare.repository.UserBasicInformationRepository;
import com.java.floatcare.repository.UserCredentialsRepository;

@RestController
@RequestMapping("/import")
public class InsertUserCredentials {

	@Autowired
	private UserBasicInformationRepository userRepository;
	@Autowired
	private UserCredentialsRepository userCredentialsRepository;
	@Autowired
	private CredentialSubTypeRepository credentialSubTypeRepository;
	@Autowired
	private CountersDAO sequence; 
	
	@GetMapping(path = "/user/credentials")
	public ResponseEntity<RestResponse> insertOldUsersCredentials() {
		
		List<UserBasicInformation> users = userRepository.findByUserTypeIdNotAndStatus(1, "Active");
				
		for (UserBasicInformation eachUser : new ArrayList<>(users)) {
		
			long credentialSubTypeCount = credentialSubTypeRepository.findByStaffCoreType(eachUser.getStaffUserCoreTypeId()).size();
			
			if (userCredentialsRepository.findByUserId(eachUser.getUserId()).size() < credentialSubTypeCount) {

				List<CredentialSubTypes> credentialSubTypes = credentialSubTypeRepository.findByStaffCoreType(eachUser.getStaffUserCoreTypeId());
				
				for(CredentialSubTypes eachCredential: credentialSubTypes) {
					
					List<UserCredentials> userCredentials = userCredentialsRepository.findByUserIdAndCredentialSubTypeId(eachUser.getUserId(), eachCredential.getCredentialSubTypeId());
					
					if (userCredentials == null || userCredentials.size() == 0) {

						UserCredentials userCredential = new UserCredentials();
						
						userCredential.setUserId(eachUser.getUserId());
						userCredential.setPending(true);
						userCredential.setCredentialTypeId(eachCredential.getCredentialTypeId());
						userCredential.setCredentialSubTypeId(eachCredential.getCredentialSubTypeId());
						userCredential.setState(eachUser.getState());
						userCredential.setUserCredentialId(sequence.getNextSequenceValue("userCredentialId"));
						
						Optional<RequiredType> requiredTypeOptional = eachCredential.getRequiredType().stream().filter(item->item.getStaffCoreTypeId() == eachUser.getStaffUserCoreTypeId()).findFirst();
						
						if(requiredTypeOptional.isPresent()) {
							userCredential.setRequired(requiredTypeOptional.get().getIsRequired());
						}
						userCredentialsRepository.save(userCredential);
					}else
						continue;
				}
			}else {
				users.remove(eachUser);
				continue;
			}
		}
		
		RestResponse response = new RestResponse();
		
		response.setData(users);
		response.setMessage(users.size()+" old user(s) credentials imported successfully.");
		response.setStatus(200);
		response.setSuccess(true);
		
		return ResponseEntity.ok(response);
	}
}
