package com.java.floatcare.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import com.java.floatcare.api.response.pojo.RestResponse;
import com.java.floatcare.dao.CountersDAO;
import com.java.floatcare.model.Businesses;
import com.java.floatcare.model.Department;
import com.java.floatcare.model.Organizations;
import com.java.floatcare.model.StaffUserCoreTypes;
import com.java.floatcare.model.SubRoleTypes;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.repository.BusinessesRepository;
import com.java.floatcare.repository.DepartmentRepository;
import com.java.floatcare.repository.OrganizationRepository;
import com.java.floatcare.repository.StaffUserCoreTypesRepository;
import com.java.floatcare.repository.SubRoleTypesRepository;
import com.java.floatcare.repository.UserBasicInformationRepository;
import com.java.floatcare.service.UserBasicInformationService;
import com.java.floatcare.service.UserWorkspacesService;

import static com.java.floatcare.utils.StringUtils.*;

@RestController
@RequestMapping("/import")
public class BulkImportUsersController {

		@Autowired
		private UserBasicInformationRepository userRepository;
		@Autowired
		private UserBasicInformationService userBasicInformationService;
		@Autowired
		private OrganizationRepository organizationRepository;
		@Autowired
		private DepartmentRepository departmentRepository;
		@Autowired
		private BusinessesRepository businessesRepository;
		@Autowired
		private StaffUserCoreTypesRepository staffUserCoreTypeRepository;
		@Autowired
		private UserWorkspacesService userWorkspacesService;
		@Autowired
		private SubRoleTypesRepository subRoleTypesRepository;
		@Autowired
		private CountersDAO sequence;

		@PostMapping(path = "/new/users/{organizationId}/{businessId}/{departmentId}/{sendsms}")
		public ResponseEntity<Object> bulkImportUsers(@RequestPart(value = "file") MultipartFile file,@PathVariable(name = "organizationId") long organizationId, 
				@PathVariable(name = "businessId") long businessId, @PathVariable(name = "departmentId") long departmentId,
				@PathVariable(name = "sendsms") int sendsms) throws IOException {

			File files = new File("floatcare/" + file.getOriginalFilename());
			files.createNewFile();
			FileOutputStream fout = new FileOutputStream(files);
			fout.write(file.getBytes());
			fout.close();
			
			LinkedList<UserBasicInformation> userList = getUserDetails("floatcare/" + file.getOriginalFilename());

			if (organizationId > 0 && businessId > 0 && departmentId > 0) {

				Optional<Organizations> organizationOptional = organizationRepository.findById(organizationId);
				Optional<Businesses> businessOptional = businessesRepository.findById(businessId);
				Optional<Department> departmentOptional = departmentRepository.findById(departmentId);
				
				if (organizationOptional.isPresent() && businessOptional.isPresent() && departmentOptional.isPresent()) {

					for (UserBasicInformation each : new LinkedList<>(userList)) {
						
						if(each.getFirstName() != null && each.getLastName() != null && each.getEmail() != null && each.getPhone() != null
								&& each.getEmail().contains("@") && each.getStaffUserCoreTypeName().length() >= 2) {
							
							long staffUserCoreTypeId  = 0;
							long subRoleTypeId  = 0;

							StaffUserCoreTypes staffUserCoreType = staffUserCoreTypeRepository.findFirstByLabel(each.getStaffUserCoreTypeName()); //fetch user core type

							if (staffUserCoreType != null) {

								staffUserCoreTypeId = staffUserCoreType.getStaffUserCoreTypesId(); //assign staffUserCoreTypeId

								if (each.getBioInformation() != null) {
									SubRoleTypes subRole = subRoleTypesRepository.findFirstByLabel(each.getBioInformation()); // chceck if user sub role already exists
	
									if (subRole != null)
										subRoleTypeId = subRole.getId(); //assign staffUserCoreTypeId
	
									else { // if sub role does not exist
	
										SubRoleTypes newSubRole = new SubRoleTypes();
	
										subRoleTypeId = sequence.getNextSequenceValue("subRoleTypeId");
										newSubRole.setParentId(staffUserCoreTypeId);
										newSubRole.setLabel(each.getBioInformation());
										newSubRole.setId(subRoleTypeId);
										
										subRoleTypesRepository.save(newSubRole);
									}
								}
							} else { // if staff user core type does not exist, add core type and sub roles as well
								
								StaffUserCoreTypes newCoreType = new StaffUserCoreTypes();
								
								staffUserCoreTypeId = subRoleTypeId = sequence.getNextSequenceValue("staffUserCoreTypeId");
								newCoreType.setLabel(each.getStaffUserCoreTypeName());
								newCoreType.setStaffUserCoreTypesId(staffUserCoreTypeId);
								
								staffUserCoreTypeRepository.save(newCoreType);
								
								if (each.getBioInformation() != null) {
									SubRoleTypes newSubRole = new SubRoleTypes();
									
									subRoleTypeId = sequence.getNextSequenceValue("subRoleTypeId");
									newSubRole.setParentId(staffUserCoreTypeId);
									newSubRole.setLabel(each.getBioInformation());
									newSubRole.setId(subRoleTypeId);
	
									subRoleTypesRepository.save(newSubRole);
								}
							}

							Optional<UserBasicInformation> findUser = userRepository.findUserByEmail(each.getEmail());

							if (!findUser.isPresent()) { //check existing email

								//saving in user basic information
								UserBasicInformation user = null;
								try { // handling same email or same phone exceptions
									user = userBasicInformationService.createUserBasicInformation(each.getFirstName(), each.getLastName(), each.getMiddleName(), each.getEmail(), "Ftp@1234", each.getDefaultPassword(),
											each.getPreferredName(), each.getAddress1(), each.getAddress2(), each.getCity(), each.getState(), each.getCountry(), each.getPostalCode(), 
											each.getPhone(), each.getPrimaryContactCountryCode(), each.getProfilePhoto(), each.getTitle(), 3,
											staffUserCoreTypeId, subRoleTypeId > 0 ? subRoleTypeId : 0, null, each.getLinkedInProfile(), organizationId, each.getGender(), null,
											LocalDate.now().toString(), each.getSecondaryPhoneNumber(), each.getAlternativeContactCountryCode(), each.getSecondaryEmail(), each.getFcmToken(), each.getDeviceType(), each.getUserLanguages(),
											each.getUserNationalProviderIdentity(), each.getSocialSecurityNumber(), null, null, null, null,
											each.getInvitedSource(),each.getInvitedFirmId(),each.getInvitedBy(),each.getInvitationStatus());

									//saving user in user workspace
									userWorkspacesService.createUserWorkspace(user.getUserId(), "department", departmentId, "Full Time", false, false,
											organizationId, businessId, "", null, staffUserCoreTypeId, false, subRoleTypeId, sendsms, null);
									continue;
								} catch (Exception e) {
									userList.remove(each); //remove the user which already exists
								} finally {

									user = userRepository.findFirstUserByPhone(each.getPhone());

									if (user != null && user.getFirstName().equals(each.getFirstName())) //saving user in user workspace
										userWorkspacesService.createUserWorkspace(user.getUserId(), "department", departmentId, "Full Time", false, false,
											organizationId, businessId, "", null, staffUserCoreTypeId, false, subRoleTypeId > 0 ? subRoleTypeId : 0, sendsms, null);
								}
							}else {// if email already exists then add the user into workspace

								userList.remove(each);
								//saving user in user workspace
								userWorkspacesService.createUserWorkspace(findUser.get().getUserId(), "department", departmentId, "Full Time", false, false,
										organizationId, businessId, "", null, staffUserCoreTypeId, false, subRoleTypeId, sendsms, null);

							}
						}else
							userList.remove(each);
					}

					files.delete(); //delete the CSV file

					RestResponse response = new RestResponse();

					response.setData(userList);
					response.setStatus(200);
					if(userList.size() == 1) {
						response.setMessage(userList.size()+" User imported successfully. ");
					}else {
						response.setMessage(userList.size()+" Users imported successfully. ");
					}
					response.setSuccess(true);

					return ResponseEntity.status(200).body(response);

			} else {

				files.delete(); //delete the CSV file
				
				RestResponse response = new RestResponse();
				
				response.setData(null);
				response.setStatus(404);
				response.setMessage("organizationId or businessId or departmentId does not exist. ");
				response.setSuccess(false);
				
				return ResponseEntity.status(404).body(response);
			}
		} else {
			
			files.delete(); //delete the CSV file
			
			RestResponse response = new RestResponse();
			
			response.setData(null);
			response.setStatus(404);
			response.setMessage("Missing organizationId or businessId or departmentId. ");
			response.setSuccess(false);
			
			return ResponseEntity.status(404).body(response);
		}
	}

		private static LinkedList<UserBasicInformation> getUserDetails(String file) {

			LinkedList<UserBasicInformation> userList = new LinkedList<>();

			Path pathToFile = Paths.get(file);
			try (BufferedReader br = Files.newBufferedReader(pathToFile)) {
				String row = br.readLine();
				while (row != null) {
					String[] attributes = row.split(",");
					UserBasicInformation user = getOneUser(attributes);
					userList.add(user);
					row = br.readLine();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}

			return userList;
		}

		private static  UserBasicInformation getOneUser(String[] attributes) {

			String firstName;
			String lastName;
			String email;
			String phone;
			String address1;
			String state;
			String city;
			String staffUserCoreTypeName;
			String subCoreTypeName;
			
			try {
				firstName = getOnlyStrings(attributes[0].trim());
			} catch (Exception e) {
				firstName = null;
			}
			try {
				lastName = getOnlyStrings(attributes[1].trim());
			} catch (Exception e) {
				lastName = null;
			}
			
			try {
				city = getOnlyStrings(attributes[2].trim());
			} catch (Exception e) {
				city = null;
			}
			
			try {
				state = getOnlyStrings(attributes[3].trim());
			} catch (Exception e) {
				state = null;
			}
			
			try {
				address1 = getOnlyStrings(attributes[4].trim());
			} catch (Exception e) {
				address1 = null;
			}
			
			try {
				email = attributes[5].trim();
				email = email.toLowerCase();
			} catch (Exception e) {
				email = null;
			}
			
			try {
				phone  = attributes[6].trim();

				PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
				PhoneNumber numberProto = phoneUtil.parse(phone.trim(), "US");
				phone = phoneUtil.format(numberProto, PhoneNumberFormat.NATIONAL);

			} catch (Exception e) {
				phone = null;
			}

			try {
				staffUserCoreTypeName = getOnlyStrings(attributes[7].trim());
			} catch (Exception e) {
				staffUserCoreTypeName = null;
			}

			try {
				subCoreTypeName = getOnlyStrings(attributes[8].trim());
			} catch (Exception e) {
				subCoreTypeName = null;
			}

		UserBasicInformation userBasicInformation = new UserBasicInformation();
		
		if (firstName != null)
			userBasicInformation.setFirstName(firstName);
		if (lastName != null)
			userBasicInformation.setLastName(lastName);
		if (staffUserCoreTypeName != null)
			userBasicInformation.setStaffUserCoreTypeName(staffUserCoreTypeName);
		if (subCoreTypeName != null)
			userBasicInformation.setBioInformation(subCoreTypeName);
		if (email != null)
			userBasicInformation.setEmail(email);
		if (phone != null)
			userBasicInformation.setPhone(phone);
		if (address1 != null)
			userBasicInformation.setAddress1(address1);
		if (state != null)
			userBasicInformation.setState(state);
		if (city != null)
			userBasicInformation.setCity(city);

		return userBasicInformation;
	}
}