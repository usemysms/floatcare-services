package com.java.floatcare.controller;

import java.text.DateFormatSymbols;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.java.floatcare.api.request.pojo.PendingRequests;
import com.java.floatcare.api.response.pojo.RestResponse;
import com.java.floatcare.api.response.pojo.TotalRecords;
import com.java.floatcare.dao.OpenShiftInviteesDAO;
import com.java.floatcare.dao.OpenShiftsDAO;
import com.java.floatcare.dao.RequestsDAO;
import com.java.floatcare.model.Department;
import com.java.floatcare.model.OpenShiftInvitees;
import com.java.floatcare.model.OpenShifts;
import com.java.floatcare.model.Requests;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.repository.DepartmentRepository;
import com.java.floatcare.repository.UserBasicInformationRepository;
import com.java.floatcare.utils.ZonedDateTimeWriteConverter;

@RestController
@RequestMapping("/requests")
public class RequestsController {

	@Autowired
	private RequestsDAO requestsDAO;
	@Autowired
	private OpenShiftsDAO openShiftsDAO;
	@Autowired
	private OpenShiftInviteesDAO openShiftInviteesDAO;
	@Autowired
	private DepartmentRepository departmentRepository;
	@Autowired
	private UserBasicInformationRepository userBasicInformationRepository;

	
	@RequestMapping(value = "/{workspaceId}", method = RequestMethod.GET)
	public ResponseEntity<Object> getListOfReqests(@PathVariable long workspaceId) {

		Optional<Department> departmentOptional = departmentRepository.findById(workspaceId);

		if (departmentOptional.isPresent()) {

			TreeSet<LocalDate> pendingDatesList = new TreeSet<LocalDate>(); // collect the list of dates in this list for pending requests and open shifts
			
			List<Requests> pendingRequestsList = requestsDAO.findRequestsByStatusAndDepartmentId("Pending", workspaceId);
			
			List<LocalDate> pendingDateList = pendingRequestsList.stream().map(emitter -> emitter.getOnDate().get(0)).collect(Collectors.toList());
			
			pendingDatesList.addAll(pendingDateList); //pending requests
			pendingDatesList.addAll(openShiftsDAO.findOpenShiftsByStatusAndDepartmentId("Active", workspaceId)); //pending open-shifts
			
			TotalRecords totalRecords = new TotalRecords();
			
			LocalDate currentDate = new ZonedDateTimeWriteConverter().convert(ZonedDateTime.now());
			
			totalRecords.setTotalCalloffs(requestsDAO.findRequestsByRequestTypeDepartmentIdAndStartDate(3, workspaceId, currentDate).size()); // count total requests of users
		
			List<Long> openShiftsId = openShiftsDAO.findByDepartmentIdAndOnDate(workspaceId, currentDate); // count total openShift records
			
			totalRecords.setTotalOpenShifts(openShiftsId.size()); // deduce total count of openShifts
			
			int countCallOffs = pendingRequestsList.size(); // count for pending callOffs
			int countOpenShift = 0; //initializing count for pending openShifts

			DateFormatSymbols dateFormatSymbol = new DateFormatSymbols();
			String[] months = dateFormatSymbol.getMonths(); //this will represent date in String format: eg. 09 -> September

			List<PendingRequests> pendingRequestList = new ArrayList<PendingRequests>();
			
			if (pendingDatesList.size() > 0) {
				
				for (LocalDate eachDate : pendingDatesList) {
					
					PendingRequests pendingRequests = new PendingRequests();
					
					pendingRequests.setDate(months[eachDate.getMonthValue()-1] + " " + eachDate.getDayOfMonth() + ","+ eachDate.getYear());
					
					List<Requests> requestsList = requestsDAO.findRequestsByRequestIdAndIsApproved(3, eachDate, workspaceId, "Pending"); // find all pending request records
					
					for (Requests eachRequest: new ArrayList<>(requestsList)) {
					
						Requests request = eachRequest; // assign firstName, lastName and profilePhoto to each user
						
						UserBasicInformation userDetails = userBasicInformationRepository.findUsersByUserId(eachRequest.getUserId());
						
						request.setName(userDetails.getFirstName()+" "+userDetails.getLastName());
						request.setProfilePhoto(userDetails.getProfilePhoto());
						requestsList.remove(eachRequest);
						requestsList.add(request);
					}
					
					if (requestsList.size() > 0)
						pendingRequests.setCalloffs(requestsList);

					List<OpenShifts> openShifts = openShiftsDAO.findByWorkspaceIdAndStatusAndOnDate(workspaceId, eachDate, "Active"); // find all pending open shifts records

					List<Long> openShiftIds = openShifts.stream().map(emitter -> emitter.getOpenShiftId()).collect(Collectors.toList());

					List<OpenShiftInvitees> openShiftInviteesList = openShiftInviteesDAO.findInviteesByOpenShiftIdAndIsApproved(openShiftIds, false); // find all invitees from pending open shift records

					for(OpenShifts openShift: openShifts) {

						List<OpenShiftInvitees> filteredOpenShiftInvitees = openShiftInviteesList.stream().filter(emitter -> emitter.getOpenShiftId() == openShift.getOpenShiftId()).collect(Collectors.toList());

						for(OpenShiftInvitees eachInvitee: new ArrayList<OpenShiftInvitees>(filteredOpenShiftInvitees)) {
							
							UserBasicInformation userDetails = userBasicInformationRepository.findUsersByUserId(eachInvitee.getUserId());
							
							OpenShiftInvitees openShiftInvitees = eachInvitee; // assign firstName, lastName and profilePhoto to each invitee
							
							openShiftInvitees.setName(userDetails.getFirstName()+" "+userDetails.getLastName());
							openShiftInvitees.setProfilePhoto(userDetails.getProfilePhoto());
							filteredOpenShiftInvitees.remove(eachInvitee);
							filteredOpenShiftInvitees.add(openShiftInvitees);
	
						}

						countOpenShift++; // counts pending open shifts
						pendingRequests.setOpenShift(openShift);
						pendingRequests.setOpenshiftInvitees(filteredOpenShiftInvitees);
					}
					
					pendingRequestList.add(pendingRequests);
				}
				
				totalRecords.setPendingCalloffs(countCallOffs);
				totalRecords.setPendingOpenshifts(countOpenShift);
				totalRecords.setPendingRequests(pendingRequestList);
			
				RestResponse restResponse = new RestResponse();

				restResponse.setStatus(200);
				restResponse.setMessage("");
				restResponse.setData(totalRecords);
				restResponse.setSuccess(true);

				return ResponseEntity.ok(restResponse);
			}
			else {
				RestResponse restResponse = new RestResponse();

				restResponse.setStatus(200);
				restResponse.setMessage("No requests of users exist");
				restResponse.setData(null);
				restResponse.setSuccess(true);
				
				return ResponseEntity.ok(restResponse);
			}
		}
		else {

			RestResponse restResponse = new RestResponse();

			restResponse.setStatus(404);
			restResponse.setMessage("Department does not exist");
			restResponse.setData(null);
			restResponse.setSuccess(true);
	
			return ResponseEntity.ok(restResponse);
		}
	}
}