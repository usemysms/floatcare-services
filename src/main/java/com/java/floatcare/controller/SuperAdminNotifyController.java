package com.java.floatcare.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.java.floatcare.api.request.pojo.SuperAdminNotifyRequest;
import com.java.floatcare.api.response.pojo.RestResponse;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.repository.UserBasicInformationRepository;
import com.java.floatcare.utils.EmailThread;
import com.java.floatcare.utils.SMSThread;

@RestController
@RequestMapping("/notify/superadmin")
public class SuperAdminNotifyController {

	@Autowired
	private UserBasicInformationRepository userBasicInformationRepository;
	@Autowired
	private ApplicationContext applicationContext;

	@PostMapping(path = "/for_new_organization")
	public ResponseEntity<RestResponse> sendNotificationToSuperAdmin(@RequestBody SuperAdminNotifyRequest superAdminNotifyRequest) {

		if (superAdminNotifyRequest.getOrganizationId() > 0 && superAdminNotifyRequest.getAccountOwnerId() > 0 && superAdminNotifyRequest.getStaffId() > 0) {

			try {
				UserBasicInformation staffDetails = userBasicInformationRepository.findFirstAndLastNameByUserId(superAdminNotifyRequest.getStaffId());
				UserBasicInformation accountOwnerDetails = userBasicInformationRepository.findFirstAndLastNameByUserId(superAdminNotifyRequest.getAccountOwnerId());
				UserBasicInformation superAdminDetails = userBasicInformationRepository.findFirstByUserTypeId(1);

				if (accountOwnerDetails != null && superAdminDetails != null) {
					String message = "Request to add new organization.";
					String messageBody = "Hi " + superAdminDetails.getFirstName() + ", Account owner "+ accountOwnerDetails.getFirstName() + " " + accountOwnerDetails.getLastName()
							+ " is requesting to add new organization for the staff member, "
							+ staffDetails.getFirstName() + " " + staffDetails.getLastName()
							+ ". You can find this staff member in Admin Portal > Indivisual Professionals.";

					if (superAdminDetails.getEmail() != null) {

						EmailThread emailThread = new EmailThread();
						emailThread.setEmails(superAdminDetails.getEmail());
						emailThread.setMessage(message);
						emailThread.setTemplate(false);
						emailThread.setMessageBody(messageBody);

						applicationContext.getAutowireCapableBeanFactory().autowireBean(emailThread);
						Thread emailthread = new Thread(emailThread); // send email on separate thread.
						emailthread.start(); // start the email thread
					}

					if (superAdminDetails.getPhone() != null) {

						SMSThread smsThread = new SMSThread();
						smsThread.setMessageBody(messageBody);
						smsThread.setPhone(superAdminDetails.getPhone());
						applicationContext.getAutowireCapableBeanFactory().autowireBean(smsThread);
						Thread smsthread = new Thread(smsThread);
						smsthread.start(); // start the sms thread
					}
					
					RestResponse response = new RestResponse();

					response.setData(null);
					response.setMessage("Super admin notified successfully.");
					response.setStatus(200);
					response.setSuccess(true);

					return ResponseEntity.ok(response);
				}else {

					RestResponse response = new RestResponse();

					response.setData(null);
					response.setMessage("Super admin details not exist.");
					response.setStatus(404);
					response.setSuccess(false);

					return ResponseEntity.ok(response);					
				}
			} catch (Exception e) {

				RestResponse response = new RestResponse();

				response.setData(e.getLocalizedMessage());
				e.printStackTrace();
				response.setMessage("Error occured.");
				response.setStatus(500);
				response.setSuccess(false);

				return ResponseEntity.ok(response);
			}
		} else {

			RestResponse response = new RestResponse();

			response.setData(null);
			response.setMessage("Missing organization or staff or account owner details.");
			response.setStatus(400);
			response.setSuccess(false);

			return ResponseEntity.status(400).body(response);
		}
	}
}