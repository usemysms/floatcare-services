package com.java.floatcare.controller;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.java.floatcare.s3bitbucket.AWSService;

@RestController
@RequestMapping(value= "/image")
public class FileUploadController {

	@Autowired
	private AWSService service;

	@PostMapping(value= "/upload/{resourceName}/{resourceId}")
	public ResponseEntity<String> uploadFile(@RequestPart(value= "file") MultipartFile multipartFile, @PathVariable("resourceName") String resourceName, @PathVariable("resourceId") int resourceId) {

		service.uploadFile(multipartFile,resourceName,resourceId);
		
		String response = "[" + multipartFile.getOriginalFilename() + "] uploaded successfully.";
		
		File fileToDelete = FileUtils.getFile(multipartFile.getOriginalFilename());
		FileUtils.deleteQuietly(fileToDelete); // delete the file
		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
}
