package com.java.floatcare.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.java.floatcare.api.request.pojo.UsersEmailRequest;
import com.java.floatcare.api.response.pojo.RestResponse;
import com.java.floatcare.dao.SetScheduleLayoutDAO;
import com.java.floatcare.dao.StaffWeekOffsDAO;
import com.java.floatcare.model.ScheduleLayoutGroups;
import com.java.floatcare.model.StaffWeekOffs;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.repository.RequestsRepository;
import com.java.floatcare.repository.ScheduleLayoutGroupsRepository;
import com.java.floatcare.repository.ScheduleLayoutQuotaRepository;
import com.java.floatcare.repository.ScheduleLayoutRepository;
import com.java.floatcare.repository.SchedulesRepository;
import com.java.floatcare.repository.UserBasicInformationRepository;
import com.java.floatcare.repository.UserCredentialsRepository;
import com.java.floatcare.repository.UserEducationRepository;
import com.java.floatcare.repository.UserNotificationRepository;
import com.java.floatcare.repository.UserOrganizationsRepository;
import com.java.floatcare.repository.UserWorkExperienceRepository;
import com.java.floatcare.repository.UserWorkspacesRepository;
import com.java.floatcare.repository.WorksiteSettingsRepository;
import com.java.floatcare.repository.WorkspaceSettingsRepository;

@RestController
@RequestMapping("delete")
public class DeleteUsersController {

	@Autowired
	private UserBasicInformationRepository userBasicInformationRepository;
	@Autowired
	private UserWorkspacesRepository userWorkspacesRepository;
	@Autowired
	private UserOrganizationsRepository userOrganizationsRepository;
	@Autowired
	private WorksiteSettingsRepository worksiteSettingsRepository;
	@Autowired
	private WorkspaceSettingsRepository workspaceSettingsRepository;
	@Autowired
	private UserCredentialsRepository userCredentialsRepository;
	@Autowired
	private RequestsRepository requestsRepository;
	@Autowired
	private SchedulesRepository schedulesRepository;
	@Autowired
	private SetScheduleLayoutDAO setScheduleLayoutDAO;
	@Autowired
	private UserNotificationRepository userNotificationRepository;
	@Autowired
	private UserWorkExperienceRepository userWorkExperienceRepository;
	@Autowired
	private UserEducationRepository userEducationRepository;
	@Autowired
	private ScheduleLayoutGroupsRepository scheduleLayoutGroupsRepository;
	@Autowired
	private ScheduleLayoutRepository scheduleLayoutRepository;
	@Autowired
	private ScheduleLayoutQuotaRepository scheduleLayoutQuotaRepository;
	@Autowired
	private StaffWeekOffsDAO staffWeekOffsDAO;

	@PostMapping(value = "/users")
	public ResponseEntity<RestResponse> deleteUserFromDatabase(@RequestBody UsersEmailRequest usersEmailRequest) {

		try {
			if (usersEmailRequest != null && usersEmailRequest.getEmails() != null
					&& usersEmailRequest.getEmails().size() > 0) {

				List<UserBasicInformation> users = userBasicInformationRepository
						.findAllUsersByEmailId(usersEmailRequest.getEmails());

				if (users != null && users.size() > 0) {

					List<Long> usersIds = users.stream().map(e -> e.getUserId()).collect(Collectors.toList());

					userBasicInformationRepository.deleteAllByUserId(usersIds);
					userWorkspacesRepository.deleteAllByUserId(usersIds);
					userCredentialsRepository.deleteAllByUserId(usersIds);
					userEducationRepository.deleteAllByUserId(usersIds);
					userWorkExperienceRepository.deleteAllByUserId(usersIds);
					userNotificationRepository.deleteAllByUserId(usersIds);
					worksiteSettingsRepository.deleteAllByUserId(usersIds);
					workspaceSettingsRepository.deleteAllByUserId(usersIds);
					userOrganizationsRepository.deleteAllByUserId(usersIds);
					requestsRepository.deleteAllByUserId(usersIds);
					schedulesRepository.deleteAllByUserId(usersIds);

					List<ScheduleLayoutGroups> scheduleLayoutGroups = scheduleLayoutGroupsRepository
							.findAllByUserId(usersIds);

					if (scheduleLayoutGroups != null && scheduleLayoutGroups.size() > 0) {
						List<Long> scheduleLayoutIds = scheduleLayoutGroups.stream().map(e -> e.getScheduleLayoutId())
								.collect(Collectors.toList());

						scheduleLayoutGroupsRepository.deleteAllByScheduleLayoutId(scheduleLayoutIds);
						scheduleLayoutRepository.deleteAllByScheduleLayoutId(scheduleLayoutIds);
						scheduleLayoutQuotaRepository.deleteAllByScheduleLayoutId(scheduleLayoutIds);
					}

					List<StaffWeekOffs> staffWeekOffs = staffWeekOffsDAO.getSetScheduleLayoutIdsByUserIds(usersIds);

					if (staffWeekOffs != null && staffWeekOffs.size() > 0) {

						List<Long> setScheduleLayoutIds = staffWeekOffs.stream().map(e -> e.getSetScheduleLayoutId())
								.collect(Collectors.toList());
						setScheduleLayoutDAO.deleteAllBySetScheduleLayoutIds(setScheduleLayoutIds);
						staffWeekOffsDAO.removeBySetScheduleLayoutIds(setScheduleLayoutIds);
					}

					RestResponse response = new RestResponse();

					response.setData(null);
					response.setMessage(usersIds.size() + " users deleted successfully.");
					response.setStatus(200);
					response.setSuccess(true);

					return ResponseEntity.ok(response);
				} else {

					RestResponse response = new RestResponse();

					response.setData(null);
					response.setMessage("No users found.");
					response.setStatus(404);
					response.setSuccess(false);

					return ResponseEntity.ok(response);
				}
			} else {

				RestResponse response = new RestResponse();

				response.setData(null);
				response.setMessage("Emails not provided.");
				response.setStatus(400);
				response.setSuccess(false);

				return ResponseEntity.ok(response);
			}
		} catch (Exception e) {

			RestResponse response = new RestResponse();

			response.setData(null);
			response.setMessage(e.getLocalizedMessage());
			response.setStatus(500);
			response.setSuccess(false);

			return ResponseEntity.ok(response);
		}
	}
}