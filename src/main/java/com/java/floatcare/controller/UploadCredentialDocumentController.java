package com.java.floatcare.controller;

import java.io.File;
import java.util.Optional;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.java.floatcare.api.response.pojo.RestResponse;
import com.java.floatcare.model.UserCredentials;
import com.java.floatcare.repository.UserCredentialsRepository;
import com.java.floatcare.s3bitbucket.AWSService;

@Controller
@RequestMapping("/upload")
public class UploadCredentialDocumentController {
	
	@Autowired
	private AWSService awsService;
	@Autowired
	private UserCredentialsRepository userCredentialsRepository;
	
	private String uniqueFileName;
	private String frontImageName;
	private String backImageName;
	
	public void setUniqueFileName(String fileName) {
		this.uniqueFileName = fileName;
	}
	
	public String getUniqueFileName() {
		return uniqueFileName;
	}
	
	public String getFrontImageName() {
		return frontImageName;
	}

	public void setFrontImageName(String frontImageName) {
		this.frontImageName = frontImageName;
	}

	public String getBackImageName() {
		return backImageName;
	}

	public void setBackImageName(String backImageName) {
		this.backImageName = backImageName;
	}

	@RequestMapping(value = "/file/{userCredentialId}", method = RequestMethod.POST)
	public ResponseEntity<Object> uploadFile(@RequestParam("file") MultipartFile file, @PathVariable long userCredentialId) {

		String message = "";

		try {
			
			File fileToDelete = FileUtils.getFile(file.getOriginalFilename());
			
			/*File files = new File("floatcare/" + file.getOriginalFilename());
			files.createNewFile();
			FileOutputStream fout = new FileOutputStream(files);
			fout.write(file.getBytes());
			fout.close();*/
			
			if (file.getSize()/1000000 < 11) {
				
				Optional<UserCredentials> userCredentialOptional = userCredentialsRepository.findById(userCredentialId);
				
				if (userCredentialOptional.isPresent()) {
					UserCredentials userCredential = userCredentialOptional.get();
				
					awsService.uploadFile(file, file.getOriginalFilename(), 4); //upload the file in aws
					
					if (uniqueFileName != null) 
						userCredential.setFileName(uniqueFileName);
					else
						userCredential.setFileName(null);
					
					//files.delete(); //delete the file from local
					FileUtils.deleteQuietly(fileToDelete); //delete the file from local
					message = "Uploaded the file successfully: " + uniqueFileName; //set status
					
					userCredentialsRepository.save(userCredential); //save the file name
					
					RestResponse restResponse = new RestResponse();
					
					restResponse.setStatus(200);
					restResponse.setMessage(message);
					restResponse.setSuccess(true);
					
					return ResponseEntity.status(HttpStatus.OK).body(restResponse);
				}
				else {
					
					//files.delete(); // delete the file
					FileUtils.deleteQuietly(fileToDelete); //delete the file from local
					
					RestResponse restResponse = new RestResponse();
					
					restResponse.setStatus(404);
					restResponse.setMessage("User Credential Id does not exist");
					restResponse.setSuccess(false);
					
					return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(restResponse);
				}

			}
			
			else {
					FileUtils.deleteQuietly(fileToDelete); //delete the file from local
					RestResponse restResponse = new RestResponse();
				
					restResponse.setStatus(400);
					restResponse.setData(null);
					restResponse.setMessage("Cannot upload file size greater than 10MB");
					restResponse.setSuccess(false);
							
					return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(restResponse);
			}
		
		} catch (Exception e) {
			File fileToDelete = FileUtils.getFile(file.getOriginalFilename());
			
			FileUtils.deleteQuietly(fileToDelete); //delete the file from local
			e.printStackTrace();
			message = "Could not upload the file: " + file.getOriginalFilename() + "!";
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.fillInStackTrace());
		}
	}
	
	@RequestMapping(value = "/frontImage/{userCredentialId}", method = RequestMethod.POST)
	public ResponseEntity<Object> uploadFrontImage(@RequestParam("file") MultipartFile file, @PathVariable long userCredentialId) {

		String message = "";

		try {
			
			File fileToDelete = FileUtils.getFile(file.getOriginalFilename());
			
			/*File files = new File("floatcare/" + file.getOriginalFilename());
			files.createNewFile();
			FileOutputStream fout = new FileOutputStream(files);
			fout.write(file.getBytes());
			fout.close();*/
			
			if (file.getSize()/1000000 < 11) {
				
				System.out.println(file.getSize());
				Optional<UserCredentials> userCredentialOptional = userCredentialsRepository.findById(userCredentialId);
				
				if (userCredentialOptional.isPresent()) {
					UserCredentials userCredential = userCredentialOptional.get();
					
					awsService.uploadFile(file, file.getOriginalFilename(), 5); //upload the file in aws
					
					if (frontImageName != null) 
						userCredential.setFrontFilePhoto(this.getFrontImageName());
					else
						userCredential.setFrontFilePhoto(null);
					
					//files.delete(); //delete the file from local
					FileUtils.deleteQuietly(fileToDelete); //delete the file from local
					message = "Uploaded the file successfully: " + frontImageName; //set status
					
					userCredentialsRepository.save(userCredential); //save the file name
					
					RestResponse restResponse = new RestResponse();
					
					restResponse.setStatus(200);
					restResponse.setMessage(message);
					restResponse.setSuccess(true);
					
					return ResponseEntity.status(HttpStatus.OK).body(restResponse);
				}
				else {
					
					//files.delete(); // delete the file
					FileUtils.deleteQuietly(fileToDelete); //delete the file from local
					
					RestResponse restResponse = new RestResponse();
					
					restResponse.setStatus(404);
					restResponse.setMessage("User Credential Id does not exist");
					restResponse.setSuccess(false);
					
					return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(restResponse);
				}

			}
			
			else {
					FileUtils.deleteQuietly(fileToDelete); //delete the file from local
					RestResponse restResponse = new RestResponse();
				
					restResponse.setStatus(400);
					restResponse.setData(null);
					restResponse.setMessage("Cannot upload file size greater than 10MB");
					restResponse.setSuccess(false);
							
					return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(restResponse);
			}
		
		} catch (Exception e) {
			File fileToDelete = FileUtils.getFile(file.getOriginalFilename());
			
			FileUtils.deleteQuietly(fileToDelete); //delete the file from local
			e.printStackTrace();
			message = "Could not upload the file: " + file.getOriginalFilename() + "!";
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.fillInStackTrace());
		}
	}
	
	@RequestMapping(value = "/backImage/{userCredentialId}", method = RequestMethod.POST)
	public ResponseEntity<Object> uploadBackImage(@RequestParam("file") MultipartFile file, @PathVariable long userCredentialId) {

		String message = "";

		try {
			
			File fileToDelete = FileUtils.getFile(file.getOriginalFilename());
			
			/*File files = new File("floatcare/" + file.getOriginalFilename());
			files.createNewFile();
			FileOutputStream fout = new FileOutputStream(files);
			fout.write(file.getBytes());
			fout.close();*/
			
			if (file.getSize()/1000000 < 11) {
				
				Optional<UserCredentials> userCredentialOptional = userCredentialsRepository.findById(userCredentialId);
				
				if (userCredentialOptional.isPresent()) {
					
					UserCredentials userCredential = userCredentialOptional.get();
					
					awsService.uploadFile(file, file.getOriginalFilename(), 6); //upload the file in aws
					
					if (backImageName != null) 
						userCredential.setBackFilePhoto(this.getBackImageName());
					else
						userCredential.setBackFilePhoto(null);
					
					//files.delete(); //delete the file from local
					FileUtils.deleteQuietly(fileToDelete); //delete the file from local
					message = "Uploaded the file successfully: " + backImageName; //set status
					
					userCredentialsRepository.save(userCredential); //save the file name
					
					RestResponse restResponse = new RestResponse();
					
					restResponse.setStatus(200);
					restResponse.setMessage(message);
					restResponse.setSuccess(true);
					
					return ResponseEntity.status(HttpStatus.OK).body(restResponse);
				}
				else {
					
					//files.delete(); // delete the file
					FileUtils.deleteQuietly(fileToDelete); //delete the file from local
					
					RestResponse restResponse = new RestResponse();
					
					restResponse.setStatus(404);
					restResponse.setMessage("User Credential Id does not exist");
					restResponse.setSuccess(false);
					
					return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(restResponse);
				}

			}
			
			else {
					FileUtils.deleteQuietly(fileToDelete); //delete the file from local
					RestResponse restResponse = new RestResponse();
				
					restResponse.setStatus(400);
					restResponse.setData(null);
					restResponse.setMessage("Cannot upload file size greater than 10MB");
					restResponse.setSuccess(false);
							
					return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(restResponse);
			}
		
		} catch (Exception e) {
			File fileToDelete = FileUtils.getFile(file.getOriginalFilename());
			
			FileUtils.deleteQuietly(fileToDelete); //delete the file from local
			e.printStackTrace();
			message = "Could not upload the file: " + file.getOriginalFilename() + "!";
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.fillInStackTrace());
		}
	}
}
