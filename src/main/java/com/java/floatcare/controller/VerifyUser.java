package com.java.floatcare.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.java.floatcare.api.request.pojo.UserVerification;
import com.java.floatcare.api.response.pojo.RestResponse;
import com.java.floatcare.dao.UserWorkspacesDAO;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.model.UserWorkspaces;
import com.java.floatcare.repository.UserBasicInformationRepository;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/verify")
@Slf4j
public class VerifyUser {

	@Autowired
	private UserWorkspacesDAO userWorkspacesDAO;
	@Autowired
	private UserBasicInformationRepository userBasicInformationRepository;

	@RequestMapping(value = "/user", method = RequestMethod.POST)
	public ResponseEntity<Object> checkUserByEmailAndDepartmentId(@RequestBody UserVerification userRequest) {

		Optional<UserBasicInformation> userOpt = userBasicInformationRepository.findUserByEmail(userRequest.getEmail());

		if (userOpt.isPresent()) {
			UserBasicInformation user = userOpt.get();
			if (user.getStatus().equals("Active")) {

				UserWorkspaces userOptional = userWorkspacesDAO
						.findByDepartmentIdAndUserId(userRequest.getDepartmentId(), userOpt.get().getUserId());

				if (userOptional != null) {

					RestResponse restResponse = new RestResponse();
					restResponse.setMessage(userRequest.getEmail() + " email already exists in database");
					restResponse.setData(userOptional);
					restResponse.setStatus(403);
					restResponse.setSuccess(false);
					return ResponseEntity.ok(restResponse);
				} else {
					RestResponse restResponse = new RestResponse();
					restResponse.setMessage("DepartmentId: " + userRequest.getDepartmentId()
							+ " is not associated with " + userRequest.getEmail());
					restResponse.setStatus(200);
					restResponse.setSuccess(true);
					restResponse.setData(userOpt.get());
					return ResponseEntity.ok(restResponse);

				}
			} else {
				RestResponse restResponse = new RestResponse();
				restResponse.setMessage("User is inactive");
				restResponse.setStatus(404);
				restResponse.setSuccess(true);
				return ResponseEntity.ok(restResponse);
			}

		} else {

			RestResponse restResponse = new RestResponse();
			restResponse.setMessage("Email: " + userRequest.getEmail() + " is not available.");
			restResponse.setStatus(404);
			restResponse.setSuccess(true);
			return ResponseEntity.ok(restResponse);
		}

	}

	@PostMapping("/emailWithUserId")
	public ResponseEntity<Object> verifyEmailWithUserId(@RequestBody UserVerification userRequest) {

		log.info("Inside verifyEmailWithUserId of VerifyUser");

		RestResponse restResponse = new RestResponse();
		userBasicInformationRepository.findUserByEmail(userRequest.getEmail()).ifPresentOrElse((user) -> {
			if(user.getUserId()==userRequest.getUserId()) {
				restResponse.setMessage("EmailId is associated with given UserId");
				restResponse.setSuccess(true);
				restResponse.setStatus(200);
			}else {
				restResponse.setMessage("User already exists with this email address");
				restResponse.setSuccess(false);
				restResponse.setStatus(403);
			}
		}, () -> {
			restResponse.setMessage("EmailId is available to use");
			restResponse.setSuccess(true);
			restResponse.setStatus(200);
		});

		return ResponseEntity.status(restResponse.getStatus()).body(restResponse);
	}
}