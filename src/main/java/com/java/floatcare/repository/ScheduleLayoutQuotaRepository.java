package com.java.floatcare.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.java.floatcare.model.ScheduleLayoutQuota;

public interface ScheduleLayoutQuotaRepository extends MongoRepository<ScheduleLayoutQuota, Long> {

	List<ScheduleLayoutQuota> findByScheduleLayoutId(long scheduleLayoutId);

	void deleteByScheduleLayoutId(long scheduleLayoutId);

	@Query(value = "{scheduleLayoutId: {$in:?0}}", delete = true)
	void deleteAllByScheduleLayoutId(List<Long> scheduleLayoutIds);

}
