package com.java.floatcare.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.java.floatcare.model.SwapShiftRequests;

public interface SwapShiftRequestsRepository extends MongoRepository<SwapShiftRequests, Long>{

	List<SwapShiftRequests> findBySenderIdAndStatus(long userId, String status);

	List<SwapShiftRequests> findByReceiverIdAndStatus(long userId, String status);

	List<SwapShiftRequests> findBySenderId(long userId);

}
