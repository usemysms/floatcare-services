package com.java.floatcare.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.java.floatcare.model.UserWorkExperience;

public interface UserWorkExperienceRepository extends MongoRepository<UserWorkExperience, Long> {

	Optional<UserWorkExperience> findUserWorkExperienceByUserId(long userId);

	List<UserWorkExperience> findByUserId(long userId);

	@Query(value = "{userId: {$in:?0}}", delete = true)
	void deleteAllByUserId(List<Long> usersIds);
}
