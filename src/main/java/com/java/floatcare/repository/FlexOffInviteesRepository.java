package com.java.floatcare.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.java.floatcare.model.FlexOffInvitees;

public interface FlexOffInviteesRepository extends MongoRepository<FlexOffInvitees, Long> {

	Optional<FlexOffInvitees> findByUserIdAndFlexOffId(long userId, long flexOffId);

	List<FlexOffInvitees> findByFlexOffId(long flexOffId);

	void deleteByFlexOffId(long flexOffId);

}
