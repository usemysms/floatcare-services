package com.java.floatcare.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.java.floatcare.model.CredentialSubTypes;

@Repository
public interface CredentialSubTypeRepository extends MongoRepository<CredentialSubTypes, Long>{

	Optional<CredentialSubTypes> findCredentialSubTypesByCredentialSubTypeId(long credentialSubTypeId);

	List<CredentialSubTypes> findByCredentialTypeId (long credentialTypeId);
		
	@Query("{'staffCoreTypes':?0}")
	List<CredentialSubTypes> findByStaffCoreType(long staffCoreType);
	
	@Query("{'_id':?0,'organizationId':?1}")
	Optional<CredentialSubTypes> findByIdAndOrganizationId (long credentialSubTypeId,long organizationId);
		
}
