package com.java.floatcare.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.java.floatcare.model.Availability;

public interface AvailabilityRepository extends MongoRepository<Availability, Long> {

	
	List<Availability> findByStartDate(LocalDate startDate);

	Availability findByUserId(Long userId);

	Availability findByUserIdAndScheduleLayoutId(long userId, long scheduleLayoutId);

	@Query(value = "{scheduleLayoutId: ?0}", delete = true)
	void deleteByScheduleLayoutId(long scheduleLayoutId);

	Availability findFirstByUserIdAndScheduleLayoutId(long userId, long scheduleLayoutId);
}
