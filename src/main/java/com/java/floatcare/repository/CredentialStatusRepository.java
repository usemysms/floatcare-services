package com.java.floatcare.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.java.floatcare.model.CredentialStatus;

@Repository
public interface CredentialStatusRepository extends MongoRepository<CredentialStatus, Long>{

	Optional<CredentialStatus> findCredentialStatusByCredentialStatusId(long credentialStatusId);

}
