package com.java.floatcare.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.java.floatcare.model.EmailVerification;

public interface EmailVerificationRepository extends MongoRepository<EmailVerification, Long> {

	EmailVerification findByEmailAndOtp(String email, String otp);

	EmailVerification findByEmail(String email);

	EmailVerification findByOtp(String otp);

	List<EmailVerification> findAllByEmail(String email);

	void deleteAllByEmail(String email);

}
