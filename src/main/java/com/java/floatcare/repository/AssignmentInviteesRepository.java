package com.java.floatcare.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.java.floatcare.model.AssignmentInvitees;

public interface AssignmentInviteesRepository extends MongoRepository<AssignmentInvitees, Long>{

}
