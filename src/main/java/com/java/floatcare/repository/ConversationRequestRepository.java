package com.java.floatcare.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.java.floatcare.model.RequestConversation;

public interface ConversationRequestRepository extends MongoRepository<RequestConversation, Long> {

}
