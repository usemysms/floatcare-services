package com.java.floatcare.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.java.floatcare.model.WorksiteSettings;

public interface WorksiteSettingsRepository extends MongoRepository<WorksiteSettings, Long>{

	List<WorksiteSettings> findByUserId(long userId);

	WorksiteSettings findByUserIdAndWorksiteId(long userId, long worksiteId);

	@Query(value = "{userId: {$in:?0}}", delete = true)
	void deleteAllByUserId(List<Long> usersIds);

}
