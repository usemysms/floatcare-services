package com.java.floatcare.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.java.floatcare.model.BusinessTypes;

public interface BusinessTypesRepository extends MongoRepository<BusinessTypes, Long> {

}
