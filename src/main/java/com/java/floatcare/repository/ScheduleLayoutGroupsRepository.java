package com.java.floatcare.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.java.floatcare.model.ScheduleLayoutGroups;

public interface ScheduleLayoutGroupsRepository extends MongoRepository<ScheduleLayoutGroups, Long> {

	List<ScheduleLayoutGroups> findByDepartmentId(long departmentId);

	List<ScheduleLayoutGroups> findByScheduleLayoutId(long scheduleLayoutId);

	void deleteByScheduleLayoutId(long scheduleLayoutId);

	@Query(value = "{scheduleLayoutId: {$in:?0}}", delete = true)
	void deleteAllByScheduleLayoutId(List<Long> scheduleLayoutIds);

	@Query(value = "{userIds:{$in: ?0}}")
	List<ScheduleLayoutGroups> findAllByUserId(List<Long> usersIds);

}
