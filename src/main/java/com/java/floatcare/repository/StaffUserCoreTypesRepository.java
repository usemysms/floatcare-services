package com.java.floatcare.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.java.floatcare.model.StaffUserCoreTypes;

public interface StaffUserCoreTypesRepository extends MongoRepository<StaffUserCoreTypes, Long> {

	StaffUserCoreTypes findStaffUserCoreTypesById(long staffUserCoreTypeId);

	StaffUserCoreTypes findFirstByLabel(String staffUserCoreTypeName);

}
