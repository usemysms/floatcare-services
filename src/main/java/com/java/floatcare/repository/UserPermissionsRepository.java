package com.java.floatcare.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.java.floatcare.model.UserPermissions;

public interface UserPermissionsRepository extends MongoRepository<UserPermissions, Long>{

}
