package com.java.floatcare.repository;

import java.util.List;
import java.util.TreeSet;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.java.floatcare.model.DeletedMessages;

public interface DeletedMessagesRepository extends MongoRepository<DeletedMessages, Long> {

	DeletedMessages findByUserIdAndReceiverMessagesDeleteduserIdAndIsReplied(long senderId, long receiverId, boolean b);

	DeletedMessages findByUserIdAndReceiverMessagesDeleteduserId(int senderId, int receiverId);

	DeletedMessages findByUserIdAndGroupId(TreeSet<Long> groupMembers, Long groupId);

	@Query(value = "{ 'userId' : {'$in' : ?0 }, 'groupId' : ?1 }", fields = "{ 'groupId': 1 }")
	List<DeletedMessages> findAllByUserIdAndGroupId(TreeSet<Long> groupMembersIds, Long groupId);

	DeletedMessages findByUserIdAndGroupId(long userId, long groupId);

	DeletedMessages findByUserIdAndReceiverMessagesDeleteduserId(long userId, long receiverId);

	@Query(fields = "{messageIds: { $slice: -1 }}")
	List<DeletedMessages> findAllByUserIdAndGroupId(long userId, long groupId);

}
