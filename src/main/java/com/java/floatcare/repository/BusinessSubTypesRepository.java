package com.java.floatcare.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.java.floatcare.model.BusinessSubTypes;

public interface BusinessSubTypesRepository extends MongoRepository<BusinessSubTypes, Long>{

}
