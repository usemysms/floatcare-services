package com.java.floatcare.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.java.floatcare.model.UserCredentials;

public interface UserCredentialsRepository extends MongoRepository<UserCredentials, Long>{

	Optional<UserCredentials> findUserCredentialsByUserId(long userId);

	List<UserCredentials> findByUserId(long userId);

	List<UserCredentials> findByUserIdAndCredentialSubTypeId(long userId, long credentialSubTypeId);
	
	List<UserCredentials> findByCredentialSubTypeId(long credentialSubTypeId);

	@Query(value = "{userId: {$in:?0}}", delete = true)
	void deleteAllByUserId(List<Long> usersIds);

}
