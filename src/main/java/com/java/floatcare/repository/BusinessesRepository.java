package com.java.floatcare.repository;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.java.floatcare.model.Businesses;
import com.java.floatcare.model.UserWorkspaces;

public interface BusinessesRepository extends MongoRepository<Businesses, Long> {

	LinkedList<Businesses> findBusinessesByName(String name);

	List<Businesses> findBusinessByEmail(String email);

	List<Businesses> findBusinessesByOrganizationId(long organizationId);
	
	List<Businesses> findBusinessesByBusinessTypeId(long businessTypeId);
	
	List<Businesses> findBusinessByBusinessId(long businessId);

	List<Businesses> findBusinessesByOrganizationIdAndPostalCodeAndCity(long organizationId, String zip, String city);

	List<Businesses> findBusinessesByBusinessTypeIdAndOrganizationId(long businessTypeId, long organizationId);

	Businesses findByNameAndAddress(String name, String address);

	@Query(value = "{organizationId:1, businessId:1, name:1}")
	List<Businesses> findAllByBusinessIds(List<Long> worksiteIds);

	Businesses findByBusinessId(long businessId);

	@Query(value = "{'preferredProviderIds':{$in:[?0]}, _id: ?1}", fields = "{'preferredProviderIds':1}")
	Businesses findPreferredProviderIdByUserIdAndBusinessId(long userId, long businessId);

	@Query(fields = "{'preferredProviderIds':1}")
	List<Businesses> findAllBusinessesByBusinessIdNot(long businessId);
	
	@Query(value = "{businessId: {$in:?0}}")
	List<Businesses> findAllByBusinessId(List<Long> businessIds);

	@Query(value = "{$and:[{'name':{$regex: ?1, $options: 'i'}}, {businessTypeId: ?0}]}")
	List<Businesses> findBusinessesByBusinessTypeIdAndName(long businessTypeId, String name);

}
