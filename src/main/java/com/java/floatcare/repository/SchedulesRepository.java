package com.java.floatcare.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.java.floatcare.model.Schedules;

public interface SchedulesRepository extends MongoRepository<Schedules, Long> {

	List<Schedules> findByUserId(long userId);

	Schedules findSchedulesByUserIdAndShiftDateAndDepartmentId(long userId, LocalDate parse, long departmentId);

	Schedules findSchedulesByUserIdAndShiftDateAndDepartmentIdAndStatus(long userId, LocalDate parse, long departmentId, String status);

	@Query(value="{'selfScheduleId' : ?0}", delete = true)
	void deleteSchedulesById(long scheduleLayoutId);

	@Query(value = "{userId: {$in:?0}}", delete = true)
	void deleteAllByUserId(List<Long> usersIds);

}
