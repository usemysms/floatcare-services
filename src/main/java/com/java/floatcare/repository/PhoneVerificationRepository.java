package com.java.floatcare.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.java.floatcare.model.PhoneVerification;

public interface PhoneVerificationRepository extends MongoRepository<PhoneVerification, Long> {
	List<PhoneVerification> findByPhoneNumber(String phoneNumber);

}
