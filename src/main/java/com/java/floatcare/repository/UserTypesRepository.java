package com.java.floatcare.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.java.floatcare.model.UserTypes;

public interface UserTypesRepository extends MongoRepository<UserTypes, Long>{
	
	Optional<UserTypes> findByUserTypeId(long userTypeId);
	
}
