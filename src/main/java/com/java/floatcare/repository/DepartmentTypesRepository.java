package com.java.floatcare.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.java.floatcare.model.DepartmentTypes;

public interface DepartmentTypesRepository extends MongoRepository<DepartmentTypes, Long> {

}
