package com.java.floatcare.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.java.floatcare.model.Organizations;

public interface OrganizationRepository extends MongoRepository<Organizations, Long> {

	List<Organizations> findByOrganizationId (long organizationId);

	Optional<Organizations> findOrganizationByEmail(String email);

	@Query(fields = "{name:1}")
	List<Organizations> findAllByOrganizationId(List<Long> organizationsIds);

	@Query(value = "{name: {$regex: ?0, $options:'i'}}")
	List<Organizations> findOrganizationsByName(String name);
	
	List<Organizations> findAllByOrganizationTypeNot(String organizationType);
}