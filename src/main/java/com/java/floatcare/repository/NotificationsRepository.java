package com.java.floatcare.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.java.floatcare.model.Notifications;

public interface NotificationsRepository extends MongoRepository<Notifications, Long> {

	List<Notifications> findByUserId(long userId);

	Notifications findByUserIdAndNotificationId(long userId, long notificationId);

	List<Notifications> findByUserIdAndNotificationTypeId(long userId, long notificationTypeId);

	boolean deleteByUserIdAndSourceId(long userId, long sourceId);

}
