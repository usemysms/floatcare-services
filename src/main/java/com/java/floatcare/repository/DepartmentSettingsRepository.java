package com.java.floatcare.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.java.floatcare.model.DepartmentSettings;

public interface DepartmentSettingsRepository extends MongoRepository<DepartmentSettings, Long> {

	DepartmentSettings findByDepartmentId(long departmentId);

}