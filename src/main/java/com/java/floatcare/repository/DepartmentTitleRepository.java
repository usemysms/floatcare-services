package com.java.floatcare.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.java.floatcare.model.DepartmentTitle;
import com.java.floatcare.model.DepartmentTypes;

public interface DepartmentTitleRepository extends MongoRepository<DepartmentTitle, Long> {

	Optional<DepartmentTitle> findDepartmentTitleByOrganizationId(long organizationId);

	Optional<DepartmentTypes> findDepartmentTypesIdByDepartmentTitleId(long departmentTitleId);

}
