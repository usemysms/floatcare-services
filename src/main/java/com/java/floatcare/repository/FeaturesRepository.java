package com.java.floatcare.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.java.floatcare.model.Features;

public interface FeaturesRepository extends MongoRepository<Features, Integer> {

}
