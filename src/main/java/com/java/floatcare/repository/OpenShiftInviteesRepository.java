package com.java.floatcare.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.java.floatcare.model.OpenShiftInvitees;

public interface OpenShiftInviteesRepository extends MongoRepository<OpenShiftInvitees, Long>{

	List<OpenShiftInvitees> findByOpenShiftId(long openShiftId);
	
	Optional<OpenShiftInvitees> findByUserId(long userId);

	Optional<OpenShiftInvitees> findByUserIdAndOpenShiftId(long userId, long openShiftId);

	long countByOpenShiftIdAndIsApproved(long openShiftId, boolean isApproved);

	@Query(fields = "{openShiftId:1}")
	List<OpenShiftInvitees> findByOpenShiftIdAndIsAcceptedAndIsApproved(long openShiftId, boolean isAccepted, boolean isApproved);

}
