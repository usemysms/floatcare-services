package com.java.floatcare.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.java.floatcare.model.ScheduleLayout;
import com.java.floatcare.model.StaffWeekOffs;

public interface StaffWeekOffsRepository extends MongoRepository<StaffWeekOffs, Long> {
}
