package com.java.floatcare.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.java.floatcare.model.LoginOtp;

public interface LoginOtpRepository extends MongoRepository<LoginOtp, String>{

}
