package com.java.floatcare.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.java.floatcare.model.UserPrivacySettings;

@Repository
public interface UserPrivacySettingsRepository extends MongoRepository<UserPrivacySettings, Long> {

	List<UserPrivacySettings> findByUserId(long userId);
}
