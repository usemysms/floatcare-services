package com.java.floatcare.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.java.floatcare.model.DepartmentStaff;

public interface DepartmentStaffRepository extends MongoRepository<DepartmentStaff, Long> {

	List<DepartmentStaff> findDepartmentStaffByDepartmentId(long businessId);

	void deleteByDepartmentId(long departmentId);

	boolean existsByStaffUserCoreTypeId(long staffUserCoreTypeId);

	List<DepartmentStaff> findDepartmentStaffByStaffUserCoreTypeId(long staffUserCoreTypesId);

	DepartmentStaff findByDepartmentIdAndStaffUserCoreTypeId(long workspaceId, long staffUserCoreTypeId);

}
