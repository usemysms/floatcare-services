package com.java.floatcare.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.java.floatcare.model.UserTypes;

public interface UserTypeRepository extends MongoRepository<UserTypes, Long> {

}
