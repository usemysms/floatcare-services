package com.java.floatcare.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.java.floatcare.model.Department;

@Repository
public interface DepartmentRepository extends MongoRepository<Department, Long> {

	List<Department> findDepartmentByBusinessId(long businessId);
	
	List<Department> findDepartmentByEmail(String email);

	List<Department> findDepartmentByDepartmentId(long departmentId);
	
	void deleteByBusinessId(long businessId);

	List<String> findDepartmentTypeIdByDepartmentId(long departmentId);

	@Query(value = "{departmentId:1, name:1}")
	List<Department> findAllDepartmentById(List<Long> departmentIds);

	@Query(fields = "{businessId:1, departmentId:1}")
	Department findByDepartmentId(long departmentId);

}
