package com.java.floatcare.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.java.floatcare.model.Requests;

public interface RequestsRepository extends MongoRepository<Requests, Long> {

	List<Requests> findByUserId(long userId);

	Optional<Requests> findByRequestIdAndStatus(long requestId, String status);

	@Query(value = "{userId: {$in:?0}}", delete = true)
	void deleteAllByUserId(List<Long> usersIds);

}
