package com.java.floatcare.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.java.floatcare.model.SubRoleTypes;

public interface SubRoleTypesRepository extends MongoRepository<SubRoleTypes, Long>{

	@Query(sort = "{ 'label': 1}")
	List<SubRoleTypes> findByParentId(long staffUserCoreTypeId);

	SubRoleTypes findFirstByLabel(String bioInformation);

	@Query(value = "{$and:[{'parentId' :?0},{'specialist': {$exists: true}}]}", fields = "{'specialist':1}", sort = "{ 'specialist': 1}")
	List<SubRoleTypes> findAllSpecialistByParentId(int parentId);
}
