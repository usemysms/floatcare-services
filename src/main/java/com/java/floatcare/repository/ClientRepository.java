package com.java.floatcare.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.java.floatcare.model.Clients;

@Repository
public interface ClientRepository extends MongoRepository<Clients, Long> {
	
}
