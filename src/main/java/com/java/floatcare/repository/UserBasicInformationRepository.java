package com.java.floatcare.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.java.floatcare.model.Organizations;
import com.java.floatcare.model.StaffUserCoreTypes;
import com.java.floatcare.model.UserBasicInformation;

@Repository
public interface UserBasicInformationRepository extends MongoRepository<UserBasicInformation, Long> {

	Optional<UserBasicInformation> findUserByEmail(String email);

	UserBasicInformation findFirstUserByPhone(String phone);

	List<UserBasicInformation> findUserByFirstName(String firstName);
	
	@Query(value = "{$and: [{'firstName': {$regex: ?0, $options:'i'}}, {'staffUserCoreTypeId': ?1}]}")
	List<UserBasicInformation> findAllByFirstNameAndStaffUserCoreTypeId(String firstName, long staffUserCoreTypeId);
	
	@Query(value = "{$and: [{$or:[{'firstName': {$regex: ?0, $options:'i'}}, {'lastName': {$regex: ?1, $options:'i'}}]}, {'staffUserCoreTypeId': ?2}]}")
	List<UserBasicInformation> findAllByFirstNameAndLastNameAndStaffUserCoreTypeId(String firstName, String lastName, long staffUserCoreTypeId);

	List<UserBasicInformation> findUsersByOrganizationId(long organizationId);

	Optional<UserBasicInformation> findUserByUserId(long userId);

	UserBasicInformation findUsersByUserId(long userId);

	StaffUserCoreTypes findStaffUserCoreTypeNameByUserId(long userId);

	List<Organizations> findOrganizationsByOrganizationId(long organizationId);

	List<UserBasicInformation> findUsersByStaffUserCoreTypeId(long staffUserCoreTypesId);

	@Query(value = "{'user_basic_information.staffUserCoreTypeId':?0,'user_basic_information.organizationId':?1}")
	List<UserBasicInformation> findUsersBytSaffUserCoreTypeIdAndOrganizationId(long staffUserCoreTypeId, long organizationId);
	
	//@Query(value = "{'user_basic_information.email':?0,'user_basic_information.organizationId':?1}")
	List<UserBasicInformation> findUsersByEmailAndOrganizationId(String email,long organizationId);

	Optional<UserBasicInformation> findByUserIdAndStatus(Long userId, String status);

	long findByUserIdAndUserTypeId(long userId, int userTypeId);

	List<UserBasicInformation> findByUserTypeId(long userTypeId);

	Optional<UserBasicInformation> findUserByEmailAndStatus(String email, String status);

	List<UserBasicInformation> findByUserTypeIdNotAndStatus(int userTypeId, String status);

	UserBasicInformation findByOrganizationIdAndUserTypeId(long organizationId, int userTypeId);

	@Query(value = "{'organizationId': ?0, 'userTypeId': {$not: {$eq :?1}}}", fields = "{userId:1, firstName:1, lastName:1, profilePhoto:1, status:1, email:1, phone: 1, staffUserCoreTypeId:1}" )
	List<UserBasicInformation> findUsersByOrganizationIdAndUserTypeIdNot(int organizationId, int userTypeId);

	@Query(value = "{$and: [{$or:[{{'firstName': {$regex: ?0, $options:'i'}}, {'lastName': {$regex: ?1, $options:'i'}}}]}, {'staffUserCoreTypeId': ?2}, {'phone':?2}, {'staffUserSubCoreTypeId': {$ne: ?3}}]}", fields = "{firstName:1, lastName:1, profilePhoto:1, staffUserCoreTypeName:1, email:1, phone:1, address1:1, address2: 1, city:1, state:1, organizationId:1}")
	List<UserBasicInformation> findByFirstNameAndLastNameAndStaffUserCoreTypeIdAndPhoneAndStaffUserSubCoreTypeIdNot(String firstName, String lastName, int i, String phone, int j);
	
	@Query(value = "{$and: [{$or:[{{'firstName': {$regex: ?0, $options:'i'}}, {'lastName': {$regex: ?1, $options:'i'}}}]}, {'staffUserCoreTypeId': ?2}, {'phone':?2}, {'staffUserSubCoreTypeId': ?3}]}", fields = "{firstName:1, lastName:1, profilePhoto:1, staffUserCoreTypeName:1, email:1, phone:1, address1:1, address2: 1, city:1, state:1, organizationId:1}")
	List<UserBasicInformation> findByFirstNameAndLastNameAndStaffUserCoreTypeIdAndPhoneAndStaffUserSubCoreTypeId(String firstName, String lastName, int staffUserCoreTypeId, String phone, int staffUserSubCoreTypeId);

	@Query(value = "{$and: [{'firstName': {$regex: ?0, $options:'i'}}, {'staffUserCoreTypeId': ?1}, {'phone':?2}, {staffUserSubCoreTypeId: {$ne:?3}}]}", fields = "{firstName:1, lastName:1, profilePhoto:1, staffUserCoreTypeName:1, email:1, phone:1, address1:1, address2: 1, city:1, state:1, organizationId:1}")
	List<UserBasicInformation> findByFirstNameAndStaffUserCoreTypeIdAndPhoneAndStaffUserCoreTypeIdNot (String firstName,int staffUserCoreTypeId, String phone, int staffUserSubCoreTypeId);

	@Query(value = "{$and: [{'firstName': {$regex: ?0, $options:'i'}}, {'staffUserCoreTypeId': ?1}, {'phone':?2}, {staffUserSubCoreTypeId:?3}]}", fields = "{firstName:1, lastName:1, profilePhoto:1, staffUserCoreTypeName:1, email:1, phone:1, address1:1, address2: 1, city:1, state:1, organizationId:1}")
	List<UserBasicInformation> findByFirstNameAndStaffUserCoreTypeIdAndPhoneAndStaffUserCoreTypeId(String firstName, int staffUserCoreTypeId, String phone, int staffUserSubCoreTypeId);

	@Query(value = "{$and: [{$or:[{'firstName': {$regex: ?0, $options:'i'}}, {'lastName': {$regex: ?1, $options:'i'}}]}, {'staffUserCoreTypeId': ?2}, {'email': {$regex: ?3, $options:'i'}}, {'staffUserSubCoreTypeId': {'$ne': ?4}}]}", fields = "{firstName:1, lastName:1, profilePhoto:1, staffUserCoreTypeName:1, email:1, phone:1, address1:1, address2: 1, city:1, state:1, organizationId:1}")
	List<UserBasicInformation> findByFirstNameAndLastNameAndStaffUserCoreTypeIdAndEmailAndStaffUserSubCoreTypeIdNot(String firstName, String lastName, int staffUserCoreTypeId, String email, int staffUserSubCoreTypeId);

	@Query(value = "{$and: [{$or:[{'firstName': {$regex: ?0, $options:'i'}}, {'lastName': {$regex: ?1, $options:'i'}}]}, {'staffUserCoreTypeId': ?2}, {'email': {$regex: ?3, $options:'i'}}, {'staffUserSubCoreTypeId': ?4}]}", fields = "{firstName:1, lastName:1, profilePhoto:1, staffUserCoreTypeName:1, email:1, phone:1, address1:1, address2: 1, city:1, state:1, organizationId:1}")
	List<UserBasicInformation> findByFirstNameAndLastNameAndStaffUserCoreTypeIdAndEmailAndStaffUserSubCoreTypeId(String firstName, String lastName, int i, String email, int j);

	@Query(value = "{$and: [{'firstName': {$regex: ?0, $options:'i'}}, {'staffUserCoreTypeId': ?1}, {'email': {$regex: ?2, $options:'i'}}, {'staffUserSubCoreTypeId': {'$ne': ?3}}]}", fields = "{firstName:1, lastName:1, profilePhoto:1, staffUserCoreTypeName:1, email:1, phone:1, address1:1, address2: 1, city:1, state:1, organizationId:1}")
	List<UserBasicInformation> findByFirstNameAndStaffUserCoreTypeIdAndEmailAndStaffUserSubCoreTypeIdNot(String firstName,int staffUserCoreTypeId, String email, int staffUserSubCoreTypeId);

	@Query(value = "{$and: [{'firstName': {$regex: ?0, $options:'i'}}, {'staffUserCoreTypeId': ?1}, {'email': {$regex: ?2, $options:'i'}}, {'staffUserSubCoreTypeId': ?3}]}", fields = "{firstName:1, lastName:1, profilePhoto:1, staffUserCoreTypeName:1, email:1, phone:1, address1:1, address2: 1, city:1, state:1, organizationId:1}")
	List<UserBasicInformation> findByFirstNameAndStaffUserCoreTypeIdAndEmailAndStaffUserSubCoreTypeId(String firstName, int staffUserCoreTypeId, String email, int staffUserSubCoreTypeId);

	@Query(value = "{$and:[{'email':{$regex: ?2, $options: 'i'}}, {'phone': ?0}, {staffUserCoreTypeId: ?1}]}", fields = "{firstName:1, lastName:1, profilePhoto:1, staffUserCoreTypeName:1, email:1, phone:1, address1:1, address2: 1, city:1, state:1, organizationId:1}")
	List<UserBasicInformation> findByPhoneAndStaffUserCoreTypeIdAndEmail(String phone, long staffUserCoreTypeId, String email);

	@Query(value = "{$and: [{'staffUserCoreTypeId': ?0 }, {'staffUserSubCoreTypeId': ?1}, {'gender': {$regex: ?2, $options:'i'}}]}")
	List<UserBasicInformation> findByStaffUserCoreTypeIdAndStaffUserSubCoreTypeIdAndGender(long staffUserCoreTypeId, long subRoleTypeId, String gender);

	@Query(value = "{$and: [{'staffUserCoreTypeId': ?0 }, {'staffUserSubCoreTypeId': ?1}, {'userLanguages.name': {$in:[?2]}}]}")
	List<UserBasicInformation> findByStaffUserCoreTypeIdAndStaffUserSubCoreTypeIdAndLanguage(long staffUserCoreTypeId, long subRoleTypeId, String language);

	List<UserBasicInformation> findByStaffUserCoreTypeIdAndStaffUserSubCoreTypeId(long staffUserCoreTypeId, long subRoleTypeId);

	UserBasicInformation findFirstByOrganizationIdAndStaffUserCoreTypeId(long organizationId, int staffUserCoreTypeId);

	@Query(value = "{$and:[{'gender':{$regex: ?1, $options: 'i'}}, {staffUserCoreTypeId: ?0}]}")
	List<UserBasicInformation> findByStaffUserCoreTypeIdAndGender(long staffUserCoreTypeId, String gender);

	@Query(value = "{$and: [{'staffUserCoreTypeId': ?0 }, {'userLanguages.name': {$in:[?1]}}]}")
	List<UserBasicInformation> findByStaffUserCoreTypeIdAndLanguage(long staffUserCoreTypeId, String language);

	@Query(value = "{$and:[{'ethnicType':{$regex: ?1, $options: 'i'}}, {staffUserCoreTypeId: ?0}]}")
	List<UserBasicInformation> findByStaffUserCoreTypeIdAndEthnicType(long staffUserCoreTypeId, String ethnicity);

	@Query(value = "{_id: {$in:?0}}", delete = true)
	void deleteAllByUserId(List<Long> usersIds);

	@Query(value = "{email: {$in: ?0}}", fields = "{_id:1}")
	List<UserBasicInformation> findAllUsersByEmailId(List<String> emails);

	@Query(value = "{userId: ?0}", fields = "{firstName:1, lastName:1}")
	UserBasicInformation findFirstAndLastNameByUserId(long userId);

	@Query(sort = "{_id:-1}")
	List<UserBasicInformation> findUsersByStaffUserCoreTypeIdAndOrganizationIdNot(long staffUserCoreTypeId, long organizationId);

	@Query(fields = "{firstName:1, lastName:1, email: 1, phone:1}")
	UserBasicInformation findFirstByOrganizationIdAndUserTypeId(long organizationId, int userTypeId);

	@Query(fields = "{firstName:1, lastName:1, email: 1, phone:1}")
	UserBasicInformation findFirstByUserTypeId(int userTypeId);
	
	@Query(value = "{$and:[{'phone': ?0}, {'staffUserCoreTypeId':{'$in': ?1}}, {'organizationId': ?2}]}", fields = "{firstName:1, lastName:1, profilePhoto:1, staffUserCoreTypeName:1, email:1, phone:1, address1:1, address2: 1, city:1, state:1, organizationId:1}")
	List<UserBasicInformation> findByPhoneAndStaffUserCoreTypeIdAndOrganizationId(String phone, List<Long> staffUserCoreTypeId, long organizationId);

	@Query(value = "{$and: [{'firstName': {$regex: ?0, $options:'i'}}, {'staffUserCoreTypeId': {'$in':?1}}, {'organizationId': ?2}]}", fields = "{firstName:1, lastName:1, profilePhoto:1, staffUserCoreTypeName:1, email:1, phone:1, address1:1, address2: 1, city:1, state:1, organizationId:1}")
	List<UserBasicInformation> findByFirstNameAndStaffUserCoreTypeIdAndOrganizationId(String firstName, List<Long> staffUserCoreTypeIds, long organizationId);
	
	@Query(value = "{$and: [{'firstName': {$regex: ?0, $options:'i'}}, {'lastName': {$regex: ?1, $options:'i'}}, {'staffUserCoreTypeId': {'$in':?2}}, {'organizationId': ?3}]}", fields = "{firstName:1, lastName:1, profilePhoto:1, staffUserCoreTypeName:1, email:1, phone:1, address1:1, address2: 1, city:1, state:1, organizationId:1}")
	List<UserBasicInformation> findByFirstNameAndLastNameAndStaffUserCoreTypeIdAndOrganizationId(String firstName, String lastName, List<Long> staffUserCoreTypeIds, long organizationId);
	
	@Query(value = "{$and: [{'email': {$regex: ?0, $options:'i'}}, {'staffUserCoreTypeId': {'$in':?1}}, {'organizationId': ?2}]}", fields = "{firstName:1, lastName:1, profilePhoto:1, staffUserCoreTypeName:1, email:1, phone:1, address1:1, address2: 1, city:1, state:1, organizationId:1}")
	List<UserBasicInformation> findUsersByEmailAndStaffUserCoreTypeIdAndOrganizationId(String email, List<Long> staffUserCoreTypeIds, long organizationId);
	
	

}