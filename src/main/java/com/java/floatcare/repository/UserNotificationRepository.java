package com.java.floatcare.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.java.floatcare.model.UserNotification;

public interface UserNotificationRepository extends MongoRepository<UserNotification, Long>{

	List<UserNotification> findByUserId(long userId);
	
	UserNotification findByNotificationTypeIdAndUserId(long notificationTypeId, long userId);

	void deleteAllByNotificationTypeId(long notificationTypeId);

	void deleteAllByUserId(long userId);

	@Query(value = "{userId: {$in:?0}}", delete = true)
	void deleteAllByUserId(List<Long> usersIds);

}
