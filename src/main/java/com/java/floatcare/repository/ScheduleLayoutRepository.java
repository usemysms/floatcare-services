package com.java.floatcare.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.java.floatcare.model.ScheduleLayout;

public interface ScheduleLayoutRepository extends MongoRepository<ScheduleLayout, Long> {

	List<ScheduleLayout> findByDepartmentId (long departmentId);
	
	ScheduleLayout findByScheduleLayoutId (long scheduleLayoutId);

	@Query(value = "{_id: {$in:?0}}", delete = true)
	void deleteAllByScheduleLayoutId(List<Long> scheduleLayoutIds);

}
