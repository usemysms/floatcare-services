package com.java.floatcare.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.java.floatcare.model.ShiftPlanningDrafts;

public interface ShiftPlanningDraftsRepository extends MongoRepository<ShiftPlanningDrafts, Long>{
	
	Optional<ShiftPlanningDrafts> findByScheduleLayoutId(long scheduleLayoutId);

	Optional<ShiftPlanningDrafts> findByScheduleLayoutIdAndStatusNot(long scheduleLayoutId, String status);

}
