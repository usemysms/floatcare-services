package com.java.floatcare.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.model.UserOrganizations;

public interface UserOrganizationsRepository extends MongoRepository<UserOrganizations, Long>{

	UserOrganizations findByUserIdAndOrganizationId(long userId, long organizationId);

	UserOrganizations findByUserIdAndOrganizationIdAndStatus(long userId, long organizationId, String status);

	List<UserOrganizations> findByOrganizationIdAndStatus(long organizationId, String status);

	@Query(value ="{$and:[{userId:{$in:?0}},{organizationId:{$ne:?1}}]}", fields = "{userId:1}")
	List<UserOrganizations> findByUserIdAndOrganizationId(List<Long> userIds, long organizationId);

	List<UserOrganizations> findByOrganizationId(long organizationId);

	@Query(value = "{userId: {$in:?0}}", delete = true)
	void deleteAllByUserId(List<Long> usersIds);
	
	@Query(value = "{organizationId: {$in: ?0}}", fields = "{userId:1}")
	List<UserOrganizations> findAllUserOrganizationsByOrganizationId(List<Long> organizationIds);

}