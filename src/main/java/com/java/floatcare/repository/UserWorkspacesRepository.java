package com.java.floatcare.repository;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.java.floatcare.model.UserWorkspaces;

public interface UserWorkspacesRepository extends MongoRepository<UserWorkspaces, Long> {

	List<UserWorkspaces> findUserWorkspacesByUserId(long userId);
	
	List<UserWorkspaces> findByInvitationCode(String invitationCode);

	LinkedList<UserWorkspaces> findByBusinessId(long worksiteId);

	List<UserWorkspaces> findByOrganizationId(long organizationId);

	List<UserWorkspaces> findByWorkspaceId(long workspaceId);

	List<UserWorkspaces> findUserWorkspacesByUserIdAndStatusAndIsJoined(long userId, String status, boolean isJoined);

	List<UserWorkspaces> findUserWorkspacesByUserIdAndStatus(long userId, String status);

	List<UserWorkspaces> findByStaffUserCoreTypeIdAndIsJoined(long staffUserCoreTypeId, boolean isJoined);

	long countByUserIdAndWorkspaceId(long userId, long workspaceId);
	
	long countByWorkspaceIdAndStaffUserCoreTypeId(long workspaceId, long staffUserCoreTypeId);

	UserWorkspaces findByUserIdAndWorkspaceIdAndStatusAndIsJoined(long eachUser, long departmentId, String status, boolean isJoined);

	UserWorkspaces findByUserIdAndWorkspaceIdAndBusinessIdAndStaffUserCoreTypeId(long eachUser, long departmentId, long businessId, long staffUserCoreTypeId);
	
	List<UserWorkspaces> findByWorkspaceIdAndStatusAndIsJoined(long departmentId, String status, boolean isJoined);

	List<UserWorkspaces> findUserWorkspacesByUserIdAndOrganizationId(long userId, long organizationId);

	UserWorkspaces findByUserIdAndWorkspaceId(long userId, long departmentId);

	long countByUserIdAndBusinessId(long userId, long worksiteId);

	List<UserWorkspaces> findByUserIdAndBusinessId(long userId, long worksiteId);

	List<UserWorkspaces> findByOrganizationIdAndStatus(long organizationId, String status);

	List<UserWorkspaces> findByOrganizationIdAndUserId(long organizationId, long userId);

	long countByWorkspaceIdAndStaffUserCoreTypeIdAndStatusNot(long workspaceId, long staffUserCoreTypeId, String string);

	UserWorkspaces findByUserIdAndWorkspaceIdAndIsJoined(Long userId, long workspaceId, boolean isJoined);

	@Query(value = "{userId: {$in:?0}}", delete = true)
	void deleteAllByUserId(List<Long> usersIds);

	@Query(value = "{userId: {$in:?0}}")
	List<UserWorkspaces> findAllByUserId(Set<Long> userIds);
	
	@Query(value = "{$and: [{'staffUserSubCoreTypeId': {$in:?0}}, {'businessId': ?1}]}")
	List<UserWorkspaces> findAllBySubCoreTypeId(List<Long> subCoreTypeId, Long businessId);
	
	@Query(value = "{$and: [{'staffUserSubCoreTypeId': {$in:?0}}, {'staffUserCoreTypeId': 1}]}")
	List<UserWorkspaces> findAllBySubCoreTypeId(List<Long> subCoreTypeId);

}
