package com.java.floatcare.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.java.floatcare.model.NotificationTypes;

public interface NotificationTypesRepository extends MongoRepository<NotificationTypes, Long>{

	NotificationTypes findNotificationTypesByNotificationTypeId(long notificationTypeId);

}
