package com.java.floatcare.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.java.floatcare.model.RequestTypes;

public interface RequestTypesRepository extends MongoRepository<RequestTypes, Long>{

}
