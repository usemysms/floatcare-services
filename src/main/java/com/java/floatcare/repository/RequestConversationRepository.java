package com.java.floatcare.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.java.floatcare.model.RequestConversation;

public interface RequestConversationRepository extends MongoRepository<RequestConversation, Long>{

	List<RequestConversation> findRequestConversationByRequestId(long requestId);

}
