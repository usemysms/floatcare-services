package com.java.floatcare.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.java.floatcare.model.WorkspaceSettings;

public interface WorkspaceSettingsRepository extends MongoRepository<WorkspaceSettings, Long>{

	Iterable<WorkspaceSettings> findWorkspaceSettingsByUserId(long userId);

	@Query(value = "{userId: {$in:?0}}", delete = true)
	void deleteAllByUserId(List<Long> usersIds);

}
