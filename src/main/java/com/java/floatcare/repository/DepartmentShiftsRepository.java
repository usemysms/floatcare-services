package com.java.floatcare.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.java.floatcare.model.DepartmentShifts;

public interface DepartmentShiftsRepository extends MongoRepository<DepartmentShifts, Long> {

	List<DepartmentShifts> findByDepartmentId(long departmentId);

	Optional<DepartmentShifts> findById(long departmentShiftid);

	List<DepartmentShifts> findByDepartmentIdAndStatus(long departmentId, String status);
	
		DepartmentShifts findFirstByIconAndDepartmentId(String icon, long departmentId);

	DepartmentShifts findFirstByIconAndDepartmentIdAndStatus(String icon, long departmentId, String status);

	DepartmentShifts findFirstByDepartmentIdAndLabelAndStatus(long departmentId, String label, String status);
	

}
