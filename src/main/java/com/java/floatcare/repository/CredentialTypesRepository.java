package com.java.floatcare.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.java.floatcare.model.CredentialTypes;

public interface CredentialTypesRepository extends MongoRepository<CredentialTypes, Long> {

	Optional<CredentialTypes> findByCredentialTypeId(long credentialTypeId);

}
