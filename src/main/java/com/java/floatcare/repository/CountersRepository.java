package com.java.floatcare.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.java.floatcare.model.Counters;

public interface CountersRepository extends MongoRepository<Counters, String>{

}
