package com.java.floatcare.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.java.floatcare.model.GeneralAvailability;

public interface GeneralAvailabilityRepository extends MongoRepository<GeneralAvailability, Long> {

}
