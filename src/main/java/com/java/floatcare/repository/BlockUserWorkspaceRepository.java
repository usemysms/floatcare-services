package com.java.floatcare.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.java.floatcare.model.BlockUserWorkspace;

@Repository
public interface BlockUserWorkspaceRepository extends MongoRepository<BlockUserWorkspace, Long>{
	
	Optional<BlockUserWorkspace> findByBlockedUserId(long blockedUserId);

	List<BlockUserWorkspace> findByUserId(long userId);
	
}
