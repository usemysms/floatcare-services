package com.java.floatcare.repository;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.java.floatcare.model.OpenShifts;

public interface OpenShiftsRepository extends MongoRepository<OpenShifts, Long>{

	List<OpenShifts> findByWorksiteId(long worksiteId);

	List<OpenShifts> findByOpenShiftId(long openShiftId);

	LinkedList<OpenShifts> findByOnDate(LocalDate parse);
	
	OpenShifts findOpenShiftByOpenShiftId(long openShiftId);

	List<OpenShifts> findByDepartmentId(long departmentId);

	OpenShifts findByDepartmentIdAndOnDateAndStaffUserCoreTypeId(long departmentId, LocalDate parse, long staffUserCoreTypeId);
	
}

