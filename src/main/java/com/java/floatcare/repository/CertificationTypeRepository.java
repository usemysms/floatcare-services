package com.java.floatcare.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.java.floatcare.model.CertificationType;

@Repository
public interface CertificationTypeRepository extends MongoRepository<CertificationType, String>{

}
