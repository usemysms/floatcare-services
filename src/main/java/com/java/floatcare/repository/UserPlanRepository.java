package com.java.floatcare.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.java.floatcare.model.UserPlan;

@Repository
public interface UserPlanRepository extends MongoRepository<UserPlan, Long>{

	Optional<UserPlan> findByPlanId(long planId);
	Optional<UserPlan> findByUserId(long userId);
}
