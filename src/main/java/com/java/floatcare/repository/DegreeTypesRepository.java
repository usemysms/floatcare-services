package com.java.floatcare.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.java.floatcare.model.DegreeType;

public interface DegreeTypesRepository extends MongoRepository<DegreeType, Long>{

	Optional<DegreeType> findDegreeTypeByDegreeTypeId(long degreeTypeId);

}
