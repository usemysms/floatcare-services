package com.java.floatcare.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.java.floatcare.model.UserEducation;

public interface UserEducationRepository extends MongoRepository<UserEducation, Long> {

	Optional<UserEducation> findUserEducationByUserId(long userId);

	List<UserEducation> findByUserId(long userId);

	@Query(value = "{userId: {$in:?0}}", delete = true)
	void deleteAllByUserId(List<Long> usersIds);

}
