package com.java.floatcare.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.java.floatcare.model.UserLoginDetails;

public interface UserLoginDetailsRepository extends MongoRepository<UserLoginDetails, String>{

}
