package com.java.floatcare.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.java.floatcare.model.Messages;

public interface MessagesRepository extends MongoRepository<Messages, Long> {

	List<Messages> findMessagesByMessageId(long messageId);

	List<Messages> findMessagesBySenderIdAndReceiverId(long senderId, long receiverId);

	List<Messages> findMessagesBySenderId(long senderId);
	
	List<Messages> findMessagesByReceiverId(long receiverId);

	List<Messages> findAllByMessageId(List<Long> messageList);

	@Query(value="{ senderId:?0, receiverId:?1, isSeen:?2, _id:{$nin: ?3}}", fields="{ messageId : 1 }")
	List<Messages> findMessagesBySenderIdAndReceiverIdAndIsSeenAndMessageId(int senderId, int receiverId, boolean isSeen, List<Long> messageIds);

	List<Messages> findByGroupId(long chatGroupId);

	@Query(fields = "{messageId: 1}")
	List<Messages> findAllByGroupId(long chatGroupId);

	@Query(fields = "{messageId: 1}")
	List<Messages> findByGroupIdAndSenderIdNot(long groupId, Long senderId);

	List<Messages> findMessagesBySenderIdAndReceiverIdAndIsSeen(int receiverId, int senderId, boolean b);

	@Query(value = "{groupId: ?0, messageId: {$nin: ?1}}")
	List<Messages> findByGroupIdAndMessageIdNot(long chatGroupId, List<Long> messageIds);

	void deleteAllByGroupId(long chatGroupId);

}