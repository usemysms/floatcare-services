package com.java.floatcare.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.java.floatcare.model.FlexOffs;

@Repository
public interface FlexOffsRepository extends MongoRepository<FlexOffs, Long>{

	List<FlexOffs> findByDepartmentId(long workspaceId);

}
