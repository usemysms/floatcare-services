package com.java.floatcare.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.java.floatcare.model.ReminderTypes;

public interface ReminderTypesRepository extends MongoRepository<ReminderTypes, Long> {

}
