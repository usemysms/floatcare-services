package com.java.floatcare.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.java.floatcare.model.FamilyContacts;

@Repository
public interface FamilyContactsRepository extends MongoRepository<FamilyContacts, Long> {

}
