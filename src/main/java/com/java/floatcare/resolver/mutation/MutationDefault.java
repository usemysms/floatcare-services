package com.java.floatcare.resolver.mutation;

import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.java.floatcare.api.request.HealthInsurances;
import com.java.floatcare.api.request.pojo.ContactInformation;
import com.java.floatcare.api.request.pojo.DepartmentsList;
import com.java.floatcare.api.request.pojo.PermissionSet;
import com.java.floatcare.api.request.pojo.UserAvailability;
import com.java.floatcare.api.request.pojo.WorksiteAvailability;
import com.java.floatcare.api.request.pojo.WorksitesList;
import com.java.floatcare.api.response.pojo.RequiredType;
import com.java.floatcare.api.response.pojo.SocialSecurityNumber;
import com.java.floatcare.api.response.pojo.UserLanguages;
import com.java.floatcare.api.response.pojo.UserNationalProviderIdentity;
import com.java.floatcare.dao.CountersDAO;
import com.java.floatcare.model.AssignmentInvitees;
import com.java.floatcare.model.AssignmentSchedules;
import com.java.floatcare.model.Assignments;
import com.java.floatcare.model.Availability;
import com.java.floatcare.model.BlockUserWorkspace;
import com.java.floatcare.model.Businesses;
import com.java.floatcare.model.Clients;
import com.java.floatcare.model.CredentialSubTypes;
import com.java.floatcare.model.Department;
import com.java.floatcare.model.DepartmentSettings;
import com.java.floatcare.model.DepartmentShifts;
import com.java.floatcare.model.EhrSystems;
import com.java.floatcare.model.FamilyContacts;
import com.java.floatcare.model.FlexOffs;
import com.java.floatcare.model.GeneralAvailability;
import com.java.floatcare.model.GroupChats;
import com.java.floatcare.model.Household;
import com.java.floatcare.model.Messages;
import com.java.floatcare.model.OpenShifts;
import com.java.floatcare.model.Organizations;
import com.java.floatcare.model.References;
import com.java.floatcare.model.RequestConversation;
import com.java.floatcare.model.Requests;
import com.java.floatcare.model.ScheduleLayout;
import com.java.floatcare.model.ScheduleLayoutQuota;
import com.java.floatcare.model.Schedules;
import com.java.floatcare.model.SetScheduleLayouts;
import com.java.floatcare.model.StaffWeekOffs;
import com.java.floatcare.model.SwapShiftRequests;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.model.UserCredentials;
import com.java.floatcare.model.UserDiploma;
import com.java.floatcare.model.UserEducation;
import com.java.floatcare.model.UserNotification;
import com.java.floatcare.model.UserOrganizations;
import com.java.floatcare.model.UserPermissions;
import com.java.floatcare.model.UserPrivacySettings;
import com.java.floatcare.model.UserWorkExperience;
import com.java.floatcare.model.UserWorkspaces;
import com.java.floatcare.model.WorksiteSettings;
import com.java.floatcare.model.WorkspaceSettings;
import com.java.floatcare.repository.AvailabilityRepository;
import com.java.floatcare.repository.GeneralAvailabilityRepository;
import com.java.floatcare.repository.NotificationsRepository;
import com.java.floatcare.repository.RequestConversationRepository;
import com.java.floatcare.repository.ScheduleLayoutQuotaRepository;
import com.java.floatcare.repository.UserCredentialsRepository;
import com.java.floatcare.service.AssignmentSchedulesService;
import com.java.floatcare.service.AssignmentsService;
import com.java.floatcare.service.BlockUserWorkspaceService;
import com.java.floatcare.service.BusinessesService;
import com.java.floatcare.service.ClientsService;
import com.java.floatcare.service.CredentialSubTypesService;
import com.java.floatcare.service.DepartmentService;
import com.java.floatcare.service.DepartmentSettingsService;
import com.java.floatcare.service.DepartmentShiftsService;
import com.java.floatcare.service.FlexOffsService;
import com.java.floatcare.service.GroupChatService;
import com.java.floatcare.service.MessageService;
import com.java.floatcare.service.OpenShiftService;
import com.java.floatcare.service.OrganizationService;
import com.java.floatcare.service.ReferencesService;
import com.java.floatcare.service.RequestsService;
import com.java.floatcare.service.ScheduleLayoutService;
import com.java.floatcare.service.SchedulesService;
import com.java.floatcare.service.SetScheduleLayoutsService;
import com.java.floatcare.service.SwapShiftRequestsService;
import com.java.floatcare.service.UserBasicInformationService;
import com.java.floatcare.service.UserCredentialsService;
import com.java.floatcare.service.UserDiplomaService;
import com.java.floatcare.service.UserEducationService;
import com.java.floatcare.service.UserNotificationService;
import com.java.floatcare.service.UserPermissionsService;
import com.java.floatcare.service.UserPrivacySettingsService;
import com.java.floatcare.service.UserWorkExperienceService;
import com.java.floatcare.service.UserWorkspacesService;
import com.java.floatcare.service.WorksiteSettingsService;
import com.java.floatcare.service.WorkspaceSettingsService;

@Component
public class MutationDefault implements GraphQLMutationResolver {

	@Autowired
	private OrganizationService organizationService;
	@Autowired
	private BusinessesService businessesService;
	@Autowired
	private UserBasicInformationService userService;
	@Autowired
	private UserWorkExperienceService userWorkExperienceService;
	@Autowired
	private UserCredentialsService userCredentialsService;
	@Autowired
	private UserEducationService userEducationService;
	@Autowired
	private DepartmentService departmentService;
	@Autowired
	private UserNotificationService userNotificationService;
	@Autowired
	private UserPrivacySettingsService userPrivacySettingsService;
	@Autowired
	private UserWorkspacesService userWorkSpacesService;
	@Autowired
	private WorkspaceSettingsService workspaceSettingsService;
	@Autowired
	private RequestsService requestsServices;
	@Autowired
	private CountersDAO sequence;
	@Autowired
	private OpenShiftService openShiftService;
	@Autowired
	private AvailabilityRepository availabilityRepository;
	@Autowired
	private GeneralAvailabilityRepository generalAvailabilityRepository;
	@Autowired
	private DepartmentSettingsService departmentSettingsService;
	@Autowired
	private DepartmentShiftsService departmentShiftsService;
	@Autowired
	private SchedulesService schedulesService;
	@Autowired
	private BlockUserWorkspaceService blockUserWorkspaceService;
	@Autowired
	private ScheduleLayoutService scheduleLayoutService;
	@Autowired
	private FlexOffsService flexOffsService;
	@Autowired
	private WorksiteSettingsService worksiteSettingsService;
	@Autowired
	private SwapShiftRequestsService swapShiftRequestsService;
	@Autowired
	private SetScheduleLayoutsService setScheduleLayoutsService;
	@Autowired
	private ClientsService clientsService;
	@Autowired
	private AssignmentsService assignmentsService;
	@Autowired
	private AssignmentSchedulesService assignmentSchedulesService;
	@Autowired
	private UserDiplomaService userDiplomaService;
	@Autowired
	private ReferencesService referencesService;
	@Autowired
	private ScheduleLayoutQuotaRepository scheduleLayoutQuotaRepository;
	@Autowired
	private RequestConversationRepository requestConversationRepository;
	@Autowired
	private NotificationsRepository notificationsRepository;  
	@Autowired
	private CredentialSubTypesService credentialSubTypesService;
	@Autowired
	private MessageService messageService;
	@Autowired
	private GroupChatService groupChatService;
	@Autowired
	private UserCredentialsRepository userCredentialsRepository;
    @Autowired
    private UserPermissionsService userPermissionsService;

    // **************************************MANAGE USER PERMISSIONS************************************************

    public UserPermissions createUserPermissions (long userId, long organizationId, String accessLevel, List<Long> organizations, List<WorksitesList> worksiteIds,
			List<DepartmentsList> departmentIds, List<PermissionSet> permissionSet) {

        return userPermissionsService.createUserPermissions(userId, organizationId, accessLevel, organizations, worksiteIds, departmentIds, permissionSet);
    }

	//***************************************MANAGE USER BASIC INFORMATION******************************************

	public UserBasicInformation createUserBasicInformation(String firstName, String lastName, String middleName, String email,
			String password, String defaultPassword, String preferredName, String address1, String address2, String city, String state, String country, String postalCode,
			String phone, String primaryContactCountryCode, String profilePhoto, String title, long userTypeId,
			long staffUserCoreTypeId, long staffUserSubCoreTypeId, String dateOfBirth, String linkedInProfile, long organizationId, String gender,
			String bioInformation, String createdOn, String secondaryPhoneNumber, String alternativeContactCountryCode,
			String secondaryEmail, String fcmToken, String deviceType, List <UserLanguages> userLanguages , 
			UserNationalProviderIdentity userNationalProviderIdentity, SocialSecurityNumber socialSecurityNumber, String publicKey,
			List<EhrSystems> ehrSystemId, String ethnicType, List<String> specialities,
			String invitedSource,long invitedFirmId,long invitedBy,String invitationStatus) throws Exception {

		return userService.createUserBasicInformation(firstName, lastName, middleName, email, password, defaultPassword, preferredName,
				address1, address2, city, state, country, postalCode, phone, primaryContactCountryCode, profilePhoto, title, userTypeId, staffUserCoreTypeId, staffUserSubCoreTypeId, 
				dateOfBirth, linkedInProfile, organizationId, gender, bioInformation, createdOn, secondaryPhoneNumber, alternativeContactCountryCode, secondaryEmail, fcmToken,
				deviceType, userLanguages, userNationalProviderIdentity, socialSecurityNumber, publicKey, ehrSystemId, ethnicType, specialities,
				invitedSource, invitedFirmId, invitedBy, invitationStatus);

	}

	public UserBasicInformation updateUserBasicInformation(long userId, String firstName, String lastName, String middleName, String status, String email,
			String password, String preferredName, String address1, String address2, String city, String state, String country, String postalCode,
			String phone, String primaryContactCountryCode, String profilePhoto, String title, long userTypeId,
			long staffUserCoreTypeId, long staffUserSubCoreTypeId, String dateOfBirth, String linkedInProfile, long organizationId, String gender,
			String bioInformation, String createdOn, String secondaryPhoneNumber, String alternativeContactCountryCode,
			String secondaryEmail, String fcmToken, String deviceType, List <UserLanguages> userLanguages, UserNationalProviderIdentity userNationalProviderIdentity, SocialSecurityNumber socialSecurityNumber,
			String cvFile, String publicKey, List<EhrSystems> ehrSystemId, String ethnicType, List<String> specialities,
			String invitedSource,long invitedFirmId,long invitedBy,String invitationStatus) throws Exception {

		return userService.updateUserBasicInformation(userId, firstName, lastName, middleName, status, email, password, preferredName, 
				address1, address2, city, state, country, postalCode, phone, primaryContactCountryCode, profilePhoto, title, userTypeId, 
				staffUserCoreTypeId, staffUserSubCoreTypeId, dateOfBirth, linkedInProfile, organizationId, gender, bioInformation, 
				createdOn, secondaryPhoneNumber, alternativeContactCountryCode, secondaryEmail, fcmToken, deviceType, userLanguages, userNationalProviderIdentity, 
				socialSecurityNumber, cvFile, publicKey, ehrSystemId, ethnicType, specialities,
				invitedSource, invitedFirmId, invitedBy, invitationStatus);
	}

	public boolean deleteUser(long userId, long organizationId) {

		return userService.deleteUser(userId, organizationId);
	}
	
	public UserBasicInformation updateUserStatus(long userId, long authorityId, String status) {
		
		return userService.updateUserStatus(userId, authorityId, status);
	}
	
public Boolean updatePreferredProviders(List<Long> userIds, long organizationId, long businessId, long departmentId) throws IOException {
		
		return userService.updatePreferredProviders(userIds, organizationId, businessId, departmentId);
	}

	//***************************************MANAGE ORGANIZATIONS**********************************

	public Organizations createOrganization(String name, String organizationType, String address, String address2, String country, String logo, String status,
			String state, String license, String email, String phoneNumber, String city, String postalCode)
			throws Exception {

		return organizationService.createOrganization(name, organizationType, address, address2, country, logo, status, state, license, email,
				phoneNumber, city, postalCode);
	}

	public Organizations updateOrganization(long organizationId, String organizationType, String name, String address, String address2, String country,
			String logo, String status, String state, String license, String email, String phoneNumber, String city,
			String postalCode, long ownerId) throws Exception {

		return organizationService.updateOrganization(organizationId, organizationType, name, address, address2, country, logo, status, state,
				license, email, phoneNumber, city, postalCode, ownerId);
	}

	public boolean deleteOrganization(long organizationId) {

		return organizationService.deleteOrganization(organizationId);
	}

	//***************************************MANAGE Businesses***************************************

	public Businesses createBusiness(long organizationId, String name, long businessTypeId, long businessSubTypeId,
			String phoneNumber, long extension, String email, String address, String address2, String city, String state,
            String postalCode, String country, String status, String coverPhoto, String description, List<WorksiteAvailability> worksiteAvailability, List<Long> preferredProviderId, 
            ContactInformation clientInfo, ContactInformation providerInfo, List<Long> preferredSpecialities) throws Exception {

		return businessesService.createBusiness(organizationId, name, businessTypeId, businessSubTypeId, phoneNumber,
                extension, email, address, address2, city, state, postalCode, country, status, coverPhoto, description, worksiteAvailability, preferredProviderId, clientInfo, providerInfo, preferredSpecialities);

	}

	public Businesses updateBusiness(long businessId, long organizationId, String name, long businessTypeId,
			long businessSubTypeId, String phoneNumber, long extension, String email, String address, String address2, String city,
            String state, String postalCode, String country, String status, String coverPhoto, String description, List<WorksiteAvailability> worksiteAvailability, List<Long> preferredProviderId,
            ContactInformation clientInfo, ContactInformation providerInfo, List<Long> preferredSpecialities)
			throws Exception {

		return businessesService.updateBusiness(businessId, organizationId, name, businessTypeId, businessSubTypeId,
				phoneNumber, extension, email, address, address2, city, state, postalCode, country, status, coverPhoto,
                description, worksiteAvailability, preferredProviderId, clientInfo, providerInfo, preferredSpecialities);
	}

	public boolean deleteBusiness(long businessId) {

		return businessesService.deleteBusiness(businessId);
	}

	//**************************************MANAGE USER WORK EXPERIENCE*******************************

	public UserWorkExperience createUserWorkExperience(long userId, String facilityName, String location, String facilityType,
			String startDate, String endDate, String positionTitle, String unit, String description, 
			boolean isCurrentlyWorking) throws ParseException {

		return userWorkExperienceService.createUserWorkExperience(userId, facilityName, location, facilityType, startDate,
				endDate, positionTitle, unit, description, isCurrentlyWorking);

	}

	public UserWorkExperience updateUserWorkExperience(long userWorkExperienceId, long userId, String facilityName, String location,
			String facilityType, String startDate, String endDate, String positionTitle, String unit,
			String description, boolean isCurrentlyWorking) throws Exception {

		return userWorkExperienceService.updateUserWorkExperience(userWorkExperienceId, userId, facilityName, location, facilityType,
				startDate, endDate, positionTitle, unit, description, isCurrentlyWorking);
	}

	public boolean deleteUserWorkExperience(long userWorkExperienceId) {

		return userWorkExperienceService.deleteUserWorkExperience(userWorkExperienceId);
	}
	
	//**********************************Manage USER CREDENTIALS******************************

	public UserCredentials createUserCredentials(long userId, String credentialIcon, String credentialName,
			long credentialTypeId, long credentialSubTypeId, String expirationDate, long credentialStatusId,
			String licenseNumber, String frontFilePhoto, String backFilePhoto, String state, boolean isCompactLicense,boolean isPending) throws ParseException {

		return userCredentialsService.createUserCredentials(userId, credentialIcon, credentialName, credentialTypeId,
				credentialSubTypeId, expirationDate, credentialStatusId,licenseNumber, 
				frontFilePhoto, backFilePhoto, state, isCompactLicense, isPending);

	}

	public UserCredentials updateUserCredentials(long userCredentialId, long userId, String credentialIcon, String credentialName,
			long credentialTypeId, long credentialSubTypeId, String expirationDate, long credentialStatusId,
			String licenseNumber, String frontFilePhoto, String backFilePhoto, String state, boolean isCompactLicense, boolean isVerified, long verifiedBy, boolean isPending) throws Exception {

		return userCredentialsService.updateUserCredentials(userCredentialId, userId, credentialIcon, credentialName, credentialTypeId,
				credentialSubTypeId, expirationDate, credentialStatusId,licenseNumber, frontFilePhoto, backFilePhoto, state, isCompactLicense,isVerified, verifiedBy, isPending);

	}

	public boolean deleteUserCredentials(long userCredentialId) {

		return userCredentialsService.deleteUserCredentials(userCredentialId);
	}

	// ***********************************Manage USER EDUCATION***************************************

	public UserEducation createUserEducation(long userId, String school, String degreeTypeId, String degreeTypeName, String degreeOfStudy, String graduationDate,
			String location, String state) {

		return userEducationService.createUserEducation(userId, school, degreeTypeId, degreeTypeName, degreeOfStudy, graduationDate, location, state);
	}

	public UserEducation updateUserEducation(long userEducationId, long userId, String school, String degreeTypeId, String degreeTypeName, String degreeOfStudy, String graduationDate,
			String location, String state) throws Exception {

		return userEducationService.updateUserEducation(userEducationId, userId, school, degreeTypeId, degreeTypeName, degreeOfStudy, graduationDate, location, state);
	}

	public boolean deleteUserEducation(long userEducationId) {

		return userEducationService.deleteUserEducation(userEducationId);
	}

	// **********************************MANAGE DEPARTMENT***************************************

	public Department createDepartment(long businessId, String departmentTypeName, String phoneNumber, long extension,
			String email, String address, String address2, String description, String status, List<Long> roles) {

		return departmentService.createDepartment(businessId, departmentTypeName, phoneNumber, extension, email,
				address, address2, description, status, roles);
	}

	public Department updateDepartment(long departmentId, long businessId, String departmentTypeName,
			String phoneNumber, long extension, String email, String address, String address2, String description, String status,
			List<Long> staffRoles) throws Exception {

		return departmentService.updateDepartment(departmentId, businessId, departmentTypeName, phoneNumber, extension,
				email, address, address2, description, status, staffRoles);
	}

	public boolean deleteDepartment(long departmentId) {
		
		return departmentService.deleteDepartment(departmentId);
	}
	
	//************************************Manage User Notification********************************
	
	public UserNotification createUserNotification(long userId, long notificationTypeId, boolean isMuteAllEnable, boolean isEnablePush, boolean isEnableSMS, boolean isEnableEmail) {
		
		return userNotificationService.createUserNotification(userId, notificationTypeId, isMuteAllEnable, isEnablePush, isEnableSMS, isEnableEmail);
	}
	
	//********************************Manage User Privacy Settings**********************************
	
	public UserPrivacySettings createUserPrivacySettings(long userId,
			boolean isVisibleToPublic, boolean isVisibleToWorkspace, boolean isVisibleToCollegues) {
		
		return userPrivacySettingsService.createUserPrivacySettings(userId,
				isVisibleToPublic, isVisibleToWorkspace, isVisibleToCollegues);
	}
	
	//**********************************Manage User Workspaces**************************************
	
	public UserWorkspaces createUserWorkspace(long userId, String workspaceType, long workspaceId, String fteStatus, boolean isChargeRole,
		    boolean isPreceptorRole, long organizationId, long businessId, String scheduledHours, List<Long> departmentShiftId, long staffUserCoreTypeId,
            boolean isPrimaryDepartment, long subCoreTypeId, List<UserAvailability> userAvailability) throws IOException {
		
		return userWorkSpacesService.createUserWorkspace(userId, workspaceType, workspaceId, fteStatus, isChargeRole, isPreceptorRole, 
                organizationId, businessId, scheduledHours, departmentShiftId, staffUserCoreTypeId, isPrimaryDepartment, subCoreTypeId, 1, userAvailability); //1 is added to send text linked with bulk import
	}
	
	public UserWorkspaces updateUserWorkspace(long userWorkspaceId, long userId, String workspaceType, long workspaceId, String fteStatus, String invitationCode,
			boolean isChargeRole, boolean isPreceptorRole, long organizationId, long businessId, 
            List<Long> departmentShiftId, long staffUserCoreTypeId, boolean isPrimaryDepartment, long subCoreTypeId, String status, List<UserAvailability> userAvailability, boolean isJoined) throws Exception {

		return userWorkSpacesService.updateUserWorkspace(userWorkspaceId, userId, workspaceType, workspaceId, fteStatus, invitationCode, isChargeRole, 
				isPreceptorRole, organizationId, businessId, departmentShiftId, staffUserCoreTypeId, isPrimaryDepartment, subCoreTypeId, status, userAvailability, isJoined);
	}

	public UserOrganizations updateUserOrganization(long userId, long organizationId, List<HealthInsurances> healthInsurances) {

		return userWorkSpacesService.updateUserOrganization(userId, organizationId, healthInsurances);
	}
	
	//**************************************Workspace Settings**************************************
	
	public WorkspaceSettings createWorkspaceSettings (long userId, long eventNotificationId, long onCallShiftReminderId, long meetingReminderId,
			long assignedWorkShiftsReminderId, long educationReminderId, long eventsReminderId) throws Exception {
		
		return workspaceSettingsService.createWorkspaceSettings(userId, eventNotificationId, onCallShiftReminderId, meetingReminderId, 
				assignedWorkShiftsReminderId, educationReminderId, eventsReminderId);
		
	}
	
	//**************************************Manage Requests*****************************************
	
	public Requests createARequest(long worksiteId, long departmentId, long requestTypeId, long userId, List<String> onDate, List<Long> shiftId, long collegueId, String notes){
		
		return requestsServices.createARequest(worksiteId, departmentId, requestTypeId, userId, onDate, shiftId, collegueId, notes);
		
	}
	
	public Requests updateARequest(long requestId, long worksiteId, long departmentId, long requestTypeId, long userId, List<String> onDate, List<Long> shiftId, long collegueId, String notes, String status) throws Exception{
		
		return requestsServices.updateARequest(requestId, worksiteId, departmentId, requestTypeId, userId, onDate, shiftId, collegueId, notes, status);
	
	}
		
	public boolean deleteARequest(long requestId) {
		
		return requestsServices.deleteARequest(requestId);
	}
	
	//****************************************Manage Open Shifts*******************************************
	
	public OpenShifts createOpenShift (long worksiteId, long departmentId, long shiftTypeId, String onDate,
			String startTime, String endTime, long staffUserCoreTypeId, long quota, boolean isUrgent, boolean isSendNotifications, 
			boolean isAutoApprove, boolean isAutoClose, boolean isAllowPartial, boolean isAllowOvertime, String note, List<Integer> invitees) throws ParseException {
		
		return openShiftService.createOpenShift(worksiteId, departmentId, shiftTypeId, onDate, startTime, endTime, staffUserCoreTypeId, quota, isUrgent, 
				isSendNotifications, isAutoApprove, isAutoClose, isAllowPartial, isAllowOvertime, note, invitees);
	}
	
	public OpenShifts updateOpenShifts (long userId, long openShiftId, boolean isAccepted, boolean isApproved, String status) throws Exception {
		
		 return openShiftService.updateOpenShifts(userId, openShiftId, isAccepted, isApproved, status);
		 
	}
	
	public boolean deleteOpenShift (long openShiftId) throws Exception {
		
		  return openShiftService.deleteOpenShift(openShiftId);
		 
	}

	//************************************** Manage Department Settings*************************************
	
	public DepartmentSettings createDepartmentSettings(long departmentId, String customName, String workWeekDay, String workPayPeriod, Long workPayPeriodHours) {
		
		return departmentSettingsService.createDepartmentSettings(departmentId, customName, workWeekDay, workPayPeriod, workPayPeriodHours);
 	}
	
	public DepartmentSettings updateDepartmentSettings(long departmentSettingId, long departmentId, String customName, String workWeekDay,
			String workPayPeriod, Long workPayPeriodHours) throws Exception{
		
		return departmentSettingsService.updateDepartmentSettings(departmentSettingId, departmentId, customName, workWeekDay, workPayPeriod, workPayPeriodHours);
	}

	//************************************** Manage Department Shifts****************************************
	
	public DepartmentShifts createDepartmentShift (long departmentId, String color, String icon, String startTime, String endTime, String label, List<Long> staffCoreTypeIds) {
		
		return departmentShiftsService.createDepartmentShift (departmentId, color, icon, startTime, endTime, label, staffCoreTypeIds);
	}
	
	public DepartmentShifts updateDepartmentShift (long departmentShiftId, long departmentId, String color, String icon, String startTime, String endTime, String label, List<Long> staffCoreTypeIds) throws Exception {
		
		return departmentShiftsService.updateDepartmentShift (departmentShiftId, departmentId, color, icon, startTime, endTime, label, staffCoreTypeIds);
	}
	
	public boolean deleteDepartmentShift (long departmentShiftId) {
		
		return departmentShiftsService.deleteDepartmentShift(departmentShiftId);
	}
	
	// ************************************* Manage Schedules***********************************************
	
	public Schedules createASchedule (long worksiteId, long departmentId, long userId, long shiftTypeId, String shiftDate, String startTime, 
			String endTime, boolean isOnCallRequest) {
		
		return schedulesService.createASchedule(worksiteId, departmentId, userId, shiftTypeId, shiftDate, startTime, endTime, isOnCallRequest);
	}
	
	public Schedules updateASchedule(long scheduleId, long worksiteId, long departmentId, long userId, long shiftTypeId,
			String shiftDate, String startTime, String endTime, String status, boolean isOnCallRequest, boolean voluntaryLowCensus) throws Exception {
		
		return schedulesService.updateASchedule(scheduleId, worksiteId, departmentId, userId, shiftTypeId, shiftDate, 
			startTime, endTime, status, isOnCallRequest, voluntaryLowCensus);
		
	}
	
	public boolean deleteASchedule (long scheduleId) {
		
		return schedulesService.deleteASchedule(scheduleId);
	}
	
	public boolean deleteShiftEvent (long userId, List<String> onDate, long departmentId, long requestId) {
		
		return schedulesService.deleteShiftEvent(userId, onDate, departmentId, requestId);
	}
	
	public boolean deleteShiftAndCreateOpenShift (long userId, List<String> onDate, long departmentId, long requestId) {
		
		return schedulesService.deleteShiftAndCreateOpenShift(userId, onDate, departmentId, requestId);
	}
	
	// ************************************* Create Schedule Layout*****************************************
	
	public ScheduleLayout createScheduleLayout (long departmentId, String shiftPlanningStartDate, String scheduleStartDate, String scheduleLength,	
			String shiftPlanningPhaseLength, String makeStaffAvailabilityDeadLineLength, 
			String maxQuota, List<Long> staffCoreTypes, List<Long> shiftTypes, boolean autoAddProfessionalsToLayout) throws Exception {
		
		return scheduleLayoutService.createScheduleLayout(departmentId, shiftPlanningStartDate, scheduleStartDate, scheduleLength, 
				shiftPlanningPhaseLength, makeStaffAvailabilityDeadLineLength, maxQuota, staffCoreTypes, shiftTypes, autoAddProfessionalsToLayout);
	}
	
	public ScheduleLayout updateScheduleLayout (long scheduleLayoutId, long departmentId, String shiftPlanningStartDate, String scheduleStartDate, String scheduleLength,	
			String shiftPlanningPhaseLength, String makeStaffAvailabilityDeadLineLength, 
			String maxQuota, List<Long> staffCoreTypes, List<Long> shiftTypes, boolean autoAddProfessionalsToLayout) throws Exception {
		
		return scheduleLayoutService.updateScheduleLayout(scheduleLayoutId, departmentId, shiftPlanningStartDate, scheduleStartDate, scheduleLength, 
				shiftPlanningPhaseLength, makeStaffAvailabilityDeadLineLength, maxQuota, staffCoreTypes, shiftTypes, autoAddProfessionalsToLayout);
		
	}
	
	public boolean deleteScheduleLayout ( long scheduleLayoutId) {
		
		return scheduleLayoutService.deleteScheduleLayout(scheduleLayoutId);
	}
	
	// ************************************** Manage Block UserWorkspace*******************************************
	
	public BlockUserWorkspace createBlockUserWorkspace (long workspaceId, long userId, long blockedBy) {
		
		return blockUserWorkspaceService.createBlockUserWorkspace(workspaceId, userId, blockedBy);
	}
	
	public boolean deleteBlockUserWorkspace (long userId) {
		
		return blockUserWorkspaceService.deleteBlockUserWorkspace(userId);
	}
	
	// ************************************* Update Schedule Layout Group******************************************
	
	public ScheduleLayoutQuota updateScheduleLayoutQuota (long scheduleLayoutQuotaId, long scheduleLayoutId, long staffCoreTypeId, long quota, long shiftTypeId, long filledCount) throws Exception {
		
		Optional<ScheduleLayoutQuota> scheduleLayoutQuotaOpt = scheduleLayoutQuotaRepository.findById(scheduleLayoutQuotaId);
		
		if (scheduleLayoutQuotaOpt.isPresent()) {
		
			ScheduleLayoutQuota scheduleLayoutQuota = scheduleLayoutQuotaOpt.get();
			
				if (quota > 0)
					scheduleLayoutQuota.setQuota(quota);
				if (shiftTypeId > 0)
					scheduleLayoutQuota.setShiftTypeId(shiftTypeId);
				if (staffCoreTypeId > 0)
					scheduleLayoutQuota.setStaffCoreTypeId(staffCoreTypeId);
				if (filledCount > 0)
					scheduleLayoutQuota.setFilledCount(filledCount);
				
				scheduleLayoutQuotaRepository.save(scheduleLayoutQuota);
				
			return scheduleLayoutQuota;
		}
		else
			throw new Exception("ScheduleLayoutQuota not found");
	}
	
	// ************************************* Manage Flex-Offs **********************************************
	
	public FlexOffs createFlexOff (long worksiteId, long departmentId, long shiftTypeId, String onDate, String startTime, String endTime,
		long staffUserCoreTypeId, long quota, boolean isSendNotifications, boolean isAutoApprove, boolean fullFlexOff, String note, List<Integer> invitees) throws ParseException {
		
		
		return flexOffsService.createFlexOff(worksiteId, departmentId, shiftTypeId, onDate, startTime, endTime, staffUserCoreTypeId, quota,
				isSendNotifications, isAutoApprove, fullFlexOff, note, invitees);
	}
	
	public FlexOffs updateFlexOff (long userId, long flexOffId, boolean isAccepted, boolean isApproved, String status) {
			
		return flexOffsService.updateFlexOff(userId, flexOffId, isAccepted, isApproved, status);
	}
	
	public boolean deleteFlexOff (long flexOffId) {
		
		return flexOffsService.deleteFlexOff(flexOffId);
	}
	
	// ************************************* Manage WorksiteSettings ***************************************
	
	public WorksiteSettings createWorksiteSetting (long userId, long worksiteId, long organizationId, String color) {
		return worksiteSettingsService.createWorksiteSettings(userId, worksiteId, organizationId, color);
	}
	
	public WorksiteSettings updateWorksiteSetting (long worksiteSettingId, long userId, long worksiteId, long organizationId, String color) throws Exception {
		return worksiteSettingsService.updateWorksiteSettings(worksiteSettingId, userId, worksiteId, organizationId, color);
	}
	
	// ************************************* Manage SwapShiftRequests **************************************
	
	public SwapShiftRequests createSwapShiftRequest (long senderId, long recieverId, long sourceShiftId, long requestedShiftId) {
		
		return swapShiftRequestsService.createSwapShiftRequest(senderId, recieverId, sourceShiftId, requestedShiftId);
	}
	
	public SwapShiftRequests updateSwapShiftRequest (long swapShiftRequestId, boolean isRecieverAccepted, boolean isSupervisorAccepted) throws Exception {
		
		return swapShiftRequestsService.updateSwapShiftRequest(swapShiftRequestId, isRecieverAccepted, isSupervisorAccepted);
	}
	
	public boolean deleteSwapShiftRequest (long swapShiftRequestId) {
		
		return swapShiftRequestsService.deleteSwapShiftRequest(swapShiftRequestId);
	}
	
	// ************************************* Manage SetScheduleLayout***************************************
	
	public SetScheduleLayouts createSetScheduleLayout(long worksiteId, long departmentId, String scheduleLength, String startDate, List<Long> roles,
			List<Long> shiftTypes, List<String >weekOffDays, List<StaffWeekOffs> staffWeekOffs) {
				
		return setScheduleLayoutsService.createSetScheduleLayout(worksiteId, departmentId, scheduleLength, startDate, roles, shiftTypes, weekOffDays, staffWeekOffs);
	}
	
	public SetScheduleLayouts updateSetScheduleLayout (long setScheduleLayoutId, long worksiteId, long departmentId, String scheduleLength, String startDate, List<Long> roles,
			List<Long> shiftTypes, List<String >weekOffDays, List<StaffWeekOffs> staffWeekOffs) throws Exception {
			
		return setScheduleLayoutsService.updateSetScheduleLayout(setScheduleLayoutId, worksiteId, departmentId, scheduleLength, startDate, roles, shiftTypes, weekOffDays, staffWeekOffs);		
	}
	
	public boolean deleteSetScheduleLayout (long setScheduleLayoutId) {
		
		return setScheduleLayoutsService.deleteSetScheduleLayout(setScheduleLayoutId);
	}
	
	// ************************************* Manage Clients ************************************************
	
	public Clients createClient(long organizationId, long businessId, String firstName, String lastName, String profilePhoto, String dateOfBirth, String phoneNumber, String medicareNumber, 
			String medicaidNumber, String martialStatus, String religion, String ethnicity, String language, String status, List<Household> householdList) {
		
		return clientsService.createClient(organizationId, businessId, firstName, lastName, profilePhoto,
				dateOfBirth, phoneNumber, medicareNumber, medicaidNumber, martialStatus, religion, ethnicity, language, status, householdList);
		
	}
	public Clients updateClient(long clientId, long organizationId, long businessId, String firstName, String lastName, String profilePhoto, String dateOfBirth, String phoneNumber, String medicareNumber, 
			String medicaidNumber, String martialStatus, String religion, String ethnicity, String language, String status, List<Household> householdList) {
		
		return clientsService.updateClient(clientId,organizationId, businessId, firstName, lastName, profilePhoto,
				dateOfBirth, phoneNumber, medicareNumber, medicaidNumber, martialStatus, religion, ethnicity, language, status, householdList);
		
	}
	public FamilyContacts updateFamilyContact (long familyContactId, String firstName, String lastName, String phoneNumber, String email, 
			String relation, boolean isEmergencyContact) throws Exception {
		
		return clientsService.updateFamilyContact(familyContactId, firstName, lastName, phoneNumber, email, relation, isEmergencyContact);
	}
	
	public boolean deleteClientProfile (long clientId) {
		
		return clientsService.deleteClientProfile(clientId);
	}
	
	public boolean deleteFamilyContact (long familyContactId) {
		
		return clientsService.deleteFamilyContact(familyContactId);
	}
	
	//************************************** Manage Assignments ********************************************
	
	public Assignments createAssignment (long businessId, long clientId, String startDate, String endDate) {
		
		return assignmentsService.createAssignment(businessId, clientId, startDate, endDate);
	}
	
	public Assignments updateAssignment (long assignmentId, long clientId, String startDate, String endDate) {
		
		return assignmentsService.updateAssignment(assignmentId, clientId, startDate, endDate);
	}

	public boolean deleteAssignment(long assignmentId) {
		
		return assignmentsService.deleteAssignment(assignmentId);
	}
	
	// ********************************** Manage Assignment Schedules **************************************
	
	public List<AssignmentSchedules> createAssignmentSchedules(long assignmentId, List<AssignmentSchedules> assignmentScheduleList) {
		
		return assignmentSchedulesService.createAssignmentSchedules(assignmentId, assignmentScheduleList);
	}
	public AssignmentInvitees updateAssignmentInvitee(long assignmentId, long userId, boolean isAccepted, boolean isApproved) {
		
		return assignmentSchedulesService.updateAssignmentInvitee(assignmentId, userId, isAccepted, isApproved);
	}
	
	public List<AssignmentInvitees> finalizeAssignment (long assignmentId) {
		
		return assignmentSchedulesService.finalizeAssignment(assignmentId);
	}
	
	public boolean deleteAssignmentScheduleByAssignmentIdAndStaffCoreTypeId(long assignmentId, long staffUserCoreTypeId) {
		
		return assignmentSchedulesService.deleteAssignmentScheduleByAssignmentIdAndStaffCoreTypeId(assignmentId, staffUserCoreTypeId);
	}
	
	// ************************************* Manage UserDiploma*********************************************
	
	public UserDiploma createUserDiploma(long userDiplomaId, long userId, LocalDate issuedDate,
			LocalDate expirationDate, String documentNumber, String frontFilePhoto, String fileName,
			String backFilePhoto, String state) {
		
		return userDiplomaService.createUserDiploma(userDiplomaId, userId, issuedDate, expirationDate, documentNumber, 
								frontFilePhoto, fileName, backFilePhoto, state);
	}
	
	public boolean deleteUserDiploma (long userDiplomaId) {
		
		return userDiplomaService.deleteUserDiploma(userDiplomaId);
	}
	
	//******************************************Manage References*******************************************
	
	public References createReference(long referenceId, long userId, String name, String jobTitle, String phoneNumber, String email) {

		return referencesService.createReference(referenceId, userId, name, jobTitle, phoneNumber, email);
	}

	public boolean deleteReferences(long referenceId) {
		
		return referencesService.deleteReferences(referenceId);
	}
	
	// ************************************* Create Availability********************************************
	
		public Availability createAvailability(long userId, long scheduleLayoutId, String startDate, String endDate, String startTime, String endTime, List<String> onDate) {
			
			Availability availability = new Availability();
			
			List<LocalDate> selectedDays = new ArrayList<>();
			
			availability.setAvailabilityId(sequence.getNextSequenceValue("availabilityId"));
			
			if (userId > 0)
				availability.setUserId(userId);
			
			if (scheduleLayoutId > 0)
				availability.setScheduleLayoutId(scheduleLayoutId);
			
			if (startDate != null)
				availability.setStartDate(LocalDate.parse(startDate));
			else
				availability.setStartDate(null);
			
			if (endDate != null)
				availability.setEndDate(LocalDate.parse(endDate));
			else
				availability.setEndDate(null);
			
			if (startTime != null)
				availability.setStartTime(LocalTime.parse(startTime));
			else
				availability.setStartTime(null);
			
			if (endTime != null)
				availability.setEndTime(LocalTime.parse(endTime));
			else
				availability.setEndTime(null);
			
			if (onDate != null) {
	            
				for (String list : onDate) {
					if(selectedDays.size() == 0 )
						selectedDays.add(LocalDate.parse(list));
					else if(!(selectedDays.contains(LocalDate.parse(list))))
						selectedDays.add(LocalDate.parse(list));
				}
				
	            availability.setOnDate(selectedDays);
			}
			
			/*if (status != null)
				availability.setStatus(status);
			*/
			availabilityRepository.save(availability);
			
			return availability;
		}
		
		// ************************************* Create General Availability********************************************
		
		public GeneralAvailability createGeneralAvailability(long userId, long scheduleLayoutId, String startDate, String endDate, String startTime, String endTime) {
			
			GeneralAvailability generalAvailability = new GeneralAvailability();
			
			generalAvailability.setAvailabilityId(sequence.getNextSequenceValue("availabilityId"));
			
			if (userId > 0)
				generalAvailability.setUserId(userId);

			if (scheduleLayoutId > 0)
				generalAvailability.setScheduleLayoutId(scheduleLayoutId);

			if (startDate != null && startDate.length() > 2)
				generalAvailability.setStartDate(LocalDate.parse(startDate));
			else
				generalAvailability.setStartDate(LocalDate.parse(null));
			
			if (endDate != null && endDate.length() > 2)
				generalAvailability.setEndDate(LocalDate.parse(endDate));
			else
				generalAvailability.setEndDate(LocalDate.parse(null));
			
			if (startTime != null && startTime.length() > 2)
				generalAvailability.setStartTime(LocalTime.parse(startTime));
			else
				generalAvailability.setStartTime(null);
			
			if (endTime != null && endTime.length() > 2)
				generalAvailability.setEndTime(LocalTime.parse(endTime));
			else
				generalAvailability.setEndTime(null);
			
			generalAvailabilityRepository.save(generalAvailability);
			
			return generalAvailability;
		}
	
	// *******************************************Manage Group Chats***********************************************

	public GroupChats createAGroup(long groupId, String groupName, long organizationId, List<Long> groupAdmins, List<Long> staffMembers, String createdOn) throws Exception {
		
		return groupChatService.createAGroup(groupId, groupName, organizationId, groupAdmins, staffMembers, createdOn);
	}

	public boolean removeGroupMember(long groupId, long adminStaffId, long staffId) {
		
		return groupChatService.removeGroupMember(groupId, adminStaffId, staffId);
	}

	public boolean leaveAGroup(long groupId, long staffId) {
		
		return groupChatService.leaveAGroup(groupId, staffId);
	}
	
	public boolean deleteGroupChats (long groupId, long userId) {
		
		return messageService.deleteGroupChats(groupId, userId);
	}
	
	public boolean deleteSingleMessageForAUserInGroup(long groupId, long userId, long messageId) {
		
		return messageService.deleteSingleMessageForAUserInGroup(groupId, userId, messageId);
	}

	public GroupChats addAdminsInAGroup(long chatGroupId, List<Long> adminIds, long adminId) {
		return groupChatService.addAdminsInGroup(chatGroupId, adminIds, adminId);
	}

	//***************************************Manage Messages**************************************************
	
	public Messages createMessages(String text, Long senderId, Long receiverId, String timeStamp, String timeZone,
			boolean isSeen, String date, long epochSeconds, String aesKey, String initializationVector) {
		
		return messageService.createMessages(text, senderId, receiverId, timeStamp, timeZone, isSeen, 
				date, epochSeconds, aesKey, initializationVector);
	}

	public Messages updateMessage(long messageId, long userId, boolean isSeen) {
		
		return messageService.updateMessage(messageId, userId, isSeen);
	}

	public boolean deleteMessagesOfUser(long senderId, long receiverId) {
		
		return messageService.deleteMessagesOfUser(senderId, receiverId);
	}

	public Messages createAGroupMessage(long chatGroupId, String text, long senderId, String timeStamp, String timeZone,
			String date, long epochSeconds, String aesKey, String initializationVector, String type,
			List<String> attachments, String sourceType) {
		
		return messageService.createAGroupMessage(chatGroupId, text, senderId, timeStamp, timeZone, 
				date, epochSeconds, aesKey, initializationVector, type, attachments, sourceType);
	}
	
	public Messages updateUnseenMessagesInGroupChat (long messageId, long staffId) {
		
		return groupChatService.updateUnSeenMessagesInGroup(messageId, staffId);
	}
	
	public boolean deleteAGroup (long chatGroupId, long staffId) {
		
		return groupChatService.deleteAGroup(chatGroupId, staffId);
	}

	public boolean deleteAMessageFromPrivateChat (long receiverId, long senderId, long messageId) {
		
		return messageService.deleteSingleMessageForAUserInPrivateMessage(receiverId, senderId, messageId);
	}

	// **************************************Create Message Request*******************************************
	
	public RequestConversation sendMessageToSupervisor (long senderId, long receiverId, String text, String timeStamp, String timeZone) {
		
		RequestConversation requestConversation = new RequestConversation();

		requestConversation.setRequestConversationId(sequence.getNextSequenceValue("requestConversationId"));

		requestConversation.setSenderId(senderId);
		
		requestConversation.setReceiverId(receiverId);
		
		requestConversation.setText(text);
		
		requestConversation.setTimeStamp(timeStamp);
		
		requestConversation.setTimeZone(timeZone);
		
		requestConversationRepository.save(requestConversation);
		
		return requestConversation;
	}
	
	// ****************************************Delete Notifications**********************************************
	
	public boolean deleteUserNotification (long notificationId) {
		
		if (notificationsRepository.findById(notificationId).isPresent()) {
			notificationsRepository.deleteById(notificationId);
			return true;
		}
		else
			return false;
	}

	//************************************** Manage Credential Types ****************************************
	
		public CredentialSubTypes createCredentialSubTypes (long credentialSubTypeId,long organizationId, long credentialTypeId, String label, List<RequiredType> requiredType, List<String> states, String credentialType, long creatorId, String status, String acronym) {
			
			return credentialSubTypesService.createCredentialSubTypes (credentialSubTypeId, organizationId, credentialTypeId, label, requiredType, states,credentialType,creatorId,status,acronym);
		}
				
		public boolean deleteCredentialSubTypes (long credentialSubTypeId) {
			
			boolean success ;
			
			success =  credentialSubTypesService.deleteCredentialSubTypes(credentialSubTypeId);
			List<UserCredentials> userCredentialByCredentialSubTypeId =  userCredentialsRepository.findByCredentialSubTypeId(credentialSubTypeId);
			
			for(UserCredentials userCredential : userCredentialByCredentialSubTypeId) {
				deleteUserCredentials(userCredential.getUserCredentialId());
			}
			
			return success;
		}
}