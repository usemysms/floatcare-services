package com.java.floatcare.resolver.query;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.java.floatcare.model.DeletedMessages;
import com.java.floatcare.model.GroupChats;
import com.java.floatcare.model.Messages;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.repository.DeletedMessagesRepository;
import com.java.floatcare.repository.MessagesRepository;
import com.java.floatcare.repository.UserBasicInformationRepository;

@Component
public class GroupChatsQuery implements GraphQLResolver<GroupChats>{

	@Autowired
	private UserBasicInformationRepository userRepository;
	@Autowired
	private MessagesRepository messageRepository;
	@Autowired
	private DeletedMessagesRepository deletedMessagesRepository;


	public Iterable<UserBasicInformation> getMembersDetails(GroupChats groupChat) {

		return userRepository.findAllById(groupChat.getMembers());
	}
	
	public Iterable<UserBasicInformation> getAdminsDetails(GroupChats groupChat) {

		return userRepository.findAllById(groupChat.getAdmins());
	}
	
	public int getMemberCount(GroupChats groupChat) {
		
		return groupChat.getMembers().size();
	}
	
	public int getAdminCount(GroupChats groupChat) {
		
		return groupChat.getAdmins().size();
	}
	
	public List<Messages> getMessagesInAGroupByUserId (GroupChats groupChats, long userId) {
	
		if (groupChats.getAdmins().contains(userId) || groupChats.getMembers().contains(userId)) {
			DeletedMessages deletedMessages = deletedMessagesRepository.findByUserIdAndGroupId(userId, groupChats.getChatGroupId());
			
			if(deletedMessages != null && deletedMessages.getMessageIds().size() > 0)
				return messageRepository.findByGroupIdAndMessageIdNot(groupChats.getChatGroupId(), deletedMessages.getMessageIds());
			else
				return messageRepository.findByGroupId(groupChats.getChatGroupId());
		}else
			return null;
	}
}