package com.java.floatcare.resolver.query;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.java.floatcare.dao.UserWorkspacesDAO;
import com.java.floatcare.model.Department;
import com.java.floatcare.model.DepartmentShifts;
import com.java.floatcare.model.DepartmentStaff;
import com.java.floatcare.model.StaffUserCoreTypes;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.model.UserWorkspaces;
import com.java.floatcare.repository.DepartmentShiftsRepository;
import com.java.floatcare.repository.DepartmentStaffRepository;
import com.java.floatcare.repository.StaffUserCoreTypesRepository;
import com.java.floatcare.repository.UserBasicInformationRepository;

@Component
public class DepartmentQuery implements GraphQLResolver<Department> {

	@Autowired
	private DepartmentStaffRepository departmentStaffRepository;
	@Autowired
	private StaffUserCoreTypesRepository staffUserCoreTypesRepository;
	@Autowired
	private DepartmentShiftsRepository departmentShiftsRepository;
	@Autowired
	private UserWorkspacesDAO userWorkspaceDAO;
	@Autowired
	private UserBasicInformationRepository userBasicInformationRepository;

	public List<DepartmentStaff> getStaffRoles(Department department) {

		return departmentStaffRepository.findDepartmentStaffByDepartmentId(department.getDepartmentId());
	}

	public String getStaffUserCoreTypeName(DepartmentStaff departmentStaff) {

		Optional<StaffUserCoreTypes> staffUserCoreTypes = staffUserCoreTypesRepository
				.findById(departmentStaff.getStaffUserCoreTypeId());

		if (staffUserCoreTypes.isPresent()) {
			return staffUserCoreTypes.get().getLabel();
		}

		return null;
	}
	
	public List<DepartmentShifts> getShiftType(Department department) {
		
		List<DepartmentShifts> departmentShifts = departmentShiftsRepository.findByDepartmentIdAndStatus(department.getDepartmentId(), "Active");

		if (departmentShifts.size() > 0)
			return departmentShifts;
		else
			return null;
	}
	
	public Iterable<UserBasicInformation> getColleagues(Department department) {
		
		Map<Long, Long> userIds = userWorkspaceDAO.findByDepartmentId(department.getDepartmentId());
		return userBasicInformationRepository.findAllById(userIds.keySet());
	}
	
	public String getFteStatusOfStaffUser (Department department, long userId) {
		
		UserWorkspaces userWorkspace = userWorkspaceDAO.findByDepartmentIdAndUserId(department.getDepartmentId(), userId);
		
		if (userWorkspace != null)
			return userWorkspace.getFteStatus();
		else
			return null;	
	}
	
	public String getStaffRoleByUserId (Department department, long userId) {
		
		UserWorkspaces userWorkspaces = userWorkspaceDAO.findByDepartmentIdAndUserId(department.getDepartmentId(), userId);
		
		if (userWorkspaces != null) {
			
			StaffUserCoreTypes staffUserCoreTypes = staffUserCoreTypesRepository.findStaffUserCoreTypesById(userWorkspaces.getStaffUserCoreTypeId());
			if (staffUserCoreTypes != null)
				return staffUserCoreTypes.getLabel();
			else
				return null;
		}
		else
			return null;
		
	}
}
