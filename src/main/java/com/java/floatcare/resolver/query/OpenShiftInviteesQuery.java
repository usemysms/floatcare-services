package com.java.floatcare.resolver.query;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.java.floatcare.dao.UserWorkspacesDAO;
import com.java.floatcare.model.OpenShiftInvitees;
import com.java.floatcare.model.OpenShifts;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.model.UserWorkspaces;
import com.java.floatcare.repository.OpenShiftsRepository;
import com.java.floatcare.repository.UserBasicInformationRepository;

@Component
public class OpenShiftInviteesQuery implements GraphQLResolver<OpenShiftInvitees> {

	@Autowired
	private UserBasicInformationRepository userBasicInformationRepository;
	@Autowired
	private UserWorkspacesDAO userWorkspacesDAO;
	@Autowired
	private OpenShiftsRepository openShiftsRepository;
	
	public String getFirstName (OpenShiftInvitees openShiftInvitees) {
		
		Optional<UserBasicInformation> user = userBasicInformationRepository.findByUserIdAndStatus(openShiftInvitees.getUserId(), "Active");	
		
		if (user.isPresent())
			return user.get().getFirstName();
		else
			return null;
	}
	
	public String getLastName (OpenShiftInvitees openShiftInvitees) {
		
		Optional<UserBasicInformation> user = userBasicInformationRepository.findByUserIdAndStatus(openShiftInvitees.getUserId(), "Active");	
		
		if (user.isPresent())
			return user.get().getLastName();
		else
			return null;
	}
	
	public String getProfilePhoto (OpenShiftInvitees openShiftInvitees) {
		
		Optional<UserBasicInformation> user = userBasicInformationRepository.findByUserIdAndStatus(openShiftInvitees.getUserId(), "Active");	
		
		if (user.isPresent())
			return user.get().getProfilePhoto();
		else
			return null;
	}
	
	public String getScheduleHours (OpenShiftInvitees openShiftInvitees) {
		
		/*
		 * UserWorkspaces userOptional =
		 * userWorkspacesRepository.findByUserIdAndStatus(openShiftInvitees.getUserId(),
		 * "Active");
		 * 
		 * if (userOptional != null) return userOptional.getHours(); else
		 */
			return null;
	}
	
	public String getFteStatus (OpenShiftInvitees openShiftInvitees) {
		
		Optional<OpenShifts> openShiftsOptional = openShiftsRepository.findById(openShiftInvitees.getOpenShiftId());
		long departmentId = 0;
		
		if (openShiftsOptional.isPresent()) {
			departmentId = openShiftsOptional.get().getDepartmentId();
			
			UserWorkspaces userWorkspaceOptional = userWorkspacesDAO.findByDepartmentIdAndUserId(departmentId, openShiftInvitees.getUserId());
			
			if (userWorkspaceOptional != null)
				return userWorkspaceOptional.getFteStatus();
			else
				return null;
		}
		else
			return null;
	}
	
	public String getHiredOn (OpenShiftInvitees openShiftInvitees) {
		
		Optional<OpenShifts> openShiftsOptional = openShiftsRepository.findById(openShiftInvitees.getOpenShiftId());
		long departmentId = 0;
		
		if (openShiftsOptional.isPresent()) {
			departmentId = openShiftsOptional.get().getDepartmentId();
			
			UserWorkspaces userWorkspaceOptional = userWorkspacesDAO.findByDepartmentIdAndUserId(departmentId, openShiftInvitees.getUserId());
			
			if (userWorkspaceOptional != null)
				return userWorkspaceOptional.getFteStatus();
			else
				return null;
		}
		else
			return null;
	}
}