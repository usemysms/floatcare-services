package com.java.floatcare.resolver.query;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.java.floatcare.model.DepartmentStaff;
import com.java.floatcare.model.StaffUserCoreTypes;
import com.java.floatcare.repository.StaffUserCoreTypesRepository;

@Component
public class DepartmentStaffQuery implements GraphQLResolver<DepartmentStaff>{
	
	@Autowired
    private StaffUserCoreTypesRepository staffUserCoreTypesRepository;
	
	
	public String getStaffUserCoreTypeName(DepartmentStaff departmentStaff){
		
		Optional<StaffUserCoreTypes> staffUserCoreTypes = staffUserCoreTypesRepository.findById(departmentStaff.getStaffUserCoreTypeId());

		if (staffUserCoreTypes.isPresent()) {
			return staffUserCoreTypes.get().getLabel();
		}

		return null;
	}
}
