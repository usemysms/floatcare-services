package com.java.floatcare.resolver.query;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.java.floatcare.dao.ScheduleDAO;
import com.java.floatcare.dao.UserWorkspacesDAO;
import com.java.floatcare.model.FlexOffInvitees;
import com.java.floatcare.model.FlexOffs;
import com.java.floatcare.model.Schedules;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.model.UserWorkspaces;
import com.java.floatcare.repository.FlexOffsRepository;
import com.java.floatcare.repository.SchedulesRepository;
import com.java.floatcare.repository.UserBasicInformationRepository;

@Component
public class FlexOffInviteesQuery implements GraphQLResolver<FlexOffInvitees> {

	@Autowired
	private UserBasicInformationRepository userBasicInformationRepository;
	@Autowired
	private UserWorkspacesDAO userWorkspacesDAO;
	@Autowired
	private FlexOffsRepository flexOffsRepository;
	@Autowired
	private SchedulesRepository schedulesRepository;
	@Autowired
	private ScheduleDAO scheduleDAO;
	
	public String getFirstName (FlexOffInvitees flexOffInvitees) {
		
		Optional<UserBasicInformation> user = userBasicInformationRepository.findByUserIdAndStatus(flexOffInvitees.getUserId(), "Active");	
		
		if (user.isPresent())
			return user.get().getFirstName();
		else
			return null;
	}
	
	public String getLastName (FlexOffInvitees flexOffInvitees) {
		
		Optional<UserBasicInformation> user = userBasicInformationRepository.findByUserIdAndStatus(flexOffInvitees.getUserId(), "Active");	
		
		if (user.isPresent())
			return user.get().getLastName();
		else
			return null;
	}
	
	public String getProfilePhoto (FlexOffInvitees flexOffInvitees) {
		
		Optional<UserBasicInformation> user = userBasicInformationRepository.findByUserIdAndStatus(flexOffInvitees.getUserId(), "Active");	
		
		if (user.isPresent())
			return user.get().getProfilePhoto();
		else
			return null;
	}
	
	public String getScheduleHours (FlexOffInvitees flexOffInvitees) {
		
		/*
		 * UserWorkspaces userOptional =
		 * userWorkspacesRepository.findByUserIdAndWorkspaceId(flexOffInvitees.getUserId
		 * ());
		 * 
		 * if (userOptional != null) return userOptional.getHours(); else
		 */
			return null;
	}
	
	public String getFteStatus (FlexOffInvitees flexOffInvitees) {
		
		Optional<FlexOffs> openShiftsOptional = flexOffsRepository.findById(flexOffInvitees.getFlexOffId());
		
		long departmentId = 0;
		
		if (openShiftsOptional.isPresent()) {
			departmentId = openShiftsOptional.get().getDepartmentId();
			
			UserWorkspaces userWorkspaceOptional = userWorkspacesDAO.findByDepartmentIdAndUserId(departmentId, flexOffInvitees.getUserId());
			
			if (userWorkspaceOptional != null)
				return userWorkspaceOptional.getFteStatus();
			else
				return null;
		}
		else
			return null;
	}
	
	public String getHiredOn (FlexOffInvitees flexOffInvitees) {
		
		Optional<FlexOffs> flexOffOptional = flexOffsRepository.findById(flexOffInvitees.getFlexOffId());
		
		long departmentId = 0;
		
		if (flexOffOptional.isPresent()) {
			
			departmentId = flexOffOptional.get().getDepartmentId();
			
			UserWorkspaces userWorkspaceOptional = userWorkspacesDAO.findByDepartmentIdAndUserId(departmentId, flexOffInvitees.getUserId());
			
			if (userWorkspaceOptional != null)
				return userWorkspaceOptional.getFteStatus();
			else
				return null;
		}
		else
			return null;
	}
	
	public String getShiftTime (FlexOffInvitees flexOffInvitees) {
		
		Optional<FlexOffs> flexOff = flexOffsRepository.findById(flexOffInvitees.getFlexOffId());
		
		if (flexOff.isPresent()) {
			
			Schedules schedule = schedulesRepository.findSchedulesByUserIdAndShiftDateAndDepartmentId(flexOffInvitees.getUserId(), 
					flexOff.get().getOnDate(), flexOff.get().getDepartmentId());
			
			if (schedule != null)
				return schedule.getStartTime()+"-"+schedule.getEndTime();
			else
				return null;
		} else
			return null;
	}
	
	public String getLastFlex (FlexOffInvitees flexOffInvitees) {
		
		Optional<FlexOffs> flexOff = flexOffsRepository.findById(flexOffInvitees.getFlexOffId());
		
		if (flexOff.isPresent()) {
			
			long departmentId = flexOff.get().getDepartmentId();
			long userId = flexOffInvitees.getUserId();
			
			List<Schedules> schedules = scheduleDAO.findUserScheduleByUserIdAndDepartmentIdAndEventType(userId, departmentId, "flexedOff", "Cancelled");
			
			if (schedules.size() > 0) {
				String date = schedules.get(schedules.size()-1).getShiftDate().getMonthValue()+"/"+schedules.get(schedules.size()-1).getShiftDate().getDayOfMonth();
				return date;
			}
			else
				return "-";
		}
		else
			return "-";
	}
}