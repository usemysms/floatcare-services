package com.java.floatcare.resolver.query;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.java.floatcare.model.Organizations;
import com.java.floatcare.model.StaffUserCoreTypes;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.model.UserOrganizations;
import com.java.floatcare.repository.OrganizationRepository;
import com.java.floatcare.repository.StaffUserCoreTypesRepository;
import com.java.floatcare.repository.UserBasicInformationRepository;

@Component
public class UserOrganizationsQuery implements GraphQLResolver<UserOrganizations> {

	@Autowired
	private UserBasicInformationRepository userBasicInformationRepository;
	@Autowired
	private OrganizationRepository organizationRepository;
	@Autowired
	private StaffUserCoreTypesRepository staffUserCoreTypesRepository;

	public UserBasicInformation getUserDetails(UserOrganizations userOrganization) {

		Optional<UserBasicInformation> optionalUser = userBasicInformationRepository.findById(userOrganization.getUserId());
		return optionalUser.isPresent() ? optionalUser.get() : null;
	}

	public Organizations getOrganizationDetails(UserOrganizations userOrganization) {

		Optional<Organizations> optionalOrganization = organizationRepository.findById(userOrganization.getOrganizationId());
		return optionalOrganization.isPresent() ? optionalOrganization.get() : null;
	}

	public String getStaffUserCoreTypeName(UserOrganizations userOrganization) {

		Optional<StaffUserCoreTypes> optionalStaffUserCoreType = staffUserCoreTypesRepository.findById(userOrganization.getStaffUserCoreTypeId());
		return optionalStaffUserCoreType.isPresent() ? optionalStaffUserCoreType.get().getLabel() : null;
	}
}
