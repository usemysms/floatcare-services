package com.java.floatcare.resolver.query;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.java.floatcare.api.response.pojo.AccessPayload;
import com.java.floatcare.api.response.pojo.AccessPayloadForBusiness;
import com.java.floatcare.api.response.pojo.AccessPayloadForDepartment;
import com.java.floatcare.model.Businesses;
import com.java.floatcare.model.Department;
import com.java.floatcare.model.Organizations;
import com.java.floatcare.model.UserPermissions;
import com.java.floatcare.repository.BusinessesRepository;
import com.java.floatcare.repository.DepartmentRepository;
import com.java.floatcare.repository.OrganizationRepository;

@Component
public class UserPermissionsQuery implements GraphQLResolver<UserPermissions>{

	@Autowired
	private DepartmentRepository departmentRepository;
	@Autowired
	private BusinessesRepository businessesRepository;
	@Autowired
	private OrganizationRepository organizationRepository;

	public List<AccessPayload> getOrganizationLabels(UserPermissions userPermission) {

		if (userPermission.getOrganizationIds() != null && userPermission.getOrganizationIds().size() > 0) {

			List<Organizations> organizationList = (List<Organizations>) organizationRepository.findAllById(userPermission.getOrganizationIds());

			List<AccessPayload> response = new ArrayList<>();
			
			for (Organizations each : organizationList) {

				AccessPayload accessPayload = new AccessPayload();

				accessPayload.setValue(each.getOrganizationId());
				accessPayload.setText(each.getName());
				response.add(accessPayload);
			}
			return response;
		}else
			return null;
	}

	public List<AccessPayloadForBusiness> getWorksiteLabels(UserPermissions userPermission) {

		if (userPermission.getWorksiteIds() != null && userPermission.getWorksiteIds().size() > 0) {

			List<Businesses> businessesList = (List<Businesses>) businessesRepository.findAllById(userPermission.getWorksiteIds().stream().map(e->e.getWorksiteId()).collect(Collectors.toList()));

			List<AccessPayloadForBusiness> response = new ArrayList<>();
			
			for (Businesses each : businessesList) {

				AccessPayloadForBusiness accessPayload = new AccessPayloadForBusiness();

				accessPayload.setBusinessId(each.getBusinessId());
				accessPayload.setName(each.getName());
				response.add(accessPayload);
			}
			return response;
		}else
			return null;
	}

	public List<AccessPayloadForDepartment> getDepartmentLabels(UserPermissions userPermission) {

		if (userPermission.getWorksiteIds() != null && userPermission.getWorksiteIds().size() > 0) {

			List<Department> departmentList = (List<Department>) departmentRepository.findAllById(userPermission.getDepartmentIds()
												.stream().map(e->e.getDepartmentId()).collect(Collectors.toList()));

			List<AccessPayloadForDepartment> response = new ArrayList<>();
			
			for (Department each : departmentList) {

				AccessPayloadForDepartment accessPayload = new AccessPayloadForDepartment();

				accessPayload.setDepartmentId(each.getDepartmentId());
				accessPayload.setDepartmentTypeName(each.getDepartmentTypeName());
				accessPayload.setWorksiteId(each.getBusinessId());			response.add(accessPayload);
			}
			return response;
		}else
			return null;
	}
}