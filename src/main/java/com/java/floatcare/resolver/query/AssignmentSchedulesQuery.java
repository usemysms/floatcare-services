package com.java.floatcare.resolver.query;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.java.floatcare.model.AssignmentInvitees;
import com.java.floatcare.model.AssignmentSchedules;
import com.java.floatcare.model.StaffUserCoreTypes;
import com.java.floatcare.model.SubRoleTypes;
import com.java.floatcare.model.UserBasicInformation;

@Component
public class AssignmentSchedulesQuery implements GraphQLResolver<AssignmentSchedules> {

	@Autowired
	private MongoTemplate mongoTemplate;
	
	public String getStaffUserCoreTypeName(AssignmentSchedules assignmentSchedules) {

		return mongoTemplate.findOne(Query.query(Criteria.where("_id").is(assignmentSchedules.getStaffCoreTypeId())), 
				StaffUserCoreTypes.class).getLabel();
	}

	public List<UserBasicInformation> getStaffMembers (AssignmentSchedules assignmetSchedules) {
		
		return mongoTemplate.find(Query.query(Criteria.where("_id").in(assignmetSchedules.getStaff())), UserBasicInformation.class);
	}

	public AssignmentInvitees getAssignmentInviteeByUserId(AssignmentSchedules assignmentSchedules, long userId) {
		
		return mongoTemplate.findOne(Query.query(Criteria.where("staffId").is(userId)).addCriteria(Criteria.where("assignmentId").is(assignmentSchedules.getAssignmentId())), 
					AssignmentInvitees.class);
	}

	public List<String> getWorkingDaysByUserId(AssignmentSchedules assignmentSchedules, long userId) {
		
		List<AssignmentSchedules> assignmentScheduleList = mongoTemplate.find(Query.query(Criteria.where("staff").in(userId))
					.addCriteria(Criteria.where("assignmentId").is(assignmentSchedules.getAssignmentId())), AssignmentSchedules.class);

		List<String> weekDays = assignmentScheduleList.stream().map(AssignmentSchedules::getWeekDay).collect(Collectors.toList());

		return weekDays;
	}
	
	public String getSubCoreTypeName (AssignmentSchedules assignmentSchedule) {
		
		SubRoleTypes subCoreType= mongoTemplate.findOne(Query.query(Criteria.where("id").is(assignmentSchedule.getSubCoreTypeId())), SubRoleTypes.class);
		
		if (subCoreType != null)
			return subCoreType.getLabel();
		else
			return "";
	}
	
	public List<AssignmentInvitees> getAssignmentInvitees(AssignmentSchedules assignmentSchedule) {
		return mongoTemplate.find(new Query().addCriteria(Criteria.where("assignmentId").is(assignmentSchedule.getAssignmentId()))
				.addCriteria(Criteria.where("staffCoreTypeId").is(assignmentSchedule.getStaffCoreTypeId())), AssignmentInvitees.class);
	}
	
	public List<AssignmentSchedules> getListOfStartTimeAndEndTimeByRole(AssignmentSchedules assignmentSchedule) {
		
		long staffCoreTypeId =  assignmentSchedule.getStaffCoreTypeId();
		long assignmentId = assignmentSchedule.getAssignmentId();
		
		List<AssignmentSchedules> assignmentSchedulesList = mongoTemplate.find(new Query().addCriteria(Criteria.where("assignmentId").is(assignmentId))
				.addCriteria(Criteria.where("staffCoreTypeId").is(staffCoreTypeId)), AssignmentSchedules.class);
		
		return assignmentSchedulesList;
	}
}