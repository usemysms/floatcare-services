package com.java.floatcare.resolver.query;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.java.floatcare.model.Schedules;
import com.java.floatcare.model.SwapShiftRequests;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.repository.SchedulesRepository;
import com.java.floatcare.repository.UserBasicInformationRepository;

@Component
public class SwapShiftRequestsQuery implements GraphQLResolver<SwapShiftRequests>{
	
	@Autowired
	private UserBasicInformationRepository userBasicInformationRepository;
	@Autowired
	private SchedulesRepository schedulesRepository;
	
	public UserBasicInformation getSenderInformation (SwapShiftRequests swapShiftsRequests) {
		
		UserBasicInformation senderUserInformation = userBasicInformationRepository.findUsersByUserId(swapShiftsRequests.getSenderId());
		
		if (senderUserInformation != null) {
			if (senderUserInformation.getStatus().equalsIgnoreCase("active")) {
				return senderUserInformation;
			}else
				return null;
		}
		else
			return null;
	}
	
	public UserBasicInformation getReceiverInformation (SwapShiftRequests swapShiftsRequests) {
		
		UserBasicInformation receiverUserInformation = userBasicInformationRepository.findUsersByUserId(swapShiftsRequests.getReceiverId());
		
		if (receiverUserInformation != null) {
			if (receiverUserInformation.getStatus().equalsIgnoreCase("active")) {
				return receiverUserInformation;
			}else
				return null;
		}
		else
			return null;
	}
	
	public Schedules getSourceShiftInformation (SwapShiftRequests swapShiftRequests) {
		
		Optional<Schedules> sourceShiftInformation = schedulesRepository.findById(swapShiftRequests.getSourceShiftId());
		
		if (sourceShiftInformation.isPresent()) {
			return sourceShiftInformation.get();
		}
		else
			return null;
	}
	
	public Schedules getRequestedShiftInformation (SwapShiftRequests swapShiftRequests) {
		
		Optional<Schedules> requestedShiftInformation = schedulesRepository.findById(swapShiftRequests.getRequestedShiftId());
		
		if (requestedShiftInformation.isPresent()) {
			return requestedShiftInformation.get();
		}
		else
			return null;
	}
	
}