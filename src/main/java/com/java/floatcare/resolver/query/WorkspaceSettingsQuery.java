package com.java.floatcare.resolver.query;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.java.floatcare.model.ReminderTypes;
import com.java.floatcare.model.WorkspaceSettings;
import com.java.floatcare.repository.ReminderTypesRepository;

@Component
public class WorkspaceSettingsQuery implements GraphQLResolver<WorkspaceSettings>{

	@Autowired
	private ReminderTypesRepository reminderTypesRepository;
	
	public String getEventNotification(WorkspaceSettings workspaceSettings) {
		
		Optional<ReminderTypes> eventNotificationReminderOptional = reminderTypesRepository.findById(workspaceSettings.getEventNotificationId());
		
		if (eventNotificationReminderOptional.isPresent())
			return eventNotificationReminderOptional.get().getLabel();
		else
			return null;
	}	
	
	public String getAssignedWorkShiftsReminder(WorkspaceSettings workspaceSettings) {
		
		Optional<ReminderTypes> assignedWorkShiftsReminderOptional = reminderTypesRepository.findById(workspaceSettings.getAssignedWorkShiftsReminderId());
		
		if (assignedWorkShiftsReminderOptional.isPresent())
			return assignedWorkShiftsReminderOptional.get().getLabel();
		else
			return null;
	}
	
	public String getOnCallShiftReminder(WorkspaceSettings workspaceSettings) {
		
		Optional<ReminderTypes> onCallShiftReminderOptional = reminderTypesRepository.findById(workspaceSettings.getOnCallShiftReminderId());
		
		if (onCallShiftReminderOptional.isPresent())
			return onCallShiftReminderOptional.get().getLabel();
		else
			return null;
	}
	
	public String getMeetingReminder(WorkspaceSettings workspaceSettings) {
		
		Optional<ReminderTypes> meetingReminderOptional = reminderTypesRepository.findById(workspaceSettings.getMeetingReminderId());
		
		if (meetingReminderOptional.isPresent())
			return meetingReminderOptional.get().getLabel();
		else
			return null;
	}
	
	public String getEducationReminder(WorkspaceSettings workspaceSettings) {
		
		Optional<ReminderTypes> educationReminderOptional = reminderTypesRepository.findById(workspaceSettings.getEducationReminderId());
		
		if (educationReminderOptional.isPresent())
			return educationReminderOptional.get().getLabel();
		else
			return null;
	}
	
	public String getEventsReminder(WorkspaceSettings workspaceSettings) {
		
		Optional<ReminderTypes> eventReminderOptional = reminderTypesRepository.findById(workspaceSettings.getEventsReminderId());
		
		if (eventReminderOptional.isPresent())
			return eventReminderOptional.get().getLabel();
		else
			return null;
	}
	
}
