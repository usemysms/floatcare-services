package com.java.floatcare.resolver.query;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.java.floatcare.model.Availability;
import com.java.floatcare.model.Businesses;
import com.java.floatcare.model.Department;
import com.java.floatcare.model.DepartmentShifts;
import com.java.floatcare.model.ScheduleLayout;
import com.java.floatcare.model.ScheduleLayoutGroups;
import com.java.floatcare.model.ScheduleLayoutQuota;
import com.java.floatcare.model.UserWorkspaces;
import com.java.floatcare.repository.AvailabilityRepository;
import com.java.floatcare.repository.BusinessesRepository;
import com.java.floatcare.repository.DepartmentRepository;
import com.java.floatcare.repository.DepartmentShiftsRepository;
import com.java.floatcare.repository.ScheduleLayoutGroupsRepository;
import com.java.floatcare.repository.ScheduleLayoutQuotaRepository;
import com.java.floatcare.repository.UserWorkspacesRepository;

@Component
public class ScheduleLayoutQuery implements GraphQLResolver<ScheduleLayout> {

	@Autowired
	private ScheduleLayoutQuotaRepository scheduleLayoutQuotaRepository;
	@Autowired
	private DepartmentRepository departmentRepository;
	@Autowired
	private DepartmentShiftsRepository departmentShiftsRepository;
	@Autowired
	private BusinessesRepository businessesRepository;
	@Autowired
	private UserWorkspacesRepository userWorkspacesRepository;
	@Autowired
	private ScheduleLayoutGroupsRepository scheduleLayoutGroupsRepository;
	@Autowired
	private AvailabilityRepository availabilityRepository;
	
	public List<ScheduleLayoutQuota> getScheduleQuota(ScheduleLayout scheduleLayout) {

		return scheduleLayoutQuotaRepository.findByScheduleLayoutId(scheduleLayout.getScheduleLayoutId());
	}

	public String getDepartmentName(ScheduleLayout scheduleLayout) {

		Optional<Department> optDepartment = departmentRepository.findById(scheduleLayout.getDepartmentId());

		if (optDepartment.isPresent())
			return optDepartment.get().getDepartmentTypeName();
		else
			return null;

	}
	
	public String getBusinessName(ScheduleLayout scheduleLayout) {

		Optional<Department> optDepartment = departmentRepository.findById(scheduleLayout.getDepartmentId());

		if (optDepartment.isPresent()) {

			long businessId = optDepartment.get().getBusinessId();
			
			Optional<Businesses> optBusiness = businessesRepository.findById(businessId);
			
			if (optBusiness.isPresent())
				return optBusiness.get().getName();
			else
				return null;
		}
			
		else
			return null;

	}

	public List<String> getShiftNames(ScheduleLayout scheduleLayout) {

		TreeSet<String> shiftName = new TreeSet<String>();
		
		if (scheduleLayout.getShiftTypes().size() > 0) {
			for (Long eachShift : scheduleLayout.getShiftTypes()) {
				if (departmentShiftsRepository.findById(eachShift).isPresent())
						shiftName.add(departmentShiftsRepository.findById(eachShift).get().getLabel());
				else
					continue;
			}
			return new ArrayList<>(shiftName);
		} else
			return null;
	}
	
	public String getUserShiftName (ScheduleLayout scheduleLayout) {
		
		List<UserWorkspaces> userWorkspace = userWorkspacesRepository.findByWorkspaceIdAndStatusAndIsJoined(scheduleLayout.getDepartmentId(), "Active", true);
		
		if (userWorkspace.size() > 0) {
			List<Long> shiftTypeId = userWorkspace.get(0).getDepartmentShiftIds();
			if (scheduleLayout.getShiftTypes().containsAll(shiftTypeId)) {
				List<DepartmentShifts> departmentShifts = (List<DepartmentShifts>) departmentShiftsRepository.findAllById(shiftTypeId);
				return departmentShifts.stream().map(DepartmentShifts::getLabel).collect(Collectors.joining(", "));
			}else
				return "";
		}else
			return "";
	}
	
	public int getScheduleLayoutGroupCount(ScheduleLayout scheduleLayout) {
		
		List<ScheduleLayoutGroups> scheduleLayoutGroups = scheduleLayoutGroupsRepository.findByScheduleLayoutId(scheduleLayout.getScheduleLayoutId());
		List<String> groupName = new ArrayList<>();
		
		for (ScheduleLayoutGroups eachGroup : scheduleLayoutGroups) {
			if (groupName.size() > 0 && !groupName.contains(eachGroup.getName()))	
				groupName.add(eachGroup.getName());
			else if (groupName.size() == 0)
				groupName.add(eachGroup.getName());
		}
		return groupName.size();
	}

	public String getStatusByUserId(ScheduleLayout scheduleLayout, long userId) {

		Availability availability = availabilityRepository.findFirstByUserIdAndScheduleLayoutId(userId, scheduleLayout.getScheduleLayoutId());

		if (availability != null && scheduleLayout.getStatus() != null && scheduleLayout.getStatus().equalsIgnoreCase("Self Schedule"))
			return "Accepted";
		else if (availability != null && scheduleLayout.getStatus() != null && scheduleLayout.getStatus().equalsIgnoreCase("Published"))
			return "Accepted";
		else if (scheduleLayout.getStatus() == null)
			return "Published";
		else
			return "Pending";
	}
}
