package com.java.floatcare.resolver.query;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.java.floatcare.dao.AssignmentInviteesDAO;
import com.java.floatcare.dao.AssignmentSchedulesDAO;
import com.java.floatcare.model.AssignmentInvitees;
import com.java.floatcare.model.AssignmentSchedules;
import com.java.floatcare.model.AssignmentSchedulesByDate;
import com.java.floatcare.model.Assignments;
import com.java.floatcare.model.Businesses;
import com.java.floatcare.model.Clients;
import com.java.floatcare.model.StaffUserCoreTypes;
import com.java.floatcare.model.SubRoleTypes;
import com.java.floatcare.model.UserBasicInformation;

@Component
public class AssignmentsQuery implements GraphQLResolver<Assignments>{

	@Autowired
	private MongoTemplate mongoTemplate;
	@Autowired
	private AssignmentSchedulesDAO assignmentScheduleDAO;
	@Autowired
	private AssignmentInviteesDAO assignmentInviteesDAO;
	
	public Clients getClientInformation (Assignments assignments) {
		
		Clients client = mongoTemplate.findOne(Query.query(Criteria.where("clientId").is(assignments.getClientId())), Clients.class);
		return  client != null ? client : null;
	}
	
	public Businesses getBusinessInformation (Assignments assignments) {
		
		return mongoTemplate.findOne(Query.query(Criteria.where("businessId").is(assignments.getBusinessId())), Businesses.class);
	}
	
	public List<AssignmentSchedules> getAssignmentSchedules (Assignments assignments) {
		
		return mongoTemplate.find(Query.query(Criteria.where("assignmentId").is(assignments.getAssignmentId())), AssignmentSchedules.class);
	}
	
	public List<AssignmentInvitees> getStaffDetails(Assignments assignments) {
		
		if (assignments.getStatus().equals("draft")) {
			return mongoTemplate.find(Query.query(Criteria.where("assignmentId").is(assignments.getAssignmentId())), AssignmentInvitees.class);
		}else {
			return mongoTemplate.find(Query.query(Criteria.where("assignmentId").is(assignments.getAssignmentId()))
					.addCriteria(Criteria.where("status").ne("draft")), AssignmentInvitees.class);
		}
	}
	
	public List<AssignmentSchedules> getAssignmentSchedulesByUserId(Assignments assignments, long userId) {
		
		return mongoTemplate.find(Query.query(Criteria.where("staff").in(userId))
				.addCriteria(Criteria.where("assignmentId").is(assignments.getAssignmentId())), AssignmentSchedules.class);
	}

	public List<String> getWorkingDaysByUserId(Assignments assignments, long userId){
		
		List<AssignmentSchedules> assignmentSchedules = mongoTemplate.find(Query.query(Criteria.where("staff").in(userId))
				.addCriteria(Criteria.where("assignmentId").is(assignments.getAssignmentId())), AssignmentSchedules.class);
		
		if (assignmentSchedules != null && assignmentSchedules.size() > 0)
			return assignmentSchedules.stream().map(AssignmentSchedules::getWeekDay).collect(Collectors.toList());
		else
			return null;
	}
	
	public String getTotalShiftsByUserId (Assignments assignments, long userId) {
		
		List<AssignmentSchedules> assignmentSchedules = mongoTemplate.find(Query.query(Criteria.where("staff").in(userId))
				.addCriteria(Criteria.where("assignmentId").is(assignments.getAssignmentId())), AssignmentSchedules.class);
		
		if (assignmentSchedules != null && assignmentSchedules.size() > 0) {
			long totalShifts = assignmentSchedules.stream().map(AssignmentSchedules::getWeekDay).collect(Collectors.toList()).size();
			
			Assignments assignment = mongoTemplate.findOne(Query.query(Criteria.where("_id").is(assignments.getAssignmentId())), Assignments.class);
			
			long totalWeeks = (ChronoUnit.DAYS.between(assignment.getStartDate(), assignments.getEndDate())+1)/7;
			
			return ""+totalShifts*totalWeeks; 
		}else
			return "-";
	}
	
	public String getDuration (Assignments assignment) {
		
		if (assignment.getStartDate() != null && assignment.getEndDate() != null) {
		
		long days = (ChronoUnit.DAYS.between(assignment.getStartDate(), assignment.getEndDate())+1);
			
			if (days > 31) {
				
				if (days/30 > 1)
					return days/30+" Months";
				else
					return days/30+" Month";
			}
			else {
				if (days/7 > 1)
					return days/7 +" Weeks";
				else
					return days/7 +" Week";
			}
		}else 
			return "";
	}
	
	public String getStaffUserCoreTypeNameByUserId (Assignments assignment, long userId) {
		
		AssignmentSchedules assignmentSchedule	= mongoTemplate.findOne(Query.query(Criteria.where("staff").in(userId))
				.addCriteria(Criteria.where("assignmentId").is(assignment.getAssignmentId())), AssignmentSchedules.class);
		
		if (assignmentSchedule != null)
			return mongoTemplate.findOne(Query.query(Criteria.where("id").is(assignmentSchedule.getStaffCoreTypeId())), StaffUserCoreTypes.class).getLabel();
		else
			return "-";
	}
	
	public int getAcuityLevelByUserId(Assignments assignment, long userId) {
	
		AssignmentSchedules assignmentSchedule	= mongoTemplate.findOne(Query.query(Criteria.where("staff").in(userId))
				.addCriteria(Criteria.where("assignmentId").is(assignment.getAssignmentId())), AssignmentSchedules.class);
		
		if (assignmentSchedule != null)
			return (int) assignmentSchedule.getAcuityLevel();
		else
			return 0;	
	}
	
	public String getSubCoreTypeNameByUserId (Assignments assignment, long userId) {
		
		AssignmentSchedules assignmentSchedule	= mongoTemplate.findOne(Query.query(Criteria.where("staff").in(userId))
				.addCriteria(Criteria.where("assignmentId").is(assignment.getAssignmentId())), AssignmentSchedules.class);
		
		if (assignmentSchedule != null) {
			SubRoleTypes subRole = mongoTemplate.findOne(Query.query(Criteria.where("id").is(assignmentSchedule.getSubCoreTypeId())), SubRoleTypes.class);
			
			if (subRole != null) {
				return subRole.getLabel();
			}else
				return "-";
		}
		else
			return "-";	
	}
	
	public String getStatusByUserId (Assignments assignment, long userId) {
		
		AssignmentInvitees assignmentInvitee = mongoTemplate.findOne(Query.query(Criteria.where("staffId").is(userId))
				.addCriteria(Criteria.where("assignmentId").is(assignment.getAssignmentId())), AssignmentInvitees.class);
		
		if (assignmentInvitee != null) {

			if (assignmentInvitee.isAccepted() == true && assignmentInvitee.isApproved() == true)
				return "Approved";
			else if (assignmentInvitee.isAccepted() == true && assignmentInvitee.isApproved() == false)
				return "Pending Approval";
			else
				return "-";
		}else
			return "-";
	}
	
	public String assignmentTimeByUserIdAndStartDate (Assignments assignment, long userId, String startDate) {
		
		SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
		Date date = null;
		try {
			date = simpleDateFormat1.parse(startDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		DateFormat weekDayFormatter = new SimpleDateFormat("EEEE"); // change the date in weekday
		String weekDay = weekDayFormatter.format(date).toLowerCase();
		AssignmentSchedules assignmentSchedule = mongoTemplate.findOne(Query.query(Criteria.where("staff").in(userId)).addCriteria(Criteria.where("assignmentId").is(assignment.getAssignmentId()))
				.addCriteria(Criteria.where("weekDay").regex(weekDay,"i")), AssignmentSchedules.class);
		
		if (assignmentSchedule != null)
			return assignmentSchedule.getStartTime()+"-"+assignmentSchedule.getEndTime();
		else
			return "-";
	}
	
	public List<AssignmentSchedulesByDate> schedulesByDate (Assignments assignment) throws ParseException {
		
		List<AssignmentSchedulesByDate> assignmentSchedulesByDate = new ArrayList<>(); // collects all required data
		
		if (assignment.getStartDate() != null && assignment.getEndDate() != null) {
			
			Map<String, AssignmentSchedules> assignmentSchedules = assignmentScheduleDAO.findAssignmentScheduleByAssignmentId(assignment.getAssignmentId()); //fetch all the schedules by id
			
			List<Long> userIds = assignmentSchedules.values().stream().flatMap(emitter -> emitter.getStaff().stream()).distinct().collect(Collectors.toList()); //fetch all the users assigned to assignment schedules.

			Set<String> keySet = assignmentSchedules.keySet();	// get all the userId key set for comparing

			Map<Long, AssignmentInvitees> assignmentInvitees = assignmentInviteesDAO.findByStaffIdsAndAssignmentIdAndIsApproved(userIds, assignment.getAssignmentId(), true); // fetch the invitees record which are been approved. 

			LocalDate startDate = assignment.getStartDate();	//store the start date of the assignment
			LocalDate endDate = assignment.getEndDate().plusDays(1); //store the end date of the assignment
			
			SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd");	// for converting date into day format
			Date date = null;
			
			while (assignmentInvitees.size() > 0 && startDate.isBefore(endDate)) { // for creating assignment schedules record grouped with date
				
				date = simpleDateFormat1.parse(startDate.toString());
				DateFormat weekDayFormatter = new SimpleDateFormat("EEEE"); // change the date in weekday
				String weekDay = weekDayFormatter.format(date).toLowerCase();
	
				if (keySet.contains(weekDay)) {	// if weekday exist in keySet 
					
					List<Long> listOfUser = new ArrayList<>();
					
					for (Long eachInvitee : assignmentInvitees.keySet()) {
						if (assignmentSchedules.get(weekDay).getStaff().contains(eachInvitee)) {
							listOfUser.add(eachInvitee);
						}
					}
					
					if (listOfUser.size() > 0) {
						
						AssignmentSchedulesByDate assignmentSchedulesByDateObj = new AssignmentSchedulesByDate();
					
						assignmentSchedulesByDateObj.setListOfUsers(mongoTemplate.find(Query.query(Criteria.where("_id").in(listOfUser)), UserBasicInformation.class));
						assignmentSchedulesByDateObj.setStartTime(assignmentSchedules.get(weekDay).getStartTime());
						assignmentSchedulesByDateObj.setEndTime(assignmentSchedules.get(weekDay).getEndTime());
						assignmentSchedulesByDateObj.setScheduleDate(startDate);
					
						assignmentSchedulesByDate.add(assignmentSchedulesByDateObj);
					}
				}
				startDate = startDate.plusDays(1);
			}
			return assignmentSchedulesByDate;
		} else
			return assignmentSchedulesByDate;
	}
}