	package com.java.floatcare.resolver.query;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.java.floatcare.api.request.pojo.OrganizationBusinessTypeCounts;
import com.java.floatcare.dao.UserTypeDAO;
import com.java.floatcare.model.BusinessTypes;
import com.java.floatcare.model.Businesses;
import com.java.floatcare.model.Organizations;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.model.UserWorkspaces;
import com.java.floatcare.repository.BusinessTypesRepository;
import com.java.floatcare.repository.BusinessesRepository;
import com.java.floatcare.repository.UserWorkspacesRepository;

@Component
public class OrganizationQuery implements GraphQLResolver<Organizations> {

	@Autowired
	private BusinessesRepository businessesRepository;
	@Autowired
	private BusinessTypesRepository businessTypesRepository;
	@Autowired
	private UserTypeDAO userTypeDAO;
	@Autowired
	private UserWorkspacesRepository userWorkspacesRepository;

	public Iterable<Businesses> getBusinesses(Organizations organizations) {

		return businessesRepository.findBusinessesByOrganizationId(organizations.getOrganizationId());
	}

	public Iterable<UserBasicInformation> getAccountOwners(Organizations organizations) {

		List<UserBasicInformation> accountOwners = userTypeDAO.findUsersByOrganizationIdAndUserTypeId(organizations.getOrganizationId(), 2);
		return accountOwners;
	}
	
	public Iterable<UserBasicInformation> getStaffMembers(Organizations organizations) {

		List<UserBasicInformation> staffMembers = userTypeDAO.findUsersByOrganizationIdAndUserTypeIdNot(organizations.getOrganizationId(), 1); // return all staff members without userTypeId as 1

		return staffMembers;
	}

	public Iterable<OrganizationBusinessTypeCounts> getBusinessTypes(Organizations organization) {

		List<OrganizationBusinessTypeCounts> listOrganizationBusinessTypeCounts = new ArrayList<OrganizationBusinessTypeCounts>();

		List<BusinessTypes> businessTypes = businessTypesRepository.findAll();

		for (BusinessTypes eachBusinessType : businessTypes) {

			List<Businesses> businesses = businessesRepository.findBusinessesByBusinessTypeIdAndOrganizationId(eachBusinessType.getBusinessTypeId(), organization.getOrganizationId());

			if (businesses.size() > 0) {
				OrganizationBusinessTypeCounts organizationBusinessTypeCounts = new OrganizationBusinessTypeCounts();

				organizationBusinessTypeCounts.setBusinessTypeCount(businesses.size());
				organizationBusinessTypeCounts.setBusinessTypeName(eachBusinessType.getLabel());

				listOrganizationBusinessTypeCounts.add(organizationBusinessTypeCounts);
			}
		}
		return listOrganizationBusinessTypeCounts;
	}
	
	public Iterable<Businesses> getBusinessesByUserId (Organizations organizations, long userId) {
		
		List<UserWorkspaces> userWorkspaces = userWorkspacesRepository.findUserWorkspacesByUserIdAndOrganizationId(userId, organizations.getOrganizationId());
		
		List<Long> businessIds = userWorkspaces.stream().map(emitter -> emitter.getBusinessId()).collect(Collectors.toList());
		
		return businessesRepository.findAllById(businessIds);
	}
}