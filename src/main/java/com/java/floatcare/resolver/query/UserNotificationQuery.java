package com.java.floatcare.resolver.query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.java.floatcare.model.NotificationTypes;
import com.java.floatcare.model.UserNotification;
import com.java.floatcare.repository.NotificationTypesRepository;

@Component
public class UserNotificationQuery implements GraphQLResolver<UserNotification>{

	@Autowired
	private NotificationTypesRepository notificationTypesRepository;
	
	public String getNotificationTypeName(UserNotification userNotification) {
		
			NotificationTypes type = notificationTypesRepository.findNotificationTypesByNotificationTypeId(userNotification.getNotificationTypeId());
			return type.getLabel();
	}
	
}
