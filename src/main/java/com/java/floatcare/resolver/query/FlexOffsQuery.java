package com.java.floatcare.resolver.query;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.java.floatcare.model.Businesses;
import com.java.floatcare.model.Department;
import com.java.floatcare.model.DepartmentShifts;
import com.java.floatcare.model.FlexOffInvitees;
import com.java.floatcare.model.FlexOffs;
import com.java.floatcare.model.Organizations;
import com.java.floatcare.model.StaffUserCoreTypes;
import com.java.floatcare.repository.BusinessesRepository;
import com.java.floatcare.repository.DepartmentRepository;
import com.java.floatcare.repository.DepartmentShiftsRepository;
import com.java.floatcare.repository.FlexOffInviteesRepository;
import com.java.floatcare.repository.OrganizationRepository;
import com.java.floatcare.repository.StaffUserCoreTypesRepository;

@Component
public class FlexOffsQuery implements GraphQLResolver<FlexOffs> {

	@Autowired
	private FlexOffInviteesRepository flexOffInviteesRepository;
	@Autowired
	private StaffUserCoreTypesRepository staffUserCoreTypesRepository;
	@Autowired
	private DepartmentShiftsRepository departmentShiftsRepository;
	@Autowired
	private DepartmentRepository departmentRepository;
	@Autowired
	private BusinessesRepository businessesRepository;
	@Autowired
	private OrganizationRepository organizationRepository;
	
	public List<FlexOffInvitees> getInvitees (FlexOffs flexOff) {
		
		List<FlexOffInvitees> flexOffList = flexOffInviteesRepository.findByFlexOffId(flexOff.getFlexOffId());
		
		if (flexOffList.size() > 0)
			return flexOffList;
		else
			return null;
	}
	
	public String getStaffUserName (FlexOffs flexOff) {
		
		Optional<StaffUserCoreTypes> staffUser = staffUserCoreTypesRepository.findById(flexOff.getStaffUserCoreTypeId());
		
		if (flexOff.getQuota()  < 2)
			return staffUser.get().getLabel();
		else
			return staffUser.get().getLabel()+"s";
	}
	
	public String getDepartmentName (FlexOffs flexOff) {
		
		Optional<Department> staffUser = departmentRepository.findById(flexOff.getDepartmentId());
		
		if (staffUser.isPresent())
			return staffUser.get().getDepartmentTypeName();
		else
			return null;
	}
	
	public Organizations getOrganization (FlexOffs flexOff) {
		
		Optional<Businesses> businesses = businessesRepository.findById(flexOff.getWorksiteId());
		
		if (businesses.isPresent()) {
			
			Optional<Organizations> organizations = organizationRepository.findById(businesses.get().getOrganizationId());
			return organizations.get();
		}
		
		else
			return null;
	}
	
	public DepartmentShifts getShiftType (FlexOffs flexOff) {
	
		Optional<DepartmentShifts> departmentShiftsOpt = departmentShiftsRepository.findById(flexOff.getShiftTypeId());
		
		if (departmentShiftsOpt.isPresent())
			return departmentShiftsOpt.get();
		else
			return null;
		
	}
	
	public String getWorksiteName (FlexOffs flexOff) {
		
		Optional<Businesses> worksiteOpt = businessesRepository.findById(flexOff.getWorksiteId());
		
		if (worksiteOpt.isPresent())
			return worksiteOpt.get().getName();
		else
			return null;
	}
	
	public String getWorksiteAddress (FlexOffs flexOff) {
		
		Optional<Businesses> worksiteOpt = businessesRepository.findById(flexOff.getWorksiteId());
		
		if (worksiteOpt.isPresent())
			return worksiteOpt.get().getCity() + " " + worksiteOpt.get().getState() + " " + worksiteOpt.get().getAddress();
		else
			return null;
	}
}