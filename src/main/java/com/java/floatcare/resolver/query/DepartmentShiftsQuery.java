package com.java.floatcare.resolver.query;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.java.floatcare.model.DepartmentShifts;
import com.java.floatcare.model.DepartmentStaff;
import com.java.floatcare.model.StaffUserCoreTypes;
import com.java.floatcare.repository.StaffUserCoreTypesRepository;

@Component
public class DepartmentShiftsQuery implements GraphQLResolver<DepartmentShifts>{
	
	@Autowired
    private StaffUserCoreTypesRepository staffUserCoreTypesRepository;
	
	
	public List<StaffUserCoreTypes> getStaffCoreTypes(DepartmentShifts departmentShifts){
		if(departmentShifts.getStaffCoreTypeIds()!=null)
		return (List<StaffUserCoreTypes>) staffUserCoreTypesRepository.findAllById(departmentShifts.getStaffCoreTypeIds());
		else
			return null;

	}
}
