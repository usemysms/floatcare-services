package com.java.floatcare.resolver.query;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.java.floatcare.model.Businesses;
import com.java.floatcare.model.Department;
import com.java.floatcare.model.DepartmentShifts;
import com.java.floatcare.model.SetScheduleLayouts;
import com.java.floatcare.model.StaffUserCoreTypes;
import com.java.floatcare.model.StaffWeekOffs;
import com.java.floatcare.repository.BusinessesRepository;
import com.java.floatcare.repository.DepartmentRepository;
import com.java.floatcare.repository.DepartmentShiftsRepository;
import com.java.floatcare.repository.StaffUserCoreTypesRepository;

@Component
public class SetScheduleLayoutsQuery implements GraphQLResolver<SetScheduleLayouts> {

	@Autowired
	private DepartmentRepository departmentRepository;
	@Autowired
	private BusinessesRepository businessesRepository;
	@Autowired
	private DepartmentShiftsRepository departmentShiftsRepository;
	@Autowired
	private StaffUserCoreTypesRepository staffUserCoreTypesRepository;
	@Autowired
	private MongoTemplate mongoTemplate;
	
	public List<StaffWeekOffs> getStaffWeekOffs (SetScheduleLayouts setScheduleLayouts) {
		
		return mongoTemplate.find(Query.query(Criteria.where("setScheduleLayoutId").
				is(setScheduleLayouts.getSetScheduleLayoutId())), StaffWeekOffs.class);
	}
	
	public Department getDepartmentDetails (SetScheduleLayouts setScheduleLayouts) {
	
		Optional<Department> departmentOptional = departmentRepository.findById(setScheduleLayouts.getDepartmentId());
		
		if (departmentOptional.isPresent())
			return departmentOptional.get();
		else
			return null;
	}
	
	public Businesses getWorksiteDetails (SetScheduleLayouts setScheduleLayouts) {
		
		Optional<Businesses> businessOptional = businessesRepository.findById(setScheduleLayouts.getWorksiteId());
		
		if (businessOptional.isPresent())
			return businessOptional.get();
		else
			return null;
	}
	
	public List<DepartmentShifts> getShiftTypes(SetScheduleLayouts setScheduleLayout) {
		
		List<DepartmentShifts> shiftNames = new ArrayList<>();
		
		for (Long eachShiftId : setScheduleLayout.getShiftTypes()) {
			
			shiftNames.add(departmentShiftsRepository.findById(eachShiftId).get());
		}
		return shiftNames;
	}
	
	public List<StaffUserCoreTypes> getRoleTypes(SetScheduleLayouts setScheduleLayouts) {
		
		List<StaffUserCoreTypes> roleTypes = new ArrayList<>();
		
		for (Long eachRoleId : setScheduleLayouts.getRoles()) {
			
			roleTypes.add(staffUserCoreTypesRepository.findById(eachRoleId).get());
		}
		
		return roleTypes;
	}
	
	public String getSetScheduleLength(SetScheduleLayouts setScheduleLayouts) {
		
		String[] scheduleLength = setScheduleLayouts.getScheduleLength().split("(?<=\\d)(?=\\D)");
		
		if (scheduleLength.length == 1)
			return "Indefinately";
		
		if (scheduleLength[1].equalsIgnoreCase("w")) {
			if (Long.parseLong(scheduleLength[0]) > 1)
				return scheduleLength[0]+" "+"Weeks";
			else
				return scheduleLength[0]+" "+"Week";
		}
		else if (scheduleLength[1].equalsIgnoreCase("m")) {
			
			if (Long.parseLong(scheduleLength[0]) > 1)
				return scheduleLength[0]+" "+"Months";
			else
				return scheduleLength[0]+" "+"Month";
		}else
			return "";
	}
}
