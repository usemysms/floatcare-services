package com.java.floatcare.resolver.query;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.java.floatcare.dao.DepartmentShiftsDAO;
import com.java.floatcare.model.Businesses;
import com.java.floatcare.model.Department;
import com.java.floatcare.model.DepartmentShifts;
import com.java.floatcare.model.RequestTypes;
import com.java.floatcare.model.Requests;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.model.UserWorkspaces;
import com.java.floatcare.model.WorksiteSettings;
import com.java.floatcare.repository.BusinessesRepository;
import com.java.floatcare.repository.DepartmentRepository;
import com.java.floatcare.repository.RequestTypesRepository;
import com.java.floatcare.repository.UserBasicInformationRepository;
import com.java.floatcare.repository.UserWorkspacesRepository;
import com.java.floatcare.repository.WorksiteSettingsRepository;

@Component
public class RequestsQuery implements GraphQLResolver<Requests>{

	@Autowired
	private BusinessesRepository businessesRepository;
	
	@Autowired
	private RequestTypesRepository requestTypesRepository;
	
	@Autowired
	private DepartmentShiftsDAO departmentShiftsDAO;
	
	@Autowired
	private UserBasicInformationRepository userBasicInformationRepository;
	
	@Autowired
	private DepartmentRepository departmentRepository;
	
	@Autowired
	private UserWorkspacesRepository userWorkspacesRepository;
	
	@Autowired
	private WorksiteSettingsRepository worksiteSettingsRepository;
	
	
	public String getRequestTypeName(Requests requests) {
		
		Optional<RequestTypes> requestTypes = requestTypesRepository.findById(requests.getRequestTypeId());
		
		if (requestTypes.isPresent())
			return requestTypes.get().getLabel();
		else
			return null;
	}
	
	public String getWorksiteName(Requests requests) {
		
		Optional<Businesses> business = businessesRepository.findById(requests.getWorksiteId());
		
		if (business.isPresent())
			return business.get().getName();
		else
			return null;
	}
	
	public String getColleagueName(Requests requests) {
		
		Optional<UserBasicInformation> colleague = userBasicInformationRepository.findById(requests.getColleagueId());
		
		if (colleague.isPresent())
			return colleague.get().getFirstName()+" "+colleague.get().getLastName();
		else
			return null;
	}
	
	public List<DepartmentShifts> getShiftType (Requests requests) {
		
		UserWorkspaces user = userWorkspacesRepository.findByUserIdAndWorkspaceIdAndStatusAndIsJoined(requests.getUserId(), requests.getDepartmentId(), "Active", true);
		
		if (user != null) {
			List<DepartmentShifts> departmentShiftsOpt = departmentShiftsDAO.findByIdAndDepartmentId(user.getDepartmentShiftIds(), requests.getDepartmentId());
			return departmentShiftsOpt;
		}else
			return null;
	}

	public String getShiftName (Requests requests) {

		List<DepartmentShifts> departmentShiftsOpt = departmentShiftsDAO.findByIdAndDepartmentId(requests.getShiftIds(), requests.getDepartmentId());
		
		if (departmentShiftsOpt != null) {
			return departmentShiftsOpt.stream().map(DepartmentShifts::getLabel).collect(Collectors.joining(", "));
			//return departmentShiftsOpt.getLabel();
		}
		else
			return null;
	}
	
	public UserBasicInformation getUserInformation (Requests requests) {

		Optional<UserBasicInformation> userOptional = userBasicInformationRepository.findByUserIdAndStatus(requests.getUserId(), "Active");

		if (userOptional.isPresent())
			return userOptional.get();
		else
			return null;
	}
	
	public UserWorkspaces getUserWorkspaceInformation (Requests requests) {
		
		UserWorkspaces userWorkspacesOptional = userWorkspacesRepository.findByUserIdAndWorkspaceIdAndStatusAndIsJoined(requests.getUserId(), requests.getDepartmentId(), "Active", true);
		
		if (userWorkspacesOptional != null)
			return userWorkspacesOptional;
		else
			return null;
	}
	
	public Businesses getWorksite (Requests requests) {
		
		Optional<Businesses> businessOptional = businessesRepository.findById(requests.getWorksiteId());
		
		if (businessOptional != null)
			return businessOptional.get();
		else
			return null;
	}
	
	public String getDepartmentName (Requests requests) {
		
		Optional<Department> departmentOptional = departmentRepository.findById(requests.getDepartmentId());
		
		if (departmentOptional != null)
			return departmentOptional.get().getDepartmentTypeName();
		else
			return null;
	}
	
	public String getWorksiteColor (Requests requests) {
		
		WorksiteSettings color = worksiteSettingsRepository.findByUserIdAndWorksiteId(requests.getUserId(), requests.getWorksiteId());
		
		if (color != null)
			return color.getColor();
		else
			return null;
	}
}
