package com.java.floatcare.resolver.query;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.java.floatcare.dao.UserBasicInformationRepositoryDAO;
import com.java.floatcare.dao.UserWorkspacesDAO;
import com.java.floatcare.model.Businesses;
import com.java.floatcare.model.Department;
import com.java.floatcare.model.DepartmentShifts;
import com.java.floatcare.model.Organizations;
import com.java.floatcare.model.Schedules;
import com.java.floatcare.model.StaffUserCoreTypes;
import com.java.floatcare.model.SubRoleTypes;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.model.UserCredentials;
import com.java.floatcare.model.UserEducation;
import com.java.floatcare.model.UserTypes;
import com.java.floatcare.model.UserWorkExperience;
import com.java.floatcare.model.UserWorkspaces;
import com.java.floatcare.repository.BusinessesRepository;
import com.java.floatcare.repository.DepartmentRepository;
import com.java.floatcare.repository.DepartmentShiftsRepository;
import com.java.floatcare.repository.OrganizationRepository;
import com.java.floatcare.repository.SchedulesRepository;
import com.java.floatcare.repository.StaffUserCoreTypesRepository;
import com.java.floatcare.repository.SubRoleTypesRepository;
import com.java.floatcare.repository.UserBasicInformationRepository;
import com.java.floatcare.repository.UserCredentialsRepository;
import com.java.floatcare.repository.UserEducationRepository;
import com.java.floatcare.repository.UserTypesRepository;
import com.java.floatcare.repository.UserWorkExperienceRepository;
import com.java.floatcare.repository.UserWorkspacesRepository;
import com.java.floatcare.utils.ConfigProperties;
import com.java.floatcare.utils.DistanceCalculate;

@Component
public class UserBasicInformationQuery implements GraphQLResolver<UserBasicInformation> {

	@Autowired
	private UserTypesRepository userTypesRepository;
	@Autowired
	private UserWorkExperienceRepository userWorkExperienceRepository;
	@Autowired
	private UserCredentialsRepository userCredentialsRepository;
	@Autowired
	private UserEducationRepository userEducationRepository;
	@Autowired
	private UserBasicInformationRepository userBasicInformationRepository;
	@Autowired
	private StaffUserCoreTypesRepository staffUserCoreTypesRepository;
	@Autowired
	private SubRoleTypesRepository subRoleTypeRepository;
	@Autowired
	private UserWorkspacesRepository userWorkspacesRepository;
	@Autowired
	private OrganizationRepository organizationRepository;
	@Autowired
	private BusinessesRepository businessesRepository;
	@Autowired
	private DepartmentRepository departmentRepository;
	@Autowired
	private DepartmentShiftsRepository departmentShiftsRepository;
	@Autowired
	private UserBasicInformationRepositoryDAO userBasicInformationRepositoryDAO;
	@Autowired
	private SchedulesRepository schedulesRepository;
	@Autowired
	private UserWorkspacesDAO userWorkspacesDAO;
	@Autowired
	private ConfigProperties configProperties;
	
	private long departmentId;
	
	private String date;
	
	private int index;

	public long getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(long departmentId) {
		this.departmentId = departmentId;
	}
	
	public int getIndex() {
		return index;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public String getUserTypeName(UserBasicInformation userBasicInformation) {

		Optional<UserTypes> userTypes = userTypesRepository.findById(userBasicInformation.getUserTypeId());

		if (userTypes.isPresent())
			return userTypes.get().getLabel();

		return null;
	}

	public String getStaffUserCoreTypeName(UserBasicInformation userBasicInformation) {

		Optional<UserBasicInformation> user = userBasicInformationRepository
				.findUserByUserId(userBasicInformation.getUserId());

		if (user.isPresent()) {
			Optional<StaffUserCoreTypes> optional = staffUserCoreTypesRepository.findById(user.get().getStaffUserCoreTypeId());
			if (optional.isPresent())
				return optional.get().getLabel();
		}
		return null;
	}

	public String getStaffUserSubCoreTypeName (UserBasicInformation userBasicInformation) {
		
		Optional<UserBasicInformation> user = userBasicInformationRepository.findUserByUserId(userBasicInformation.getUserId());

		if (user.isPresent()) {
			Optional<SubRoleTypes> optional = subRoleTypeRepository.findById(user.get().getStaffUserSubCoreTypeId());
			if (optional.isPresent())
				return optional.get().getLabel();
			else
				return null;
		}
		return null;
	}
	
	public String getStaffUserSubCoreTypeSpeciality (UserBasicInformation userBasicInformation) {
			
			Optional<UserBasicInformation> user = userBasicInformationRepository.findUserByUserId(userBasicInformation.getUserId());
	
			if (user.isPresent()) {
				Optional<SubRoleTypes> optional = subRoleTypeRepository.findById(user.get().getStaffUserSubCoreTypeId());
				if (optional.isPresent())
					return optional.get().getSpecialist();
				else
					return null;
			}
			return null;
		}
	
		public UserWorkExperience getUserWorkExperience(UserBasicInformation userBasicInformation) {

		Optional<UserWorkExperience> userWorkExperience = userWorkExperienceRepository.findUserWorkExperienceByUserId(userBasicInformation.getUserId());
		
		if (userWorkExperience.isPresent())
			return userWorkExperience.get();

		else
			return null;
	}

	public UserCredentials getUserCredentials(UserBasicInformation userBasicInformation) {

		Optional<UserCredentials> userCredentials = userCredentialsRepository.findUserCredentialsByUserId(userBasicInformation.getUserId());

		if (userCredentials.isPresent())
			return userCredentials.get();

		else
			return null;
	}

	public UserEducation getUserEducation(UserBasicInformation userBasicInformation) {

		Optional<UserEducation> userEducation = userEducationRepository
				.findUserEducationByUserId(userBasicInformation.getUserId());

		if (userEducation.isPresent())
			return userEducation.get();

		else
			return null;
	}

	public List<UserWorkspaces> getWorkspaces(UserBasicInformation userBasicInformation) {
		
		List<UserWorkspaces> userWorkSpaces = userWorkspacesRepository.findUserWorkspacesByUserId(userBasicInformation.getUserId());

		if (userWorkSpaces.size() > 0) {
			for (UserWorkspaces each : userWorkSpaces) {

				switch (each.getWorkspaceType().toLowerCase()) {

				case "organization":
					List<Organizations> organizations = organizationRepository.findByOrganizationId(each.getOrganizationId());

					if (organizations.size() > 0) {
						each.setWorkspaceName(organizations.get(0).getName());
					}
					break;
				case "business":
					List<Businesses> businesses = businessesRepository.findBusinessByBusinessId(each.getBusinessId());
					if (businesses.size() > 0) {
						each.setWorkspaceName(businesses.get(0).getName());
					}
					break;
				case "department":
					List<Department> department = departmentRepository.findDepartmentByDepartmentId(each.getWorkspaceId());
					if (department.size()>0) {
						each.setWorkspaceName(department.get(0).getDepartmentTypeName());
					}
					break;
				default:
					break;
				}
			}
			return userWorkSpaces;
		} else {
			return null;
		}
	}
	
	public List<UserWorkspaces> getWorkspacesBasedOnOrganizationId(UserBasicInformation userBasicInformation, long organizationIds) {
		
		List<UserWorkspaces> userWorkSpaces = userWorkspacesRepository.findByOrganizationIdAndUserId(organizationIds, userBasicInformation.getUserId());

		if (userWorkSpaces.size() > 0) {
			for (UserWorkspaces each : userWorkSpaces) {

				switch (each.getWorkspaceType().toLowerCase()) {

				case "organization":
					List<Organizations> organizations = organizationRepository.findByOrganizationId(each.getOrganizationId());

					if (organizations.size() > 0) {
						each.setWorkspaceName(organizations.get(0).getName());
					}
					break;
				case "business":
					List<Businesses> businesses = businessesRepository.findBusinessByBusinessId(each.getBusinessId());
					if (businesses.size() > 0) {
						each.setWorkspaceName(businesses.get(0).getName());
					}
					break;
				case "department":
					List<Department> department = departmentRepository.findDepartmentByDepartmentId(each.getWorkspaceId());
					if (department.size()>0) {
						each.setWorkspaceName(department.get(0).getDepartmentTypeName());
					}
					break;
				default:
					break;
				}
			}
			return userWorkSpaces;
		} else {
			return null;
		}
	}
	
	public UserWorkspaces getUserWorkspace (UserBasicInformation userBasicInformation) {
		
		if (departmentId > 0)
			return userWorkspacesRepository.findByUserIdAndWorkspaceIdAndStatusAndIsJoined(userBasicInformation.getUserId(), this.getDepartmentId(), "Active", true);
		
		else {
			
			List<UserWorkspaces> userWorkspace = userWorkspacesRepository.findUserWorkspacesByUserIdAndStatusAndIsJoined(userBasicInformation.getOrganizationId(), "Active", true);
			
			Random random = new Random();
			int index = random.nextInt(userWorkspace.size());
			this.setIndex(index);
			return userWorkspace.get(index);
		}	
	}
	
	public List<DepartmentShifts> getShiftType (UserBasicInformation userBasicInformation) {
	
		List<DepartmentShifts> departmentShifts = new ArrayList<DepartmentShifts>();
		if (departmentId > 0 && date != null) {
			
			long userId = userBasicInformationRepositoryDAO.findUserByIdAndUserTypeId(userBasicInformation.getUserId(), 3);
			
			if (userId > 0) {
				
				Schedules schedule = schedulesRepository.findSchedulesByUserIdAndShiftDateAndDepartmentId(userId, LocalDate.parse(date), departmentId);
				
				UserWorkspaces userWorkspaces = userWorkspacesRepository.findByUserIdAndWorkspaceIdAndStatusAndIsJoined(userBasicInformation.getUserId(), departmentId, "Active", true);
				
				
				
				if (schedule == null) {
					if (userWorkspaces != null && userWorkspaces.isPrimaryDepartment() == true)
						departmentShifts = (List<DepartmentShifts>) departmentShiftsRepository.findAllById(userWorkspaces.getDepartmentShiftIds());
					else
						departmentShifts = null;
				}
				else {
					departmentShifts.add(departmentShiftsRepository.findById(schedule.getShiftTypeId()).get());
				}
				if (departmentShifts != null)
					return departmentShifts;
				else
					return null;
			} else 
				return null;
		}else {
			List<UserWorkspaces> userWorkspace = userWorkspacesRepository.findUserWorkspacesByUserIdAndStatusAndIsJoined(userBasicInformation.getUserId(), "Active", true);
			
			departmentShifts = (List<DepartmentShifts>) departmentShiftsRepository.findAllById(userWorkspace.get(this.getIndex()).getDepartmentShiftIds());
			
			if (departmentShifts!=null)
				return departmentShifts;
			else
				return null;
		}
	}
	
	public List<DepartmentShifts> getShiftTypeByDepartmentId (UserBasicInformation userBasicInformation, long departmentId) {
		
		List<DepartmentShifts> departmentShifts = new ArrayList<DepartmentShifts>();
		
		if (date != null) {
			
			long userId = userBasicInformationRepositoryDAO.findUserByIdAndUserTypeId(userBasicInformation.getUserId(), 3);
			
			if (userId > 0) {
				
				Schedules schedule = schedulesRepository.findSchedulesByUserIdAndShiftDateAndDepartmentIdAndStatus(userId, LocalDate.parse(date), departmentId, "Active");
				
				UserWorkspaces userWorkspaces = userWorkspacesRepository.findByUserIdAndWorkspaceIdAndStatusAndIsJoined(userBasicInformation.getUserId(), departmentId, "Active", true);
				
				
				
				if (schedule == null)
					if (userWorkspaces != null && userWorkspaces.isPrimaryDepartment() == true)
						departmentShifts = (List<DepartmentShifts>) departmentShiftsRepository.findAllById(userWorkspaces.getDepartmentShiftIds());
					else
						departmentShifts = null;
				else
					departmentShifts.add(departmentShiftsRepository.findById(schedule.getShiftTypeId()).get());
			
				if (departmentShifts != null)
					return departmentShifts;
				else
					return null;
			} else 
				return null;
		}else {
			List<UserWorkspaces> userWorkspace = userWorkspacesRepository.findUserWorkspacesByUserIdAndStatusAndIsJoined(userBasicInformation.getUserId(), "Active", true);
			
			 departmentShifts = (List<DepartmentShifts>) departmentShiftsRepository.findAllById(userWorkspace.get(this.getIndex()).getDepartmentShiftIds());
			
			if (departmentShifts!=null)
				return departmentShifts;
			else
				return null;
		}
	}
	
	public String getStaffUserCoreTypeNameByDepartmentId(UserBasicInformation userBasicInformation, long departmentId) {
		
		UserWorkspaces userWorkspace = userWorkspacesRepository.findByUserIdAndWorkspaceId(userBasicInformation.getUserId(), departmentId);
		
		if (userWorkspace != null) {
			long staffUserCoreTypeId = userWorkspace.getStaffUserCoreTypeId();
			Optional<StaffUserCoreTypes> staffCoreType = staffUserCoreTypesRepository.findById(staffUserCoreTypeId);
			if (staffCoreType.isPresent())
				return staffCoreType.get().getLabel();
			else
				return null;
		}	
		else
			return null;
	}

	public Organizations getOrganizationDetails (UserBasicInformation userBasicInformation) {

		Optional<UserBasicInformation> user = userBasicInformationRepository.findByUserIdAndStatus(userBasicInformation.getUserId(), "Active");

		if (user.isPresent()) {

			Optional<Organizations> organizationOptional = organizationRepository.findById(userBasicInformation.getOrganizationId());
			return organizationOptional.isPresent() ? organizationOptional.get() : null;
		}else
			return null;
	}

	public boolean getPreferredProviderInWorksite(UserBasicInformation userBasicInformation, long businessId) {

		Businesses businesses = businessesRepository.findPreferredProviderIdByUserIdAndBusinessId(userBasicInformation.getUserId(), businessId);
		return businesses != null ? true : false;
	}

	public String getAge(UserBasicInformation userBasicInformation) {

		if (userBasicInformation.getDateOfBirth() != null) {
			return LocalDate.now().getYear() - userBasicInformation.getDateOfBirth().getYear()+"";
		}else
			return "Please provide date of birth!!";
	}

	public Iterable<Businesses> getPractices(UserBasicInformation userBasicInformation) {

		return businessesRepository.findAllById(userWorkspacesDAO.findBusinessesIdsByUserId(userBasicInformation.getUserId()));
	}

	public String getDistanceFromPatient(UserBasicInformation userBasicInformation, String patientLatitude, String patientLongitude) {

		if (userBasicInformation.getLongitude() != null && userBasicInformation.getLatitude() != null
				&& patientLatitude != null && patientLongitude != null)
			return DistanceCalculate.distance(Double.parseDouble(userBasicInformation.getLatitude()), Double.parseDouble(patientLatitude),
					Double.parseDouble(userBasicInformation.getLongitude()), Double.parseDouble(patientLongitude))+" mi.";
		else
			return "-";
	}

	public int getReferralCount(UserBasicInformation userBasicInformation) {

		if (userBasicInformation.getUserId() > 0) {
			
			RestTemplate restTemplate = new RestTemplate();
	
			HttpHeaders httpHeaders = new HttpHeaders();
			
			httpHeaders.add("Authorization", "Bearer *************");
			httpHeaders.add("content-type", "application/graphql"); // maintain graphql
	
			String query = "{\r\n"
					+ "  getReferralCount(userId: "+userBasicInformation.getUserId()+")"
					+ "}\r\n";
	
			ResponseEntity<String> response = restTemplate.postForEntity(configProperties.getReferralServiceGraphqlEndPoint(), new HttpEntity<>(query, httpHeaders), String.class);
			
			@SuppressWarnings("deprecation")
			JsonObject jsonObject = new JsonParser().parse(response.getBody()).getAsJsonObject();
	
			if(jsonObject.get("data").isJsonNull() == false) {
				
				JsonObject jsonDataObject = jsonObject.get("data").getAsJsonObject();
	
				if (jsonDataObject.isJsonNull() == false) {
		
					if (jsonDataObject.get("getReferralCount").isJsonNull() == false) {
		
						int referralCount = jsonDataObject.get("getReferralCount").isJsonNull() == false ? 
								jsonDataObject.get("getReferralCount").getAsInt() : 0;
						return referralCount;
					}else
						return 0;
				}else
					return 0;
			}else
				return 0;
		}else
			return 0;
	}
}