package com.java.floatcare.resolver.query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.model.UserDiploma;

@Component
public class UserDiplomaQuery implements GraphQLResolver<UserDiploma> {

	@Autowired
	private MongoTemplate mongoTemplate;
	
	public UserBasicInformation getUserInformation(UserDiploma userDiploma) {
		
		UserBasicInformation userBasicInformationObj = mongoTemplate.findOne(Query.query(Criteria.where("userId").is(userDiploma.getUserId())), UserBasicInformation.class);
		
		return (userBasicInformationObj != null) ? userBasicInformationObj : new UserBasicInformation();
	}
}
