package com.java.floatcare.resolver.query;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.java.floatcare.model.CredentialSubTypes;
import com.java.floatcare.model.CredentialTypes;
import com.java.floatcare.model.Organizations;
import com.java.floatcare.repository.CredentialTypesRepository;
import com.java.floatcare.repository.OrganizationRepository;

@Component
public class CredentialSubTypesQuery implements GraphQLResolver<CredentialSubTypes> {

	@Autowired
	private CredentialTypesRepository credentialTypeRepository;
	@Autowired
	private OrganizationRepository organizationRepository;
	
	public String getCredentialTypeName(CredentialSubTypes credentialSubTypes) {
		
		Optional<CredentialTypes> credentialTypes = credentialTypeRepository.findById(credentialSubTypes.getCredentialTypeId());

		if (credentialTypes.isPresent())
			return credentialTypes.get().getLabel();
		else
			return null;
	}
	
    public String getOrganizationName(CredentialSubTypes credentialSubTypes) {
		
		Optional<Organizations> organizations = organizationRepository.findById(credentialSubTypes.getOrganizationId());

		if (organizations.isPresent())
			return organizations.get().getName();
		else
			return null;
	}
}
