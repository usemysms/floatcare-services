package com.java.floatcare.resolver.query;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.java.floatcare.model.BusinessSubTypes;
import com.java.floatcare.model.BusinessTypes;
import com.java.floatcare.model.Businesses;
import com.java.floatcare.model.Department;
import com.java.floatcare.model.Organizations;
import com.java.floatcare.model.UserWorkspaces;
import com.java.floatcare.model.WorksiteSettings;
import com.java.floatcare.repository.BusinessSubTypesRepository;
import com.java.floatcare.repository.BusinessTypesRepository;
import com.java.floatcare.repository.BusinessesRepository;
import com.java.floatcare.repository.DepartmentRepository;
import com.java.floatcare.repository.OrganizationRepository;
import com.java.floatcare.repository.UserWorkspacesRepository;
import com.java.floatcare.repository.WorksiteSettingsRepository;

@Component
public class BusinessesQuery implements GraphQLResolver<Businesses> {

	@Autowired
	private DepartmentRepository departmentRepository;
	@Autowired
	private BusinessTypesRepository businessTypesRepository;
	@Autowired
	private BusinessSubTypesRepository businessSubTypesRepository;
	@Autowired
	private WorksiteSettingsRepository worksiteSettingsRepository;
	@Autowired
	private UserWorkspacesRepository userWorkspacesRepository;
	@Autowired
	private OrganizationRepository organizationRepository;
	@Autowired
	private BusinessesRepository businessesRepository;

	public List<Department> getDepartment(Businesses businesses) {

		return departmentRepository.findDepartmentByBusinessId(businesses.getBusinessId());
	}

	public long getNumberOfDepartment(Businesses businesses) {
		List<Department> department = departmentRepository.findDepartmentByBusinessId(businesses.getBusinessId());
		return department.size();
	}

	public String getBusinessType(Businesses businesses) {
		Optional<BusinessTypes> businessTypes = businessTypesRepository.findById(businesses.getBusinessTypeId());

		if (businessTypes.isPresent()) {
			return businessTypes.get().getLabel();
		}

		return null;
	}

	public String getBusinessSubType(Businesses businesses) {
		Optional<BusinessSubTypes> businessSubTypes = businessSubTypesRepository
				.findById(businesses.getBusinessSubTypeId());

		if (businessSubTypes.isPresent()) {
			return businessSubTypes.get().getLabel();
		}

		return null;
	}

	public long getBusinessTypeCount(Businesses businesses) {

		return businesses.countBusinessTypeId();
	}
	
	public String getWorksiteColor (Businesses businesses, long userId) {
		
		WorksiteSettings color = worksiteSettingsRepository.findByUserIdAndWorksiteId(userId, businesses.getBusinessId());
		
		if (color != null)
			return color.getColor();
		else
			return null;
	}
	
	public Iterable<Department> getDepartmentByUserId (Businesses businesses, long userId) {
		
		List<UserWorkspaces> workspaces = userWorkspacesRepository.findByUserIdAndBusinessId(userId, businesses.getBusinessId());
		
		List<Long> departmentIds = workspaces.stream().map(emitter -> emitter.getWorkspaceId()).collect(Collectors.toList());
		
		return departmentRepository.findAllById(departmentIds);
		
	}

	public Organizations getOrganizationDetails(Businesses business) {

		if (business.getOrganizationId() > 0) {
			Optional<Organizations> organization = organizationRepository.findById(business.getOrganizationId());
			return organization.isPresent() ? organization.get() : null;
		}else
			return null;
	}
	
	public List<Businesses> getPreferredSpecialitiesDetails(Businesses business) {

		if (business.getPreferredSpecialities() != null && business.getPreferredSpecialities().size() > 0) {
			List<Businesses> businesses = businessesRepository.findAllByBusinessId(business.getPreferredSpecialities());
			return businesses;
		}else
			return null;
	}
}
