package com.java.floatcare.resolver.query;

import java.time.LocalTime;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.java.floatcare.model.DepartmentShifts;
import com.java.floatcare.model.ScheduleLayoutQuota;
import com.java.floatcare.repository.DepartmentShiftsRepository;
import com.java.floatcare.repository.StaffUserCoreTypesRepository;

@Component
public class ScheduleLayoutQuotaQuery implements GraphQLResolver<ScheduleLayoutQuota> {

	@Autowired
	private StaffUserCoreTypesRepository staffUserCoreTypesRepository;
	@Autowired
	private DepartmentShiftsRepository departmentShiftsRepository;
	
	public String getStaffCoreTypeName (ScheduleLayoutQuota scheduleLayoutQuota) {
		
		return staffUserCoreTypesRepository.findStaffUserCoreTypesById(scheduleLayoutQuota.getStaffCoreTypeId()).getLabel();
	}
	
	
	public LocalTime getShiftStartTime (ScheduleLayoutQuota scheduleLayoutQuota) {
		
		Optional<DepartmentShifts> optDepartmentShifts = departmentShiftsRepository.findById(scheduleLayoutQuota.getShiftTypeId());
		
		if (optDepartmentShifts.isPresent()) 
			return optDepartmentShifts.get().getStartTime();
		else
			return null;		
	}
	
	public LocalTime getShiftEndTime (ScheduleLayoutQuota scheduleLayoutQuota) {
		
		Optional<DepartmentShifts> optDepartmentShifts = departmentShiftsRepository.findById(scheduleLayoutQuota.getShiftTypeId());
	
		if (optDepartmentShifts.isPresent()) 
			return optDepartmentShifts.get().getEndTime();
		else
			return null;		
	}
	
	public String getShiftTypeName (ScheduleLayoutQuota scheduleLayoutQuota) {
		
		Optional<DepartmentShifts> optDepartmentShifts = departmentShiftsRepository.findById(scheduleLayoutQuota.getShiftTypeId());
	
		if (optDepartmentShifts.isPresent()) {
			return optDepartmentShifts.get().getLabel();
		}
		else
			return null;
	}
	
}
