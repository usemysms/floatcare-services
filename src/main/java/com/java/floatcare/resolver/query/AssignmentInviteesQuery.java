package com.java.floatcare.resolver.query;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.java.floatcare.dao.ClientsDAO;
import com.java.floatcare.model.AssignmentInvitees;
import com.java.floatcare.model.Assignments;
import com.java.floatcare.model.Household;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.model.UserWorkspaces;
import com.java.floatcare.repository.UserBasicInformationRepository;
import static com.java.floatcare.utils.DistanceCalculate.*;

@Component
public class AssignmentInviteesQuery implements GraphQLResolver<AssignmentInvitees> {

	@Autowired
	private MongoTemplate mongoTemplate;
	@Autowired
	private ClientsDAO clientsDAO;
	@Autowired
	private UserBasicInformationRepository userDAO;

	public UserWorkspaces getInviteeDetails(AssignmentInvitees assignmentInvitee) {

		return mongoTemplate.findOne(Query.query(Criteria.where("userId").is(assignmentInvitee.getStaffId())), UserWorkspaces.class);
	}

	public String getDistance(AssignmentInvitees assignmentInvitees) {

		Assignments assignment = mongoTemplate.findOne(Query.query(Criteria.where("assignmentId").is(assignmentInvitees.getAssignmentId())), Assignments.class);

		Household householdInfo = clientsDAO.getHouseholdByClientId(assignment.getClientId());

		Optional<UserBasicInformation> userInfo = userDAO.findByUserIdAndStatus(assignmentInvitees.getStaffId(), "Active");

		if (userInfo.isPresent() && householdInfo != null) {
			if (userInfo.get().getLatitude() != null && userInfo.get().getLongitude() != null
					&& householdInfo.getLatitude() != null && householdInfo.getLongitude() != null) {

				double userLat = Double.parseDouble(userInfo.get().getLatitude());
				double userLon = Double.parseDouble(userInfo.get().getLongitude());
				double householdLat = Double.parseDouble(householdInfo.getLatitude());
				double householdLon = Double.parseDouble(householdInfo.getLongitude());

				double distance = distance(userLat, householdLat, userLon, householdLon);
				return distance + " mi"; // converting distance from miles
			} else
				return "-";
		} else
			return "-";
	}
}
