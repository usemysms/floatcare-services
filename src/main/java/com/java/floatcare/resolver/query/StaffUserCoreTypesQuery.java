package com.java.floatcare.resolver.query;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.java.floatcare.api.response.pojo.SetScheduleResponse;
import com.java.floatcare.dao.AssignmentSchedulesDAO;
import com.java.floatcare.dao.AssignmentsDAO;
import com.java.floatcare.dao.GeneralAvailabilityDAO;
import com.java.floatcare.dao.RequestsDAO;
import com.java.floatcare.dao.ScheduleDAO;
import com.java.floatcare.dao.SetScheduleLayoutDAO;
import com.java.floatcare.dao.UserBasicInformationRepositoryDAO;
import com.java.floatcare.dao.UserOrganizationDAO;
import com.java.floatcare.dao.UserWorkspacesDAO;
import com.java.floatcare.model.AssignmentSchedules;
import com.java.floatcare.model.Assignments;
import com.java.floatcare.model.Department;
import com.java.floatcare.model.DepartmentStaff;
import com.java.floatcare.model.Schedules;
import com.java.floatcare.model.StaffUserCoreTypes;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.repository.DepartmentRepository;
import com.java.floatcare.repository.DepartmentStaffRepository;
import com.java.floatcare.repository.UserBasicInformationRepository;
import com.java.floatcare.utils.ConstantUtils;

@Component
public class StaffUserCoreTypesQuery implements GraphQLResolver<StaffUserCoreTypes> {

	@Autowired
	private UserBasicInformationRepository userBasicInformationRepository;
	@Autowired
	private UserBasicInformationRepositoryDAO userBasicInformationRepositoryDAO;
	@Autowired
	private DepartmentRepository departmentRepository;
	@Autowired
	private DepartmentStaffRepository departmentStaffRepository;
	@Autowired
	private UserBasicInformationRepositoryDAO userDAO;
	@Autowired
	private GeneralAvailabilityDAO generalAvailabilityDAO;
	@Autowired
	private SetScheduleLayoutDAO setScheduleLayoutDAO;
	@Autowired
	private AssignmentsDAO assignmentDAO;
	@Autowired
	private AssignmentSchedulesDAO assignmentSchedulesDAO;
	@Autowired
	private UserWorkspacesDAO userWorkspaceDAO;
	@Autowired
	private UserOrganizationDAO userOrganizationDAO;
	@Autowired
	private RequestsDAO requestDAO;
	@Autowired
	private ScheduleDAO scheduleDAO;

	private long organizationId;
	private long departmentId;
	private String onDate;
	private long shiftId;
	private String filterBy;

	public long getOrganizationId() {
		return organizationId;
	}

	public long getDepartmentId() {
		return departmentId;
	}

	public String getOnDate() {
		return onDate;
	}

	public long getShiftId() {
		return shiftId;
	}

	public void setOrganizationId(long organizationId) {
		this.organizationId = organizationId;
	}

	public void setDepartmentId(long departmentId) {
		this.departmentId = departmentId;
	}

	public void setOnDate(String onDate) {
		this.onDate = onDate;
	}

	public void setShiftId(long shiftId) {
		this.shiftId = shiftId;
	}
	
	public String getFilterBy() {
		return filterBy;
	}

	public void setFilterBy(String filterBy) {
		this.filterBy = filterBy;
	}

	public Iterable<UserBasicInformation> getStaffMembers(StaffUserCoreTypes staffUserCoreTypes) {

		List<Long> userIds = userOrganizationDAO.findByOrganizationId(organizationId, staffUserCoreTypes.getStaffUserCoreTypesId());

		return userBasicInformationRepositoryDAO.findByUserId(userIds);
			
	}
	
	public List<UserBasicInformation> getFilteredUsers (StaffUserCoreTypes staffUserCoreTypes) {
		
		List<Long> userIds = new LinkedList<Long>();
		List<UserBasicInformation> userList = new ArrayList<>();
		
		LocalDate date = LocalDate.parse(onDate);
		
		switch (filterBy) {

		case "working": // filter by current day working members in a department and shift type

			if (shiftId > 0) {
				userIds = scheduleDAO.findUsersByShiftDateAndDepartmentIdAndShiftId(date, departmentId, shiftId);

				List<SetScheduleResponse> setScheduleLayouts = setScheduleLayoutDAO.findByDepartmentIdAndCoreTypeIdAndShiftId(departmentId, staffUserCoreTypes.getStaffUserCoreTypesId(), date.toString(), ConstantUtils.convertLocalDateToWeekDay(date), shiftId);
	
				List<Long> userIdsFromSetSchedules = setScheduleLayouts.size() > 0 ? setScheduleLayouts.stream().flatMap(e->e.getSchedules().stream()).map(Schedules::getUserId).distinct()
						.collect(Collectors.toList()) : null;
	
				if (userIdsFromSetSchedules != null && userIdsFromSetSchedules.size() > 0) {
					userIdsFromSetSchedules.removeAll(userIds); // remove the common records
					userIds.addAll(userIdsFromSetSchedules); // add the unique users
				}

			}else {
				//fetch schedules
				userIds = scheduleDAO.findUsersByShiftDateAndDepartmentId(date, departmentId);

				//add setSchedule
				List<SetScheduleResponse> setScheduleLayouts = setScheduleLayoutDAO.findByDepartmentIdAndCoreTypeId(departmentId, staffUserCoreTypes.getStaffUserCoreTypesId(), date.toString(), ConstantUtils.convertLocalDateToWeekDay(date));

				List<Long> userIdsFromSetSchedules = setScheduleLayouts.size() > 0 ? setScheduleLayouts.stream().flatMap(e->e.getSchedules().stream()).map(Schedules::getUserId).distinct()
						.collect(Collectors.toList()) : null;

				if (userIdsFromSetSchedules != null && userIdsFromSetSchedules.size() > 0) {
					userIdsFromSetSchedules.removeAll(userIds); // remove the common records
					userIds.addAll(userIdsFromSetSchedules); // add the unique users
				}

				//fetch worksiteid to add assignments of users.
				Department department = departmentRepository.findByDepartmentId(departmentId);
				long businessId = department != null ? department.getBusinessId() : 0;

				if (businessId > 0) {
					List<Assignments> assignment = assignmentDAO.findAssignmentByBusinessIdAndGivenDate(businessId, date);
	
					if (assignment.size() > 0) {

						List<Long> assignmentIds = assignment.stream().map(Assignments::getAssignmentId).collect(Collectors.toList());
						
						List<AssignmentSchedules> assignmentSchedules = assignmentSchedulesDAO.findAssignmentSchedulesByAssignmentIdsAndWeekDay(assignmentIds, ConstantUtils.convertLocalDateToWeekDay(date));
	
						List<Long> userIdsInAssignments = assignmentSchedules.stream().map(e->e.getStaff()).flatMap(Collection::stream).collect(Collectors.toList());
	
						userIdsInAssignments.removeAll(userIds);
						userIds.addAll(userIdsInAssignments);
					}
				}
			}

			if(userIds != null && userIds.size() > 0)
				userList = userDAO.findByUserIdAndStaffUserCoreTypeId(userIds, staffUserCoreTypes.getStaffUserCoreTypesId());

			break;

		case "available": // filter by current day general available members in a department and shift type
			if (shiftId > 0) {
				Set<Long> usersListBasedOnShift = userWorkspaceDAO.findByDepartmentIdAndShiftId(departmentId, shiftId);
				userIds = generalAvailabilityDAO.findGeneralAvailabilityOfUsersByUserIdsStartDate(usersListBasedOnShift, date);
			} else {
				Map<Long, Long> usersList = userWorkspaceDAO.findByDepartmentId(departmentId);
				Set<Long> userIdList = usersList.keySet().stream().collect(Collectors.toSet());
				userIds = generalAvailabilityDAO.findGeneralAvailabilityOfUsersByUserIdsStartDate(userIdList, date);
			}

			if(userIds != null && userIds.size() > 0)
				userList = userDAO.findByUserIdAndStaffUserCoreTypeId(userIds, staffUserCoreTypes.getStaffUserCoreTypesId());
			break;

		case "oncalloff": // filter by current day sick leave request members in a department and shift type	
			if (shiftId > 0) 
				userIds = requestDAO.findUserIdsByRequestIdAndShiftIdAndIsApproved(3, shiftId, date, departmentId, "Approved");
			else
				userIds = requestDAO.findUserIdsByRequestIdAndIsApproved(3, date, departmentId, "Approved");

			if(userIds != null && userIds.size() > 0)
				userList = userDAO.findByUserIdAndStaffUserCoreTypeId(userIds, staffUserCoreTypes.getStaffUserCoreTypesId());
			break;
			
		case "onvacation": // filter by current day holiday leave request members in a department and shift type
			if (shiftId > 0)
				userIds = requestDAO.findUserIdsByRequestIdAndShiftIdAndIsApproved(5, shiftId, date, departmentId, "Approved");
			else
				userIds = requestDAO.findUserIdsByRequestIdAndIsApproved(5, date, departmentId, "Approved");

			if(userIds != null && userIds.size() > 0)
				userList = userDAO.findByUserIdAndStaffUserCoreTypeId(userIds, staffUserCoreTypes.getStaffUserCoreTypesId());
			break;
		}
		return userList;
	}

	public Integer getNumberOfStaff(StaffUserCoreTypes staffUserCoreTypes) {

		return userBasicInformationRepository.findUsersByStaffUserCoreTypeId(staffUserCoreTypes.getStaffUserCoreTypesId()).size();
	}

	public List<Department> getDepartment(StaffUserCoreTypes staffUserCoreTypes) {

		List<DepartmentStaff> departmentStaff = departmentStaffRepository.findDepartmentStaffByStaffUserCoreTypeId(staffUserCoreTypes.getStaffUserCoreTypesId());

		List<Department> department = new ArrayList<Department>();

		for (DepartmentStaff list : departmentStaff) {
			department = departmentRepository.findDepartmentByDepartmentId(list.getDepartmentId());
		}
		return department;
	}
}
