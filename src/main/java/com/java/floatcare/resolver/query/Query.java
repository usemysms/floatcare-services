package com.java.floatcare.resolver.query;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TimeZone;
import java.util.stream.Collectors;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.java.floatcare.api.request.pojo.IsSeenByGroupMembers;
import com.java.floatcare.api.response.pojo.ActiveSchedulesAndRequests;
import com.java.floatcare.api.response.pojo.DepartmentShiftsResponse;
import com.java.floatcare.api.response.pojo.UnreadMessagesAndNotificationsCount;
import com.java.floatcare.dao.AssignmentInviteesDAO;
import com.java.floatcare.dao.AssignmentSchedulesDAO;
import com.java.floatcare.dao.AssignmentsDAO;
import com.java.floatcare.dao.AvailabilityDAO;
import com.java.floatcare.dao.BusinessesDAO;
import com.java.floatcare.dao.ClientsDAO;
import com.java.floatcare.dao.CredentialSubTypesDAO;
import com.java.floatcare.dao.DepartmentDAO;
import com.java.floatcare.dao.DepartmentShiftsDAO;
import com.java.floatcare.dao.FlexOffsDAO;
import com.java.floatcare.dao.GeneralAvailabilityDAO;
import com.java.floatcare.dao.GroupChatsDAO;
import com.java.floatcare.dao.MessagesDAO;
import com.java.floatcare.dao.OpenShiftInviteesDAO;
import com.java.floatcare.dao.OpenShiftsDAO;
import com.java.floatcare.dao.OrganizationDAO;
import com.java.floatcare.dao.ReferencesDAO;
import com.java.floatcare.dao.RequestsDAO;
import com.java.floatcare.dao.ScheduleDAO;
import com.java.floatcare.dao.ScheduleLayoutDAO;
import com.java.floatcare.dao.ScheduleLayoutGroupsDAO;
import com.java.floatcare.dao.SetScheduleLayoutDAO;
import com.java.floatcare.dao.StaffUserCoreTypesDAO;
import com.java.floatcare.dao.StaffWeekOffsDAO;
import com.java.floatcare.dao.UserBasicInformationRepositoryDAO;
import com.java.floatcare.dao.UserDiplomaDAO;
import com.java.floatcare.dao.UserWorkspacesDAO;
import com.java.floatcare.model.AssignmentInvitees;
import com.java.floatcare.model.AssignmentSchedules;
import com.java.floatcare.model.Assignments;
import com.java.floatcare.model.Availability;
import com.java.floatcare.model.BusinessSubTypes;
import com.java.floatcare.model.BusinessTypes;
import com.java.floatcare.model.Businesses;
import com.java.floatcare.model.Clients;
import com.java.floatcare.model.CredentialSubTypes;
import com.java.floatcare.model.DeletedMessages;
import com.java.floatcare.model.Department;
import com.java.floatcare.model.DepartmentSettings;
import com.java.floatcare.model.DepartmentShifts;
import com.java.floatcare.model.DepartmentTypes;
import com.java.floatcare.model.EhrSystems;
import com.java.floatcare.model.Features;
import com.java.floatcare.model.FlexOffs;
import com.java.floatcare.model.GeneralAvailability;
import com.java.floatcare.model.GroupChats;
import com.java.floatcare.model.Messages;
import com.java.floatcare.model.Notifications;
import com.java.floatcare.model.OpenShiftInvitees;
import com.java.floatcare.model.OpenShifts;
import com.java.floatcare.model.Organizations;
import com.java.floatcare.model.References;
import com.java.floatcare.model.Requests;
import com.java.floatcare.model.ScheduleLayout;
import com.java.floatcare.model.Schedules;
import com.java.floatcare.model.SetScheduleLayouts;
import com.java.floatcare.model.StaffUserCoreTypes;
import com.java.floatcare.model.SubRoleTypes;
import com.java.floatcare.model.SwapShiftRequests;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.model.UserCredentials;
import com.java.floatcare.model.UserDiploma;
import com.java.floatcare.model.UserEducation;
import com.java.floatcare.model.UserNotification;
import com.java.floatcare.model.UserOrganizations;
import com.java.floatcare.model.UserPermissions;
import com.java.floatcare.model.UserPrivacySettings;
import com.java.floatcare.model.UserWorkExperience;
import com.java.floatcare.model.UserWorkspaces;
import com.java.floatcare.model.WorksiteSettings;
import com.java.floatcare.model.WorkspaceSettings;
import com.java.floatcare.repository.BusinessSubTypesRepository;
import com.java.floatcare.repository.BusinessTypesRepository;
import com.java.floatcare.repository.BusinessesRepository;
import com.java.floatcare.repository.CredentialSubTypeRepository;
import com.java.floatcare.repository.DeletedMessagesRepository;
import com.java.floatcare.repository.DepartmentRepository;
import com.java.floatcare.repository.DepartmentSettingsRepository;
import com.java.floatcare.repository.DepartmentShiftsRepository;
import com.java.floatcare.repository.DepartmentTypesRepository;
import com.java.floatcare.repository.FeaturesRepository;
import com.java.floatcare.repository.FlexOffsRepository;
import com.java.floatcare.repository.MessagesRepository;
import com.java.floatcare.repository.NotificationsRepository;
import com.java.floatcare.repository.OpenShiftsRepository;
import com.java.floatcare.repository.OrganizationRepository;
import com.java.floatcare.repository.RequestsRepository;
import com.java.floatcare.repository.ScheduleLayoutRepository;
import com.java.floatcare.repository.SchedulesRepository;
import com.java.floatcare.repository.StaffUserCoreTypesRepository;
import com.java.floatcare.repository.SubRoleTypesRepository;
import com.java.floatcare.repository.SwapShiftRequestsRepository;
import com.java.floatcare.repository.UserBasicInformationRepository;
import com.java.floatcare.repository.UserCredentialsRepository;
import com.java.floatcare.repository.UserEducationRepository;
import com.java.floatcare.repository.UserNotificationRepository;
import com.java.floatcare.repository.UserOrganizationsRepository;
import com.java.floatcare.repository.UserPermissionsRepository;
import com.java.floatcare.repository.UserPrivacySettingsRepository;
import com.java.floatcare.repository.UserWorkExperienceRepository;
import com.java.floatcare.repository.UserWorkspacesRepository;
import com.java.floatcare.repository.WorksiteSettingsRepository;
import com.java.floatcare.repository.WorkspaceSettingsRepository;
import com.java.floatcare.service.MessageService;
import com.java.floatcare.service.UserBasicInformationService;
import com.java.floatcare.utils.ConstantUtils;
import com.java.floatcare.utils.InAppNotifications;
import com.java.floatcare.utils.MessageNotificationCountDAO;

@Component
public class Query implements GraphQLQueryResolver {

	@Autowired
	private DeletedMessagesRepository deletedMessagesRepository;
	@Autowired
	private OrganizationRepository organizationRepository;
	@Autowired
	private BusinessesRepository businessesRepository;
	@Autowired
	private UserBasicInformationRepository userRepository;
	@Autowired
	private UserWorkExperienceRepository userWorkExperienceRepository;
	@Autowired
	private UserCredentialsRepository userCredentialsRepository;
	@Autowired
	private UserEducationRepository userEducationRepository;
	@Autowired
	private DepartmentRepository departmentRepository;
	@Autowired
	private BusinessTypesRepository businessTypesRepository;
	@Autowired
	private BusinessSubTypesRepository businessSubTypesRepository;
	@Autowired
	private StaffUserCoreTypesRepository staffUserCoreTypesRepository;
	@Autowired
	private StaffUserCoreTypesDAO staffUserCoreTypesDAO;
	@Autowired
	private DepartmentTypesRepository departmentTypesRepository;
	@Autowired
	private UserNotificationRepository userNotificationRepository;
	@Autowired
	private UserPrivacySettingsRepository userPrivacySettingsRepository;
	@Autowired
	private UserWorkspacesRepository userWorkspacesRepository;
	@Autowired
	private StaffUserCoreTypesQuery staffUserCoreTypesQuery;
	@Autowired
	private SubRoleTypesRepository subRoleTypesRepository;
	@Autowired
	private UserBasicInformationQuery userBasicInformationQuery;
	@Autowired
	private WorkspaceSettingsRepository workspaceSettingsRepository;
	@Autowired
	private RequestsRepository requestsRepository;
	@Autowired
	private OpenShiftsRepository openShiftsRepository;
	@Autowired
	private NotificationsRepository notificationsRepository;
	@Autowired
	private SchedulesRepository schedulesRepository;
	@Autowired
	private DepartmentSettingsRepository departmentSettingsRepository;
	@Autowired
	private DepartmentShiftsRepository departmentShiftsRepository;
	@Autowired
	private ScheduleLayoutRepository scheduleLayoutRepository;
	@Autowired
	private FlexOffsRepository flexOffsRepository;
	@Autowired
	private WorksiteSettingsRepository worksiteSettingsRepository;
	@Autowired
	private CredentialSubTypeRepository credentialSubTypeRepository;
	@Autowired
	private SwapShiftRequestsRepository swapShiftRequestsRepository;
	@Autowired
	private MessagesRepository messagesRepository;
	@Autowired
	private UserBasicInformationRepositoryDAO userDAO;
	@Autowired
	private BusinessesDAO businessesDAO;
	@Autowired
	private DepartmentDAO departmentDAO;
	@Autowired
	private DepartmentShiftsDAO departmentShiftsDAO;
	@Autowired
	private ScheduleLayoutGroupsDAO scheduleLayoutGroupsDAO;
	@Autowired
	private AvailabilityDAO availabilityDAO;
	@Autowired
	private RequestsDAO requestsDAO;
	@Autowired
	private ScheduleDAO scheduleDAO;
	@Autowired
	private OpenShiftsDAO openShiftsDAO;
	@Autowired
	private UserWorkspacesDAO userWorkspacesDAO;
	@Autowired
	private OpenShiftInviteesDAO openShiftInviteesDAO;
	@Autowired
	private GeneralAvailabilityDAO generalAvailabilityDAO;
	@Autowired
	private MessagesDAO messagesDAO;
	@Autowired
	private FlexOffsDAO flexOffsDAO;
	@Autowired
	private SetScheduleLayoutDAO setScheduleLayoutDAO;
	@Autowired
	private ClientsDAO clientsDAO;
	@Autowired
	private AssignmentsDAO assignmentsDAO;
	@Autowired
	private AssignmentSchedulesDAO assignmentSchedulesDAO;
	@Autowired
	private AssignmentInviteesDAO assignmentsInviteesDAO;
	@Autowired
	private UserDiplomaDAO userDiplomaDAO;
	@Autowired
	private ReferencesDAO referencesDAO;
	@Autowired
	private ScheduleLayoutDAO scheduleLayoutDAO;
	@Autowired
	private StaffWeekOffsDAO staffWeekOffsDAO;
	@Autowired
	private GroupChatsDAO groupChatsDAO;
	@Autowired
	private MongoTemplate mongoTemplate;
	@Autowired
	private MessageNotificationCountDAO util;
	@Autowired
	private InAppNotifications inAppNotification;
	@Autowired
	private CredentialSubTypesDAO credentialSubTypesDAO;
	@Autowired
	private FeaturesRepository featuresRepository;
	@Autowired
	private UserPermissionsRepository userPermissionsRepository;
	@Autowired
	private UserOrganizationsRepository userOrganizationsRepository;
	@Autowired
	private UserBasicInformationService userBasicInformationService;
	@Autowired
	private MessageService messageService;
	@Autowired
	private OrganizationDAO organizationDAO;
	@Autowired
	private UserBasicInformationRepositoryDAO userBasicInformationDAO;

	// ***********************************************QUERY
	// ORGANIZATION**********************************************

	public Iterable<Organizations> findAllOrganizations() {
		return organizationRepository.findAll();
	}

	public List<Organizations> findAllOrganizationsByFirmType(String firmType) {
		return organizationDAO.findAllOrganizationsByFirmType(firmType);
	}

	public Iterable<Organizations> findOrganizationById(long organizationId) {
		return organizationRepository.findByOrganizationId(organizationId);
	}

	public Iterable<Organizations> findAllByOrganizationType(String organizationType) {
		return organizationRepository.findAllByOrganizationTypeNot(organizationType);
	}

	public long countOrganizations() {
		return organizationRepository.count();
	}

	public Iterable<Organizations> findOrganizationByName(String name) {

		return organizationRepository.findOrganizationsByName(name);
	}

	public Iterable<Organizations> findAllStaffGroupByRoles(long organizationId) {

		return organizationRepository.findByOrganizationId(organizationId);
	}

	// ********************************************QUERY
	// BUSINESSES***********************************************

	public Iterable<Businesses> findAllBusinesses() {
		return businessesRepository.findAll();
	}

	public long countBusinesses() {
		return businessesRepository.count();
	}

	public Iterable<Businesses> findBusinessById(long businessId) {
		return businessesRepository.findBusinessByBusinessId(businessId);
	}

	public Iterable<Businesses> findBusinessesByName(String businessName) {
		return businessesRepository.findBusinessesByName(businessName);
	}

	public List<UserWorkspaces> findAvailableUsersByWeekDays(List<String> weekDays) {

		return userWorkspacesDAO.findAvailableUsersByWeekdays(weekDays);
	}

	public List<Businesses> findBusinessesByOrganizationId(long organizationId) {

		return businessesRepository.findBusinessesByOrganizationId(organizationId);
	}

	public List<Businesses> findBusinessesByBusinessTypeId(long businessTypeId) {

		return businessesRepository.findBusinessesByBusinessTypeId(businessTypeId);
	}

	public List<Businesses> findBusinessesByBusinessTypeIdAndName(long businessTypeId, String name) {

		List<Businesses> business = businessesRepository.findBusinessesByBusinessTypeIdAndName(businessTypeId, "^"+name);
		return business;
	}
	
	public List<Businesses> searchFacilities(long businessTypeId, String name, String city, String state, String zip) {
		return businessesDAO.searchFacilities(businessTypeId, name, city, state, zip);
		
	}

	public List<UserBasicInformation> findByLanguageAndGenderAndSubRoleTypeIdAndUserIds(String language, String gender,
			long subRoleTypeId, String ethnicType, Set<Long> userIds) {

		return userDAO.findByLanguageAndGenderAndSubRoleTypeId(language, gender, subRoleTypeId, userIds, ethnicType);
	}

	// ********************************QUERY USER BASIC
	// INFORMATION*******************************************

	public Iterable<UserBasicInformation> findAllUsersBasicInformation() {
		return userRepository.findAll();
	}

	public Iterable<UserBasicInformation> findAllUsersByIds(List<Long> userIds) {
		return userRepository.findAllById(userIds);
	}

	public List<UserBasicInformation> findAllStaffMembers(String name, String email, String phone, String firmType,
			long organizationId) {
		return userBasicInformationService.findAllStaffMembers(name, email, phone, firmType, organizationId);
	}

	/*
	 * public List<UserBasicInformation> findAllStaffMembersByFirmType(String name,
	 * String email, String phone, String firmType) { return
	 * userBasicInformationService.findAllStaffMembersByFirmType(name, email, phone,
	 * firmType); }
	 */
	public long countUsers() {
		return userRepository.count();
	}

	public Iterable<Businesses> findAllBusinesses(long organizationId) {
		return businessesRepository.findBusinessesByOrganizationId(organizationId);
	}

	public UserBasicInformation findUsersById(long userId) {

		Optional<UserBasicInformation> userBasicInformation = userRepository.findById(userId);

		if (userBasicInformation.isPresent()) {
			return userBasicInformation.get();
		} else
			return null;
	}

	public List<UserBasicInformation> findUsersNotPartOfAnyOrganizations() {

		return userRepository.findUsersByOrganizationIdAndUserTypeIdNot(0, 1);
	}

	public List<UserBasicInformation> findAllPcpByNameAndEmailAndPhone(String name, String email, String phone,
			boolean isPcp) {

		if (name != null && phone != null && (email == null || email.length() == 0)) {

			String[] firstAndLastName = name.split(" ");

			try { // if there is a first and last name
				String firstName = firstAndLastName[0];
				String lastName = firstAndLastName[1];

				if (isPcp == false)
					return userRepository
							.findByFirstNameAndLastNameAndStaffUserCoreTypeIdAndPhoneAndStaffUserSubCoreTypeIdNot(
									firstName, lastName, 1, phone, 180);
				else
					return userRepository
							.findByFirstNameAndLastNameAndStaffUserCoreTypeIdAndPhoneAndStaffUserSubCoreTypeId(
									firstName, lastName, 1, phone, 180);

			} catch (Exception e) { // if only first name exists
				String firstName = firstAndLastName[0];

				if (isPcp == false)
					return userRepository.findByFirstNameAndStaffUserCoreTypeIdAndPhoneAndStaffUserCoreTypeIdNot(
							firstName, 1, phone, 180);
				else
					return userRepository.findByFirstNameAndStaffUserCoreTypeIdAndPhoneAndStaffUserCoreTypeId(firstName,
							1, phone, 180);
			}
		} else if (name != null && email != null && (phone == null || phone.length() == 0)) {

			String[] firstAndLastName = name.split(" ");

			try { // if there is a first and last name
				String firstName = firstAndLastName[0];
				String lastName = firstAndLastName[1];

				if (isPcp == false)
					return userRepository
							.findByFirstNameAndLastNameAndStaffUserCoreTypeIdAndEmailAndStaffUserSubCoreTypeIdNot(
									firstName, lastName, 1, email, 180);
				else
					return userRepository
							.findByFirstNameAndLastNameAndStaffUserCoreTypeIdAndEmailAndStaffUserSubCoreTypeId(
									firstName, lastName, 1, email, 180);

			} catch (Exception e) { // if only first name exists

				String firstName = firstAndLastName[0];

				if (isPcp == false)
					return userRepository.findByFirstNameAndStaffUserCoreTypeIdAndEmailAndStaffUserSubCoreTypeIdNot(
							firstName, 1, email, 180);
				else
					return userRepository.findByFirstNameAndStaffUserCoreTypeIdAndEmailAndStaffUserSubCoreTypeId(
							firstName, 1, email, 180);
			}

		} else if (email != null && phone != null && (name == null || name.length() == 0)) {

			return userRepository.findByPhoneAndStaffUserCoreTypeIdAndEmail(phone, 1, email);
		} else
			return null;
	}

	public List<UserBasicInformation> findByAddress1AndGenderAndStaffUserCoreTypeIdAndSubRoleTypeId(String location,
			String gender, long staffUserCoreTypeId, long subRoleTypeId) {

		return userDAO.findByAddress1AndGenderAndStaffUserCoreTypeIdAndSubRoleTypeId(location, gender,
				staffUserCoreTypeId, subRoleTypeId);
	}

	public List<UserBasicInformation> findByLanguageAndGenderAndStaffUserCoreTypeIdAndSubRoleTypeIdAndUserIds(
			String language, String gender, long staffUserCoreTypeId, long subRoleTypeId, String ethnicType,
			Set<Long> userIds) {

		return userDAO.findByLanguageAndGenderAndStaffUserCoreTypeIdAndSubRoleTypeId(language, gender,
				staffUserCoreTypeId, subRoleTypeId, userIds, ethnicType);
	}

	public List<UserBasicInformation> findByStaffUserCoreTypeIdAndStaffUserSubCoreTypeIdAndGender(
			long staffUserCoreTypeId, long subRoleTypeId, String gender) {
		return userRepository.findByStaffUserCoreTypeIdAndStaffUserSubCoreTypeIdAndGender(staffUserCoreTypeId,
				subRoleTypeId, gender);
	}

	public List<UserBasicInformation> findByStaffUserCoreTypeIdAndStaffUserSubCoreTypeIdAndLanguage(
			long staffUserCoreTypeId, long subRoleTypeId, String language) {
		return userRepository.findByStaffUserCoreTypeIdAndStaffUserSubCoreTypeIdAndLanguage(staffUserCoreTypeId,
				subRoleTypeId, language);
	}

	public List<UserBasicInformation> findByStaffUserCoreTypeIdAndStaffUserSubCoreTypeId(long staffUserCoreTypeId,
			long subRoleTypeId) {
		return userRepository.findByStaffUserCoreTypeIdAndStaffUserSubCoreTypeId(staffUserCoreTypeId, subRoleTypeId);
	}

	public List<UserBasicInformation> findByStaffUserCoreTypeId(long staffUserCoreTypeId) {
		return userRepository.findUsersByStaffUserCoreTypeId(staffUserCoreTypeId);
	}

	public UserBasicInformation getPrimaryCarePhysician(List<Long> userIds) {
		return userDAO.findPrimaryCarePhysicanByUserIdAndStaffUserCoreTypeId(userIds, 180);
	}

	public UserBasicInformation getOfficeAdminDetails(long organizationId) {

		return userRepository.findFirstByOrganizationIdAndStaffUserCoreTypeId(organizationId, 8);
	}

	public List<UserBasicInformation> getProviders(long organizationId) {

		List<UserBasicInformation> users = userRepository.findUsersByStaffUserCoreTypeIdAndOrganizationIdNot(1,
				organizationId);

		if (users != null) {
			return users;
		} else
			return null;
	}

	public List<UserBasicInformation> findByStaffUserCoreTypeIdAndGender(long staffUserCoreTypeId, String gender) {

		return userRepository.findByStaffUserCoreTypeIdAndGender(staffUserCoreTypeId, gender);
	}

	public List<UserBasicInformation> findByStaffUserCoreTypeIdAndLanguage(long staffUserCoreTypeId, String language) {

		return userRepository.findByStaffUserCoreTypeIdAndLanguage(staffUserCoreTypeId, language);
	}

	public List<UserBasicInformation> findByStaffUserCoreTypeIdAndEthnicity(long staffUserCoreTypeId,
			String ethnicity) {

		return userRepository.findByStaffUserCoreTypeIdAndEthnicType(staffUserCoreTypeId, ethnicity);
	}

	public List<UserWorkspaces> findProvidersByDistanceAndLanguageAndGenderAndRoleAndSubRole(String name,
			String startTime, String endTime, List<String> weekDays, double distance, String language, String gender,
			long staffUserCoreTypeId, long subRoleTypeId, List<String> specialityTags, String patientLongitude,
			String patientLatitude, String ethnicGroup, boolean isPreferred, String postalCode) {

		return userBasicInformationService.findProvidersAdvancedSearch(name, startTime, endTime, weekDays, distance,
				language, gender, staffUserCoreTypeId, subRoleTypeId, specialityTags, patientLongitude, patientLatitude,
				ethnicGroup, isPreferred, postalCode);
	}

	public Iterable<UserWorkspaces> getPreferredProviders(long businessId) {

		List<Businesses> businesses = businessesRepository.findAllBusinessesByBusinessIdNot(businessId);

		if (businesses != null && businesses.size() > 0) {

			Set<Long> preferredProviderIds = businesses.stream()
					.map(e -> e.getPreferredProviderIds().stream().collect(Collectors.toSet()))
					.flatMap(Collection::stream).collect(Collectors.toSet());
			return preferredProviderIds != null && preferredProviderIds.size() > 0
					? userWorkspacesRepository.findAllByUserId(preferredProviderIds)
					: null;
		} else
			return null;
	}

	public Iterable<UserBasicInformation> findAllPreferredProviders(List<Long> subCoreTypeId, Long businessId) {

		List<UserWorkspaces> userWorkSpaces = userWorkspacesRepository.findAllBySubCoreTypeId(subCoreTypeId,
				businessId);

		if (userWorkSpaces != null && userWorkSpaces.size() > 0) {

			List<Long> userIds = userWorkSpaces.stream().map(each -> each.getUserId()).distinct()
					.collect(Collectors.toList());
			return userIds != null && userIds.size() > 0 ? userBasicInformationDAO.findByUserId(userIds) : null;
		} else
			return null;
	}

	public Iterable<UserBasicInformation> findAllPartnerProviders(List<Long> subCoreTypeId) {

		List<UserWorkspaces> userWorkSpaces = userWorkspacesRepository.findAllBySubCoreTypeId(subCoreTypeId);

		if (userWorkSpaces != null && userWorkSpaces.size() > 0) {

			List<Long> userIds = userWorkSpaces.stream().map(each -> each.getUserId()).distinct()
					.collect(Collectors.toList());
			return userIds != null && userIds.size() > 0 ? userBasicInformationDAO.findByUserId(userIds) : null;
		} else
			return null;
	}

	public Iterable<UserBasicInformation> findAllUsersByName(String name) {

		String[] firstAndLastName = name.split(" ");

		try { // if there is a first and last name
			String firstName = firstAndLastName[0];
			String lastName = firstAndLastName[1];

			return userRepository.findAllByFirstNameAndLastNameAndStaffUserCoreTypeId(firstName, lastName,
					ConstantUtils.Physician);

		} catch (Exception e) { // if only first name exists

			String firstName = firstAndLastName[0];

			List<UserBasicInformation> users = userRepository.findAllByFirstNameAndStaffUserCoreTypeId(firstName,
					ConstantUtils.Physician);
			return users;
		}

	}

	public Iterable<UserBasicInformation> findAllUsersByOrganizationName(String organizationName) {

		List<Organizations> organizations = organizationRepository.findOrganizationsByName(organizationName);
		if (organizations != null && organizations.size() > 0) {

			List<Long> organizationIds = organizations.stream().map(Organizations::getOrganizationId)
					.collect(Collectors.toList());
			List<UserOrganizations> userOrganizations = userOrganizationsRepository
					.findAllUserOrganizationsByOrganizationId(organizationIds);
			if (userOrganizations != null && userOrganizations.size() > 0) {
				List<Long> userIds = userOrganizations.stream().map(each -> each.getUserId()).distinct()
						.collect(Collectors.toList());
				return userBasicInformationDAO.findByUserId(userIds).stream()
						.filter(e -> e.getStaffUserCoreTypeId() == ConstantUtils.Physician)
						.collect(Collectors.toList());

			} else {
				return null;
			}

		} else {
			return null;
		}

		/*
		 * try { //if there is a first and last name
		 * 
		 * List<Organizations> orgs =
		 * organizationDAO.getUsersFromOrganizationName(organizationName); return null;
		 * 
		 * }catch(Exception e) {
		 * 
		 * } return null;
		 */

	}

	public Iterable<UserBasicInformation> getOfficeAdminsByBusinessId(long businessId) {

		List<UserWorkspaces> userWorkspaces = userWorkspacesDAO.findByStaffUserCoreTypeIdAndBusinessId(8, businessId);

		if (userWorkspaces != null && userWorkspaces.size() > 0) {

			Set<Long> userIds = userWorkspaces.stream().map(e -> e.getUserId()).collect(Collectors.toSet());
			return userRepository.findAllById(userIds);
		} else
			return null;
	}

	// *************************************Query USER
	// WORKEXPERIENCE****************************************

	public Iterable<UserWorkExperience> findAllUserWorkExperienceByUserId(long userId) {

		Optional<UserBasicInformation> user = userRepository.findByUserIdAndStatus(userId, "Active");

		if (user.isPresent()) {
			List<UserWorkExperience> userWorkExperienceList = userWorkExperienceRepository
					.findByUserId(user.get().getUserId());

			if (userWorkExperienceList.size() > 0)
				return userWorkExperienceList;
			else
				return null;
		} else
			return null;
	}

	public UserWorkExperience findUserWorkExperienceByUserWorkEperienceId(long userWorkExperienceId) {

		Optional<UserWorkExperience> userWorkExperienceOptional = userWorkExperienceRepository
				.findById(userWorkExperienceId);

		if (userWorkExperienceOptional.isPresent())
			return userWorkExperienceOptional.get();
		else
			return null;
	}

	public long countUserWorkExperience() {
		return userWorkExperienceRepository.count();
	}

	// ******************************************Query USER
	// CREDENTIALS***************************************

	public Iterable<UserCredentials> findAllUserCredentialsByUserId(long userId) {

		Optional<UserBasicInformation> user = userRepository.findByUserIdAndStatus(userId, "Active");

		if (user.isPresent()) {
			List<UserCredentials> userCredentialsList = userCredentialsRepository.findByUserId(userId);

			if (userCredentialsList.size() > 0)
				return userCredentialsList;
			else
				return null;
		} else
			return null;
	}

	public long countUsersCredentials() {
		return userCredentialsRepository.count();
	}

	public List<UserCredentials> findAllUserCredentialsByCredentialSubTypeId(long credentialSubTypeId) {
		return userCredentialsRepository.findByCredentialSubTypeId(credentialSubTypeId);
	}

	// ******************************************Query CREDENTIALS SUB
	// TYPES***************************************

	public List<CredentialSubTypes> getCredentialSubTypesByCredentialTypeId(long credentialTypeId,
			long organizationId) {
		return credentialSubTypesDAO.getCredentialSubTypesByCredentialTypeIdAndOrganizationId(credentialTypeId,
				organizationId);
	}

	public List<CredentialSubTypes> getCredentialSubTypesByCredentialTypeAndCoreTypeIdAndOrganizationId(
			long credentialTypeId, Long staffCoreType, Long organizationId) {
		return credentialSubTypesDAO.getCredentialSubTypesByCredentialTypeIdAndCoreTypeIdAndOrganizationId(
				credentialTypeId, staffCoreType, organizationId);
	}

	public List<CredentialSubTypes> getCredentialSubTypesByOrganizationId(long organizationId) {

		return credentialSubTypesDAO.getCredentialSubTypesByOrganizationId(organizationId);
	}

	public CredentialSubTypes getCredentialSubTypesById(long credentialSubTypeId) {

		Optional<CredentialSubTypes> credentialSubTypesOptional = credentialSubTypeRepository
				.findCredentialSubTypesByCredentialSubTypeId(credentialSubTypeId);

		CredentialSubTypes credentialSubTypes;
		if (credentialSubTypesOptional.isPresent()) {
			credentialSubTypes = credentialSubTypesOptional.get();
			return credentialSubTypes;
		} else
			return credentialSubTypes = new CredentialSubTypes();
	}

	public List<CredentialSubTypes> getCredentialSubTypesByStatus() {
		return credentialSubTypesDAO.getCredentialSubTypesByStatus();
	}

	public List<CredentialSubTypes> findByStaffCoreType(long staffCoreTypeId) {
		return credentialSubTypeRepository.findByStaffCoreType(staffCoreTypeId);
	}

	public UserCredentials findUserCredentailById(long userCredentialId) {

		Optional<UserCredentials> userCredentialOptional = userCredentialsRepository.findById(userCredentialId);

		if (userCredentialOptional.isPresent())
			return userCredentialOptional.get();
		else
			return null;
	}

	// ****************************************Query USER
	// EDUCATION********************************************

	public Iterable<UserEducation> findAllUserEducationByUserId(long userId) {

		Optional<UserBasicInformation> user = userRepository.findByUserIdAndStatus(userId, "Active");

		if (user.isPresent()) {

			List<UserEducation> userEducationList = userEducationRepository.findByUserId(userId);

			if (userEducationList.size() > 0)
				return userEducationList;
			else
				return null;
		} else
			return null;
	}

	public UserEducation findUserEducationByUserEducationId(long userEducationId) {

		Optional<UserEducation> userEducationalOptional = userEducationRepository.findById(userEducationId);

		if (userEducationalOptional.isPresent())
			return userEducationalOptional.get();
		else
			return null;
	}

	public long countUserEducation() {
		return userEducationRepository.count();
	}

	// *******************************************Query
	// Department*********************************************

	public Iterable<Department> findAllDepartmentsByOrganizationId(long organizationId) {

		List<Long> businessIds = businessesDAO.getBusinessesIdByOrganizationId(organizationId);
		List<Department> departments = departmentDAO.getDepartmentByBusinessIds(businessIds);
		return departments;
	}

	public long countAllDepartments() {

		return departmentRepository.count();
	}

	public Iterable<Department> findDepartmentById(long departmentId) {

		return departmentRepository.findDepartmentByDepartmentId(departmentId);
	}

	public List<Department> findDepartmentsByBusinessId(List<Long> businessIds) {

		return departmentDAO.getDepartmentByBusinessIds(businessIds);
	}

	// *********************************************Query
	// BusinessTypes*******************************************

	public List<BusinessTypes> findAllBusinessTypes() {
		return businessTypesRepository.findAll();

	}

	// *********************************************Query Business
	// SubTypes****************************************

	public Iterable<BusinessSubTypes> findAllBusinessSubTypes() {
		return businessSubTypesRepository.findAll();
	}

	// **********************************************Query Staff User Core
	// Types*************************************

	public Iterable<StaffUserCoreTypes> findAllStaffUserCoreTypes() {
		return staffUserCoreTypesRepository.findAll();
	}

	public Iterable<StaffUserCoreTypes> findAllStaffMembersByOrganizationAndCoreType(long organizationId) {

		staffUserCoreTypesQuery.setOrganizationId(organizationId);
		return staffUserCoreTypesRepository.findAll();
	}

	public Iterable<StaffUserCoreTypes> findAllStaffMembersByDepartmentsAndFilterBy(long departmentId, String onDate,
			long shiftId, String filterBy) {

		if (departmentId > 0) {
			staffUserCoreTypesQuery.setDepartmentId(departmentId);
			userBasicInformationQuery.setDepartmentId(departmentId);
		}
		if (onDate != null) {
			userBasicInformationQuery.setDate(onDate);
			staffUserCoreTypesQuery.setOnDate(onDate);
		} else
			staffUserCoreTypesQuery.setOnDate("");
		if (shiftId > 0)
			staffUserCoreTypesQuery.setShiftId(shiftId);
		else
			staffUserCoreTypesQuery.setShiftId(0);
		if (filterBy != null)
			staffUserCoreTypesQuery.setFilterBy(filterBy);
		else
			staffUserCoreTypesQuery.setFilterBy("");

		if (departmentId > 0) {
			List<Long> staffCoreTypeIds = userWorkspacesDAO.findStaffCoreTypeIdsByWorkspaceId(departmentId);
			return staffUserCoreTypesRepository.findAllById(staffCoreTypeIds);
		} else {
			return staffUserCoreTypesRepository.findAll();
		}
	}

	// ***********************************************Query
	// SubCoreTypes*******************************************

	public List<SubRoleTypes> findSubRoleTypesByStaffUserCoreTypeId(long staffUserCoreTypeId) {

		return subRoleTypesRepository.findByParentId(staffUserCoreTypeId);
	}

	public List<SubRoleTypes> findAllSpecialityTags() {
		return subRoleTypesRepository.findAllSpecialistByParentId(1);
	}

	// ***********************************************Query
	// DepartmentTypes****************************************

	public Iterable<DepartmentTypes> findAllDepartmentTypes() {
		return departmentTypesRepository.findAll();
	}

	// *****************************************Query User
	// Notifications*******************************************

	public List<UserNotification> findUserNotificationByUserId(long userId) {

		Optional<UserBasicInformation> user = userRepository.findByUserIdAndStatus(userId, "Active");

		if (user.isPresent())
			return userNotificationRepository.findByUserId(userId);
		else
			return null;
	}

	// *****************************************Query User Privacy
	// Setting*****************************************

	public List<UserPrivacySettings> findUserPrivacySettingsByUserId(long userId) {

		Optional<UserBasicInformation> user = userRepository.findByUserIdAndStatus(userId, "Active");

		if (user.isPresent())
			return userPrivacySettingsRepository.findByUserId(userId);
		else
			return null;
	}

	// ********************************************Query User
	// Workspaces*********************************************

	public Iterable<UserWorkspaces> getWorkspaceByUserId(long userId) {

		List<UserWorkspaces> userWorkspaces = userWorkspacesRepository.findUserWorkspacesByUserIdAndStatus(userId,
				"Active");

		if (userWorkspaces.size() > 0)
			return userWorkspaces;
		else
			return null;
	}

	public List<UserBasicInformation> getProviderListingByOrganizationId(long organizationId) {

		List<UserWorkspaces> userWorkspaces = userWorkspacesDAO.findUserIdsByOrganizationId(organizationId);
		return (List<UserBasicInformation>) userRepository
				.findAllById(userWorkspaces.stream().map(e -> e.getUserId()).collect(Collectors.toList()));
	}

	public boolean canAddUserShiftType(long userId, long departmentId, long departmentShiftId) {

		List<DepartmentShiftsResponse> userWorkspaces = userWorkspacesDAO.findByUserIdAndStatusAndIsJoined(userId,
				"Active", true, departmentShiftId);

		List<DepartmentShifts> departmentShifts = Lists.newArrayList(Iterables
				.concat(userWorkspaces.stream().map(e -> e.getDepartmentShifts()).collect(Collectors.toList())));

		Optional<DepartmentShifts> checkDepartmentShift = departmentShiftsRepository.findById(departmentShiftId);

		if (checkDepartmentShift.isPresent()) {
			for (DepartmentShifts each : departmentShifts) {

				if (checkDepartmentShift.get().getStartTime().equals(each.getStartTime())
						&& checkDepartmentShift.get().getEndTime().equals(each.getEndTime()))
					return false;
			}
		}
		return true;
	}

	public ActiveSchedulesAndRequests validateUserDepartmentShiftId(long userId, long departmentShiftId) {

		List<Schedules> schedulesAndShiftPlanning = scheduleDAO.findByUserIdAndShiftTypeIdOrDepartmentId(userId,
				departmentShiftId, 0);

		List<ScheduleLayout> scheduleLayout = scheduleLayoutGroupsDAO
				.findScheduleLayoutOfUserByDepartmentShiftId(userId, departmentShiftId);

		ActiveSchedulesAndRequests activeSchedulesAndRequests = new ActiveSchedulesAndRequests();

		activeSchedulesAndRequests.setActiveSchedules(schedulesAndShiftPlanning);
		activeSchedulesAndRequests.setActiveScheduleLayouts(scheduleLayout);
		activeSchedulesAndRequests.setActiveRequests(
				requestsDAO.findByUserIdAndShiftTypeIdOrDepartmentId(userId, departmentShiftId, Long.valueOf(0)));

		return activeSchedulesAndRequests;
	}

	public ActiveSchedulesAndRequests validateUserDepartment(long userId, long departmentId) {

		List<Schedules> schedulesAndShiftPlanning = scheduleDAO.findByUserIdAndShiftTypeIdOrDepartmentId(userId, 0,
				departmentId); // fetch the schedules

		List<ScheduleLayout> scheduleLayout = scheduleLayoutGroupsDAO.findScheduleLayoutOfUserByDepartmentId(userId,
				departmentId);

		ActiveSchedulesAndRequests activeSchedulesAndRequests = new ActiveSchedulesAndRequests();

		activeSchedulesAndRequests.setActiveSchedules(schedulesAndShiftPlanning);
		activeSchedulesAndRequests.setActiveScheduleLayouts(scheduleLayout);
		activeSchedulesAndRequests.setActiveRequests(
				requestsDAO.findByUserIdAndShiftTypeIdOrDepartmentId(userId, Long.valueOf(0), departmentId));

		return activeSchedulesAndRequests;
	}

	public UserWorkspaces getWorkspaceById(long userWorkspaceId) {

		Optional<UserWorkspaces> userWorkspacesOptional = userWorkspacesRepository.findById(userWorkspaceId);

		if (userWorkspacesOptional.isPresent())
			return userWorkspacesOptional.get();
		else
			return null;
	}

	public List<DepartmentShifts> getShiftDetailsByInviteCode(String inviteCode) {

		List<UserWorkspaces> userWorkspace = userWorkspacesRepository.findByInvitationCode(inviteCode);

		if (userWorkspace.size() > 0) {

			List<DepartmentShifts> departmentShifts = new ArrayList<>();

			long userId = userWorkspace.get(0).getUserId();

			List<UserWorkspaces> userWorkspacesList = userWorkspacesRepository.findUserWorkspacesByUserId(userId);

			for (UserWorkspaces eachWorkspace : userWorkspacesList) {

				Iterable<DepartmentShifts> departmentShift = departmentShiftsRepository
						.findAllById(eachWorkspace.getDepartmentShiftIds());
				departmentShift.forEach(departmentShifts::add);
			}
			return departmentShifts;
		} else
			return null;
	}

	public List<UserBasicInformation> getUsersByWorksiteId(long worksiteId) {

		return (List<UserBasicInformation>) userRepository
				.findAllById(userWorkspacesDAO.findByBusinessIdAndStaffUserCoreTypeId(worksiteId, 1));
	}

	// *************************************************Query Workspace
	// Settings**************************************

	public Iterable<WorkspaceSettings> getWorkspaceSettingsByUserId(long userId) {

		Optional<UserBasicInformation> user = userRepository.findByUserIdAndStatus(userId, "Active");

		if (user.isPresent()) {
			Iterable<WorkspaceSettings> workspaceSettingsOptional = workspaceSettingsRepository
					.findWorkspaceSettingsByUserId(userId);

			if (workspaceSettingsOptional != null)
				return workspaceSettingsOptional;
			else
				return null;
		} else
			return null;
	}

	// *************************************************Query
	// Requests************************************************

	public Requests getARequestById(long requestId) {

		Optional<Requests> requests = requestsRepository.findById(requestId);

		if (requests.isPresent())
			return requests.get();
		else
			return null;
	}

	public List<Requests> getAllRequestsByUserId(long userId, long requestTypeId) {

		Optional<UserBasicInformation> user = userRepository.findByUserIdAndStatus(userId, "Active");

		if (user.isPresent()) {

			if (requestTypeId > 0) {

				List<Requests> requestsList = requestsDAO.findRequestsByUserIdAndRequestTypeId(userId, requestTypeId);
				return requestsList;
			} else {
				List<Requests> requests = requestsDAO.findRequestsByUserId(userId);

				if (requests.size() > 0)
					return requests;
				else
					return null;
			}
		} else
			return null;
	}

	public List<Requests> getRequestsByTypeAndWorkspace(List<Long> requestTypeId, long workspaceId) {

		List<Requests> userRequests = requestsDAO.findRequestsByTypeIdAndDepartmentId(requestTypeId, workspaceId);

		if (userRequests.size() > 0) {
			List<Requests> filteredUserRequests = userRequests.stream().filter(e -> e.getOnDate().stream()
					.filter(e1 -> e1.isBefore(LocalDate.now())).collect(Collectors.toList()).size() > 0)
					.collect(Collectors.toList());

			if (filteredUserRequests.size() > 0) {

				requestsRepository.deleteAll(filteredUserRequests);
				userRequests.removeAll(filteredUserRequests);
				List<Long> setScheduleLayoutIds = filteredUserRequests.stream().map(e -> e.getRequestId())
						.collect(Collectors.toList());
				staffWeekOffsDAO.removeBySetScheduleLayoutIds(setScheduleLayoutIds);
				inAppNotification.deleteBySourceId(setScheduleLayoutIds);
			}
			return userRequests;
		} else
			return userRequests;
	}

	// ************************************************ Query
	// Schedules*********************************************

	public List<Schedules> getListOfSchedulesByUserId(long userId) {

		Optional<UserBasicInformation> user = userRepository.findByUserIdAndStatus(userId, "Active");

		if (user.isPresent()) {

			List<Schedules> schedules = schedulesRepository.findByUserId(userId);
			if (schedules != null)
				return schedules;
			else
				return null;
		} else
			return null;
	}

	public List<Schedules> getScheduleByUserIdAndSelectedDay(long userId, long departmentId, String selectedDate) {

		Optional<UserBasicInformation> user = userRepository.findByUserIdAndStatus(userId, "Active");

		if (user.isPresent()) {

			if (departmentId > 0) {

				Schedules schedule = scheduleDAO.findScheduleByUserIdAndShiftDateAndDepartmentId(userId,
						LocalDate.parse(selectedDate), departmentId);

				if (schedule != null) {

					List<Schedules> scheduleList = new ArrayList<>();
					scheduleList.add(schedule);
					return scheduleList;
				} else
					return null;
			} else {
				List<Schedules> schedule = scheduleDAO.findScheduleUsersByUserIdAndOnDate(userId,
						LocalDate.parse(selectedDate));

				if (schedule != null)
					return schedule;
				else
					return null;
			}
		} else
			return null;
	}

	public List<Schedules> findSchedulesByDepartmentIdAndUserIdAndSelectedDate(long departmentId, long userId,
			String date) {

		UserWorkspaces userWorkspaces = userWorkspacesDAO.findByDepartmentIdAndUserId(departmentId, userId);

		if (userWorkspaces != null) {

			List<Long> userIds = userWorkspacesDAO.findByDepartmentIdAndStaffCoreTypeIdAndStatusAndIsJoined(
					departmentId, userWorkspaces.getStaffUserCoreTypeId(), "Active", true);

			if (userIds.contains(userId))
				userIds.remove(userId);

			List<Schedules> scheduleList = scheduleDAO.findSchedulesByUserIdsAndDepartmentIdAndOnDate(userIds,
					departmentId, LocalDate.parse(date));

			return scheduleList;
		} else
			return null;
	}

	public List<Schedules> verifyConflicts(long userId, long departmentId, List<String> onDate) throws ParseException {

		UserWorkspaces userWorkspace = userWorkspacesRepository.findByUserIdAndWorkspaceIdAndStatusAndIsJoined(userId,
				departmentId, "Active", true);

		List<LocalDate> localOnDate = onDate.stream().map(e -> LocalDate.parse(e)).collect(Collectors.toList());

		List<Schedules> conflictSchedules = new ArrayList<>();

		if (userWorkspace != null) {

			List<DepartmentShifts> departmentShifts = (List<DepartmentShifts>) departmentShiftsRepository
					.findAllById(userWorkspace.getDepartmentShiftIds());

			List<Schedules> assignedShifts = scheduleDAO.findScheduleUsersByUserIdAndOnDates(userId, localOnDate);

			DateFormat timeFormatter = new SimpleDateFormat("HH:mm");
			timeFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));

			for (Schedules eachSchedule : assignedShifts) {

				Optional<DepartmentShifts> departmentShift = departmentShifts.stream()
						.filter(e -> e.getDepartmentShiftId() == eachSchedule.getShiftTypeId()).findAny();

				Date timeFrom = timeFormatter.parse(departmentShift.get().getStartTime().minusSeconds(1).toString()); // formatting
																														// department
																														// Shift
																														// start
																														// time
																														// to
																														// epoch
																														// time
				Date timeTo = timeFormatter.parse(departmentShift.get().getEndTime().minusSeconds(1).toString()); // formatting
																													// department
																													// Shift
																													// endtime
																													// to
																													// epoch
																													// time

				Date scheduleStartTime = timeFormatter.parse(eachSchedule.getStartTime().minusSeconds(1).toString());
				Date scheduleEndTime = timeFormatter.parse(eachSchedule.getEndTime().minusSeconds(1).toString());

				long departmentShiftTimeTo = timeTo.toInstant().getEpochSecond(); // converting departmentShift endTime
																					// to epoch seconds
				long departmentShiftTimeFrom = timeFrom.toInstant().getEpochSecond(); // converting departmentShift
																						// startTime to epoch seconds
				long secondsinScheduleStartTime = scheduleStartTime.toInstant().getEpochSecond(); // converting schedule
																									// startTime to
																									// epoch seconds
				long secondsinScheduleEndTime = scheduleEndTime.toInstant().getEpochSecond(); // converting schedule
																								// endTime to epoch
																								// seconds

				if (departmentShift.get().getStartTime().isAfter(departmentShift.get().getEndTime())) {

					departmentShiftTimeTo = departmentShiftTimeTo + (24 * 3600);
				}
				Optional<DepartmentShifts> shiftType = departmentShiftsRepository
						.findById(eachSchedule.getShiftTypeId());

				if (shiftType.isPresent() && shiftType.get().getStartTime().isAfter(shiftType.get().getEndTime())) {

					secondsinScheduleEndTime = secondsinScheduleEndTime + (24 * 3600);
				}

				// System.out.println(secondsDateFrom+"-"+secondsDateTo);
				// System.out.println(secondsinScheduleStartTime+"-"+secondsinScheduleEndTime);

				if (((departmentShiftTimeFrom <= secondsinScheduleStartTime)
						&& (departmentShiftTimeTo >= secondsinScheduleStartTime))
						|| (departmentShiftTimeFrom <= secondsinScheduleEndTime)
								&& (departmentShiftTimeTo >= secondsinScheduleEndTime)) {
					// System.out.println("conflict 1");
					conflictSchedules.add(eachSchedule);

				} else if (Math.abs((timeFrom.getTime() - scheduleEndTime.getTime()) / 3600000) < 0.5
						|| Math.abs((timeTo.getTime() - scheduleStartTime.getTime()) / 3600000) < 0.5) {

					// System.out.println("conflict 2");
					conflictSchedules.add(eachSchedule);
				}
			}
		}
		return conflictSchedules;
	}

	// ************************************************Query Open
	// Shifts**********************************************

	public List<OpenShifts> getOpenShiftDetailsByWorksiteId(long worksiteId, String selectedDate) {

		if (selectedDate != null) {
			List<OpenShifts> openShiftList = openShiftsDAO.findByWorksiteIdAndFromSelectedDate(worksiteId,
					LocalDate.parse(selectedDate));
			return openShiftList;
		} else
			return openShiftsRepository.findByWorksiteId(worksiteId);
	}

	public List<OpenShifts> getOpenShiftDetailsByWorkspaceId(long workspaceId, String selectedDate) {

		if (selectedDate != null && selectedDate.length() > 0) {
			List<OpenShifts> openShiftList = openShiftsDAO.findByWorkspaceIdAndFromSelectedDate(workspaceId,
					LocalDate.parse(selectedDate));
			return openShiftList;
		} else {
			List<OpenShifts> openShifts = openShiftsRepository.findByDepartmentId(workspaceId);

			if (openShifts.size() > 0) {
				List<OpenShifts> filteredOpenShifts = openShifts.stream()
						.filter(e -> e.getOnDate().isBefore(LocalDate.now())).collect(Collectors.toList());

				if (filteredOpenShifts.size() > 0) {
					openShiftsRepository.deleteAll(filteredOpenShifts);
					openShifts.removeAll(filteredOpenShifts);
					List<Long> openShiftIds = filteredOpenShifts.stream().map(e -> e.getOpenShiftId())
							.collect(Collectors.toList());
					openShiftInviteesDAO.removeByOpenShiftIds(openShiftIds);
					inAppNotification.deleteBySourceId(openShiftIds);
				}
				return openShifts;
			} else
				return openShifts;
		}
	}

	public List<OpenShifts> getOpenShiftDetailsByOpenShiftId(long openShiftId) {

		return openShiftsRepository.findByOpenShiftId(openShiftId);
	}

	public List<UserWorkspaces> getStaffByWorksiteAndAvailableToWork(String startDate, long workspaceId,
			long staffUserCoreTypeId) {

		LocalDate givenDate = LocalDate.parse(startDate);

		List<UserWorkspaces> userWorkspaces = userWorkspacesDAO
				.findByDepartmentIdAndStaffUserCoreTypeIdAndIsJoined(workspaceId, staffUserCoreTypeId, true);
		Set<Long> userIds = userWorkspaces.stream().map(e -> e.getUserId()).collect(Collectors.toSet()); // fetch
																											// userIds

		List<UserWorkspaces> availableUsers = new ArrayList<UserWorkspaces>(); // collect the users who are available to
																				// work

		List<Long> availabilityList = generalAvailabilityDAO.findGeneralAvailabilityOfUsersByUserIdsStartDate(userIds,
				givenDate);

		for (Long eachUser : availabilityList) {

			List<Requests> request = requestsDAO.findByUserIdAndOnDateAndDepartmentId(eachUser,
					LocalDate.parse(startDate), workspaceId);

			if (request.size() > 0) {
				if (request.get(0).getStatus().equalsIgnoreCase("Approved"))
					continue;

				else
					availableUsers.add(userWorkspacesRepository.findByUserIdAndWorkspaceIdAndIsJoined(eachUser,
							workspaceId, true));
			}
		}
		return availableUsers;
	}

	public List<UserWorkspaces> getStaffByWorksiteAndNotAvailableToWork(String startDate, long workspaceId,
			long staffUserCoreTypeId) {

		LocalDate givenDate = LocalDate.parse(startDate);

		List<UserWorkspaces> userWorkspaces = userWorkspacesDAO
				.findByDepartmentIdAndStaffUserCoreTypeIdAndIsJoined(workspaceId, staffUserCoreTypeId, true);
		Set<Long> userIds = userWorkspaces.stream().map(e -> e.getUserId()).collect(Collectors.toSet());

		List<UserWorkspaces> notAvailableUsers = new ArrayList<UserWorkspaces>();

		List<Long> availabilityList = generalAvailabilityDAO.findGeneralAvailabilityOfUsersByUserIdsStartDate(userIds,
				givenDate);

		List<Long> usersScheduleList = scheduleDAO.findUserSchedulesByShiftDateAndUserIds(givenDate, userIds);

		for (Long eachUser : userIds) {

			List<Requests> request = requestsDAO.findByUserIdAndOnDateAndDepartmentId(eachUser, givenDate, workspaceId);

			if (availabilityList.contains(eachUser)) {
				continue;
			} else {

				if (request.size() > 0 && request.get(0).getStatus().equals("Approved")) {
					continue;
				}

				else if (usersScheduleList.size() > 0 && usersScheduleList.contains(eachUser)) {
					continue;
				}

				else {
					UserWorkspaces user = userWorkspacesRepository.findByUserIdAndWorkspaceIdAndIsJoined(eachUser,
							workspaceId, true);
					if (user != null)
						notAvailableUsers.add(user);
				}
			}
		}
		return notAvailableUsers;
	}

	public List<OpenShifts> getOpenShiftsByUserId(long userId) {

		List<OpenShiftInvitees> inviteesList = openShiftInviteesDAO.findOpenShiftInviteesByUserId(userId);
		return openShiftsDAO.findByOpenShiftId(inviteesList);
	}

	// ************************************************Query
	// Notification********************************************

	public List<Notifications> getNotificationsByUserId(long userId) {

		Optional<UserBasicInformation> user = userRepository.findByUserIdAndStatus(userId, "Active");

		if (user.isPresent()) {

			List<Notifications> notification = notificationsRepository.findByUserId(userId);

			if (notification != null && notification.size() > 0) {

				for (Notifications each : new ArrayList<>(notification)) {

					if (ChronoUnit.DAYS.between(each.getOnDate(), LocalDate.now()) <= 90) {
						continue;
					} else {
						notification.remove(each);
						notificationsRepository.delete(each);
					}
				}
				return notification;
			} else
				return null;
		} else
			return null;
	}

	// ************************************************Query Department
	// Settings*************************************

	public DepartmentSettings getDepartmentSettingsByDepartmentId(long departmentId) {

		return departmentSettingsRepository.findByDepartmentId(departmentId);
	}

	// ************************************************Query Department
	// Shifts***************************************

	public List<DepartmentShifts> getShiftTypesByDepartmentId(long departmentId) {

		return departmentShiftsRepository.findByDepartmentIdAndStatus(departmentId, "Active");
	}

	// Fetch department shifts by staff core type
	public List<DepartmentShifts> getShiftTypesByDepartmentIdAndStaffCoreType(long departmentId, long staffCoreTypeId) {

		return departmentShiftsDAO.findByDepartmentIdAndStaffCoreTypeIdsAndStatus(departmentId, staffCoreTypeId,
				"Active");
	}

	// ************************************************Query
	// ScheduleLayout*********************************************

	public List<ScheduleLayout> findScheduleLayoutByDepartmentId(long departmentId) {

		List<ScheduleLayout> scheduleLayouts = scheduleLayoutRepository.findByDepartmentId(departmentId);

		if (scheduleLayouts.size() > 0) {

			List<ScheduleLayout> filteredScheduleLayout = scheduleLayouts.stream()
					.filter(e -> e.getScheduleEndDate().isBeforeNow()).collect(Collectors.toList());

			if (filteredScheduleLayout.size() > 0) {

				scheduleLayoutRepository.deleteAll(filteredScheduleLayout);
				scheduleLayouts.removeAll(filteredScheduleLayout);
				List<Long> scheduleLayoutIds = filteredScheduleLayout.stream().map(e -> e.getScheduleLayoutId())
						.collect(Collectors.toList());
				scheduleLayoutGroupsDAO.removeByScheduleLayoutIds(scheduleLayoutIds);
				inAppNotification.deleteBySourceId(scheduleLayoutIds);
			}
		}
		return scheduleLayouts;
	}

	public ScheduleLayout findScheduleLayoutById(long id) {

		Optional<ScheduleLayout> scheduleLayoutOptional = scheduleLayoutRepository.findById(id);

		if (scheduleLayoutOptional.isPresent())
			return scheduleLayoutOptional.get();
		else
			return null;
	}

	public List<ScheduleLayout> findScheduleLayoutByUserId(long userId) {

		Optional<UserBasicInformation> user = userRepository.findByUserIdAndStatus(userId, "Active");

		if (user.isPresent()) {

			List<ScheduleLayout> scheduleLayoutResponse = scheduleLayoutDAO.findByUserIdAndCurrentDate(userId,
					DateTime.now());
			return scheduleLayoutResponse;
		} else
			return null;
	}

	public List<ScheduleLayout> checkDuplicateScheduleLayout(String scheduleStartDate, List<Long> staffCoreTypes,
			List<Long> shiftTypes, long departmentId) {

		List<ScheduleLayout> scheduleLayoutRolesAndShiftTypes = scheduleLayoutDAO
				.findAvailableScheduleLayoutForRolesAndShiftTypes(staffCoreTypes, shiftTypes,
						DateTime.parse(scheduleStartDate), departmentId); // check whether given staff core types is
																			// already added or not

		if (scheduleLayoutRolesAndShiftTypes != null && scheduleLayoutRolesAndShiftTypes.size() > 0)
			return scheduleLayoutRolesAndShiftTypes;

		else
			return new ArrayList<>();
	}

	// ************************************************Query
	// Flex-Off************************************************

	public FlexOffs getFlexOffByFlexOffId(long flexOffId) {

		Optional<FlexOffs> flexOffOptional = flexOffsRepository.findById(flexOffId);

		if (flexOffOptional.isPresent())
			return flexOffOptional.get();
		else
			return null;
	}

	public List<FlexOffs> getFlexOffByWorkspaceIdAndSelectedDate(long workspaceId, String selectedDate) {

		if (selectedDate != null) {
			List<FlexOffs> flexOffsList = flexOffsDAO.findByWorksiteIdAndFromSelectedDate(workspaceId,
					LocalDate.parse(selectedDate));
			return flexOffsList;
		} else {

			List<FlexOffs> flexOffs = flexOffsRepository.findByDepartmentId(workspaceId);
			List<FlexOffs> filteredFlexOffs = flexOffs.stream().filter(e -> e.getOnDate().isBefore(LocalDate.now()))
					.collect(Collectors.toList());

			if (filteredFlexOffs.size() > 0) {

				flexOffsRepository.deleteAll(filteredFlexOffs);
				flexOffs.removeAll(filteredFlexOffs);
				List<Long> flexOffIds = filteredFlexOffs.stream().map(e -> e.getFlexOffId())
						.collect(Collectors.toList());
				inAppNotification.deleteBySourceId(flexOffIds);
			}
			return flexOffs;
		}
	}

	public List<UserWorkspaces> getStaffByRoleAndScheduleDate(long staffUserCoreTypeId, String scheduleDate,
			long workspaceId) {

		List<Long> usersList = scheduleDAO.findUsersByShiftDateAndDepartmentId(LocalDate.parse(scheduleDate),
				workspaceId);

		List<UserWorkspaces> userWorkspaces = userWorkspacesDAO
				.findByStaffUserCoreTypeIdAndUserIdAndWorkspaceId(staffUserCoreTypeId, usersList, workspaceId);

		return userWorkspaces;
	}

	public List<UserWorkspaces> getStaffByVoluntaryLowCensus(long staffUserCoreTypeId, String scheduleDate,
			long workspaceId) {

		List<Long> usersList = scheduleDAO.findUsersByShiftDateAndDepartmentIdAndVoluntaryLowCensus(
				LocalDate.parse(scheduleDate), workspaceId, true);

		List<UserWorkspaces> userWorkspaces = userWorkspacesDAO
				.findByStaffUserCoreTypeIdAndUserIdAndWorkspaceId(staffUserCoreTypeId, usersList, workspaceId);

		return userWorkspaces;
	}

	// ************************************************Query Worksite
	// Settings***************************************

	public List<WorksiteSettings> getWorksiteSettingsByUserId(long userId) {
		List<WorksiteSettings> settings = worksiteSettingsRepository.findByUserId(userId);
		ArrayList<WorksiteSettings> finalSettings = new ArrayList<>();
		for (WorksiteSettings each : settings) {
			if (finalSettings.size() > 0 && (!(finalSettings.contains(each))))
				finalSettings.add(each);
			else if (finalSettings.size() == 0)
				finalSettings.add(each);
		}
		return finalSettings;
	}

	public WorksiteSettings getWorksiteSettingByUserIdAndWorksiteId(long userId, long worksiteId) {
		return worksiteSettingsRepository.findByUserIdAndWorksiteId(userId, worksiteId);
	}

	// ************************************************Query
	// SwapShiftRequests***************************************

	public SwapShiftRequests getSwapShiftRequestById(long swapShiftRequestId) {

		Optional<SwapShiftRequests> swapShiftRequestOptional = swapShiftRequestsRepository.findById(swapShiftRequestId);

		if (swapShiftRequestOptional.isPresent())
			return swapShiftRequestOptional.get();
		else
			return null;
	}

	public List<SwapShiftRequests> getSwapShiftRequestsSentByUserId(long userId) {

		List<SwapShiftRequests> swapShiftRequests = swapShiftRequestsRepository.findBySenderId(userId);
		List<SwapShiftRequests> updatedSwapShiftRequests = new ArrayList<>();

		for (SwapShiftRequests eachRequest : swapShiftRequests) {
			Optional<Schedules> swapShiftRequestOptional = schedulesRepository
					.findById(eachRequest.getRequestedShiftId());
			if (swapShiftRequestOptional.isPresent() == false) {
				continue;
			} else if (swapShiftRequestOptional.get().getShiftDate().isBefore(LocalDate.now())) {
				continue;
			} else
				updatedSwapShiftRequests.add(eachRequest);
		}
		return updatedSwapShiftRequests;
	}

	public List<SwapShiftRequests> getSwapShiftRequestsRecievedByUserId(long userId) {

		List<SwapShiftRequests> swapShiftRequests = new ArrayList<>();

		swapShiftRequests.addAll(swapShiftRequestsRepository.findByReceiverIdAndStatus(userId, "pending"));
		swapShiftRequests.addAll(swapShiftRequestsRepository.findByReceiverIdAndStatus(userId, "admin_review"));
		swapShiftRequests.addAll(swapShiftRequestsRepository.findByReceiverIdAndStatus(userId, "approved"));

		return swapShiftRequests;
	}

	// ******************************************Query Set Schedule
	// Layout*******************************************

	public SetScheduleLayouts getSetScheduleLayoutById(long setScheduleLayoutId) {

		return setScheduleLayoutDAO.findBySetScheduleLayoutId(setScheduleLayoutId);
	}

	public List<SetScheduleLayouts> getSetScheduleLayoutsByDepartmentId(long departmentId) {

		List<SetScheduleLayouts> setScheduleLayouts = setScheduleLayoutDAO.findByDepartmentId(departmentId);

		if (setScheduleLayouts.size() > 0) {
			List<SetScheduleLayouts> filteredSetScheduleLayouts = setScheduleLayouts.stream()
					.filter(e -> e.getEndDate() != null && e.getEndDate().isBefore(LocalDate.now()))
					.collect(Collectors.toList());

			if (filteredSetScheduleLayouts.size() > 0) {

				setScheduleLayouts.removeAll(filteredSetScheduleLayouts);
				List<Long> setScheduleLayoutIds = filteredSetScheduleLayouts.stream()
						.map(e -> e.getSetScheduleLayoutId()).collect(Collectors.toList());
				setScheduleLayoutDAO.deleteAllBySetScheduleLayoutIds(setScheduleLayoutIds);
				staffWeekOffsDAO.removeBySetScheduleLayoutIds(setScheduleLayoutIds);
				inAppNotification.deleteBySourceId(setScheduleLayoutIds);
			}
			return setScheduleLayouts;
		} else
			return setScheduleLayouts;
	}

	public Iterable<UserBasicInformation> checkDuplicateSetScheduleLayoutsByStaffCoreTypeIdsAndDepartmentId(
			List<Long> staffCoreTypeIds, List<Long> shiftTypes, long departmentId, String scheduleLength,
			String startDate) {

		List<Long> userIds = new ArrayList<>();

		LocalDate localStartDate = LocalDate.parse(startDate);
		LocalDate localEndDate = null;

		if (scheduleLength.equalsIgnoreCase("INF")) {
			localEndDate = localStartDate.plusDays(1);
		} else {
			String[] splitScheduleLength = scheduleLength.split("(?<=\\d)(?=\\D)");

			if (splitScheduleLength[1].equalsIgnoreCase("w")) {
				int number = Integer.parseInt(splitScheduleLength[0]);
				localEndDate = LocalDate.parse(startDate).plusWeeks(number).minusDays(1);
			} else if (splitScheduleLength[1].equalsIgnoreCase("m")) {
				int number = Integer.parseInt(splitScheduleLength[0]);
				localEndDate = LocalDate.parse(startDate).plusMonths(number).minusDays(1);
			}
		}

		List<SetScheduleLayouts> setScheduleLayouts = setScheduleLayoutDAO.findByStaffUserCoreTypeIdsAndDepartmentId(
				staffCoreTypeIds, shiftTypes, departmentId, localStartDate, localEndDate);

		if (setScheduleLayouts != null && setScheduleLayouts.size() > 0) {

			for (SetScheduleLayouts eachSchedule : setScheduleLayouts) {

				if (eachSchedule.getEndDate() != null && eachSchedule.getEndDate().isAfter(LocalDate.now())) {
					List<Long> selectedRoles = eachSchedule.getRoles().stream()
							.filter(emitter -> staffCoreTypeIds.contains(emitter)).collect(Collectors.toList());
					userIds.addAll(
							userWorkspacesDAO.findByStaffUserCoreTypeIdAndDepartmentId(selectedRoles, departmentId));
				}
			}
			return userRepository.findAllById(userIds);
		} else
			return new ArrayList<>();
	}

	public Iterable<UserBasicInformation> getStaffByStaffUserCoreTypeIdsAndShiftTypesAndDepartmentId(
			List<Long> staffUserCoreTypeIds, List<Long> shiftTypes, long departmentId) {

		List<UserWorkspaces> users = userWorkspacesDAO
				.findUserWorkspacesByStaffUserCoreTypeIdsAndDepartmentId(staffUserCoreTypeIds, departmentId);
		List<Long> userIds = new ArrayList<>();

		if (users != null) {
			for (UserWorkspaces eachUser : users) {
				if (eachUser.getDepartmentShiftIds() != null) {
					if (shiftTypes.containsAll(eachUser.getDepartmentShiftIds()))
						userIds.add(eachUser.getUserId());
				}
			}

			return userRepository.findAllById(userIds);
		} else {
			return null;
		}
	}

	// ************************************ Query
	// Clients-Household-FamilyContacts***********************************

	public Clients getClientProfileById(long clientId) {

		if (clientId > 0) {
			return clientsDAO.getClientProfileById(clientId);
		} else
			return null;
	}

	public List<Clients> getClientsByBusinessId(long businessId) {

		return clientsDAO.getClientsByBusinessId(businessId);
	}

	// ************************** Query Assignments-Assignment Schedules-Assignment
	// Invitee**************************

	public Assignments getAssignmentById(long assignmentId) {

		return assignmentsDAO.findAssignmentById(assignmentId);
	}

	public List<Assignments> getAssignmentDetails(long assignmentId) {

		return assignmentsDAO.getAssignmentsByBusinessId(assignmentId);
	}

	public List<UserWorkspaces> getStaffMembersByStaffUserCoreTypeIdAndBusinessIdAndStartTimeAndEndTime(
			long staffUserCoreTypeId, long businessId, long clientId, List<AssignmentSchedules> assignmentSchedules) {

		List<UserWorkspaces> userWorkspaceList = userWorkspacesDAO
				.findByStaffUserCoreTypeIdAndBusinessId(staffUserCoreTypeId, businessId);
		Map<Long, UserWorkspaces> userWorkspaceMap = userWorkspaceList.stream()
				.collect(Collectors.toMap(UserWorkspaces::getUserId, userWorkspace -> userWorkspace));
		List<Long> assignmentIds = assignmentsDAO.findAssignmentsByEndDate();
		for (Long eachUserId : userWorkspaceMap.keySet()) {

			for (AssignmentSchedules eachSchedule : assignmentSchedules) {
				List<AssignmentSchedules> assignmentSchedule = assignmentSchedulesDAO
						.findByStaffIdAndStartTimeAndEndTime(eachUserId, assignmentIds, eachSchedule.getWeekDay(),
								eachSchedule.getStartTime(), eachSchedule.getEndTime());

				if (assignmentSchedule.size() > 0)
					userWorkspaceList.remove(userWorkspaceMap.get(eachUserId));
			}
		}
		return userWorkspaceList;
	}

	public List<Assignments> getAssignmentsByUserIdAndSelectedDate(long userId, String onDate) {

		List<Assignments> assignmentList = assignmentsDAO.findAssignmentByGivenDate(LocalDate.parse(onDate));

		SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
		Date date = null;
		try {
			date = simpleDateFormat1.parse(onDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		DateFormat weekDayFormatter = new SimpleDateFormat("EEEE"); // change the date in weekday
		String weekDay = weekDayFormatter.format(date).toLowerCase();

		for (Assignments eachAssignment : new ArrayList<>(assignmentList)) {

			if (assignmentSchedulesDAO.findAssignmentSchedulesByAssignmentIdAndUserIdAndWeekDay(
					eachAssignment.getAssignmentId(), userId, weekDay) == null)
				assignmentList.remove(eachAssignment);
		}
		return assignmentList;
	}

	// **********************************Query Assignments-Assignments
	// Schedules*************************************

	public List<Assignments> getAssignmentsByBusinessId(long businessId) {

		List<Assignments> assignmentList = assignmentsDAO.getAssignmentsByBusinessId(businessId);

		for (Assignments eachAssignment : assignmentList) {

			if (eachAssignment.getEndDate() != null) {
				long dateDifference = ChronoUnit.DAYS.between(LocalDate.now(), eachAssignment.getEndDate()) + 1;

				if (((eachAssignment.getStatus().equals("Inactive")) || eachAssignment.getStatus().equals("Draft"))
						&& (dateDifference < 0 && dateDifference >= -30)) {

					eachAssignment.setStatus("Inactive");
					mongoTemplate.save(eachAssignment);
				} else if (((eachAssignment.getStatus().equals("Inactive"))
						|| eachAssignment.getStatus().equals("Draft"))
						&& (dateDifference < 0 && dateDifference <= -30)) {

					eachAssignment.setStatus("DeletedAssignment");
					mongoTemplate.save(eachAssignment);
				} else if (eachAssignment.getStatus().equals("DeletedAssignment") && dateDifference <= -30) {

					mongoTemplate.findAndRemove(
							org.springframework.data.mongodb.core.query.Query
									.query(Criteria.where("assignmentId").is(eachAssignment.getAssignmentId())),
							AssignmentInvitees.class);
					mongoTemplate.findAndRemove(
							org.springframework.data.mongodb.core.query.Query
									.query(Criteria.where("assignmentId").is(eachAssignment.getAssignmentId())),
							AssignmentSchedules.class);
					mongoTemplate.remove(eachAssignment, "assignments");
				}
			}
		}
		return assignmentList;
	}

	public List<Assignments> getAssignmentsByUserId(long userId) {

		List<Long> assignmentIds = assignmentsDAO.findAssignmentsByEndDate();

		List<AssignmentSchedules> assignmentSchedules = assignmentSchedulesDAO
				.findAssignmentSchedulesByAssignmentIdsAndUserId(assignmentIds, userId);

		assignmentIds = assignmentSchedules.stream().map(AssignmentSchedules::getAssignmentId)
				.collect(Collectors.toList());

		assignmentIds = assignmentsInviteesDAO.findByUserIdAndAssignmentIdsAndIsAccepted(userId, assignmentIds, false)
				.stream().map(e -> e.getAssignmentId()).collect(Collectors.toList());

		return assignmentsDAO.findAssignmentByIds(assignmentIds);
	}

	public AssignmentSchedules getAssignmentsSchedulesById(long assignmentScheduleId) {

		AssignmentSchedules assignmentSchedule = assignmentSchedulesDAO
				.findAssignmentSchedulesById(assignmentScheduleId);
		return assignmentSchedule;
	}

	public List<Long> checkDuplicateAssignmentsByStartDateAndEndDateAndBusinessIdAndStaffCoreTypeId(String startDate,
			String endDate, long businessId, long staffCoreTypeId) {

		LocalDate localStartDate = LocalDate.parse(startDate);
		LocalDate localEndDate = LocalDate.parse(endDate);

		List<Long> assignmentList = assignmentsDAO.findByStartDateAndEndDateAndBusinessId(localStartDate, localEndDate,
				businessId);

		List<AssignmentSchedules> assignmentSchedules = assignmentSchedulesDAO
				.findAssignmentSchedulesByAssignmentIdsAndStaffCoreTypeId(assignmentList, staffCoreTypeId);

		if (assignmentSchedules.size() > 0)
			return assignmentList;
		else
			return new ArrayList<>();
	}

	// ************************************************Query User Diploma
	// *******************************************

	public UserDiploma findUserDiplomaById(long userDiplomaId) {

		return userDiplomaDAO.findUserDiplomaById(userDiplomaId);
	}

	public List<UserDiploma> findListOfDiplomaByUserId(long userId) {

		return userDiplomaDAO.findListOfDiplomaByUserId(userId);
	}

	// ************************************************Query
	// References**********************************************

	public References findReferenceById(long referenceId) {

		return referencesDAO.findReferenceById(referenceId);
	}

	public List<References> findListOfReferencesByUserId(long userId) {

		return referencesDAO.findListOfReferencesByUserId(userId);
	}

	// ************************************************Query
	// Messages************************************************

	public List<Messages> findMessagesOfUser(long senderId) {

		Set<Long> receiverIds = new HashSet<>(); // collects list of receiverId without any duplicate values
		List<Messages> finalMessages = new ArrayList<>(); // displays the latest messages of receiverId

		List<GroupChats> groupChats = groupChatsDAO.findGroupChatByUserId(senderId);
		List<Long> groupIds = groupChats.stream().distinct().map(e -> e.getChatGroupId()).collect(Collectors.toList()); // extract
																														// all
																														// group
																														// ids
		List<DeletedMessages> deletedGroupMessages = messagesDAO.findDeletedMessagesBySenderIdAndGroupId(senderId,
				groupIds); // fetch deleted groups

		List<Long> deletedMessageIds = deletedGroupMessages.stream().map(e -> e.getGroupId())
				.collect(Collectors.toList()); // extract all ids
		if (groupIds.size() > 0) {
			groupIds.removeAll(deletedMessageIds); // remove the groups
			for (Long each : groupIds) {
				Messages message = messagesDAO.findLatestMessagesByGroupId(each); // add the recent message of each
																					// group
				if (message != null)
					finalMessages.add(message);
			}
		}

		List<Long> deletedMessagesUserIds = messagesDAO.findUsersDeletedMessagesIdsByUserId(senderId);

		messagesDAO.findReceiverIdsBySenderId(senderId).stream().forEach(element -> {

			if (element.getReceiverId() != null && !element.getReceiverId().equals(senderId))
				receiverIds.add(element.getReceiverId());
			else if (element.getSenderId() != senderId)
				receiverIds.add(element.getSenderId());
		});

		receiverIds.removeAll(deletedMessagesUserIds);

		for (Long each : receiverIds) {

			List<Messages> displayMessage = new ArrayList<>();

			displayMessage.addAll(messagesDAO.findLatestMessagesBySenderIdAndReceiverId(senderId, each));
			displayMessage.addAll(messagesDAO.findLatestMessagesBySenderIdAndReceiverId(each, senderId));

			if (displayMessage.size() > 0) {
				displayMessage.sort(new SortMessages());
				finalMessages.add(displayMessage.get(displayMessage.size() - 1));
			}
		}
		finalMessages.sort(new SortMessages());

		return finalMessages;
	}

	public List<Messages> findMessagesOfSenderAndReceiverId(long senderId, long receiverId) {

		Map<Long, Messages> messageList = messagesDAO.findAllMessagesBySenderIdAndReceiverId(senderId, receiverId)
				.stream().collect(Collectors.toMap(Messages::getMessageId, e -> e));

		DeletedMessages deletedMessage = messagesDAO.findDeletedMessagesBySenderAndReceiverId(senderId, receiverId);

		if (deletedMessage != null)
			messageList.keySet().removeAll(deletedMessage.getMessageIds());

		List<Messages> finalMessageList = new ArrayList<>(messageList.values());

		Collections.sort(finalMessageList, new SortMessages());

		return finalMessageList;
	}

	public List<Messages> findTextMessagesOfUsers(long userId, String text) {

		List<Messages> messages = new ArrayList<>(messagesDAO.findReceiverIdsBySenderId(userId));
		List<Long> receiverIds = messages.stream().filter(e -> e.getReceiverId() != null).map(e -> e.getReceiverId())
				.distinct().collect(Collectors.toList());
		List<Long> removedUserMessagesUserIds = messagesDAO.findUsersDeletedMessagesIdsByUserId(userId);
		receiverIds.removeAll(removedUserMessagesUserIds);

		List<GroupChats> groupChats = groupChatsDAO.findGroupChatByUserId(userId);
		List<Long> groupIds = groupChats.stream().map(e -> e.getChatGroupId()).distinct().collect(Collectors.toList());

		return messagesDAO.findTextMessagesOfUsers(receiverIds, text, groupIds);
	}

	public List<Messages> findLatestMessageOfReceiverId(long senderId, long receiverId) {

		return messagesDAO.findLatestMessagesOfReceiverIdToSenderId(receiverId, senderId);
	}

	public List<Messages> findMessagesOfReceiverInConversationScreen(long senderId) {

		return util.getResponseOfConversationScreen(senderId);
	}

	public GroupChats findGroupById(long chatGroupId) {

		return groupChatsDAO.findGroupChatByGroupChatId(chatGroupId);
	}

	public List<IsSeenByGroupMembers> getIsSeenByUsers(long messageId) {

		Optional<Messages> messageOptional = messagesRepository.findById(messageId);

		if (messageOptional.isPresent())
			return messageOptional.get().getIsSeenByGroupMembers();
		else
			return new ArrayList<>();
	}

	// *************************************************QUERY
	// EHRSYSTEMS**********************************************

	public List<EhrSystems> findAllEhrSystems() {
		return mongoTemplate.findAll(EhrSystems.class);
	}

	// ************************************************* Query
	// Availability*******************************************

	public List<Availability> findUserAvailabilityForScheduleDate(long userId, String scheduleStartDate,
			String scheduleEndDate) {

		if (userRepository.findByUserIdAndStatus(userId, "Active").isPresent()) {

			List<Availability> availability = availabilityDAO.findAvailableUsersByScheduleDates(userId,
					LocalDate.parse(scheduleStartDate), LocalDate.parse(scheduleEndDate));

			return availability;
		} else
			return null;
	}

	public List<GeneralAvailability> findUserGeneralAvailabilityForSelectedDate(long userId, String shiftDate) {

		List<GeneralAvailability> generalAvailability = new ArrayList<>();

		if (userRepository.findByUserIdAndStatus(userId, "Active").isPresent()) {

			LocalDate localShiftDate = LocalDate.parse(shiftDate);

			generalAvailability = generalAvailabilityDAO.findGeneralAvailabilityOfUsersByUserIdStartDate(userId,
					localShiftDate);

			List<Requests> userRequests = requestsDAO.findRequestsByUserIdAndOnDate(userId, localShiftDate);

			if (userRequests.size() == 0)
				return generalAvailability;
			else {
				generalAvailability.clear();
				return generalAvailability;
			}
		} else
			return generalAvailability;
	}

	// ************************************************QUERY
	// FEATURES*****************************************************

	public List<Features> findAllFeatures() {

		return featuresRepository.findAll();
	}

	// ************************************************QUERY
	// USER-PERMISSIONS*********************************************

	public UserPermissions findUserPermissionsByUserId(long userId) {

		Optional<UserPermissions> userPermission = userPermissionsRepository.findById(userId);

		return userPermission.isPresent() ? userPermission.get() : null;
	}

	public UserOrganizations findUserOrganizationByUserIdAndOrganizationId(long userId, long organizationId) {

		return userOrganizationsRepository.findByUserIdAndOrganizationId(userId, organizationId);
	}

	public List<StaffUserCoreTypes> findAllStaffUserCoreTypesByFirmType(String firmType) {

		return staffUserCoreTypesDAO.findAllStaffUserCoreTypesByFirmType(firmType);

	}

	public UnreadMessagesAndNotificationsCount getUnreadMessagesAndNotificationsCount(long userId) {
		return messageService.getUnreadMessagesAndNotificationsCount(userId);
	}
}

class SortMessages implements Comparator<Messages> {

	@Override
	public int compare(Messages messageObject1, Messages messageObject2) {

		if (messageObject1.getMessageId() > messageObject2.getMessageId())
			return 1;
		else if (messageObject1.getMessageId() < messageObject2.getMessageId())
			return -1;
		else
			return 0;
	}
}