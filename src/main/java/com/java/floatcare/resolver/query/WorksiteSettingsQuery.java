package com.java.floatcare.resolver.query;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.java.floatcare.model.AssignmentInvitees;
import com.java.floatcare.model.Assignments;
import com.java.floatcare.model.Businesses;
import com.java.floatcare.model.Department;
import com.java.floatcare.model.Organizations;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.model.UserWorkspaces;
import com.java.floatcare.model.WorksiteSettings;
import com.java.floatcare.repository.BusinessesRepository;
import com.java.floatcare.repository.DepartmentRepository;
import com.java.floatcare.repository.OrganizationRepository;
import com.java.floatcare.repository.UserBasicInformationRepository;
import com.java.floatcare.repository.UserWorkspacesRepository;

@Component
public class WorksiteSettingsQuery implements GraphQLResolver<WorksiteSettings>{
	
	@Autowired
	private MongoTemplate mongoTemplate;
	@Autowired
	private BusinessesRepository businessesRepository;
	@Autowired 
	private OrganizationRepository organizationRepository;
	@Autowired
	private UserWorkspacesRepository userWorkspacesRepository;
	@Autowired
	private DepartmentRepository departmentRepository;
	@Autowired
	private UserBasicInformationRepository userBasicInformationRepository;
	
	public Organizations getOrganizationDetails (WorksiteSettings worksiteSettings) {
		
		Optional<Organizations> organizationOptional = organizationRepository.findById(worksiteSettings.getOrganizationId());
		
		if (organizationOptional.isPresent())
			return organizationOptional.get();
		else
			return null;
	}
	
	public Businesses getWorksiteDetails (WorksiteSettings worksiteSettings) {

		Optional<Businesses> businessOptional = businessesRepository.findById(worksiteSettings.getWorksiteId());
		
		if (businessOptional.isPresent())
			return businessOptional.get();
		else
			return null;
		
	}
	
	public UserBasicInformation getUserDetails (WorksiteSettings worksiteSettings) {
		
		Optional<UserBasicInformation> userOptional = userBasicInformationRepository.findById(worksiteSettings.getUserId());
		
		if (userOptional.isPresent())
			return userOptional.get();
		else
			return null;
	}
	
	public int departmentCount (WorksiteSettings worksiteSettings) {
	
		long count = userWorkspacesRepository.countByUserIdAndBusinessId(worksiteSettings.getUserId(), worksiteSettings.getWorksiteId());
		return (int)count;
	}
	
	public Iterable<Department> getDepartmentDetails(WorksiteSettings worksiteSettings) {
		
		List<UserWorkspaces> workspaces = userWorkspacesRepository.findByUserIdAndBusinessId(worksiteSettings.getUserId(), worksiteSettings.getWorksiteId());
		
		List<Long> departmentIds = workspaces.stream().map(emitter -> emitter.getWorkspaceId()).collect(Collectors.toList());
		
		return departmentRepository.findAllById(departmentIds);
		
	}
	
	public List<Assignments> getAssignments (WorksiteSettings worksiteSettings) {
		
		List<Assignments> assignmentList = new ArrayList<>();
		List<AssignmentInvitees> assignmentAssigned  = mongoTemplate.find(Query.query(Criteria.where("staffId").is(worksiteSettings.getUserId()))
				.addCriteria(Criteria.where("isApproved").is(true)), AssignmentInvitees.class);

		for (AssignmentInvitees eachInvitees: assignmentAssigned) {
			
			 Assignments assignment = mongoTemplate.findOne(Query.query(Criteria.where("assignmentId").is(eachInvitees.getAssignmentId()))
					 .addCriteria(Criteria.where("businessId").is(worksiteSettings.getWorksiteId())), Assignments.class);

			 if (assignment!= null && assignment.getEndDate() != null && assignment.getEndDate().isAfter(LocalDate.now())) {
				assignmentList.add(assignment);
			 }
		 }
		return assignmentList;
	}		
}
