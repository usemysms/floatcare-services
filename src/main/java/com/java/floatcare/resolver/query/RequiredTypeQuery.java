package com.java.floatcare.resolver.query;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.java.floatcare.api.response.pojo.RequiredType;
import com.java.floatcare.model.StaffUserCoreTypes;
import com.java.floatcare.repository.StaffUserCoreTypesRepository;

@Component
public class RequiredTypeQuery implements GraphQLResolver<RequiredType>{

	@Autowired
	private StaffUserCoreTypesRepository staffUserCoreTypeRepository;
	
	public String getStaffCoreTypeName (RequiredType requiredType) {
		
		Optional<StaffUserCoreTypes> staffUserCoreType = staffUserCoreTypeRepository.findById(requiredType.getStaffCoreTypeId());
		
		if (staffUserCoreType.isPresent())
			return staffUserCoreType.get().getLabel();
		else
			return "";
	}
}
