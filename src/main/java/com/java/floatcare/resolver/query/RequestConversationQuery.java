package com.java.floatcare.resolver.query;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.java.floatcare.model.RequestConversation;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.repository.UserBasicInformationRepository;

@Component
public class RequestConversationQuery implements GraphQLResolver<RequestConversation>{

	@Autowired
	private UserBasicInformationRepository userRepository;
	
	
	public UserBasicInformation getSenderInformation (RequestConversation requestConversation) {
		
		Optional<UserBasicInformation> senderInformation = userRepository.findById(requestConversation.getSenderId());
		
		if (senderInformation.isPresent())
			return senderInformation.get();
		else
			return null;
	}
	
	public UserBasicInformation getReceiverInformation (RequestConversation requestConversation) {
		
		Optional<UserBasicInformation> receiverInformation = userRepository.findById(requestConversation.getReceiverId());
		
		if (receiverInformation.isPresent())
			return receiverInformation.get();
		else
			return null;
	}
	
	
}
