package com.java.floatcare.resolver.query;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.java.floatcare.model.Department;
import com.java.floatcare.model.DepartmentSettings;
import com.java.floatcare.repository.DepartmentRepository;

@Component
public class DepartmentSettingsQuery implements GraphQLResolver<DepartmentSettings>{
	
	@Autowired
	private DepartmentRepository departmentRepository;
	
	
	public String getDepartmentName (DepartmentSettings departmentSettings) {
		
		Optional<Department> departmentOpt = departmentRepository.findById(departmentSettings.getDepartmentId());
		
		if (departmentOpt.isPresent())
			return departmentOpt.get().getDepartmentTypeName();
		else
			return null;
	}
}
