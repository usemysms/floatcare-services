package com.java.floatcare.resolver.query;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.java.floatcare.dao.ScheduleDAO;
import com.java.floatcare.model.Businesses;
import com.java.floatcare.model.Department;
import com.java.floatcare.model.DepartmentShifts;
import com.java.floatcare.model.Schedules;
import com.java.floatcare.model.StaffUserCoreTypes;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.model.UserWorkspaces;
import com.java.floatcare.model.WorksiteSettings;
import com.java.floatcare.repository.BusinessesRepository;
import com.java.floatcare.repository.DepartmentRepository;
import com.java.floatcare.repository.DepartmentShiftsRepository;
import com.java.floatcare.repository.StaffUserCoreTypesRepository;
import com.java.floatcare.repository.UserBasicInformationRepository;
import com.java.floatcare.repository.UserWorkspacesRepository;
import com.java.floatcare.repository.WorksiteSettingsRepository;

@Component
public class SchedulesQuery implements GraphQLResolver<Schedules> {

	@Autowired
	private BusinessesRepository businessesRepository;
	@Autowired
	private DepartmentRepository departmentRepository;
	@Autowired
	private DepartmentShiftsRepository departmentShiftsRepository;
	@Autowired
	private UserWorkspacesRepository userWorkspacesRepository;
	@Autowired
	private StaffUserCoreTypesRepository staffUserCoreTypesRepository;
	@Autowired
	private WorksiteSettingsRepository worksiteSettingsRepository;
	@Autowired
	private UserBasicInformationRepository userRepository;
	@Autowired
	private ScheduleDAO scheduleDAO;

	public String getWorksiteName(Schedules schedules) {

		Optional<Businesses> optionalBusinesses = businessesRepository.findById(schedules.getWorksiteId());

		if (optionalBusinesses.isPresent())
			return optionalBusinesses.get().getName();
		else
			return null;
	}

	public String getDepartmentName(Schedules schedules) {

		Optional<Department> optionalDepartment = departmentRepository.findById(schedules.getDepartmentId());

		if (optionalDepartment.isPresent())
			return optionalDepartment.get().getDepartmentTypeName();
		else
			return null;
	}
	
	public String getShiftTypeName (Schedules schedules) {
		
		Optional<DepartmentShifts> departmentShiftsOptional = departmentShiftsRepository.findById(schedules.getShiftTypeId());
		
		if (departmentShiftsOptional.isPresent())
			return departmentShiftsOptional.get().getLabel();
		else
			return null;
	}
	
	public String getStaffCoreTypeName(Schedules schedules) {
		
		UserWorkspaces userWorkspace = userWorkspacesRepository.findByUserIdAndWorkspaceId(schedules.getUserId(), schedules.getDepartmentId());
		
		if (userWorkspace != null) {
			StaffUserCoreTypes staffUser = staffUserCoreTypesRepository.findStaffUserCoreTypesById(userWorkspace.getStaffUserCoreTypeId());
			if (staffUser != null)
				return staffUser.getLabel();
			else
				return null;
		}
		else
			return null;	
	}
	
	public String getWorksiteColor (Schedules schedules) {
		
		WorksiteSettings color = worksiteSettingsRepository.findByUserIdAndWorksiteId(schedules.getUserId(), schedules.getWorksiteId());
		
		if (color != null)
			return color.getColor();
		else
			return null;
	}
	
	public Iterable<UserBasicInformation> getWorkingColleagues (Schedules schedules) {
		
		List<Long> userIds = scheduleDAO.findUsersByShiftDateAndDepartmentId(schedules.getShiftDate(), schedules.getDepartmentId());
		
		return userRepository.findAllById(userIds);
	}
	
	public Businesses getWorksiteDetails (Schedules schedules) {
		
		Optional<Businesses> businessOptional = businessesRepository.findById(schedules.getWorksiteId());
		
		if (businessOptional.isPresent())
			return businessOptional.get();
		else
			return null;
	}
	
	public UserBasicInformation getUserDetails (Schedules schedules) {
		
		Optional<UserBasicInformation> userOptional = userRepository.findById(schedules.getUserId());
		
		if (userOptional.isPresent())
			return userOptional.get();
		else
			return null;
	}
}
