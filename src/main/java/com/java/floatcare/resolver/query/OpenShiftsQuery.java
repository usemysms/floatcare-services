package com.java.floatcare.resolver.query;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.java.floatcare.dao.ScheduleDAO;
import com.java.floatcare.dao.UserWorkspacesDAO;
import com.java.floatcare.model.Businesses;
import com.java.floatcare.model.Department;
import com.java.floatcare.model.DepartmentShifts;
import com.java.floatcare.model.OpenShiftInvitees;
import com.java.floatcare.model.OpenShifts;
import com.java.floatcare.model.Organizations;
import com.java.floatcare.model.Schedules;
import com.java.floatcare.model.StaffUserCoreTypes;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.model.UserWorkspaces;
import com.java.floatcare.repository.BusinessesRepository;
import com.java.floatcare.repository.DepartmentRepository;
import com.java.floatcare.repository.DepartmentShiftsRepository;
import com.java.floatcare.repository.OpenShiftInviteesRepository;
import com.java.floatcare.repository.OrganizationRepository;
import com.java.floatcare.repository.StaffUserCoreTypesRepository;
import com.java.floatcare.repository.UserBasicInformationRepository;
import com.java.floatcare.repository.UserWorkspacesRepository;

@Component
public class OpenShiftsQuery implements GraphQLResolver<OpenShifts> {

	@Autowired
	private OpenShiftInviteesRepository openShiftInviteesRepository;
	@Autowired
	private StaffUserCoreTypesRepository staffUserCoreTypesRepository;
	@Autowired
	private DepartmentShiftsRepository departmentShiftsRepository;
	@Autowired
	private DepartmentRepository departmentRepository;
	@Autowired
	private BusinessesRepository businessesRepository;
	@Autowired
	private OrganizationRepository organizationRepository;
	@Autowired
	private UserWorkspacesRepository userWorkspacesRepository;
	@Autowired
	private UserWorkspacesDAO userWorkspacesDAO;
	@Autowired
	private ScheduleDAO scheduleDAO;
	@Autowired
	private UserBasicInformationRepository userBasicInformationRepository;

	private long userId;

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public List<OpenShiftInvitees> getInvitees(OpenShifts openShift) {

		List<OpenShiftInvitees> openShiftList = openShiftInviteesRepository.findByOpenShiftId(openShift.getOpenShiftId());

		return openShiftList;
	}

	public String getStaffUserName(OpenShifts openShifts) {

		Optional<StaffUserCoreTypes> staffUser = staffUserCoreTypesRepository.findById(openShifts.getStaffUserCoreTypeId());

		if (openShifts.getQuota()  < 2)
			return staffUser.get().getLabel();
		else
			return staffUser.get().getLabel()+"s";
	}
	
	public String getOpenShiftCoreTypeName(OpenShifts openShifts) {

		Optional<StaffUserCoreTypes> staffUser = staffUserCoreTypesRepository.findById(openShifts.getStaffUserCoreTypeId());

		if (staffUser != null)
			return staffUser.get().getLabel();
		else
			return "";
	}

	public String getDepartmentName(OpenShifts openShifts) {

		Optional<Department> staffUser = departmentRepository.findById(openShifts.getDepartmentId());

		return staffUser.get().getDepartmentTypeName();
	}

	public Organizations getOrganization(OpenShifts openShifts) {

		Optional<Businesses> businesses = businessesRepository.findById(openShifts.getWorksiteId());

		if (businesses.isPresent()) {

			Optional<Organizations> organizations = organizationRepository.findById(businesses.get().getOrganizationId());
			return organizations.get();

		}

		else
			return null;
	}

	public DepartmentShifts getShiftType(OpenShifts openShifts) {

		Optional<DepartmentShifts> departmentShiftsOpt = departmentShiftsRepository.findById(openShifts.getShiftTypeId());

		if (departmentShiftsOpt.isPresent())
			return departmentShiftsOpt.get();
		else
			return null;

	}

	public String getWorksiteName(OpenShifts openShifts) {

		Optional<Businesses> worksiteOpt = businessesRepository.findById(openShifts.getWorksiteId());

		if (worksiteOpt.isPresent())
			return worksiteOpt.get().getName();
		else
			return null;
	}

	public String getWorksiteAddress(OpenShifts openShifts) {

		Optional<Businesses> worksiteOpt = businessesRepository.findById(openShifts.getWorksiteId());

		if (worksiteOpt.isPresent())
			return worksiteOpt.get().getCity() +" " + worksiteOpt.get().getState() +" "+ worksiteOpt.get().getAddress();
		else
			return null;
	}

	public List<Schedules> getConflictShifts(OpenShifts openShifts) throws ParseException {

		UserWorkspaces userWorkspace = userWorkspacesRepository.findByUserIdAndWorkspaceIdAndStatusAndIsJoined(this.getUserId(), openShifts.getDepartmentId(), "Active", true);

		List<Schedules> conflictSchedules = new ArrayList<>();

		//System.out.println("userWorkspace: "+userWorkspace.getDepartmentShiftId());
		if (userWorkspace != null) {

			List<DepartmentShifts> departmentShifts = (List<DepartmentShifts>) departmentShiftsRepository.findAllById(userWorkspace.getDepartmentShiftIds());

			List<Schedules> assignedShifts = scheduleDAO.findScheduleUsersByUserIdAndOnDate(userId, openShifts.getOnDate());

			//	System.out.println("assignedShifts.size() : "+ assignedShifts.size());

			DateFormat timeFormatter = new SimpleDateFormat("HH:mm");
			timeFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));

			
			for (Schedules eachSchedule : assignedShifts) {
				
				// filter department shift and calculate the conflict
				Optional<DepartmentShifts> departmentShift = departmentShifts.stream().filter(e->e.getDepartmentShiftId() == eachSchedule.getShiftTypeId()).findFirst();
				Date timeFrom = timeFormatter.parse(departmentShift.get().getStartTime().minusSeconds(1).toString()); // formatting departmentShift start time to epoch time
				Date timeTo = timeFormatter.parse(departmentShift.get().getEndTime().minusSeconds(1).toString()); // formatting department Shift end time to epoch time

				long secondsDateTo = timeTo.toInstant().getEpochSecond(); // converting departmentShift endTime to epoch seconds
				long secondsDateFrom = timeFrom.toInstant().getEpochSecond(); // converting departmentShift startTime to epoch seconds
				
				if (departmentShift.get().getLabel().equalsIgnoreCase("Night Shift")) {
					secondsDateTo = secondsDateTo + (24 * 3600);
				}

				
				

				Date scheduleStartTime = timeFormatter.parse(eachSchedule.getStartTime().minusSeconds(1).toString());
				Date scheduleEndTime = timeFormatter.parse(eachSchedule.getEndTime().minusSeconds(1).toString());

				long secondsinScheduleStartTime = scheduleStartTime.toInstant().getEpochSecond(); // converting schedule startTime to epoch seconds
				long secondsinScheduleEndTime = scheduleEndTime.toInstant().getEpochSecond(); // converting schedule endTime to epoch seconds

				if (departmentShiftsRepository.findById(eachSchedule.getShiftTypeId()).get().getLabel().equalsIgnoreCase("Night Shift")) {
					secondsinScheduleEndTime = secondsinScheduleEndTime + (24 * 3600);
				}

				if (((secondsDateFrom <= (secondsinScheduleStartTime)) && (secondsDateTo >= (secondsinScheduleStartTime))) || 
						((secondsDateFrom <= (secondsinScheduleEndTime) && secondsDateTo >= (secondsinScheduleEndTime)))) {
					//System.out.println("conflict 1");
					conflictSchedules.add(eachSchedule);

				} else if (Math.abs((timeFrom.getTime() - scheduleEndTime.getTime()) / 3600000) <= 0.5 || Math.abs((timeTo.getTime() - 
						scheduleStartTime.getTime()) / 3600000) <= 0.5) {

					//System.out.println("conflict 2");
					conflictSchedules.add(eachSchedule);
				}
			}
		}
		return conflictSchedules;
	}
	
	public Businesses getWorksiteDetails (OpenShifts openShifts) {
		
		Optional<Businesses> businessOptional = businessesRepository.findById(openShifts.getWorksiteId());
		
		if (businessOptional.isPresent())
			return businessOptional.get();
		else
			return null;
	}
	
	public int getApprovedCount (OpenShifts openShifts) {
		
		List<OpenShiftInvitees> openShiftInvitees = openShiftInviteesRepository.findByOpenShiftIdAndIsAcceptedAndIsApproved(openShifts.getOpenShiftId(), true, true);
		
		return openShiftInvitees.size();
	}
	
	public int getAcceptedCount (OpenShifts openShifts) {
		
		List<OpenShiftInvitees> openShiftInvitees = openShiftInviteesRepository.findByOpenShiftIdAndIsAcceptedAndIsApproved(openShifts.getOpenShiftId(), true, false);
		
		return openShiftInvitees.size();
	}
	
	public int getPendingCount (OpenShifts openShifts) {
		
		List<OpenShiftInvitees> openShiftInvitees = openShiftInviteesRepository.findByOpenShiftIdAndIsAcceptedAndIsApproved(openShifts.getOpenShiftId(), false, false);

		return openShiftInvitees.size();
	}

	public Iterable<UserBasicInformation> getColleaguesInWorksite(OpenShifts openShifts) {
		
		List<Long> users = userWorkspacesDAO.findByBusinessId(openShifts.getWorksiteId());

		return userBasicInformationRepository.findAllById(users);
	}
}