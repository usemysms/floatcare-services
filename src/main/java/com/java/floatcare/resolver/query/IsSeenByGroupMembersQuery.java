package com.java.floatcare.resolver.query;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.java.floatcare.api.request.pojo.IsSeenByGroupMembers;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.repository.UserBasicInformationRepository;

@Repository
public class IsSeenByGroupMembersQuery implements GraphQLResolver<IsSeenByGroupMembers> {

	@Autowired
	private UserBasicInformationRepository userRepository;
	
	public UserBasicInformation getUserInformation (IsSeenByGroupMembers isSeenByGroupMembers) {
		
		Optional<UserBasicInformation> user = userRepository.findByUserIdAndStatus(isSeenByGroupMembers.getUserId(), "Active");
		
		if (user.isPresent())
			return user.get();
		else
			return null;
	}
}
