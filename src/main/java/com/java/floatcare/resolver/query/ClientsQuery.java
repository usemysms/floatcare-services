package com.java.floatcare.resolver.query;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.java.floatcare.model.AssignmentSchedules;
import com.java.floatcare.model.Assignments;
import com.java.floatcare.model.Clients;
import com.java.floatcare.model.FamilyContacts;
import com.java.floatcare.model.Household;

@Component
public class ClientsQuery implements GraphQLResolver<Clients>{

	@Autowired
	private MongoTemplate mongoTemplate;
	
	
	public List<Household> getHousehold(Clients clients){
		
		return mongoTemplate.find(Query.query(Criteria.where("clientId").is(clients.getClientId())),Household.class);
	}
	
	public List<FamilyContacts> getFamilyContacts(Clients clients){
		
		return mongoTemplate.find(Query.query(Criteria.where("clientId").is(clients.getClientId())),FamilyContacts.class);
	}
	
	public int getAge(Clients clients) {
		
		LocalDate dateOfBirth = clients.getDateOfBirth();
		if(dateOfBirth!=null)
		return LocalDate.now().getYear() - (dateOfBirth.getYear());	
		else
		return 0;
	}
	
	public List<AssignmentSchedules> getAssignmentSchedules(Clients clients) {
		
		List<Long> assignmentIdList = mongoTemplate.find(Query.query(Criteria.where("clientId").is(clients.getClientId())), Assignments.class)
					.stream().map(Assignments::getAssignmentId).collect(Collectors.toList());
		
		return mongoTemplate.find(Query.query(Criteria.where("assignmentId").in(assignmentIdList)), AssignmentSchedules.class);
	}
}
