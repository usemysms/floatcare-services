package com.java.floatcare.resolver.query;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.java.floatcare.api.request.pojo.PermissionSet;
import com.java.floatcare.model.Features;
import com.java.floatcare.repository.FeaturesRepository;

@Component
public class PermissionSetQuery implements GraphQLResolver<PermissionSet> {

	@Autowired
	private FeaturesRepository featuresRepository;

	public String getLabel(PermissionSet permissionSet) {
		
		Optional<Features> feature = featuresRepository.findById(permissionSet.getFeatureId());
		
		String finalResult = feature.isPresent() ? feature.get().getLabel() : "";
		return finalResult;
	}
}
