package com.java.floatcare.resolver.query;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.java.floatcare.model.NotificationTypes;
import com.java.floatcare.model.Notifications;
import com.java.floatcare.repository.NotificationTypesRepository;

@Component
public class NotificationsQuery implements GraphQLResolver<Notifications> {

	@Autowired
	private NotificationTypesRepository notificationTypesRepository;
	
	public String getNotificationType(Notifications notifications) {
		
		Optional<NotificationTypes> notifictionTypes = notificationTypesRepository.findById(notifications.getNotificationTypeId());
		
		if (notifictionTypes.isPresent())
			return notifictionTypes.get().getLabel();
		else
			return null;
	}
	
}
