package com.java.floatcare.resolver.query;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.java.floatcare.dao.ClientsDAO;
import com.java.floatcare.dao.ScheduleDAO;
import com.java.floatcare.dao.UserBasicInformationRepositoryDAO;
import com.java.floatcare.dao.UserCredentialsDAO;
import com.java.floatcare.model.Businesses;
import com.java.floatcare.model.Department;
import com.java.floatcare.model.DepartmentShifts;
import com.java.floatcare.model.Household;
import com.java.floatcare.model.Organizations;
import com.java.floatcare.model.Schedules;
import com.java.floatcare.model.StaffUserCoreTypes;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.model.UserCredentials;
import com.java.floatcare.model.UserWorkspaces;
import com.java.floatcare.repository.BusinessesRepository;
import com.java.floatcare.repository.DepartmentRepository;
import com.java.floatcare.repository.DepartmentShiftsRepository;
import com.java.floatcare.repository.OrganizationRepository;
import com.java.floatcare.repository.SchedulesRepository;
import com.java.floatcare.repository.StaffUserCoreTypesRepository;
import com.java.floatcare.repository.UserBasicInformationRepository;
import com.java.floatcare.repository.UserWorkspacesRepository;
import static com.java.floatcare.utils.DistanceCalculate.*;

@Component
public class UserWorkspaceQuery implements GraphQLResolver<UserWorkspaces> {

	@Autowired
	private BusinessesRepository businessesRepository;
	@Autowired
	private DepartmentRepository departmentRepository;
	@Autowired
	private OrganizationRepository organizationRepository;
	@Autowired
	private UserBasicInformationRepositoryDAO userBasicInformationRepositoryDAO;
	@Autowired
	private UserBasicInformationRepository userBasicInformationRepository;
	@Autowired
	private StaffUserCoreTypesRepository staffUserCoreTypesRepository;
	@Autowired
	private DepartmentShiftsRepository departmentShiftsRepository;
	@Autowired
	private UserWorkspacesRepository userWorkspacesRepository;
	@Autowired
	private SchedulesRepository schedulesRepository;
	@Autowired
	private UserCredentialsDAO userCredentialsDAO;
	@Autowired
	private ScheduleDAO scheduleDAO;
	@Autowired
	private ClientsDAO clientsDAO;

	public String getWorkspaceName(UserWorkspaces userWorkSpaces) {

		if (userWorkSpaces != null) {
			switch (userWorkSpaces.getWorkspaceType().toLowerCase()) {

			case "organization":
				List<Organizations> organization = organizationRepository.findByOrganizationId(userWorkSpaces.getOrganizationId());

				if (organization.size() > 0) {
					userWorkSpaces.setWorkspaceName(organization.get(0).getName());
				} else
					userWorkSpaces.setWorkspaceName(null);
				break;
			case "business":
				List<Businesses> businesses = businessesRepository
						.findBusinessByBusinessId(userWorkSpaces.getWorkspaceId());
				if (businesses.size() > 0) {
					userWorkSpaces.setWorkspaceName(businesses.get(0).getName());
				} else
					userWorkSpaces.setWorkspaceName(null);
				break;
			case "department":
				List<Department> department = departmentRepository
						.findDepartmentByDepartmentId(userWorkSpaces.getWorkspaceId());
				if (department.size()>0) {
					userWorkSpaces.setWorkspaceName(department.get(0).getDepartmentTypeName());
				} else
					userWorkSpaces.setWorkspaceName(null);
				break;
			default:
				break;
			}
			return userWorkSpaces.getWorkspaceName();
		}

		else
			return null;
	}

	public String getOrganizationName(UserWorkspaces userWorkspaces) {

		List<Organizations> organization = organizationRepository.findByOrganizationId(userWorkspaces.getOrganizationId());

		if (organization.size() > 0)
			return organization.get(0).getName();
		else
			return null;

	}

	public List<Businesses> getWorksites(UserWorkspaces userWorkspaces) {

		return businessesRepository.findBusinessByBusinessId(userWorkspaces.getBusinessId());
	}
	
	public Businesses getBusiness(UserWorkspaces userWorkspaces) {

		Optional<Businesses> businessOptional =  businessesRepository.findById(userWorkspaces.getBusinessId());
		
		if(businessOptional.isPresent())
			return businessOptional.get();
		else
			return null;
	}

	public Iterable<UserBasicInformation> getColleagues(UserWorkspaces userWorkspaces) {
		
		return userBasicInformationRepositoryDAO.findColleguesByWorksiteId(userWorkspaces.getUserId() ,userWorkspaces.getBusinessId());
	}
	
	public String getStaffUserCoreTypeName (UserWorkspaces userWorkspaces) {
		
		Optional<StaffUserCoreTypes> staffUserCoreTypesOptional = staffUserCoreTypesRepository.findById(userWorkspaces.getStaffUserCoreTypeId());
		
		if (staffUserCoreTypesOptional.isPresent())
			return staffUserCoreTypesRepository.findStaffUserCoreTypesById(userWorkspaces.getStaffUserCoreTypeId()).getLabel();
		else
			return null;
	}
	
	public String getDepartmentShiftName (UserWorkspaces userWorkspaces) {
		
		/*List<DepartmentShifts> departmentShiftsOptional = ( List<DepartmentShifts>) departmentShiftsRepository.findAllById(userWorkspaces.getDepartmentShiftIds());
		
		if (departmentShiftsOptional!=null)
			return departmentShiftsOptional.stream().map(DepartmentShifts::getLabel).collect(Collectors.joining(", "));
		else
			return null;*/
		return null;
	}
	
	public long getDepartmentCount (UserWorkspaces userWorkspaces) {
		
		long count = userWorkspacesRepository.countByUserIdAndWorkspaceId(userWorkspaces.getUserId(), userWorkspaces.getWorkspaceId());
		
		return count;
	}
	
	public UserBasicInformation getUserInformation (UserWorkspaces userWorkspaces) {
		
		Optional<UserBasicInformation> userOptional = userBasicInformationRepository.findById(userWorkspaces.getUserId());
		
		if (userOptional.isPresent())
			return userOptional.get();
		else
			return null;
		
	}
	
	public String getStaffUserSubCoreTypeName(UserWorkspaces userWorkspaces) {
		
		Optional<UserWorkspaces> userWorkspaceOpt = userWorkspacesRepository.findById(userWorkspaces.getUserWorkspaceId());
		
		if (userWorkspaceOpt.isPresent()) {
			StaffUserCoreTypes staffRole = staffUserCoreTypesRepository.findStaffUserCoreTypesById(userWorkspaceOpt.get().getStaffUserSubCoreTypeId());
			if (staffRole != null)
				return staffRole.getLabel();
			else
				return null;
		}
		else
			return null;
	}
	
	public Organizations getOrganizationDetails (UserWorkspaces userWorkspaces) {
		
		Optional<Organizations> organizationOptional = organizationRepository.findById(userWorkspaces.getOrganizationId());
		
		if (organizationOptional.isPresent())
			return organizationOptional.get();
		else
			return null;
	}
	
	public String getShiftTime (UserWorkspaces userWorkspaces, long departmentId, String date) {
		
		Schedules schedule = schedulesRepository.findSchedulesByUserIdAndShiftDateAndDepartmentIdAndStatus(userWorkspaces.getUserId(), LocalDate.parse(date), departmentId, "Active");
		
		if (schedule != null)
			return schedule.getStartTime()+"-"+schedule.getEndTime();
		else
			return null;
	}
	
	
	public String getLastFlex (UserWorkspaces userWorkspace, long departmentId) {
		 
		long userId = userWorkspace.getUserId();
		
		List<Schedules> schedules = scheduleDAO.findUserScheduleByUserIdAndDepartmentIdAndEventType(userId, departmentId, "flexedOff", "Cancelled");

		if (schedules.size() > 0) {
			String date = schedules.get(schedules.size()-1).getShiftDate().getMonthValue()+"/"+schedules.get(schedules.size()-1).getShiftDate().getDayOfMonth();
			return date;
		}
		else
			return "-";
	}
	
	public String getDistance (UserWorkspaces userWorkspaces, long clientId) {
		
		Household household = clientsDAO.getHouseholdByClientId(clientId);
		
		UserBasicInformation userInfo = userBasicInformationRepository.findUsersByUserId(userWorkspaces.getUserId());
		
		if (userInfo != null && household != null) {
			
			if (userInfo.getLatitude() != null && userInfo.getLongitude() != null && household.getLatitude() != null && household.getLongitude() != null) {
			
				double distance = distance(Double.parseDouble(userInfo.getLatitude()), Double.parseDouble(household.getLatitude()),
						Double.parseDouble(userInfo.getLongitude()), Double.parseDouble(household.getLongitude()));
				return distance/(1.609344)+"mi"; // converting distance from miles
			}else
				return "-";
		}else
			return "-";
	}
	
	public String getStatusByWorkspaces (UserWorkspaces userWorkspaces) {
		
		List<UserCredentials> userCredential = null;
		Optional<UserBasicInformation> user = userBasicInformationRepository.findById(userWorkspaces.getUserId());

		if (userWorkspaces.isJoined() == false) {
			return "Pending Workspace";
		}
		else if (user.isPresent() && user.get().getStatus().equalsIgnoreCase("Inactive")) {
			return "Inactive";
		}
		else {
			//userCredential = userCredentialsDAO.findByUserIdAndIsPendingAndIsRequired(userWorkspaces.getUserId(), true, true); 
			
			if (user.get() != null || user.get().getUserLanguages().size() == 0 ||  
					user.get().getUserNationalProviderIdentity() == null) {
				return "Pending Profile";
			}
			else {
				return "Active";
			}
		}
	}
	
	public List<DepartmentShifts> getDepartmentShifts(UserWorkspaces userWorkspace) {
		if(userWorkspace.getDepartmentShiftIds()!=null) {
		return (List<DepartmentShifts>) departmentShiftsRepository.findAllById(userWorkspace.getDepartmentShiftIds());
		} else {
			return null;
		}
	}
}
