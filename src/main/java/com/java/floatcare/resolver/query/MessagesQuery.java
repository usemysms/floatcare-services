package com.java.floatcare.resolver.query;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.java.floatcare.dao.MessagesDAO;
import com.java.floatcare.model.DeletedMessages;
import com.java.floatcare.model.GroupChats;
import com.java.floatcare.model.Messages;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.repository.DeletedMessagesRepository;
import com.java.floatcare.repository.MessagesRepository;
import com.java.floatcare.repository.UserBasicInformationRepository;

@Component
public class MessagesQuery implements GraphQLResolver<Messages>{

	@Autowired
	private UserBasicInformationRepository userRepository;
	@Autowired
	private MessagesRepository messageRepository;
	@Autowired
	private DeletedMessagesRepository deletedMessageRepository;
	@Autowired
	private MongoTemplate mongoTemplate;
	@Autowired
	private MessagesDAO messageDAO;
	
	
	public UserBasicInformation getSenderInformation (Messages message) {
		
		Optional<UserBasicInformation> senderInformation = userRepository.findById(message.getSenderId());
		
		if (senderInformation.isPresent())
			return senderInformation.get();
		else
			return null;
	}
	
	public UserBasicInformation getReceiverInformation (Messages message) {
		
		Optional<UserBasicInformation> receiverInformation = userRepository.findById(message.getReceiverId());
		
		if (receiverInformation.isPresent())
			return receiverInformation.get();
		else
			return null;
	}
	
	public int getUnreadMessagesCount (Messages message, int userId) {
	
		int count = 0;
		
		Long sender = message.getSenderId();
		Long receiver = message.getReceiverId();

		int senderId = (sender != null) ? (int)Long.parseLong(sender.toString()) : 0;
		int receiverId =  receiver != null ? (int)Long.parseLong(receiver.toString()) : (int)Long.parseLong(message.getGroupId().toString());

		if (senderId > 0 && senderId == userId) {

			DeletedMessages deletedMessages = null;
			DeletedMessages deletedGroupMessages = null;

			if (receiver != null && message.getGroupId() == 0) {

				deletedMessages = deletedMessageRepository.findByUserIdAndReceiverMessagesDeleteduserId(senderId, receiverId);

				if (deletedMessages != null) {

					count = messageRepository.findMessagesBySenderIdAndReceiverIdAndIsSeenAndMessageId(receiverId, senderId, false, 
							deletedMessages.getMessageIds()).size();

				}else {
					count = messageRepository.findMessagesBySenderIdAndReceiverIdAndIsSeen(receiverId, senderId, false).size();
				}
				
				return count;
			}
			else if (senderId > 0 && receiver == null && message.getGroupId() != null && message.getGroupId() > 0) {
				deletedGroupMessages = deletedMessageRepository.findByUserIdAndGroupId(senderId, receiverId);
				
				if (deletedGroupMessages != null)
					count = messageDAO.findAllByGroupIdAndSenderIdAndMessageIds(message.getGroupId(), userId,
							deletedGroupMessages.getMessageIds()).size();
				else
					count = messageDAO.findAllByGroupIdAndSenderIdAndMessageIds(message.getGroupId(), userId,
							null).size();
				return count;
			}
			else
				return 0;
		}else {

			DeletedMessages deletedMessages = null;
			DeletedMessages deletedGroupMessages = null;

			if (message.getReceiverId() != null && message.getGroupId() == 0) {

				deletedMessages = deletedMessageRepository.findByUserIdAndReceiverMessagesDeleteduserId(receiverId, senderId);
				
				if (deletedMessages != null) {
					count = messageRepository.findMessagesBySenderIdAndReceiverIdAndIsSeenAndMessageId(senderId, receiverId, false,
							deletedMessages.getMessageIds()).size();
				}
				else
					count = messageRepository.findMessagesBySenderIdAndReceiverIdAndIsSeen(senderId, receiverId, false).size();
			}
			else if (message.getGroupId() > 0) {
				deletedGroupMessages = deletedMessageRepository.findByUserIdAndGroupId(senderId, receiverId);
				
				if (deletedGroupMessages != null)
					count = messageDAO.findAllByGroupIdAndSenderIdAndMessageIds(message.getGroupId(), userId,
						deletedGroupMessages.getMessageIds()).size();
				else
					count = messageDAO.findAllByGroupIdAndSenderIdAndMessageIds(message.getGroupId(), userId,
							null).size();
			}

			return count;
		}
	}

	public GroupChats getGroupInformation (Messages message) {

		if (message.getGroupId() > 0) {

			return mongoTemplate.findOne(new org.springframework.data.mongodb.core.query.Query().addCriteria(Criteria.where("chatGroupId")
					.is(message.getGroupId())), GroupChats.class);
		}else
			return null;
	}
}