package com.java.floatcare.resolver.query;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.java.floatcare.model.Businesses;
import com.java.floatcare.model.Department;
import com.java.floatcare.model.StaffWeekOffs;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.repository.BusinessesRepository;
import com.java.floatcare.repository.DepartmentRepository;
import com.java.floatcare.repository.UserBasicInformationRepository;

@Component
public class StaffWeekOffsQuery implements GraphQLResolver<StaffWeekOffs>{

	@Autowired
	private DepartmentRepository departmentRepository;
	@Autowired
	private BusinessesRepository businessesRepository;
	@Autowired
	private UserBasicInformationRepository userRepository;
	
	public Businesses getWorksiteDetails (StaffWeekOffs staffWeekOffs) {
		
		Optional<Businesses> businessOptional = businessesRepository.findById(staffWeekOffs.getWorksiteId());
		
		if (businessOptional.isPresent())
			return businessOptional.get();
		else
			return null;
	}
	
	public Department getDepartmentDetails (StaffWeekOffs staffWeekOffs) {
		
		Optional<Department> departmentOptional = departmentRepository.findById(staffWeekOffs.getDepartmentId());
		
		if (departmentOptional.isPresent())
			return departmentOptional.get();
		else
			return null;
	}
	
	public UserBasicInformation getUserDetails (StaffWeekOffs staffWeekOffs) {
	
		Optional<UserBasicInformation> useroOptional = userRepository.findById(staffWeekOffs.getUserId());
		
		if (useroOptional.isPresent())
			return useroOptional.get();
		else
			return null;
		
	}
	
}
