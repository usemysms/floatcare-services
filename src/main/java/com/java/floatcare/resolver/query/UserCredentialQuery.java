package com.java.floatcare.resolver.query;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.java.floatcare.model.CredentialStatus;
import com.java.floatcare.model.CredentialTypes;
import com.java.floatcare.model.CredentialSubTypes;
import com.java.floatcare.model.UserCredentials;
import com.java.floatcare.repository.CredentialStatusRepository;
import com.java.floatcare.repository.CredentialSubTypeRepository;
import com.java.floatcare.repository.CredentialTypesRepository;

@Component
public class UserCredentialQuery implements GraphQLResolver<UserCredentials> {

	@Autowired
	private CredentialTypesRepository credentialTypesRepository;
	@Autowired
	private CredentialStatusRepository credentialStatusRepository;
	@Autowired
	private CredentialSubTypeRepository credentialSubTypeRepository;

	public String getCredentialType(UserCredentials userCredentials) {

		Optional<CredentialTypes> credentialTypesOptional = credentialTypesRepository
				.findByCredentialTypeId(userCredentials.getCredentialTypeId());

		if (credentialTypesOptional.isPresent())
			return credentialTypesOptional.get().getLabel();
			
		return null;

	}

	public String getCredentialStatus(UserCredentials userCredentials) {
		
		Optional<CredentialStatus> credentialStatusOptional = credentialStatusRepository
				.findCredentialStatusByCredentialStatusId(userCredentials.getCredentialStatusId());

		if (credentialStatusOptional.isPresent())
			return credentialStatusOptional.get().getLabel();
		
		return null;
	}
	
	public String getCredentialSubTypeName(UserCredentials userCredentials) {

		Optional<CredentialSubTypes> credentialSubTypeOptional = credentialSubTypeRepository
				.findCredentialSubTypesByCredentialSubTypeId(userCredentials.getCredentialSubTypeId());

		if (credentialSubTypeOptional.isPresent())
			return credentialSubTypeOptional.get().getLabel();
		
		return null;
	}
}