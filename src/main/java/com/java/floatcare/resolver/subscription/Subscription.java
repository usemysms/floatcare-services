package com.java.floatcare.resolver.subscription;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.reactivestreams.Publisher;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLSubscriptionResolver;
import com.java.floatcare.dao.CountersDAO;
import com.java.floatcare.dao.MessagesDAO;
import com.java.floatcare.dao.RequestConversationDAO;
import com.java.floatcare.model.Messages;
import com.java.floatcare.model.RequestConversation;
import com.java.floatcare.repository.MessagesRepository;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Observable;
import io.reactivex.observables.ConnectableObservable;

@Component
public class Subscription implements GraphQLSubscriptionResolver, DisposableBean {

	@Autowired
	private CountersDAO sequence;
	@Autowired
	private MessagesRepository messagesRepository;
	@Autowired
	private MessagesDAO messagesDAO;
	@Autowired
	private RequestConversationDAO requestConversationDAO;

	public Publisher<Messages> displayLatestMessage() {

		Observable<Messages> observable = Observable.create(e -> {
			
			ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(1);
			scheduledExecutorService.scheduleAtFixedRate(() -> {
				List<Messages> messageList = messagesRepository.findMessagesByMessageId((sequence.getNextSequenceValue("messageId")) - 1);
				
				e.onNext(messageList.get(0));
			}, 0, 2, TimeUnit.SECONDS);
		});

		ConnectableObservable<Messages> connectableObservable = observable.share().publish();
		connectableObservable.connect();

		return connectableObservable.toFlowable(BackpressureStrategy.BUFFER);
	}

	public Publisher<List<Messages>> displayAllMessagesOfUser(long senderId, long receiverId) {

		Observable<List<Messages>> observable = Observable.create(e -> {

			ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(2);
			scheduledExecutorService.scheduleAtFixedRate(() -> {
				List<Messages> messageList = messagesDAO.findAllMessagesBySenderIdAndReceiverId(senderId, receiverId);
				messageList.sort(new SortMessages());
				e.onNext(messageList);
			}, 0, 2, TimeUnit.SECONDS);
		});
		
		ConnectableObservable<List<Messages>> connectableObservable = observable.share().publish();
		connectableObservable.connect();

		return connectableObservable.toFlowable(BackpressureStrategy.BUFFER);
	}
	
	public Publisher<List<Messages>> displayAllMessages(long senderId) {
		
		Observable<List<Messages>> observable = Observable.create(e -> {

			ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(1);
			scheduledExecutorService.scheduleAtFixedRate(() -> {
				
				TreeSet<Long> receiverIds = new TreeSet<>(); 		// collects list of receiverId without any duplicate values
				List<Messages> displayMessage = new ArrayList<>(); // collects all the messages between senderId and all receiverId's
				List<Messages> finalMessages = new ArrayList<>(); // displays the latest messages of receiverId

//				receiverIds.addAll(messagesDAO.findReceiverIdsBySenderId(senderId));	// collects all receiver id's												
				
				for (Long receiverId : receiverIds) {
					
					displayMessage.addAll((messagesDAO.findLatestMessagesBySenderIdAndReceiverId(senderId, receiverId)));
					displayMessage.addAll((messagesDAO.findLatestMessagesBySenderIdAndReceiverId(receiverId,senderId)));   //add all the messages in the list
					displayMessage.sort(new SortMessages());
					finalMessages.add(displayMessage.get(displayMessage.size() - 1));
					displayMessage.clear();
				}
				
				finalMessages.sort(new SortMessages());
				e.onNext(finalMessages);
			}, 0, 5, TimeUnit.SECONDS);
		});

		ConnectableObservable<List<Messages>> connectableObservable = observable.share().publish();
		connectableObservable.connect();

		return connectableObservable.toFlowable(BackpressureStrategy.BUFFER);
	}

	public Publisher<List<RequestConversation>> displayMessagesOfSenderAndReceiver(long senderId, long receiverId) {

		Observable<List<RequestConversation>> observable = Observable.create(e -> {
			ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(1);
			scheduledExecutorService.scheduleAtFixedRate(() -> {
				List<RequestConversation> messageList = requestConversationDAO.findMessagesBySenderIdAndReceiverId(senderId, receiverId);
				messageList.sort(new SortRequestMessages());
				e.onNext(messageList);
			}, 0, 2, TimeUnit.SECONDS);
		});

		ConnectableObservable<List<RequestConversation>> connectableObservable = observable.share().publish();
		connectableObservable.connect();

		return connectableObservable.toFlowable(BackpressureStrategy.BUFFER);

	}

	@Override
	public void destroy() throws Exception {
		destroy();
	}
}

class SortMessages implements Comparator<Messages> {

	@Override
	public int compare(Messages o1, Messages o2) {

		if (o1.getMessageId() > o2.getMessageId())
			return 1;
		else if (o1.getMessageId() < o2.getMessageId())
			return -1;
		else
			return 0;
	}
}

class SortRequestMessages implements Comparator<RequestConversation> {

	@Override
	public int compare(RequestConversation o1, RequestConversation o2) {

		if (o1.getRequestConversationId() > o2.getRequestConversationId())
			return 1;
		else if (o1.getRequestConversationId() < o2.getRequestConversationId())
			return -1;
		else
			return 0;
	}
}