package com.java.floatcare.api.request.pojo;

import java.util.ArrayList;
import java.util.List;

import com.java.floatcare.model.OpenShiftInvitees;
import com.java.floatcare.model.OpenShifts;
import com.java.floatcare.model.Requests;

public class PendingRequests {

	private String date;
	private List<Requests> calloffs = new ArrayList<>();
	private List<OpenShiftInvitees> openshiftInvitees = new ArrayList<>();
	private OpenShifts openShift;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public List<Requests> getCalloffs() {
		return calloffs;
	}
	public void setCalloffs(List<Requests> calloffs) {
		this.calloffs = calloffs;
	}

	public List<OpenShiftInvitees> getOpenshiftInvitees() {
		return openshiftInvitees;
	}

	public void setOpenshiftInvitees(List<OpenShiftInvitees> openshiftInvitees) {
		this.openshiftInvitees = openshiftInvitees;
	}

	public OpenShifts getOpenShift() {
		return openShift;
	}

	public void setOpenShift(OpenShifts openShift) {
		this.openShift = openShift;
	}

	@Override
	public String toString() {
		return "PendingRequests [date=" + date + ", calloffs=" + calloffs + ", openshiftInvitees=" + openshiftInvitees
				+ ", openShift=" + openShift + "]";
	}
}
