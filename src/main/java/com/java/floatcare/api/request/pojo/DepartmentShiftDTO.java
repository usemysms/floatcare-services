package com.java.floatcare.api.request.pojo;

import java.time.LocalDate;
import java.time.LocalTime;

public class DepartmentShiftDTO {

	private long id;
	private LocalTime startTime;
	private LocalTime endTime;
	private LocalTime shiftStartTime;
	private LocalTime shiftEndTime;
	private LocalDate shiftDate;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public LocalTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalTime startTime) {
		this.startTime = startTime;
	}

	public LocalTime getEndTime() {
		return endTime;
	}

	public void setEndTime(LocalTime endTime) {
		this.endTime = endTime;
	}

	public LocalTime getShiftStartTime() {
		return shiftStartTime;
	}

	public void setShiftStartTime(LocalTime shiftStartTime) {
		this.shiftStartTime = shiftStartTime;
	}

	public LocalTime getShiftEndTime() {
		return shiftEndTime;
	}

	public void setShiftEndTime(LocalTime shiftEndTime) {
		this.shiftEndTime = shiftEndTime;
	}

	public LocalDate getShiftDate() {
		return shiftDate;
	}

	public void setShiftDate(LocalDate shiftDate) {
		this.shiftDate = shiftDate;
	}

	@Override
	public String toString() {
		return "DepartmentShiftDTO [id=" + id + ", startTime=" + startTime + ", endTime=" + endTime
				+ ", shiftStartTime=" + shiftStartTime + ", shiftEndTime=" + shiftEndTime + ", shiftDate=" + shiftDate
				+ "]";
	}
}