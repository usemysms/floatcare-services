package com.java.floatcare.api.request.pojo;

public class SuperAdminNotifyRequest {

	private long organizationId;
	private long staffId;
	private long accountOwnerId;

	public long getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(long organizationId) {
		this.organizationId = organizationId;
	}

	public long getStaffId() {
		return staffId;
	}

	public void setStaffId(long staffId) {
		this.staffId = staffId;
	}

	public long getAccountOwnerId() {
		return accountOwnerId;
	}

	public void setAccountOwnerId(long accountOwnerId) {
		this.accountOwnerId = accountOwnerId;
	}
}