package com.java.floatcare.api.request.pojo;

public class PermissionSet {

	private int featureId;
	private Permissions permissions;

	public int getFeatureId() {
		return featureId;
	}

	public void setFeatureId(int featureId) {
		this.featureId = featureId;
	}

	public Permissions getPermissions() {
		return permissions;
	}

	public void setPermissions(Permissions permissions) {
		this.permissions = permissions;
	}

	@Override
	public String toString() {
		return "PermissionSet [featureId=" + featureId + ", permissions=" + permissions + "]";
	}
}

class Permissions {

	private Boolean read;
	private Boolean create;
	private Boolean edit;
	private Boolean delete;

	public Boolean getRead() {
		return read;
	}

	public void setRead(Boolean read) {
		this.read = read;
	}

	public Boolean getCreate() {
		return create;
	}

	public void setCreate(Boolean create) {
		this.create = create;
	}

	public Boolean getEdit() {
		return edit;
	}

	public void setEdit(Boolean edit) {
		this.edit = edit;
	}

	public Boolean getDelete() {
		return delete;
	}

	public void setDelete(Boolean delete) {
		this.delete = delete;
	}

	@Override
	public String toString() {
		return "Permissions [read=" + read + ", create=" + create + ", edit=" + edit + ", delete=" + delete + "]";
	}
}
