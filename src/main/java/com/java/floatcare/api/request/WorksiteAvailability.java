package com.java.floatcare.api.request;

public class WorksiteAvailability {

	private String weekDay;
	private TimeObject startAndEndTime;

	public String getWeekDay() {
		return weekDay;
	}

	public void setWeekDay(String weekDay) {
		this.weekDay = weekDay;
	}

	public TimeObject getStartAndEndTime() {
		return startAndEndTime;
	}

	public void setStartAndEndTime(TimeObject startAndEndTime) {
		this.startAndEndTime = startAndEndTime;
	}

}

class TimeObject {

	private String startTime;
	private String endTime;

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

}