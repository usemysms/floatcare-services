package com.java.floatcare.api.request.pojo;

public class RequestSchedulesByDate {

	private String onDate;
	private long departmentId;
	private long staffCoreType;
	
	public long getStaffCoreType() {
		return staffCoreType;
	}

	public void setStaffCoreType(long staffCoreType) {
		this.staffCoreType = staffCoreType;
	}

	public String getOnDate() {
		return onDate;
	}

	public void setOnDate(String onDate) {
		this.onDate = onDate;
	}

	public long getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(long departmentId) {
		this.departmentId = departmentId;
	}

}
