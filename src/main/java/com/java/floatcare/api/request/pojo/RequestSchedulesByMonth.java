package com.java.floatcare.api.request.pojo;

public class RequestSchedulesByMonth {

	private long staffCoreType;
	private int month;
	private int year;
	private long departmentId;

	public long getStaffCoreType() {
		return staffCoreType;
	}

	public void setStaffCoreType(long staffCoreType) {
		this.staffCoreType = staffCoreType;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public long getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(long departmentId) {
		this.departmentId = departmentId;
	}

}
