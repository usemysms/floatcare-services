package com.java.floatcare.api.request.pojo;

import java.util.List;

public class UsersEmailRequest {

	private List<String> emails;

	public List<String> getEmails() {
		return emails;
	}

	public void setEmails(List<String> emails) {
		this.emails = emails;
	}
}
