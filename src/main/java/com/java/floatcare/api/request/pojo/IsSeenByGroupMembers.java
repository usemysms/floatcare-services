package com.java.floatcare.api.request.pojo;

public class IsSeenByGroupMembers {

	private long userId;
	private String timeStamp;

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	@Override
	public String toString() {
		return "IsSeenByGroupMembers [userId=" + userId + ", timeStamp=" + timeStamp + "]";
	}
}