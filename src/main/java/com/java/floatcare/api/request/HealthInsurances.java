package com.java.floatcare.api.request;

public class HealthInsurances {

	private int id;
	private String label;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	@Override
	public String toString() {
		return "HealthInsurances [id=" + id + ", label=" + label + "]";
	}
}