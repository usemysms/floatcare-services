package com.java.floatcare.api.request.pojo;

public class EmailWithOrganiationVerificationRequest {
	String email;
	long organizationId;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public long getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(long organizationId) {
		this.organizationId = organizationId;
	}
	
	
	

}
