package com.java.floatcare.api.request.pojo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class UserVerification implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3324578453702120903L;
	private String email;
	private long departmentId;
	private long userId;
}
