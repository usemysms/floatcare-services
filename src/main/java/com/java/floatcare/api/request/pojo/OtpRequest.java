package com.java.floatcare.api.request.pojo;

public class OtpRequest {
	long userId;
	String otpString;
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getOtpString() {
		return otpString;
	}
	public void setOtpString(String otpString) {
		this.otpString = otpString;
	}
	@Override
	public String toString() {
		return "OtpRequest [userId=" + userId + ", otpString=" + otpString + "]";
	}
	
}
