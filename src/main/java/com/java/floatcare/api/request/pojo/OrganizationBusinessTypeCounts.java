package com.java.floatcare.api.request.pojo;

public class OrganizationBusinessTypeCounts {

	private long businessTypeCount;
	private String businessTypeName;
	
	public OrganizationBusinessTypeCounts() {
	
	}

	public long getBusinessTypeCount() {
		return businessTypeCount;
	}

	public void setBusinessTypeCount(long businessTypeCount) {
		this.businessTypeCount = businessTypeCount;
	}

	public String getBusinessTypeName() {
		return businessTypeName;
	}

	public void setBusinessTypeName(String businessTypeName) {
		this.businessTypeName = businessTypeName;
	}
	}
