package com.java.floatcare.api.request.pojo;

public class DepartmentsList {

	private long organizationId;
	private long worksiteId;
	private long departmentId;

	public long getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(long organizationId) {
		this.organizationId = organizationId;
	}

	public long getWorksiteId() {
		return worksiteId;
	}

	public void setWorksiteId(long worksiteId) {
		this.worksiteId = worksiteId;
	}

	public long getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(long departmentId) {
		this.departmentId = departmentId;
	}
}
