package com.java.floatcare.api.request.pojo;

public class UserAvailability {

	private String weekDay;
	private String type;

	public String getWeekDay() {
		return weekDay;
	}

	public void setWeekDay(String weekDay) {
		this.weekDay = weekDay;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "UserAvailability [weekDay=" + weekDay + ", type=" + type + "]";
	}

}