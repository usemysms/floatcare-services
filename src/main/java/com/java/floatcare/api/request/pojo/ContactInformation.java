package com.java.floatcare.api.request.pojo;

public class ContactInformation {
	
	private String email;
	private String phoneNumber;
	private String faxNumber;
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getFaxNumber() {
		return faxNumber;
	}
	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}
	
	@Override
	public String toString() {
		return "ContactInformation [email=" + email + ", phoneNumber=" + phoneNumber + ", faxNumber=" + faxNumber + "]";
	}
	
	

}
