package com.java.floatcare.api.request.pojo;

public class ForgotPasswordRequest {

	private String newPassword;
	private String otp;

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}
}
