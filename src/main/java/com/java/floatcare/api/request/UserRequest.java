package com.java.floatcare.api.request;

import java.util.List;
import java.util.Map;

import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.model.UserWorkspaces;

public class UserRequest {

	private Map<Long, UserBasicInformation> userBasicMap;
	private List<UserWorkspaces> userWorkspaces;

	public UserRequest(Map<Long, UserBasicInformation> userBasicMap, List<UserWorkspaces> userWorkspaces) {

		this.userBasicMap = userBasicMap;
		this.userWorkspaces = userWorkspaces;
	}

	public Map<Long, UserBasicInformation> getUserBasicMap() {
		return userBasicMap;
	}

	public void setUserBasicMap(Map<Long, UserBasicInformation> userBasicMap) {
		this.userBasicMap = userBasicMap;
	}

	public List<UserWorkspaces> getUserWorkspaces() {
		return userWorkspaces;
	}

	public void setUserWorkspaces(List<UserWorkspaces> userWorkspaces) {
		this.userWorkspaces = userWorkspaces;
	}

	@Override
	public String toString() {
		return "UserRequest [userBasicMap=" + userBasicMap + ", userWorkspaces=" + userWorkspaces + "]";
	}
}
