package com.java.floatcare.api.request.pojo;

public class LoginUser {
	
	String email;
	String password;
	String deviceType;
	String uuid;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	@Override
	public String toString() {
		return "LoginUser [email=" + email + ", password=" + password + ", deviceType=" + deviceType + ", uuid=" + uuid
				+ "]";
	}
	
}
