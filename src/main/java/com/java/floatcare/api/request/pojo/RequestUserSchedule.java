package com.java.floatcare.api.request.pojo;

import java.util.ArrayList;
import java.util.List;

public class RequestUserSchedule {

	private long userId;
	private String startDate;
	private String endDate;
	private List<String> eventType = new ArrayList<String>();
	private List<Long> workspaces = new ArrayList<Long>();

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public List<String> getEventType() {
		return eventType;
	}

	public void setEventType(List<String> eventTypes) {
		this.eventType.addAll(eventTypes);
	}

	public List<Long> getWorkspaces() {
		return workspaces;
	}

	public void setWorkspaces(List<Long> workspaceIds) {
		this.workspaces.addAll(workspaceIds);
	}

}