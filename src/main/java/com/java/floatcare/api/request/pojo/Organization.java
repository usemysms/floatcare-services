package com.java.floatcare.api.request.pojo;

public class Organization {

	public String organizationName;
	private String businessLocation;
	private String city;
	private String zip;
	private String state;
	private String department;
	private String firstName;
	private String lastName;
	private String position;
	private String email;
	private String phone;
	private String shiftType;

	public Organization(String organizationName, String businessLocation, String city, String zip, String state,
			String department, String firstName, String lastName, String position, String email, String phone,
			String shiftType) {
		super();
		this.organizationName = organizationName;
		this.businessLocation = businessLocation;
		this.city = city;
		this.zip = zip;
		this.state = state;
		this.department = department;
		this.firstName = firstName;
		this.lastName = lastName;
		this.position = position;
		this.email = email;
		this.phone = phone;
		this.shiftType = shiftType;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public String getBusinessLocation() {
		return businessLocation;
	}

	public void setBusinessLocation(String businessLocation) {
		this.businessLocation = businessLocation;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getShiftType() {
		return shiftType;
	}

	public void setShiftType(String shiftType) {
		this.shiftType = shiftType;
	}

	@Override
	public String toString() {
		return "Organization [organizationName=" + organizationName + ", businessLocation=" + businessLocation
				+ ", city=" + city + ", zip=" + zip + ", state=" + state + ", department=" + department + ", firstName="
				+ firstName + ", lastName=" + lastName + ", position=" + position + ", email=" + email + ", phone="
				+ phone + ", shiftType=" + shiftType + "]";
	}
}
