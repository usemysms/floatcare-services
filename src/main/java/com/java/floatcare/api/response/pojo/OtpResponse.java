package com.java.floatcare.api.response.pojo;

public class OtpResponse {
	long userId;
	String message;
	String responseType;
	
	public OtpResponse(long userId, String message, String responseType) {
		super();
		this.userId = userId;
		this.message = message;
		this.responseType = responseType;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getResponseType() {
		return responseType;
	}
	public void setResponseType(String responseType) {
		this.responseType = responseType;
	}
	@Override
	public String toString() {
		return "OtpResponse [userId=" + userId + ", message=" + message + ", responseType=" + responseType + "]";
	}
	
}
