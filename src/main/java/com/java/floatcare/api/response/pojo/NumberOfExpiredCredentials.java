package com.java.floatcare.api.response.pojo;

public class NumberOfExpiredCredentials {

	private int userId;
	private int noOfExpiredCredentials;
	private String message;

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getNoOfExpiredCredentials() {
		return noOfExpiredCredentials;
	}

	public void setNoOfExpiredCredentials(int noOfExpiredCredentials) {
		this.noOfExpiredCredentials = noOfExpiredCredentials;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
