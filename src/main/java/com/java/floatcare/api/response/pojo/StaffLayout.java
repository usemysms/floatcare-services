package com.java.floatcare.api.response.pojo;

import java.util.ArrayList;
import java.util.List;

import com.java.floatcare.model.UserBasicInformation;

public class StaffLayout {

	UserBasicInformation userBasicInformation;
	List<ShiftPlanningLayout> shiftPlanningLayout = new ArrayList<ShiftPlanningLayout>();

	public UserBasicInformation getUserBasicInformation() {
		return userBasicInformation;
	}

	public void setUserBasicInformation(UserBasicInformation userBasicInformation) {
		this.userBasicInformation = userBasicInformation;
	}

	public List<ShiftPlanningLayout> getShiftPlanningLayout() {
		return shiftPlanningLayout;
	}

	public void setShiftPlanningLayout(List<ShiftPlanningLayout> shiftPlanningLayoutList) {
		shiftPlanningLayout.addAll(shiftPlanningLayoutList);
	}

}
