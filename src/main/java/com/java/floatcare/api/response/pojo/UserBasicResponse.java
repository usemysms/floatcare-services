package com.java.floatcare.api.response.pojo;

public class UserBasicResponse {

	private String firstName;
	private String lastName;
	private String coreTypeName;
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getCoreTypeName() {
		return coreTypeName;
	}
	public void setCoreTypeName(String coreTypeName) {
		this.coreTypeName = coreTypeName;
	}
	
	
}
