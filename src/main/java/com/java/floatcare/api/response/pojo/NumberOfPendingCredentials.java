package com.java.floatcare.api.response.pojo;

public class NumberOfPendingCredentials {

	private int userId;
	private int noOfPendingCredentials;
	private String message;

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getNoOfPendingCredentials() {
		return noOfPendingCredentials;
	}

	public void setNoOfPendingCredentials(int noOfPendingCredentials) {
		this.noOfPendingCredentials = noOfPendingCredentials;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
