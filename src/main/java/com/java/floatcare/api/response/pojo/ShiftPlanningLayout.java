package com.java.floatcare.api.response.pojo;

import java.time.LocalTime;
import java.util.List;

import org.joda.time.DateTime;

import com.java.floatcare.model.DepartmentShifts;
import com.java.floatcare.model.WorksiteSettings;

public class ShiftPlanningLayout {

	DateTime shiftDate;
	boolean isAssigned;
	boolean isAvailable;
	boolean openShift;
	boolean request;
	long userId;
	long departmentId;
	long organizationId;
	long worksiteId;
	String organizationName;
	String worksiteName;
	WorksiteSettings worksiteSettings;
	//List<Long> departmentShiftIds;
	List<DepartmentShifts> departmentShifts;
	String shiftTypeName;
	LocalTime startTime;
	LocalTime endTime;
	String onDate;

	public DateTime getShiftDate() {
		return shiftDate;
	}

	public void setShiftDate(DateTime shiftDate) {
		this.shiftDate = shiftDate;
	}

	public boolean isAssigned() {
		return isAssigned;
	}

	public void setAssigned(boolean isAssigned) {
		this.isAssigned = isAssigned;
	}

	public boolean isAvailable() {
		return isAvailable;
	}

	public void setAvailable(boolean isAvailable) {
		this.isAvailable = isAvailable;
	}

	public boolean isOpenShift() {
		return openShift;
	}

	public void setOpenShift(boolean openShift) {
		this.openShift = openShift;
	}

	public boolean isRequest() {
		return request;
	}

	public void setRequest(boolean request) {
		this.request = request;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(long departmentId) {
		this.departmentId = departmentId;
	}

	public WorksiteSettings getWorksiteSettings() {
		return worksiteSettings;
	}

	public void setWorksiteSettings(WorksiteSettings worksiteSettings) {
		this.worksiteSettings = worksiteSettings;
	}

	public List<DepartmentShifts> getDepartmentShifts() {
		return departmentShifts;
	}

	public void setDepartmentShifts(List<DepartmentShifts> departmentShifts) {
		this.departmentShifts = departmentShifts;
	}
	
	public String getShiftTypeName() {
		return shiftTypeName;
	}

	public void setShiftTypeName(String shiftTypeName) {
		this.shiftTypeName = shiftTypeName;
	}

	public LocalTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalTime startTime) {
		this.startTime = startTime;
	}

	public LocalTime getEndTime() {
		return endTime;
	}

	public void setEndTime(LocalTime endTime) {
		this.endTime = endTime;
	}

	public String getOnDate() {
		return onDate;
	}

	public void setOnDate(String onDate) {
		this.onDate = onDate;
	}
	
	
	@Override
	public String toString() {
		return "ShiftPlanningLayout [shiftDate=" + shiftDate + ", isAssigned=" + isAssigned + ", isAvailable="
				+ isAvailable + ", openShift=" + openShift + ", request=" + request + ", userId=" + userId
				+ ", departmentId=" + departmentId + ", organizationId=" + organizationId + ", worksiteId=" + worksiteId
				+ ", organizationName=" + organizationName + ", worksiteName=" + worksiteName + ", worksiteSettings="
				+ worksiteSettings + ", departmentShifts=" + departmentShifts + ", shiftTypeName=" + shiftTypeName
				+ ", startTime=" + startTime + ", endTime=" + endTime + ", onDate=" + onDate + "]";
	}

}
