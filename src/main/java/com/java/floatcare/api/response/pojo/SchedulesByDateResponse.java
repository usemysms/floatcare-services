package com.java.floatcare.api.response.pojo;

import java.util.List;

public class SchedulesByDateResponse {
	
	private List<ShiftwiseSchedules> shiftwiseSchedules;
	private String shiftDate;
	
	public String getShiftDate() {
		return shiftDate;
	}

	public void setShiftDate(String shiftDate) {
		this.shiftDate = shiftDate;
	}

	public List<ShiftwiseSchedules> getShiftwiseSchedules() {
		return shiftwiseSchedules;
	}

	public void setShiftwiseSchedules(List<ShiftwiseSchedules> shiftwiseSchedules) {
		this.shiftwiseSchedules = shiftwiseSchedules;
	}

}
