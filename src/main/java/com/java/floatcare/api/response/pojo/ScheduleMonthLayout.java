package com.java.floatcare.api.response.pojo;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ScheduleMonthLayout {

	private LocalDate shiftDate;
	List<WorkingStaffCountByCoreType> staffLayout = new ArrayList<WorkingStaffCountByCoreType>();
	public LocalDate getShiftDate() {
		return shiftDate;
	}
	public void setShiftDate(LocalDate shiftDate) {
		this.shiftDate = shiftDate;
	}
	public List<WorkingStaffCountByCoreType> getStaffLayout() {
		return staffLayout;
	}
	public void setStaffLayout(List<WorkingStaffCountByCoreType> staffLayout) {
		this.staffLayout = staffLayout;
	}
	
}
