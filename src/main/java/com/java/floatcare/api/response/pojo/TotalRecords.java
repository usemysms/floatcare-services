package com.java.floatcare.api.response.pojo;

import java.util.ArrayList;
import java.util.List;

import com.java.floatcare.api.request.pojo.PendingRequests;

public class TotalRecords {

	private long totalCalloffs;
	private long pendingCalloffs;
	private long totalOpenShifts;
	private long pendingOpenshifts;
	private List<PendingRequests> pendingRequests = new ArrayList<PendingRequests>();

	public long getTotalCalloffs() {
		return totalCalloffs;
	}

	public void setTotalCalloffs(long totalCalloffs) {
		this.totalCalloffs = totalCalloffs;
	}

	public long getPendingCalloffs() {
		return pendingCalloffs;
	}

	public void setPendingCalloffs(long pendingCalloffs) {
		this.pendingCalloffs = pendingCalloffs;
	}

	public long getTotalOpenShifts() {
		return totalOpenShifts;
	}

	public void setTotalOpenShifts(long totalOpenShifts) {
		this.totalOpenShifts = totalOpenShifts;
	}

	public long getPendingOpenshifts() {
		return pendingOpenshifts;
	}

	public void setPendingOpenshifts(long pendingOpenshifts) {
		this.pendingOpenshifts = pendingOpenshifts;
	}

	public List<PendingRequests> getPendingRequests() {
		return pendingRequests;
	}

	public void setPendingRequests(List<PendingRequests> pendingRequestsList) {
		pendingRequests.addAll(pendingRequestsList);
	}

}