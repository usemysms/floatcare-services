package com.java.floatcare.api.response.pojo;

import java.time.LocalDate;

public class UserNationalProviderIdentity {

	private LocalDate expirationDate;
	private LocalDate issuedDate;
	private String npiNumber;

	public LocalDate getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(LocalDate expirationDate) {
		this.expirationDate = expirationDate;
	}

	public LocalDate getIssuedDate() {
		return issuedDate;
	}

	public void setIssuedDate(LocalDate issuedDate) {
		this.issuedDate = issuedDate;
	}

	public String getNpiNumber() {
		return npiNumber;
	}

	public void setNpiNumber(String npiNumber) {
		this.npiNumber = npiNumber;
	}

	@Override
	public String toString() {
		return "UserNationalProviderIdentity [expirationDate=" + expirationDate + ", issuedDate=" + issuedDate
				+ ", npiNumber=" + npiNumber + "]";
	}
}
