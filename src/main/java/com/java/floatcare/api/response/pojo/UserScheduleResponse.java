package com.java.floatcare.api.response.pojo;

import java.util.ArrayList;
import java.util.List;

public class UserScheduleResponse {
	
	private String period;
	private String year;
	private String month;
	private String week;
	List<UserStaffLayout> staffLayout = new ArrayList<UserStaffLayout>();

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getWeek() {
		return week;
	}

	public void setWeek(String week) {
		this.week = week;
	}

	public List<UserStaffLayout> getStaffLayout() {
		return staffLayout;
	}

	public void setStaffLayout(List<UserStaffLayout> staffLayout2) {
		staffLayout.addAll(staffLayout2);
	}

}