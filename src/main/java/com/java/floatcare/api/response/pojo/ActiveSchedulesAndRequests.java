package com.java.floatcare.api.response.pojo;

import java.util.ArrayList;
import java.util.List;

import com.java.floatcare.model.Requests;
import com.java.floatcare.model.ScheduleLayout;
import com.java.floatcare.model.Schedules;

public class ActiveSchedulesAndRequests {

	private List<Schedules> activeSchedules = new ArrayList<>();
	private List<ScheduleLayout> activeScheduleLayouts = new ArrayList<>();
	private List<Requests> activeRequests = new ArrayList<>();

	public List<Schedules> getActiveSchedules() {
		return activeSchedules;
	}

	public void setActiveSchedules(List<Schedules> activeSchedules) {
		this.activeSchedules = activeSchedules;
	}

	public List<ScheduleLayout> getActiveScheduleLayouts() {
		return activeScheduleLayouts;
	}

	public void setActiveScheduleLayouts(List<ScheduleLayout> activeScheduleLayouts) {
		this.activeScheduleLayouts = activeScheduleLayouts;
	}

	public List<Requests> getActiveRequests() {
		return activeRequests;
	}

	public void setActiveRequests(List<Requests> activeRequests) {
		this.activeRequests = activeRequests;
	}
}