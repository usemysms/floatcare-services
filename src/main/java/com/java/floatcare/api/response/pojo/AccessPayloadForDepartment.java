package com.java.floatcare.api.response.pojo;

public class AccessPayloadForDepartment {
	
	private long departmentId;
	private String departmentTypeName;
	private long worksiteId;
	public long getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(long departmentId) {
		this.departmentId = departmentId;
	}
	public String getDepartmentTypeName() {
		return departmentTypeName;
	}
	public void setDepartmentTypeName(String departmentTypeName) {
		this.departmentTypeName = departmentTypeName;
	}
	public long getWorksiteId() {
		return worksiteId;
	}
	public void setWorksiteId(long worksiteId) {
		this.worksiteId = worksiteId;
	}
	
	

}
