package com.java.floatcare.api.response.pojo;

import java.util.ArrayList;
import java.util.List;

public class StaffSetSchedules {
	private long shiftTypeId;
	private List<String> staffWeekOffDays = new ArrayList<>();
	public long getShiftTypeId() {
		return shiftTypeId;
	}
	public void setShiftTypeId(long shiftTypeId) {
		this.shiftTypeId = shiftTypeId;
	}
	public List<String> getStaffWeekOffDays() {
		return staffWeekOffDays;
	}
	public void setStaffWeekOffDays(List<String> staffWeekOffDays) {
		this.staffWeekOffDays = staffWeekOffDays;
	} 
}