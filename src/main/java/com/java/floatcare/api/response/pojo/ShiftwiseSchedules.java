package com.java.floatcare.api.response.pojo;

import java.time.LocalTime;
import java.util.List;

public class ShiftwiseSchedules {
	private String shiftName;
	private String startTime;
	private String endTime;
	private List<UserBasicResponse> userList;
	private String icon;

	public String getShiftName() {
		return shiftName;
	}

	public void setShiftName(String shiftName) {
		this.shiftName = shiftName;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public List<UserBasicResponse> getUserList() {
		return userList;
	}

	public void setUserList(List<UserBasicResponse> userList) {
		this.userList = userList;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	

}


