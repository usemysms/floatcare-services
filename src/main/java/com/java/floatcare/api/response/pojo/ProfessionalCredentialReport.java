package com.java.floatcare.api.response.pojo;

public class ProfessionalCredentialReport {
	private String name;
	private String department;
	private long verified;
	private long pending;
	private long expiringSoon;
	private long expired;
	private String profilePhoto;
	private String staffUserCoreTypeName;
	private long userId;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public long getVerified() {
		return verified;
	}
	public void setVerified(long verified) {
		this.verified = verified;
	}
	public long getPending() {
		return pending;
	}
	public void setPending(long pending) {
		this.pending = pending;
	}
	public long getExpiringSoon() {
		return expiringSoon;
	}
	public void setExpiringSoon(long expiringSoon) {
		this.expiringSoon = expiringSoon;
	}
	public long getExpired() {
		return expired;
	}
	public void setExpired(long expired) {
		this.expired = expired;
	}
	public String getProfilePhoto() {
		return profilePhoto;
	}
	public void setProfilePhoto(String string) {
		this.profilePhoto = string;
	}
	public String getStaffUserCoreTypeName() {
		return staffUserCoreTypeName;
	}
	public void setStaffUserCoreTypeName(String staffUserCoreTypeName) {
		this.staffUserCoreTypeName = staffUserCoreTypeName;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	

}
