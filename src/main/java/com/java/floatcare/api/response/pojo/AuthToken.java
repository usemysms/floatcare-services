package com.java.floatcare.api.response.pojo;

import com.java.floatcare.model.UserBasicInformation;

public class AuthToken {

    private String token;
    private UserBasicInformation userBasicInformation;
    private String responseType;
	public String getToken() {
		return token;
	}
	public AuthToken(String token, UserBasicInformation userBasicInformation, String responseType) {
		super();
		this.token = token;
		this.userBasicInformation = userBasicInformation;
		this.responseType = responseType;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public UserBasicInformation getUserBasicInformation() {
		return userBasicInformation;
	}
	public void setUserBasicInformation(UserBasicInformation userBasicInformation) {
		this.userBasicInformation = userBasicInformation;
	}
	public String getResponseType() {
		return responseType;
	}
	public void setResponseType(String responseType) {
		this.responseType = responseType;
	}
	
}