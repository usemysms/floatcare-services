package com.java.floatcare.api.response.pojo;

import java.util.List;

import com.java.floatcare.model.DepartmentShifts;

public class DepartmentShiftsResponse {

	public long _id;
	public List<DepartmentShifts> departmentShifts;

	public long getId() {
		return _id;
	}

	public void setId(long _id) {
		this._id = _id;
	}

	public List<DepartmentShifts> getDepartmentShifts() {
		return departmentShifts;
	}

	public void setDepartmentShifts(List<DepartmentShifts> departmentShifts) {
		this.departmentShifts = departmentShifts;
	}

	@Override
	public String toString() {
		return "DepartmentShiftsResponse [departmentShiftResponseId=" + _id
				+ ", departmentShifts=" + departmentShifts + "]";
	}
}
