package com.java.floatcare.api.response.pojo;

public class UserLanguages {

	private String name;
	private boolean canRead;
	private boolean canWrite;
	private boolean canSpeak;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isCanRead() {
		return canRead;
	}

	public void setCanRead(boolean canRead) {
		this.canRead = canRead;
	}

	public boolean isCanWrite() {
		return canWrite;
	}

	public void setCanWrite(boolean canWrite) {
		this.canWrite = canWrite;
	}

	public boolean isCanSpeak() {
		return canSpeak;
	}

	public void setCanSpeak(boolean canSpeak) {
		this.canSpeak = canSpeak;
	}

	@Override
	public String toString() {
		return "UserLanguages [name=" + name + ", canRead=" + canRead + ", canWrite=" + canWrite + ", canSpeak="
				+ canSpeak + "]";
	}
}