package com.java.floatcare.api.response.pojo;

import java.time.LocalDate;
import java.util.List;

import com.java.floatcare.model.Schedules;

public class SetScheduleResponse {

	private long _id;
	private LocalDate endDate;
	private List<Schedules> schedules;

	public long get_id() {
		return _id;
	}

	public void set_id(long _id) {
		this._id = _id;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public List<Schedules> getSchedules() {
		return schedules;
	}

	public void setSchedules(List<Schedules> schedules) {
		this.schedules = schedules;
	}
}
