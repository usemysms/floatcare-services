package com.java.floatcare.api.response.pojo;

import java.util.ArrayList;
import java.util.List;

public class StaffGroups {
	
	String groupName;
	List<StaffLayout> staffLayout = new ArrayList<StaffLayout>();

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public List<StaffLayout> getStaffLayout() {
		return staffLayout;
	}

	public void setStaffLayout(List<StaffLayout> eachStaffLayout) {
		staffLayout.addAll(eachStaffLayout);
	}

}
