package com.java.floatcare.api.response.pojo;

public class ResponseError {
	String message;
	String formField;
	
	public ResponseError(String message, String formField) {
		super();
		this.message = message;
		this.formField = formField;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getFormField() {
		return formField;
	}

	public void setFormField(String formField) {
		this.formField = formField;
	}
	

}
