package com.java.floatcare.api.response.pojo;
public class WorkingStaffCountByCoreType {
	private String coreTypeShortName;
	private int count;
	public String getCoreTypeShortName() {
		return coreTypeShortName;
	}
	public void setCoreTypeShortName(String coreTypeShortName) {
		this.coreTypeShortName = coreTypeShortName;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
}