package com.java.floatcare.api.response.pojo;

public class RequiredType {

	private long staffCoreTypeId;
	private boolean isRequired;

	public long getStaffCoreTypeId() {
		return staffCoreTypeId;
	}

	public void setStaffCoreTypeId(long staffCoreTypeId) {
		this.staffCoreTypeId = staffCoreTypeId;
	}

	public boolean getIsRequired() {
		return isRequired;
	}

	public void setIsRequired(boolean isRequired) {
		this.isRequired = isRequired;
	}

	@Override
	public String toString() {
		return "RequiredType [staffCoreTypeId=" + staffCoreTypeId + ", isRequired=" + isRequired + "]";
	}
}
