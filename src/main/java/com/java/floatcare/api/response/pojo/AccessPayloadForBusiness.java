package com.java.floatcare.api.response.pojo;

public class AccessPayloadForBusiness {
	
	private long businessId;
	private String name;
	
	public long getBusinessId() {
		return businessId;
	}
	public void setBusinessId(long businessId) {
		this.businessId = businessId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	

}
