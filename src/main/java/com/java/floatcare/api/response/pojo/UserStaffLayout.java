package com.java.floatcare.api.response.pojo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.java.floatcare.model.UserBasicInformation;

public class UserStaffLayout {

	UserBasicInformation userBasicInformation;
	Map<String,List<ShiftPlanningLayout>> shiftPlanningLayout = new HashMap<String,List<ShiftPlanningLayout>>();

	public UserBasicInformation getUserBasicInformation() {
		return userBasicInformation;
	}

	public void setUserBasicInformation(UserBasicInformation userBasicInformation) {
		this.userBasicInformation = userBasicInformation;
	}

	public Map<String, List<ShiftPlanningLayout>> getShiftPlanningLayout() {
		return (Map<String, List<ShiftPlanningLayout>>) shiftPlanningLayout;
	}

	public void setShiftPlanningLayout(Map<String, List<ShiftPlanningLayout>> shiftPlanningLayout) {
		this.shiftPlanningLayout = shiftPlanningLayout;
	}



}
