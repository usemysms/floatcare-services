package com.java.floatcare.api.response.pojo;

import java.util.List;

public class CredentialsReportResponse {
	private long verified;
	private long pending;
	private long expiringSoon;
	private long expired;
	private long lastYearVerified;
	private long lastYearPending;
	private long lastYearExpiringSoon;
	private long lastYearExpired;
	private float verifiedDiff;
	private float pendingDiff;
	private float expiringSoonDiff;
	private float expiredDiff;
	private List<ProfessionalCredentialReport> professionalCredentialReport;
	public long getVerified() {
		return verified;
	}
	public void setVerified(long verified) {
		this.verified = verified;
	}
	public long getPending() {
		return pending;
	}
	public void setPending(long pending) {
		this.pending = pending;
	}
	public long getExpiringSoon() {
		return expiringSoon;
	}
	public void setExpiringSoon(long expiringSoon) {
		this.expiringSoon = expiringSoon;
	}
	public long getExpired() {
		return expired;
	}
	public void setExpired(long expired) {
		this.expired = expired;
	}
	public List<ProfessionalCredentialReport> getProfessionalCredentialReport() {
		return professionalCredentialReport;
	}
	public void setProfessionalCredentialReport(List<ProfessionalCredentialReport> professionalCredentialReport) {
		this.professionalCredentialReport = professionalCredentialReport;
	}
	public long getLastYearVerified() {
		return lastYearVerified;
	}
	public void setLastYearVerified(long lastYearVerified) {
		this.lastYearVerified = lastYearVerified;
	}
	public long getLastYearPending() {
		return lastYearPending;
	}
	public void setLastYearPending(long lastYearPending) {
		this.lastYearPending = lastYearPending;
	}
	public long getLastYearExpiringSoon() {
		return lastYearExpiringSoon;
	}
	public void setLastYearExpiringSoon(long lastYearExpiringSoon) {
		this.lastYearExpiringSoon = lastYearExpiringSoon;
	}
	public long getLastYearExpired() {
		return lastYearExpired;
	}
	public void setLastYearExpired(long lastYearExpired) {
		this.lastYearExpired = lastYearExpired;
	}
	public float getVerifiedDiff() {
		return verifiedDiff;
	}
	public void setVerifiedDiff(float verifiedDiff) {
		this.verifiedDiff = verifiedDiff;
	}
	public float getPendingDiff() {
		return pendingDiff;
	}
	public void setPendingDiff(float pendingDiff) {
		this.pendingDiff = pendingDiff;
	}
	public float getExpiringSoonDiff() {
		return expiringSoonDiff;
	}
	public void setExpiringSoonDiff(float expiringSoonDiff) {
		this.expiringSoonDiff = expiringSoonDiff;
	}
	public float getExpiredDiff() {
		return expiredDiff;
	}
	public void setExpiredDiff(float expiredDiff) {
		this.expiredDiff = expiredDiff;
	}
	
	
	}
