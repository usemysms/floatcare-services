package com.java.floatcare.api.response.pojo;

public class UnreadMessagesAndNotificationsCount {
    long userId;
    long unreadMessagesCount;
    long unreadNotificationsCount;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getUnreadMessagesCount() {
        return unreadMessagesCount;
    }

    public void setUnreadMessagesCount(long unreadMessagesCount) {
        this.unreadMessagesCount = unreadMessagesCount;
    }

    public long getUnreadNotificationsCount() {
        return unreadNotificationsCount;
    }

    public void setUnreadNotificationsCount(long unreadNotificationsCount) {
        this.unreadNotificationsCount = unreadNotificationsCount;
    }
}
