package com.java.floatcare.api.response.pojo;

public class ImageUploadResponse {

	private long statusCode;
	private String status;

	public long getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(long statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
