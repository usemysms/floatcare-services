package com.java.floatcare.api.response.pojo;

import java.time.LocalDate;

public class SocialSecurityNumber {

	private LocalDate expirationDate;
	private LocalDate issuedDate;
	private String socialSecurityNumber;
	private String frontSide;
	private String backSide;
	private String state;

	public LocalDate getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(LocalDate expirationDate) {
		this.expirationDate = expirationDate;
	}

	public LocalDate getIssuedDate() {
		return issuedDate;
	}

	public void setIssuedDate(LocalDate issuedDate) {
		this.issuedDate = issuedDate;
	}

	public String getSocialSecurityNumber() {
		return socialSecurityNumber;
	}

	public void setSocialSecurityNumber(String socialSecurityNumber) {
		this.socialSecurityNumber = socialSecurityNumber;
	}

	public String getFrontSide() {
		return frontSide;
	}

	public void setFrontSide(String frontSide) {
		this.frontSide = frontSide;
	}

	public String getBackSide() {
		return backSide;
	}

	public void setBackSide(String backSide) {
		this.backSide = backSide;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "SocialSecurityNumber [expirationDate=" + expirationDate + ", issuedDate=" + issuedDate
				+ ", socialSecurityNumber=" + socialSecurityNumber + ", frontSide=" + frontSide + ", backSide="
				+ backSide + ", state=" + state + "]";
	}
}
