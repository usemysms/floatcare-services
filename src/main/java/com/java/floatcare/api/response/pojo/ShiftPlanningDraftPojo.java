package com.java.floatcare.api.response.pojo;

import java.util.ArrayList;
import java.util.List;

public class ShiftPlanningDraftPojo {
	
	String period;
	List<StaffGroups> staffGroups = new ArrayList<StaffGroups>();

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public List<StaffGroups> getStaffGroups() {
		return staffGroups;
	}

	public void setStaffGroups(StaffGroups staffGroup) {
		staffGroups.add(staffGroup);
	}

}