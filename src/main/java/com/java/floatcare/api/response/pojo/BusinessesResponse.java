package com.java.floatcare.api.response.pojo;

import java.util.List;

import com.java.floatcare.model.UserWorkspaces;


public class BusinessesResponse {

    public long _id;
    public UserWorkspaces userWorkspaces;
    public List<Long> preferredProviderIds;

    public long get_Id() {
        return _id;
    }

    public void set_Id(long _id) {
        this._id = _id;
    }

    public UserWorkspaces getUserWorkspaces() {
        return userWorkspaces;
    }

    public void setUserWorkspaces(UserWorkspaces userWorkspaces) {
        this.userWorkspaces = userWorkspaces;
    }

	public long get_id() {
		return _id;
	}

	public void set_id(long _id) {
		this._id = _id;
	}

	public List<Long> getPreferredProviderIds() {
		return preferredProviderIds;
	}

	public void setPreferredProviderIds(List<Long> preferredProviderIds) {
		this.preferredProviderIds = preferredProviderIds;
	}

	@Override
	public String toString() {
		return "BusinessesResponse [_id=" + _id + ", userWorkspaces=" + userWorkspaces + ", preferredProviderIds="
				+ preferredProviderIds + "]";
	}
}