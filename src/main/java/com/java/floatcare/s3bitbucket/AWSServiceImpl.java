package com.java.floatcare.s3bitbucket;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.java.floatcare.controller.UploadCredentialDocumentController;
import com.java.floatcare.model.Businesses;
import com.java.floatcare.repository.BusinessesRepository;
import com.java.floatcare.service.ClientsServiceImpl;
import com.java.floatcare.service.OrganizationServiceImpl;
import com.java.floatcare.service.UserBasicInformationServiceImpl;
import com.java.floatcare.service.UserCredentialsServiceImpl;

@Service
public class AWSServiceImpl implements AWSService {

	private static final Logger LOGGER = LoggerFactory.getLogger(AWSServiceImpl.class);

	@Autowired
	private AmazonS3 amazonS3;
	@Value("${aws.s3.bucket}")
	private String bucketName;
	@Autowired
	private BusinessesRepository businessesRepository;
	@Autowired
	private UserBasicInformationServiceImpl userBasicInformationServiceImpl;
	@Autowired
	private OrganizationServiceImpl organizationServiceImpl;
	@Autowired
	private UserCredentialsServiceImpl userCredentialsServiceImpl;
	@Autowired
	private UploadCredentialDocumentController uploadFileController;
	@Autowired
	private ClientsServiceImpl clientsServiceImpl;
	
	private static final String businessCoverPhoto ="businessCoverPhoto";

	@Override
	// @Async annotation ensures that the method is executed in a different
	// background thread
	// but not consume the main thread.

	@Async
	public void uploadFile(final MultipartFile multipartFile,String resourceName,long resourceId) {
		LOGGER.info("File upload in progress.");
		try {
			final File file = convertMultiPartFileToFile(multipartFile);
			String uniqueFileName = file.getName()+"_"+LocalDateTime.now() + ".jpg" ;
			
			if (resourceId == 1)
				organizationServiceImpl.setUniqueFileName(uniqueFileName);
			
			if (resourceId == 2)
				userBasicInformationServiceImpl.setUniqueFileName(uniqueFileName);

			if (resourceId == 3)
				userCredentialsServiceImpl.setUniqueFileName(uniqueFileName);
			
			if (resourceId == 4) {
				uniqueFileName = LocalDateTime.now()+"_"+file.getName();
				uploadFileController.setUniqueFileName(uniqueFileName);
			}
			if (resourceId == 5) {
				uniqueFileName = LocalDateTime.now()+"_"+file.getName();
				uploadFileController.setFrontImageName(uniqueFileName);
			}
			if (resourceId == 6) {
				uniqueFileName = LocalDateTime.now()+"_"+file.getName();
				uploadFileController.setBackImageName(uniqueFileName);
			}
			
			if (resourceId == 7) {
				uniqueFileName = LocalDateTime.now()+"_"+file.getName();
				clientsServiceImpl.setUniqueFileName(uniqueFileName);
			}
			
			PutObjectResult result = null;
			
			if (resourceId > 0) {
				result = uploadFileToS3Bucket(bucketName, file, uniqueFileName);
			} else
				result = uploadFileToS3Bucket(bucketName, file, resourceName);

			System.out.println(result.getMetadata());
			if(result!=null) {
				if(resourceName.equals(businessCoverPhoto)) {
					List<Businesses> businesses = businessesRepository.findBusinessByBusinessId(resourceId);
					if(businesses.size()>0) {
						Businesses single = businesses.get(0);
						single.setCoverPhoto(uniqueFileName);
						businessesRepository.save(single);
					}
				}
			}
		} catch (final AmazonServiceException ex) {
			LOGGER.info("File upload is failed.");
			LOGGER.error("Error= {} while uploading file.", ex.getMessage());
		}
	}

	private File convertMultiPartFileToFile(final MultipartFile multipartFile) {
		final File file = new File(multipartFile.getOriginalFilename());
		try (final FileOutputStream outputStream = new FileOutputStream(file)) {
			outputStream.write(multipartFile.getBytes());
		} catch (final IOException ex) {
			LOGGER.error("Error converting the multi-part file to file= ", ex.getMessage());
		}
		return file;
	}

	private PutObjectResult uploadFileToS3Bucket(final String bucketName, final File file, String uniqueFileName) {
		
		LOGGER.info("Uploading file with name= " + uniqueFileName);
		final PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, uniqueFileName, file);
		return amazonS3.putObject(putObjectRequest);
	}
}