package com.java.floatcare.s3bitbucket;

import org.springframework.web.multipart.MultipartFile;

public interface AWSService {

	void uploadFile(MultipartFile multipartFile,String resourceName, long resourceId);
}
