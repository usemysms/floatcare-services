package com.java.floatcare.communication;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import com.twilio.Twilio;
import com.twilio.exception.ApiException;
import com.twilio.rest.api.v2010.account.Message;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class TwilioCommunication {

	public static final String ACCOUNT_SID = "AC57b7c86a6f51a0e08a782c4ae5926c12";
	public static final String AUTH_TOKEN = "d45988d9b9e4a143af88ce981f4bbbec";

	static {
		Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
	}

	public String sendSMS(String to, String text) {

		try {
			Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
			Message message = Message
					.creator(new com.twilio.type.PhoneNumber(to), new com.twilio.type.PhoneNumber("+18504035115"), text)
					.create();
			log.info("OTP res :", message.getSid());
			return message.getSid();
		} catch (Exception ex) {
			log.error("Exception occurred while sending sms to phone number " + to + " {}", ex.getMessage());
		}
		return StringUtils.EMPTY;
	}

}
