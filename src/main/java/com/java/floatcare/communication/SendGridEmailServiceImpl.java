package com.java.floatcare.communication;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;
import com.sendgrid.helpers.mail.objects.Personalization;

@Service
public class SendGridEmailServiceImpl implements SendGridEmailService {

	@Autowired
	private SendGrid sendGrid;

	@Override
	public void sendText(String to, String subject, String body) {
		
		Mail mail = new Mail(new Email("hello@float.care"), subject, new Email(to), new Content("text/plain", body));
		Request request = new Request();
		Response response = null;

		try {

			request.setMethod(Method.POST);
			request.setEndpoint("mail/send");
			request.setBody(mail.build());
			response = sendGrid.api(request);

		} catch (IOException ex) {

			System.out.println(ex.getMessage());
		}
		
		if (response != null)
			System.out.println("Status Code: " + response.getStatusCode() + ", Body: " + response.getBody() + ", Headers: "+ response.getHeaders());
	}

	@Override
	public void sendHTML(String to, String subject, String body, String dynamicName, String departmentDynamicName, String dynamicBusinessName, String template) throws IOException {

		Mail mail = new Mail();
		
	    Email fromAddress = new Email();
	    fromAddress.setEmail("hello@float.care");
	    mail.setFrom(fromAddress);

	    Personalization personalization = new Personalization();

	    Email toAddress = new Email();
	    toAddress.setEmail(to);
	    personalization.addTo(toAddress);

	    personalization.addDynamicTemplateData("name",dynamicName);
	    
	    if (departmentDynamicName != null)
	    	personalization.addDynamicTemplateData("departmentName",departmentDynamicName);
	    
	    if (dynamicBusinessName != null)
	    	personalization.addDynamicTemplateData("businessName", dynamicBusinessName);

	    mail.addPersonalization(personalization);

	    Content content = new Content();
	    content.setType("text/html");
	    content.setValue(" ");
	    mail.addContent(content);

	    mail.setTemplateId(template);

	    Request request = new Request();
	    try {
	        request.setMethod(Method.POST);
	        request.setEndpoint("mail/send");
	        request.setBody(mail.build());
	        @SuppressWarnings("unused")
			Response response = sendGrid.api(request);

	    } catch (IOException ex) {
	        throw ex;
	    }		
	}	
}