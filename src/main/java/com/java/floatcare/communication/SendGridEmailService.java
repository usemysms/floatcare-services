package com.java.floatcare.communication;

import java.io.IOException;

public interface SendGridEmailService {

	void sendText(String to, String subject, String body);

	void sendHTML(String to, String subject, String body, String dynamicName, String departmentDynamicName, String dynamicBusinessName, String template) throws IOException;
}