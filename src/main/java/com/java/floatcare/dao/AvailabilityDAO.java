package com.java.floatcare.dao;


import java.time.LocalDate;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Repository;

import com.java.floatcare.model.Availability;

@Repository
public interface AvailabilityDAO {

	List<Availability> findAvailableUsersByStartDate(long userId, LocalDate startDate);
	
	List<Availability> findAvailableUsersByOnDateAndScheduleLayoutId(long userId, LocalDate startDate, long scheduleLayoutId);

	List<Availability> findAvailableUsersByScheduleDates(long userId, LocalDate scheduleStartDate, LocalDate scheduleEndDate);

	List<Availability> findAvailableUsersByOnDate(long userId, LocalDate startDate);
	
	List<String> findAvailableUserByShiftDate(List<Long> userId, LocalDate startDate, LocalDate endDate);

	List<String> findAvailableUserByShiftDateAndScheduleLayoutId(List<Long> usersList, LocalDate localStartDate, LocalDate localEndDate, long scheduleLayoutId);

	Availability findByUserIdAndScheduleLayoutId(long userId, Long eachId);

	List<Availability> findByUserIdsAndSchedulelayoutId(Set<Long> userIds, List<Long> collect);
	
}