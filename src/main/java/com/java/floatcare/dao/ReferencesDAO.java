package com.java.floatcare.dao;

import java.util.List;

import com.java.floatcare.model.References;

public interface ReferencesDAO {

	References findReferenceById(long referenceId);

	List<References> findListOfReferencesByUserId(long userId);
}
