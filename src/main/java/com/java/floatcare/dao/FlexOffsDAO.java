package com.java.floatcare.dao;

import java.time.LocalDate;
import java.util.List;

import com.java.floatcare.model.FlexOffs;

public interface FlexOffsDAO {

	List<FlexOffs> findByWorksiteIdAndFromSelectedDate(long workspaceId, LocalDate selectedDate);
}
