package com.java.floatcare.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.java.floatcare.model.UserBasicInformation;

@Repository
public interface UserTypeDAO {

	List<UserBasicInformation> findUsersByOrganizationIdAndUserTypeId(long organizationId, long userTypeId);

	List<UserBasicInformation> findUsersByOrganizationIdAndUserTypeIdNot(long organizationId, int userTypeId);

}
