package com.java.floatcare.dao;

import java.util.List;

import com.java.floatcare.model.GroupChats;

public interface GroupChatsDAO {

	List<GroupChats> findGroupChatByUserId (long userId);

	GroupChats findGroupChatsByGroupIdAndStaffId(long groupId, long staffId);

	GroupChats findGroupChatByGroupChatId(long groupId);

	GroupChats findGroupChatsByGroupIdAndAdminIdAndStaffId(long groupId, long adminStaffId, long staffId);

	void removeGroup(long chatGroupId);
}
