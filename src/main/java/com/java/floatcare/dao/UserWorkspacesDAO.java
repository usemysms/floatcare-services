package com.java.floatcare.dao;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Repository;

import com.java.floatcare.api.response.pojo.BusinessesResponse;
import com.java.floatcare.api.response.pojo.DepartmentShiftsResponse;
import com.java.floatcare.model.UserWorkspaces;

@Repository
public interface UserWorkspacesDAO {

	UserWorkspaces findByInvitationCodeAndIsJoined (String invitationCode, boolean isJoined) throws Exception;
	
	List<Long> findByDepartmentIdAndStaffCoreTypeIdAndStatusAndIsJoined(long departmentId, long staffUserCoreTypeId, String status, boolean isJoined);
	
	Map<Long, Long> findByDepartmentId (long departmentId);

	List<UserWorkspaces> findByDepartmentIdAndStaffUserCoreTypeIdAndIsJoined(long departmentId, long staffUserCoreTypeId, boolean isJoined);

	UserWorkspaces findByDepartmentIdAndUserId (long departmentId, long userId);

	List<Long> findByStaffUserCoreTypeIdAndDepartmentId(List<Long> staffRoles, long departmentId);
	
	List<UserWorkspaces> findUserWorkspacesByStaffUserCoreTypeIdsAndDepartmentId(List<Long> staffRole, long departmentId);

	List<Long> findByUserIdAndDepartmentId(List<Long> userId, Long departmentId);
	
	List<UserWorkspaces> findByUserIdsAndDepartmentId(List<Long> userId, Long departmentId);

	List<UserWorkspaces> findByStaffUserCoreTypeIdAndUserIdAndWorkspaceId(long staffUserCoreTypeId, List<Long> usersList, long workspaceId);

	Set<Long> findByDepartmentIdAndShiftId(long departmentId, long shiftId);

	List<Long> findStaffCoreTypeIdsByWorkspaceId(long departmentId);

	List<UserWorkspaces> findByUserIdAndBusinessIds(long userId, List<Long> businessIds);

	List<UserWorkspaces> findByStaffUserCoreTypeIdAndBusinessId(long staffUserCoreTypeId, long businessId);

	List<UserWorkspaces> findByUserIds(List<Long> userIds);
	
	List<UserWorkspaces> findUserIdsByDepartmentId(long departmentId);
	
	List<UserWorkspaces> findUserIdsByDepartmentIdAndNonPrimary(long departmentId);

	List<UserWorkspaces> findUserWorkspacesByStaffUserCoreTypeIdsAndDepartmentIdAndShiftIds(List<Long> staffRoles, long departmentId, List<Long> departmentShiftIds);

	List <UserWorkspaces> findByStaffUserCoreTypeIdsAndShiftTypeAndDepartmentId(List<Long> staffCoreTypes, List<Long> shiftTypes, long departmentId);

	List<Long> findByBusinessId(long worksiteId);

	void updateStatusOfUser(long userId, String status);

	void updateStatusOfUserInWorkspace(long userId, String status, long organizationId);
	
	List<UserWorkspaces> findByStaffUserCoreTypeIdAndOrganizationId(List<Long> staffUserCoreTypeIds, long organizationId);

	void updateDepartmentShiftIdOfUsers(long oldDepartmentShiftId, long newDepartmentShiftId);

	List<DepartmentShiftsResponse> findByUserIdAndStatusAndIsJoined(long userId, String status, boolean isJoined, long departmentShiftId);

	List<UserWorkspaces> findUserIdsByOrganizationId(long worksiteId);

	List<UserWorkspaces> findAvailableUsersByWeekdays(List<String> weekDays);

	List<BusinessesResponse> findAvailableUsers(List<String> weekDay, String startTime, String endTime,
			long staffUserCoreTypeId, List<Long> userIds);

	List<Long> findByBusinessIdAndStaffUserCoreTypeId(long worksiteId, long staffUserCoreTypeId);

	List<Long> findBusinessesIdsByUserId(long userId);

}