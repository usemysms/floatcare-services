package com.java.floatcare.dao;

import java.time.LocalDate;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.java.floatcare.model.UserCredentials;

@Repository
public interface UserCredentialsDAO {

	List<UserCredentials> findUserCredentials(List<Long> userIds);

	List<UserCredentials> findUserCredentialsByExpiredDate(LocalDate checkExpiryDate);

	Iterator<UserCredentials> findUserCredentialsByCurrentDate();

	List<UserCredentials> findByUserIdAndIsPendingAndIsRequired(long userId, boolean isPending, boolean isRequired);

	List<UserCredentials> findUserCredentialsByIsRequiredAndIsPending(boolean isRequired, boolean isPending);

}
