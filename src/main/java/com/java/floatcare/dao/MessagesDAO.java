package com.java.floatcare.dao;

import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import org.springframework.stereotype.Repository;

import com.java.floatcare.model.DeletedMessages;
import com.java.floatcare.model.Messages;

@Repository
public interface MessagesDAO {

	List<Messages> findLatestMessagesBySenderIdAndReceiverId(long senderId, long receiverId);
	
	List<Messages> findReceiverIdsBySenderId(long senderId);

	List<Messages> findAllMessagesBySenderIdAndReceiverId(long senderId, long receiverId);

	List<Messages> findMessagesByReceiverId(long eachReceiverId);

	List<Messages> findTextMessagesOfUsers(List<Long> receiverIds, String text, List<Long> groupIds);

	List<Messages> findMessagesBetweenSenderIdAndReceiverId(long senderId, long receiverId);
	
	List<Long> findUsersDeletedMessagesIdsByUserId (long userId);

	DeletedMessages findDeletedMessagesBySenderAndReceiverId(long senderId, long receiverId);

	DeletedMessages findByUserIdAndReceiverMessagesDeletedUserIdAndIsReplied(Long senderId, Long receiverId, boolean isReplied);

	List<Messages> findLatestMessagesOfReceiverIdToSenderId(long receiverId, long senderId);

	Messages findLatestMessagesByGroupId(Long eachReceiverId);

	List<Messages> findAllByGroupIdAndSenderIdAndMessageIds(long groupId, long staffId, List<Long> messageIds);

	void updateIsSeenMessages(long senderId, long receiverId);

	List<DeletedMessages> findDeletedMessagesBySenderIdAndGroupId(long senderId, List<Long> groupIds);

	void findAndUpdateIsRepliedByUserIdAndGroupId(TreeSet<Long> groupMembers, Long groupId);

	boolean removeDeletedMessagesEntryByGroupId(long chatGroupId);

	Map<Long,Messages> findMessagesOfSenderId(Long senderId);
	long getUnreadMessagesCount(long userId);
}
