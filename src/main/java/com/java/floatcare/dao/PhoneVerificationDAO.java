package com.java.floatcare.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.java.floatcare.model.PhoneVerification;

@Repository
public interface PhoneVerificationDAO {

	public List<PhoneVerification> findByOtpAndPhoneNumber(String phoneNumber,String otp);
	
}
