package com.java.floatcare.dao;

import java.util.List;

import com.java.floatcare.model.Clients;
import com.java.floatcare.model.Household;

public interface ClientsDAO {

	Clients getClientProfileById(long clientId);

	List<Clients> getClientsByBusinessId(long businessId);

	Household getHouseholdByClientId(long clientId);

}