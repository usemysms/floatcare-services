package com.java.floatcare.dao;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Repository;

import com.java.floatcare.model.Schedules;

@Repository
public interface ScheduleDAO {

	
	List<Schedules> findScheduleUsersByUserIdAndOnDate(long userId, LocalDate onDate);
	
	Schedules findByUserIdAndDepartmentId(long userId, long departmentId);

	List<Schedules> findUserScheduleByWorkspaceIdsUserIdAndOnDate(List<Long> workspaceIds, long userId, LocalDate onDate);
	
	List<String> findScheduleUserByUserIdAndOnDate(long userId, LocalDate startDate, LocalDate endDate);
	
	List<Schedules> findSchedulesByDepartmentIdAndDuration(long departmentId, LocalDate startDate, LocalDate endDate);
	
	List<Schedules> findUserScheduleByWorkspaceIdUserIdAndOnDate(long workspaceId, long userId, LocalDate onDate);

	List<String> findUserScheduleByWorkspaceIdsUserIdsListAndOnDate(long departmentId, List<Long> userId, LocalDate startDate, LocalDate endDate);

	Schedules findScheduleByUserIdAndShiftDateAndDepartmentId(long userId, LocalDate shiftDate, long departmentId);

	List<Long> findUsersByShiftDateAndDepartmentId(LocalDate shiftDate, long workspaceId);
	
	List<Schedules> findSchedulesByShiftDateAndDepartmentId(LocalDate shiftDate, long workspaceId);
	
	List<Schedules> findUserScheduleByWorkspaceIdsUserIdAndStartDateAndEndDate(List<Long> workspaceIds, long userId, LocalDate startDate, LocalDate endDate);

	List<Long> findUsersByShiftDateAndDepartmentIdAndVoluntaryLowCensus(LocalDate shiftDate, long workspaceId, boolean voluntaryLowCensus);

	List<Long> findUsersByShiftDateAndDepartmentIdAndShiftId(LocalDate onDate, long departmentId, long shiftId);

	List<Schedules> findSchedulesByUserIdsAndDepartmentIdAndOnDate(List<Long> userIds, long departmentId, LocalDate onDate);

	List<Long> findUserSchedulesByShiftDateAndUserIds(LocalDate givenDate, Set<Long> userIds);

	List<Schedules> findUserScheduleByUserIdAndDepartmentIdAndEventType(long userId, long departmentId, String eventType, String status);

	List<Schedules> findUserScheduleByUserIdAndDepartmentIdAndEventTypeAndOnDate(Integer invitee, long departmentId,
			String eventType, String status, LocalDate onDate);

	List<Schedules> findByUserIdAndShiftTypeIdOrDepartmentId(long userId, long departmentShiftId, long departmentId);

	List<Schedules> findScheduleUsersByUserIdAndOnDates(long userId, List<LocalDate> onDates);

}
