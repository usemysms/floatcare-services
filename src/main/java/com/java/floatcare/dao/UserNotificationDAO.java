package com.java.floatcare.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.java.floatcare.model.UserNotification;

@Repository
public interface UserNotificationDAO {

	public List<UserNotification> findUserNotificationByUserIdAndNotificationTypeId(long userId, long notificationTypeId);
	
}
