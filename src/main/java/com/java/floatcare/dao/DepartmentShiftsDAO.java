package com.java.floatcare.dao;

import java.time.LocalDate;
import java.util.List;

import com.java.floatcare.api.request.pojo.DepartmentShiftDTO;
import com.java.floatcare.model.DepartmentShifts;

public interface DepartmentShiftsDAO {

	List<DepartmentShifts> findByIdAndDepartmentId(List<Long> shiftId, long departmentId);

	List<DepartmentShiftDTO> findSchedulesOfUserByDepartmentShiftId(long userId, List<LocalDate> dates);
	
	List<DepartmentShifts> findByDepartmentIdAndStaffCoreTypeIdsAndStatus(long departmentId, long staffCoreTypeId, String status);

}
