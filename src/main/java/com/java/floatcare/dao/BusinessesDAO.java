package com.java.floatcare.dao;

import java.util.List;

import com.java.floatcare.api.request.WorksiteAvailability;
import com.java.floatcare.api.response.pojo.BusinessesResponse;
import com.java.floatcare.model.Businesses;

public interface BusinessesDAO {

	List<Long> getBusinessesIdByOrganizationId (long organizationId);
	
	Businesses getBusinessByOrganizationIdAndBusinessLocationAndStateAndZip (long organizationId, String businessLocation, String city, String state, String zip);

	long updateWorksiteAvailability(long businessId, List<WorksiteAvailability> worksiteAvailability);

	List<BusinessesResponse> findAvailableUsers(long businessId, String weekDay, String startTime, String endTime, long staffUserCoreTypeId);
	
	List<Businesses> searchFacilities(long businessTypeId, String name, String city, String state, String zip);
}
