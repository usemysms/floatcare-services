package com.java.floatcare.dao;

import java.time.LocalDate;
import java.util.List;

import com.java.floatcare.api.response.pojo.SetScheduleResponse;
import com.java.floatcare.model.SetScheduleLayouts;
import com.java.floatcare.model.StaffWeekOffs;

public interface SetScheduleLayoutDAO {

	SetScheduleLayouts findBySetScheduleLayoutId (long setScheduleLayoutId);

	List<SetScheduleLayouts> findByDepartmentId(long departmentId);

	List<SetScheduleLayouts> findByStaffUserCoreTypeIdsAndDepartmentId(List<Long> staffCoreTypeIds, List<Long> shiftTypes, long departmentId, LocalDate startDate, LocalDate endDate);

	SetScheduleLayouts findBySetScheduleLayoutIdsAndUserIdAndWeekDay(List<Long> setScheduleLayoutIds, long userId, String weekDay);

	List<StaffWeekOffs> findByUserId(List<Long> userIds);

	void deleteAllBySetScheduleLayoutIds(List<Long> filteredSetScheduleLayouts);

	List<SetScheduleResponse> findByDepartmentIdAndCoreTypeId(long workspaceId, long staffUserCoreTypeId, String date, String weekDay);

	List<SetScheduleLayouts> findByDepartmentIdAndCoreTypeId(long workspaceId, long staffUserCoreTypeId);

	List<SetScheduleResponse> findByDepartmentIdAndCoreTypeIdAndShiftId(long departmentId, long staffUserCoreTypesId, String string, String convertLocalDateToWeekDay, long shiftTypeId);

}