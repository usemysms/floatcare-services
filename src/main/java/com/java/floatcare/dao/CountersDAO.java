package com.java.floatcare.dao;

public interface CountersDAO {
	
	long getNextSequenceValue(String sequenceName);

	long getCurrentSequenceValue(String counterId);

}
