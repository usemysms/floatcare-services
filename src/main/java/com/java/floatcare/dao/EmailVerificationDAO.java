package com.java.floatcare.dao;

import java.util.List;

import com.google.i18n.phonenumbers.NumberParseException;
import com.java.floatcare.model.EmailVerification;

public interface EmailVerificationDAO {

	List<EmailVerification> findByOtpAndEmail(String email, String otp) throws NumberParseException;

}
