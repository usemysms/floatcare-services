package com.java.floatcare.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import com.java.floatcare.dao.EmailVerificationDAO;
import com.java.floatcare.model.EmailVerification;

@Repository
public class EmailVerificationDAOImpl implements EmailVerificationDAO{

	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Override
	public List<EmailVerification> findByOtpAndEmail(String email, String otp) throws NumberParseException {
		
		//if (email.charAt(0) == '+') // remove special character from the first position in case of phone number.
			//email = email.replace("+", "");
		if (!email.contains("@")) {
		PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
		PhoneNumber numberProto = phoneUtil.parse(email, "US");
		email = phoneUtil.format(numberProto, PhoneNumberFormat.NATIONAL);
		}
		Query query = new Query();
		query = query.addCriteria(Criteria.where("email").is(email)).addCriteria(Criteria.where("otp").is(otp));
		return mongoTemplate.find(query, EmailVerification.class);
	}
}
