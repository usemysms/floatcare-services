package com.java.floatcare.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.java.floatcare.dao.GroupChatsDAO;
import com.java.floatcare.model.GroupChats;

@Repository
public class GroupChatsDAOImpl implements GroupChatsDAO {

	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Override
	public List<GroupChats> findGroupChatByUserId(long userId) {

		Criteria criteria1 = Criteria.where("members").in(userId);
		Criteria criteria2 = Criteria.where("admins").in(userId);
		
		Query query = new Query();
		query.fields().include("chatGroupId");

		return mongoTemplate.find(query.addCriteria(new Criteria().orOperator(criteria1, criteria2)), GroupChats.class);
	}
	
	@Override
	public GroupChats findGroupChatsByGroupIdAndStaffId (long groupId, long staffId) {

		Criteria criteria1 = Criteria.where("chatGroupId").is(groupId);
		Criteria criteria2 = Criteria.where("members").in(staffId);
		Criteria criteria3 = Criteria.where("admins").in(staffId);
		
		return mongoTemplate.findOne(Query.query(criteria1.orOperator(criteria2, criteria3)), GroupChats.class);
	}

	@Override
	public GroupChats findGroupChatByGroupChatId (long groupId) {
		
		return	mongoTemplate.findOne(Query.query(Criteria.where("chatGroupId").is(groupId)), GroupChats.class);
	}

	@Override
	public GroupChats findGroupChatsByGroupIdAndAdminIdAndStaffId(long groupId, long adminStaffId, long staffId) {
		
		Criteria criteria1 = Criteria.where("chatGroupId").is(groupId);
		Criteria criteria2 = Criteria.where("members").in(staffId);
		Criteria criteria3 = Criteria.where("admins").in(adminStaffId);

		return mongoTemplate.findOne(Query.query(criteria1.andOperator(criteria2, criteria3)), GroupChats.class);
	}

	@Override
	public void removeGroup(long chatGroupId) {
		
		mongoTemplate.remove(Query.query(Criteria.where("chatGroupId").is(chatGroupId)), GroupChats.class);
	}
}
