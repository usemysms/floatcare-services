package com.java.floatcare.dao.impl;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.java.floatcare.dao.AssignmentsDAO;
import com.java.floatcare.model.Assignments;

@Repository
public class AssignmentsDAOImpl implements AssignmentsDAO {

	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Override
	public Assignments getAssignmentDetails(long assignmentId) {
		
		Query query = new Query();
		query.addCriteria(Criteria.where("assignmentId").is(assignmentId));
		return mongoTemplate.findOne(query, Assignments.class);
	}

	@Override
	public List<Assignments> getAssignmentsByBusinessId(long businessId) {
		
		Query query = new Query();
		query.addCriteria(Criteria.where("businessId").is(businessId));
		query.with(Sort.by(Sort.Direction.ASC,"assignmentId"));
		return mongoTemplate.find(query, Assignments.class);
	}

	@Override
	public Assignments findAssignmentById(long assignmentId) {
		
		Query query = new Query();
		query.addCriteria(Criteria.where("_id").is(assignmentId));
		return mongoTemplate.findOne(query, Assignments.class);
	}

	@Override
	public List<Long> findAssignmentsByEndDate() {

		Query query = new Query();

		Criteria criteria1 = Criteria.where("status").ne("Inactive");
		Criteria criteria2 = Criteria.where("status").ne("DeletedAssignment");
		query.addCriteria(Criteria.where("endDate").gte(LocalDate.now()));
		query.fields().include("assignmentId");
		return mongoTemplate.find(query.addCriteria(criteria1.andOperator(criteria2)), Assignments.class).stream().map(Assignments::getAssignmentId).collect(Collectors.toList());
	}

	@Override
	public List<Assignments> findAssignmentByIds(List<Long> assignmentIds) {
		
		Query query = new Query();
		query.addCriteria(Criteria.where("assignmentId").in(assignmentIds));
		return mongoTemplate.find(query, Assignments.class);
	}

	@Override
	public List<Assignments> findAssignmentByGivenDate(LocalDate givenDate) {
		
		Query query = new Query();
		query.addCriteria(Criteria.where("startDate").lte(givenDate).orOperator(Criteria.where("endDate").gte(givenDate)));
		return mongoTemplate.find(query, Assignments.class);
	}
	
	@Override
	public List<Assignments> findAssignmentByBusinessIdAndGivenDate(long businessId, LocalDate givenDate) {
		
		Query query = new Query();
		query.addCriteria(Criteria.where("businessId").is(businessId));
		query.addCriteria(Criteria.where("startDate").lte(givenDate).orOperator(Criteria.where("endDate").gte(givenDate)));
		return mongoTemplate.find(query, Assignments.class);
	}

	@Override
	public List<Long> findByStartDateAndEndDateAndBusinessId(LocalDate localStartDate, LocalDate localEndDate, long businessId) {
		
		Query query = new Query();
		Criteria criteria1 = Criteria.where("status").ne("Inactive");
		Criteria criteria2 = Criteria.where("status").ne("DeletedAssignment");
		query.addCriteria(Criteria.where("startDate").lte(localStartDate)).addCriteria(Criteria.where("endDate").gte(localEndDate))
			.addCriteria(Criteria.where("businessId").is(businessId)).addCriteria(criteria1.orOperator(criteria2));
		query.fields().include("assignmentId");
		return mongoTemplate.find(query, Assignments.class).stream().map(emitter->emitter.getAssignmentId()).collect(Collectors.toList());
	}
}
