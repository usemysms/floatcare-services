package com.java.floatcare.dao.impl;

import java.time.LocalDate;
import java.util.Iterator;
import java.util.List;

import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.java.floatcare.dao.UserCredentialsDAO;
import com.java.floatcare.model.UserCredentials;

@Repository
public class UserCredentialsDAOImpl implements UserCredentialsDAO {

	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public List<UserCredentials> findUserCredentials(List<Long> userIds) {
		Query query = new Query();
		query.addCriteria(Criteria.where("userId").in(userIds));
		//query.fields().include("userId");
		//query.fields().include("isVerified");
		//query.fields().include("isPending");
		return mongoTemplate.find(query, UserCredentials.class);
	}

	@Override
	public List<UserCredentials> findUserCredentialsByExpiredDate(LocalDate checkExpiryDate) {
		
		Query query = new Query();
		query.addCriteria(Criteria.where("expirationDate").is(checkExpiryDate));
		return mongoTemplate.find(query, UserCredentials.class);
	}

	@Override
	public Iterator<UserCredentials> findUserCredentialsByCurrentDate() {

		String jsonExpression = "{\"$subtract\":[\"$expirationDate\", new Date()]}";
		
		Aggregation agg = Aggregation.newAggregation(Aggregation.project("userCredentialId", "userId", "expirationDate")
				.and(context -> context.getMappedObject(Document.parse(jsonExpression))).as("dateDifference"),

//		Aggregation.match(Criteria.where("dateDifference").lte(15*24*3600*1000)));
		Aggregation.match(new Criteria().andOperator(Criteria.where("dateDifference").gte(24*3600*1000), Criteria.where("dateDifference").lte(15*24*3600*1000))));
		
		return mongoTemplate.aggregate(agg, UserCredentials.class, UserCredentials.class).iterator();
	}

	@Override
	public List<UserCredentials> findByUserIdAndIsPendingAndIsRequired(long userId, boolean isPending, boolean isRequired) {
		
		Query query = new Query();
		Criteria criteria1 = Criteria.where("userId").is(userId);
		Criteria criteria2 = Criteria.where("isPending").is(isPending);
		Criteria criteria3 = Criteria.where("isRequired").is(isRequired);
		query.addCriteria(criteria1.andOperator(criteria2,criteria3));

		return mongoTemplate.find(query, UserCredentials.class);
	}

	@Override
	public List<UserCredentials> findUserCredentialsByIsRequiredAndIsPending(boolean isRequired, boolean isPending) {
		
		Query query = new Query();
		
		Criteria criteria1 = Criteria.where("isPending").is(isPending);
		Criteria criteria2 = Criteria.where("isRequired").is(isRequired);
		query.addCriteria(criteria1.andOperator(criteria2));

		return mongoTemplate.find(query, UserCredentials.class);
	}
}