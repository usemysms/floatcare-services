package com.java.floatcare.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.java.floatcare.dao.UserNotificationDAO;
import com.java.floatcare.model.UserNotification;

@Repository
public class UserNotificationDAOImpl implements UserNotificationDAO{

	@Autowired
	private MongoTemplate mongoTemplate;


	@Override
	public List<UserNotification> findUserNotificationByUserIdAndNotificationTypeId(long userId, long notificationTypeId) {
		Query query = new Query();
		query.addCriteria(Criteria.where("userId").is(userId)).addCriteria(Criteria.where("notificationTypeId").is(notificationTypeId));
		return mongoTemplate.find(query, UserNotification.class);
	}

}
