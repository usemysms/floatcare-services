package com.java.floatcare.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.java.floatcare.dao.PhoneVerificationDAO;
import com.java.floatcare.model.PhoneVerification;

@Repository
public class PhoneVerificationDAOImpl implements PhoneVerificationDAO {

	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public List<PhoneVerification> findByOtpAndPhoneNumber(String phone, String otp) {
		Query query = new Query();
		query.addCriteria(Criteria.where("phoneNumber").is(phone)).addCriteria(Criteria.where("isVerified").is(false)).addCriteria(Criteria.where("otp").is(otp));
		return mongoTemplate.find(query, PhoneVerification.class);
	}

}
