package com.java.floatcare.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.java.floatcare.dao.AssignmentSchedulesDAO;
import com.java.floatcare.model.AssignmentSchedules;

@Repository
public class AssignmentSchedulesDAOImpl implements AssignmentSchedulesDAO {

	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public List<AssignmentSchedules> findAssignmentSchedulesByAssignmentIdsAndUserId(List<Long> assignmentIds, long userId) {

		Query query = new Query();
		query.addCriteria(Criteria.where("assignmentId").in(assignmentIds));
		query.addCriteria(Criteria.where("staff").in(userId));
		return mongoTemplate.find(query, AssignmentSchedules.class);
	}
	
	@Override
	public List<AssignmentSchedules> findAssignmentSchedulesByAssignmentIdsAndStaffCoreTypeId(List<Long> assignmentIds, long staffCoreTypeId) {

		Query query = new Query();
		query.addCriteria(Criteria.where("assignmentId").in(assignmentIds));
		query.addCriteria(Criteria.where("staffCoreTypeId").is(staffCoreTypeId));
		return mongoTemplate.find(query, AssignmentSchedules.class);
	}
	
	@Override
	public List<AssignmentSchedules> findAssignmentSchedulesByAssignmentIdsAndUserIdAndWeekDay(List<Long> assignmentIds, long userId, String weekDay) {

		Query query = new Query();
		query.addCriteria(Criteria.where("assignmentId").in(assignmentIds));
		query.addCriteria(Criteria.where("staff").in(userId));
		query.addCriteria(Criteria.where("weekDay").regex(weekDay,"i"));
		return mongoTemplate.find(query, AssignmentSchedules.class);
	}

	@Override
	public AssignmentSchedules findAssignmentSchedulesById(long assignmentScheduleId) {
		
		Query query = new Query();
		query.addCriteria(Criteria.where("_id").is(assignmentScheduleId));
		return mongoTemplate.findOne(query, AssignmentSchedules.class);
	}

	@Override
	public AssignmentSchedules findAssignmentSchedulesByAssignmentIdAndUserIdAndWeekDay(long assignmentId, long userId, String weekDay) {
		
		Query query = new Query();
		query.addCriteria(Criteria.where("assignmentId").in(assignmentId));
		query.addCriteria(Criteria.where("staff").in(userId));
		query.addCriteria(Criteria.where("weekDay").regex(weekDay,"i"));
		return mongoTemplate.findOne(query, AssignmentSchedules.class);
	}

	@Override
	public Map<String, AssignmentSchedules> findAssignmentScheduleByAssignmentId(long assignmentId) {

		Query query = new Query();
		query.addCriteria(Criteria.where("assignmentId").is(assignmentId));
		List<AssignmentSchedules> assignmentScheduleList = mongoTemplate.find(query, AssignmentSchedules.class);
		Map<String, AssignmentSchedules> assignmentSchedules = new HashMap<>();
		for (AssignmentSchedules each : assignmentScheduleList) {
			assignmentSchedules.put(each.getWeekDay().toLowerCase(), each);
		}
		return assignmentSchedules;
	}

	@Override
	public List<AssignmentSchedules> findByStaffIdAndStartTimeAndEndTime(long staffId, List<Long> assignmentIds, String weekDay, String startTime, String endTime) {
		
		Query query = new Query();
		
		Criteria criteria1 = Criteria.where("staff").in(staffId);
		Criteria criteria2 = (Criteria.where("startTime").lte(startTime).lte(endTime)
				.andOperator(Criteria.where("endTime").gte(startTime).gte(endTime)));

		Criteria criteria3 = (Criteria.where("startTime").lte(startTime).lte(endTime)
				.andOperator(Criteria.where("endTime").gte(startTime).lte(endTime)));
		
		Criteria criteria4 = (Criteria.where("startTime").gte(startTime).lte(endTime)
				.andOperator(Criteria.where("endTime").gte(startTime).gte(endTime)));
		
		Criteria criteria5 = (Criteria.where("startTime").gte(startTime).lte(endTime)
				.andOperator(Criteria.where("endTime").gte(startTime).lte(endTime)));

		Criteria criteria6 = Criteria.where("weekDay").regex(weekDay,"i");
		
		Criteria criteria7 = Criteria.where("assignmentId").in(assignmentIds);

		return mongoTemplate.find(query.addCriteria(criteria1.orOperator(criteria2, criteria2, criteria3, criteria4, criteria5)).addCriteria(criteria6).addCriteria(criteria7), AssignmentSchedules.class);
	}

	@Override
	public List<AssignmentSchedules> findAssignmentSchedulesByAssignmentIdsAndWeekDay(List<Long> assignmentIds, String weekDay) {
		Query query = new Query();
		query.addCriteria(Criteria.where("assignmentId").in(assignmentIds));
		query.addCriteria(Criteria.where("weekDay").regex(weekDay,"i"));
		query.fields().include("staff");
		return mongoTemplate.find(query, AssignmentSchedules.class);
	}
}