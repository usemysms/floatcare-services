package com.java.floatcare.dao.impl;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.java.floatcare.dao.RequestsDAO;
import com.java.floatcare.model.Requests;
import com.java.floatcare.utils.ZonedDateTimeWriteConverter;

@Repository
public class RequestsDAOImpl implements RequestsDAO {

	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Override
	public List<Requests> findRequestsByUserId(long userId) {
		
		ZonedDateTimeWriteConverter zonedDateTimeWriteConverter = new ZonedDateTimeWriteConverter();
		
		Query query = new Query();
		query.addCriteria(Criteria.where("userId").is(userId)).addCriteria(Criteria.where("onDate").gte(zonedDateTimeWriteConverter.convert(ZonedDateTime.now())));
		return mongoTemplate.find(query, Requests.class);
	}

	@Override
	public List<Requests> findRequestsByTypeIdAndDepartmentId(List<Long> requestTypeId, long workspaceId) {
		
		Query query = new Query();
		query.addCriteria(Criteria.where("requestTypeId").in(requestTypeId)).addCriteria(Criteria.where("departmentId").is(workspaceId));
		query.with(Sort.by(Sort.Direction.DESC,"requestId"));
		return mongoTemplate.find(query, Requests.class);		
	}

	@Override
	public List<Requests> findByUserIdAndOnDateAndDepartmentId(long userId, LocalDate onDate, long departmentId) {

		Query query = new Query();
		query.addCriteria(Criteria.where("userId").is(userId)).addCriteria(Criteria.where("onDate").in(onDate)).addCriteria(Criteria.where("departmentId").is(departmentId));
		query.fields().include("status").include("requestId");
		return mongoTemplate.find(query, Requests.class);
	}
	
	@Override
	public List<Requests> findByUserIdAndOnDateAndDepartmentIds(long userId, LocalDate onDate, List<Long> departmentId) {

		Query query = new Query();
		query.addCriteria(Criteria.where("userId").is(userId)).addCriteria(Criteria.where("onDate").in(onDate)).addCriteria(Criteria.where("departmentId").in(departmentId));
		return mongoTemplate.find(query, Requests.class);
	}

	@Override
	public List<String> findRequestsByUserIdAndOnDate(List<Long> usersId, LocalDate localStartDate, LocalDate localEndDate) {
		Query query = new Query();
		query.addCriteria(Criteria.where("userId").in(usersId)).addCriteria(Criteria.where("onDate").gte(localStartDate).lte(localEndDate));
		query.fields().include("status");
		query.fields().include("onDate");
		query.fields().include("userId");
		query.fields().include("departmentId");
		return mongoTemplate.find(query, Requests.class).stream().map(emitter -> emitter.getOnDate().stream().map(emitter1 -> emitter.getUserId()+" "+emitter1+" "+emitter.getDepartmentId()+" "+emitter.getStatus()).collect(Collectors.toList())).flatMap(Collection::stream).collect(Collectors.toList());
	}
	
	@Override
	public List<Requests> findRequestsByDepartmentIdAndDuration(long departmentId, LocalDate localStartDate, LocalDate localEndDate) {
		Query query = new Query();
		query.addCriteria(Criteria.where("departmentId").in(departmentId)).addCriteria(Criteria.where("onDate").gte(localStartDate).lte(localEndDate));
		//query.fields().include("status");
		//query.fields().include("onDate");
		//query.fields().include("userId");
		//query.fields().include("departmentId");
		return mongoTemplate.find(query, Requests.class);//.stream().map(emitter -> emitter.getOnDate().stream().map(emitter1 -> emitter.getUserId()+" "+emitter1+" "+emitter.getDepartmentId()+" "+emitter.getStatus()).collect(Collectors.toList())).flatMap(Collection::stream).collect(Collectors.toList());
	}
	

	@Override
	public List<Long> findUserIdsByRequestIdAndShiftIdAndIsApproved(long requestTypeId, long shiftId, LocalDate onDate, long departmentId, String status) {

		Query query = new Query();
		query.addCriteria(Criteria.where("requestTypeId").is(requestTypeId)).addCriteria(Criteria.where("onDate").in(onDate)).addCriteria(Criteria.where("status").is(status)).addCriteria(Criteria.where("shiftId").is(shiftId));
		query.fields().include("userId");
		return mongoTemplate.find(query, Requests.class).stream().map(emitter -> emitter.getUserId()).collect(Collectors.toList());
	}
	
	@Override
	public List<Long> findUserIdsByRequestIdAndIsApproved(long requestTypeId, LocalDate onDate, long departmentId, String status) {

		Query query = new Query();
		query.addCriteria(Criteria.where("requestTypeId").is(requestTypeId)).addCriteria(Criteria.where("onDate").in(onDate)).addCriteria(Criteria.where("status").is(status));
		query.fields().include("userId");
		return mongoTemplate.find(query, Requests.class).stream().map(emitter -> emitter.getUserId()).collect(Collectors.toList());
	}

	@Override
	public List<Requests> findRequestsByStatusAndDepartmentId(String status, long workspaceId) {
		
		ZonedDateTimeWriteConverter zonedDateTimeWriteConverter = new ZonedDateTimeWriteConverter();
		LocalDate date = zonedDateTimeWriteConverter.convert(ZonedDateTime.now());
		Query query = new Query();
		query.addCriteria(Criteria.where("status").is(status)).addCriteria(Criteria.where("departmentId").in(workspaceId)).addCriteria(Criteria.where("onDate").gte(date))
			.addCriteria(Criteria.where("requestTypeId").is(3));
		query.fields().include("onDate");
		query.with(Sort.by(Sort.Direction.DESC,"onDate"));
		return mongoTemplate.find(query, Requests.class);
	}

	@Override
	public List<Requests> findRequestsByRequestIdAndIsApproved(long requestTypeId, LocalDate onDate, long workspaceId, String status) {

		Query query = new Query();
		query.addCriteria(Criteria.where("requestTypeId").is(requestTypeId)).addCriteria(Criteria.where("onDate").in(onDate)).addCriteria(Criteria.where("status").is(status)).addCriteria(Criteria.where("departmentId").is(workspaceId));
		return mongoTemplate.find(query, Requests.class);
	}

	@Override
	public List<Requests> findRequestsByRequestTypeDepartmentIdAndStartDate(long requestTypeId, long workspaceId, LocalDate onDate) {

		Query query = new Query();
		query.addCriteria(Criteria.where("requestTypeId").is(requestTypeId)).addCriteria(Criteria.where("onDate").gte(onDate)).addCriteria(Criteria.where("departmentId").is(workspaceId));
		return mongoTemplate.find(query, Requests.class);
	}

	@Override
	public List<Requests> findRequestsByUserIdAndRequestTypeId(long userId, long requestTypeId) {
		
		ZonedDateTimeWriteConverter zonedDateTimeWriteConverter = new ZonedDateTimeWriteConverter();
		LocalDate currentDate = zonedDateTimeWriteConverter.convert(ZonedDateTime.now());
		
		Query query = new Query();
		query.addCriteria(Criteria.where("requestTypeId").is(requestTypeId)).addCriteria(Criteria.where("userId").is(userId)).addCriteria(Criteria.where("onDate").gte(currentDate));
		return mongoTemplate.find(query, Requests.class);
	}

	@Override
	public List<Requests> findRequestsByUserIdAndOnDate(long userId, LocalDate shiftDate) {
		
		Query query = new Query();
		query.addCriteria(Criteria.where("userId").is(userId)).addCriteria(Criteria.where("onDate").in(shiftDate)).addCriteria(Criteria.where("status").is("Reviewed"));
		return mongoTemplate.find(query, Requests.class);
		
	}

	@Override
	public List<Requests> findByUserIdAndShiftTypeIdOrDepartmentId(Long userId, Long shiftId, Long departmentId) {

		Query query = new Query();

		if (departmentId == 0) {

			query.addCriteria(Criteria.where("userId").is(userId)).addCriteria(Criteria.where("onDate").gte(LocalDate.now())).addCriteria(Criteria.where("shiftId").is(shiftId))
				.addCriteria(Criteria.where("status").is("Pending"));
			query.fields().include("requestId").include("requestTypeId").include("departmentId").include("onDate");
			return mongoTemplate.find(query, Requests.class);
		}else {

			query.addCriteria(Criteria.where("userId").is(userId)).addCriteria(Criteria.where("onDate").gte(LocalDate.now())).addCriteria(Criteria.where("departmentId").is(departmentId))
				.addCriteria(Criteria.where("status").is("Pending"));
			query.fields().include("requestId").include("requestTypeId").include("departmentId").include("onDate");
		return mongoTemplate.find(query, Requests.class);
		}
	}
}
