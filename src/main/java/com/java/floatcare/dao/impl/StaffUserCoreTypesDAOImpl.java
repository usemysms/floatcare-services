package com.java.floatcare.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import com.java.floatcare.dao.StaffUserCoreTypesDAO;
import com.java.floatcare.model.StaffUserCoreTypes;
import com.java.floatcare.utils.ConstantUtils;

@Component
public class StaffUserCoreTypesDAOImpl implements StaffUserCoreTypesDAO {
	@Autowired
	private MongoTemplate mongoTemplate;
	
	public List<StaffUserCoreTypes> findAllStaffUserCoreTypesByFirmType(String firmType){
		Query query = new Query();
		Criteria criteria;
		if(firmType == null || firmType.isEmpty()) {
			 criteria = Criteria.where("firmType").in(ConstantUtils.MedicalFacility);	
		} else {
			 criteria = Criteria.where("firmType").in(firmType);
		}
		
        return mongoTemplate.find(query.addCriteria(criteria), StaffUserCoreTypes.class);
		
	}
}
