package com.java.floatcare.dao.impl;

import java.time.LocalDate;
import java.util.List;

import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.java.floatcare.api.response.pojo.SetScheduleResponse;
import com.java.floatcare.dao.SetScheduleLayoutDAO;
import com.java.floatcare.model.SetScheduleLayouts;
import com.java.floatcare.model.StaffWeekOffs;

@Repository
public class SetScheduleLayoutDAOImpl implements SetScheduleLayoutDAO{

	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Override
	public SetScheduleLayouts findBySetScheduleLayoutId(long setScheduleLayoutId) {

		Query query = new Query();
		query.addCriteria(Criteria.where("_id").is(setScheduleLayoutId));
		return mongoTemplate.findOne(query, SetScheduleLayouts.class);
	}

	@Override
	public List<SetScheduleLayouts> findByDepartmentId(long departmentId) {
		
		Query query = new Query();
		query.addCriteria(Criteria.where("departmentId").is(departmentId));
		return mongoTemplate.find(query, SetScheduleLayouts.class);
	}

	@Override
	public List<SetScheduleLayouts> findByStaffUserCoreTypeIdsAndDepartmentId(List<Long> staffCoreTypeIds, List<Long> shiftTypes, long departmentId,
				LocalDate startDate, LocalDate endDate) {
		
		Query query = new Query();
		
		Criteria criteria1 = Criteria.where("roles").in(staffCoreTypeIds);
		
		Criteria criteria2 = Criteria.where("shiftTypes").in(shiftTypes);
		
		Criteria criteria3 = (Criteria.where("startDate").lte(startDate).lte(endDate)
				.andOperator(Criteria.where("endDate").gte(startDate).gte(endDate)));

		Criteria criteria4 = (Criteria.where("startDate").lte(startDate).lte(endDate)
				.andOperator(Criteria.where("endDate").gte(startDate).lte(endDate)));
		
		Criteria criteria5 = (Criteria.where("startDate").gte(startDate).lte(endDate)
				.andOperator(Criteria.where("endDate").gte(startDate).gte(endDate)));
		
		Criteria criteria6 = (Criteria.where("startDate").gte(startDate).lte(endDate)
				.andOperator(Criteria.where("endDate").gte(startDate).lte(endDate)));
		
		Criteria criteria7 = Criteria.where("departmentId").is(departmentId);
		
		query.addCriteria(criteria7.andOperator(criteria2, criteria1).orOperator(criteria3, criteria4, criteria5, criteria6));
		
		return mongoTemplate.find(query, SetScheduleLayouts.class);
	}

	@Override
	public SetScheduleLayouts findBySetScheduleLayoutIdsAndUserIdAndWeekDay(List<Long> setScheduleLayoutIds, long userId, String weekDay) {
		
		Query query = new Query();
		Criteria criteria1 = Criteria.where("setScheduleLayoutId").in(setScheduleLayoutIds);
		Criteria criteria2 = Criteria.where("weekOffDays").in(weekDay);
		query.fields().include("setScheduleLayoutId");
		return mongoTemplate.findOne(query.addCriteria(criteria1).addCriteria(criteria2), SetScheduleLayouts.class);
	}

	@Override
	public List<StaffWeekOffs> findByUserId(List<Long> userIds) {

		Query query = new Query();
		Criteria criteria1 = Criteria.where("userId").in(userIds);
		return mongoTemplate.find(query.addCriteria(criteria1), StaffWeekOffs.class);
	}

	@Override
	public void deleteAllBySetScheduleLayoutIds(List<Long> setScheduleLayoutIds) {
		Query query = new Query();
		Criteria criteria1 = Criteria.where("setScheduleLayoutId").in(setScheduleLayoutIds);
		mongoTemplate.findAndRemove(query.addCriteria(criteria1), SetScheduleLayouts.class);
	}

	@Override
	public List<SetScheduleResponse> findByDepartmentIdAndCoreTypeId(long workspaceId, long staffUserCoreTypeId, String date, String weekDay) {


		String project1 = "{$ifNull: [\"$endDate\",  ISODate(\""+date+"T00:00:00Z\")]}";

		Aggregation aggregation = Aggregation.newAggregation(SetScheduleLayouts.class, Aggregation.match(Criteria.where("startDate").lte(LocalDate.parse(date))),
				Aggregation.lookup("staff_week_offs", "_id", "setScheduleLayoutId", "schedules"),
				Aggregation.match(Criteria.where("schedules.setScheduleWeekOffDays").nin(weekDay)),
				Aggregation.project("shiftTypes").and(e->e.getMappedObject(Document.parse(project1))).as("endDate")
					.andInclude("schedules").andInclude("startDate").andInclude("departmentId").andInclude("roles").andInclude("weekOffDays"),
				Aggregation.match(new Criteria().andOperator(Criteria.where("startDate").lte(LocalDate.parse(date)), Criteria.where("endDate").gte(LocalDate.parse(date)),
					Criteria.where("roles").in(staffUserCoreTypeId), Criteria.where("departmentId").is(workspaceId), Criteria.where("weekOffDays").nin(weekDay))));

	    AggregationResults<SetScheduleResponse> results = mongoTemplate.aggregate(aggregation, "set_schedule_layouts", SetScheduleResponse.class);

		return  results.getMappedResults();
	}

	@Override
	public List<SetScheduleLayouts> findByDepartmentIdAndCoreTypeId(long workspaceId, long staffUserCoreTypeId) {

		Query query = new Query();
		Criteria criteria1 = Criteria.where("departmentId").is(workspaceId);
		Criteria criteria2 = Criteria.where("roles").in(staffUserCoreTypeId);

        return mongoTemplate.find(query.addCriteria(criteria1).addCriteria(criteria2), SetScheduleLayouts.class);
	}

	@Override
	public List<SetScheduleResponse> findByDepartmentIdAndCoreTypeIdAndShiftId(long departmentId, long staffUserCoreTypeId, String date, String weekDay, long shiftTypeId) {

		String project1 = "{$ifNull: [\"$endDate\",  ISODate(\""+date+"T00:00:00Z\")]}";

		Aggregation aggregation = Aggregation.newAggregation(SetScheduleLayouts.class, Aggregation.match(Criteria.where("startDate").lte(LocalDate.parse(date))),
				Aggregation.lookup("staff_week_offs", "_id", "setScheduleLayoutId", "schedules"),
				Aggregation.match(Criteria.where("schedules.setScheduleWeekOffDays").nin(weekDay)),
				Aggregation.project("shiftTypes").and(e->e.getMappedObject(Document.parse(project1))).as("endDate")
					.andInclude("schedules").andInclude("startDate").andInclude("departmentId").andInclude("roles").andInclude("weekOffDays"),
				Aggregation.match(new Criteria().andOperator(Criteria.where("startDate").lte(LocalDate.parse(date)), Criteria.where("endDate").gte(LocalDate.parse(date)),
					Criteria.where("roles").in(staffUserCoreTypeId), Criteria.where("departmentId").is(departmentId), Criteria.where("weekOffDays").nin(weekDay),
					Criteria.where("shiftTypes").in(shiftTypeId))));

	    AggregationResults<SetScheduleResponse> results = mongoTemplate.aggregate(aggregation, "set_schedule_layouts", SetScheduleResponse.class);

		return  results.getMappedResults();
	}
}