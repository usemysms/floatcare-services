package com.java.floatcare.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.java.floatcare.dao.UserLoginDetailsDAO;
import com.java.floatcare.model.UserLoginDetails;

@Repository
public class UserLoginDetailsDAOImpl implements UserLoginDetailsDAO {

	@Autowired
	private MongoTemplate mongoTemplate;

	public List<UserLoginDetails> findByUserIdAndDevice(long userId,String device) {
		Query query = new Query();
		query.addCriteria(Criteria.where("userId").is(userId)).addCriteria(Criteria.where("device").is(device));
		return mongoTemplate.find(query, UserLoginDetails.class);
	}

	@Override
	public List<UserLoginDetails> findByUserId(long userId) {
		Query query = new Query();
		query.addCriteria(Criteria.where("userId").is(userId));
		return mongoTemplate.find(query, UserLoginDetails.class);
	}
	

}
