package com.java.floatcare.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.java.floatcare.dao.CountersDAO;
import com.java.floatcare.model.Counters;

@Repository
public class CountersDAOImpl implements CountersDAO {

	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Override
	public long getNextSequenceValue(String sequenceName) {
		Query query = new Query(Criteria.where("name").is(sequenceName));
		Update update = new Update().inc("sequence", 1);
        
        Counters counter = mongoTemplate.findAndModify(query, update, Counters.class); // return old Counter object
        return counter.getSequence();    
	}
	
	@Override
	public long getCurrentSequenceValue(String counterId) {
		Query query = new Query(Criteria.where("name").is(counterId));
        return mongoTemplate.findOne(query, Counters.class).getSequence();
	}

}
