package com.java.floatcare.dao.impl;

import java.time.LocalDate;
import java.util.List;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.java.floatcare.dao.ScheduleLayoutDAO;
import com.java.floatcare.model.ScheduleLayout;

@Repository
public class ScheduleLayoutDAOImpl implements ScheduleLayoutDAO {

	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Override
	public ScheduleLayout findAvailableScheduleLayoutForRole(List<Long> staffUserCoreTypeIds, DateTime scheduleStartDate, long departmentId) {

		Query query = new Query();
		query.addCriteria(Criteria.where("staffUserCoreTypeIds").in(staffUserCoreTypeIds)).addCriteria(Criteria.where("scheduleStartDate").lte(scheduleStartDate).orOperator(Criteria.where("scheduleEndDate").gte(scheduleStartDate)))
			 .addCriteria(Criteria.where("departmentId").is(departmentId));
		return mongoTemplate.findOne(query, ScheduleLayout.class);
	}
	
	@Override
	public ScheduleLayout findAvailableScheduleLayoutForShiftTypeAndDepartmentId(List<Long> shiftTypeIds, DateTime scheduleStartDate, long departmentId, List<Long> roleTypes) {

		Query query = new Query();
		query.addCriteria(Criteria.where("shiftTypes").in(shiftTypeIds).andOperator(Criteria.where("scheduleStartDate").lte(scheduleStartDate),Criteria.where("scheduleEndDate").gte(scheduleStartDate))).addCriteria(Criteria.where("departmentId").is(departmentId))
			.addCriteria(Criteria.where("staffUserCoreTypeIds").in(roleTypes));
		return mongoTemplate.findOne(query, ScheduleLayout.class);
	}
	
	@Override
	public List<ScheduleLayout> findByAvailabilityDeadlineDateAndIsPublised(DateTime makeStaffAvailabilityDate, boolean isPublisedExist, String comparison) {

		Query query = new Query();
		
		if (comparison.equalsIgnoreCase("lte")) {
			query.addCriteria(Criteria.where("makeStaffAvailabilityDeadLine").lte(makeStaffAvailabilityDate)).
			addCriteria(Criteria.where("publishedOn").exists(isPublisedExist));
			query.fields().include("scheduleLayoutId");
			return mongoTemplate.find(query, ScheduleLayout.class);
		}else if (comparison.equalsIgnoreCase("gte")){
			query.addCriteria(Criteria.where("makeStaffAvailabilityDeadLine").gte(makeStaffAvailabilityDate)).
			addCriteria(Criteria.where("publishedOn").exists(isPublisedExist));
			query.fields().include("scheduleLayoutId");
			return mongoTemplate.find(query, ScheduleLayout.class);
		}else
			return null;
	}

	@Override
	public List<ScheduleLayout> findAvailableScheduleLayoutForRolesAndShiftTypes(List<Long> staffCoreTypes, List<Long> shiftTypes, DateTime scheduleStartDate, long departmentId) {
		
		Query query = new Query();
		Criteria criteria1 = Criteria.where("shiftTypes").in(shiftTypes);
		Criteria criteria2 = Criteria.where("staffUserCoreTypeIds").in(staffCoreTypes);
		Criteria criteria3 = Criteria.where("scheduleStartDate").lte(scheduleStartDate).and("scheduleEndDate").gte(scheduleStartDate);
		Criteria criteria4 = Criteria.where("departmentId").is(departmentId);

		query.addCriteria(criteria4.andOperator(criteria1,criteria2)).addCriteria(criteria3);
		return mongoTemplate.find(query, ScheduleLayout.class);
	}

	@Override
	public List<ScheduleLayout> findByScheduleLayoutIdsAndShiftPlanningDate(List<Long> scheduleLayoutIds, LocalDate currentDate, long departmentId, long shiftTypeId) {

		Query query = new Query();

		if (departmentId == 0) {
			
			Criteria criteria1 = Criteria.where("scheduleEndDate").gte(currentDate);
			Criteria criteria2 = Criteria.where("shiftPlanningEndDate").gte(currentDate);
			Criteria criteria3 = Criteria.where("shiftTypes").in(shiftTypeId);
			Criteria criteria4 = Criteria.where("scheduleLayoutId").in(scheduleLayoutIds);

			query.addCriteria(new Criteria().andOperator(criteria3, criteria4).orOperator(criteria1, criteria2));
			return mongoTemplate.find(query, ScheduleLayout.class);
		}else {

			Criteria criteria1 = Criteria.where("scheduleEndDate").gte(currentDate);
			Criteria criteria2 = Criteria.where("shiftPlanningEndDate").gte(currentDate);
			Criteria criteria3 = Criteria.where("departmentId").is(departmentId);
			Criteria criteria4 = Criteria.where("scheduleLayoutId").in(scheduleLayoutIds);

			query.addCriteria(new Criteria().andOperator(criteria3, criteria4).orOperator(criteria1, criteria2));
			return mongoTemplate.find(query, ScheduleLayout.class);
		}
	}

	@Override
	public List<ScheduleLayout> findByUserIdAndCurrentDate(long userId, DateTime currentDate) {

		Aggregation agg = Aggregation.newAggregation(Aggregation.lookup("schedule_layout_groups", "_id", "scheduleLayoutId", "groups"), Aggregation.unwind("groups"),
				Aggregation.match(new Criteria().andOperator(Criteria.where("scheduleEndDate").gte(currentDate), Criteria.where("groups.userIds").in(userId))));
		
		AggregationResults<ScheduleLayout> result = mongoTemplate.aggregate(agg, ScheduleLayout.class, ScheduleLayout.class);

		return result.getMappedResults();
	}
}