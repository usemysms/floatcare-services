package com.java.floatcare.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import static java.util.Comparator.comparingLong;
import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toCollection;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.java.floatcare.api.response.pojo.BusinessesResponse;
import com.java.floatcare.api.response.pojo.DepartmentShiftsResponse;
import com.java.floatcare.dao.UserWorkspacesDAO;
import com.java.floatcare.model.Businesses;
import com.java.floatcare.model.UserWorkspaces;

@Repository
public class UserWorkspacesDAOImpl implements UserWorkspacesDAO {

	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public UserWorkspaces findByInvitationCodeAndIsJoined(String invitationCode, boolean isJoined) throws Exception {

		try {
			Query query = new Query();
			query.addCriteria(Criteria.where("invitationCode").is(invitationCode))
					.addCriteria(Criteria.where("isJoined").is(isJoined));
			return mongoTemplate.findOne(query, UserWorkspaces.class);
		} catch (Exception e) {
			throw new Exception("User has already joined the workspace");
		}
	}

	@Override
	public List<Long> findByDepartmentIdAndStaffCoreTypeIdAndStatusAndIsJoined(long departmentId,
			long staffUserCoreTypeId, String status, boolean isJoined) {

		Query query = new Query();
		query.addCriteria(Criteria.where("workspaceId").is(departmentId))
				.addCriteria(Criteria.where("staffUserCoreTypeId").is(staffUserCoreTypeId))
				.addCriteria(Criteria.where("status").is(status)).addCriteria(Criteria.where("isJoined").is(isJoined));
		query.fields().include("userId");
		return mongoTemplate.find(query, UserWorkspaces.class).stream().map(UserWorkspaces::getUserId)
				.collect(Collectors.toList());
	}

	@Override
	public UserWorkspaces findByDepartmentIdAndUserId(long departmentId, long userId) {

		Query query = new Query();
		query.addCriteria(Criteria.where("workspaceId").is(departmentId))
				.addCriteria(Criteria.where("userId").is(userId)).addCriteria(Criteria.where("status").is("Active"))
				.addCriteria(Criteria.where("isJoined").is(true));
		return mongoTemplate.findOne(query, UserWorkspaces.class);
	}

	@Override
	public Map<Long, Long> findByDepartmentId(long departmentId) {

		Query query = new Query();
		query.addCriteria(Criteria.where("workspaceId").is(departmentId))
				.addCriteria(Criteria.where("status").is("Active")).addCriteria(Criteria.where("isJoined").is(true));
		query.fields().include("userId");
		query.fields().include("organizationId");
		List<UserWorkspaces> userList = mongoTemplate.find(query, UserWorkspaces.class);

		HashMap<Long, Long> users = new HashMap<>();

		for (UserWorkspaces eachUser : userList)
			users.put(eachUser.getUserId(), eachUser.getOrganizationId());
		return users;
	}

	@Override
	public List<UserWorkspaces> findUserIdsByDepartmentId(long departmentId) {

		Query query = new Query();
		query.addCriteria(Criteria.where("workspaceId").is(departmentId))
				.addCriteria(Criteria.where("status").is("Active")).addCriteria(Criteria.where("isJoined").is(true))
				.addCriteria(Criteria.where("isPrimaryDepartment").is(true));
		query.fields().include("userId");
		query.fields().include("staffUserCoreTypeId");
		List<UserWorkspaces> currentId = mongoTemplate.find(query, UserWorkspaces.class);
		return currentId;
	}

	@Override
	public List<UserWorkspaces> findUserIdsByDepartmentIdAndNonPrimary(long departmentId) {

		Query query = new Query();
		query.addCriteria(Criteria.where("workspaceId").is(departmentId))
				.addCriteria(Criteria.where("status").is("Active")).addCriteria(Criteria.where("isJoined").is(true));
		query.fields().include("userId");
		query.fields().include("staffUserCoreTypeId");
		List<UserWorkspaces> currentId = mongoTemplate.find(query, UserWorkspaces.class);
		return currentId;
	}

	@Override
	public List<UserWorkspaces> findByDepartmentIdAndStaffUserCoreTypeIdAndIsJoined(long departmentId,
			long staffUserCoreTypeId, boolean isJoined) {

		Query query = new Query();
		query.addCriteria(Criteria.where("workspaceId").is(departmentId))
				.addCriteria(Criteria.where("isJoined").is(isJoined))
				.addCriteria(Criteria.where("staffUserCoreTypeId").is(staffUserCoreTypeId));
		query.fields().include("userId");
		return mongoTemplate.find(query, UserWorkspaces.class);
	}

	@Override
	public List<Long> findByStaffUserCoreTypeIdAndDepartmentId(List<Long> staffUserCoreTypeIds, long departmentId) {

		Query query = new Query();
		query.addCriteria(Criteria.where("staffUserCoreTypeId").in(staffUserCoreTypeIds))
				.addCriteria(Criteria.where("workspaceId").is(departmentId))
				.addCriteria(Criteria.where("status").is("Active")).addCriteria(Criteria.where("isJoined").is(true))
				.addCriteria(Criteria.where("isPrimaryDepartment").is(true));
		query.fields().include("userId");
		List<Long> currentId = mongoTemplate.find(query, UserWorkspaces.class).stream().map(UserWorkspaces::getUserId)
				.collect(Collectors.toList());
		return currentId;
	}

	@Override
	public List<Long> findByUserIdAndDepartmentId(List<Long> userId, Long departmentId) {

		Query query = new Query();
		query.addCriteria(Criteria.where("userId").in(userId))
				.addCriteria(Criteria.where("workspaceId").is(departmentId));
		query.fields().include("userId");
		List<Long> currentId = mongoTemplate.find(query, UserWorkspaces.class).stream().map(UserWorkspaces::getUserId)
				.collect(Collectors.toList());
		return currentId;
	}

	@Override
	public List<UserWorkspaces> findByUserIdsAndDepartmentId(List<Long> userId, Long departmentId) {

		Query query = new Query();
		query.addCriteria(Criteria.where("userId").in(userId))
				.addCriteria(Criteria.where("workspaceId").is(departmentId));
		query.fields().include("userId");
		query.fields().include("staffUserCoreTypeId");
		return mongoTemplate.find(query, UserWorkspaces.class);
	}

	@Override
	public List<UserWorkspaces> findByStaffUserCoreTypeIdAndUserIdAndWorkspaceId(long staffUserCoreTypeId,
			List<Long> usersList, long workspaceId) {

		Query query = new Query();
		query.addCriteria(Criteria.where("userId").in(usersList))
				.addCriteria(Criteria.where("staffUserCoreTypeId").is(staffUserCoreTypeId))
				.addCriteria(Criteria.where("status").is("Active"))
				.addCriteria(Criteria.where("workspaceId").is(workspaceId));
		return mongoTemplate.find(query, UserWorkspaces.class);

	}

	@Override
	public Set<Long> findByDepartmentIdAndShiftId(long departmentId, long shiftId) {

		Query query = new Query();
		query.addCriteria(Criteria.where("departmentShiftIds").in(shiftId))
				.addCriteria(Criteria.where("status").is("Active"))
				.addCriteria(Criteria.where("workspaceId").is(departmentId))
				.addCriteria(Criteria.where("isJoined").is(true));
		query.fields().include("userId");
		Set<Long> currentId = mongoTemplate.find(query, UserWorkspaces.class).stream().map(UserWorkspaces::getUserId)
				.collect(Collectors.toSet());
		return currentId;
	}

	@Override
	public List<Long> findStaffCoreTypeIdsByWorkspaceId(long departmentId) {

		Query query = new Query();
		query.addCriteria(Criteria.where("workspaceId").is(departmentId));
		query.fields().include("staffUserCoreTypeId");
		return mongoTemplate.find(query, UserWorkspaces.class).stream().map(UserWorkspaces::getStaffUserCoreTypeId)
				.collect(Collectors.toList());
	}

	@Override
	public List<UserWorkspaces> findByUserIdAndBusinessIds(long userId, List<Long> businessIds) {
		Query query = new Query();
		query.addCriteria(Criteria.where("businessId").in(businessIds))
				.addCriteria(Criteria.where("status").is("Active")).addCriteria(Criteria.where("isJoined").is(true))
				.addCriteria(Criteria.where("userId").is(userId));
		return mongoTemplate.find(query, UserWorkspaces.class);
	}

	@Override
	public List<UserWorkspaces> findUserWorkspacesByStaffUserCoreTypeIdsAndDepartmentId(List<Long> staffRoles,
			long departmentId) {

		Query query = new Query();
		query.addCriteria(Criteria.where("staffUserCoreTypeId").in(staffRoles))
				.addCriteria(Criteria.where("workspaceId").is(departmentId))
				.addCriteria(Criteria.where("status").is("Active")).addCriteria(Criteria.where("isJoined").is(true))
				.addCriteria(Criteria.where("isPrimaryDepartment").is(true));
		query.fields().include("userId");
		query.fields().include("departmentShiftIds");
		return mongoTemplate.find(query, UserWorkspaces.class);
	}

	@Override
	public List<UserWorkspaces> findUserWorkspacesByStaffUserCoreTypeIdsAndDepartmentIdAndShiftIds(
			List<Long> staffRoles, long departmentId, List<Long> departmentShiftIds) {

		Query query = new Query();
		query.addCriteria(Criteria.where("staffUserCoreTypeId").in(staffRoles))
				.addCriteria(Criteria.where("workspaceId").is(departmentId))
				.addCriteria(Criteria.where("status").is("Active")).addCriteria(Criteria.where("isJoined").is(true))
				.addCriteria(Criteria.where("isPrimaryDepartment").is(true))
				.addCriteria(Criteria.where("departmentShiftIds").in(departmentShiftIds));
		query.fields().include("userId");
		query.fields().include("departmentShiftIds");
		return mongoTemplate.find(query, UserWorkspaces.class);
	}

	@Override
	public List<UserWorkspaces> findByStaffUserCoreTypeIdAndBusinessId(long staffUserCoreTypeId, long businessId) {

		Query query = new Query();
		query.addCriteria(Criteria.where("staffUserCoreTypeId").is(staffUserCoreTypeId))
				.addCriteria(Criteria.where("businessId").is(businessId));
		return mongoTemplate.find(query, UserWorkspaces.class).stream()
				.collect(collectingAndThen(
						toCollection(() -> new TreeSet<UserWorkspaces>(comparingLong(UserWorkspaces::getUserId))),
						ArrayList::new));
	}

	@Override
	public List<UserWorkspaces> findByUserIds(List<Long> userIds) {

		Query query = new Query();
		query.addCriteria(Criteria.where("userId").in(userIds)).addCriteria(Criteria.where("status").is("Active"))
				.addCriteria(Criteria.where("isJoined").is(true));
		List<UserWorkspaces> users = mongoTemplate.find(query, UserWorkspaces.class);
		return users.stream()
				.collect(collectingAndThen(
						toCollection(() -> new TreeSet<UserWorkspaces>(comparingLong(UserWorkspaces::getUserId))),
						ArrayList::new));
	}

	@Override
	public List<UserWorkspaces> findByStaffUserCoreTypeIdsAndShiftTypeAndDepartmentId(List<Long> staffCoreTypes,
			List<Long> shiftTypes, long departmentId) {

		Criteria criteria1 = Criteria.where("staffUserCoreTypeId").in(staffCoreTypes);
		Criteria criteria2 = Criteria.where("departmentShiftIds").in(shiftTypes);
		Criteria criteria3 = Criteria.where("isJoined").is(true);
		Query query = new Query();
		query.addCriteria(Criteria.where("status").is("Active"))
				.addCriteria(Criteria.where("workspaceId").is(departmentId))
				.addCriteria(criteria3.andOperator(criteria1, criteria2));

		return mongoTemplate.find(query, UserWorkspaces.class);
	}

	@Override
	public List<Long> findByBusinessId(long worksiteId) {

		Query query = new Query();
		query.addCriteria(Criteria.where("businessId").is(worksiteId))
				.addCriteria(Criteria.where("status").is("Active"));
		return mongoTemplate.findDistinct(query, "userId", UserWorkspaces.class, Long.class);
	}

	@Override
	public List<UserWorkspaces> findUserIdsByOrganizationId(long organizationId) {

		Aggregation aggregation = Aggregation.newAggregation(
				Aggregation.match(new Criteria().andOperator(Criteria.where("organizationId").is(organizationId),
						Criteria.where("status").is("Active"), Criteria.where("staffUserCoreTypeId").is(1))),
				group("userId").first("userId").as("userId"));
		return mongoTemplate.aggregate(aggregation, UserWorkspaces.class, UserWorkspaces.class).getMappedResults();
	}

	@Override
	public void updateStatusOfUser(long userId, String status) {

		Query query = new Query();
		query.addCriteria(Criteria.where("userId").is(userId))
				.addCriteria(Criteria.where("status").ne("Pending Workspace"));

		mongoTemplate.updateMulti(query, Update.update("status", status), UserWorkspaces.class);
	}

	@Override
	public void updateStatusOfUserInWorkspace(long userId, String status, long organizationId) {

		Query query = new Query();
		query.addCriteria(Criteria.where("userId").is(userId))
				.addCriteria(Criteria.where("status").ne("Pending Workspace"))
				.addCriteria(Criteria.where("organizationId").is(organizationId));

		mongoTemplate.updateMulti(query, Update.update("status", status), UserWorkspaces.class);
	}

	@Override
	public List<UserWorkspaces> findByStaffUserCoreTypeIdAndOrganizationId(List<Long> staffUserCoreTypeIds,
			long organizationId) {
		if (organizationId > 0) {

			Query query = new Query();
			query.addCriteria(Criteria.where("staffUserCoreTypeId").in(staffUserCoreTypeIds))
					.addCriteria(Criteria.where("status").is("Active"))
					.addCriteria(Criteria.where("organizationId").is(organizationId));

			return mongoTemplate.find(query, UserWorkspaces.class);

		} else {
			Query query = new Query();
			query.addCriteria(Criteria.where("staffUserCoreTypeId").in(staffUserCoreTypeIds))
					.addCriteria(Criteria.where("status").is("Active"));

			return mongoTemplate.find(query, UserWorkspaces.class);
		}

	}

	@Override
	public void updateDepartmentShiftIdOfUsers(long oldDepartmentShiftId, long newDepartmentShiftId) {

		Query query = new Query();
		query.addCriteria(Criteria.where("departmentShiftIds").in(oldDepartmentShiftId));

		mongoTemplate.updateMulti(query, Update.update("departmentShiftIds", newDepartmentShiftId),
				UserWorkspaces.class);
	}

	@Override
	public List<DepartmentShiftsResponse> findByUserIdAndStatusAndIsJoined(long userId, String status, boolean isJoined,
			long departmentShiftId) {
		Aggregation agg = Aggregation.newAggregation(
				Aggregation.lookup("department_shifts", "departmentShiftId", "_id", "departmentShifts"),
				Aggregation.match(Criteria.where("userId").is(userId).andOperator(Criteria.where("status").is(status),
						Criteria.where("isJoined").is(isJoined),
						Criteria.where("departmentShiftIds").nin(departmentShiftId))),
				Aggregation.project("departmentShifts"));

		AggregationResults<DepartmentShiftsResponse> response = mongoTemplate.aggregate(agg, UserWorkspaces.class,
				DepartmentShiftsResponse.class);

		return response.getMappedResults();
	}

	@Override
	public List<BusinessesResponse> findAvailableUsers(List<String> weekDay, String endTime, String startTime, long staffUserCoreTypeId, List<Long> userIds) {
		
		Criteria criteria2 = Criteria.where("worksiteAvailability").elemMatch(new Criteria().andOperator(Criteria.where("startAndEndTime.startTime").lte(startTime).lte(endTime), 
				Criteria.where("startAndEndTime.endTime").gte(startTime).gte(endTime), 
				Criteria.where("weekDay").in(weekDay)));

		Criteria criteria3 = Criteria.where("worksiteAvailability").elemMatch(new Criteria().andOperator(Criteria.where("startAndEndTime.startTime").lte(startTime).lte(endTime), 
				Criteria.where("startAndEndTime.endTime").gte(startTime).lte(endTime), 
				Criteria.where("weekDay").in(weekDay)));

		Criteria criteria4 = Criteria.where("worksiteAvailability").elemMatch(new Criteria().andOperator(Criteria.where("startAndEndTime.startTime").gte(startTime).lte(endTime), 
				Criteria.where("startAndEndTime.endTime").gte(startTime).gte(endTime),
				Criteria.where("weekDay").in(weekDay)));

		Criteria criteria5 = Criteria.where("worksiteAvailability").elemMatch(new Criteria().andOperator(Criteria.where("startAndEndTime.startTime").gte(startTime).lte(endTime), 
				Criteria.where("startAndEndTime.endTime").gte(startTime).lte(endTime),
				Criteria.where("weekDay").in(weekDay)));

		Criteria criteriaFinal = new Criteria().orOperator(criteria2, criteria3, criteria4, criteria5);

		Aggregation aggregation = Aggregation.newAggregation(
				Aggregation.match(criteriaFinal),
				Aggregation.lookup("user_workspaces", "_id", "businessId", "userWorkspaces"),
				Aggregation.unwind("userWorkspaces"),
				Aggregation.project("businessId").andInclude("userWorkspaces").andInclude("preferredProviderIds"),
				Aggregation.match(new Criteria().andOperator(
						Criteria.where("userWorkspaces.staffUserCoreTypeId").is(staffUserCoreTypeId),
						Criteria.where("userWorkspaces.userId").in(userIds),
						Criteria.where("userWorkspaces.userAvailability").ne(null),
						Criteria.where("userWorkspaces.userAvailability")
								.elemMatch(new Criteria().andOperator(Criteria.where("weekDay").in(weekDay), Criteria.where("type").regex("clinic", "i"))))));

		AggregationResults<BusinessesResponse> response;

		try {
			response = mongoTemplate.aggregate(aggregation, Businesses.class, BusinessesResponse.class);
			return response.getMappedResults();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<UserWorkspaces> findAvailableUsersByWeekdays(List<String> weekDays) {

		Query query = new Query();
		query.addCriteria(new Criteria().andOperator(Criteria.where("userAvailability").exists(true),
				Criteria.where("userAvailability.weekDay").in(weekDays),
				Criteria.where("userAvailability.type").regex("Clinic", "i")));
		query.fields().include("userId");
		try {
			Iterator<Long> files = mongoTemplate.findDistinct(query, "userId", UserWorkspaces.class, Long.class)
					.iterator();
			List<UserWorkspaces> userWorkspaces = new ArrayList<>();
			while (files.hasNext()) {
				UserWorkspaces userWorkspace = new UserWorkspaces();
				userWorkspace.setUserId(files.next());
				userWorkspaces.add(userWorkspace);
			}
			return userWorkspaces;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<Long> findByBusinessIdAndStaffUserCoreTypeId(long worksiteId, long staffUserCoreTypeId) {
		Query query = new Query();
		query.addCriteria(new Criteria().andOperator(Criteria.where("businessId").is(worksiteId),
				Criteria.where("staffUserCoreTypeId").is(staffUserCoreTypeId)));
		return mongoTemplate.findDistinct(query, "userId", UserWorkspaces.class, Long.class);
	}

	@Override
	public List<Long> findBusinessesIdsByUserId(long userId) {
		Query query = new Query();
		query.addCriteria(new Criteria().andOperator(Criteria.where("userId").is(userId),
				Criteria.where("status").ne("Inactive")));
		return mongoTemplate.findDistinct(query, "businessId", UserWorkspaces.class, Long.class);
	}
}