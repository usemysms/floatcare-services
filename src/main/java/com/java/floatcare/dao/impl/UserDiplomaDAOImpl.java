package com.java.floatcare.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.java.floatcare.dao.UserDiplomaDAO;
import com.java.floatcare.model.UserDiploma;

@Repository
public class UserDiplomaDAOImpl implements UserDiplomaDAO {

	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public UserDiploma findUserDiplomaById(long userDiplomaId) {

		return mongoTemplate.findOne(Query.query(Criteria.where("userDiplomaId").is(userDiplomaId)), UserDiploma.class);
	}

	@Override
	public List<UserDiploma> findListOfDiplomaByUserId(long userId) {

		return mongoTemplate.find(Query.query(Criteria.where("userId").is(userId)), UserDiploma.class);
	}
}
