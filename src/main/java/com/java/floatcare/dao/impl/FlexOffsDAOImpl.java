package com.java.floatcare.dao.impl;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.java.floatcare.dao.FlexOffsDAO;
import com.java.floatcare.model.FlexOffs;

@Repository
public class FlexOffsDAOImpl implements FlexOffsDAO{

	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Override
	public List<FlexOffs> findByWorksiteIdAndFromSelectedDate(long workspaceId, LocalDate selectedDate) {

		Query query = new Query();
		query.addCriteria(Criteria.where("departmentId").is(workspaceId)).addCriteria(Criteria.where("onDate").is(selectedDate));
		return mongoTemplate.find(query, FlexOffs.class);
	}

}
