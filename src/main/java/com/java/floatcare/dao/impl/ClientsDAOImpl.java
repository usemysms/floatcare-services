package com.java.floatcare.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.java.floatcare.dao.ClientsDAO;
import com.java.floatcare.model.Clients;
import com.java.floatcare.model.Household;

@Repository
public class ClientsDAOImpl implements ClientsDAO{

	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Override
	public Clients getClientProfileById(long clientId) {
		
		Query query = new Query();
		query.addCriteria(Criteria.where("clientId").is(clientId));
		return mongoTemplate.findOne(query, Clients.class);
	}

	@Override
	public List<Clients> getClientsByBusinessId(long businessId) {
		
		Query query = new Query();
		query.addCriteria(Criteria.where("businessId").is(businessId));
		return mongoTemplate.find(query, Clients.class);
	}

	@Override
	public Household getHouseholdByClientId(long clientId) {
		
		Query query = new Query();
		query.addCriteria(Criteria.where("clientId").is(clientId));
		return mongoTemplate.findOne(query, Household.class);
	}

}
