package com.java.floatcare.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.java.floatcare.dao.RequestConversationDAO;
import com.java.floatcare.model.RequestConversation;


@Repository
public class RequestConversationDAOImpl implements RequestConversationDAO{

	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public List<RequestConversation> findMessagesBySenderIdAndReceiverId(long senderId, long receiverId) {
		
		Query query = new Query();
		query.addCriteria(Criteria.where("senderId").in(senderId, receiverId)).addCriteria(Criteria.where("receiverId").in(receiverId, senderId));
		return mongoTemplate.find(query, RequestConversation.class);
	}
}
