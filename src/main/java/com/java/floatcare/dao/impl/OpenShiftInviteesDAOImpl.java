package com.java.floatcare.dao.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.java.floatcare.dao.OpenShiftInviteesDAO;
import com.java.floatcare.model.OpenShiftInvitees;

@Repository
public class OpenShiftInviteesDAOImpl implements OpenShiftInviteesDAO {

	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public List<OpenShiftInvitees> findOpenShiftInviteesByUserId(long userId) {

		Query query = new Query();
		query.addCriteria(Criteria.where("userId").is(userId)).addCriteria(Criteria.where("isAccepted").is(false));
		return mongoTemplate.find(query, OpenShiftInvitees.class);
	}

	@Override
	public List<OpenShiftInvitees> findInviteesByOpenShiftIdAndIsApproved(List<Long> openShiftIds, boolean isApproved) {

		Query query = new Query();
		query.addCriteria(Criteria.where("openShiftId").in(openShiftIds))
				.addCriteria(Criteria.where("isApproved").is(isApproved))
				.addCriteria(Criteria.where("isAccepted").is(true));
		return mongoTemplate.find(query, OpenShiftInvitees.class);
	}

	@Override
	public List<OpenShiftInvitees> findInviteesByOpenShiftIds(List<Long> openShiftsId) {

		Query query = new Query();
		query.addCriteria(Criteria.where("openShiftId").in(openShiftsId));
		query.fields().include("openShiftId");
		return mongoTemplate.find(query, OpenShiftInvitees.class);
	}

	@Override
	public OpenShiftInvitees findByOpenShiftIdAndUserIdAndIsApproved(long openShiftId, long userId,
			boolean isApproved) {

		Query query = new Query();
		query.addCriteria(Criteria.where("openShiftId").in(openShiftId))
				.addCriteria(Criteria.where("userId").is(userId))
				.addCriteria(Criteria.where("isApproved").is(isApproved));
		return mongoTemplate.findOne(query, OpenShiftInvitees.class);
	}

	@Override
	public List<OpenShiftInvitees> findByOpenShiftIdAndIsApproved(long openShiftId, boolean isApproved) {
		Query query = new Query();
		query.addCriteria(Criteria.where("openShiftId").in(openShiftId))
				.addCriteria(Criteria.where("isApproved").is(isApproved));
		query.fields().include("openShiftId");
		return mongoTemplate.find(query, OpenShiftInvitees.class);
	}

	@Override
	public List<OpenShiftInvitees> findByOpenShiftIdAndIsAccepted(long openShiftId, boolean isAccepted) {
		Query query = new Query();
		query.addCriteria(Criteria.where("openShiftId").in(openShiftId))
				.addCriteria(Criteria.where("isAccepted").is(isAccepted));
		query.fields().include("openShiftId");
		return mongoTemplate.find(query, OpenShiftInvitees.class);
	}

	@Override
	public List<Long> findByUserIdAndIsApproved(Long userId, boolean isApproved) {

		Query query = new Query();
		query.addCriteria(Criteria.where("userId").is(userId)).addCriteria(Criteria.where("isApproved").is(isApproved));
		query.fields().include("openShiftId");
		return mongoTemplate.find(query, OpenShiftInvitees.class).stream().map(OpenShiftInvitees::getOpenShiftId)
				.collect(Collectors.toList());
	}

	@Override
	public void removeByOpenShiftIds(List<Long> openShiftIds) {
		Query query = new Query();
		query.addCriteria(Criteria.where("openShiftId").in(openShiftIds));
		mongoTemplate.findAndRemove(query, OpenShiftInvitees.class);
	}
}
