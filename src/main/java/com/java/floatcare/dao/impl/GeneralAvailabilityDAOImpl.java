package com.java.floatcare.dao.impl;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.java.floatcare.dao.GeneralAvailabilityDAO;
import com.java.floatcare.model.GeneralAvailability;

@Repository
public class GeneralAvailabilityDAOImpl implements GeneralAvailabilityDAO {

	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public List<Long> findGeneralAvailabilityOfUsersByStartDate(LocalDate startDate) {

		Query query = new Query();
		query.addCriteria(Criteria.where("startDate").lte(startDate).orOperator(Criteria.where("endDate").gte(startDate)));
		query.fields().include("userId");
		return mongoTemplate.find(query, GeneralAvailability.class).stream().map(emitter -> emitter.getUserId()).collect(Collectors.toList());		//fetch all availabile user from startDate to endDate
	}

	@Override
	public List<Long> findGeneralAvailabilityOfUsersByUserIdsStartDate(Set<Long> usersList, LocalDate onDate) {
		Query query = new Query();
		query.addCriteria(Criteria.where("startDate").lte(onDate).orOperator(Criteria.where("endDate").gte(onDate)));
		query.addCriteria(Criteria.where("userId").in(usersList));
		query.fields().include("userId");
		mongoTemplate.indexOps(GeneralAvailability.class);
		return mongoTemplate.findDistinct(query,"userId", GeneralAvailability.class, Long.class);
	}

	@Override
	public List<GeneralAvailability> findGeneralAvailabilityOfUsersByUserIdStartDate(long userId, LocalDate shiftDate) {
		
		Query query = new Query();
		query.addCriteria(Criteria.where("userId").is(userId)).addCriteria(Criteria.where("startDate").lte(shiftDate).orOperator(Criteria.where("endDate").gte(shiftDate)));
		return mongoTemplate.find(query, GeneralAvailability.class);
	}
}