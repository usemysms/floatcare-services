package com.java.floatcare.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.java.floatcare.dao.DepartmentDAO;
import com.java.floatcare.model.Department;

@Repository
public class DepartmentDAOImpl implements DepartmentDAO {

	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Override
	public List<Department> getDepartmentByBusinessIds(List<Long> businessIds) {
		Query query = new Query();
		query.addCriteria(Criteria.where("businessId").in(businessIds));
		query.fields().include("departmentTypeName").include("departmentId").include("address").include("businessId");
		return mongoTemplate.find(query, Department.class);
	}

	@Override
	public Department getDepartmentByBusinessIdAndDepartmentName(long businessId, String departmentName) {
		
		Criteria businessIdCriteria = Criteria.where("businessId").is(businessId);
		Criteria departmentNameCriteria = Criteria.where("departmentTypeName").regex(departmentName, "i");
		return mongoTemplate.findOne(new Query().addCriteria(businessIdCriteria).addCriteria(departmentNameCriteria), Department.class);
	}
}
