package com.java.floatcare.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.java.floatcare.api.request.WorksiteAvailability;
import com.java.floatcare.api.response.pojo.BusinessesResponse;
import com.java.floatcare.dao.BusinessesDAO;
import com.java.floatcare.model.Businesses;

@Repository
public class BusinessesDAOImpl implements BusinessesDAO {

	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public List<Long> getBusinessesIdByOrganizationId(long organizationId) {
		Query query = new Query();
		query.addCriteria(Criteria.where("organizationId").is(organizationId));
		query.fields().include("name").include("businessId").include("status");
		return mongoTemplate.find(query, Businesses.class).stream().map(emitter -> emitter.getBusinessId())
				.collect(Collectors.toList());
	}

	@Override
	public Businesses getBusinessByOrganizationIdAndBusinessLocationAndStateAndZip(long organizationId,
			String businessLocation, String city, String state, String zip) {

		Criteria organizationIdCriteria = Criteria.where("organizationId").is(organizationId);
		Criteria businessLocationCriteria = Criteria.where("name").regex(businessLocation, "i");
		Criteria cityCriteria = Criteria.where("city").regex(city, "i");
		Criteria stateCriteria = Criteria.where("state").regex(state, "i");
		Criteria zipCriteria = Criteria.where("postalCode").regex(zip, "i");
		return mongoTemplate.findOne(
				new Query().addCriteria(organizationIdCriteria).addCriteria(businessLocationCriteria)
						.addCriteria(cityCriteria).addCriteria(stateCriteria).addCriteria(zipCriteria),
				Businesses.class);
	}

	@Override
	public long updateWorksiteAvailability(long businessId, List<WorksiteAvailability> worksiteAvailability) {

		Query query = new Query();
		query.addCriteria(Criteria.where("businessId").is(businessId));
		Update update = new Update();
		update.set("worksiteAvailability", worksiteAvailability);
		return mongoTemplate.updateFirst(query, update, Businesses.class).getMatchedCount();
	}

	@Override
	public List<BusinessesResponse> findAvailableUsers(long businessId, String weekDay, String startTime,
			String endTime, long staffUserCoreTypeId) {

		Aggregation aggregation = Aggregation.newAggregation(
				Aggregation.match(Criteria.where("worksiteAvailability")
						.elemMatch(Criteria.where("startAndEndTime.startTime").lte(startTime)
								.andOperator(Criteria.where("startAndEndTime.endTime").gte(endTime),
										Criteria.where("startAndEndTime.startTime").lte(endTime))
								.and("weekDay").regex(weekDay, "i"))),
				Aggregation.lookup("user_workspaces", "_id", "businessId", "userWorkspaces"),
				Aggregation.unwind("userWorkspaces"), Aggregation.project("businessId").andInclude("userWorkspaces"),
				Aggregation.match(new Criteria().andOperator(Criteria.where("_id").is(businessId),
						Criteria.where("userWorkspaces.userAvailability").ne(null),
						Criteria.where("staffUserCoreTypeId").is(staffUserCoreTypeId),
						Criteria.where("userWorkspaces.userAvailability").elemMatch(
								Criteria.where("weekDay").regex(weekDay, "i").and("type").regex("Clinic", "i")))));

		AggregationResults<BusinessesResponse> response = mongoTemplate.aggregate(aggregation, Businesses.class,
				BusinessesResponse.class);
		return response.getMappedResults();
	}

	@Override
	public List<Businesses> searchFacilities(long businessTypeId, String name, String city, String state, String zip) {

		Query query = new Query();

		try {
			List<Criteria> criteria = new ArrayList<Criteria>();
			if (businessTypeId > 0)
				criteria.add(Criteria.where("businessTypeId").is(businessTypeId));
			if (name != null && !name.isEmpty())
				criteria.add(Criteria.where("name").regex("^" + name, "i"));
			if (city != null && !city.isEmpty())
				criteria.add(Criteria.where("city").regex("^" + city, "i"));
			if (state != null && !state.isEmpty())
				criteria.add(Criteria.where("state").regex("^" + state, "i"));
			if (zip != null && !zip.isEmpty())
				criteria.add(Criteria.where("postalCode").is(zip));
			if (criteria.size() > 0)
				query.addCriteria(new Criteria().andOperator(criteria.toArray(new Criteria[criteria.size()])));
			return mongoTemplate.find(query, Businesses.class);
		} catch (Exception e) {

			return null;
		}	}
}
