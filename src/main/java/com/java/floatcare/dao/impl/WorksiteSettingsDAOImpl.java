package com.java.floatcare.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.java.floatcare.dao.WorksiteSettingsDAO;
import com.java.floatcare.model.WorksiteSettings;

@Repository
public class WorksiteSettingsDAOImpl implements WorksiteSettingsDAO {

	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Override
	public Map<Long, WorksiteSettings> findWorksiteSettingsByUserIdAndWorksiteIds(long userId, List<Long> worksiteIds) {
		
		Query query = new Query();
		query.addCriteria(Criteria.where("userId").is(userId)).addCriteria(Criteria.where("worksiteId").in(worksiteIds));
		List<WorksiteSettings> worksiteSettingsList = mongoTemplate.find(query, WorksiteSettings.class);
		
		Map<Long, WorksiteSettings> worksiteSettings = new HashMap<>();
		
		for (WorksiteSettings each : worksiteSettingsList)
			worksiteSettings.put(each.getWorksiteId(), each);
		
		return worksiteSettings;
	}
}
