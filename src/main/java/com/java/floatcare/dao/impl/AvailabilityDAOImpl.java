package com.java.floatcare.dao.impl;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.java.floatcare.dao.AvailabilityDAO;
import com.java.floatcare.model.Availability;

@Repository
public class AvailabilityDAOImpl implements AvailabilityDAO {

	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public List<Availability> findAvailableUsersByStartDate(long userId, LocalDate startDate) {

		Query query = new Query();
		query.addCriteria(Criteria.where("userId").is(userId)).addCriteria((Criteria.where("startDate").lte(startDate).orOperator(Criteria.where("endDate").gte(startDate))));
		return mongoTemplate.find(query, Availability.class);		//fetch all availability of user from startDate to endDate
	}
	
	public List<Availability> findAvailableUsersByOnDateAndScheduleLayoutId(long userId, LocalDate startDate, long scheduleLayoutId) {

		Query query = new Query();
		query.addCriteria(Criteria.where("userId").is(userId)).addCriteria((Criteria.where("onDate").in(startDate))).addCriteria(Criteria.where("scheduleLayoutId").is(scheduleLayoutId));
		return mongoTemplate.find(query, Availability.class);		//fetch all availability of user from startDate to endDate
	}

	@Override
	public List<Availability> findAvailableUsersByScheduleDates(long userId, LocalDate scheduleStartDate, LocalDate scheduleEndDate) {
		
		Query query = new Query();
		query.addCriteria(Criteria.where("userId").is(userId))
			.addCriteria((Criteria.where("startDate").lte(scheduleStartDate).orOperator(Criteria.where("endDate").gte(scheduleEndDate))));
		return mongoTemplate.find(query, Availability.class);		//fetch all availability of user from startDate to endDate
	}
	
	@Override
	public List<Availability> findAvailableUsersByOnDate(long userId, LocalDate startDate) {

		Query query = new Query();
		query.addCriteria(Criteria.where("userId").is(userId)).addCriteria((Criteria.where("onDate").in(startDate)));
		return mongoTemplate.find(query, Availability.class);		//fetch all availability of user from startDate to endDate
	}
	
	@Override
	public List<String> findAvailableUserByShiftDate(List<Long> userId, LocalDate startDate, LocalDate endDate) {

		Query query = new Query();
		query.addCriteria(Criteria.where("userId").in(userId)).addCriteria((Criteria.where("onDate").gte(startDate)).lte(endDate));
		query.fields().include("userId");
		query.fields().include("onDate");
		return mongoTemplate.find(query, Availability.class).stream().map(emitter -> emitter.getOnDate().stream().map(emitter1-> emitter.getUserId()+" "+emitter1).collect(Collectors.toList())).flatMap(Collection::stream).collect(Collectors.toList());		//fetch all availability of user from startDate to endDate
	}

	@Override
	public List<String> findAvailableUserByShiftDateAndScheduleLayoutId(List<Long> usersList, LocalDate startDate,LocalDate endDate, long scheduleLayoutId) {
		Query query = new Query();
		query.addCriteria(Criteria.where("userId").in(usersList)).addCriteria((Criteria.where("onDate").gte(startDate)).lte(endDate)).addCriteria(Criteria.where("scheduleLayoutId").is(scheduleLayoutId));
		query.fields().include("userId");
		query.fields().include("onDate");
		return mongoTemplate.find(query, Availability.class).stream().map(emitter -> emitter.getOnDate().stream().map(emitter1-> emitter.getUserId()+" "+emitter1).collect(Collectors.toList())).flatMap(Collection::stream).collect(Collectors.toList());
	}

	@Override
	public Availability findByUserIdAndScheduleLayoutId(long userId, Long scheduleLayoutId) {
		Query query = new Query();
		query.addCriteria(Criteria.where("userId").is(userId)).addCriteria((Criteria.where("scheduleLayoutId").is(scheduleLayoutId)));
		query.fields().include("userId");
		query.fields().include("availabilityId");
		return mongoTemplate.findOne(query, Availability.class);
	}

	@Override
	public List<Availability> findByUserIdsAndSchedulelayoutId(Set<Long> userIds, List<Long> scheduleLayoutIds) {
		
		Query query = new Query();
		query.addCriteria(Criteria.where("userId").in(userIds)).addCriteria((Criteria.where("scheduleLayoutId").in(scheduleLayoutIds)));
		query.fields().include("userId");
		return mongoTemplate.find(query, Availability.class);
	}	
}