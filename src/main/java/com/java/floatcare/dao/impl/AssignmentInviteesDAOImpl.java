package com.java.floatcare.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.java.floatcare.dao.AssignmentInviteesDAO;
import com.java.floatcare.model.AssignmentInvitees;

@Repository
public class AssignmentInviteesDAOImpl implements AssignmentInviteesDAO {

	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public List<AssignmentInvitees> findByUserIdAndAssignmentIdsAndIsApproved(long userId, List<Long> assignmentIds, boolean isApproved) {
		
		Query query = new Query();
		query.addCriteria(Criteria.where("assignmentId").in(assignmentIds));
		query.addCriteria(Criteria.where("staffId").is(userId));
		query.addCriteria(Criteria.where("isApproved").is(isApproved));
		return mongoTemplate.find(query, AssignmentInvitees.class);
	}
	
	@Override
	public List<AssignmentInvitees> findByUserIdAndAssignmentIdsAndIsAccepted(long userId, List<Long> assignmentIds, boolean isAccepted) {
		
		Query query = new Query();
		query.addCriteria(Criteria.where("assignmentId").in(assignmentIds));
		query.addCriteria(Criteria.where("staffId").is(userId));
		query.addCriteria(Criteria.where("isAccepted").is(isAccepted));
		return mongoTemplate.find(query, AssignmentInvitees.class);
	}

	@Override
	public Map<Long, AssignmentInvitees> findByStaffIdsAndAssignmentIdAndIsApproved(List<Long> userIds, long assignmentId, boolean isApproved) {
		
		Query query = new Query();
		query.addCriteria(Criteria.where("assignmentId").is(assignmentId));
		query.addCriteria(Criteria.where("staffId").in(userIds));
		query.addCriteria(Criteria.where("isApproved").is(isApproved));
		
		List<AssignmentInvitees> assignmentInviteeList = mongoTemplate.find(query, AssignmentInvitees.class);
		Map<Long, AssignmentInvitees> assignmentInvitees = new HashMap<>();
		
		for (AssignmentInvitees each : assignmentInviteeList) {
			assignmentInvitees.put(each.getStaffId(), each);
		}
		return assignmentInvitees;
	}
}