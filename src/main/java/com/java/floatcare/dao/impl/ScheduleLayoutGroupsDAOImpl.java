package com.java.floatcare.dao.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.java.floatcare.dao.ScheduleLayoutGroupsDAO;
import com.java.floatcare.model.ScheduleLayout;
import com.java.floatcare.model.ScheduleLayoutGroups;

@Repository
public class ScheduleLayoutGroupsDAOImpl implements ScheduleLayoutGroupsDAO{

	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public List<Long> fetchScheduleLayoutIdByUserId(long userId) {
		
		Query query = new Query();
		query.addCriteria(Criteria.where("userIds").in(userId));
		query.fields().include("scheduleLayoutId");
		return mongoTemplate.find(query, ScheduleLayoutGroups.class).stream().map(ScheduleLayoutGroups::getScheduleLayoutId).collect(Collectors.toList());
	}

	@Override
	public List<ScheduleLayoutGroups> findUsersByScheduleLayoutId(List<Long> scheduleLayoutId) {
		
		Query query = new Query();
		query.addCriteria(Criteria.where("scheduleLayoutId").in(scheduleLayoutId));
		query.fields().include("userIds");
		query.fields().include("scheduleLayoutId");
		return mongoTemplate.find(query, ScheduleLayoutGroups.class);
	}

	@Override
	public void removeByScheduleLayoutIds(List<Long> scheduleLayoutIds) {
		Query query = new Query();
		query.addCriteria(Criteria.where("scheduleLayoutId").in(scheduleLayoutIds));
		mongoTemplate.findAndRemove(query, ScheduleLayoutGroups.class);
	}

	@Override
	public List<ScheduleLayoutGroups> findScheduleLayoutByUserId(long userId) {

		Query query = new Query();
		query.addCriteria(Criteria.where("userIds").in(userId));
		query.fields().include("scheduleLayoutId");
		return mongoTemplate.find(query, ScheduleLayoutGroups.class);
	}
	
	@Override
	public List<ScheduleLayout> findScheduleLayoutOfUserByDepartmentId(long userId, long departmentId) {
		Aggregation agg = Aggregation.newAggregation(Aggregation.lookup("schedule_layout", "scheduleLayoutId", "_id", "scheduleLayout"),
		        Aggregation.match(Criteria.where("userIds").in(userId).andOperator(Criteria.where("scheduleLayout.scheduleEndDate").gte(LocalDate.now()),
		        		Criteria.where("scheduleLayout.departmentId").is(departmentId), Criteria.where("scheduleLayout.status").is("Self Schedule"))),
		        Aggregation.project("scheduleLayout.scheduleLayoutId").andInclude("scheduleLayout.scheduleEndDate").andInclude("scheduleLayout.scheduleStartDate")
				.andInclude("scheduleLayout.departmentId").andInclude("scheduleLayout.shiftPlanningEndDate").andInclude("scheduleLayout.shiftPlanningStartDate"));
				
		AggregationResults<ScheduleLayout> result = mongoTemplate.aggregate(agg, ScheduleLayoutGroups.class, ScheduleLayout.class);
		return result.getMappedResults();
	}

	@Override
	public List<ScheduleLayout> findScheduleLayoutOfUserByDepartmentShiftId(long userId, long departmentShiftId) {
		Aggregation agg = Aggregation.newAggregation(Aggregation.lookup("schedule_layout", "scheduleLayoutId", "_id", "scheduleLayout"),
		        Aggregation.match(Criteria.where("userIds").in(userId).andOperator(Criteria.where("scheduleLayout.scheduleEndDate").gte(LocalDate.now()),
		        		Criteria.where("scheduleLayout.shiftTypes").in(departmentShiftId), Criteria.where("scheduleLayout.status").is("Self Schedule"))),
		        Aggregation.project("scheduleLayout.scheduleLayoutId").andInclude("scheduleLayout.scheduleEndDate").andInclude("scheduleLayout.scheduleStartDate")
					.andInclude("scheduleLayout.shiftPlanningEndDate").andInclude("scheduleLayout.shiftPlanningStartDate"));
				
		AggregationResults<ScheduleLayout> result = mongoTemplate.aggregate(agg, ScheduleLayoutGroups.class, ScheduleLayout.class);
		return result.getMappedResults();
	}
}
