package com.java.floatcare.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.java.floatcare.dao.WorkspaceSettingsDAO;
import com.java.floatcare.model.WorkspaceSettings;

@Repository
public class WorkspaceSettingsDAOImpl implements WorkspaceSettingsDAO{

	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public List<WorkspaceSettings> findWorkspaceSettingsByUserId(long userId) {
		
		Query query = new Query();
		query.addCriteria(Criteria.where("userId").is(userId));
		return mongoTemplate.find(query, WorkspaceSettings.class);
	}
}
