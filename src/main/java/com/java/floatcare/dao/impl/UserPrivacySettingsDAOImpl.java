package com.java.floatcare.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.java.floatcare.dao.UserPrivacySettingsDAO;
import com.java.floatcare.model.UserPrivacySettings;

@Repository
public class UserPrivacySettingsDAOImpl implements UserPrivacySettingsDAO{

	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public List<UserPrivacySettings> findUserPrivacySettingsByUserId(long userId) {
		Query query = new Query();
		query.addCriteria(Criteria.where("userId").is(userId));
		return mongoTemplate.find(query, UserPrivacySettings.class);
	}

}
