package com.java.floatcare.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.java.floatcare.api.response.pojo.BusinessesResponse;
import com.java.floatcare.dao.OrganizationDAO;
import com.java.floatcare.model.Businesses;
import com.java.floatcare.model.Organizations;
import com.java.floatcare.model.StaffUserCoreTypes;
import com.java.floatcare.model.UserOrganizations;
import com.java.floatcare.utils.ConstantUtils;

@Repository
public class OrganizationDAOImpl implements OrganizationDAO{

	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Override
	public Organizations findOrganizationByName (String name) {
		Criteria criteriaName = Criteria.where("name").regex(name, "i");
		return mongoTemplate.findOne(new Query().addCriteria(criteriaName), Organizations.class);
	}
	
	public List<Organizations> findAllOrganizationsByFirmType(String firmType){
		Query query = new Query();
		Criteria criteria;
		if(firmType.equals(ConstantUtils.lawFirm)) {
			criteria = Criteria.where("organizationType").in(firmType);
		} else {
			 
			Criteria criteria1 = Criteria.where("organizationType").is(ConstantUtils.MedicalFacility);
			Criteria criteria2 = Criteria.where("organizationType").is(null);
			criteria = new Criteria().orOperator(criteria1,criteria2);
		}
		
        return mongoTemplate.find(query.addCriteria(criteria), Organizations.class);
		
	}
	
	public List<Organizations> getUsersFromOrganizationName(String searchText) {

		if (searchText != null) {
			Aggregation  agg = Aggregation.newAggregation(Aggregation.lookup("organizations", "organizationId", "_id", "organizations"),
			Aggregation.match(Criteria.where("organizations.name").regex(searchText,"i")));
			List<Organizations> organizations =  mongoTemplate.aggregate(agg, UserOrganizations.class, Organizations.class).getMappedResults();
			return organizations;

		}else {
			return null;
		}
		
		
	}
	
	

}
