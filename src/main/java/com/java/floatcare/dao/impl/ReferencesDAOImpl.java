package com.java.floatcare.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.java.floatcare.dao.ReferencesDAO;
import com.java.floatcare.model.References;

@Repository
public class ReferencesDAOImpl implements ReferencesDAO {

	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public References findReferenceById(long referenceId) {

		return mongoTemplate.findOne(Query.query(Criteria.where("referenceId").is(referenceId)), References.class);
	}

	@Override
	public List<References> findListOfReferencesByUserId(long userId) {

		return mongoTemplate.find(Query.query(Criteria.where("userId").is(userId)), References.class);
	}
}
