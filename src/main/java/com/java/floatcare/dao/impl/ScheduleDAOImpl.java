package com.java.floatcare.dao.impl;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.java.floatcare.dao.ScheduleDAO;
import com.java.floatcare.model.Schedules;

@Repository
public class ScheduleDAOImpl implements ScheduleDAO {

	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public List<Schedules> findScheduleUsersByUserIdAndOnDate(long userId, LocalDate onDate) {
		
		Query query = new Query();
		Criteria dateC = Criteria.where("shiftDate").is(onDate);
		query.addCriteria(Criteria.where("userId").is(userId)).addCriteria(dateC).addCriteria(Criteria.where("status").is("Active"));
		return mongoTemplate.find(query, Schedules.class);
	}
	
	@Override
	public Schedules findByUserIdAndDepartmentId(long userId, long departmentId) {
		
		Query query = new Query();
		query.addCriteria(Criteria.where("userId").is(userId)).addCriteria(Criteria.where("departmentId").is(departmentId))
				.addCriteria(Criteria.where("status").is("Active"));
		query.with(Sort.by(Sort.Direction.DESC, "scheduleId"));
		query.limit(1);
		return mongoTemplate.findOne(query, Schedules.class);		
	}

	@Override
	public List<Schedules> findUserScheduleByWorkspaceIdUserIdAndOnDate(long workspaceId, long userId, LocalDate onDate) {

		Query query = new Query();
		Criteria dateC = Criteria.where("shiftDate").is(onDate);
		query.addCriteria(Criteria.where("userId").is(userId)).addCriteria(dateC);
		return mongoTemplate.find(query, Schedules.class);
	}

	@Override
	public List<String> findUserScheduleByWorkspaceIdsUserIdsListAndOnDate(long departmentId, List<Long> userId, LocalDate startDate, LocalDate endDate) {
		Query query = new Query();
        query.addCriteria(Criteria.where("userId").in(userId)).addCriteria(Criteria.where("departmentId").is(departmentId)).addCriteria((Criteria.where("shiftDate").gte(startDate).orOperator(Criteria.where("shiftDate").lte(endDate))))
        	.addCriteria(Criteria.where("status").is("Active")).addCriteria(Criteria.where("eventType").ne("flexedOff"));
		return mongoTemplate.find(query, Schedules.class).stream().map(emitter -> emitter.getDepartmentId()+" "+emitter.getUserId()+" "+ emitter.getShiftDate()+" "+ emitter.getStatus()).collect(Collectors.toList());
	}
	
	@Override
	public List<String> findScheduleUserByUserIdAndOnDate(long userId, LocalDate startDate, LocalDate endDate) {
		Query query = new Query();
        query.addCriteria(Criteria.where("userId").is(userId)).addCriteria((Criteria.where("shiftDate").gte(startDate).orOperator(Criteria.where("shiftDate").lte(endDate))));
		return mongoTemplate.find(query, Schedules.class).stream().map(emitter -> emitter.getUserId()+ " "+ emitter.getShiftDate()+ " "+ emitter.getStatus()).collect(Collectors.toList());
	}

	@Override
	public Schedules findScheduleByUserIdAndShiftDateAndDepartmentId(long userId, LocalDate shiftDate, long departmentId) {
		Query query = new Query();
        query.addCriteria(Criteria.where("userId").is(userId)).addCriteria((Criteria.where("shiftDate").is(shiftDate))).addCriteria(Criteria.where("departmentId").is(departmentId)).addCriteria(Criteria.where("status").is("Active"))
        .addCriteria(Criteria.where("eventType").ne("flexedOff"));
		return mongoTemplate.findOne(query, Schedules.class);
	}

	@Override
	public List<Long> findUsersByShiftDateAndDepartmentId(LocalDate shiftDate, long workspaceId) {
		
		Query query = new Query();
        query.addCriteria((Criteria.where("shiftDate").is(shiftDate))).addCriteria(Criteria.where("departmentId").is(workspaceId)).addCriteria(Criteria.where("status").is("Active"))
        	.addCriteria(Criteria.where("voluntaryLowCensus").is(false)).addCriteria(Criteria.where("eventType").ne("flexedOff"));
        query.fields().include("userId");
		return mongoTemplate.find(query, Schedules.class).stream().map(Schedules :: getUserId).collect(Collectors.toList());
	}
	@Override
	public List<Schedules> findSchedulesByShiftDateAndDepartmentId(LocalDate shiftDate, long workspaceId) {
		
		Query query = new Query();
        query.addCriteria((Criteria.where("shiftDate").is(shiftDate))).addCriteria(Criteria.where("departmentId").is(workspaceId)).addCriteria(Criteria.where("status").is("Active"));
        	//.addCriteria(Criteria.where("eventType").ne("flexedOff"));
        //query.fields().include("userId");
        //System.out.println(query.getQueryObject());
		return mongoTemplate.find(query, Schedules.class);
	}
	
	@Override
	public List<Long> findUsersByShiftDateAndDepartmentIdAndVoluntaryLowCensus(LocalDate shiftDate, long workspaceId, boolean voluntaryLowCensus) {
		
		Query query = new Query();
        query.addCriteria(Criteria.where("shiftDate").is(shiftDate)).addCriteria(Criteria.where("departmentId").is(workspaceId)).addCriteria(Criteria.where("voluntaryLowCensus").is(voluntaryLowCensus))
        	.addCriteria(Criteria.where("eventType").ne("flexedOff"));
        query.fields().include("userId");
		return mongoTemplate.find(query, Schedules.class).stream().map(Schedules :: getUserId).collect(Collectors.toList());
	}

	@Override
	public List<Schedules> findUserScheduleByWorkspaceIdsUserIdAndOnDate(List<Long> workspaceIds, long userId, LocalDate onDate) {
		Query query = new Query();
		Criteria dateC = Criteria.where("shiftDate").is(onDate);
		query.addCriteria(Criteria.where("userId").is(userId)).addCriteria(dateC).addCriteria(Criteria.where("departmentId").in(workspaceIds))
			.addCriteria(Criteria.where("eventType").ne("flexedOff"));
		return mongoTemplate.find(query, Schedules.class);
	}
	
	@Override
	public List<Schedules> findUserScheduleByWorkspaceIdsUserIdAndStartDateAndEndDate(List<Long> workspaceIds, long userId, LocalDate startDate, LocalDate endDate) {
		Query query = new Query();
		query.addCriteria(Criteria.where("userId").is(userId)).addCriteria(Criteria.where("departmentId").in(workspaceIds)).addCriteria((Criteria.where("shiftDate").gte(startDate).orOperator(Criteria.where("shiftDate").lte(endDate))))
			.addCriteria(Criteria.where("status").is("Active"));
		query.fields().include("userId");
		query.fields().include("shiftDate");
		query.fields().include("status");
		query.fields().include("departmentId");
		query.fields().include("shiftTypeId");
		return mongoTemplate.find(query, Schedules.class);
	}

	@Override
	public List<Long> findUsersByShiftDateAndDepartmentIdAndShiftId(LocalDate onDate, long departmentId, long shiftId) {
		Query query = new Query();
        query.addCriteria((Criteria.where("shiftDate").is(onDate))).addCriteria(Criteria.where("departmentId").is(departmentId)).addCriteria(Criteria.where("shiftTypeId").is(shiftId))
        	.addCriteria(Criteria.where("status").is("Active"));
        query.fields().include("userId");
		return mongoTemplate.find(query, Schedules.class).stream().map(Schedules :: getUserId).collect(Collectors.toList());
	}

	@Override
	public List<Schedules> findSchedulesByUserIdsAndDepartmentIdAndOnDate(List<Long> userIds, long departmentId, LocalDate onDate) {
		
		Query query = new Query();
		query.addCriteria(Criteria.where("userId").in(userIds)).addCriteria(Criteria.where("departmentId").is(departmentId)).addCriteria(Criteria.where("shiftDate").is(onDate))
			.addCriteria(Criteria.where("status").is("Active"));
		return mongoTemplate.find(query, Schedules.class);
	}

	@Override
	public List<Long> findUserSchedulesByShiftDateAndUserIds(LocalDate givenDate, Set<Long> userIds) {

		Query query = new Query();
		query.addCriteria(Criteria.where("userId").in(userIds)).addCriteria(Criteria.where("shiftDate").is(givenDate)).addCriteria(Criteria.where("status").is("Active"));
		query.fields().include("userId");
		return mongoTemplate.find(query, Schedules.class).stream().map(emitter -> emitter.getUserId()).collect(Collectors.toList());
	}

	@Override
	public List<Schedules> findUserScheduleByUserIdAndDepartmentIdAndEventType(long userId, long departmentId, String eventType, String status) {
		
		Query query = new Query();
		query.addCriteria(Criteria.where("userId").is(userId)).addCriteria(Criteria.where("departmentId").is(departmentId)).addCriteria(Criteria.where("status").is(status))
			.addCriteria(Criteria.where("eventType").is(eventType));
		query.fields().include("shiftDate");
		query.with(Sort.by(Sort.Direction.DESC, "scheduleId"));
		return mongoTemplate.find(query, Schedules.class);
	}

	@Override
	public List<Schedules> findUserScheduleByUserIdAndDepartmentIdAndEventTypeAndOnDate(Integer userId, long departmentId,
			String eventType, String status, LocalDate onDate) {
		
		Query query = new Query();
		query.addCriteria(Criteria.where("userId").is(userId)).addCriteria(Criteria.where("departmentId").is(departmentId)).
			addCriteria(Criteria.where("eventType").is(eventType)).addCriteria(Criteria.where("status").is(status)).addCriteria(Criteria.where("shiftDate").is(onDate));
		query.with(Sort.by(Sort.Direction.DESC, "scheduleId"));
		return mongoTemplate.find(query, Schedules.class);
	}

	@Override
	public List<Schedules> findByUserIdAndShiftTypeIdOrDepartmentId(long userId, long departmentShiftId, long departmentId) {
		
		Query query = new Query();
		if (departmentId == 0) {
			
			query.addCriteria(Criteria.where("userId").is(userId)).addCriteria(Criteria.where("shiftTypeId").is(departmentShiftId))
					.addCriteria(Criteria.where("status").is("Active")).addCriteria(Criteria.where("shiftDate").gte(LocalDate.now()));
			query.fields().include("userId").include("shiftDate");
			return mongoTemplate.find(query, Schedules.class);
		}else {
			
			query.addCriteria(Criteria.where("userId").is(userId)).addCriteria(Criteria.where("departmentId").is(departmentId))
				.addCriteria(Criteria.where("status").is("Active")).addCriteria(Criteria.where("shiftDate").gte(LocalDate.now()));
			query.fields().include("userId").include("shiftDate").include("departmentId");
			return mongoTemplate.find(query, Schedules.class);
		}
	}

	@Override
	public List<Schedules> findScheduleUsersByUserIdAndOnDates(long userId, List<LocalDate> onDates) {
		Query query = new Query();
		Criteria dateC = Criteria.where("shiftDate").in(onDates);
		query.addCriteria(Criteria.where("userId").is(userId)).addCriteria(dateC).addCriteria(Criteria.where("status").is("Active"));
		query.fields().exclude("status").exclude("eventType").exclude("voluntaryLowCensus");
		return mongoTemplate.find(query, Schedules.class);
	}

	@Override
	public List<Schedules> findSchedulesByDepartmentIdAndDuration(long departmentId, LocalDate startDate, LocalDate endDate) {
		Query query = new Query();
        query.addCriteria(Criteria.where("departmentId").is(departmentId)).addCriteria((new Criteria().orOperator(Criteria.where("shiftDate").gte(startDate),Criteria.where("shiftDate").lte(endDate))));
        return mongoTemplate.find(query, Schedules.class);
	}
}
