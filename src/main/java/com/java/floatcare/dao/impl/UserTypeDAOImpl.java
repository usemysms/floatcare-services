package com.java.floatcare.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.java.floatcare.dao.UserTypeDAO;
import com.java.floatcare.model.UserBasicInformation;

@Repository
public class UserTypeDAOImpl implements UserTypeDAO{

	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public List<UserBasicInformation> findUsersByOrganizationIdAndUserTypeId(long organizationId, long userTypeId) {

		Query query = new Query();
		query.addCriteria(Criteria.where("organizationId").is(organizationId))
			.addCriteria(Criteria.where("userTypeId").is(userTypeId)).addCriteria(Criteria.where("status").is("Active"));
		return mongoTemplate.find(query, UserBasicInformation.class);
	}

	@Override
	public List<UserBasicInformation> findUsersByOrganizationIdAndUserTypeIdNot(long organizationId, int userTypeId) {
		Query query = new Query();
		query.addCriteria(Criteria.where("organizationId").is(organizationId))
			.addCriteria(Criteria.where("userTypeId").ne(userTypeId)).addCriteria(Criteria.where("status").is("Active"));
		return mongoTemplate.find(query, UserBasicInformation.class);
	}
}
