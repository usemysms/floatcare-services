package com.java.floatcare.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.java.floatcare.dao.CredentialSubTypesDAO;
import com.java.floatcare.model.CredentialSubTypes;

@Repository
public class CredentialSubTypesDAOImpl implements CredentialSubTypesDAO {
	
	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public List<CredentialSubTypes> getCredentialSubTypesByOrganizationId(long organizationId) {
		
		Query query = new Query();
		
		query.addCriteria(
				new Criteria().orOperator(Criteria.where("organizationId").is(organizationId),Criteria.where("organizationId").is(0))
				).addCriteria(Criteria.where("removedFromOrganizations").nin(organizationId));
	
		return  mongoTemplate.find(query, CredentialSubTypes.class);
	}

	@Override
	public List<CredentialSubTypes> getCredentialSubTypesByCredentialTypeIdAndCoreTypeIdAndOrganizationId(
			long credentialTypeId, Long coreType, Long organizationId) {
	   
		Query query = new Query();
		
		query.addCriteria(
				new Criteria().orOperator(
						Criteria.where("organizationId").is(organizationId),
						Criteria.where("organizationId").is(0)
						)
				).addCriteria(Criteria.where("removedFromOrganizations").nin(organizationId))
		         .addCriteria(Criteria.where("requiredType.staffCoreTypeId").in(coreType));
		
		if(credentialTypeId != 0) {
			query.addCriteria(Criteria.where("credentialTypeId").is(credentialTypeId));	}
	
		return mongoTemplate.find(query, CredentialSubTypes.class);
	}

	@Override
	public List<CredentialSubTypes> getCredentialSubTypesByStatus() {
		
		Query query = new Query();
		
		query.addCriteria(Criteria.where("status").is("pending"))	;
		return mongoTemplate.find(query, CredentialSubTypes.class);
	}

	@Override
	public List<CredentialSubTypes> getCredentialSubTypesByCredentialTypeIdAndOrganizationId(long credentialTypeId,
			Long organizationId) {
		
         Query query = new Query();
		
		query.addCriteria(
				new Criteria().orOperator(
						Criteria.where("organizationId").is(organizationId),
						Criteria.where("organizationId").is(0)
						)
				).addCriteria(Criteria.where("removedFromOrganizations").nin(organizationId))
			     .addCriteria(Criteria.where("credentialTypeId").is(credentialTypeId));
	
		return mongoTemplate.find(query, CredentialSubTypes.class);
	}

}
