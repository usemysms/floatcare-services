package com.java.floatcare.dao.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.java.floatcare.dao.UserOrganizationDAO;
import com.java.floatcare.model.UserOrganizations;

@Repository
public class UserOrganizationsDAOImpl implements UserOrganizationDAO {

	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Override
	public List<Long> findByOrganizationId(long organizationId, long staffUserCoreTypeId) {
		
		Query query = new Query();
		query = query.addCriteria(Criteria.where("organizationId").is(organizationId)).addCriteria(Criteria.where("staffUserCoreTypeId").is(staffUserCoreTypeId)).addCriteria(Criteria.where("status").is("Active"));
		query.fields().include("userId");
		return mongoTemplate.find(query, UserOrganizations.class).stream().map(emitter -> emitter.getUserId()).collect(Collectors.toList());
	}

	@Override
	public UserOrganizations findByUserIdAndOrganizationId(long userId, long organizationId) {
		
		Query query = new Query();
		query = query.addCriteria(Criteria.where("userId").is(userId)).addCriteria(Criteria.where("organizationId").is(organizationId)).addCriteria(Criteria.where("status").is("Active"));
		return mongoTemplate.findOne(query, UserOrganizations.class);
	}

}
