package com.java.floatcare.dao.impl;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.java.floatcare.dao.OpenShiftsDAO;
import com.java.floatcare.model.OpenShiftInvitees;
import com.java.floatcare.model.OpenShifts;
import com.java.floatcare.utils.ZonedDateTimeWriteConverter;

@Repository
public class OpenShiftsDAOImpl implements OpenShiftsDAO{

	@Autowired
	private MongoTemplate mongoTemplate;
	
	ZonedDateTimeWriteConverter zonedDateTime = new ZonedDateTimeWriteConverter(); // to fetch current date by zone

	@Override
	public List<OpenShifts> findByWorksiteIdAndFromSelectedDate(long worksiteId, LocalDate onDate) {
		Query query = new Query();
		query.addCriteria(Criteria.where("worksiteId").is(worksiteId)).addCriteria(Criteria.where("onDate").gte(onDate));
		return mongoTemplate.find(query, OpenShifts.class);
	}
	
	@Override
	public List<OpenShifts> findByWorkspaceIdAndFromSelectedDate(long workspaceId, LocalDate onDate) {
		Query query = new Query();
		query.addCriteria(Criteria.where("departmentId").is(workspaceId)).addCriteria(Criteria.where("onDate").gte(onDate));
		return mongoTemplate.find(query, OpenShifts.class);
	}

	@Override
	public List<OpenShifts> findByOpenShiftId(List<OpenShiftInvitees> inviteesList) {
		
		Query query = new Query();
		query.addCriteria(Criteria.where("openShiftId").in(inviteesList.stream().map(e->e.getOpenShiftId()).toArray())).addCriteria(Criteria.where("status").ne("Closed"));
		return mongoTemplate.find(query, OpenShifts.class);
	}

	@Override
	public OpenShifts findByUserIdAndOnDateAndDepartmentId(long userId, LocalDate shiftDate, long departmentId) {
		Query query = new Query();
		query.addCriteria(Criteria.where("invitees").in(userId)).addCriteria(Criteria.where("onDate").is(shiftDate)).addCriteria(Criteria.where("departmentId").is(departmentId));
		query.fields().include("openShiftId");
		return mongoTemplate.findOne(query, OpenShifts.class);
	}
	
	@Override
	public List<OpenShifts> findByUserIdAndOnDateAndDepartmentIds(long userId, LocalDate shiftDate, List<Long> departmentIds) {
		Query query = new Query();
		query.addCriteria(Criteria.where("invitees").in(userId)).addCriteria(Criteria.where("onDate").is(shiftDate)).addCriteria(Criteria.where("departmentId").in(departmentIds));
		return mongoTemplate.find(query, OpenShifts.class);
	}

	@Override
	public List<OpenShifts> findByUserIdAndStartDateEndDateAndDepartmentId(long userId, LocalDate localStartDate, LocalDate localEndDate, long departmentId) {
		Query query = new Query();
        query.addCriteria(Criteria.where("invitees").in(userId)).addCriteria((Criteria.where("onDate").lte(localStartDate).orOperator(Criteria.where("onDate").gte(localEndDate))));
		return mongoTemplate.find(query, OpenShifts.class);
		
	}

	@Override
	public List<LocalDate> findOpenShiftsByStatusAndDepartmentId(String status, long workspaceId) {
		
		ZonedDateTimeWriteConverter zonedDateTimeWriteConverter = new ZonedDateTimeWriteConverter();
		LocalDate date = zonedDateTimeWriteConverter.convert(ZonedDateTime.now());
		
		Query query = new Query();
		query.addCriteria(Criteria.where("status").is(status)).addCriteria(Criteria.where("departmentId").is(workspaceId)).addCriteria(Criteria.where("onDate").gte(date));
		return mongoTemplate.find(query, OpenShifts.class).stream().map(emitter -> emitter.getOnDate()).collect(Collectors.toList());
	}

	@Override
	public List<OpenShifts> findByWorkspaceIdAndStatusAndOnDate(long workspaceId, LocalDate onDate, String status) {
		
		Query query = new Query();
		query.addCriteria(Criteria.where("status").is(status)).addCriteria(Criteria.where("departmentId").is(workspaceId)).addCriteria(Criteria.where("onDate").is(onDate));
		return mongoTemplate.find(query, OpenShifts.class);//.stream().map(emitter -> emitter.getOpenShiftId()).collect(Collectors.toList());
	}

	@Override
	public List<Long> findByDepartmentIdAndOnDate(long workspaceId, LocalDate onDate) {
		Query query = new Query();
		query.addCriteria(Criteria.where("onDate").gte(onDate)).addCriteria(Criteria.where("departmentId").is(workspaceId));
		return mongoTemplate.find(query, OpenShifts.class).stream().map(emitter -> emitter.getOpenShiftId()).collect(Collectors.toList());
	}

	@Override
	public void removeOpenShiftByCurrentDateAndDepartmentId(LocalDate currentDate, long workspaceId) {
		Query query = new Query();
		query.addCriteria(Criteria.where("onDate").lte(currentDate)).addCriteria(Criteria.where("departmentId").is(workspaceId));
		mongoTemplate.findAndRemove(query, OpenShifts.class);
	}
}
