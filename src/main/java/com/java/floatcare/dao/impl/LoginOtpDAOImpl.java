package com.java.floatcare.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.java.floatcare.dao.LoginOtpDAO;
import com.java.floatcare.model.LoginOtp;

@Repository
public class LoginOtpDAOImpl implements LoginOtpDAO {

	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public List<LoginOtp> findByUserIdAndOtp(long userId, String otp) {
		Query query = new Query();
		query.addCriteria(Criteria.where("userId").is(userId)).addCriteria(Criteria.where("isExpired").is(false)).addCriteria(Criteria.where("otpString").is(otp));
		return mongoTemplate.find(query, LoginOtp.class);
	}

}
