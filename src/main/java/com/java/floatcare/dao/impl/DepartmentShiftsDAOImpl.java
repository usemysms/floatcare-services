package com.java.floatcare.dao.impl;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.LookupOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.java.floatcare.api.request.pojo.DepartmentShiftDTO;
import com.java.floatcare.dao.DepartmentShiftsDAO;
import com.java.floatcare.model.DepartmentShifts;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;

@Repository
public class DepartmentShiftsDAOImpl implements DepartmentShiftsDAO{

	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Override
	public List<DepartmentShifts> findByIdAndDepartmentId(List<Long> shiftIds, long departmentId) {
		
		Query query = new Query();
		query = query.addCriteria(Criteria.where("departmentShiftId").in(shiftIds)).addCriteria(Criteria.where("departmentId").is(departmentId));
		
		List<DepartmentShifts> listDepartmentShift = mongoTemplate.find(query, DepartmentShifts.class);
		
		if (listDepartmentShift.size() > 0)
			return listDepartmentShift;
		else
			return null;	
	}

	@Override
	public List<DepartmentShiftDTO> findSchedulesOfUserByDepartmentShiftId(long userId, List<LocalDate> dates) {

		LookupOperation lookupOperation = LookupOperation.newLookup().from("schedules").localField("_id").foreignField("shiftTypeId").as("schedules");
		Aggregation aggregation = newAggregation(lookupOperation,
				  Aggregation.unwind("schedules"),
				  Aggregation.match(Criteria.where("schedules.userId").is(userId).andOperator(Criteria.where("schedules.shiftDate").in(dates), Criteria.where("schedules.status").is("Active"))),
				  Aggregation.group("schedules._id").first("schedules.startTime").as("shiftStartTime").first("schedules.endTime").as("shiftEndTime")
				  			.first("endTime").as("endTime").first("startTime").as("startTime").first("schedules.shiftDate").as("shiftDate"));
		AggregationResults<DepartmentShiftDTO> result = mongoTemplate.aggregate(aggregation, DepartmentShifts.class, DepartmentShiftDTO.class);

		return result.getMappedResults();
	}
	
	public List<DepartmentShifts> findByDepartmentIdAndStaffCoreTypeIdsAndStatus(long departmentId, long staffCoreTypeId, String status) {
		Query query = new Query();
		query = query.addCriteria(Criteria.where("departmentId").in(departmentId)).addCriteria(Criteria.where("staffCoreTypeIds").in(staffCoreTypeId)).addCriteria(Criteria.where("status").is("Active"));
		List<DepartmentShifts> listDepartmentShift = mongoTemplate.find(query, DepartmentShifts.class);
		
		if (listDepartmentShift.size() > 0)
			return listDepartmentShift;
		else
			return null;
	}
}
