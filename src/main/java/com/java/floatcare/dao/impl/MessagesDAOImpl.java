package com.java.floatcare.dao.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.ObjectOperators;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.java.floatcare.dao.MessagesDAO;
import com.java.floatcare.model.DeletedMessages;
import com.java.floatcare.model.Messages;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;

@Repository
public class MessagesDAOImpl implements MessagesDAO {

	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public List<Messages> findLatestMessagesBySenderIdAndReceiverId(long senderId, long receiverId) {
		Query query = new Query();
		query.addCriteria(Criteria.where("senderId").is(senderId)).addCriteria(Criteria.where("receiverId").is(receiverId));
	    query.with(Sort.by(Sort.Direction.DESC,"messageId"));
	    query.limit(1);
		return mongoTemplate.find(query, Messages.class);
	}

	@Override
	public List<Messages> findReceiverIdsBySenderId(long senderId) {

		Query query = new Query();
		Criteria criteria1 = Criteria.where("senderId").is(senderId);
		Criteria criteria2 = Criteria.where("receiverId").is(senderId);

		query.addCriteria(new Criteria().orOperator(criteria1,criteria2));
		query.fields().include("senderId").include("receiverId");

		query.with(Sort.by(Sort.Direction.ASC,"messageId"));
		return mongoTemplate.find(query, Messages.class);
	}

	@Override
	public List<Messages> findAllMessagesBySenderIdAndReceiverId(long senderId, long receiverId) {
		Query query = new Query();
		query.addCriteria(Criteria.where("receiverId").in(receiverId, senderId));
		query.addCriteria(Criteria.where("senderId").in(senderId, receiverId));
		return mongoTemplate.find(query, Messages.class).stream().filter(emitter -> emitter.getSenderId() != emitter.getReceiverId()).collect(Collectors.toList());
		
	}
	
	@Override
	public List<Messages> findMessagesBetweenSenderIdAndReceiverId(long senderId, long receiverId) {
		Query query = new Query();
		query.addCriteria(Criteria.where("receiverId").is(receiverId));
		query.addCriteria(Criteria.where("senderId").is(senderId));
		return mongoTemplate.find(query, Messages.class).stream().filter(emitter -> emitter.getSenderId() != emitter.getReceiverId()).collect(Collectors.toList());
		
	}

	@Override
	public List<Messages> findMessagesByReceiverId(long receiverId) {
		
		Query query = new Query();
		query.limit(1);
		query.addCriteria(Criteria.where("receiverId").is(receiverId));
	    query.with(Sort.by(Sort.Direction.DESC,"messageId"));
		return mongoTemplate.find(query, Messages.class);
	}

	@Override
	public List<Messages> findTextMessagesOfUsers(List<Long> receiverIds, String text, List<Long> groupIds) {
		
		Query query = new Query();
		Criteria criteria1 = Criteria.where("senderId").in(receiverIds);
		Criteria criteria2 = Criteria.where("receiverId").in(receiverIds);
		Criteria criteria3 = Criteria.where("text").regex(text, "i");
		Criteria criteria4 = Criteria.where("groupId").in(groupIds);
		Criteria criteria5 = Criteria.where("sourceType").ne("generated");
		query.addCriteria(new Criteria().orOperator(criteria2, criteria1, criteria4).andOperator(criteria5)).addCriteria(criteria3);

		query.with(Sort.by(Sort.Direction.DESC,"messageId"));
		return mongoTemplate.find(query, Messages.class);
	}

	@Override
	public List<Long> findUsersDeletedMessagesIdsByUserId(long userId) {
		
		Query query = new Query();
		query.fields().include("receiverMessagesDeleteduserId");
		Criteria criteria1 = Criteria.where("userId").is(userId);
		Criteria criteria2 = Criteria.where("isReplied").is(false);
		return mongoTemplate.find(query.addCriteria(criteria1.andOperator(criteria2)), DeletedMessages.class).stream().map(DeletedMessages::getReceiverMessagesDeleteduserId).collect(Collectors.toList());
	}

	@Override
	public DeletedMessages findDeletedMessagesBySenderAndReceiverId(long senderId, long receiverId) {
		
		Query query = new Query();
		Criteria criteria1 = Criteria.where("userId").is(senderId);
		Criteria criteria2 = Criteria.where("receiverMessagesDeleteduserId").is(receiverId);
		return mongoTemplate.findOne(query.addCriteria(criteria1).addCriteria(criteria2), DeletedMessages.class);
	}

	@Override
	public DeletedMessages findByUserIdAndReceiverMessagesDeletedUserIdAndIsReplied(Long senderId, Long receiverId, boolean isReplied) {

		Query query = new Query();
		
		Criteria criteria1 = Criteria.where("userId").is(senderId);
		Criteria criteria2 = Criteria.where("userId").is(receiverId);
		Criteria criteria3 = Criteria.where("receiverMessagesDeleteduserId").is(receiverId);
		Criteria criteria4 = Criteria.where("receiverMessagesDeleteduserId").is(senderId);
		Criteria criteria5 = Criteria.where("isReplied").is(isReplied);
		
		query.addCriteria(new Criteria().orOperator(criteria1, criteria2).andOperator(new Criteria().orOperator(criteria3, criteria4),(criteria5)));
		return mongoTemplate.findOne(query, DeletedMessages.class);
	}

	@Override
	public List<Messages> findLatestMessagesOfReceiverIdToSenderId(long receiverId, long senderId) {
		
		Query query = new Query();
		query.addCriteria(Criteria.where("receiverId").is(receiverId)).addCriteria(Criteria.where("senderId").is(senderId))
		.addCriteria(Criteria.where("isSeen").is(false));

		return mongoTemplate.find(query, Messages.class);
	}

	@Override
	public Messages findLatestMessagesByGroupId(Long groupId) {

		Query query = new Query();
		query.addCriteria(Criteria.where("groupId").is(groupId));
		query.with(Sort.by(Sort.Direction.DESC, "messageId"));
		query.limit(1);
		return mongoTemplate.findOne(query, Messages.class);
	}

	@Override
	public Map<Long, Messages> findMessagesOfSenderId (Long senderId) {

		String jsonExp = "{$cond: [{$eq: [\"$senderId\","+senderId+"]},\"$receiverId\",\"$senderId\"]}}";

		Criteria criteria1 = Criteria.where("senderId").is(senderId);
		Criteria criteria2 = Criteria.where("receiverId").is(senderId);
		Aggregation agg = newAggregation(match(new Criteria().orOperator(criteria1, criteria2)),

		project().andInclude("senderId").andInclude("receiverId").andInclude("epochSeconds").andInclude("text").andInclude("messageId").andInclude("timeStamp")
					.andInclude("aesKey").andInclude("date").andInclude("initializationVector").andInclude("timeZone")
					.and(context -> context.getMappedObject(Document.parse(jsonExp))).as("interlocutor"),
		group("interlocutor").max(ROOT).as("text"),
		replaceRoot().withValueOf(ObjectOperators.valueOf("text").merge()),
		sort(Sort.by(Direction.DESC, "messageId")));

		AggregationResults<Messages> result = mongoTemplate.aggregate(agg, Messages.class, Messages.class);

		return result.getMappedResults().stream().collect(Collectors.toMap(Messages::getMessageId, e->e));
	}

	@Override
	public List<Messages> findAllByGroupIdAndSenderIdAndMessageIds(long groupId, long staffId, List<Long> messageIds) {

		Query query = new Query();
		
		if (messageIds != null) {
			
			query.addCriteria(Criteria.where("groupId").is(groupId))
			.addCriteria(Criteria.where("senderId").ne(staffId))
			.addCriteria(Criteria.where("isSeenByGroupMembers.userId").nin(Arrays.asList(staffId)))
			.addCriteria(Criteria.where("messageId").nin(messageIds));
			
			return mongoTemplate.find(query, Messages.class);
		}else {
			
			query.addCriteria(Criteria.where("groupId").is(groupId))
			.addCriteria(Criteria.where("senderId").ne(staffId))
			.addCriteria(Criteria.where("isSeenByGroupMembers.userId").nin(Arrays.asList(staffId)));
			
			return mongoTemplate.find(query, Messages.class);
		}
	}

	@Override
	public void updateIsSeenMessages(long senderId, long receiverId) {
		
		Query query = new Query();

		query.addCriteria(Criteria.where("senderId").is(senderId)).addCriteria(Criteria.where("receiverId").is(receiverId))
			.addCriteria(Criteria.where("isSeen").is(false));

		mongoTemplate.updateMulti(query, new Update().set("isSeen", true), Messages.class).getMatchedCount();
	}

	@Override
	public List<DeletedMessages> findDeletedMessagesBySenderIdAndGroupId(long senderId, List<Long> groupIds) {
		
		Query query = new Query();
		query.addCriteria(Criteria.where("groupId").in(groupIds))
		.addCriteria(Criteria.where("userId").is(senderId)).addCriteria(Criteria.where("isReplied").is(false));
		query.fields().include("groupId");
		return mongoTemplate.find(query, DeletedMessages.class);
	}

	@Override
	public void findAndUpdateIsRepliedByUserIdAndGroupId(TreeSet<Long> groupMembers, Long groupId) {
		
		Criteria criteria1 = Criteria.where("groupId").is(groupId);
		Criteria criteria2 = Criteria.where("userId").in(groupMembers);
		
		mongoTemplate.updateMulti(Query.query(criteria1).addCriteria(criteria2), new Update().set("isReplied", true), DeletedMessages.class);
	}

	@Override
	public boolean removeDeletedMessagesEntryByGroupId(long chatGroupId) {

		Query query = new Query();
		
		Criteria criteria1 = Criteria.where("groupId").is(chatGroupId);
		query.addCriteria(criteria1);

		return  mongoTemplate.findAndRemove(query, DeletedMessages.class) != null ? true: false ;

	}
	public long getUnreadMessagesCount(long userId) {
		Query query = new Query();
		query.addCriteria(Criteria.where("isSeen").is(false))
				.addCriteria(Criteria.where("receiverId").is(userId));
		query.fields().include("_id");
		//System.out.println("message: "+mongoTemplate.find(query, Messages.class));
		//System.out.println("message count: "+mongoTemplate.find(query, Messages.class).stream().count());
		return mongoTemplate.find(query, Messages.class).stream().count();
	}
}
