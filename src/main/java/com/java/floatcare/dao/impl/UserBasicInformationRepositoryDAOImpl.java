package com.java.floatcare.dao.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.java.floatcare.api.request.UserRequest;
import com.java.floatcare.api.response.pojo.BusinessesResponse;
import com.java.floatcare.dao.UserBasicInformationRepositoryDAO;
import com.java.floatcare.dao.UserWorkspacesDAO;
import com.java.floatcare.model.BlockUserWorkspace;
import com.java.floatcare.model.Businesses;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.model.UserWorkspaces;
import com.java.floatcare.repository.UserBasicInformationRepository;

@Repository
public class UserBasicInformationRepositoryDAOImpl implements UserBasicInformationRepositoryDAO {

	@Autowired
	private MongoTemplate mongoTemplate;
	@Autowired
	private UserBasicInformationRepository userBasicInformationRepository;
	@Autowired
	private UserWorkspacesDAO userWorkspacesDAO;

	public UserBasicInformation findUserByEmail(String email) {

		Query query = new Query();
		query.addCriteria(Criteria.where("email").regex(email, "i"));
		return mongoTemplate.findOne(query, UserBasicInformation.class);
	}

	public List<UserBasicInformation> findUsersByEmail(String email) {

		Query query = new Query();
		query.addCriteria(Criteria.where("email").regex(email, "i")).addCriteria(Criteria.where("userTypeId").ne(1))
				.addCriteria(Criteria.where("status").is("Active"));
		return mongoTemplate.find(query, UserBasicInformation.class);
	}

	public UserBasicInformation findAllUsersByPhone(String phone) {

		Query query = new Query();
		query.addCriteria(Criteria.where("phone").is(phone));
		return mongoTemplate.findOne(query, UserBasicInformation.class);
	}

	public UserBasicInformation findUserByPhone(String phone) {

		// if (phone.contains("+")) //neglect the special character
		// phone = phone.replace("+", "");

		Query query = new Query();
		query.addCriteria(Criteria.where("phone").is(phone)).addCriteria(Criteria.where("userTypeId").is(3))
				.addCriteria(Criteria.where("status").is("Active"));
		return mongoTemplate.findOne(query, UserBasicInformation.class);
	}

	@Override
	public List<UserBasicInformation> findUserById(long userId) {
		Query query = new Query();
		query.addCriteria(Criteria.where("_id").is(userId)).addCriteria(Criteria.where("status").is("Active"));
		return mongoTemplate.find(query, UserBasicInformation.class);
	}

	@Override
	public List<UserBasicInformation> findUserByorganizationIdAndStaffUserCoreTypeId(long staffUserCoreTypeId,
			long organizationId) {
		Query query = new Query();
		query.addCriteria(Criteria.where("staffUserCoreTypeId").is(staffUserCoreTypeId))
				.addCriteria(Criteria.where("organizationId").is(organizationId))
				.addCriteria(Criteria.where("status").is("Active"));
		return mongoTemplate.find(query, UserBasicInformation.class);
	}

	@Override
	public List<UserBasicInformation> findColleguesByWorksiteId(long userId, long businessId) {

		List<UserBasicInformation> users = new ArrayList<UserBasicInformation>();

		Query query = new Query();
		query.addCriteria(Criteria.where("businessId").is(businessId))
				.addCriteria(Criteria.where("status").is("Active"));
		query.fields().include("userId");
		List<Long> currentId = mongoTemplate.find(query, UserWorkspaces.class).stream()
				.filter(emitter -> emitter.getUserId() != userId).map(UserWorkspaces::getUserId)
				.collect(Collectors.toList());

		Query queryBlockUser = new Query();
		queryBlockUser.addCriteria(Criteria.where("blockedUserId").is(userId));
		queryBlockUser.fields().include("userId");
		List<Long> blockedUsers = mongoTemplate.find(queryBlockUser, BlockUserWorkspace.class).stream()
				.map(BlockUserWorkspace::getUserId).collect(Collectors.toList());

		currentId.removeAll(blockedUsers);

		for (Long each : currentId) {
			UserBasicInformation user = userBasicInformationRepository.findUsersByUserId(each);
			if (user != null)
				users.add(user);
		}
		return users;
	}

	@Override
	public List<UserBasicInformation> findByUserId(List<Long> userId) {

		Query query = new Query();
		query.addCriteria(Criteria.where("userId").in(userId)).addCriteria(Criteria.where("userTypeId").ne(1));
		return mongoTemplate.find(query, UserBasicInformation.class);
	}

	@Override
	public List<UserBasicInformation> findByListOfUserIds(List<Long> userId) {

		Query query = new Query();
		query.addCriteria(Criteria.where("userId").in(userId));
		return mongoTemplate.find(query, UserBasicInformation.class);
	}

	@Override
	public List<UserBasicInformation> findByUserIdAndStaffUserCoreTypeId(List<Long> users, long staffUserCoreTypeId) {
		Query query = new Query();
		query.addCriteria(Criteria.where("userId").in(users)).addCriteria(Criteria.where("status").is("Active"))
				.addCriteria(Criteria.where("staffUserCoreTypeId").is(staffUserCoreTypeId));
		query.fields().exclude("password").exclude("address1").exclude("address2");
		return mongoTemplate.find(query, UserBasicInformation.class);
	}

	@Override
	public long findUserByIdAndUserTypeId(long userId, int userTypeId) {

		Query query = new Query();
		query.addCriteria(Criteria.where("userId").in(userId)).addCriteria(Criteria.where("status").is("Active"))
				.addCriteria(Criteria.where("userTypeId").is(userTypeId));

		UserBasicInformation userFound = mongoTemplate.findOne(query, UserBasicInformation.class);
		if (userFound != null)
			return userFound.getUserId();
		else
			return 0;
	}

	@Override
	public List<UserBasicInformation> findUsersByOrganizationIdAndUserTypeId(long organizationId, int userTypeId) {

		Query query = new Query();
		query.addCriteria(Criteria.where("organizationId").in(organizationId))
				.addCriteria(Criteria.where("status").is("Active"))
				.addCriteria(Criteria.where("userTypeId").is(userTypeId));

		return mongoTemplate.find(query, UserBasicInformation.class);

	}

	@Override
	public List<UserBasicInformation> findByFirstName(String name) {

		Criteria regexCriteria1 = Criteria.where("firstName").regex(name, "i"); // search user with first and last name
		Criteria regexCriteria2 = Criteria.where("lastName").regex(name, "i");
		Criteria userType = Criteria.where("userTypeId").ne(1);
		Query query = new Query();
		query.addCriteria(userType.orOperator(regexCriteria1, regexCriteria2));

		return mongoTemplate.find(query, UserBasicInformation.class);
	}

	@Override
	public List<UserBasicInformation> findByPhone(String phone) {
		Criteria regex = Criteria.where("phone").is(phone);
		Criteria userType = Criteria.where("userTypeId").is(3);
		return mongoTemplate.find(new Query().addCriteria(regex).addCriteria(userType), UserBasicInformation.class);
	}

	@Override
	public List<UserBasicInformation> findByFirstNameAndEmailAndPhone(String name, String email, String phone) {

		String[] splitted = name.split("[\\W]");
		if (splitted.length == 1) {

			Criteria regexCriteria1 = Criteria.where("firstName").regex(name, "i"); // search user with first and last
																					// name
			Criteria regexCriteria2 = Criteria.where("lastName").regex(name, "i");
			Criteria emailCriteria = Criteria.where("email").regex(email, "i");
			Criteria userTypeCriteria = Criteria.where("userTypeId").ne(1);
			Query query = new Query();
			query.addCriteria(userTypeCriteria.orOperator(regexCriteria1, regexCriteria2).andOperator(emailCriteria));

			return mongoTemplate.find(query, UserBasicInformation.class);
		} else {
			Criteria regex = Criteria.where("firstName").regex(splitted[0], "i");
			Criteria userType = Criteria.where("userTypeId").ne(1);
			Criteria lastNameRegex = Criteria.where("lastName").regex(splitted[1], "i");
			Query query = new Query();
			query.addCriteria(regex).addCriteria(lastNameRegex).addCriteria(Criteria.where("email").regex(email, "i"))
					.addCriteria(Criteria.where("phone").regex(phone, "i")).addCriteria(userType);
			return mongoTemplate.find(query, UserBasicInformation.class);
		}
	}

	@Override
	public List<UserBasicInformation> findByFirstNameAndLastName(String firstName, String lastName) {
		Criteria firstNameRegEx = Criteria.where("firstName").regex(firstName, "i");
		Criteria lastNameRegex = Criteria.where("lastName").regex(lastName, "i");
		Criteria userType = Criteria.where("userTypeId").ne(1);
		return mongoTemplate.find(
				new Query().addCriteria(firstNameRegEx).addCriteria(lastNameRegex).addCriteria(userType),
				UserBasicInformation.class);
	}

	@Override
	public List<UserBasicInformation> findByEmailAndPhone(String email, String phone) {
		Query query = new Query();
		query.addCriteria(Criteria.where("email").regex(email, "i")).addCriteria(Criteria.where("phone").is(phone))
				.addCriteria(Criteria.where("userTypeId").ne(1));
		return mongoTemplate.find(query, UserBasicInformation.class);

	}

	@Override
	public List<UserBasicInformation> findByNameAndEmail(String firstName, String email) {

		Criteria regexCriteria1 = Criteria.where("firstName").regex(firstName, "i"); // search user with first and last
																						// name
		Criteria regexCriteria2 = Criteria.where("lastName").regex(firstName, "i");
		Criteria emailCriteria = Criteria.where("email").regex(email, "i");
		Criteria userTypeCriteria = Criteria.where("userTypeId").ne(1);
		Query query = new Query();
		query.addCriteria(userTypeCriteria.orOperator(regexCriteria1, regexCriteria2).andOperator(emailCriteria));
		return mongoTemplate.find(query, UserBasicInformation.class);
	}

	@Override
	public List<UserBasicInformation> findByFirstNameAndLastNameAndEmail(String firstName, String lastName,
			String email) {
		Criteria firstNameRegEx = Criteria.where("firstName").regex(firstName, "i");
		Criteria lastNameRegex = Criteria.where("lastName").regex(lastName, "i");
		Criteria userType = Criteria.where("userTypeId").ne(1);
		Criteria emailRegex = Criteria.where("email").regex(email, "i");
		return mongoTemplate.find(new Query().addCriteria(firstNameRegEx).addCriteria(lastNameRegex)
				.addCriteria(userType).addCriteria(emailRegex), UserBasicInformation.class);
	}

	@Override
	public List<UserBasicInformation> findByNameAndPhone(String firstName, String phone) {

		Criteria criteria1 = Criteria.where("phone").is(phone);
		Criteria criteria2 = Criteria.where("firstName").regex(firstName, "i");
		Criteria criteria3 = Criteria.where("userTypeId").ne(1);
		Query query = new Query();
		query.addCriteria(criteria3.andOperator(criteria1, criteria2));
		return mongoTemplate.find(query, UserBasicInformation.class);
	}

	@Override
	public List<UserBasicInformation> findByFirstNameAndLastNameAndPhone(String firstName, String lastName,
			String phone) {
		Criteria firstNameRegEx = Criteria.where("firstName").regex(firstName, "i");
		Criteria lastNameRegex = Criteria.where("lastName").regex(lastName, "i");
		Criteria userType = Criteria.where("userTypeId").ne(1);
		Criteria emailRegex = Criteria.where("phone").is(phone);
		return mongoTemplate.find(new Query().addCriteria(firstNameRegEx).addCriteria(lastNameRegex)
				.addCriteria(userType).addCriteria(emailRegex), UserBasicInformation.class);
	}

	@Override
	public UserBasicInformation findByEmailOrPhone(String email) {
		Criteria criteria1 = Criteria.where("email").is(email);
		Criteria criteria2 = Criteria.where("phone").is(email);

		Query query = new Query();
		query.addCriteria(new Criteria().orOperator(criteria1, criteria2));
		query.fields().include("userId");

		return mongoTemplate.findOne(query, UserBasicInformation.class);
	}

	@Override
	public List<UserBasicInformation> findByAddress1AndGenderAndStaffUserCoreTypeIdAndSubRoleTypeId(String location,
			String gender, long staffUserCoreTypeId, long subRoleTypeId) {

		Criteria criteria1 = Criteria.where("address1").regex(location, "i");
		Criteria criteria2 = Criteria.where("city").regex(location, "i");
		Criteria criteria3 = Criteria.where("state").regex(location, "i");
		Criteria criteria4 = Criteria.where("gender").regex(gender, "i");
		Criteria criteria5 = Criteria.where("staffUserCoreTypeId").is(staffUserCoreTypeId);
		Criteria criteria6 = Criteria.where("staffUserSubCoreTypeId").is(subRoleTypeId);
		Criteria criteria7 = Criteria.where("country").regex(location, "i");

		try {
			return mongoTemplate.find(Query.query(new Criteria().orOperator(criteria1, criteria2, criteria3, criteria7)
					.andOperator(criteria4, criteria5, criteria6)), UserBasicInformation.class);
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<UserBasicInformation> findByLanguageAndGenderAndStaffUserCoreTypeIdAndSubRoleTypeId(String language,
			String gender, long staffUserCoreTypeId, long subRoleTypeId, Set<Long> userIds, String ethnicType) {

		Criteria criteria1 = Criteria.where("userLanguages.name").regex(language, "i");
		Criteria criteria2 = Criteria.where("gender").regex(gender, "i");
		Criteria criteria3 = Criteria.where("staffUserCoreTypeId").is(staffUserCoreTypeId);
		Criteria criteria4 = Criteria.where("staffUserSubCoreTypeId").is(subRoleTypeId);
		Criteria criteria5 = Criteria.where("userId").in(userIds);
		Criteria criteria6 = Criteria.where("ethnicType").regex(ethnicType, "i");

		try {
			return mongoTemplate.find(Query.query(
					new Criteria().andOperator(criteria1, criteria2, criteria3, criteria4, criteria5, criteria6)),
					UserBasicInformation.class);
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<UserBasicInformation> findByLanguagesAndGenderAndStaffUserCoreTypeIdAndSubRoleTypeIdAndEthnicType(
			String language, String gender, long staffUserCoreTypeId, long subRoleTypeId, String ethnicType) {

		Criteria criteria1 = Criteria.where("userLanguages.name").regex(language, "i");
		Criteria criteria2 = Criteria.where("gender").regex(gender, "i");
		Criteria criteria3 = Criteria.where("staffUserCoreTypeId").is(staffUserCoreTypeId);
		Criteria criteria4 = Criteria.where("staffUserSubCoreTypeId").is(subRoleTypeId);
		Criteria criteria5 = Criteria.where("ethnicType").regex(ethnicType, "i");

		try {
			return mongoTemplate.find(
					Query.query(new Criteria().andOperator(criteria1, criteria2, criteria3, criteria4, criteria5)),
					UserBasicInformation.class);
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public UserBasicInformation findPrimaryCarePhysicanByUserIdAndStaffUserCoreTypeId(List<Long> userIds,
			int staffUserSubcoreTypeId) {
		Query query = new Query();
		query.addCriteria(new Criteria().andOperator(Criteria.where("userId").in(userIds),
				Criteria.where("staffUserSubCoreTypeId").is(staffUserSubcoreTypeId)));
		return mongoTemplate.findOne(query, UserBasicInformation.class);
	}

	@Override
	public List<UserBasicInformation> findByLanguageAndGenderAndSubRoleTypeId(String language, String gender,
			long subRoleTypeId, Set<Long> userIds, String ethnicType) {

		Criteria criteria1 = Criteria.where("userLanguages.name").regex(language, "i");
		Criteria criteria2 = Criteria.where("gender").regex(gender, "i");
		Criteria criteria3 = Criteria.where("staffUserSubCoreTypeId").is(subRoleTypeId);
		Criteria criteria4 = Criteria.where("ethnicType").regex(ethnicType, "i");

		try {
			return mongoTemplate.find(
					Query.query(new Criteria().andOperator(criteria1, criteria2, criteria3, criteria4)),
					UserBasicInformation.class);
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<UserBasicInformation> findByStaffUserCoreTypeIdAndSubRoleTypeIdAndSpecialityTags(String firstName,
			String lastName, long staffUserCoreTypeId, long subRoleTypeId, List<String> specialityTags, String postalCode) {

		Query query = new Query();
		query.fields().include("userId");
		query.fields().include("latitude");
		query.fields().include("longitude");

		try {
			List<Criteria> criteria = new ArrayList<Criteria>();
			criteria.add(Criteria.where("staffUserCoreTypeId").is(staffUserCoreTypeId));
			if (firstName != null && !firstName.isEmpty())
				criteria.add(Criteria.where("firstName").regex(firstName, "i"));
			if (lastName != null && !lastName.isEmpty())
				criteria.add(Criteria.where("lastName").regex(lastName, "i"));
			if (specialityTags != null && specialityTags.size() > 0)
				criteria.add(Criteria.where("specialities").in(specialityTags));
			if (subRoleTypeId > 0)
				criteria.add(Criteria.where("staffUserSubCoreTypeId").is(subRoleTypeId));
			if (postalCode!=null && !postalCode.isEmpty())
				criteria.add(Criteria.where("postalCode").is(postalCode));
			if (criteria.size() > 0)
				query.addCriteria(new Criteria().andOperator(criteria.toArray(new Criteria[criteria.size()])));
			return mongoTemplate.find(query, UserBasicInformation.class);
		} catch (Exception e) {

			return null;
		}
	}

	@Override
	public UserRequest findByStaffUserCoreTypeIdAndSubRoleTypeIdAndSpecialityTagsAndIsPreferredAndLanguagesAndGenderAndEthnicGroupsAndAvailability(
			String firstName, String lastName, long staffUserCoreTypeId, long subRoleTypeId,
			List<String> specialityTags, boolean isPreferred, String language, String gender, String ethnicGroup,
			String startTime, String endTime, List<String> weekDays, String postalCode) {
		List<UserBasicInformation> users = null;
		try {
			Query query = new Query();

			List<Criteria> criteria = new ArrayList<Criteria>();

			if (staffUserCoreTypeId > 0)
				criteria.add(Criteria.where("staffUserCoreTypeId").is(staffUserCoreTypeId));

			if (subRoleTypeId > 0)
				criteria.add(Criteria.where("staffUserSubCoreTypeId").is(subRoleTypeId));
			
			if (postalCode !=null && !postalCode.isEmpty())
				criteria.add(Criteria.where("postalCode").is(postalCode));
			
			if (gender != null && !gender.isEmpty())
				criteria.add(Criteria.where("gender").is(gender));

			if (language != null && !language.isEmpty())
				criteria.add(Criteria.where("userLanguages.name").regex(language, "i"));

			if (ethnicGroup != null && !ethnicGroup.isEmpty())
				criteria.add(Criteria.where("ethnicType").is(ethnicGroup));

			if (specialityTags != null && specialityTags.size() > 0)
				criteria.add(Criteria.where("specialities").in(specialityTags));

			if (firstName != null && !firstName.isEmpty())
				criteria.add(Criteria.where("firstName").regex(firstName, "i"));

			if (lastName != null && !lastName.isEmpty())
				criteria.add(Criteria.where("lastName").regex(lastName, "i"));

			if (specialityTags != null && specialityTags.size() > 0)
				criteria.add(Criteria.where("specialities").in(specialityTags));

			if (subRoleTypeId > 0)
				criteria.add(Criteria.where("staffUserSubCoreTypeId").is(subRoleTypeId));
			if (criteria.size() > 0)
				query.addCriteria(new Criteria().andOperator(criteria.toArray(new Criteria[criteria.size()])));
			users = mongoTemplate.find(query, UserBasicInformation.class);
		} catch (Exception e) {

			return null;
		}
		/*
		 * Criteria criteria1 =
		 * Criteria.where("staffUserCoreTypeId").is(staffUserCoreTypeId); Criteria
		 * criteria2 = subRoleTypeId > 0 ?
		 * Criteria.where("staffUserSubCoreTypeId").is(subRoleTypeId) : null; Criteria
		 * criteria3 = gender != null ? Criteria.where("gender").is(gender) : null;
		 * Criteria criteria4 = language != null ?
		 * Criteria.where("userLanguages.name").regex(language,"i") : null; Criteria
		 * criteria5 = ethnicGroup != null ?
		 * Criteria.where("ethnicType").is(ethnicGroup) : null; Criteria criteria6 =
		 * specialityTags != null ? Criteria.where("specialities").in(specialityTags) :
		 * null; Criteria criteria7_1 = firstName != null ?
		 * Criteria.where("firstName").in(firstName, "i") : null; Criteria criteria7_2 =
		 * lastName != null ? Criteria.where("lastName").regex(lastName, "i") : null;
		 * 
		 * List<UserBasicInformation> users = this.getCriteriaResult(criteria1,
		 * criteria2, criteria3, criteria4, criteria5, criteria6, criteria7_1,
		 * criteria7_2);
		 */
		System.out.println("total records: " + users.size());
		if (users != null && users.size() > 0) {
			Map<Long, UserBasicInformation> usersMap = users.stream()
					.collect(Collectors.toMap(UserBasicInformation::getUserId, e -> e));
			List<Long> userIds = new ArrayList<>(usersMap.keySet());
			List<UserWorkspaces> userWorkspaces = this.findUserWorkspacesByCriteriaAndUserIds(isPreferred, startTime,
					endTime, weekDays, staffUserCoreTypeId, userIds);
			if (userWorkspaces != null && userWorkspaces.size() > 0) {

				List<Long> removeUserIds = userWorkspaces.stream().filter(e -> !userIds.contains(e.getUserId()))
						.map(e -> e.getUserId()).distinct().collect(Collectors.toList());

				usersMap.keySet().removeAll(removeUserIds);

				UserRequest request = new UserRequest(usersMap, userWorkspaces);

				return request;
			} else
				return null;
		} else
			return null;
	}

	/*
	 * public List<UserBasicInformation> getCriteriaResult(Criteria criteria1,
	 * Criteria criteria2, Criteria criteria3, Criteria criteria4, Criteria
	 * criteria5, Criteria criteria6, Criteria criteria7_1, Criteria criteria7_2) {
	 * 
	 * if (criteria2 != null && criteria3 != null && criteria4 != null && criteria5
	 * != null && criteria6 != null && criteria7_1 != null && criteria7_2 != null)
	 * return mongoTemplate.find(Query.query(new Criteria().andOperator(criteria1,
	 * criteria2, criteria3, criteria4, criteria5, criteria6, criteria7_1,
	 * criteria7_2)), UserBasicInformation.class); else if (criteria2 == null &&
	 * criteria3 == null && criteria4 == null && criteria5 == null && criteria6 ==
	 * null && criteria7_1 != null && criteria7_2 != null) return
	 * mongoTemplate.find(Query.query(new Criteria().andOperator(criteria1,
	 * criteria7_1, criteria7_2)), UserBasicInformation.class); else if (criteria2
	 * != null && criteria3 == null && criteria4 == null && criteria5 == null &&
	 * criteria6 == null && criteria7_1 != null && criteria7_2 != null) return
	 * mongoTemplate.find(Query.query(new Criteria().andOperator(criteria1,
	 * criteria2, criteria7_1, criteria7_2)), UserBasicInformation.class); else if
	 * (criteria2 != null && criteria3 != null && criteria4 == null && criteria5 ==
	 * null && criteria6 == null && criteria7_1 != null && criteria7_2 != null)
	 * return mongoTemplate.find(Query.query(new Criteria().andOperator(criteria1,
	 * criteria2, criteria3, criteria7_1, criteria7_2)),
	 * UserBasicInformation.class); else if (criteria2 != null && criteria3 != null
	 * && criteria4 != null && criteria5 == null && criteria6 == null && criteria7_1
	 * != null && criteria7_2 != null) return mongoTemplate.find(Query.query(new
	 * Criteria().andOperator(criteria1, criteria2, criteria3, criteria4,
	 * criteria7_1, criteria7_2)), UserBasicInformation.class); else if (criteria2
	 * != null && criteria3 != null && criteria4 != null && criteria5 != null &&
	 * criteria6 == null && criteria7_1 != null && criteria7_2 != null) return
	 * mongoTemplate.find(Query.query(new Criteria().andOperator(criteria1,
	 * criteria2, criteria3, criteria4, criteria5, criteria7_1, criteria7_2)),
	 * UserBasicInformation.class); else if (criteria2 != null && criteria3 != null
	 * && criteria4 != null && criteria5 != null && criteria6 != null && criteria7_1
	 * != null && criteria7_2 == null) return mongoTemplate.find(Query.query(new
	 * Criteria().andOperator(criteria1, criteria2, criteria3, criteria4, criteria5,
	 * criteria6, criteria7_1)), UserBasicInformation.class); else if (criteria2 ==
	 * null && criteria3 == null && criteria4 == null && criteria5 == null &&
	 * criteria6 == null && criteria7_1 != null && criteria7_2 == null) return
	 * mongoTemplate.find(Query.query(new Criteria().andOperator(criteria1,
	 * criteria7_1, criteria7_2)), UserBasicInformation.class); else if (criteria2
	 * != null && criteria3 == null && criteria4 == null && criteria5 == null &&
	 * criteria6 == null && criteria7_1 != null && criteria7_2 == null) return
	 * mongoTemplate.find(Query.query(new Criteria().andOperator(criteria1,
	 * criteria2, criteria7_1, criteria7_2)), UserBasicInformation.class); else if
	 * (criteria2 != null && criteria3 != null && criteria4 == null && criteria5 ==
	 * null && criteria6 == null && criteria7_1 != null && criteria7_2 == null)
	 * return mongoTemplate.find(Query.query(new Criteria().andOperator(criteria1,
	 * criteria2, criteria3, criteria7_1)), UserBasicInformation.class); else if
	 * (criteria2 != null && criteria3 != null && criteria4 != null && criteria5 ==
	 * null && criteria6 == null && criteria7_1 != null && criteria7_2 == null)
	 * return mongoTemplate.find(Query.query(new Criteria().andOperator(criteria1,
	 * criteria2, criteria3, criteria4, criteria7_1)), UserBasicInformation.class);
	 * else if (criteria2 != null && criteria3 != null && criteria4 != null &&
	 * criteria5 != null && criteria6 == null && criteria7_1 != null && criteria7_2
	 * == null) return mongoTemplate.find(Query.query(new
	 * Criteria().andOperator(criteria1, criteria2, criteria3, criteria4, criteria5,
	 * criteria7_1)), UserBasicInformation.class); else if (criteria2 != null &&
	 * criteria3 != null && criteria4 == null && criteria5 == null && criteria6 ==
	 * null && criteria7_1 != null && criteria7_2 == null) return
	 * mongoTemplate.find(Query.query(new Criteria().andOperator(criteria1,
	 * criteria2, criteria3, criteria7_1)), UserBasicInformation.class); else if
	 * (criteria2 != null && criteria3 != null && criteria4 == null && criteria5 ==
	 * null && criteria6 != null) return mongoTemplate.find(Query.query(new
	 * Criteria().andOperator(criteria1, criteria2, criteria3, criteria6)),
	 * UserBasicInformation.class); else if (criteria2 != null && criteria3 != null
	 * && criteria4 != null && criteria5 == null && criteria6 == null) return
	 * mongoTemplate.find(Query.query(new Criteria().andOperator(criteria1,
	 * criteria2, criteria3, criteria4)), UserBasicInformation.class); else if
	 * (criteria2 != null && criteria3 != null && criteria4 == null && criteria5 ==
	 * null && criteria6 != null) return mongoTemplate.find(Query.query(new
	 * Criteria().andOperator(criteria1, criteria2, criteria3, criteria6)),
	 * UserBasicInformation.class); else if (criteria2 == null && criteria3 != null
	 * && criteria4 != null && criteria5 != null && criteria6 != null) return
	 * mongoTemplate.find(Query.query(new Criteria().andOperator(criteria1,
	 * criteria4, criteria3, criteria6, criteria5, criteria6)),
	 * UserBasicInformation.class); else if (criteria2 != null && criteria3 == null
	 * && criteria4 != null && criteria5 != null && criteria6 != null) return
	 * mongoTemplate.find(Query.query(new Criteria().andOperator(criteria1,
	 * criteria3, criteria4, criteria5, criteria6)), UserBasicInformation.class);
	 * else if (criteria2 == null && criteria3 != null && criteria4 == null &&
	 * criteria5 == null && criteria6 == null) return
	 * mongoTemplate.find(Query.query(new Criteria().andOperator(criteria1,
	 * criteria3)), UserBasicInformation.class); else if (criteria2 != null &&
	 * criteria3 == null && criteria4 == null && criteria5 == null && criteria6 ==
	 * null) return mongoTemplate.find(Query.query(new
	 * Criteria().andOperator(criteria1, criteria2)), UserBasicInformation.class);
	 * else if (criteria2 != null && criteria3 != null && criteria4 != null &&
	 * criteria5 != null && criteria6 != null) return
	 * mongoTemplate.find(Query.query(new Criteria().andOperator(criteria1,
	 * criteria2, criteria3, criteria4, criteria5, criteria6)),
	 * UserBasicInformation.class); else if (criteria2 == null && criteria3 != null
	 * && criteria4 != null && criteria5 != null && criteria6 != null) return
	 * mongoTemplate.find(Query.query(new Criteria().andOperator(criteria1,
	 * criteria3, criteria4, criteria5, criteria6)), UserBasicInformation.class);
	 * else if (criteria2 == null && criteria3 == null && criteria4 != null &&
	 * criteria5 != null && criteria6 != null) return
	 * mongoTemplate.find(Query.query(new Criteria().andOperator(criteria1,
	 * criteria4, criteria5, criteria6)), UserBasicInformation.class); else if
	 * (criteria2 == null && criteria3 == null && criteria4 == null && criteria5 !=
	 * null && criteria6 != null) return mongoTemplate.find(Query.query(new
	 * Criteria().andOperator(criteria1, criteria5, criteria6)),
	 * UserBasicInformation.class); else if (criteria2 == null && criteria3 == null
	 * && criteria4 == null && criteria5 == null && criteria6 != null) return
	 * mongoTemplate.find(Query.query(new Criteria().andOperator(criteria1,
	 * criteria6)), UserBasicInformation.class); else if (criteria2 == null &&
	 * criteria3 == null && criteria4 == null && criteria5 == null && criteria6 ==
	 * null) return mongoTemplate.find(Query.query(new
	 * Criteria().andOperator(criteria1)), UserBasicInformation.class); else if
	 * (criteria2 != null && criteria3 == null && criteria4 == null && criteria5 ==
	 * null && criteria6 == null) return mongoTemplate.find(Query.query(new
	 * Criteria().andOperator(criteria1, criteria2)), UserBasicInformation.class);
	 * else if (criteria2 == null && criteria3 != null && criteria4 == null &&
	 * criteria5 == null && criteria6 == null) return
	 * mongoTemplate.find(Query.query(new Criteria().andOperator(criteria1,
	 * criteria3)), UserBasicInformation.class); else if (criteria2 == null &&
	 * criteria3 == null && criteria4 != null && criteria5 == null && criteria6 ==
	 * null) return mongoTemplate.find(Query.query(new
	 * Criteria().andOperator(criteria1, criteria4)), UserBasicInformation.class);
	 * else if (criteria2 == null && criteria3 == null && criteria4 == null &&
	 * criteria5 != null && criteria6 == null) return
	 * mongoTemplate.find(Query.query(new Criteria().andOperator(criteria1,
	 * criteria5)), UserBasicInformation.class); else if (criteria2 == null &&
	 * criteria3 == null && criteria4 == null && criteria5 == null && criteria6 !=
	 * null) return mongoTemplate.find(Query.query(new
	 * Criteria().andOperator(criteria1, criteria6)), UserBasicInformation.class);
	 * else if (criteria2 != null && criteria3 != null && criteria4 == null &&
	 * criteria5 == null && criteria6 == null) return
	 * mongoTemplate.find(Query.query(new Criteria().andOperator(criteria1,
	 * criteria2, criteria3)), UserBasicInformation.class); else if (criteria2 ==
	 * null && criteria3 != null && criteria4 != null && criteria5 == null &&
	 * criteria6 == null) return mongoTemplate.find(Query.query(new
	 * Criteria().andOperator(criteria1, criteria4, criteria3)),
	 * UserBasicInformation.class); else if (criteria2 != null && criteria3 == null
	 * && criteria4 != null && criteria5 != null && criteria6 == null) return
	 * mongoTemplate.find(Query.query(new Criteria().andOperator(criteria1,
	 * criteria4, criteria5)), UserBasicInformation.class); else if (criteria2 ==
	 * null && criteria3 == null && criteria4 == null && criteria5 != null &&
	 * criteria6 != null) return mongoTemplate.find(Query.query(new
	 * Criteria().andOperator(criteria1, criteria5, criteria6)),
	 * UserBasicInformation.class); else if (criteria2 != null && criteria3 != null
	 * && criteria4 == null && criteria5 == null && criteria6 != null) return
	 * mongoTemplate.find(Query.query(new Criteria().andOperator(criteria1,
	 * criteria2, criteria3, criteria6)), UserBasicInformation.class); else if
	 * (criteria2 != null && criteria3 != null && criteria4 != null && criteria5 ==
	 * null && criteria6 == null) return mongoTemplate.find(Query.query(new
	 * Criteria().andOperator(criteria1, criteria2, criteria3, criteria4)),
	 * UserBasicInformation.class); else if (criteria2 != null && criteria3 != null
	 * && criteria4 == null && criteria5 != null && criteria6 == null) return
	 * mongoTemplate.find(Query.query(new Criteria().andOperator(criteria1,
	 * criteria2, criteria3, criteria5)), UserBasicInformation.class); else if
	 * (criteria2 == null && criteria3 != null && criteria4 != null && criteria5 ==
	 * null && criteria6 != null) return mongoTemplate.find(Query.query(new
	 * Criteria().andOperator(criteria1, criteria3, criteria6)),
	 * UserBasicInformation.class); else if (criteria2 != null && criteria3 == null
	 * && criteria4 == null && criteria5 == null && criteria6 != null) return
	 * mongoTemplate.find(Query.query(new Criteria().andOperator(criteria1,
	 * criteria2, criteria6)), UserBasicInformation.class); else return null; }
	 */

	public List<UserWorkspaces> findUserWorkspacesByCriteriaAndUserIds(boolean isPreferred, String startTime,
			String endTime, List<String> weekDays, long staffUserCoreTypeId, List<Long> userIds) {

		System.out.println("====> " + isPreferred + ", " + startTime + ", " + endTime + ", " + weekDays + ", "
				+ staffUserCoreTypeId + " " + userIds);
		if (isPreferred && startTime != null && endTime != null && weekDays != null && weekDays.size() > 0
				&& userIds != null) {

			List<BusinessesResponse> userList = userWorkspacesDAO.findAvailableUsers(weekDays, startTime, endTime,
					staffUserCoreTypeId, userIds);

			if (userList != null && userList.size() > 0) {

				Set<Long> preferredUserIds = userList.stream().map(e -> e.getPreferredProviderIds())
						.flatMap(Collection::stream).collect(Collectors.toSet());
				List<UserWorkspaces> userWorkspaces = userList.stream().map(e -> e.getUserWorkspaces())
						.collect(Collectors.toList());
				userWorkspaces = userWorkspaces.stream().filter(e -> preferredUserIds.contains(e.getUserId()))
						.collect(Collectors.toList());

				return userWorkspaces;
			} else
				return null;

		} else if (weekDays != null && weekDays.size() > 0 && startTime == null && endTime == null
				&& isPreferred == false && userIds != null) {

			List<UserWorkspaces> userList = this.getUserIdsFromUserWorkspaceCriteria(weekDays, userIds);

			if (userList != null && userList.size() > 0) {
				return userList;
			} else
				return null;

		} else if (isPreferred == true && weekDays != null && weekDays.size() > 0 && startTime == null
				&& endTime == null && userIds != null) {

			List<BusinessesResponse> response = this.getBusinessResponseFromWeekDaysAndUserIds(weekDays, userIds,
					staffUserCoreTypeId);

			if (response != null && response.size() > 0) {
				Set<Long> preferredUserIds = response.stream().map(e -> e.getPreferredProviderIds())
						.flatMap(Collection::stream).collect(Collectors.toSet());
				List<UserWorkspaces> userWorkspaces = response.stream().map(e -> e.getUserWorkspaces())
						.collect(Collectors.toList());
				userWorkspaces = userWorkspaces.stream().filter(e -> preferredUserIds.contains(e.getUserId()))
						.collect(Collectors.toList());
				return userWorkspaces;
			} else
				return null;

		} else if (isPreferred == false && startTime != null && endTime != null && weekDays != null
				&& weekDays.size() > 0) {

			List<BusinessesResponse> userWorkspaces = userWorkspacesDAO.findAvailableUsers(weekDays, startTime, endTime,
					staffUserCoreTypeId, userIds);

			if (userWorkspaces != null && userWorkspaces.size() > 0) {

				return userWorkspaces.stream().map(e -> e.getUserWorkspaces()).collect(Collectors.toList());

			} else
				return null;
		} else if (userIds != null) {

			return userWorkspacesDAO.findByUserIds(userIds);
		} else {
			return null;
		}
	}

	public List<UserWorkspaces> getUserIdsFromUserWorkspaceCriteria(List<String> weekDays, List<Long> userIds) {

		if (userIds != null) {
			List<UserWorkspaces> userWorkspaces = mongoTemplate.find(Query.query(Criteria.where("userId").in(userIds))
					.addCriteria(Criteria.where("userAvailability").elemMatch(new Criteria().andOperator(
							Criteria.where("weekDay").in(weekDays), Criteria.where("type").regex("clinic", "i")))),
					UserWorkspaces.class);
			return userWorkspaces;

		} else {
			List<UserWorkspaces> userWorkspaces = mongoTemplate.find(
					Query.query(Criteria.where("userAvailability").elemMatch(new Criteria().andOperator(
							Criteria.where("weekDay").in(weekDays), Criteria.where("type").regex("clinic", "i")))),
					UserWorkspaces.class);

			return userWorkspaces;
		}
	}

	public List<BusinessesResponse> getBusinessResponseFromWeekDaysAndUserIds(List<String> weekDays, List<Long> userIds,
			long staffUserCoreTypeId) {

		if (userIds != null) {
			Aggregation agg = Aggregation.newAggregation(
					Aggregation.lookup("user_workspaces", "_id", "businessId", "userWorkspaces"),
					Aggregation.match(new Criteria().andOperator(Criteria.where("userWorkspaces.userId").in(userIds),
							Criteria.where("userWorkspaces.staffUserCoreTypeId").in(staffUserCoreTypeId),
							Criteria.where("userWorkspaces.userAvailability")
									.elemMatch(new Criteria().andOperator(Criteria.where("weekDay").in(weekDays),
											Criteria.where("type").regex("Clinic", "i"))))));

			return mongoTemplate.aggregate(agg, Businesses.class, BusinessesResponse.class).getMappedResults();

		} else {
			return null;
		}
	}
}