package com.java.floatcare.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.java.floatcare.dao.StaffWeekOffsDAO;
import com.java.floatcare.model.StaffWeekOffs;

@Repository
public class StaffWeekOffsDAOImpl implements StaffWeekOffsDAO {

	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public StaffWeekOffs getStaffWeekOffByUserIdAndDepartmentIdAndStaffWeekOffDay(long userId, long departmentId, String weekDay) {

		StaffWeekOffs staffWeekOffObj = mongoTemplate.findOne(Query.query(Criteria.where("userId").is(userId))
				.addCriteria(Criteria.where("departmentId").is(departmentId))
				.addCriteria(Criteria.where("setScheduleWeekOffDays").in(weekDay)), StaffWeekOffs.class);

		if (staffWeekOffObj != null)
			return staffWeekOffObj;

		else {
			StaffWeekOffs staffWeekOff = mongoTemplate.findOne(Query.query(Criteria.where("userId").is(userId))
					.addCriteria(Criteria.where("departmentId").is(departmentId))
					.addCriteria(Criteria.where("staffWeekOffDays").in(weekDay)), StaffWeekOffs.class);
			return staffWeekOff;
		}
	}
	
	@Override
	public List<StaffWeekOffs> getStaffWeekOffByUserIdAndDepartmentId(List<Long> userId) {
		
		return mongoTemplate.find(Query.query(Criteria.where("userId").in(userId)), StaffWeekOffs.class);
	}

	@Override
	public void removeBySetScheduleLayoutIds(List<Long> setScheduleLayoutIds) {
		Query query = new Query();
		query.addCriteria(Criteria.where("setScheduleLayoutId").in(setScheduleLayoutIds));
		mongoTemplate.findAndRemove(query, StaffWeekOffs.class);
	}

	@Override
	public List<StaffWeekOffs> getStaffWeekOffByUserIdAndSetScheduleLayoutIdAndDepartmentId(long userId,
			List<Long> setScheduleLayoutIds, long departmentId) {

		Query query = new Query();
		query.addCriteria(Criteria.where("userId").is(userId));
		query.addCriteria(Criteria.where("departmentId").is(departmentId));
		query.addCriteria(Criteria.where("setScheduleLayoutId").in(setScheduleLayoutIds));
		query.fields().include("userId");
		query.fields().include("setScheduleLayoutId");

		return mongoTemplate.find(query, StaffWeekOffs.class);
	}

	@Override
	public List<StaffWeekOffs> getSetScheduleLayoutIdsByUserIds(List<Long> usersIds) {

		Query query = new Query();
		query.addCriteria(Criteria.where("userId").in(usersIds));
		query.fields().include("setScheduleLayoutId");

		return mongoTemplate.find(query, StaffWeekOffs.class);
	}
}
