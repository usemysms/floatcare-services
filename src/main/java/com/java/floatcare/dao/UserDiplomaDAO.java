package com.java.floatcare.dao;

import java.util.List;

import com.java.floatcare.model.UserDiploma;

public interface UserDiplomaDAO {

	UserDiploma findUserDiplomaById (long userDiplomaId);
	
	List<UserDiploma> findListOfDiplomaByUserId (long userId); 
}
