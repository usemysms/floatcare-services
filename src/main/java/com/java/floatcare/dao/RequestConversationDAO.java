package com.java.floatcare.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.java.floatcare.model.RequestConversation;


@Repository
public interface RequestConversationDAO {

	public List<RequestConversation> findMessagesBySenderIdAndReceiverId(long senderId, long receiverId);
	
}
