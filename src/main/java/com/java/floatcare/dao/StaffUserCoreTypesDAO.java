package com.java.floatcare.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.java.floatcare.model.StaffUserCoreTypes;

@Repository
public interface StaffUserCoreTypesDAO {

	List<StaffUserCoreTypes> findAllStaffUserCoreTypesByFirmType(String firmType);

}
