package com.java.floatcare.dao;

import java.util.List;

import com.java.floatcare.model.StaffWeekOffs;

public interface StaffWeekOffsDAO {

	StaffWeekOffs getStaffWeekOffByUserIdAndDepartmentIdAndStaffWeekOffDay(long userId, long departmentId, String weekDay);

	List<StaffWeekOffs> getStaffWeekOffByUserIdAndDepartmentId(List<Long> userIds);

	void removeBySetScheduleLayoutIds(List<Long> setScheduleLayoutIds);

	List<StaffWeekOffs> getStaffWeekOffByUserIdAndSetScheduleLayoutIdAndDepartmentId(long userId, List<Long> setScheduleLayoutIds, long departmentId);

	List<StaffWeekOffs> getSetScheduleLayoutIdsByUserIds(List<Long> usersIds);
	
}
