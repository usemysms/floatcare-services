package com.java.floatcare.dao;

import java.util.List;
import java.util.Map;

import com.java.floatcare.model.WorksiteSettings;

public interface WorksiteSettingsDAO {

	Map<Long, WorksiteSettings> findWorksiteSettingsByUserIdAndWorksiteIds (long userId, List<Long> worksiteIds);
}
