package com.java.floatcare.dao;

import java.util.List;

import com.java.floatcare.model.CredentialSubTypes;

public interface CredentialSubTypesDAO {
	
	List<CredentialSubTypes> getCredentialSubTypesByOrganizationId(long organizationId);
	
	List<CredentialSubTypes> getCredentialSubTypesByCredentialTypeIdAndCoreTypeIdAndOrganizationId (long credentialTypeId, Long coreType, Long organizationId);
	
	List<CredentialSubTypes> getCredentialSubTypesByStatus();
	
	List<CredentialSubTypes> getCredentialSubTypesByCredentialTypeIdAndOrganizationId (long credentialTypeId, Long organizationId);

}
