package com.java.floatcare.dao;

import java.util.List;

import com.java.floatcare.model.UserPrivacySettings;

public interface UserPrivacySettingsDAO {

	List<UserPrivacySettings> findUserPrivacySettingsByUserId(long userId);

}
