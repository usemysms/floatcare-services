package com.java.floatcare.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.java.floatcare.model.ScheduleLayout;
import com.java.floatcare.model.ScheduleLayoutGroups;

@Repository
public interface ScheduleLayoutGroupsDAO {
	
	List<Long> fetchScheduleLayoutIdByUserId(long userId);

	List<ScheduleLayoutGroups> findUsersByScheduleLayoutId(List<Long> scheduleLayout);

	void removeByScheduleLayoutIds(List<Long> scheduleLayoutIds);

	List<ScheduleLayoutGroups> findScheduleLayoutByUserId(long userId);

	List<ScheduleLayout> findScheduleLayoutOfUserByDepartmentId(long userId, long departmentId);

	List<ScheduleLayout> findScheduleLayoutOfUserByDepartmentShiftId(long userId, long departmentShiftId);
	
}