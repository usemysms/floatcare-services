package com.java.floatcare.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.java.floatcare.model.OpenShiftInvitees;

@Repository
public interface OpenShiftInviteesDAO {
	
	List<OpenShiftInvitees> findOpenShiftInviteesByUserId (long userId);

	List<OpenShiftInvitees> findInviteesByOpenShiftIdAndIsApproved(List<Long> openShiftIds, boolean isApproved);

	List<OpenShiftInvitees> findInviteesByOpenShiftIds(List<Long> openShiftsId);

	OpenShiftInvitees findByOpenShiftIdAndUserIdAndIsApproved(long openShiftId, long userId, boolean isApproved);

	List<OpenShiftInvitees> findByOpenShiftIdAndIsApproved(long openShiftId, boolean isApproved);

	List<OpenShiftInvitees> findByOpenShiftIdAndIsAccepted(long openShiftId, boolean isAccepted);

	List<Long> findByUserIdAndIsApproved(Long eachUser, boolean isApproved);

	void removeByOpenShiftIds(List<Long> openShiftIds);
	
}
