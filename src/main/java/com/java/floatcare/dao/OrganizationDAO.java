package com.java.floatcare.dao;

import java.util.List;

import com.java.floatcare.model.Organizations;


public interface OrganizationDAO {
	
	Organizations findOrganizationByName (String name);
	
	List<Organizations> findAllOrganizationsByFirmType(String firmType);
	
	List<Organizations> getUsersFromOrganizationName(String searchText);
}
