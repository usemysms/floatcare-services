package com.java.floatcare.dao;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Repository;

import com.java.floatcare.model.GeneralAvailability;

@Repository
public interface GeneralAvailabilityDAO {

	List<Long> findGeneralAvailabilityOfUsersByStartDate(LocalDate startDate);

	List<Long> findGeneralAvailabilityOfUsersByUserIdsStartDate(Set<Long> usersList, LocalDate onDate);

	List<GeneralAvailability> findGeneralAvailabilityOfUsersByUserIdStartDate(long userId, LocalDate shiftDate);
	
}