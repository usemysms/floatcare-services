package com.java.floatcare.dao;

import java.time.LocalDate;
import java.util.List;

import com.java.floatcare.model.Requests;

public interface RequestsDAO {

	List<Requests> findRequestsByUserId(long userId);

	List<Requests> findRequestsByTypeIdAndDepartmentId(List<Long> requestTypeId, long workspaceId);
	
	List<Requests> findRequestsByRequestTypeDepartmentIdAndStartDate(long requestTypeId, long workspaceId, LocalDate onDate);

	List<Requests> findByUserIdAndOnDateAndDepartmentId(long userId, LocalDate onDate, long departmentId);
	
	List<Requests> findByUserIdAndOnDateAndDepartmentIds(long userId, LocalDate onDate, List<Long> departmentId);

	List<String> findRequestsByUserIdAndOnDate(List<Long> userWorkspaceList, LocalDate localStartDate, LocalDate localEndDate);
	
	List<Requests> findRequestsByDepartmentIdAndDuration(long departmentId, LocalDate localStartDate, LocalDate localEndDate);

	List<Long> findUserIdsByRequestIdAndShiftIdAndIsApproved(long requestTypeId, long shiftId, LocalDate onDate, long departmentId, String status);

	List<Long> findUserIdsByRequestIdAndIsApproved(long requestTypeId, LocalDate onDate, long departmentId, String status);

	List<Requests> findRequestsByRequestIdAndIsApproved(long requestTypeId, LocalDate onDate, long workspaceId, String status);
	
	List<Requests> findRequestsByStatusAndDepartmentId(String string, long workspaceId);

	List<Requests> findRequestsByUserIdAndRequestTypeId(long userId, long requestTypeId);

	List<Requests> findRequestsByUserIdAndOnDate(long userId, LocalDate shiftDate);

	List<Requests> findByUserIdAndShiftTypeIdOrDepartmentId(Long userId, Long departmentShiftId, Long departmentId);

}
