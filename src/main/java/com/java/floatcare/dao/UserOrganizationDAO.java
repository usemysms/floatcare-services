package com.java.floatcare.dao;

import java.util.List;

import com.java.floatcare.model.UserOrganizations;

public interface UserOrganizationDAO {

	List<Long> findByOrganizationId (long organizationId, long staffUserCoreTypeId);

	UserOrganizations findByUserIdAndOrganizationId(long userId, long organizationId);

}
