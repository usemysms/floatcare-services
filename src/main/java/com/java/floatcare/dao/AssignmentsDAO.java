package com.java.floatcare.dao;

import java.time.LocalDate;
import java.util.List;

import com.java.floatcare.model.Assignments;

public interface AssignmentsDAO {
	
	Assignments getAssignmentDetails (long assignmentId);
	
	List<Assignments> getAssignmentsByBusinessId (long businessId);

	Assignments findAssignmentById(long assignmentId);

	List<Long> findAssignmentsByEndDate();

	List<Assignments> findAssignmentByIds(List<Long> assignmentIds);

	List<Assignments> findAssignmentByGivenDate(LocalDate givenDate);
	
	List<Assignments> findAssignmentByBusinessIdAndGivenDate(long businessId, LocalDate givenDate);

	List<Long> findByStartDateAndEndDateAndBusinessId(LocalDate localStartDate, LocalDate localEndDate, long businessId);
}
