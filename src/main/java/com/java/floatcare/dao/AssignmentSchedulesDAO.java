package com.java.floatcare.dao;

import java.util.List;
import java.util.Map;

import com.java.floatcare.model.AssignmentSchedules;

public interface AssignmentSchedulesDAO {

	List<AssignmentSchedules> findAssignmentSchedulesByAssignmentIdsAndUserId(List<Long> assignmentIds, long userId);
	
	List<AssignmentSchedules> findAssignmentSchedulesByAssignmentIdsAndStaffCoreTypeId(List<Long> assignmentIds, long staffCoreTypeId);

	List<AssignmentSchedules> findAssignmentSchedulesByAssignmentIdsAndUserIdAndWeekDay(List<Long> assignmentIds, long userId, String weekDay);

	AssignmentSchedules findAssignmentSchedulesById(long assignmentScheduleId);

	AssignmentSchedules findAssignmentSchedulesByAssignmentIdAndUserIdAndWeekDay(long assignmentId, long userId, String weekDay);

	Map<String, AssignmentSchedules> findAssignmentScheduleByAssignmentId(long assignmentId);

	List<AssignmentSchedules> findByStaffIdAndStartTimeAndEndTime(long staffId, List<Long> assignmentIds, String weekDay, String startTime, String endTime);

	List<AssignmentSchedules> findAssignmentSchedulesByAssignmentIdsAndWeekDay(List<Long> assignmentIds, String weekDay);
}