package com.java.floatcare.dao;

import java.util.List;
import java.util.Map;

import com.java.floatcare.model.AssignmentInvitees;

public interface AssignmentInviteesDAO {

	List<AssignmentInvitees> findByUserIdAndAssignmentIdsAndIsApproved(long userId, List<Long> assignmentIds, boolean isApproved);

	Map<Long, AssignmentInvitees> findByStaffIdsAndAssignmentIdAndIsApproved(List<Long> userIds, long assignmentId,boolean isApproved);

	List<AssignmentInvitees> findByUserIdAndAssignmentIdsAndIsAccepted(long userId, List<Long> assignmentIds, boolean isAccepted);

}