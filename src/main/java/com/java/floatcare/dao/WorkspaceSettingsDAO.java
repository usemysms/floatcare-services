package com.java.floatcare.dao;


import java.util.List;

import org.springframework.stereotype.Repository;

import com.java.floatcare.model.WorkspaceSettings;

@Repository
public interface WorkspaceSettingsDAO {

	List<WorkspaceSettings> findWorkspaceSettingsByUserId(long userId);
	
}
