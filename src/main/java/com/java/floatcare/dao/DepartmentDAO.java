package com.java.floatcare.dao;

import java.util.List;

import com.java.floatcare.model.Department;

public interface DepartmentDAO {

	List<Department> getDepartmentByBusinessIds (List<Long> businessIds);
	
	Department getDepartmentByBusinessIdAndDepartmentName (long businessId, String departmentName);
}
