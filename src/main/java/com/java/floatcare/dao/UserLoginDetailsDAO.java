package com.java.floatcare.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.java.floatcare.model.UserLoginDetails;

@Repository
public interface UserLoginDetailsDAO {

	public List<UserLoginDetails> findByUserIdAndDevice(long userId,String device);
	public List<UserLoginDetails> findByUserId(long userId);
	
}
