package com.java.floatcare.dao;

import java.time.LocalDate;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.java.floatcare.model.OpenShiftInvitees;
import com.java.floatcare.model.OpenShifts;

@Repository
public interface OpenShiftsDAO {
	
	List<OpenShifts> findByWorksiteIdAndFromSelectedDate(long worksiteId, LocalDate onDate);

	List<OpenShifts> findByOpenShiftId(List<OpenShiftInvitees> inviteesList);

	OpenShifts findByUserIdAndOnDateAndDepartmentId(long userId, LocalDate shiftDate, long departmentId);
	
	List<OpenShifts> findByUserIdAndOnDateAndDepartmentIds(long userId, LocalDate shiftDate, List<Long> departmentIds); 

	List<OpenShifts> findByUserIdAndStartDateEndDateAndDepartmentId(long userId, LocalDate localStartDate, LocalDate localEndDate, long departmentId);

	List<OpenShifts> findByWorkspaceIdAndFromSelectedDate(long workspaceId, LocalDate onDate);

	List<LocalDate> findOpenShiftsByStatusAndDepartmentId(String status, long workspaceId);

	List<OpenShifts> findByWorkspaceIdAndStatusAndOnDate(long workspaceId, LocalDate onDate, String status);

	List<Long> findByDepartmentIdAndOnDate(long workspaceId, LocalDate onDate);

	void removeOpenShiftByCurrentDateAndDepartmentId(LocalDate currentDate, long workspaceId);
	
}
