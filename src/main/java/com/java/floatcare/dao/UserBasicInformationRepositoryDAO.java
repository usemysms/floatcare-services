package com.java.floatcare.dao;

import java.util.List;
import java.util.Set;

import com.java.floatcare.api.request.UserRequest;
import com.java.floatcare.model.UserBasicInformation;


public interface UserBasicInformationRepositoryDAO {

	UserBasicInformation findUserByEmail(String email);
	
	List<UserBasicInformation> findUsersByEmail(String email);
	
	UserBasicInformation findUserByPhone(String phone);
	
	UserBasicInformation findAllUsersByPhone(String phone);

	List<UserBasicInformation> findUserById(long userId);

	List<UserBasicInformation> findUserByorganizationIdAndStaffUserCoreTypeId(long staffUserCoreTypeId, long organizationId);

	List<UserBasicInformation> findColleguesByWorksiteId(long userId, long businessId);

	List<UserBasicInformation> findByUserId(List<Long> userId);
	
	List<UserBasicInformation> findByListOfUserIds(List<Long> userId);

	List<UserBasicInformation> findByUserIdAndStaffUserCoreTypeId(List<Long> users, long staffUserCoreTypeId);

	long findUserByIdAndUserTypeId(long userId, int userTypeId);
	
    List<UserBasicInformation> findUsersByOrganizationIdAndUserTypeId(long organizationId, int userTypeId);

	List<UserBasicInformation> findByFirstName(String name);

	List<UserBasicInformation> findByFirstNameAndEmailAndPhone(String name, String email, String phone);

	List<UserBasicInformation> findByPhone(String phone);

	List<UserBasicInformation> findByFirstNameAndLastName(String firstName, String lastName);

	List<UserBasicInformation> findByEmailAndPhone(String email, String phone);

	List<UserBasicInformation> findByNameAndEmail(String firstName, String email);

	List<UserBasicInformation> findByFirstNameAndLastNameAndEmail(String firstName, String lastName, String email);

	List<UserBasicInformation> findByNameAndPhone(String firstName, String phone);

	List<UserBasicInformation> findByFirstNameAndLastNameAndPhone(String string, String string2, String phone);

	List<UserBasicInformation> findByAddress1AndGenderAndStaffUserCoreTypeIdAndSubRoleTypeId(String location, String gender, long staffUserCoreTypeId, long subRoleTypeId);

	UserBasicInformation findByEmailOrPhone(String email);

	List<UserBasicInformation> findByLanguageAndGenderAndStaffUserCoreTypeIdAndSubRoleTypeId(String language, String gender, long staffUserCoreTypeId, long subRoleTypeId, Set<Long> userIds, String ethnicType);

	List<UserBasicInformation> findByLanguagesAndGenderAndStaffUserCoreTypeIdAndSubRoleTypeIdAndEthnicType(String language, String gender, long staffUserCoreTypeId, long subRoleTypeId, String ethnicType);

	UserBasicInformation findPrimaryCarePhysicanByUserIdAndStaffUserCoreTypeId(List<Long> userIds, int staffUserSubcoreTypeId);

	List<UserBasicInformation> findByLanguageAndGenderAndSubRoleTypeId(String language, String gender, long subRoleTypeId, Set<Long> userIds, String ethnicType);

	List<UserBasicInformation> findByStaffUserCoreTypeIdAndSubRoleTypeIdAndSpecialityTags(String firstName, String lastName, long staffUserCoreTypeId, long subRoleTypeId, List<String> specialityTags, String postalCode);

	UserRequest findByStaffUserCoreTypeIdAndSubRoleTypeIdAndSpecialityTagsAndIsPreferredAndLanguagesAndGenderAndEthnicGroupsAndAvailability(
			String firstName,String lastName, long staffUserCoreTypeId, long subRoleTypeId, List<String> specialityTags, boolean isPreferred,
			String language, String gender, String ethnicGroup, String startTime, String endTime,
			List<String> weekDays, String postolCode);

}
