package com.java.floatcare.dao;

import java.time.LocalDate;
import java.util.List;

import org.joda.time.DateTime;

import com.java.floatcare.model.ScheduleLayout;

public interface ScheduleLayoutDAO {
	
	ScheduleLayout findAvailableScheduleLayoutForRole(List<Long> scheduleLayoutId, DateTime scheduleStartDate, long departmentId);

	ScheduleLayout findAvailableScheduleLayoutForShiftTypeAndDepartmentId(List<Long> shiftTypeId, DateTime scheduleStartDate, long departmentId, List<Long> roleTypes);

	List<ScheduleLayout> findByAvailabilityDeadlineDateAndIsPublised(DateTime makeStaffAvailabilityDate, boolean isPublisedExist, String comparison);

	List<ScheduleLayout> findAvailableScheduleLayoutForRolesAndShiftTypes(List<Long> staffCoreTypes, List<Long> shiftTypes, DateTime parse, long departmentId);

	List<ScheduleLayout> findByScheduleLayoutIdsAndShiftPlanningDate(List<Long> scheduleLayoutIds, LocalDate currentDate, long departmentId, long shiftTypeId);

	List<ScheduleLayout> findByUserIdAndCurrentDate(long userId, DateTime currentDate);

}
