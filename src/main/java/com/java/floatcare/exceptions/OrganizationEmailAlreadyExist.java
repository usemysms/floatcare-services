package com.java.floatcare.exceptions;

@SuppressWarnings("serial")
public class OrganizationEmailAlreadyExist extends Exception {

	public OrganizationEmailAlreadyExist(String message) {
		super(message);
	}
}
