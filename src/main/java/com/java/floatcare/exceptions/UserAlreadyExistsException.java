package com.java.floatcare.exceptions;

import java.util.List;

import graphql.ErrorType;
import graphql.GraphQLError;
import graphql.language.SourceLocation;

@SuppressWarnings("serial")
public class UserAlreadyExistsException extends RuntimeException implements GraphQLError {

	public UserAlreadyExistsException(String message) {
		super(message);
	}

	
	public String getMessage() {
		return "User With Same Email Exists";
	}

	@Override
	public List<SourceLocation> getLocations() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ErrorType getErrorType() {
		// TODO Auto-generated method stub
		return null;
	}
}