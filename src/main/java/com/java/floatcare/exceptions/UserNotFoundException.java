package com.java.floatcare.exceptions;

public class UserNotFoundException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1053079458126363388L;

	public UserNotFoundException(String message) {
        super(message);
    }

}
