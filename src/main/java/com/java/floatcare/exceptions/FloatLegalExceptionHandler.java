package com.java.floatcare.exceptions;

import java.io.IOException;

import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.java.floatcare.api.response.pojo.RestResponse;

@ControllerAdvice
public class FloatLegalExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(UserNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	ResponseEntity<RestResponse> handleNotFoundException(Exception e) throws IOException {
		RestResponse errorResponse = new RestResponse();
		errorResponse.setMessage("Not found");
		errorResponse.setData(e.getMessage());
		errorResponse.setStatus(HttpStatus.NOT_FOUND.value());
		errorResponse.setSuccess(false);
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorResponse);
	}

	@ExceptionHandler(ConstraintViolationException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	ResponseEntity<RestResponse> handleConstraintViolationException(ConstraintViolationException e) {
		RestResponse errorResponse = new RestResponse();
		errorResponse.setMessage("Validation error");
		errorResponse.setData(e.getMessage());
		errorResponse.setStatus(HttpStatus.BAD_REQUEST.value());
		errorResponse.setSuccess(false);
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
	}

	@ExceptionHandler(Exception.class)
	ResponseEntity<RestResponse> handleGenericException(Exception e) throws IOException {
		RestResponse errorResponse = new RestResponse();
		errorResponse.setMessage("Something went wrong");
		errorResponse.setData(e.getMessage());
		errorResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
		errorResponse.setSuccess(false);
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorResponse);
	}
}
