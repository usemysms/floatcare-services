package com.java.floatcare.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "family_contact")
public class FamilyContacts {

	@Id
	private long familyContactId;
	private long clientId;
	private String firstName;
	private String lastName;
	private String phoneNumber;
	private String email;
	private String relation;
	private boolean isEmergencyContact;

	public long getFamilyContactId() {
		return familyContactId;
	}

	public void setFamilyContactId(long familyContactId) {
		this.familyContactId = familyContactId;
	}

	public long getClientId() {
		return clientId;
	}

	public void setClientId(long clientId) {
		this.clientId = clientId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRelation() {
		return relation;
	}

	public void setRelation(String relation) {
		this.relation = relation;
	}

	public boolean getIsEmergencyContact() {
		return isEmergencyContact;
	}

	public void setIsEmergencyContact(boolean isEmergencyContact) {
		this.isEmergencyContact = isEmergencyContact;
	}

	@Override
	public String toString() {
		return "FamilyContacts [familyContactId=" + familyContactId + ", firstName=" + firstName + ", lastName="
				+ lastName + ", phoneNumber=" + phoneNumber + ", email=" + email + ", relation=" + relation
				+ ", isEmergencyContact=" + isEmergencyContact + "]";
	}	
}