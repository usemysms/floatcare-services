package com.java.floatcare.model;

import java.time.LocalTime;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "department_shifts")
public class DepartmentShifts {

	@Id
	private long departmentShiftId;
	private long departmentId;
	private String color;
	private String icon;
	private LocalTime startTime; // 24 hour format
	private LocalTime endTime; // 24 hour format
	private String label;
	private String status; // active or deleted
	private List<Long> staffCoreTypeIds;

	public long getDepartmentShiftId() {
		return departmentShiftId;
	}

	public void setDepartmentShiftId(long departmentShiftId) {
		this.departmentShiftId = departmentShiftId;
	}

	public long getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(long departmentId) {
		this.departmentId = departmentId;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public LocalTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalTime startTime) {
		this.startTime = startTime;
	}

	public LocalTime getEndTime() {
		return endTime;
	}

	public void setEndTime(LocalTime endTime) {
		this.endTime = endTime;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}


	public List<Long> getStaffCoreTypeIds() {
		return staffCoreTypeIds;
	}

	public void setStaffCoreTypeIds(List<Long> staffCoreTypeIds) {
		this.staffCoreTypeIds = staffCoreTypeIds;
	}

	@Override
	public String toString() {
		return "DepartmentShifts [departmentShiftId=" + departmentShiftId + ", departmentId=" + departmentId
				+ ", color=" + color + ", icon=" + icon + ", startTime=" + startTime + ", endTime=" + endTime
				+ ", label=" + label + ", status=" + status + ", staffCoreTypeIds=" + staffCoreTypeIds + "]";
	}

	
}
