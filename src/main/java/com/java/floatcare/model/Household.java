package com.java.floatcare.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "household")
public class Household {

	@Id
	private long householdId;
	private long clientId;
	private String address;
	private String address2;
	private String state;
	private String zip;
	private String country;
	private String longitude;
	private String latitude;
	private String phoneNumber;
	private String email;
	private List<FamilyContacts> familyContacts = new ArrayList<>();

	public long getHouseholdId() {
		return householdId;
	}

	public void setHouseholdId(long householdId) {
		this.householdId = householdId;
	}

	public long getClientId() {
		return clientId;
	}

	public void setClientId(long clientId) {
		this.clientId = clientId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<FamilyContacts> getFamilyContacts() {
		return familyContacts;
	}

	public void setFamilyContacts(List<FamilyContacts> familyContactList) {
		this.familyContacts.addAll(familyContactList);
	}

	@Override
	public String toString() {
		return "Household [householdId=" + householdId + ", clientId=" + clientId + ", address=" + address + ", state="
				+ state + ", zip=" + zip + ", country=" + country + ", longitude=" + longitude + ", latitude="
				+ latitude + ", phoneNumber=" + phoneNumber + ", email=" + email + ", familyContacts=" + familyContacts
				+ "]";
	}
}