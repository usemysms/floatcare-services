package com.java.floatcare.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/***
 * 
 * User Details will be captured in User Types such as Account Owner(System
 * Admin), Admin, Staff, Guest, Custom [FloatCare-Architecture-Super-Admin-panel
 * | Coggle]
 * 
 *
 */

@Document(collection = "user_types")
public class UserTypes {

	@Id
	private long userTypeId;
	private String label;

	public long getUserTypeId() {
		return userTypeId;
	}

	public void setUserTypeId(long userTypeId) {
		this.userTypeId = userTypeId;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	@Override
	public String toString() {
		return "UserTypes [userTypeId=" + userTypeId + ", label=" + label + "]";
	}

}
