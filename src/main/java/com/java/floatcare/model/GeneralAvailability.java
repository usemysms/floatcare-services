package com.java.floatcare.model;

import java.time.LocalDate;
import java.time.LocalTime;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "general_availability")
public class GeneralAvailability {

	@Id
	private long generalAvailabilityId;
	private long userId;
	private long scheduleLayoutId;
	private LocalDate startDate;
	private LocalDate endDate;
	private LocalTime startTime;
	private LocalTime endTime;
	private boolean isAvailable;

	public long getAvailabilityId() {
		return generalAvailabilityId;
	}

	public void setAvailabilityId(long generalAvailabilityId) {
		this.generalAvailabilityId = generalAvailabilityId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getScheduleLayoutId() {
		return scheduleLayoutId;
	}

	public void setScheduleLayoutId(long scheduleLayoutId) {
		this.scheduleLayoutId = scheduleLayoutId;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public LocalTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalTime startTime) {
		this.startTime = startTime;
	}

	public LocalTime getEndTime() {
		return endTime;
	}

	public void setEndTime(LocalTime endTime) {
		this.endTime = endTime;
	}

	public boolean isAvailable() {
		return isAvailable;
	}

	public void setAvailable(boolean isAvailable) {
		this.isAvailable = isAvailable;
	}

	@Override
	public String toString() {
		return "GeneralAvailability [generalAvailabilityId=" + generalAvailabilityId + ", userId=" + userId + ", scheduleLayoutId="
				+ scheduleLayoutId + ", startDate=" + startDate + ", endDate=" + endDate + ", startTime=" + startTime
				+ ", endTime=" + endTime + ", isAvailable=" + isAvailable + "]";
	}
}