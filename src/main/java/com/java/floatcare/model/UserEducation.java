package com.java.floatcare.model;

import java.time.LocalDate;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/*
 * 
 * User Education will be captured as a part of 
 * Data: school, degreeType/program type, field of study, graduation date
 * 
 */

@Document(collection = "user_education")
public class UserEducation {

	@Id
	private long userEducationId;
	private long userId;
	private String school;
	private String degreeTypeId;
	private String degreeTypeName;
	private String degreeOfStudy;
	private LocalDate graduationDate;
	private String location;
	private String state;

	public UserEducation() {

	}

	public UserEducation(long userId, String school, String degreeTypeId, String degreeOfStudy, LocalDate graduationDate,
			String location, String state) {
		super();
		this.userId = userId;
		this.school = school;
		this.degreeTypeId = degreeTypeId;
		this.degreeOfStudy = degreeOfStudy;
		this.graduationDate = graduationDate;
		this.location = location;
		this.state = state;
	}

	public long getUserEducationId() {
		return userEducationId;
	}

	public void setUserEducationId(long userEducationId) {
		this.userEducationId = userEducationId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getSchool() {
		return school;
	}

	public void setSchool(String school) {
		this.school = school;
	}

	public String getDegreeOfStudy() {
		return degreeOfStudy;
	}

	public void setDegreeOfStudy(String degreeOfStudy) {
		this.degreeOfStudy = degreeOfStudy;
	}

	public LocalDate getGraduationDate() {
		return graduationDate;
	}

	public void setGraduationDate(LocalDate graduationDate) {
		this.graduationDate = graduationDate;
	}

	public String getDegreeTypeId() {
		return degreeTypeId;
	}

	public void setDegreeTypeId(String degreeTypeId) {
		this.degreeTypeId = degreeTypeId;
	}

	public String getDegreeTypeName() {
		return degreeTypeName;
	}

	public void setDegreeTypeName(String degreeTypeName) {
		this.degreeTypeName = degreeTypeName;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "UserEducation [userEducationId=" + userEducationId + ", userId=" + userId + ", school=" + school
				+ ", degreeTypeId=" + degreeTypeId + ", degreeTypeName=" + degreeTypeName + ", degreeOfStudy="
				+ degreeOfStudy + ", graduationDate=" + graduationDate + ", location=" + location + ", state=" + state
				+ "]";
	}
}
