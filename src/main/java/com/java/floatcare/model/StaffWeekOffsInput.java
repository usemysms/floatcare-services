package com.java.floatcare.model;

import java.util.ArrayList;
import java.util.List;

public class StaffWeekOffsInput {

	private long userId;
	private List<String> staffWeekOffDays = new ArrayList<>();

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public List<String> getStaffWeekOffDays() {
		return staffWeekOffDays;
	}

	public void setStaffWeekOffDays(List<String> staffWeekOffDaysList) {
		staffWeekOffDays.addAll(staffWeekOffDaysList);
	}

	@Override
	public String toString() {
		return "StaffWeekOffsInput [userId=" + userId + ", staffWeekOffDays=" + staffWeekOffDays + "]";
	}

}