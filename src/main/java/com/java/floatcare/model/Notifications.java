package com.java.floatcare.model;

import java.time.LocalDate;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "notifications")
public class Notifications {

	@Id
	private long notificationId;
	private long notificationTypeId; // shift planning
	private long userId;
	private String notificationHeader;
	private String content; // The shift plan has started for worksite (worksiteName) from (fromDate) to (todate) and deadline to complete by (lastDeadline)
	private long resourceId;
	private String resourceLink;
	private boolean isRead;
	private LocalDate onDate;
	private String onTime;
	private long sourceId;

	public long getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(long notificationId) {
		this.notificationId = notificationId;
	}

	public long getNotificationTypeId() {
		return notificationTypeId;
	}

	public void setNotificationTypeId(long notificationTypeId) {
		this.notificationTypeId = notificationTypeId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getNotificationHeader() {
		return notificationHeader;
	}

	public void setNotificationHeader(String notificationHeader) {
		this.notificationHeader = notificationHeader;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public long getResourceId() {
		return resourceId;
	}

	public void setResourceId(long resourceId) {
		this.resourceId = resourceId;
	}

	public String getResourceLink() {
		return resourceLink;
	}

	public void setResourceLink(String resourceLink) {
		this.resourceLink = resourceLink;
	}

	public boolean isRead() {
		return isRead;
	}

	public void setRead(boolean isRead) {
		this.isRead = isRead;
	}

	public LocalDate getOnDate() {
		return onDate;
	}

	public void setOnDate(LocalDate onDate) {
		this.onDate = onDate;
	}

	public String getOnTime() {
		return onTime;
	}

	public void setOnTime(String onTime) {
		this.onTime = onTime;
	}

	public long getSourceId() {
		return sourceId;
	}

	public void setSourceId(long sourceId) {
		this.sourceId = sourceId;
	}

	@Override
	public String toString() {
		return "Notifications [notificationId=" + notificationId + ", notificationTypeId=" + notificationTypeId
				+ ", userId=" + userId + ", content=" + content + ", resourceId=" + resourceId + ", resourceLink="
				+ resourceLink + ", isRead=" + isRead + ", onDate=" + onDate + "]";
	}

}