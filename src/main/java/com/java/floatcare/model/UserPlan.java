package com.java.floatcare.model;

import java.time.LocalDate;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Document(collection = "user_plan")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class UserPlan {
	
	@Id
	private long planId;
	private long userId;
	private LocalDate reigisteredDate;
	private LocalDate planUpgradeDate;
	private LocalDate expirationDate;
	private String planType;
	private String activeStatus;
	

}
