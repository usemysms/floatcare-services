package com.java.floatcare.model;

public class SchedulePerformances {

	private long shiftPlanningTypeId;
	private String otherSchedulePreferences;
	
	public SchedulePerformances() {
		
	}

	public SchedulePerformances(String otherSchedulePreferences) {
		super();
		this.otherSchedulePreferences = otherSchedulePreferences;
	}

	public long getShiftPlanningTypeId() {
		return shiftPlanningTypeId;
	}

	public void setShiftPlanningTypeId(long shiftPlanningTypeId) {
		this.shiftPlanningTypeId = shiftPlanningTypeId;
	}

	public String getOtherSchedulePreferences() {
		return otherSchedulePreferences;
	}

	public void setOtherSchedulePreferences(String otherSchedulePreferences) {
		this.otherSchedulePreferences = otherSchedulePreferences;
	}

	@Override
	public String toString() {
		return "SchedulePerformances [shiftPlanningTypeId=" + shiftPlanningTypeId + ", otherSchedulePreferences="
				+ otherSchedulePreferences + "]";
	}
}
