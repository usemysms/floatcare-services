package com.java.floatcare.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "assignment_schedules")
public class AssignmentSchedules {

	@Id
	private long assignmentScheduleId;
	private long assignmentId;
	private long staffCoreTypeId;
	private long subCoreTypeId;
	private long acuityLevel;
	private String weekDay;
	private String startTime;
	private String endTime;
	private List<Long> staff = new ArrayList<>();

	public long getAssignmentScheduleId() {
		return assignmentScheduleId;
	}

	public void setAssignmentScheduleId(long assignmentScheduleId) {
		this.assignmentScheduleId = assignmentScheduleId;
	}

	public long getAssignmentId() {
		return assignmentId;
	}

	public void setAssignmentId(long assignmentId) {
		this.assignmentId = assignmentId;
	}

	public long getStaffCoreTypeId() {
		return staffCoreTypeId;
	}

	public void setStaffCoreTypeId(long staffCoreTypeId) {
		this.staffCoreTypeId = staffCoreTypeId;
	}

	public long getSubCoreTypeId() {
		return subCoreTypeId;
	}

	public void setSubCoreTypeId(long subCoreTypeId) {
		this.subCoreTypeId = subCoreTypeId;
	}

	public long getAcuityLevel() {
		return acuityLevel;
	}

	public void setAcuityLevel(long acuityLevel) {
		this.acuityLevel = acuityLevel;
	}

	public String getWeekDay() {
		return weekDay;
	}

	public void setWeekDay(String weekDay) {
		this.weekDay = weekDay;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public List<Long> getStaff() {
		return staff;
	}

	public void setStaff(List<Long> staff) {
		this.staff = staff;
	}
	
	@Override
	public String toString() {
		return "AssignmentSchedules [assignmentScheduleId=" + assignmentScheduleId + ", assignmentId=" + assignmentId
				+ ", staffCoreTypeId=" + staffCoreTypeId + ", acuityLevel=" + acuityLevel + ", weekDay=" + weekDay
				+ ", startTime=" + startTime + ", endTime=" + endTime + ", staff=" + staff + "]";
	}
}