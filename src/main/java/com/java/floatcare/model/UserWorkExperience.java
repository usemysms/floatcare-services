package com.java.floatcare.model;

import java.text.ParseException;
import java.time.LocalDate;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/***
 * 
 * User Work Experience will be captured as a part of: facility
 * name,location(address info), facility type, start date, end date/ current
 * position, position title, unit, description of job duties
 *
 */

@Document(collection = "user_work_experience")
public class UserWorkExperience {

	@Id
	private long userWorkExperienceId;
	private long userId;
	private String facilityName;
	private String location;
	private String facilityType;
	private LocalDate startDate;
	private LocalDate endDate;
	private String positionTitle;
	private String unit;
	private String description;
	private String travelAssignment;
	private boolean isCurrentlyWorking;

	public UserWorkExperience() {

	}

	public UserWorkExperience(long userId, String facilityName, String location, String facilityType, String startDate,
			String endDate, String positionTitle, String unit, String description, String travelAssignment) throws ParseException {
		super();
		this.userId = userId;
		this.facilityName = facilityName;
		this.location = location;
		this.facilityType = facilityType;
		this.startDate = LocalDate.parse(startDate);
		this.endDate = LocalDate.parse(endDate);
		this.positionTitle = positionTitle;
		this.unit = unit;
		this.description = description;
		this.travelAssignment = travelAssignment;
	}

	public long getUserWorkExperienceId() {
		return userWorkExperienceId;
	}

	public void setUserWorkExperienceId(long userWorkExperienceId) {
		this.userWorkExperienceId = userWorkExperienceId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getFacilityName() {
		return facilityName;
	}

	public void setFacilityName(String facilityName) {
		this.facilityName = facilityName;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getFacilityType() {
		return facilityType;
	}

	public void setFacilityType(String facilityType) {
		this.facilityType = facilityType;
	}

	public LocalDate getStartDate() {
		return (startDate);
	}

	public void setStartDate(LocalDate startDate) throws ParseException {
		this.startDate = (startDate);
	}

	public LocalDate getEndDate() {
		return (endDate);
	}

	public void setEndDate(LocalDate endDate) throws ParseException {
		this.endDate = (endDate);
	}

	public String getPositionTitle() {
		return positionTitle;
	}

	public void setPositionTitle(String positionTitle) {
		this.positionTitle = positionTitle;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTravelAssignment() {
		return travelAssignment;
	}

	public void setTravelAssignment(String travelAssignment) {
		this.travelAssignment = travelAssignment;
	}

	public boolean getIsCurrentlyWorking() {
		return isCurrentlyWorking;
	}

	public void setIsCurrentlyWorking(boolean isCurrentlyWorking) {
		this.isCurrentlyWorking = isCurrentlyWorking;
	}

}