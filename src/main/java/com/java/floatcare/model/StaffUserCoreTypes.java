package com.java.floatcare.model;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/***
 * 
 * 
 * Staff User Core Types will be capturing data such as Physician, Nurse, Techs,
 * Therapist, Medical Assistants, Pharmacists, Medical Technologist,Medical
 * Laboratory Technologist, Dietitian, Surgeon
 *
 */

@Document(collection = "staff_user_core_types")
public class StaffUserCoreTypes {

	@Id
	private long id;
	private String label;
	private String shortName;
	private List<String> firmType;

	public StaffUserCoreTypes() {

	}

	public StaffUserCoreTypes(long staffUserCoreTypesId, String label) {
		super();
		this.id = staffUserCoreTypesId;
		this.label = label;
	}

	public long getStaffUserCoreTypesId() {
		return id;
	}

	public void setStaffUserCoreTypesId(long staffUserCoreTypesId) {
		this.id = staffUserCoreTypesId;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public List<String> getFirmType() {
		return firmType;
	}

	public void setFirmType(List<String> firmType) {
		this.firmType = firmType;
	}

	@Override
	public String toString() {
		return String.format("StaffUserCoreTypes [" + " label=" + label + "]");
	}
	
	 @Override
	    public boolean equals(Object obj) {
		  
	        if(obj instanceof StaffUserCoreTypes)
	        {
	        	StaffUserCoreTypes temp = (StaffUserCoreTypes) obj;
	            if(this.id == temp.id)
	                return true;
	        }
	        return false;

	    }

}
