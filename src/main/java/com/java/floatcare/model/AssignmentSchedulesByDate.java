package com.java.floatcare.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class AssignmentSchedulesByDate {

	private LocalDate scheduleDate;
	private long assignmentScheduleId;
	private List<UserBasicInformation> listOfUsers = new ArrayList<>();
	private String startTime;
	private String endTime;

	public LocalDate getScheduleDate() {
		return scheduleDate;
	}

	public void setScheduleDate(LocalDate scheduleDate) {
		this.scheduleDate = scheduleDate;
	}

	public long getAssignmentScheduleId() {
		return assignmentScheduleId;
	}

	public void setAssignmentScheduleId(long assignmentScheduleId) {
		this.assignmentScheduleId = assignmentScheduleId;
	}

	public List<UserBasicInformation> getListOfUsers() {
		return listOfUsers;
	}

	public void setListOfUsers(List<UserBasicInformation> listOfUsers) {
		this.listOfUsers = listOfUsers;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	@Override
	public String toString() {
		return "AssignmentSchedulesByDate [scheduleDate=" + scheduleDate + ", listOfUsers=" + listOfUsers
				+ ", startTime=" + startTime + ", endTime=" + endTime + "]";
	}
}
