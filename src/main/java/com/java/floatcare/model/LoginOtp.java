package com.java.floatcare.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "login_otp")
public class LoginOtp {
	@Id
	long loginOtpId;
	long userId;
	String otpString;
	Date createdTime;
	boolean isExpired;
	
	public long getLoginOtpId() {
		return loginOtpId;
	}
	public void setLoginOtpId(long loginOtpId) {
		this.loginOtpId = loginOtpId;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getOtpString() {
		return otpString;
	}
	public void setOtpString(String otpString) {
		this.otpString = otpString;
	}
	public Date getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}
	public boolean isExpired() {
		return isExpired;
	}
	public void setExpired(boolean isExpired) {
		this.isExpired = isExpired;
	}
	@Override
	public String toString() {
		return "LoginOtp [loginOtpId=" + loginOtpId + ", userId=" + userId + ", otpString=" + otpString
				+ ", createdTime=" + createdTime + ", isExpired=" + isExpired + "]";
	}
	

}
