package com.java.floatcare.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/*
 * 
 * 
 * Position Types will be captured as a part of: Cardiovascular Nurse, Pediatric
 * Nurse, Pre-Op, PACU, Emergency Room Nurse, Medical-surgical
 * Nurse,Perioperative/ Operating Room Nurse (OR Nurse),Telemetry Nurse,Neonatal
 * intensive care unit (NICU) nurse,Dialysis Nurse,Labor & Delivery
 * Nurse,Radiology Nurse,Psychiatric-mental health nurse,Oncology
 * Nurse,Orthopedic Nurse,Geriatric Nurse,Nurse Anesthetist (APRN)
 *
 */

@Document(collection = "position_types")
public class PositionTypes {

	@Id
	private long positionTypeId;
	private String label;

	public PositionTypes() {

	}

	public PositionTypes(long positionType, String label) {

		this.positionTypeId = positionType;
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public long getPositionTypeId() {
		return positionTypeId;
	}

	public void setPositionTypeId(long positionTypeId) {
		this.positionTypeId = positionTypeId;
	}

	@Override
	public String toString() {
		return String.format("PositionTypes [label=" + label + "]");
	}
}
