package com.java.floatcare.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection = "request_types")
public class RequestTypes {

	@Id
	private long requestTypeId;
	private String label;

	public RequestTypes() {

	}

	public RequestTypes(long requestTypeId, String label) {
		super();
		this.requestTypeId = requestTypeId;
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public long getrequestTypeId() {
		return requestTypeId;
	}

	public void setrequestTypeId(long requestTypeId) {
		this.requestTypeId = requestTypeId;
	}

	@Override
	public String toString() {
		return String.format(", label=" + label + "]");
	}
}
