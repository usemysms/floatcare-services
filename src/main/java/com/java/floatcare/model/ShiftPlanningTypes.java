package com.java.floatcare.model;

import org.springframework.data.annotation.Id;

/*
 * Shift planning types will capture date in the form of
 * 
 * 2 weeks, 4 weeks, 6 weeks, 8 weeks, 2-6 months
 * 
 */

public class ShiftPlanningTypes {

	@Id
	private long shiftPlanningTypeId;
	private String label;
	
	public ShiftPlanningTypes() {
		
	}

	public ShiftPlanningTypes(long shiftPlanningTypeId, String label) {
		super();
		this.shiftPlanningTypeId = shiftPlanningTypeId;
		this.label = label;
	}

	public long getShiftPlanningTypeId() {
		return shiftPlanningTypeId;
	}

	public void setShiftPlanningTypeId(long shiftPlanningTypeId) {
		this.shiftPlanningTypeId = shiftPlanningTypeId;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	@Override
	public String toString() {
		return "ShiftPlanningTypes [shiftPlanningTypeId=" + shiftPlanningTypeId + ", label=" + label + "]";
	}
}
