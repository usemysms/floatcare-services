package com.java.floatcare.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/***
 * permissions will be captured as part of Owner,Admin,Admin with custom access [FloatCare-Super-Admin-main |COGGLE] 
 * 
 * permissions will be captured as part of Select & Set Admin, roles & Permissions Under Management/Admin User Types [FloatCare-Super-Admin-main| COGGLE] 
 * 
 * permissions will be captured as part of Manage Users(CRUD Users)
 */
@Document(collection = "permission")
public class Permission {

	@Id
	private long permissionId;
	private String label;

	public Permission() {

	}

	public Permission(long permissionId, String label) {
		super();
		this.permissionId = permissionId;
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public long getPermissionId() {
		return permissionId;
	}

	public void setPermissionId(long permissionId) {
		this.permissionId = permissionId;
	}

	@Override
	public String toString() {
		return String.format("Permission [ label=" + label + "]");
	}

}
