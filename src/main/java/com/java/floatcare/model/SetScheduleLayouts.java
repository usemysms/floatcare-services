package com.java.floatcare.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "set_schedule_layouts")
public class SetScheduleLayouts {

	@Id
	private long setScheduleLayoutId;
	private long worksiteId;
	private long departmentId;
	private String scheduleLength; //2W,4W,6W,1M,2M,3M 
	private LocalDate startDate;
	private LocalDate endDate; 
	private List<Long> roles = new ArrayList<>(); // staff user core type ids
	private List<Long> shiftTypes = new ArrayList<>(); // shift type ids
	private List<String> weekOffDays = new ArrayList<>(); // mon, tue
	private LocalDate createdOn;
	private LocalDate updatedOn;
	private String status;

	public long getSetScheduleLayoutId() {
		return setScheduleLayoutId;
	}

	public void setSetScheduleLayoutId(long setScheduleLayoutId) {
		this.setScheduleLayoutId = setScheduleLayoutId;
	}

	public long getWorksiteId() {
		return worksiteId;
	}

	public void setWorksiteId(long worksiteId) {
		this.worksiteId = worksiteId;
	}

	public long getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(long departmentId) {
		this.departmentId = departmentId;
	}

	public String getScheduleLength() {
		return scheduleLength;
	}

	public void setScheduleLength(String scheduleLength) {
		this.scheduleLength = scheduleLength;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public List<Long> getRoles() {
		return roles;
	}

	public void setRoles(List<Long> roleList) {
		roles.addAll(roleList);
	}

	public List<Long> getShiftTypes() {
		return shiftTypes;
	}

	public void setShiftTypes(List<Long> shiftTypeList) {
		shiftTypes.addAll(shiftTypeList);
	}

	public List<String> getWeekOffDays() {
		return weekOffDays;
	}

	public void setWeekOffDays(List<String> weekOffDays) {
		this.weekOffDays = weekOffDays;
	}

	public LocalDate getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(LocalDate createdOn) {
		this.createdOn = createdOn;
	}

	public LocalDate getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(LocalDate updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "SetScheduleLayouts [setScheduleLayoutId=" + setScheduleLayoutId + ", worksiteId=" + worksiteId
				+ ", departmentId=" + departmentId + ", scheduleLength=" + scheduleLength + ", startDate=" + startDate
				+ ", roles=" + roles + ", shiftTypes=" + shiftTypes + ", weekOffDays=" + weekOffDays + ", createdOn="
				+ createdOn + ", updatedOn=" + updatedOn +"]";
	}
}
