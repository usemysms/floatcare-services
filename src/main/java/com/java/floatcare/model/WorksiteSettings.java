package com.java.floatcare.model;

import org.springframework.data.mongodb.core.mapping.Document;

import org.springframework.data.annotation.Id;

@Document(collection = "worksite_settings")
public class WorksiteSettings {

	@Id
	private long worksiteSettingId;
	private long userId;
	private long organizationId;
	private String organizationName;
	private long worksiteId;
	private String worksiteName;
	private String color;

	public long getWorksiteSettingId() {
		return worksiteSettingId;
	}

	public void setWorksiteSettingId(long worksiteSettingId) {
		this.worksiteSettingId = worksiteSettingId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(long organizationId) {
		this.organizationId = organizationId;
	}

	public long getWorksiteId() {
		return worksiteId;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public String getWorksiteName() {
		return worksiteName;
	}

	public void setWorksiteName(String worksiteName) {
		this.worksiteName = worksiteName;
	}

	public void setWorksiteId(long worksiteId) {
		this.worksiteId = worksiteId;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	@Override
	public String toString() {
		return "WorksiteSettings [worksiteSettingId=" + worksiteSettingId + ", userId=" + userId + ", organizationId="
				+ organizationId + ", worksiteId=" + worksiteId + ", color=" + color + "]";
	}
}