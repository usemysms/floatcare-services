package com.java.floatcare.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "business_sub_types")
public class BusinessSubTypes {

	@Id
	private long businessSubTypeId;
	private String label;

	public BusinessSubTypes() {

	}

	public long getBusinessSubTypeId() {
		return businessSubTypeId;
	}

	public void setBusinessSubTypeId(long businessSubTypeId) {
		this.businessSubTypeId = businessSubTypeId;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	@Override
	public String toString() {
		return "BusinessSubTypes [businessSubTypeId=" + businessSubTypeId + ", label=" + label + "]";
	}

}
