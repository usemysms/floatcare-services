package com.java.floatcare.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "department_settings")
public class DepartmentSettings {

	@Id
	private long departmentSettingId;
	private long departmentId;
	private String customName;
	private String workWeekDay;
	private String workPayPeriod; // 1h | 10h | 2w // 1 day || 1week
	private long workPayPeriodHours;
	private String restPeriod;

	public long getDepartmentSettingId() {
		return departmentSettingId;
	}

	public void setDepartmentSettingId(long departmentSettingId) {
		this.departmentSettingId = departmentSettingId;
	}

	public long getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(long departmentId) {
		this.departmentId = departmentId;
	}

	public String getCustomName() {
		return customName;
	}

	public void setCustomName(String customName) {
		this.customName = customName;
	}

	public String getWorkWeekDay() {
		return workWeekDay;
	}

	public void setWorkWeekDay(String workWeekDay) {
		this.workWeekDay = workWeekDay;
	}

	public String getWorkPayPeriod() {
		return workPayPeriod;
	}

	public void setWorkPayPeriod(String workPayPeriod) {
		this.workPayPeriod = workPayPeriod;
	}

	public long getWorkPayPeriodHours() {
		return workPayPeriodHours;
	}

	public void setWorkPayPeriodHours(long workPayPeriodHours) {
		this.workPayPeriodHours = workPayPeriodHours;
	}

	public String getRestPeriod() {
		return restPeriod;
	}

	public void setRestPeriod(String restPeriod) {
		this.restPeriod = restPeriod;
	}

	@Override
	public String toString() {
		return "DepartmentSettings [departmentSettingId=" + departmentSettingId + ", departmentId=" + departmentId
				+ ", customName=" + customName + ", workWeekDay=" + workWeekDay + ", workPayPeriod=" + workPayPeriod
				+ ", workPayPeriodHours=" + workPayPeriodHours + "]";
	}
}