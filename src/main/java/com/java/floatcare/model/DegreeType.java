package com.java.floatcare.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/*
 * 
 * Degree Types will be captured as a part of User Education
 * 
 * Data: Technical/Nursing, College University, Graduate, Other
 * 
 */

@Document(collection = "degree_type")
public class DegreeType {

	@Id
	private long degreeTypeId;
	private String label;

	public DegreeType() {

	}

	public DegreeType(long degreeTypeId, String label) {
		super();
		this.degreeTypeId = degreeTypeId;
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public long getDegreeTypeId() {
		return degreeTypeId;
	}

	public void setDegreeTypeId(long degreeTypeId) {
		this.degreeTypeId = degreeTypeId;
	}

}