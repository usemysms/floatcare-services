package com.java.floatcare.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/*
 * 
 * Availabiity State Types will be captured as a part of 
 * Working, On Call, Sick, Vacation, Available
 * 
 * 
 */
@Document(collection = "availability_state_types")
public class AvailabilityStateTypes {

	@Id
	private long availabilityStateTypes;
	private String status;

	public AvailabilityStateTypes() {

	}

	public AvailabilityStateTypes(String status) {
		super();
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public long getAvailabilityStateTypes() {
		return availabilityStateTypes;
	}

	public void setAvailabilityStateTypes(long availabilityStateTypes) {
		this.availabilityStateTypes = availabilityStateTypes;
	}

	@Override
	public String toString() {
		return "AvailabilityStateTypes [status=" + status + "]";
	}

}
