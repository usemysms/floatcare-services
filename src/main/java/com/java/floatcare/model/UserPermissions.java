package com.java.floatcare.model;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.java.floatcare.api.request.pojo.DepartmentsList;
import com.java.floatcare.api.request.pojo.PermissionSet;
import com.java.floatcare.api.request.pojo.WorksitesList;

@Document(collection = "user_permissions")
public class UserPermissions {

	@Id
	private long userId;
	private long organizationId;
	private String accessLevel;
	private List<Long> organizationIds;
	private List<WorksitesList> worksiteIds;
	private List<DepartmentsList> departmentIds;
	private List<PermissionSet> permissionSet;

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(long organizationId) {
		this.organizationId = organizationId;
	}

	public String getAccessLevel() {
		return accessLevel;
	}

	public void setAccessLevel(String accessLevel) {
		this.accessLevel = accessLevel;
	}

	public List<Long> getOrganizationIds() {
		return organizationIds;
	}

	public void setOrganizationIds(List<Long> organizationIds) {
		this.organizationIds = organizationIds;
	}

	public List<WorksitesList> getWorksiteIds() {
		return worksiteIds;
	}

	public void setWorksiteIds(List<WorksitesList> worksiteIds) {
		this.worksiteIds = worksiteIds;
	}

	public List<DepartmentsList> getDepartmentIds() {
		return departmentIds;
	}

	public void setDepartmentIds(List<DepartmentsList> departmentIds) {
		this.departmentIds = departmentIds;
	}

	public List<PermissionSet> getPermissionSet() {
		return permissionSet;
	}

	public void setPermissionSet(List<PermissionSet> permissionSet) {
		this.permissionSet = permissionSet;
	}

	@Override
	public String toString() {
		return "UserPermissions [userId=" + userId + ", organizationId=" + organizationId + ", accessLevel="
				+ accessLevel + ", organizationIds=" + organizationIds + ", worksiteIds=" + worksiteIds
				+ ", departmentIds=" + departmentIds + ", permissionSet=" + permissionSet + "]";
	}
}