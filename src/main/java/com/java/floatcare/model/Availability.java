package com.java.floatcare.model;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "availability")
public class Availability {

	@Id
	private long availabilityId;
	private long userId;
	private long scheduleLayoutId;
	private LocalDate startDate;
	private LocalDate endDate;
	private List<LocalDate> onDate = new ArrayList<>();
	private LocalTime startTime;
	private LocalTime endTime;
	private boolean isAvailable;
	private String status;					/*published || draft*/

	public long getAvailabilityId() {
		return availabilityId;
	}

	public void setAvailabilityId(long availabilityId) {
		this.availabilityId = availabilityId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getScheduleLayoutId() {
		return scheduleLayoutId;
	}

	public void setScheduleLayoutId(long scheduleLayoutId) {
		this.scheduleLayoutId = scheduleLayoutId;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public LocalTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalTime startTime) {
		this.startTime = startTime;
	}

	public LocalTime getEndTime() {
		return endTime;
	}

	public void setEndTime(LocalTime endTime) {
		this.endTime = endTime;
	}

	public boolean isAvailable() {
		return isAvailable;
	}

	public void setAvailable(boolean isAvailable) {
		this.isAvailable = isAvailable;
	}
	
	public List<LocalDate> getOnDate () {
		return onDate;
	}
	
	public void setOnDate (List<LocalDate> onDates) {
		onDate.addAll(onDates);
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Availability [availabilityId=" + availabilityId + ", userId=" + userId + ", scheduleLayoutId="
				+ scheduleLayoutId + ", startDate=" + startDate + ", endDate=" + endDate + ", startTime=" + startTime
				+ ", endTime=" + endTime + ", isAvailable=" + isAvailable + "]";
	}

}
