package com.java.floatcare.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/*
 * 
 * Facility type will be captured as a part of Ambulatory Surgical Center, Clinics and Medical Offices, Dialysis Centers, Freestanding Emergency Room, Hospital, Imaging and Radiology center, skilled nursing facility 
 * 	Ambulatory Surgical Center
 */

@Document(collection = "business_types")
public class BusinessTypes {

	@Id
	private long businessTypeId;
	private String label;

	public BusinessTypes() {

	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public long getBusinessTypeId() {

		return businessTypeId;		
	}

	public void setBusinessTypeId(long businessTypeId) {
		this.businessTypeId = businessTypeId;
	}

	@Override
	public String toString() {
		return "BusinessType [businessTypeId=" + businessTypeId + ", label=" + label + "]";
	}

}
