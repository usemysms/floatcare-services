package com.java.floatcare.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "user_notification")
public class UserNotification {

	@Id
	private long userNotificationId;
	private long userId;
	private long notificationTypeId; // (Mentions,UpdatesFromCollegues, Reminders, Birthdays, Events,OtherNotifications) -- master data
	private boolean isEnablePush;
	private boolean isEnableSMS;
	private boolean isEnableEmail;
	
	public long getUserNotificationId() {
		return userNotificationId;
	}

	public void setUserNotificationId(long userNotificationId) {
		this.userNotificationId = userNotificationId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getNotificationTypeId() {
		return notificationTypeId;
	}

	public void setNotificationTypeId(long notificationTypeId) {
		this.notificationTypeId = notificationTypeId;
	}

	public boolean isEnablePush() {
		return isEnablePush;
	}

	public void setEnablePush(boolean isEnablePush) {
		this.isEnablePush = isEnablePush;
	}

	public boolean isEnableSMS() {
		return isEnableSMS;
	}

	public void setEnableSMS(boolean isEnableSMS) {
		this.isEnableSMS = isEnableSMS;
	}

	public boolean isEnableEmail() {
		return isEnableEmail;
	}

	public void setEnableEmail(boolean isEnableEmail) {
		this.isEnableEmail = isEnableEmail;
	}

	@Override
	public String toString() {
		return "UserNotification [userNotificationId=" + userNotificationId + ", userId=" + userId
				+ ", notificationTypeId=" + notificationTypeId + ", isEnablePush=" + isEnablePush + ", isEnableSMS="
				+ isEnableSMS + ", isEnableEmail=" + isEnableEmail + "]";
	}

}