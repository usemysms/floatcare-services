package com.java.floatcare.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "requests")
public class Requests {

	@Id
	private long requestId;
	private long worksiteId; // (businessId)
	private long departmentId;
	private long requestTypeId;
	private long userId;
	private String name;
	private String profilePhoto;
	private List<LocalDate> onDate = new ArrayList<LocalDate>();
	private List<Long> shiftIds; // (Example : for swap shift)
	private long colleagueId;
	private String notes;
	private String status;

	public long getRequestId() {
		return requestId;
	}

	public void setRequestId(long requestId) {
		this.requestId = requestId;
	}

	public long getWorksiteId() {
		return worksiteId;
	}

	public void setWorksiteId(long worksiteId) {
		this.worksiteId = worksiteId;
	}

	public long getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(long departmentId) {
		this.departmentId = departmentId;
	}

	public long getRequestTypeId() {
		return requestTypeId;
	}

	public void setRequestTypeId(long requestTypeId) {
		this.requestTypeId = requestTypeId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public List<LocalDate> getOnDate() {
		return onDate;
	}

	public void setOnDate(List<LocalDate> onDateList) {
		onDate.addAll(onDateList);
	}

	public List<Long> getShiftIds() {
		return shiftIds;
	}

	public void setShiftIds(List<Long> shiftIds) {
		this.shiftIds = shiftIds;
	}

	public long getColleagueId() {
		return colleagueId;
	}

	public void setColleagueId(long colleagueId) {
		this.colleagueId = colleagueId;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProfilePhoto() {
		return profilePhoto;
	}

	public void setProfilePhoto(String profilePhoto) {
		this.profilePhoto = profilePhoto;
	}
	

	@Override
	public String toString() {
		return "Requests [requestId=" + requestId + ", worksiteId=" + worksiteId + ", departmentId=" + departmentId
				+ ", requestTypeId=" + requestTypeId + ", userId=" + userId + ", onDate=" + onDate + ", shiftId="
				+ shiftIds + ", colleagueId=" + colleagueId + ", notes=" + notes + ", status=" + status + "]";
	}
}