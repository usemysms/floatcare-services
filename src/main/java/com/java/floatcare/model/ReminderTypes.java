package com.java.floatcare.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "reminder_types")
public class ReminderTypes {

	@Id
	private long reminderTypeId;
	private String label;

	public long getReminderTypeId() {
		return reminderTypeId;
	}

	public void setReminderTypeId(long reminderTypeId) {
		this.reminderTypeId = reminderTypeId;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

}
