package com.java.floatcare.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "block_user_workspace")
public class BlockUserWorkspace {

	@Id
	private long blockUserWorkspaceId;
	private long workspaceId;
	private long userId;
	private long blockedUserId; // blocked user by an user

	public long getBlockUserWorkspaceId() {
		return blockUserWorkspaceId;
	}

	public void setBlockUserWorkspaceId(long blockUserWorkspaceId) {
		this.blockUserWorkspaceId = blockUserWorkspaceId;
	}

	public long getWorkspaceId() {
		return workspaceId;
	}

	public void setWorkspaceId(long workspaceId) {
		this.workspaceId = workspaceId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getBlockedUserId() {
		return blockedUserId;
	}

	public void setBlockedUserId(long blockedUserId) {
		this.blockedUserId = blockedUserId;
	}

	@Override
	public String toString() {
		return "BlockUserWorkspace [blockUserWorkspaceId=" + blockUserWorkspaceId + ", workspaceId=" + workspaceId
				+ ", userId=" + userId + ", blockedUser=" + blockedUserId + "]";
	}

}
