package com.java.floatcare.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "notification_types")
public class NotificationTypes {

	@Id
	private long notificationTypeId;
	private String label;

	public NotificationTypes(long notificationTypeId, String label) {
		super();
		this.notificationTypeId = notificationTypeId;
		this.label = label;
	}

	public long getNotificationTypeId() {
		return notificationTypeId;
	}

	public void setNotificationTypeId(long notificationTypeId) {
		this.notificationTypeId = notificationTypeId;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	@Override
	public String toString() {
		return "NotificationTypes [notificationTypeId=" + notificationTypeId + ", label=" + label + "]";
	}

}
