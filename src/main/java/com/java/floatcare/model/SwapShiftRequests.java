package com.java.floatcare.model;

import java.time.LocalDate;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "swap_shift_requests")
public class SwapShiftRequests {

	@Id
	private long swapShiftRequestId;
	private long senderId;
	private long receiverId;
	private long sourceShiftId;
	private long requestedShiftId;
	private boolean isRecieverAccepted;
	private boolean isSupervisorAccepted;
	private LocalDate createdOn;
	private LocalDate updatedOn;
	private String status; //pending, approved

	public long getSwapShiftRequestId() {
		return swapShiftRequestId;
	}

	public void setSwapShiftRequestId(long swapShiftRequestId) {
		this.swapShiftRequestId = swapShiftRequestId;
	}

	public long getSenderId() {
		return senderId;
	}

	public void setSenderId(long senderId) {
		this.senderId = senderId;
	}

	public long getReceiverId() {
		return receiverId;
	}

	public void setReceiverId(long receiverId) {
		this.receiverId = receiverId;
	}

	public long getSourceShiftId() {
		return sourceShiftId;
	}

	public void setSourceShiftId(long sourceShiftId) {
		this.sourceShiftId = sourceShiftId;
	}

	public long getRequestedShiftId() {
		return requestedShiftId;
	}

	public void setRequestedShiftId(long requestedShiftId) {
		this.requestedShiftId = requestedShiftId;
	}

	public boolean isReceiverAccepted() {
		return isRecieverAccepted;
	}

	public void setReceiverAccepted(boolean isRecieverAccepted) {
		this.isRecieverAccepted = isRecieverAccepted;
	}

	public boolean isSupervisorAccepted() {
		return isSupervisorAccepted;
	}

	public void setSupervisorAccepted(boolean isSupervisorAccepted) {
		this.isSupervisorAccepted = isSupervisorAccepted;
	}

	public LocalDate getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(LocalDate createdOn) {
		this.createdOn = createdOn;
	}

	public LocalDate getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(LocalDate updatedOn) {
		this.updatedOn = updatedOn;
	}

	public boolean isRecieverAccepted() {
		return isRecieverAccepted;
	}

	public void setRecieverAccepted(boolean isRecieverAccepted) {
		this.isRecieverAccepted = isRecieverAccepted;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "SwapShiftRequests [swapShiftRequestId=" + swapShiftRequestId + ", senderId=" + senderId
				+ ", receiverId=" + receiverId + ", sourceShiftId=" + sourceShiftId + ", requestedShiftId="
				+ requestedShiftId + ", isRecieverAccepted=" + isRecieverAccepted + ", isSupervisorAccepted="
				+ isSupervisorAccepted + ", createdOn=" + createdOn + ", updatedOn=" + updatedOn + "]";
	}
}
