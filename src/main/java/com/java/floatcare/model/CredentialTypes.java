package com.java.floatcare.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/*
 * 
 * Credentials types will be captured as part of user credentials with license, certification and document 
 *
 */

@Document(collection = "credential_types")
public class CredentialTypes {

	@Id
	private long credentialTypeId;
	private String label;

	public CredentialTypes() {

	}

	public CredentialTypes(long credentialTypeId, String label) {
		super();
		this.credentialTypeId = credentialTypeId;
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

	public long getCredentialTypeId() {
		return credentialTypeId;
	}

	public void setCredentialTypeId(long credentialTypesId) {
		this.credentialTypeId = credentialTypesId;
	}

	public void setLabel(String label) {
		this.label = label;
	}

}