package com.java.floatcare.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "request_conversation")
public class RequestConversation {

	@Id
	private long requestConversationId;
	private long requestId;
	private long senderId;
	private long receiverId ;
	private String text;
	private String timeStamp;
	private String timeZone;

	public long getRequestConversationId() {
		return requestConversationId;
	}

	public void setRequestConversationId(long requestConversationId) {
		this.requestConversationId = requestConversationId;
	}

	public long getRequestId() {
		return requestId;
	}

	public void setRequestId(long requestId) {
		this.requestId = requestId;
	}

	public long getSenderId() {
		return senderId;
	}

	public void setSenderId(long senderId) {
		this.senderId = senderId;
	}

	public long getReceiverId() {
		return receiverId ;
	}

	public void setReceiverId(long receiverId) {
		this.receiverId = receiverId;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	@Override
	public String toString() {
		return "RequestConversation [requestConversationId=" + requestConversationId + ", requestId=" + requestId
				+ ", senderId=" + senderId + ", recieverId=" + receiverId + ", text=" + text + "]";
	}

}
