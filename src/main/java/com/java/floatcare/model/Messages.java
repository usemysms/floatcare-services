package com.java.floatcare.model;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.java.floatcare.api.request.pojo.IsSeenByGroupMembers;

@Document(collection = "messages")
public class Messages {

	@Id
	private long messageId;
	private String text;
	private Long senderId;
	private Long receiverId;
	private boolean isSeen;
	private LocalTime timeStamp;
	private String timeZone;
	private String date;
	private long epochSeconds;
	private String aesKey;
	private String initializationVector;
	private String type;  //attachment/text/video/link/image/
	private List<String> attachments; //: [{name:""}]
	private String sourceType; //: replied/forwarded/new/generated
	private Long sourceMessageId;
	private long groupId;
	private List<IsSeenByGroupMembers> isSeenByGroupMembers = new ArrayList<>(); //":[{userId:12,when:time},{userId:12,when:time}
	
	public long getMessageId() {
		return messageId;
	}

	public void setMessageId(long messageId) {
		this.messageId = messageId;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Long getSenderId() {
		return senderId;
	}

	public void setSenderId(long senderId) {
		this.senderId = senderId;
	}

	public Long getReceiverId() {
		return receiverId;
	}

	public void setReceiverId(long receiverId) {
		this.receiverId = receiverId;
	}

	public boolean isSeen() {
		return isSeen;
	}

	public void setSeen(boolean isSeen) {
		this.isSeen = isSeen;
	}

	public LocalTime getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(LocalTime timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public long getEpochSeconds() {
		return epochSeconds;
	}

	public void setEpochSeconds(long epochSeconds) {
		this.epochSeconds = epochSeconds;
	}

	public String getAesKey() {
		return aesKey;
	}

	public void setAesKey(String aesKey) {
		this.aesKey = aesKey;
	}

	public String getInitializationVector() {
		return initializationVector;
	}

	public void setInitializationVector(String initializationVector) {
		this.initializationVector = initializationVector;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<String> getAttachments() {
		return attachments;
	}

	public void setAttachments(List<String> attachments) {
		this.attachments = attachments;
	}

	public String getSourceType() {
		return sourceType;
	}

	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}

	public Long getSourceMessageId() {
		return sourceMessageId;
	}

	public void setSourceMessageId(Long sourceMessageId) {
		this.sourceMessageId = sourceMessageId;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public List<IsSeenByGroupMembers> getIsSeenByGroupMembers() {
		return isSeenByGroupMembers;
	}

	public void setIsSeenByGroupMembers(IsSeenByGroupMembers isSeenByGroupMembers) {
		this.isSeenByGroupMembers.add(isSeenByGroupMembers);
	}

	public void setSenderId(Long senderId) {
		this.senderId = senderId;
	}

	public void setReceiverId(Long receiverId) {
		this.receiverId = receiverId;
	}

	@Override
	public String toString() {
		return "Messages [messageId=" + messageId + ", text=" + text + ", senderId=" + senderId + ", receiverId="
				+ receiverId + ", isSeen=" + isSeen + ", timeStamp=" + timeStamp + ", timeZone=" + timeZone + ", date="
				+ date + ", epochSeconds=" + epochSeconds + ", aesKey=" + aesKey + ", initializationVector="
				+ initializationVector + ", type=" + type + ", attachments=" + attachments + ", sourceType="
				+ sourceType + ", sourceMessageId=" + sourceMessageId + ", groupId=" + groupId
				+ ", isSeenByGroupMembers=" + isSeenByGroupMembers + "]";
	}
}