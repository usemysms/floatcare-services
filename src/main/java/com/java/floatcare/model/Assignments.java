package com.java.floatcare.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "assignments")
public class Assignments {

	@Id
	private long assignmentId;
	private long businessId;
	private long clientId;
	private LocalDate startDate;
	private LocalDate endDate;
	private String duration;
	private List<Long> staffCoreTypes = new ArrayList<>();
	private String status; // published/draft/deleted - default: draft

	public long getAssignmentId() {
		return assignmentId;
	}

	public void setAssignmentId(long assignmentId) {
		this.assignmentId = assignmentId;
	}

	public long getBusinessId() {
		return businessId;
	}

	public void setBusinessId(long businessId) {
		this.businessId = businessId;
	}

	public long getClientId() {
		return clientId;
	}

	public void setClientId(long clientId) {
		this.clientId = clientId;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public List<Long> getStaffCoreTypes() {
		return staffCoreTypes;
	}

	public void setStaffCoreTypes(List<Long> staffCoreTypes) {
		this.staffCoreTypes = staffCoreTypes;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Assignments [assignmentId=" + assignmentId + ", businessId=" + businessId + ", clientId=" + clientId
				+ ", startDate=" + startDate + ", endDate=" + endDate + ", duration=" + duration + ", staffCoreTypes="
				+ staffCoreTypes + ", status=" + status + "]";
	}
}