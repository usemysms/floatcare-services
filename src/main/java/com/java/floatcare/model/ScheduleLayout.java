package com.java.floatcare.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/*
 * 

shiftPlanningStartDate Date // It will capture from UI 4th July, 2020--------------------------------------------UI
makeStaffAvailabilityDeadLineLength string // It will capture from UI 7d-----------------------------------------UI
makeStaffAvailabilityDeadLine Date // need to calculate in create mutation 10th July, 2020 --------------------> calc


scheduleStartDate Date // It will capture from UI 1st Aug, 2020--------------------------------------------------UI
scheduleLength string // It will capture from UI 2w--------------------------------------------------------------UI
scheduleEndDate Date // need to calculate in create mutation  14th Aug, 2020-----------------------------------> calc


shifPlanningPhaseLength string // It will capture from UI 2w-----------------------------------------------------UI
shiftPlanningStartDate Date // It will capture from UI 4th July, 2020--------------------------------------------UI
shiftPlanningEndDate Date  // need to calculate in create mutation 17th July, 2020-----------------------------> calc

 */

@Document(collection = "schedule_layout")
public class ScheduleLayout {

	@Id
	private long scheduleLayoutId;
	private long departmentId;
	private DateTime makeStaffAvailabilityDeadLine; // need to calculate in create mutation 10th July, 2020
	private DateTime shiftPlanningStartDate; // It will capture from UI 4th July, 2020
	private DateTime shiftPlanningEndDate; // need to calculate in create mutation 17th July, 2020
	private DateTime scheduleStartDate; // It will capture from UI 1st Aug, 2020
	private DateTime scheduleEndDate; // need to calculate in create mutation 14th Aug, 2020
	private String shiftPlanningPhaseLength; // It will capture from UI 2w
	private String makeStaffAvailabilityDeadLineLength; // It will capture from UI 7d
	private String scheduleLength; //It will capture from UI 2w
	private String maxQuota;
	private LocalDate publishedOn;
	private String status;
	private boolean autoAddProfessionalsToLayout;
	private List<Long> staffUserCoreTypeIds = new ArrayList<Long>();
	private List<Long> shiftTypes = new ArrayList<>();

	public long getScheduleLayoutId() {
		return scheduleLayoutId;
	}

	public void setScheduleLayoutId(long scheduleLayoutId) {
		this.scheduleLayoutId = scheduleLayoutId;
	}

	public long getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(long departmentId) {
		this.departmentId = departmentId;
	}

	public DateTime getMakeStaffAvailabilityDeadLine() {
		return makeStaffAvailabilityDeadLine;
	}

	public void setMakeStaffAvailabilityDeadLine(DateTime makeStaffAvailabilityDeadLine) {
		this.makeStaffAvailabilityDeadLine = makeStaffAvailabilityDeadLine;
	}

	public DateTime getShiftPlanningStartDate() {
		return shiftPlanningStartDate;
	}

	public void setShiftPlanningStartDate(DateTime shiftPlanningStartDate) {
		this.shiftPlanningStartDate = shiftPlanningStartDate;
	}

	public DateTime getShiftPlanningEndDate() {
		return shiftPlanningEndDate;
	}

	public void setShiftPlanningEndDate(DateTime shiftPlanningEndDate) {
		this.shiftPlanningEndDate = shiftPlanningEndDate;
	}

	public DateTime getScheduleStartDate() {
		return scheduleStartDate;
	}

	public void setScheduleStartDate(DateTime scheduleStartDate) {
		this.scheduleStartDate = scheduleStartDate;
	}

	public DateTime getScheduleEndDate() {
		return scheduleEndDate;
	}

	public void setScheduleEndDate(DateTime scheduleEndDate) {
		this.scheduleEndDate = scheduleEndDate;
	}

	public String getShiftPlanningPhaseLength() {
		return shiftPlanningPhaseLength;
	}

	public void setShiftPlanningPhaseLength(String shiftPlanningPhaseLength) {
		this.shiftPlanningPhaseLength = shiftPlanningPhaseLength;
	}

	public String getMakeStaffAvailabilityDeadLineLength() {
		return makeStaffAvailabilityDeadLineLength;
	}

	public void setMakeStaffAvailabilityDeadLineLength(String makeStaffAvailabilityDeadLineLength) {
		this.makeStaffAvailabilityDeadLineLength = makeStaffAvailabilityDeadLineLength;
	}

	public String getScheduleLength() {
		return scheduleLength;
	}

	public void setScheduleLength(String scheduleLength) {
		this.scheduleLength = scheduleLength;
	}

	public String getMaxQuota() {
		return maxQuota;
	}

	public void setMaxQuota(String maxQuota) {
		this.maxQuota = maxQuota;
	}
	
	public LocalDate getPublishedOn() {
		return publishedOn;
	}
	
	public void setPublishedOn(LocalDate publishedOn) {
		this.publishedOn = (publishedOn);	
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isAutoAddProfessionalsToLayout() {
		return autoAddProfessionalsToLayout;
	}

	public void setAutoAddProfessionalsToLayout(boolean autoAddProfessionalsToLayout) {
		this.autoAddProfessionalsToLayout = autoAddProfessionalsToLayout;
	}
	
	public List<Long> getStaffUserCoreTypeIds() {
		return staffUserCoreTypeIds;
	}
	
	public void setStaffUserCoreTypeIds(List<Long> staffCoreTypes) {
		staffUserCoreTypeIds.addAll(staffCoreTypes);
	}

	public List<Long> getShiftTypes() {
		return shiftTypes;
	}

	public void setShiftTypes(List<Long> shiftTypesIds) {
		this.shiftTypes.addAll(shiftTypesIds);
	}

	@Override
	public String toString() {
		return "ScheduleLayout [scheduleLayoutId=" + scheduleLayoutId + ", departmentId=" + departmentId
				+ ", makeStaffAvailabilityDeadLine=" + makeStaffAvailabilityDeadLine + ", shiftPlanningStartDate="
				+ shiftPlanningStartDate + ", shiftPlanningEndDate=" + shiftPlanningEndDate + ", scheduleStartDate="
				+ scheduleStartDate + ", scheduleEndDate=" + scheduleEndDate + ", shiftPlanningPhaseLength="
				+ shiftPlanningPhaseLength + ", makeStaffAvailabilityDeadLineLength="
				+ makeStaffAvailabilityDeadLineLength + ", scheduleLength=" + scheduleLength + ", maxQuota=" + maxQuota
				+ ", publishedOn=" + publishedOn + "]";
	}
}