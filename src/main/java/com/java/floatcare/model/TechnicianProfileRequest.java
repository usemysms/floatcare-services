package com.java.floatcare.model;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "technician_profile_request")
public class TechnicianProfileRequest {

	private String title;
	private String firstName;
	private String lastName;
	private String email;
	private String phoneNumber;
	private String dateOfBirth;
	private String workableRegions;
	private String workStartDate;
	private String workAssignmentLength;
	private String nursingDegree;
	private String stateNursingLicenseNumber;
	private String stateNursingLicenseState;
	private String stateNursingLicenseRestrictions;
	private String nursingLicenseStatus;
	private String credentialName;
	private String medicalWorkExperienceBusinessName;
	private String medicalWorkExperienceCity;
	private String workExperienceState;
	private String workExperienceZipcode;
	private String workExperienceDepartments;
	private String workExperiencePositionTitle;
	private String workExperienceDescriptionofJobDuties;
	private String workExperiencePositionLinkedInProfileURL;

	public TechnicianProfileRequest() {

	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getWorkableRegions() {
		return workableRegions;
	}

	public void setWorkableRegions(String workableRegions) {
		this.workableRegions = workableRegions;
	}

	public String getWorkStartDate() {
		return workStartDate;
	}

	public void setWorkStartDate(String workStartDate) {
		this.workStartDate = workStartDate;
	}

	public String getWorkAssignmentLength() {
		return workAssignmentLength;
	}

	public void setWorkAssignmentLength(String workAssignmentLength) {
		this.workAssignmentLength = workAssignmentLength;
	}

	public String getNursingDegree() {
		return nursingDegree;
	}

	public void setNursingDegree(String nursingDegree) {
		this.nursingDegree = nursingDegree;
	}

	public String getStateNursingLicenseNumber() {
		return stateNursingLicenseNumber;
	}

	public void setStateNursingLicenseNumber(String stateNursingLicenseNumber) {
		this.stateNursingLicenseNumber = stateNursingLicenseNumber;
	}

	public String getStateNursingLicenseState() {
		return stateNursingLicenseState;
	}

	public void setStateNursingLicenseState(String stateNursingLicenseState) {
		this.stateNursingLicenseState = stateNursingLicenseState;
	}

	public String getStateNursingLicenseRestrictions() {
		return stateNursingLicenseRestrictions;
	}

	public void setStateNursingLicenseRestrictions(String stateNursingLicenseRestrictions) {
		this.stateNursingLicenseRestrictions = stateNursingLicenseRestrictions;
	}

	public String getNursingLicenseStatus() {
		return nursingLicenseStatus;
	}

	public void setNursingLicenseStatus(String nursingLicenseStatus) {
		this.nursingLicenseStatus = nursingLicenseStatus;
	}

	public String getCredentialName() {
		return credentialName;
	}

	public void setCredentialName(String credentialName) {
		this.credentialName = credentialName;
	}

	public String getMedicalWorkExperienceBusinessName() {
		return medicalWorkExperienceBusinessName;
	}

	public void setMedicalWorkExperienceBusinessName(String medicalWorkExperienceBusinessName) {
		this.medicalWorkExperienceBusinessName = medicalWorkExperienceBusinessName;
	}

	public String getMedicalWorkExperienceCity() {
		return medicalWorkExperienceCity;
	}

	public void setMedicalWorkExperienceCity(String medicalWorkExperienceCity) {
		this.medicalWorkExperienceCity = medicalWorkExperienceCity;
	}

	public String getWorkExperienceState() {
		return workExperienceState;
	}

	public void setWorkExperienceState(String workExperienceState) {
		this.workExperienceState = workExperienceState;
	}

	public String getWorkExperienceZipcode() {
		return workExperienceZipcode;
	}

	public void setWorkExperienceZipcode(String workExperienceZipcode) {
		this.workExperienceZipcode = workExperienceZipcode;
	}

	public String getWorkExperienceDepartments() {
		return workExperienceDepartments;
	}

	public void setWorkExperienceDepartments(String workExperienceDepartments) {
		this.workExperienceDepartments = workExperienceDepartments;
	}

	public String getWorkExperiencePositionTitle() {
		return workExperiencePositionTitle;
	}

	public void setWorkExperiencePositionTitle(String workExperiencePositionTitle) {
		this.workExperiencePositionTitle = workExperiencePositionTitle;
	}

	public String getWorkExperienceDescriptionofJobDuties() {
		return workExperienceDescriptionofJobDuties;
	}

	public void setWorkExperienceDescriptionofJobDuties(String workExperienceDescriptionofJobDuties) {
		this.workExperienceDescriptionofJobDuties = workExperienceDescriptionofJobDuties;
	}

	public String getWorkExperiencePositionLinkedInProfileURL() {
		return workExperiencePositionLinkedInProfileURL;
	}

	public void setWorkExperiencePositionLinkedInProfileURL(String workExperiencePositionLinkedInProfileURL) {
		this.workExperiencePositionLinkedInProfileURL = workExperiencePositionLinkedInProfileURL;
	}

	@Override
	public String toString() {
		return "TechnicianProfileRequest [title=" + title + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", email=" + email + ", phoneNumber=" + phoneNumber + ", dateOfBirth=" + dateOfBirth
				+ ", workableRegions=" + workableRegions + ", workStartDate=" + workStartDate
				+ ", workAssignmentLength=" + workAssignmentLength + ", nursingDegree=" + nursingDegree
				+ ", stateNursingLicenseNumber=" + stateNursingLicenseNumber + ", stateNursingLicenseState="
				+ stateNursingLicenseState + ", stateNursingLicenseRestrictions=" + stateNursingLicenseRestrictions
				+ ", nursingLicenseStatus=" + nursingLicenseStatus + ", credentialName=" + credentialName
				+ ", medicalWorkExperienceBusinessName=" + medicalWorkExperienceBusinessName
				+ ", medicalWorkExperienceCity=" + medicalWorkExperienceCity + ", workExperienceState="
				+ workExperienceState + ", workExperienceZipcode=" + workExperienceZipcode
				+ ", workExperienceDepartments=" + workExperienceDepartments + ", workExperiencePositionTitle="
				+ workExperiencePositionTitle + ", workExperienceDescriptionofJobDuties="
				+ workExperienceDescriptionofJobDuties + ", workExperiencePositionLinkedInProfileURL="
				+ workExperiencePositionLinkedInProfileURL + "]";
	}

}
