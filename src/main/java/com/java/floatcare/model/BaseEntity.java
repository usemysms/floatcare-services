package com.java.floatcare.model;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@SuppressWarnings("serial")
public abstract class BaseEntity implements Serializable {
    @Id
    private String id;
}