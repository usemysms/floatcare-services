package com.java.floatcare.model;

import java.time.LocalDate;
import java.time.LocalTime;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "schedules")
public class Schedules {

	@Id
	private long scheduleId;
	private long worksiteId;
	private long departmentId;
	private long userId; 
	private long shiftTypeId; // shiftTypeId (day shift | night shift | swing shift)
	private String eventType;
	private LocalDate shiftDate;
	private LocalTime startTime;
	private LocalTime endTime; 
	private boolean isOnCallRequest;
	private String status;
	private boolean voluntaryLowCensus;
	private long selfScheduleId;

	public long getScheduleId() {
		return scheduleId;
	}

	public void setScheduleId(long scheduleId) {
		this.scheduleId = scheduleId;
	}

	public long getWorksiteId() {
		return worksiteId;
	}

	public void setWorksiteId(long worksiteId) {
		this.worksiteId = worksiteId;
	}

	public long getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(long departmentId) {
		this.departmentId = departmentId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public LocalDate getShiftDate() {
		return shiftDate;
	}

	public void setShiftDate(LocalDate shiftDate) {
		this.shiftDate = shiftDate;
	}

	public LocalTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalTime startTime) {
		this.startTime = startTime;
	}

	public LocalTime getEndTime() {
		return endTime;
	}

	public void setEndTime(LocalTime endTime) {
		this.endTime = endTime;
	}
	
	public void setShiftTypeId(long shiftTypeId) {
		this.shiftTypeId = shiftTypeId;
	}
	
	public long getShiftTypeId() {
		return shiftTypeId;
	}

	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public boolean isOnCallRequest() {
		return isOnCallRequest;
	}

	public void setOnCallRequest(boolean isOnCallRequest) {
		this.isOnCallRequest = isOnCallRequest;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isVoluntaryLowCensus() {
		return voluntaryLowCensus;
	}

	public void setVoluntaryLowCensus(boolean voluntaryLowCensus) {
		this.voluntaryLowCensus = voluntaryLowCensus;
	}

	public long getSelfScheduleId() {
		return selfScheduleId;
	}

	public void setSelfScheduleId(long selfScheduleId) {
		this.selfScheduleId = selfScheduleId;
	}

	@Override
	public String toString() {
		return "Schedules [scheduleId=" + scheduleId + ", worksiteId=" + worksiteId + ", departmentId=" + departmentId
				+ ", userId=" + userId + ", shiftTypeId=" + shiftTypeId + ", shiftDate=" + shiftDate + ", startTime="
				+ startTime + ", endTime=" + endTime + ", isOnCallRequest=" + isOnCallRequest + ", status=" + status
				+ ", voluntaryLowCensus=" + voluntaryLowCensus + "]";
	}
}
