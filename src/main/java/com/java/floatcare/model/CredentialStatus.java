package com.java.floatcare.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/*
 * 
 * Credential State will be captured as a part of active, missing, rejected, pending review 	 
 * 
 */

@Document(collection = "credential_status")
public class CredentialStatus {

	@Id
	private long credentialStatusId;
	private String label;

	public CredentialStatus() {

	}

	public CredentialStatus(long credentialStatusId, String label) {
		super();
		this.credentialStatusId = credentialStatusId;
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public long getCredentialStatusId() {
		return credentialStatusId;
	}

	public void setCredentialStatusId(long credentialStatusId) {
		this.credentialStatusId = credentialStatusId;
	}

	@Override
	public String toString() {
		return String.format("CredentialStatus [ label=" + label + "]");
	}

}
