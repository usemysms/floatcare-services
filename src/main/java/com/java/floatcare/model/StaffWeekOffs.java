package com.java.floatcare.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.java.floatcare.api.response.pojo.StaffSetSchedules;


@Document(collection = "staff_week_offs")
public class StaffWeekOffs {

	@Id
	private long staffWeekOffId;
	private long worksiteId;
	private long departmentId;
	private long setScheduleLayoutId;
	private long userId;
	private List<StaffSetSchedules> staffSetSchedules = new ArrayList<StaffSetSchedules>();
	
	private List<String> setScheduleWeekOffDays = new ArrayList<>();

	public long getStaffWeekOffId() {
		return staffWeekOffId;
	}

	public void setStaffWeekOffId(long staffWeekOffId) {
		this.staffWeekOffId = staffWeekOffId;
	}

	public long getWorksiteId() {
		return worksiteId;
	}

	public void setWorksiteId(long worksiteId) {
		this.worksiteId = worksiteId;
	}

	public long getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(long departmentId) {
		this.departmentId = departmentId;
	}

	public long getSetScheduleLayoutId() {
		return setScheduleLayoutId;
	}

	public void setSetScheduleLayoutId(long setScheduleLayoutId) {
		this.setScheduleLayoutId = setScheduleLayoutId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	public List<String> getSetScheduleWeekOffDays() {
		return setScheduleWeekOffDays;
	}

	public void setSetScheduleWeekOffDays(List<String> setScheduleWeekOffDays) {
		this.setScheduleWeekOffDays = setScheduleWeekOffDays;
	}

	public List<StaffSetSchedules> getStaffSetSchedules() {
		return staffSetSchedules;
	}

	public void setStaffSetSchedules(List<StaffSetSchedules> staffSetSchedules) {
		this.staffSetSchedules = staffSetSchedules;
	}

	@Override
	public String toString() {
		return "StaffWeekOffs [staffWeekOffId=" + staffWeekOffId + ", worksiteId=" + worksiteId + ", departmentId="
				+ departmentId + ", setScheduleLayoutId=" + setScheduleLayoutId + ", userId=" + userId
				+ ", staffSetSchedules=" + staffSetSchedules + "]";
	}
}

