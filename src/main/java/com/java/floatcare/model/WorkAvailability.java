package com.java.floatcare.model;

import java.time.LocalDate;

import org.springframework.data.annotation.Id;

public class WorkAvailability {

	@Id
	private long workAvailabilityId;
	private String region;
	private LocalDate startWorkDate;
	private long workForId;
	private long userId;

	public WorkAvailability() {

	}

	public WorkAvailability(String region, LocalDate startWorkDate) {
		super();
		this.region = region;
		this.startWorkDate = startWorkDate;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public LocalDate getStartWorkDate() {
		return startWorkDate;
	}

	public void setStartWorkDate(LocalDate startWorkDate) {
		this.startWorkDate = startWorkDate;
	}

	public long getWorkAvailabilityId() {
		return workAvailabilityId;
	}

	public void setWorkAvailabilityId(long workAvailabilityId) {
		this.workAvailabilityId = workAvailabilityId;
	}

	public long getWorkForId() {
		return workForId;
	}

	public void setWorkForId(long workForId) {
		this.workForId = workForId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "WorkAvailability [workAvailabilityId=" + workAvailabilityId + ", region=" + region + ", startWorkDate="
				+ startWorkDate + ", workForId=" + workForId + ", userId=" + userId + "]";
	}
}
