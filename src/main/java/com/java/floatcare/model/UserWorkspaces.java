package com.java.floatcare.model;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.java.floatcare.api.request.pojo.UserAvailability;

@Document(collection = "user_workspaces")
public class UserWorkspaces {
	
	@Id
	private long userWorkspaceId;
	private long userId;
	private String workspaceType;// department || business || organization
	private long workspaceId;// departmentId || businessId || organizationId
	private String status;
	private String fteStatus; // Full Time, Part Time, Per Diem
	private String workspaceName; // setter and getter for assigning workspace name
	private boolean isChargeRole;
	private boolean isPreceptorRole;
	private long businessId;
	private Businesses business; // setter and getter for assigning business name 
	private long organizationId;
	private String invitationCode;
	private boolean isJoined;
	private LocalDate joinedDate;
	private boolean isSentEmail;
	private boolean isSentSms;
	private String hours;
	private LocalDate hiredOn;
	private List<Long> departmentShiftIds;
	private long staffUserCoreTypeId;
	private boolean isPrimaryDepartment;
	private long staffUserSubCoreTypeId;
    private List<UserAvailability> userAvailability;

	public long getUserWorkspaceId() {
		return userWorkspaceId;
	}

	public void setUserWorkspaceId(long userWorkspaceId) {
		this.userWorkspaceId = userWorkspaceId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getWorkspaceType() {
		return workspaceType;
	}

	public void setWorkspaceType(String workspaceType) {
		this.workspaceType = workspaceType;
	}

	public long getWorkspaceId() {
		return workspaceId;
	}

	public void setWorkspaceId(long workspaceId) {
		this.workspaceId = workspaceId;
	}

	public String getFteStatus() {
		return fteStatus;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setFteStatus(String fteStatus) {
		this.fteStatus = fteStatus;
	}

	public String getWorkspaceName() {
		return workspaceName;
	}

	public void setWorkspaceName(String workspaceName) {
		this.workspaceName = workspaceName;
	}

	public boolean getIsChargeRole() {
		return isChargeRole;
	}

	public void setIsChargeRole(boolean isChargeRole) {
		this.isChargeRole = isChargeRole;
	}

	public boolean getIsPreceptorRole() {
		return isPreceptorRole;
	}

	public void setIsPreceptorRole(boolean isPreceptorRole) {
		this.isPreceptorRole = isPreceptorRole;
	}

	public long getBusinessId() {
		return businessId;
	}

	public void setBusinessId(long businessId) {
		this.businessId = businessId;
	}

	public long getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(long organizationId) {
		this.organizationId = organizationId;
	}

	public String getInvitationCode() {
		return invitationCode;
	}

	public void setInvitationCode(String invitationCode) {
		this.invitationCode = invitationCode;
	}

	public boolean isJoined() {
		return isJoined;
	}

	public void setJoined(boolean isJoined) {
		this.isJoined = isJoined;
	}

	public LocalDate getJoinedDate() {
		return joinedDate;
	}

	public void setJoinedDate(LocalDate joinedDate) {
		this.joinedDate = joinedDate;
	}

	public boolean isSentEmail() {
		return isSentEmail;
	}

	public void setSentEmail(boolean isSentEmail) {
		this.isSentEmail = isSentEmail;
	}

	public boolean isSentSms() {
		return isSentSms;
	}

	public void setSentSms(boolean isSentSms) {
		this.isSentSms = isSentSms;
	}

	public void setChargeRole(boolean isChargeRole) {
		this.isChargeRole = isChargeRole;
	}

	public void setPreceptorRole(boolean isPreceptorRole) {
		this.isPreceptorRole = isPreceptorRole;
	}

	public String getHours() {
		return hours;
	}

	public void setHours(String hours) {
		this.hours = hours;
	}

	public LocalDate getHiredOn() {
		return hiredOn;
	}

	public void setHiredOn(LocalDate hiredOn) {
		this.hiredOn = hiredOn;
	}

	public List<Long> getDepartmentShiftIds() {
		return departmentShiftIds;
	}

	public void setDepartmentShiftIds(List<Long> departmentShiftIds) {
		this.departmentShiftIds = departmentShiftIds;
	}
	
	public long getStaffUserCoreTypeId() {
		return staffUserCoreTypeId;
	}

	public void setStaffUserCoreTypeId(long staffUserCoreTypeId) {
		this.staffUserCoreTypeId = staffUserCoreTypeId;
	}

	public boolean isPrimaryDepartment() {
		return isPrimaryDepartment;
	}

	public void setPrimaryDepartment(boolean isPrimaryDepartment) {
		this.isPrimaryDepartment = isPrimaryDepartment;
	}

	public long getStaffUserSubCoreTypeId() {
		return staffUserSubCoreTypeId;
	}

	public void setStaffUserSubCoreTypeId(long staffUserSubCoreTypeId) {
		this.staffUserSubCoreTypeId = staffUserSubCoreTypeId;
	}

    public List<UserAvailability> getUserAvailability() {
        return userAvailability;
    }

    public void setUserAvailability(List<UserAvailability> userAvailability) {
        this.userAvailability = userAvailability;
    }
    

	@Override
	public String toString() {
		return "UserWorkspaces [userWorkspaceId=" + userWorkspaceId + ", userId=" + userId + ", workspaceType="
				+ workspaceType + ", workspaceId=" + workspaceId + ", status=" + status + ", fteStatus=" + fteStatus
				+ ", workspaceName=" + workspaceName + ", isChargeRole=" + isChargeRole + ", isPreceptorRole="
				+ isPreceptorRole + ", businessId=" + businessId + ", organizationId=" + organizationId
				+ ", invitationCode=" + invitationCode + ", isJoined=" + isJoined + ", joinedDate=" + joinedDate
				+ ", isSentEmail=" + isSentEmail + ", isSentSms=" + isSentSms + ", hours=" + hours + ", hiredOn="
				+ hiredOn + ", departmentShiftId=" + departmentShiftIds + ", staffUserCoreTypeId=" + staffUserCoreTypeId
				+ ", isPrimaryDepartment=" + isPrimaryDepartment + ", staffUserSubCoreTypeId=" + staffUserSubCoreTypeId
				+ ", userAvailability=" + userAvailability + "]";
	}

	public Businesses getBusiness() {
		return business;
	}

	public void setBusiness(Businesses business) {
		this.business = business;
	}

}