package com.java.floatcare.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/***
 * 
 * Certification type will be captured as a part of Credential Type
 * 
 * Data: Advanced Cardiac Life Support, Basic Life Support, Pediatric Advanced
 * Life Support, Certified Sedation Registered Nurse
 * 
 *
 */

@Document(collection = "certification_type")
public class CertificationType {

	@Id
	private long certificationTypeId;
	private String label;

	public CertificationType() {

	}

	public CertificationType(long certificationTypeId,String label) {
		super();
	
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public long getCertificationTypeId() {
		return certificationTypeId;
	}

	public void setCertificationTypeId(long certificationTypeId) {
		this.certificationTypeId = certificationTypeId;
	}

	@Override
	public String toString() {
		return String
				.format("CertificationType [ label=" + label + "]");
	}

}
