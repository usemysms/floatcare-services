package com.java.floatcare.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "flex_off_invitees")
public class FlexOffInvitees {

	@Id
	private long flexOffInviteeId;
	private long flexOffId;
	private long userId;
	private boolean isApproved;
	private boolean isAccepted;

	public long getFlexOffInviteeId() {
		return flexOffInviteeId;
	}

	public void setFlexOffInviteeId(long flexOffInviteeId) {
		this.flexOffInviteeId = flexOffInviteeId;
	}

	public long getFlexOffId() {
		return flexOffId;
	}

	public void setFlexOffId(long flexOffId) {
		this.flexOffId = flexOffId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public boolean isApproved() {
		return isApproved;
	}

	public void setApproved(boolean isApproved) {
		this.isApproved = isApproved;
	}

	public boolean isAccepted() {
		return isAccepted;
	}

	public void setAccepted(boolean isAccepted) {
		this.isAccepted = isAccepted;
	}

	@Override
	public String toString() {
		return "FlexOffInvitees [flexOffInviteeId=" + flexOffInviteeId + ", flexOffId=" + flexOffId + ", userId="
				+ userId + ", isApproved=" + isApproved + ", isAccepted=" + isAccepted + "]";
	}
}
