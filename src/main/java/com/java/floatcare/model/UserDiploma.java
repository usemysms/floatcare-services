package com.java.floatcare.model;

import java.time.LocalDate;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "user_diploma")
public class UserDiploma {

	@Id
	private long userDiplomaId;
	private long userId;
	private LocalDate issuedDate;
	private LocalDate expirationDate;
	private String documentNumber;
	private String frontFilePhoto;
	private String fileName;
	private String backFilePhoto;
	private String state;

	public long getUserDiplomaId() {
		return userDiplomaId;
	}

	public void setUserDiplomaId(long userDiplomaId) {
		this.userDiplomaId = userDiplomaId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public LocalDate getIssuedDate() {
		return issuedDate;
	}

	public void setIssuedDate(LocalDate issuedDate) {
		this.issuedDate = issuedDate;
	}

	public LocalDate getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(LocalDate expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public String getFrontFilePhoto() {
		return frontFilePhoto;
	}

	public void setFrontFilePhoto(String frontFilePhoto) {
		this.frontFilePhoto = frontFilePhoto;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getBackFilePhoto() {
		return backFilePhoto;
	}

	public void setBackFilePhoto(String backFilePhoto) {
		this.backFilePhoto = backFilePhoto;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "User_Diploma [userDeplomaId=" + userDiplomaId + ", userId=" + userId + ", issuedDate=" + issuedDate
				+ ", expirationDate=" + expirationDate + ", documentNumber=" + documentNumber + ", frontFilePhoto="
				+ frontFilePhoto + ", fileName=" + fileName + ", backFilePhoto=" + backFilePhoto + ", state=" + state
				+ "]";
	}
}