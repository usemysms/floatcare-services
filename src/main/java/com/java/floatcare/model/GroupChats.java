package com.java.floatcare.model;

import java.util.TreeSet;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "group_chats")
public class GroupChats {

	@Id
	private long chatGroupId;
	private long organizationId;
	private String name;
	private TreeSet<Long> members;
	private TreeSet<Long> admins = new TreeSet<>();
	private String createdOn;

	public long getChatGroupId() {
		return chatGroupId;
	}

	public void setChatGroupId(long chatGroupId) {
		this.chatGroupId = chatGroupId;
	}

	public long getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(long organizationId) {
		this.organizationId = organizationId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public TreeSet<Long> getMembers() {
		return members;
	}

	public void setMembers(TreeSet<Long> member) {
		this.members = member;
	}

	public TreeSet<Long> getAdmins() {
		return admins;
	}

	public void setAdmins(TreeSet<Long> adminIds) {
		this.admins = adminIds;
	}

	public void setAdmin(long adminId) {
		this.admins.add(adminId);
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	@Override
	public String toString() {
		return "GroupChat [chatGroupId=" + chatGroupId + ", organizationId=" + organizationId + ", name=" + name
				+ ", members=" + members + ", admins=" + admins + ", createdOn=" + createdOn + "]";
	}
}
