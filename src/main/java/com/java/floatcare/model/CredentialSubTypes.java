package com.java.floatcare.model;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.java.floatcare.api.response.pojo.RequiredType;

@Document(collection = "credential_sub_types")
public class CredentialSubTypes {

	@Id
	private long credentialSubTypeId;
	private long credentialTypeId;
	private String label;
	private List<String> states;
	private long organizationId;
	private List<RequiredType> requiredType;
	private String credentialType;
	private List<Long> removedFromOrganizations;
	private String status;
	private long creatorId;
	private String acronym;

	public long getCredentialSubTypeId() {
		return credentialSubTypeId;
	}

	public void setCredentialSubTypeId(long credentialSubTypeId) {
		this.credentialSubTypeId = credentialSubTypeId;
	}

	public long getCredentialTypeId() {
		return credentialTypeId;
	}

	public void setCredentialTypeId(long credentialTypeId) {
		this.credentialTypeId = credentialTypeId;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	
	public List<String> getStates() {
		return states;
	}

	public void setStates(List<String> states) {
		this.states = states;
	}

	public long getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(long organizationId) {
		this.organizationId = organizationId;
	}
	

	public List<RequiredType> getRequiredType() {
		return requiredType;
	}

	public void setRequiredType(List<RequiredType> requiredTypeList) {
		this.requiredType = requiredTypeList;
	}

	public String getCredentialType() {
		return credentialType;
	}

	public void setCredentialType(String credentialType) {
		this.credentialType = credentialType;
	}

	public List<Long> getRemovedFromOrganizations() {
		return removedFromOrganizations;
	}

	public void setRemovedFromOrganizations(List<Long> removedFromOrganizations) {
		this.removedFromOrganizations = removedFromOrganizations;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public long getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(long creatorId) {
		this.creatorId = creatorId;
	}
	
	public String getAcronym() {
		return acronym;
	}

	public void setAcronym(String acronym) {
		this.acronym = acronym;
	}

	@Override
	public String toString() {
		return "CredentialSubTypes [credentialSubTypeId=" + credentialSubTypeId + ", credentialTypeId="
				+ credentialTypeId + ", label=" + label + ", states=" + states + ", organizationId=" + organizationId
				+ ", requiredType=" + requiredType + ", credentialType=" + credentialType
				+ ", removedFromOrganizations=" + removedFromOrganizations + ", status=" + status + ", creatorId="
				+ creatorId + ", acronym=" + acronym + "]";
	}
	
	
}
