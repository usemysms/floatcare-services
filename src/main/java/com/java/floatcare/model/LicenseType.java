package com.java.floatcare.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/*
 * 
 * License Type will be captured as a part of Credential Type
 * 
 */

@Document(collection = "license_type")
public class LicenseType {

	@Id 
	private long licenseTypeId;
	private String label;

	public LicenseType() {

	}

	public LicenseType(long licenseType, String label) {
		super();
		this.licenseTypeId = licenseType;
		this.label = label;
	}


	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public long getLicenseType() {
		return licenseTypeId;
	}

	public void setLicenseType(long licenseType) {
		this.licenseTypeId = licenseType;
	}

	@Override
	public String toString() {
		return String.format("label=" + label + "]");
	}

}
