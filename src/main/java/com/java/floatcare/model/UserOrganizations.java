package com.java.floatcare.model;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.java.floatcare.api.request.HealthInsurances;

/*
 * 
 * Chief Nursing Officer (CNO)
 * Director Of Nursing
 * Nurse Manager or Nurse Supervisor	
 * 
 */
@Document(collection = "user_organizations")
public class UserOrganizations {

	@Id
	private long userOrganizationId;
	private long userId;
	private long organizationId;
	private long staffUserCoreTypeId;
	private String status;
	private List<HealthInsurances> healthInsurances;

	public long getUserOrganizationId() {
		return userOrganizationId;
	}

	public void setUserOrganizationId(long userOrganizationId) {
		this.userOrganizationId = userOrganizationId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(long organizationId) {
		this.organizationId = organizationId;
	}

	public long getStaffUserCoreTypeId() {
		return staffUserCoreTypeId;
	}

	public void setStaffUserCoreTypeId(long staffUserCoreTypeId) {
		this.staffUserCoreTypeId = staffUserCoreTypeId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<HealthInsurances> getHealthInsurances() {
		return healthInsurances;
	}

	public void setHealthInsurances(List<HealthInsurances> healthInsurances) {
		this.healthInsurances = healthInsurances;
	}

	@Override
	public String toString() {
		return "UserOrganizations [userOrganizationId=" + userOrganizationId + ", userId=" + userId
				+ ", organizationId=" + organizationId + ", status=" + status + "]";
	}
}