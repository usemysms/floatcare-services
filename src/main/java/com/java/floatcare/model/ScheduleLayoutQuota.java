package com.java.floatcare.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection = "schedule_layout_quota")
public class ScheduleLayoutQuota {

	@Id
	private long scheduleLayoutQuotaId;
	private long scheduleLayoutId;
	private long staffCoreTypeId;
	private long quota;
	private long shiftTypeId;
	private long filledCount;
	
	public long getScheduleLayoutQuotaId() {
		return scheduleLayoutQuotaId;
	}

	public void setScheduleLayoutQuotaId(long scheduleLayoutQuotaId) {
		this.scheduleLayoutQuotaId = scheduleLayoutQuotaId;
	}

	public long getScheduleLayoutId() {
		return scheduleLayoutId;
	}

	public void setScheduleLayoutId(long scheduleLayoutId) {
		this.scheduleLayoutId = scheduleLayoutId;
	}

	public long getStaffCoreTypeId() {
		return staffCoreTypeId;
	}

	public void setStaffCoreTypeId(long staffCoreTypeId) {
		this.staffCoreTypeId = staffCoreTypeId;
	}

	public long getQuota() {
		return quota;
	}

	public void setQuota(long quota) {
		this.quota = quota;
	}

	public long getShiftTypeId() {
		return shiftTypeId;
	}

	public void setShiftTypeId(long shiftTypeId) {
		this.shiftTypeId = shiftTypeId;
	}

	public long getFilledCount() {
		return filledCount;
	}

	public void setFilledCount(long filledCount) {
		this.filledCount = filledCount;
	}

	@Override
	public String toString() {
		return "ScheduleLayoutQuota [staffCoreTypeId=" + staffCoreTypeId + ", quota=" + quota + ", shiftTypeId="
				+ shiftTypeId + "]";
	}
}
