package com.java.floatcare.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "schedule_layout_groups")
public class ScheduleLayoutGroups {

	@Id
	private long scheduleLayoutGroupId;
	private long departmentId;
	private long scheduleLayoutId;
	private String name;
	private List<Long> userIds = new ArrayList<Long>();

	public long getScheduleLayoutGroupId() {
		return scheduleLayoutGroupId;
	}

	public void setScheduleLayoutGroupId(long scheduleLayoutGroupId) {
		this.scheduleLayoutGroupId = scheduleLayoutGroupId;
	}

	public long getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(long departmentId) {
		this.departmentId = departmentId;
	}

	public long getScheduleLayoutId() {
		return scheduleLayoutId;
	}

	public void setScheduleLayoutId(long scheduleLayoutId) {
		this.scheduleLayoutId = scheduleLayoutId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Long> getUserIds() {
		return userIds;
	}

	public void setUserIds(List<Long> userId) {
		userIds.addAll(userId);
	}

	@Override
	public String toString() {
		return "ScheduleLayoutGroups [scheduleLayoutGroupId=" + scheduleLayoutGroupId + ", departmentId=" + departmentId
				+ ", name=" + name + ", userIds=" + userIds + "]";
	}

}
