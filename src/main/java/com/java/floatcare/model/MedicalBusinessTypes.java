package com.java.floatcare.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/*
 * 
 * Medical Business Types will be capturing data such as Facility Care, Home Care, Medical Supplies, Medical Delivery
 * 
 */

@Document(collection = "medical_business_types")
public class MedicalBusinessTypes {

	@Id
	private long medicalBusinessTypesId;
	private String label;

	public MedicalBusinessTypes() {

	}

	public MedicalBusinessTypes(long medicalBusinessTypesId, String label) {
		super();
		this.medicalBusinessTypesId = medicalBusinessTypesId;
		this.label = label;
	}


	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public long getMedicalBusinessTypesId() {
		return medicalBusinessTypesId;
	}

	public void setMedicalBusinessTypesId(long medicalBusinessTypesId) {
		this.medicalBusinessTypesId = medicalBusinessTypesId;
	}

	@Override
	public String toString() {
		return String.format("[" +label+"=" + label + "]");
	}

}
