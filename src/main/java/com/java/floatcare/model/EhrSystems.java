package com.java.floatcare.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "ehr_systems")
public class EhrSystems {

	@Id
	private long ehrSystemId;
	private String label;

	public long getEhrSystemId() {
		return ehrSystemId;
	}

	public void setEhrSystemId(long ehrSystemId) {
		this.ehrSystemId = ehrSystemId;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	@Override
	public String toString() {
		return "EhrSystems [ehrSystemId=" + ehrSystemId + ", label=" + label + "]";
	}
}
