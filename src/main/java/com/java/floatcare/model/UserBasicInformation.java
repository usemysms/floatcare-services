package com.java.floatcare.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import com.java.floatcare.api.response.pojo.SocialSecurityNumber;
import com.java.floatcare.api.response.pojo.UserLanguages;
import com.java.floatcare.api.response.pojo.UserNationalProviderIdentity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/***
 * 
 * User Details/User Basic Information will be captured as a part of first name,
 * last name, middle name(optional), preferred name, address_1(option based on
 * user type), address_2(option based on user type), city (option based on user
 * type), state (option based on user type), Zip (option based on user type),
 * phone, profilePhoto(optional)
 *
 */

@Document(collection = "user_basic_information")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class UserBasicInformation {

	@Id
	private long userId;
	private long organizationId;
	private String firstName;
	private String lastName;
	private String middleName;
	private String email;
	private String password;
	private String defaultPassword;
	private String preferredName;
	private String address1;
	private String address2;
	private String city;
	private String state;
	private String postalCode;
	private String country;
	private String phone;
	private String longitude;
	private String latitude;
	private String primaryContactCountryCode;
	private String profilePhoto;
	private String title;
	private long userTypeId;
	private LocalDate dateOfBirth;
	private long staffUserCoreTypeId;
	private long staffUserSubCoreTypeId;
	private String staffUserCoreTypeName;
	private String linkedInProfile;
	private String gender;
	private String bioInformation;
	private LocalDate createdOn;
	private String secondaryPhoneNumber;
	private String alternativeContactCountryCode;
	private String secondaryEmail;
	private String status;
	private String fteStatus;
	private long assignedSlots;
	private long totalSlots;
	private String fcmToken;
	private boolean muteAllNotifications;
	private String deviceType;
	private List<UserLanguages> userLanguages = new ArrayList<UserLanguages>();
	private UserNationalProviderIdentity userNationalProviderIdentity;
	private SocialSecurityNumber socialSecurityNumber;
	@Transient
	private List<UserWorkspaces> userWorkspaces; // this is only for setter and getter not db field
	@Transient
	private UserPermissions userPermissions;
	private String cvFile;
	private boolean firstLogin;
	private String publicKey;
	private List<EhrSystems> ehrSystems;
	private Integer statusChangedBy;
	private String ethnicType;
	private List<String> specialities;
	
	private String invitedSource;
	private long invitedFirmId;
	private long invitedBy;
	private String invitationStatus;

	public UserBasicInformation(long userId, String firstName, String lastName, String email, String password, long userTypeId, String city, String state, String address1) {
		
		this.userId = userId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
		this.userTypeId = userTypeId;
		this.status = "Active";
		this.city = city;
		this.state = state;
		this.address1 = address1;

	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserTypeId(long userTypeId) {
		this.userTypeId = userTypeId;
	}

	public long getStaffUserCoreTypeId() {
		return staffUserCoreTypeId;
	}

	public void setStaffUserCoreTypeId(long staffUserCoreTypeId) {
		this.staffUserCoreTypeId = staffUserCoreTypeId;
	}

	public String getStaffUserCoreTypeName() {
		return staffUserCoreTypeName;
	}

	public void setStaffUserCoreTypeName(String staffUserCoreTypeName) {
		this.staffUserCoreTypeName = staffUserCoreTypeName;
	}

	public long getUserTypeId() {
		return userTypeId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getPreferredName() {
		return preferredName;
	}

	public void setPreferredName(String preferredName) {
		this.preferredName = preferredName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDefaultPassword() {
		return defaultPassword;
	}

	public void setDefaultPassword(String defaultPassword) {
		this.defaultPassword = defaultPassword;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phoneNumber) {
		this.phone = phoneNumber;
	}

	public String getProfilePhoto() {
		return profilePhoto;
	}

	public void setProfilePhoto(String profilePhoto) {
		this.profilePhoto = profilePhoto;
	}

	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(LocalDate dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getLinkedInProfile() {
		return linkedInProfile;
	}

	public void setLinkedInProfile(String linkedInProfile) {
		this.linkedInProfile = linkedInProfile;
	}

	public void setOrganizationId(long organizationId) {
		this.organizationId = organizationId;
	}

	public long getOrganizationId() {
		return organizationId;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getBioInformation() {
		return bioInformation;
	}

	public void setBioInformation(String bioInformation) {
		this.bioInformation = bioInformation;
	}

	public LocalDate getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(LocalDate createdOn) {
		this.createdOn = createdOn;
	}

	public String getSecondaryPhoneNumber() {
		return secondaryPhoneNumber;
	}

	public void setSecondaryPhoneNumber(String secondaryPhoneNumber) {
		this.secondaryPhoneNumber = secondaryPhoneNumber;
	}

	public String getSecondaryEmail() {
		return secondaryEmail;
	}

	public void setSecondaryEmail(String secondaryEmail) {
		this.secondaryEmail = secondaryEmail;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFteStatus() {
		return fteStatus;
	}

	public void setFteStatus(String fteStatus) {
		this.fteStatus = fteStatus;
	}

	public long getAssignedSlots() {
		return assignedSlots;
	}

	public void setAssignedSlots(long assignedSlots) {
		this.assignedSlots = assignedSlots;
	}

	public long getTotalSlots() {
		return totalSlots;
	}

	public void setTotalSlots(long totalSlots) {
		this.totalSlots = totalSlots;
	}

	public String getPrimaryContactCountryCode() {
		return primaryContactCountryCode;
	}

	public void setPrimaryContactCountryCode(String primaryContactCountryCode) {
		this.primaryContactCountryCode = primaryContactCountryCode;
	}

	public String getAlternativeContactCountryCode() {
		return alternativeContactCountryCode;
	}

	public void setAlternativeContactCountryCode(String alternativeContactCountryCode) {
		this.alternativeContactCountryCode = alternativeContactCountryCode;
	}

	public String getFcmToken() {
		return fcmToken;
	}

	public void setFcmToken(String fcmToken) {
		this.fcmToken = fcmToken;
	}

	public boolean isMuteAllNotifications() {
		return muteAllNotifications;
	}

	public void setMuteAllNotifications(boolean muteAllNotifications) {
		this.muteAllNotifications = muteAllNotifications;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public long getStaffUserSubCoreTypeId() {
		return staffUserSubCoreTypeId;
	}

	public void setStaffUserSubCoreTypeId(long staffUserSubCoreTypeId) {
		this.staffUserSubCoreTypeId = staffUserSubCoreTypeId;
	}
	

	public List<UserLanguages> getUserLanguages() {
		return userLanguages;
	}

	public void setUserLanguages(List<UserLanguages> userLanguageList) {
		if(userLanguageList!=null)
		this.userLanguages.addAll(userLanguageList);
	}
	

	public UserNationalProviderIdentity getUserNationalProviderIdentity() {
		return userNationalProviderIdentity;
	}

	public void setUserNationalProviderIdentity(UserNationalProviderIdentity userNationalProviderIdentity) {
		this.userNationalProviderIdentity = userNationalProviderIdentity;
	}
	

	public SocialSecurityNumber getSocialSecurityNumber() {
		return socialSecurityNumber;
	}

	public void setSocialSecurityNumber(SocialSecurityNumber socialSecurityNumber) {
		this.socialSecurityNumber = socialSecurityNumber;
	}

	@Override
	public boolean equals(Object obj) {

		if (obj instanceof UserBasicInformation) {
			UserBasicInformation temp = (UserBasicInformation) obj;
			if (this.staffUserCoreTypeId == temp.staffUserCoreTypeId)
				return true;
		}
		return false;
	}

	public List<UserWorkspaces> getUserWorkspaces() {
		return userWorkspaces;
	}

	public void setUserWorkspaces(List<UserWorkspaces> userWorkspaces) {
		this.userWorkspaces = userWorkspaces;
	}

	public String getCvFile() {
		return cvFile;
	}

	public void setCvFile(String cvFile) {
		this.cvFile = cvFile;
	}

	public boolean isFirstLogin() {
		return firstLogin;
	}

	public void setFirstLogin(boolean firstLogin) {
		this.firstLogin = firstLogin;
	}

	public String getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}

	public List<EhrSystems> getEhrSystems() {
		return ehrSystems;
	}

	public void setEhrSystems(List<EhrSystems> ehrSystemId) {
		this.ehrSystems = ehrSystemId;
	}

	public Integer getStatusChangedBy() {
		return statusChangedBy;
	}

	public void setStatusChangedBy(Integer statusChangedBy) {
		this.statusChangedBy = statusChangedBy;
	}

	public UserPermissions getUserPermissions() {
		return userPermissions;
	}

	public void setUserPermissions(UserPermissions userPermissions) {
		this.userPermissions = userPermissions;
	}

	public String getEthnicType() {
		return ethnicType;
	}

	public void setEthnicType(String ethnicType) {
		this.ethnicType = ethnicType;
	}

	public List<String> getSpecialities() {
		return specialities;
	}

	public void setSpecialities(List<String> specialities) {
		this.specialities = specialities;
	}

	@Override
	public String toString() {
		return "UserBasicInformation [userId=" + userId + ", organizationId=" + organizationId + ", firstName="
				+ firstName + ", lastName=" + lastName + ", middleName=" + middleName + ", email=" + email
				+ ", password=" + password + ", defaultPassword=" + defaultPassword + ", preferredName=" + preferredName
				+ ", address1=" + address1 + ", address2=" + address2 + ", city=" + city + ", state=" + state
				+ ", phone=" + phone + ", longitude=" + longitude + ", latitude=" + latitude
				+ ", primaryContactCountryCode=" + primaryContactCountryCode + ", profilePhoto=" + profilePhoto
				+ ", title=" + title + ", userTypeId=" + userTypeId + ", dateOfBirth=" + dateOfBirth
				+ ", staffUserCoreTypeId=" + staffUserCoreTypeId + ", staffUserSubCoreTypeId=" + staffUserSubCoreTypeId
				+ ", staffUserCoreTypeName=" + staffUserCoreTypeName + ", linkedInProfile=" + linkedInProfile
				+ ", gender=" + gender + ", bioInformation=" + bioInformation + ", createdOn=" + createdOn
				+ ", secondaryPhoneNumber=" + secondaryPhoneNumber + ", alternativeContactCountryCode="
				+ alternativeContactCountryCode + ", secondaryEmail=" + secondaryEmail + ", status=" + status
				+ ", fteStatus=" + fteStatus + ", assignedSlots=" + assignedSlots + ", totalSlots=" + totalSlots
				+ ", fcmToken=" + fcmToken + ", muteAllNotifications=" + muteAllNotifications + ", deviceType="
				+ deviceType + ", userLanguages=" + userLanguages + ", userNationalProviderIdentity="
				+ userNationalProviderIdentity + ", socialSecurityNumber=" + socialSecurityNumber + ", userWorkspaces="
				+ userWorkspaces + ", cvFile=" + cvFile + ", firstLogin=" + firstLogin + "]";
	}
}