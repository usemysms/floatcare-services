package com.java.floatcare.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document( collection = "organization_admin")
public class OrganizationAdmin {
	
	@Id
	private long organizationAdminId;
	private long userId;
	private long organizationId;
	
	public OrganizationAdmin() {
		
	}

	public long getOrganizationAdminId() {
		return organizationAdminId;
	}

	public void setOrganizationAdminId(long organizationAdminId) {
		this.organizationAdminId = organizationAdminId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(long organizationId) {
		this.organizationId = organizationId;
	}

	@Override
	public String toString() {
		return "OrganizationAdmin [organizationAdminId=" + organizationAdminId + ", userId=" + userId
				+ ", organizationId=" + organizationId + "]";
	}

}
