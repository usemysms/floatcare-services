package com.java.floatcare.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/*
 * 
 * Chief Nursing Officer (CNO)
 * Director Of Nursing
 * Nurse Manager or Nurse Supervisor	
 * 
 */
@Document(collection = "admin_user_types")
public class AdminUserTypes {

	@Id
	private long adminUserTypeId;
	private String label;

	public AdminUserTypes() {

	}

	public AdminUserTypes(String label) {
		super();
		this.label = label;
	}

	public long getAdminUserTypeId() {
		return adminUserTypeId;
	}

	public void setAdminUserTypeId(long adminUserTypeId) {
		this.adminUserTypeId = adminUserTypeId;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	@Override
	public String toString() {
		return "AdminUserTypes [adminUserTypeId=" + adminUserTypeId + ", label=" + label + "]";
	}

}
