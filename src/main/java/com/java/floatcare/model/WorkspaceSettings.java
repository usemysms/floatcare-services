package com.java.floatcare.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "workspace_settings")
public class WorkspaceSettings {

	@Id
	private long workspaceSettingId;
	private long userId;
	private long eventNotificationId;
	private long onCallShiftReminderId;
	private long meetingReminderId;
	private long assignedWorkShiftsReminderId;
	private long eventsReminderId;
	private long educationReminderId;

	public long getWorkspaceSettingId() {
		return workspaceSettingId;
	}

	public void setWorkspaceSettingId(long workspaceSettingId) {
		this.workspaceSettingId = workspaceSettingId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getEventNotificationId() {
		return eventNotificationId;
	}

	public void setEventNotificationId(long eventNotificationId) {
		this.eventNotificationId = eventNotificationId;
	}

	public long getOnCallShiftReminderId() {
		return onCallShiftReminderId;
	}

	public void setOnCallShiftReminderId(long onCallShiftReminderId) {
		this.onCallShiftReminderId = onCallShiftReminderId;
	}

	public long getMeetingReminderId() {
		return meetingReminderId;
	}

	public void setMeetingReminderId(long meetingReminderId) {
		this.meetingReminderId = meetingReminderId;
	}

	public long getAssignedWorkShiftsReminderId() {
		return assignedWorkShiftsReminderId;
	}

	public void setAssignedWorkShiftsReminderId(long assignedWorkShiftsReminderId) {
		this.assignedWorkShiftsReminderId = assignedWorkShiftsReminderId;
	}

	public long getEventsReminderId() {
		return eventsReminderId;
	}

	public void setEventsReminderId(long eventsReminderId) {
		this.eventsReminderId = eventsReminderId;
	}

	public long getEducationReminderId() {
		return educationReminderId;
	}

	public void setEducationReminderId(long educationReminderId) {
		this.educationReminderId = educationReminderId;
	}

	@Override
	public String toString() {
		return "WorkspaceSettings [workspaceSettingId=" + workspaceSettingId + ", userId=" + userId
				+ ", eventNotificationId=" + eventNotificationId + ", onCallShiftReminderId=" + onCallShiftReminderId
				+ ", meetingReminderId=" + meetingReminderId + ", assignedWorkShiftsReminderId="
				+ assignedWorkShiftsReminderId + ", eventsReminderId=" + eventsReminderId + ", educationReminderId="
				+ educationReminderId + "]";
	}
}