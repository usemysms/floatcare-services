package com.java.floatcare.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "sub_role_types")
public class SubRoleTypes {

	@Id
	private long id;
	private long parentId;
	private String label;
	private String specialist;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getParentId() {
		return parentId;
	}

	public void setParentId(long parentId) {
		this.parentId = parentId;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getSpecialist() {
		return specialist;
	}

	public void setSpecialist(String specialist) {
		this.specialist = specialist;
	}

	@Override
	public String toString() {
		return "SubRoleTypes [id=" + id + ", parentId=" + parentId + ", label=" + label + ", specialist=" + specialist
				+ "]";
	}
}
