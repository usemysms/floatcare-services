package com.java.floatcare.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/*
 * Document Type will be captured as a part of Credential Type
 * 
 * Data: Hep B Immunizations, Flu Vaccination, Malpractise Insurance, H&P, Immunization/Vaccination Record, Fire and Life Safety, TB Questionnaire, Tuberculosis Screening
 *
 *
 */

@Document(collection = "department_types")
public class DepartmentTypes {

	@Id
	private long id;
	private String label;

	public DepartmentTypes() {

	}

	public DepartmentTypes(long id, String label) {
		super();
		this.id = id;
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public long getDocumentTypeId() {
		return id;
	}

	public void setDocumentTypeId(long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return String.format(", label=" + label + "]");
	}
}
