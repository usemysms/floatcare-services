package com.java.floatcare.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "assignment_invitees")
public class AssignmentInvitees {

	@Id
	private long assignmentInviteeId;
	private long assignmentId;
	private long staffCoreTypeId;
	private long subCoreTypeId;
	private double distance;
	private long staffId;
	private boolean isAccepted;
	private boolean isApproved;

	public long getAssignmentInviteeId() {
		return assignmentInviteeId;
	}

	public void setAssignmentInviteeId(long assignmentInviteeId) {
		this.assignmentInviteeId = assignmentInviteeId;
	}

	public long getAssignmentId() {
		return assignmentId;
	}

	public void setAssignmentId(long assignmentId) {
		this.assignmentId = assignmentId;
	}

	public long getStaffCoreTypeId() {
		return staffCoreTypeId;
	}

	public void setStaffCoreTypeId(long staffCoreTypeId) {
		this.staffCoreTypeId = staffCoreTypeId;
	}

	public long getSubCoreTypeId() {
		return subCoreTypeId;
	}

	public void setSubCoreTypeId(long subCoreTypeId) {
		this.subCoreTypeId = subCoreTypeId;
	}

	public long getStaffId() {
		return staffId;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public void setStaffId(long staffId) {
		this.staffId = staffId;
	}

	public boolean isAccepted() {
		return isAccepted;
	}

	public void setAccepted(boolean isAccepted) {
		this.isAccepted = isAccepted;
	}

	public boolean isApproved() {
		return isApproved;
	}

	public void setApproved(boolean isApproved) {
		this.isApproved = isApproved;
	}

	@Override
	public String toString() {
		return "AssignmentInvitees [assignmentInviteeId=" + assignmentInviteeId + ", assignmentId=" + assignmentId
				+ ", staffCoreTypeId=" + staffCoreTypeId + ", staffId=" + staffId + ", isAccepted=" + isAccepted
				+ ", isApproved=" + isApproved + "]";
	}
}