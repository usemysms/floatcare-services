package com.java.floatcare.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "email_verification")
public class EmailVerification {

	@Id
	private long emailId;
	private String otp;
	private String email;
	private boolean isVerified;
	private long expiryTime;

	public void setEmailId(long emailId) {
		this.emailId = emailId;
	}

	public long getEmailId() {
		return emailId;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isVerified() {
		return isVerified;
	}

	public void setVerified(boolean isVerified) {
		this.isVerified = isVerified;
	}

	public long getExpiryTime() {
		return expiryTime;
	}

	public void setExpiryTime(long expiryTime) {
		this.expiryTime = expiryTime;
	}

	@Override
	public String toString() {
		return "EmailVerification [emailId=" + emailId + ", otp=" + otp + ", email=" + email
				+ ", isVerified=" + isVerified + ", expiryTime=" + expiryTime + "]";
	}

}
