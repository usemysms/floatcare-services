package com.java.floatcare.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "open_shift_invitees")
public class OpenShiftInvitees {

	@Id
	private long openShiftInviteeId;
	private long openShiftId;
	private long userId;
	private String name;
	private String profilePhoto;
	private boolean isApproved;
	private boolean isAccepted;

	public long getOpenShiftInviteeId() {
		return openShiftInviteeId;
	}

	public void setOpenShiftInviteeId(long openShiftInviteeId) {
		this.openShiftInviteeId = openShiftInviteeId;
	}

	public long getOpenShiftId() {
		return openShiftId;
	}

	public void setOpenShiftId(long openShiftId) {
		this.openShiftId = openShiftId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public boolean isApproved() {
		return isApproved;
	}

	public void setApproved(boolean isApproved) {
		this.isApproved = isApproved;
	}

	public boolean isAccepted() {
		return isAccepted;
	}

	public void setAccepted(boolean isAccepted) {
		this.isAccepted = isAccepted;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProfilePhoto() {
		return profilePhoto;
	}

	public void setProfilePhoto(String profilePhoto) {
		this.profilePhoto = profilePhoto;
	}

	@Override
	public String toString() {
		return "OpenShiftInvitees [openShiftId=" + openShiftId + ", userId=" + userId + ", isApproved=" + isApproved
				+ ", isAccepted=" + isAccepted + "]";
	}
}
