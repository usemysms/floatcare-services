package com.java.floatcare.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "user_privacy_settings")
public class UserPrivacySettings {

	@Id
	private long userPrivacySettingId;
	private long userId;
	private boolean isVisibleToPublic;
	private boolean isVisibleToWorkspace;
	private boolean isVisibleToCollegues;
	
	public long getUserPrivacySettingId() {
		return userPrivacySettingId;
	}

	public void setUserPrivacySettingId(long userPrivacySettingId) {
		this.userPrivacySettingId = userPrivacySettingId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public boolean isVisibleToPublic() {
		return isVisibleToPublic;
	}

	public void setVisibleToPublic(boolean isVisibleToPublic) {
		this.isVisibleToPublic = isVisibleToPublic;
	}

	public boolean isVisibleToWorkspace() {
		return isVisibleToWorkspace;
	}

	public void setVisibleToWorkspace(boolean isVisibleToWorkspace) {
		this.isVisibleToWorkspace = isVisibleToWorkspace;
	}

	public boolean isVisibleToCollegues() {
		return isVisibleToCollegues;
	}

	public void setVisibleToCollegues(boolean isVisibleToCollegues) {
		this.isVisibleToCollegues = isVisibleToCollegues;
	}

	@Override
	public String toString() {
		return "UserPrivacySettings [isVisibleToPublic=" + isVisibleToPublic + ", isVisibleToWorkspace="
				+ isVisibleToWorkspace + ", isVisibleToCollegues=" + isVisibleToCollegues + "]";
	}

}
