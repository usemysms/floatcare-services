package com.java.floatcare.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection = "shift_planning_drafts")
public class ShiftPlanningDrafts {

	@Id
	private long shiftPlanningDraftId;
	private long scheduleLayoutId;
	private String shiftPlanningDraftPojo;
	private UserBasicInformation user;
	private String status;

	public long getShiftPlanningDraftId() {
		return shiftPlanningDraftId;
	}

	public void setShiftPlanningDraftId(long shiftPlanningDraftId) {
		this.shiftPlanningDraftId = shiftPlanningDraftId;
	}

	public long getScheduleLayoutId() {
		return scheduleLayoutId;
	}

	public void setScheduleLayoutId(long scheduleLayoutId) {
		this.scheduleLayoutId = scheduleLayoutId;
	}


	public String getShiftPlanningDraftPojo() {
		return shiftPlanningDraftPojo;
	}

	public void setShiftPlanningDraftPojo(String shiftPlanningDraftPojo) {
		this.shiftPlanningDraftPojo = shiftPlanningDraftPojo;
	}

	public UserBasicInformation getUser() {
		return user;
	}

	public void setUser(UserBasicInformation user) {
		this.user = user;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "ShiftPlanningDrafts [shiftPlanningDraftId=" + shiftPlanningDraftId + ", scheduleLayoutId="
				+ scheduleLayoutId + ", object=" + shiftPlanningDraftPojo + "]";
	}
}