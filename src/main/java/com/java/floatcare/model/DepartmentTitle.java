package com.java.floatcare.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "department_title")
public class DepartmentTitle {

	@Id
	private long departmentTitleId;
	private String name;
	private long departmentId;
	private long departmentTypeId;
	private long organizationId;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(long departmentId) {
		this.departmentId = departmentId;
	}

	public long getDepartmentTitleId() {
		return departmentTitleId;
	}

	public void setDepartmentTitleId(long departmentTitleId) {
		this.departmentTitleId = departmentTitleId;
	}

	public long getDepartmentTypeId() {
		return departmentTypeId;
	}

	public void setDepartmentTypeId(long departmentTypeId) {
		this.departmentTypeId = departmentTypeId;
	}

	public long getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(long organizationId) {
		this.organizationId = organizationId;
	}

	@Override
	public String toString() {
		return "DepartmentTitle [name=" + name + ", departmentId=" + departmentId + ", organizationId=" + organizationId
				+ "]";
	}

}
