package com.java.floatcare.model;

import org.springframework.data.annotation.Id;

public class DepartmentAdmin {
	
	@Id
	private long departmentAdminId;
	private long userId;
	
	public DepartmentAdmin()
	{
		
	}

	public long getDepartmentAdminId() {
		return departmentAdminId;
	}

	public void setDepartmentAdminId(long departmentAdminId) {
		this.departmentAdminId = departmentAdminId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "DepartmentAdmin [departmentAdminId=" + departmentAdminId + ", userId=" + userId + "]";
	}
	
}
