package com.java.floatcare.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "department_staff")
public class DepartmentStaff {

	@Id
	private long departmentStaffId;
	private long departmentId;
	private long staffUserCoreTypeId;	
	private long staffCount;

	public long getDepartmentStaffId() {
		return departmentStaffId;
	}


	public void setDepartmentStaffId(long departmentStaffId) {
		this.departmentStaffId = departmentStaffId;
	}

	public long getDepartmentId() {
		return departmentId;
	}


	public void setDepartmentId(long departmentId) {
		this.departmentId = departmentId;
	}


	public long getStaffUserCoreTypeId() {
		return staffUserCoreTypeId;
	}


	public void setStaffUserCoreTypeId(long staffUserCoreTypeId) {
		this.staffUserCoreTypeId = staffUserCoreTypeId;
	}


	public long getStaffCount() {
		return staffCount;
	}


	public void setStaffCount(long staffCount) {
		this.staffCount = staffCount;
	}


	@Override
	public String toString() {
		return "DepartmentStaff [departmentStaffId=" + departmentStaffId + ", departmentId=" + departmentId
				+ ", staffUserCoreTypeId=" + staffUserCoreTypeId + ", staffCount=" + staffCount + "]";
	}

}
