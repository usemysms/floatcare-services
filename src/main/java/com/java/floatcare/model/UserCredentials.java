package com.java.floatcare.model;

import java.time.LocalDate;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/*
 * 
 * User Credentials will be captured as a part of credential icon, credential name, credential type, expiration date, credential status
 * 
 * 
 */

@Document(collection = "user_credentials")
public class UserCredentials {

	@Id
	private long userCredentialId;
	private long userId;
	private String credentialIcon;
	private String credentialName;
	private long credentialTypeId;
	private long credentialSubTypeId;
	private LocalDate expirationDate;
	private long credentialStatusId;
	private String licenseNumber;
	private String frontFilePhoto;
	private String backFilePhoto;
	private String fileName;
	private String state;
	private boolean isCompactLicense;
	private transient String formattedDate;
	private boolean isVerified;
	private boolean isPending;
	private boolean isRequired;
	private LocalDate verifiedOn;
	private LocalDate createdOn;
	private long verifiedBy;
	private boolean isGenerated;

	public long getUserCredentialId() {
		return userCredentialId;
	}

	public void setUserCredentialId(long userCredentialId) {
		this.userCredentialId = userCredentialId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getCredentialIcon() {
		return credentialIcon;
	}

	public void setCredentialIcon(String credentialIcon) {
		this.credentialIcon = credentialIcon;
	}

	public String getCredentialName() {
		return credentialName;
	}

	public void setCredentialName(String credentialName) {
		this.credentialName = credentialName;
	}

	public long getCredentialTypeId() {
		return credentialTypeId;
	}

	public void setCredentialTypeId(long credentialTypeId) {
		this.credentialTypeId = credentialTypeId;
	}

	public long getCredentialSubTypeId() {
		return credentialSubTypeId;
	}

	public void setCredentialSubTypeId(long credentialSubTypeId) {
		this.credentialSubTypeId = credentialSubTypeId;
	}

	public LocalDate getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(LocalDate expirationDate) {
		this.expirationDate = expirationDate;
	}

	public long getCredentialStatusId() {
		return credentialStatusId;
	}

	public void setCredentialStatusId(long credentialStatusId) {
		this.credentialStatusId = credentialStatusId;
	}

	public String getLicenseNumber() {
		return licenseNumber;
	}

	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}

	public String getFrontFilePhoto() {
		return frontFilePhoto;
	}

	public void setFrontFilePhoto(String frontFilePhoto) {
		this.frontFilePhoto = frontFilePhoto;
	}

	public String getBackFilePhoto() {
		return backFilePhoto;
	}

	public void setBackFilePhoto(String backFilePhoto) {
		this.backFilePhoto = backFilePhoto;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public boolean getIsCompactLicense() {
		return isCompactLicense;
	}

	public void setIsCompactLicense(boolean isCompactLicense) {
		this.isCompactLicense = isCompactLicense;
	}

	public String getFormattedDate() {
		return formattedDate;
	}

	public void setFormattedDate(String formattedDate) {
		this.formattedDate = formattedDate;
	}

	public boolean isVerified() {
		return isVerified;
	}

	public void setVerified(boolean isVerified) {
		this.isVerified = isVerified;
	}

	public void setCompactLicense(boolean isCompactLicense) {
		this.isCompactLicense = isCompactLicense;
	}

	public LocalDate getVerifiedOn() {
		return verifiedOn;
	}

	public void setVerifiedOn(LocalDate verifiedOn) {
		this.verifiedOn = verifiedOn;
	}

	public LocalDate getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(LocalDate createdOn) {
		this.createdOn = createdOn;
	}

	public boolean isPending() {
		return isPending;
	}

	public void setPending(boolean isPending) {
		this.isPending = isPending;
	}

	public long getVerifiedBy() {
		return verifiedBy;
	}

	public void setVerifiedBy(long verifiedBy) {
		this.verifiedBy = verifiedBy;
	}

	public boolean isRequired() {
		return isRequired;
	}

	public void setRequired(boolean isRequired) {
		this.isRequired = isRequired;
	}

	public boolean isGenerated() {
		return isGenerated;
	}

	public void setGenerated(boolean isGenerated) {
		this.isGenerated = isGenerated;
	}

	
	@Override
	public String toString() {
		return "UserCredentials [userCredentialId=" + userCredentialId + ", userId=" + userId + ", credentialIcon="
				+ credentialIcon + ", credentialName=" + credentialName + ", credentialTypeId=" + credentialTypeId
				+ ", credentialSubTypeId=" + credentialSubTypeId + ", expirationDate=" + expirationDate
				+ ", credentialStatusId=" + credentialStatusId + ", licenseNumber=" + licenseNumber
				+ ", frontFilePhoto=" + frontFilePhoto + ", backFilePhoto=" + backFilePhoto + ", fileName=" + fileName
				+ ", state=" + state + ", isCompactLicense=" + isCompactLicense + ", isVerified=" + isVerified
				+ ", isPending=" + isPending + ", isRequired=" + isRequired + ", verifiedOn=" + verifiedOn
				+ ", createdOn=" + createdOn + ", verifiedBy=" + verifiedBy + ", isGenerated=" + isGenerated + "]";
	}
}