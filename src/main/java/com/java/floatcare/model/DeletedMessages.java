package com.java.floatcare.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "deleted_messages")
public class DeletedMessages {

	@Id
	private long deletedMessageId;
	private long userId;
	private long receiverMessagesDeleteduserId;
	private long groupId;
	private List<Long> messageIds = new ArrayList<>();
	private boolean isReplied;

	public long getDeletedMessageId() {
		return deletedMessageId;
	}

	public void setDeletedMessageId(long deletedMessageId) {
		this.deletedMessageId = deletedMessageId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getReceiverMessagesDeleteduserId() {
		return receiverMessagesDeleteduserId;
	}

	public void setReceiverMessagesDeleteduserId(long receiverMessagesDeleteduserId) {
		this.receiverMessagesDeleteduserId = receiverMessagesDeleteduserId;
	}

	public List<Long> getMessageIds() {
		return messageIds;
	}

	public void setMessageIds(List<Long> messageIdList) {
		this.messageIds.addAll(messageIdList);
	}

	public boolean isReplied() {
		return isReplied;
	}

	public void setReplied(boolean isReplied) {
		this.isReplied = isReplied;
	}

	public long getGroupId() {
		return groupId;
	}

	public void setGroupId(long groupId) {
		this.groupId = groupId;
	}
	
	public void addSingleDeletedMessage(Long messageId) {
		this.messageIds.add(messageId);
	}

	@Override
	public String toString() {
		return "DeletedMessages [deletedMessageId=" + deletedMessageId + ", userId=" + userId
				+ ", receiverMessagesDeleteduserId=" + receiverMessagesDeleteduserId + ", groupId=" + groupId
				+ ", messageIds=" + messageIds + ", isReplied=" + isReplied + "]";
	}
}
