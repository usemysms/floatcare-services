package com.java.floatcare.model;

import java.time.LocalDate;
import java.util.List;
import java.util.TreeSet;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.java.floatcare.api.request.pojo.ContactInformation;
import com.java.floatcare.api.request.pojo.WorksiteAvailability;

/*
 * 
 * 
 * Businesses will be captured as a part of organizations
 * 
 * 
 */

@Document(collection = "businesses")
public class Businesses {

	@Id
	private long businessId;
	private long organizationId;
	private String name;
	private long businessTypeId;
	private long businessSubTypeId;
	private String phoneNumber;
	private long extension;
	private String email;
	private String address;
	private String address2;
	private String city;
	private String state;
	private String postalCode;
	private String country;
	private long count;
	private String status;
	private String coverPhoto;
	private String description;
	private LocalDate createdDate;
	private List<WorksiteAvailability> worksiteAvailability;
	private TreeSet<Long> preferredProviderIds = new TreeSet<>();
	private ContactInformation clientInfo;
	private ContactInformation providerInfo;
	private List<Long> preferredSpecialities;


	public long getBusinessId() {
		return businessId;
	}

	public void setBusinessId(long businessId) {
		this.businessId = businessId;
	}

	public long getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(long organizationId) {
		this.organizationId = organizationId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getBusinessTypeId() {
		++count;
		return businessTypeId;
	}

	public void setBusinessTypeId(long businessTypeId) {
		this.businessTypeId = businessTypeId;
	}

	public long getBusinessSubTypeId() {
		return businessSubTypeId;
	}

	public void setBusinessSubTypeId(long businessSubTypeId) {
		this.businessSubTypeId = businessSubTypeId;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public long getExtension() {
		return extension;
	}

	public void setExtension(long extension) {
		this.extension = extension;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public long countBusinessTypeId() {
		return count;	
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getCoverPhoto() {
		return coverPhoto;
	}
	public void setCoverPhoto(String coverPhoto) {
		this.coverPhoto = coverPhoto;
	}

	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}
	
	public LocalDate getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(LocalDate createdDate) {
		this.createdDate = createdDate;
	}

	public List<WorksiteAvailability> getWorksiteAvailability() {
		return worksiteAvailability;
	}

	public void setWorksiteAvailability(List<WorksiteAvailability> worksiteAvailability) {
		this.worksiteAvailability = worksiteAvailability;
	}

	public TreeSet<Long> getPreferredProviderIds() {
		return preferredProviderIds;
	}

	public void setPreferredProviderId(Long preferredProviderId) {
		this.preferredProviderIds.add(preferredProviderId);
	}

	public void setPreferredProviderIds(List<Long> preferredProviderId) {
		this.preferredProviderIds.addAll(preferredProviderId);
	}
	
	

	public ContactInformation getClientInfo() {
		return clientInfo;
	}

	public void setClientInfo(ContactInformation clientInfo) {
		this.clientInfo = clientInfo;
	}

	public ContactInformation getProviderInfo() {
		return providerInfo;
	}

	public void setProviderInfo(ContactInformation providerInfo) {
		this.providerInfo = providerInfo;
	}

	public List<Long> getPreferredSpecialities() {
		return preferredSpecialities;
	}

	public void setPreferredSpecialities(List<Long> preferredSpecialities) {
		this.preferredSpecialities = preferredSpecialities;
	}

	
	@Override
	public String toString() {
		return "Businesses [businessId=" + businessId + ", organizationId=" + organizationId + ", name=" + name
				+ ", businessTypeId=" + businessTypeId + ", businessSubTypeId=" + businessSubTypeId + ", phoneNumber="
				+ phoneNumber + ", extension=" + extension + ", email=" + email + ", address=" + address + ", address2="
				+ address2 + ", city=" + city + ", state=" + state + ", postalCode=" + postalCode + ", country="
				+ country + ", count=" + count + ", status=" + status + ", coverPhoto=" + coverPhoto + ", description="
				+ description + ", createdDate=" + createdDate + ", worksiteAvailability=" + worksiteAvailability
				+ ", preferredProviderIds=" + preferredProviderIds + ", clientInfo=" + clientInfo + ", providerInfo="
				+ providerInfo + ", preferredSpecialities=" + preferredSpecialities + "]";
	}


}