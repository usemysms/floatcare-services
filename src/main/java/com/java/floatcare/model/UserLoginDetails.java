package com.java.floatcare.model;

import java.util.Date;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "user_login_details")
public class UserLoginDetails {
	
	long userId;
	String authToken;
	String device;// IP address/UUID/Device name
	String deviceType;
	boolean isLoggedOut;
	Date loggedinTime;
	Date lasttouch;
	long loginOtpId;
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getAuthToken() {
		return authToken;
	}
	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}
	public String getDevice() {
		return device;
	}
	public void setDevice(String device) {
		this.device = device;
	}
	public boolean isLoggedOut() {
		return isLoggedOut;
	}
	public void setLoggedOut(boolean isLoggedOut) {
		this.isLoggedOut = isLoggedOut;
	}
	public Date getLoggedinTime() {
		return loggedinTime;
	}
	public void setLoggedinTime(Date loggedinTime) {
		this.loggedinTime = loggedinTime;
	}
	public Date getLasttouch() {
		return lasttouch;
	}
	public void setLasttouch(Date lasttouch) {
		this.lasttouch = lasttouch;
	}
	public long getLoginOtpId() {
		return loginOtpId;
	}
	public void setLoginOtpId(long loginOtpId) {
		this.loginOtpId = loginOtpId;
	}
	
	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	@Override
	public String toString() {
		return "UserLoginDetails [userId=" + userId + ", authToken=" + authToken + ", device=" + device
				+ ", isLoggedOut=" + isLoggedOut + ", loggedinTime=" + loggedinTime + ", lasttouch=" + lasttouch
				+ ", loginOtpId=" + loginOtpId + "]";
	}
	
	
	}
