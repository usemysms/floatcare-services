package com.java.floatcare.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "clients")
public class Clients {

	@Id
	private long clientId;
	private long organizationId;
	private long businessId;
	private String firstName;
	private String lastName;
	private String profilePhoto;
	private LocalDate dateOfBirth;
	private String phoneNumber;
	private String medicareNumber;
	private String medicaidNumber;
	private String martialStatus;
	private String religion;
	private String ethnicity;
	private String language;
	private String status;
	private List<Household> household = new ArrayList<>();

	public long getClientId() {
		return clientId;
	}

	public void setClientId(long clientId) {
		this.clientId = clientId;
	}

	public long getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(long organizationId) {
		this.organizationId = organizationId;
	}

	public long getBusinessId() {
		return businessId;
	}

	public void setBusinessId(long businessId) {
		this.businessId = businessId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getProfilePhoto() {
		return profilePhoto;
	}

	public void setProfilePhoto(String profilePhoto) {
		this.profilePhoto = profilePhoto;
	}

	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(LocalDate dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getMedicareNumber() {
		return medicareNumber;
	}

	public void setMedicareNumber(String medicareNumber) {
		this.medicareNumber = medicareNumber;
	}

	public String getMedicaidNumber() {
		return medicaidNumber;
	}

	public void setMedicaidNumber(String medicaidNumber) {
		this.medicaidNumber = medicaidNumber;
	}

	public String getMartialStatus() {
		return martialStatus;
	}

	public void setMartialStatus(String martialStatus) {
		this.martialStatus = martialStatus;
	}

	public String getReligion() {
		return religion;
	}

	public void setReligion(String religion) {
		this.religion = religion;
	}

	public String getEthnicity() {
		return ethnicity;
	}

	public void setEthnicity(String ethnicity) {
		this.ethnicity = ethnicity;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public List<Household> getHousehold() {
		return household;
	}

	public void setHousehold(List<Household> householdList) {
		this.household.addAll(householdList);
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Clients [clientId=" + clientId + ", organizationId=" + organizationId + ", businessId=" + businessId
				+ ", firstName=" + firstName + ", lastName=" + lastName + ", dateOfBirth=" + dateOfBirth
				+ ", phoneNumber=" + phoneNumber + ", medicareNumber=" + medicareNumber + ", medicaidNumber="
				+ medicaidNumber + ", martialStatus=" + martialStatus + ", religion=" + religion + ", ethnicity="
				+ ethnicity + ", language=" + language + ", household=" + household + ", status=" + status + "]";
	}
}
