package com.java.floatcare.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "facilitiesAdmin")
public class FacilitiesAdmin {

	@Id
	private long facilitiesId;
	private long userId;

	public FacilitiesAdmin() {

	}

	public long getFacilitiesId() {
		return facilitiesId;
	}

	public void setFacilitiesId(long facilitiesId) {
		this.facilitiesId = facilitiesId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "FacilitiesAdmin [facilitiesId=" + facilitiesId + ", userId=" + userId + "]";
	}

}
