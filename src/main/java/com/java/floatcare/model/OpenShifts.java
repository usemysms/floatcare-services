package com.java.floatcare.model;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "open_shifts")
public class OpenShifts {

	@Id
	private long openShiftId;
	private long worksiteId;
	private long departmentId;
	private long shiftTypeId;
	private LocalDate onDate;
	private LocalTime startTime;
	private LocalTime endTime;
	private long staffUserCoreTypeId;
	private long quota;
	private boolean isUrgent;
	private boolean isSendNotifications;
	private boolean isAutoApprove;
	private boolean isAutoClose;
	private boolean isAllowPartial;
	private boolean isAllowOvertime;
	private String note;
	private String status;
	private List<Integer> invitees = new ArrayList<>();

	public long getOpenShiftId() {
		return openShiftId;
	}

	public void setOpenShiftId(long openShiftId) {
		this.openShiftId = openShiftId;
	}

	public long getWorksiteId() {
		return worksiteId;
	}

	public void setWorksiteId(long worksiteId) {
		this.worksiteId = worksiteId;
	}

	public long getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(long departmentId) {
		this.departmentId = departmentId;
	}

	public long getShiftTypeId() {
		return shiftTypeId;
	}

	public void setShiftTypeId(long shiftTypeId) {
		this.shiftTypeId = shiftTypeId;
	}

	public LocalDate getOnDate() {
		return onDate;
	}

	public void setOnDate(LocalDate onDate) {
		this.onDate = onDate;
	}

	public LocalTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalTime startTime) {
		this.startTime = startTime;
	}

	public LocalTime getEndTime() {
		return endTime;
	}

	public void setEndTime(LocalTime endTime) {
		this.endTime = endTime;
	}

	public long getStaffUserCoreTypeId() {
		return staffUserCoreTypeId;
	}

	public void setStaffUserCoreTypeId(long staffUserCoreTypeId) {
		this.staffUserCoreTypeId = staffUserCoreTypeId;
	}

	public long getQuota() {
		return quota;
	}

	public void setQuota(long quota) {
		this.quota = quota;
	}

	public boolean isUrgent() {
		return isUrgent;
	}

	public void setUrgent(boolean isUrgent) {
		this.isUrgent = isUrgent;
	}

	public boolean isSendNotifications() {
		return isSendNotifications;
	}

	public void setSendNotifications(boolean isSendNotifications) {
		this.isSendNotifications = isSendNotifications;
	}

	public boolean isAutoApprove() {
		return isAutoApprove;
	}

	public void setAutoApprove(boolean isAutoApprove) {
		this.isAutoApprove = isAutoApprove;
	}

	public boolean isAutoClose() {
		return isAutoClose;
	}

	public void setAutoClose(boolean isAutoClose) {
		this.isAutoClose = isAutoClose;
	}

	public boolean isAllowPartial() {
		return isAllowPartial;
	}

	public void setAllowPartial(boolean isAllowPartial) {
		this.isAllowPartial = isAllowPartial;
	}

	public boolean isAllowOvertime() {
		return isAllowOvertime;
	}

	public void setAllowOvertime(boolean isAllowOvertime) {
		this.isAllowOvertime = isAllowOvertime;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<Integer> getInvitees() {
		return invitees;
	}

	public void setInvitees(List<Integer> invitee) {
		this.invitees.addAll(invitee);
	}

	@Override
	public String toString() {
		return "OpenShifts [openShiftId=" + openShiftId + ", worksiteId=" + worksiteId + ", departmentId="
				+ departmentId + ", shiftTypeId=" + shiftTypeId + ", onDate=" + onDate + ", startTime=" + startTime
				+ ", endTime=" + endTime + ", staffUserCoreTypeId=" + staffUserCoreTypeId + ", quota=" + quota
				+ ", isUrgent=" + isUrgent + ", isSendNotifications=" + isSendNotifications + ", isAutoApprove="
				+ isAutoApprove + ", isAutoClose=" + isAutoClose + ", isAllowPartial=" + isAllowPartial
				+ ", isAllowOvertime=" + isAllowOvertime + ", note=" + note + ", status=" + status + "]";
	}
}