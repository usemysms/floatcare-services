package com.java.floatcare;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.java.floatcare.service.PeriodicSchedulerService;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

@SpringBootApplication
@EnableScheduling
@OpenAPIDefinition(info = @Info(title = "FloatCare API", version = "1.0", description = "FloatCare API Information"))
public class FloatcareApplication {
	@Bean
	public PeriodicSchedulerService task() {
		return new PeriodicSchedulerService();
	}

	public static void main(String[] args) {
		SpringApplication.run(FloatcareApplication.class, args);
	}
}
