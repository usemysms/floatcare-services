package com.java.floatcare.utils;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

public class SplitStringForCharacterAndNumric {

	public static DateTime splitString(String startDate, String text) {

		String[] splitString = text.split("(?<=\\d)(?=\\D)");

		if (splitString[1].equalsIgnoreCase("w")) {
			return ((DateTime.parse(startDate).plusWeeks(Integer.parseInt(splitString[0])).minusDays(1).withTime(0,0,0,0).withZone(DateTimeZone.UTC)));
		}
		else if (splitString[1].equalsIgnoreCase("d")) {
			return ((DateTime.parse(startDate).plusDays(Integer.parseInt(splitString[0]))));
		}
		else if (splitString[1].equalsIgnoreCase("h")) {
			return ((DateTime.parse(startDate).plusHours(Integer.parseInt(splitString[0]))));
		}
		else if (splitString[1].equalsIgnoreCase("m"))
			return ((DateTime.parse(startDate).plusMonths(Integer.parseInt(splitString[0]))).minusDays(1).withTime(0,0,0,0).withZone(DateTimeZone.UTC));
		else
			return null;
	
	}
}
