package com.java.floatcare.utils;

public interface UploadImageDAO {

	public boolean uploadImageFile(String profilePhoto, String fileName, long id) throws Exception;

}
