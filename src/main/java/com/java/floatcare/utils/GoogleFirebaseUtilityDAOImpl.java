package com.java.floatcare.utils;

import org.json.simple.JSONObject;
import org.springframework.stereotype.Component;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import static com.java.floatcare.utils.ConstantUtils.*;

import java.io.IOException;

@Component
public class GoogleFirebaseUtilityDAOImpl implements GoogleFirebaseUtilityDAO {

	@SuppressWarnings("unchecked")
	@Override
	public void sendMessageNotificationToUser(String userName, long senderId, long receiverId, long groupId, String profilePhoto,
			String receiverFcmToken, String message, String messageBody, String deepLink, String aesKey, String initVector, String userDeviceType) {

		try {

			MediaType JSON = MediaType.parse("application/json; charset=utf-8");

			OkHttpClient client = new OkHttpClient();

			/*
			 * for Android
			 */
			if (userDeviceType.equalsIgnoreCase("Android")) {
				
				JSONObject json = new JSONObject();
				JSONObject dataJson = new JSONObject();
	
				dataJson.put("title", userName);
				dataJson.put("detail", messageBody);
				dataJson.put("deepLink", deepLink);
				dataJson.put("url", profilePhoto);
				dataJson.put("userId", senderId);
				
				if (groupId > 0)
					dataJson.put("groupId", groupId);
				else
					dataJson.put("groupId", 0);
	
				if(aesKey != null)
					dataJson.put("key", aesKey);
				if(initVector != null)
					dataJson.put("salt", initVector);
	
				json.put("to", receiverFcmToken);
				json.put("data", dataJson);
				
				RequestBody bodyData = RequestBody.create(JSON, json.toString());
				Request request = new Request.Builder().header("Authorization", "key=" + FCMServerKey)
						.url("https://fcm.googleapis.com/fcm/send").post(bodyData).build();

				Response response = client.newCall(request).execute();
				@SuppressWarnings("unused")
				String finalResponse = response.body().string();
			}

			/*
			 * for ios
			 */
			else if (userDeviceType.equalsIgnoreCase("ios")) {

				JSONObject data = new JSONObject();
				JSONObject object = new JSONObject();
	
				data.put("detail", message);
				data.put("body", messageBody);
				data.put("deepLink", deepLink);
				data.put("url", profilePhoto);
				data.put("userId", senderId);
				data.put("title", userName);
				if(aesKey != null)
					data.put("key", aesKey);
				if(initVector != null)
					data.put("salt", initVector);
				data.put("category", "SECRET");
				data.put("mutable-content",1);
	
				object.put("priority", "high");
				object.put("notification", data); // ios
				object.put("to", receiverFcmToken);
				
				RequestBody bodyData = RequestBody.create(JSON, object.toString());
				Request request = new Request.Builder().header("Authorization", "key=" + FCMServerKey)
						.url("https://fcm.googleapis.com/fcm/send").post(bodyData).build();

				@SuppressWarnings("unused")
				Response response = client.newCall(request).execute();
//				String finalResponse = response.body().string();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void sendEventNotificationToUser(long userId, String receiverFcmToken, String message, String messageBody,
			String deepLink, String userDeviceType, String invitationCode) {

		try {

			MediaType JSON = MediaType.parse("application/json; charset=utf-8");

			OkHttpClient client = new OkHttpClient();

			JSONObject object = new JSONObject();

			/*
			 * for Android
			 */
			if (userDeviceType.equalsIgnoreCase("Android")) {

				JSONObject json = new JSONObject();
				JSONObject dataJson = new JSONObject();
	
				dataJson.put("title", message);
				dataJson.put("detail", messageBody);
				dataJson.put("deepLink", deepLink);
				if (invitationCode != null)
					dataJson.put("invitationCode", invitationCode);
	
				json.put("data", dataJson);
				json.put("to", receiverFcmToken); // android

				RequestBody bodyData = RequestBody.create(JSON, json.toString());
				Request request = new Request.Builder().header("Authorization", "key=" + FCMServerKey)
						.url("https://fcm.googleapis.com/fcm/send").post(bodyData).build();

				Response response = client.newCall(request).execute();
				@SuppressWarnings("unused")
				String finalResponse = response.body().string();

			}

			/*
			 * for ios
			 */			
			else if (userDeviceType.equalsIgnoreCase("ios")) {

				JSONObject data = new JSONObject();

				data.put("body", messageBody);
				data.put("deepLink", deepLink);
				data.put("title", message);
				if (invitationCode != null)
					data.put("invitationCode", invitationCode);
	
				object.put("priority", "high");
				object.put("notification", data); // ios
				object.put("to", receiverFcmToken);
				// object.putAll(json);

				RequestBody bodyData = RequestBody.create(JSON, object.toString());
				// System.out.println(object);
				Request request = new Request.Builder().header("Authorization", "key=" + FCMServerKey)
						.url("https://fcm.googleapis.com/fcm/send").post(bodyData).build();

				Response response = client.newCall(request).execute();
				@SuppressWarnings("unused")
				String finalResponse = response.body().string();
//				System.out.println(finalResponse);				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	@Override // sending message count in the notification
	public void sendNotificationToIosUser(String userName, Long senderId, Long receiverId, String profilePhoto,
			String receiverFcmToken, String message, String messageBody, String deepLink, String aesKey, String initVector, int badgeCount) {

		MediaType JSON = MediaType.parse("application/json; charset=utf-8");

		OkHttpClient client = new OkHttpClient();

		JSONObject data = new JSONObject();
		JSONObject object = new JSONObject();

		message = userName+" has sent a message";

		data.put("detail", "");
//		data.put("body", messageBody);
		data.put("deepLink", deepLink);
		data.put("url", profilePhoto);
		data.put("userId", senderId);
		data.put("title", message);
        data.put("badge", badgeCount);
        if(aesKey != null)
			data.put("key", aesKey);
		if(initVector != null)
			data.put("salt", initVector);
		data.put("category", "SECRET");
		data.put("mutable-content",1);

		object.put("priority", "high");
		object.put("notification", data); // ios
		object.put("to", receiverFcmToken);

		RequestBody bodyData = RequestBody.create(JSON, object.toString());
		Request request = new Request.Builder().header("Authorization", "key=" + FCMServerKey).url("https://fcm.googleapis.com/fcm/send").post(bodyData).build();

		try {
			@SuppressWarnings("unused")
			Response response = client.newCall(request).execute();
		} catch (IOException e) {

		}
	}
}