package com.java.floatcare.utils;

import java.time.LocalDate;
import java.time.ZonedDateTime;

import org.springframework.core.convert.converter.Converter;

public class ZonedDateTimeWriteConverter implements Converter<ZonedDateTime, LocalDate> {
    @Override
    public LocalDate convert(ZonedDateTime zonedDateTime) {
        return zonedDateTime.toLocalDate();
    }
}