package com.java.floatcare.utils;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;

import javax.imageio.ImageIO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.java.floatcare.s3bitbucket.AWSService;

@Component
public class UploadImageDAOImpl implements UploadImageDAO {

	@Autowired
	private AWSService awsService;

	public boolean uploadImageFile (String profilePhoto, String fileName, long id) throws Exception {

		try {

			String[] arrOfStr = profilePhoto.split(",", -1); // split the base 64 string
			BufferedImage image = null;

			for (String each : arrOfStr) // separates the data and store the right value of base64 in profilePhoto
				profilePhoto = each;

			byte[] imageByteArray = Base64.getDecoder().decode(profilePhoto); // extracting image from base64 string
			ByteArrayInputStream bis = new ByteArrayInputStream(imageByteArray);
			image = ImageIO.read(bis);
			bis.close();

			File outputfile = new File(fileName); // write the image to a file
			ImageIO.write(image, "png", outputfile);

			byte[] content = null;

			Path path = Paths.get(fileName); // find the image from the directory
			content = Files.readAllBytes(path);

			MultipartFile result = new MockMultipartFile(fileName, fileName, fileName, content); // converting file to multiFile which is required for AWS file upload
			
			if (result.getSize()/1000000 < 11) {
				awsService.uploadFile(result, fileName, id);
				return true;
			}
			else {
				outputfile.delete();
				throw new Exception("File size exceeds memory limit of 10MB");
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;

		} catch (IOException ioe) {
			ioe.printStackTrace();
			return false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}
