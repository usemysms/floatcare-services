package com.java.floatcare.utils;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.java.floatcare.model.Messages;

@Component
public class MessageNotificationCountUtil implements MessageNotificationCountDAO {

	@Autowired
	private ConfigProperties configProperties;

	public List<Messages> getResponseOfConversationScreen(long senderId) {

		RestTemplate restTemplate = new RestTemplate();
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Bearer *************");
		headers.add("content-type", "application/graphql"); // maintain graphql

		// query is a grapql query wrapped into a String

		String query1 = "{\n" + "  findMessagesOfUser(senderId:" + senderId + "){\n" + "    messageId\n"
				+ "    senderId\n" +"text\n"+"receiverId\n"+"timeStamp\n"+ "    isSeen\n" + "  }\n" + "}\n";

		ResponseEntity<String> response = restTemplate.postForEntity(configProperties.getFloatcareServiceGraphqlEndPoint(), new HttpEntity<>(query1, headers), String.class);

		@SuppressWarnings("deprecation")
		JsonObject jsonObject = new JsonParser().parse(response.getBody()).getAsJsonObject();

		JsonObject jsonMessageObject = jsonObject.get("data").getAsJsonObject();
		JsonArray jsonMessagesArray = jsonMessageObject.get("findMessagesOfUser").getAsJsonArray();
		List<Messages> messageList = new ArrayList<>();
		
		for (JsonElement each : jsonMessagesArray) {
			if (each.getAsJsonObject().get("isSeen").getAsBoolean() == false 
					&& each.getAsJsonObject().get("receiverId") != null &&
					each.getAsJsonObject().get("receiverId").getAsLong() == senderId) {

				Messages message = new Messages();
				message.setMessageId(each.getAsJsonObject().get("messageId").getAsLong());
				message.setText(each.getAsJsonObject().get("text").getAsString());
				message.setSenderId(each.getAsJsonObject().get("senderId").getAsLong());
				message.setReceiverId(each.getAsJsonObject().get("receiverId").getAsLong());
				message.setTimeStamp(LocalTime.parse(each.getAsJsonObject().get("timeStamp").getAsString()));
				messageList.add(message);
			}
		}
		return messageList;
	}

	public int getMessageCount (long receiverId) {

		int messageNotificationCount = 0;

		RestTemplate restTemplate = new RestTemplate();

		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Bearer *************");
		headers.add("content-type", "application/graphql"); // maintain graphql

		// query is a grapql query wrapped into a String

		String query1 = "{\n" + "  findMessagesOfUser(senderId:" + receiverId + "){\n" + "    messageId\n" +" receiverId\n"
				+ "    senderId\n" + "    isSeen\n" + "  }\n" + "}\n";

		ResponseEntity<String> response = restTemplate.postForEntity(configProperties.getFloatcareServiceGraphqlEndPoint(), new HttpEntity<>(query1, headers), String.class);

		@SuppressWarnings("deprecation")
		JsonObject jsonObject = new JsonParser().parse(response.getBody()).getAsJsonObject();

		JsonObject jsonMessageObject = jsonObject.get("data").getAsJsonObject();
		JsonArray jsonMessagesArray = jsonMessageObject.get("findMessagesOfUser").getAsJsonArray();

		for (JsonElement each : jsonMessagesArray) {

			if (each.getAsJsonObject().get("isSeen").getAsBoolean() == false && each.getAsJsonObject().get("receiverId") != null && 
					receiverId != each.getAsJsonObject().get("senderId").getAsLong()) {

				messageNotificationCount += 1;
			}
		}
		return messageNotificationCount;
	}
}