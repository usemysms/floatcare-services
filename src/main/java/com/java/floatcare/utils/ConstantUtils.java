package com.java.floatcare.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

import org.joda.time.DateTime;

public class ConstantUtils {
	
	public static final String DAYSHIFT="day";
	public static final String NIGHTSHIFT="night";
	public static final String SWINGSHIFT="swing";
	public static final String FCMServerKey = "AAAAgUfjxM4:APA91bG-AVYicYvEashII-zq7g8R4bkYQhysYBFxBxdPe4LdkSZiRfehia59HUxwSfgEKMnm_lYkbD5xFSZFPr2DxP9gSS6TSPAGfWaZ-MBtmde7d5WGvBAf11bb7yBfSZBYxeY7ZytG";
	
	public static final String 	welcomeProfessional = "d-8dffdcc70a984dd2ab1ed9718ebbf337";
	public static final String assignedSchedule = "d-f083af007888469fb2e81c2d271e5b70";
	public static final String assignedOpenShift = "d-c3b5d1dee75445e0b39ef567206efefd";
	public static final String approvedSchedule = "d-0a521954375d4c0ca4ceec6852aba16f";
	public static final String openShiftInvite = "d-b08c766e684c4ec9a7814efeb11b72b6";
	public static final String flexedOffinvitation = "d-88a68a5d2b8c44218b12979021950e3b";
	
	public static final String branchIODeepLink = "https://floatcare.app.link/JMxxR3PQI8";
	public static final String branchIOLoginPage = "https://floatcare.app.link/mD7Q1u04Ebb";
	public static final String branchIOMySchedule = "https://floatcare.app.link/VKI9qCnJnbb";
	public static final String branchIOOpenShift = "https://floatcare.app.link/YMOfA1gJnbb";
	public static final String branchIOMyRequest = "https://floatcare.app.link/ji0YBS7Inbb";
	
	public static final String lawFirm = "LAW_FIRM";
	public static final String MedicalInstitution = "MEDICAL_INSTITUTION";
	public static final String MedicalFacility = "MEDICAL_FACILITY";
	
	public static final Long Attorney = new Long(15);
	public static final Long CaseManager = new Long(16);
	public  static final long Physician = 1;
	
	public static final int adminUserTypeId = 2;
	
	public static final String AL = "Alabama";	
	public static final String AK = "Alaska";	
	public static final String AZ = "Arizona";	
	public static final String AR = "Arkansas";
	public static final String CA = "California";	
	public static final String CO = "Colorado";	
	public static final String CT = "Connecticut";	
	public static final String DE = "Delaware"; 	
	public static final String FL = "Florida";
	public static final String GA = "Georgia"; 
	public static final String HI = "Hawaii";	
	public static final String ID = "Idaho";
	public static final String IL = "Illinois";	
	public static final String IN = "Indiana";
	public static final String IA = "Iowa";
	public static final String KS = "Kansas";
	public static final String KY = "Kentucky";	
	public static final String LA = "Louisiana";
	public static final String ME = "Maine";
	public static final String MD = "Maryland";
	public static final String MA = "Massachusetts";
	public static final String MI = "Michigan";
	public static final String MN = "Minnesota";
	public static final String MS = "Mississippi"; 
	public static final String MO = "Missouri";
	public static final String MT = "Montana";
	public static final String NE = "Nebraska";
	public static final String NV = "Nevada";
	public static final String NH = "New Hampshire"; 
	public static final String NJ = "New Jersey";
	public static final String NM = "New Mexico";	
	public static final String NY = "New York";
	public static final String NC = "North Carolina";
	public static final String ND = "North Dakota";
	public static final String OH = "Ohio";
	public static final String OK = "Oklahoma";
	public static final String OR = "Oregon";
	public static final String PA = "Pennsylvania";
	public static final String RI = "Rhode Island";	
	public static final String SC = "South Carolina";
	public static final String SD = "South Dakota";
	public static final String TN = "Tennessee";
	public static final String TX = "Texas";
	public static final String UT = "Utah";
	public static final String VT = "Vermont";
	public static final String VA = "Virginia";
	public static final String WA = "Washington";
	public static final String WV = "West Virginia";
	public static final String WI = "Wisconsin";
	public static final String WY = "Wyoming";
	
	public static final String publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCz1zqQHtHvKczHh58ePiRNgOyiHEx6lZDPlvwBTaHmkNlQyyJ06SIlMU1pmGKxILjT7n06nxG7LlFVUN5MkW/jwF39/+drkHM5B0kh+hPQygFjRq81yxvLwolt+Vq7h+CTU0Z1wkFABcTeQQldZkJlTpyx0c3+jq0o47wIFjq5fwIDAQAB";
	public static final String privateKey = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBALPXOpAe0e8pzMeHnx4+JE2A7KIcTHqVkM+W/AFNoeaQ2VDLInTpIiUxTWmYYrEguNPufTqfEbsuUVVQ3kyRb+PAXf3/52uQczkHSSH6E9DKAWNGrzXLG8vCiW35WruH4JNTRnXCQUAFxN5BCV1mQmVOnLHRzf6OrSjjvAgWOrl/AgMBAAECgYAgA0YHdZUFL7mmIvwuE/2+Vh7JVKRAhfM7ILNHQBx7wHkOqro9eWp8mGQhUeDvitWb1C4yizJK0Znkx/pqQtFZuoatUsggocjXFl86FElQwrBp08DvfKfd0bGgy0VTFQVmCtxiqhpAmC7xmXNZXfBD41rl9CKbFfZw05QC5BoQ0QJBAO7LSku97NgFBJQ+vbmVDonuvgnQjVNb7SnwrcpJHEUAGbaVq1a50jz+s6n39TOagASaW6pcY0uwiygYu6xDnkMCQQDAzIGNKFKomTI6djcOyHfQ1ZXqyDQ3guX6nHhzZnNHFF8ZD3fPyyIRSZ3JvPK5iEzJLhB7FRtyWkGcdXgJTWoVAkBfx9zKGqkYUJLwn2XcPWRygPdq2mMFb5bmPqqGu+KB7rNhoBD0nV4tpwALifCpPSxiLEPeRmZxoqN+dsU4KHsfAkAyQt4fK3zpAQ8MGJdf3jkGEzhC/bBHLHPB8pqgEvxIcnIcOWEVpbIa6aMd3Yk1fuftpnmbbLQ8CnWCUUlau3jFAkEAk6bOZIWhTYRwIZcwBdkpyLlbatQFoTTM3i444YutXt3FrFfaWBxge+eYKId+J4dCrt/EmHhSfWKEzHibf6N5Sg==";

	public static String convertDateToWeekDay (DateTime nextDate) {

		String formattedDate = nextDate.getDayOfMonth()+"-"+nextDate.getMonthOfYear()+"-"+nextDate.getYear();
		SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("dd-MM-yyyy");
		Date date = null;
		try {
			date = simpleDateFormat1.parse(formattedDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		DateFormat weekDayFormatter = new SimpleDateFormat("EEEE"); // change the date in weekday
		String weekDay = weekDayFormatter.format(date).toLowerCase();

		return weekDay;
	}

	public static String convertLocalDateToWeekDay (LocalDate nextDate) {

		String formattedDate = nextDate.getDayOfMonth()+"-"+nextDate.getMonthValue()+"-"+nextDate.getYear();
		SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("dd-MM-yyyy");
		Date date = null;
		try {
			date = simpleDateFormat1.parse(formattedDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		DateFormat weekDayFormatter = new SimpleDateFormat("EEEE"); // change the date in weekday
		String weekDay = weekDayFormatter.format(date).toLowerCase();

		return weekDay;
	}
}
