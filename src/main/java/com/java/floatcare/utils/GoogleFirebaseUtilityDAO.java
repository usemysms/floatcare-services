package com.java.floatcare.utils;

public interface GoogleFirebaseUtilityDAO {

	// sending initVector and aesKey
	public void sendMessageNotificationToUser (String userName, long senderId, long receiverId, long groupId, String profilePhoto, String receiverFcmToken, String message, String messageBody, String deepLink,
			String aesKey, String initVector, String userDeviceType); 

	public void sendEventNotificationToUser(long userId, String receiverFcmToken, String message, String messageBody, String deepLink, String userDeviceType, String invitationCode);

	// sending message count
	public void sendNotificationToIosUser(String userName, Long senderId, Long receiverId, String profilePhoto, String receiverFcmToken, String message, 
			String messageBody, String deepLink, String aesKey, String initVector, int badgeCount);
}
