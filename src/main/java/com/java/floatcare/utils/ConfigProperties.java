package com.java.floatcare.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConfigProperties {

	@Value("${floatcare.graphql.endpoint}")
	public String floatcareServiceGraphqlEndPoint;
	
	@Value("${credential.api.endpoint}")
	public String credentialServiceEndPoint;
	
	@Value("${referral.graphql.endpoint}")
	public String referralServiceGraphqlEndPoint;
	
	public String getFloatcareServiceGraphqlEndPoint() {
		return floatcareServiceGraphqlEndPoint;
	}
	public String getCredentialServiceEndPoint() {
		return credentialServiceEndPoint;
	}
	public String getReferralServiceGraphqlEndPoint() {
		return referralServiceGraphqlEndPoint;
	}

	
}
