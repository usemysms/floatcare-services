package com.java.floatcare.utils;

import java.time.ZonedDateTime;
import java.util.List;

import com.java.floatcare.model.UserNotification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import com.java.floatcare.dao.CountersDAO;
import com.java.floatcare.model.Notifications;
import com.java.floatcare.repository.NotificationsRepository;

@Component
public class InAppNotifications {

	@Autowired
	private NotificationsRepository notificationsRepository;
	@Autowired
	private CountersDAO sequence;
	@Autowired
	private MongoTemplate mongoTemplate;

	public void createInAppNotifications(long userId, long notificationTypeId, String notificationHeader, String content, long resourceId, boolean isRead,
			long sourceId) {

		Notifications notifications = new Notifications();

		ZonedDateTimeWriteConverter zonedDateTimeReadConverter = new ZonedDateTimeWriteConverter();

		notifications.setNotificationId(sequence.getNextSequenceValue("notificationId"));
		notifications.setUserId(userId);
		notifications.setNotificationTypeId(notificationTypeId);
		notifications.setNotificationHeader(notificationHeader);
		notifications.setContent(content);
		notifications.setOnDate(zonedDateTimeReadConverter.convert(ZonedDateTime.now()));
		notifications.setResourceId(resourceId);
		notifications.setResourceLink(null);
		notifications.setRead(false);
		notifications.setSourceId(sourceId);

		notificationsRepository.save(notifications);
	}

	public void removeByUserIdAndSourceId(long userId, long sourceId, String content) {

		Query query = new Query();
		query.addCriteria(Criteria.where("userId").is(userId)).addCriteria(Criteria.where("sourceId").is(sourceId)).addCriteria(Criteria.where("notificationHeader").regex(content,"i"));
		mongoTemplate.findAndRemove(query, Notifications.class);
	}

	public void removeNotificationBySourceIdAndContent(long openShiftId, String content) {

		Query query = new Query();
		query.addCriteria(Criteria.where("sourceId").is(openShiftId)).addCriteria(Criteria.where("notificationHeader").regex(content,"i"));
		mongoTemplate.findAllAndRemove(query, Notifications.class);
	}

	public void deleteBySourceId(List<Long> sourceIds) {
		Query query = new Query();
		query.addCriteria(Criteria.where("sourceId").in(sourceIds));
		mongoTemplate.findAllAndRemove(query, Notifications.class);
	}

	public long getUnreadNotificationsCount(long userId) {

		Query query = new Query();
		query.addCriteria(Criteria.where("userId").is(userId));
		query.fields().include("_id");
		System.out.println(mongoTemplate.find(query, UserNotification.class));
		return mongoTemplate.find(query, Notifications.class).stream().count();

	}
}