package com.java.floatcare.utils;

import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

import org.springframework.core.convert.converter.Converter;

public class ZonedDateTimeReadConverter implements Converter<LocalDate, ZonedDateTime> {
    @Override
    public ZonedDateTime convert(LocalDate localDate) {
        return localDate.atStartOfDay(ZoneOffset.UTC);
    }
}