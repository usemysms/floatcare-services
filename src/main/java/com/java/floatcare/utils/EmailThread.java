package com.java.floatcare.utils;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;

import com.java.floatcare.communication.MailUtils;
import com.java.floatcare.communication.SendGridEmailService;

public class EmailThread extends Thread {

	@Autowired
	private MailUtils mailUtil;
	@Autowired
	private SendGridEmailService emailService;
	String dynamicName;
	String departmentName;
	String businessName;
	String message;
	String messageBody;
	String emails;
	String template;
	boolean isTemplate;

	@Override
	public void run() {

		if (this.isTemplate == false)
			mailUtil.sendMail(emails, message, messageBody); // send email
		else
			try {
				emailService.sendHTML(emails, message, messageBody, dynamicName, departmentName, businessName, template);
			} catch (IOException e) {

			}
	}

	public String getDynamicName() {
		return dynamicName;
	}

	public void setDynamicName(String dynamicName) {
		this.dynamicName = dynamicName;
	}

	public String getEmails() {
		return emails;
	}

	public void setEmails(String emails) {
		this.emails = emails;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessageBody() {
		return messageBody;
	}

	public void setMessageBody(String messageBody) {
		this.messageBody = messageBody;
	}

	public boolean isTemplate() {
		return isTemplate;
	}

	public void setTemplate(boolean isTemplate) {
		this.isTemplate = isTemplate;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public SendGridEmailService getEmailService() {
		return emailService;
	}

	public void setEmailService(SendGridEmailService emailService) {
		this.emailService = emailService;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public String getBusinessName() {
		return businessName;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}
}
