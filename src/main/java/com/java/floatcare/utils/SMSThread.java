package com.java.floatcare.utils;

import org.springframework.beans.factory.annotation.Autowired;

import com.java.floatcare.communication.TwilioCommunication;

public class SMSThread extends Thread {

	@Autowired
	private TwilioCommunication twilioCommunication;
	private String messageBody;
	private String phone;

	public void run() {
		try {
			twilioCommunication.sendSMS(phone, messageBody);
		}catch (Exception e) {
			
		}
	}

	public String getMessageBody() {
		return messageBody;
	}

	public void setMessageBody(String messageBody) {
		this.messageBody = messageBody;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

}