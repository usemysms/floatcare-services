package com.java.floatcare.utils;

public class WorkWeekDay {
	
	public static final String MONDAY="monday";
	public static final String TUESDAY="tuesday";
	public static final String WEDNESDAY="wednesday";
	public static final String THURSDAY="thursday";
	public static final String FRIDAY="friday";
	public static final String SATURDAY="saturday";
	public static final String SUNDAY="sunday";
	
}
