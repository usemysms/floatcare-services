package com.java.floatcare.utils;

import java.util.List;

import com.java.floatcare.model.Messages;

public interface MessageNotificationCountDAO {

	public List<Messages> getResponseOfConversationScreen(long senderId);
	
	public int getMessageCount (long receiverId);
}
