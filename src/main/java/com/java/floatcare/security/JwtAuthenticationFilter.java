package com.java.floatcare.security;

import java.io.IOException;
import java.util.Arrays;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.filter.OncePerRequestFilter;

import com.amazonaws.services.kms.model.ExpiredImportTokenException;
import com.java.floatcare.dao.UserBasicInformationRepositoryDAO;
import com.java.floatcare.model.UserBasicInformation;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.SignatureException;

public class JwtAuthenticationFilter extends OncePerRequestFilter {

	@Autowired
	private UserBasicInformationRepositoryDAO userBasicInformationRepository;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;
	
	@Autowired
	private JwtAuthenticationEntryPoint authenticationEntryPoint;
	
	@Override
	protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain)
			throws IOException, ServletException {
		String header = req.getHeader(Constants.HEADER_STRING);
		String username = null;
		String authToken = null;
		/*Enumeration<String> headerNames = req.getHeaderNames();

        if (headerNames != null) {
            while (headerNames.hasMoreElements()) {
                String name = headerNames.nextElement();
                System.out.println("Header: " + name + " value:" + req.getHeader(name));
            }
        }
*/
		//System.out.println("Header: " + header );
		try {
		if (header != null && header.startsWith(Constants.TOKEN_PREFIX)) {
			authToken = header.replace(Constants.TOKEN_PREFIX, "");
			try {
				username = jwtTokenUtil.getUsernameFromToken(authToken);
				//System.out.println("username :"+username);
			} catch (IllegalArgumentException e) {
				logger.error("an error occured during getting username from token", e);
			} catch (ExpiredJwtException e) {
				logger.warn("the token is expired and not valid anymore", e);
				
				req.setAttribute("expired",e.getMessage());
				authenticationEntryPoint.commence(req, res, null);

			} catch (SignatureException e) {
				logger.error("Authentication Failed. Username or Password not valid.");
			}
			catch (ExpiredImportTokenException e) {
				logger.error("Authentication Failed. Username or Password not valid.");
			}
		} else {
			
			//req.setAttribute("expired","Authorization header not found");
			//authenticationEntryPoint.commence(req, res, null);
			//logger.warn("couldn't find bearer string, will ignore the header");
		}
		if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {

			UserBasicInformation userDetail = userBasicInformationRepository.findUserByEmail(username);
			
			if (jwtTokenUtil.validateToken(authToken, userDetail)) {
				UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
						userDetail, null, Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN")));
				authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(req));
				SecurityContextHolder.getContext().setAuthentication(authentication);
			}
		} else {
			/*res.setStatus(HttpStatus.FORBIDDEN.value());
			res.setContentType(MediaType.APPLICATION_JSON_VALUE);
			
			RestResponse restResponse = new RestResponse();
			restResponse.setMessage("Authorization failed.");
			restResponse.setStatus(403);
			restResponse.setSuccess(false);
			restResponse.setData("");
			PrintWriter printWriter = res.getWriter();
			printWriter.format(MediaType.APPLICATION_JSON_VALUE, restResponse);*/
			
		}
		} catch(Exception e) {
			SecurityContextHolder.clearContext();
		}
		chain.doFilter(req, res);
	}
	@Override
	protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
		return new AntPathMatcher().match("/auth/*", request.getServletPath());
	}
}