package com.java.floatcare.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import com.java.floatcare.dao.CountersDAO;
import com.java.floatcare.dao.OpenShiftInviteesDAO;
import com.java.floatcare.model.Businesses;
import com.java.floatcare.model.OpenShiftInvitees;
import com.java.floatcare.model.OpenShifts;
import com.java.floatcare.model.Schedules;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.model.UserNotification;
import com.java.floatcare.repository.BusinessesRepository;
import com.java.floatcare.repository.DepartmentRepository;
import com.java.floatcare.repository.OpenShiftInviteesRepository;
import com.java.floatcare.repository.OpenShiftsRepository;
import com.java.floatcare.repository.SchedulesRepository;
import com.java.floatcare.repository.UserBasicInformationRepository;
import com.java.floatcare.repository.UserNotificationRepository;
import com.java.floatcare.utils.ConstantUtils;
import com.java.floatcare.utils.EmailThread;
import com.java.floatcare.utils.GoogleFirebaseUtilityDAO;
import com.java.floatcare.utils.InAppNotifications;
import com.java.floatcare.utils.SMSThread;
import com.twilio.exception.TwilioException;

@Service
public class OpenShiftServiceImpl implements OpenShiftService {

	@Autowired
	private OpenShiftsRepository openShiftsRepository;
	@Autowired
	private OpenShiftInviteesRepository openShiftInviteesRepository;
	@Autowired
	private OpenShiftInviteesDAO openShiftInviteesDAO;
	@Autowired
	private CountersDAO sequence;
	@Autowired
	private BusinessesRepository businessesRepository;
	@Autowired
	private SchedulesRepository schedulesRepository;
	@Autowired
	private DepartmentRepository departmentRepository;
	@Autowired
	private UserBasicInformationRepository userBasicInformationRepository;
	@Autowired
	private InAppNotifications inAppNotifications;
	@Autowired
	private UserNotificationRepository userNotificationRepository;
	@Autowired
	private GoogleFirebaseUtilityDAO googleFirebaseUtilityDAO;
	@Autowired
	private ApplicationContext applicationContext;

	@Override
	public OpenShifts createOpenShift(long worksiteId, long departmentId, long shiftTypeId, String onDate,
			String startTime, String endTime, long staffUserCoreTypeId, long quota, boolean isUrgent,
			boolean isSendNotifications, boolean isAutoApprove, boolean isAutoClose, boolean isAllowPartial,
			Boolean isAllowOvertime, String note, List<Integer> invitees) throws ParseException {

		// check duplicate
		OpenShifts openShiftFound = openShiftsRepository.findByDepartmentIdAndOnDateAndStaffUserCoreTypeId(departmentId, LocalDate.parse(onDate), staffUserCoreTypeId);
		
		if (openShiftFound == null) {
			
			OpenShifts openShifts = new OpenShifts();
	
			openShifts.setOpenShiftId(sequence.getNextSequenceValue("openShiftId"));
			openShifts.setWorksiteId(worksiteId);
			openShifts.setDepartmentId(departmentId);
			openShifts.setShiftTypeId(shiftTypeId);
	
			if (onDate != null) {
				openShifts.setOnDate(LocalDate.parse(onDate));
			} else
				openShifts.setOnDate(null);
	
			if (startTime != null)
				openShifts.setStartTime(LocalTime.parse(startTime));
			else
				openShifts.setStartTime(null);
	
			if (endTime != null)
				openShifts.setEndTime(LocalTime.parse(endTime));
			else
				openShifts.setEndTime(null);
	
			openShifts.setStaffUserCoreTypeId(staffUserCoreTypeId);
			openShifts.setQuota(quota);
	
			if (isUrgent == true || isUrgent == false)
				openShifts.setUrgent(isUrgent);
	
			if (isSendNotifications == true || isSendNotifications == false)
				openShifts.setSendNotifications(isSendNotifications);
	
			if (isAutoApprove == true || isAutoApprove == true)
				openShifts.setAutoApprove(isAutoApprove);
	
			if (isAutoClose == true || isAutoClose == false)
				openShifts.setAutoClose(isAutoClose);
	
			if (isAllowPartial == true || isAllowPartial == false)
				openShifts.setAllowPartial(isAllowPartial);
	
			if (isAllowOvertime == true || isAllowOvertime == false)
				openShifts.setAllowOvertime(isAllowOvertime);
	
			openShifts.setNote(note);
	
			openShifts.setStatus("Pending");
	
			if (invitees != null)
				openShifts.setInvitees(invitees);
	
			OpenShifts openShift = openShiftsRepository.save(openShifts);
	
			TreeSet<Integer> userIds = new TreeSet<>(invitees);
			
			for (Integer invitee : userIds) {
	
				// create invitees records
				OpenShiftInvitees openShiftInvitee = new OpenShiftInvitees();
	
				openShiftInvitee.setOpenShiftInviteeId(sequence.getNextSequenceValue("openShiftInviteeId"));
				openShiftInvitee.setAccepted(false);
				openShiftInvitee.setApproved(false);
				openShiftInvitee.setOpenShiftId(openShift.getOpenShiftId());
				openShiftInvitee.setUserId(invitee);
	
				openShiftInviteesRepository.save(openShiftInvitee);
	
				// create the notification records for open shift invitation
				Optional<Businesses> worksite = businessesRepository.findById(worksiteId);
	
				SimpleDateFormat stf = new SimpleDateFormat("hh:mm a");
				SimpleDateFormat shf = new SimpleDateFormat("HH:mm");
	
				Date _startTime = shf.parse(startTime);
				Date _endTime = shf.parse(endTime);
				String dateString = LocalDate.parse(onDate).format(DateTimeFormatter.ofPattern("MM-dd-yyyy"));
				String startTimeString = stf.format(_startTime);
				String endTimeString = stf.format(_endTime);

				String departmentName = null;
				String businessName = null;

				try {
					departmentName = departmentRepository.findById(departmentId).get().getDepartmentTypeName();
					businessName = worksite.get().getName();
				}catch(Exception e) {
					departmentName = "";
					businessName = "";
				}

				UserBasicInformation userDetail = userBasicInformationRepository.findUsersByUserId(invitee);
				String message = "Open Shift Invite!";
				String content = "You have invited for open shift in " + worksite.get().getName() + " from " + onDate;
				String messageBody = "You have been invited to work at an open shift in"
						+ departmentName + " @ "+ businessName + " on " + dateString + " " + "" + startTimeString + "-" + endTimeString;

				String deepLink = "9"; // Deep Link for redirecting user to my request screen to see new open shift
				String receiverFcmToken = userDetail.getFcmToken();
				if (receiverFcmToken != null)
					googleFirebaseUtilityDAO.sendEventNotificationToUser(invitee, receiverFcmToken, message, messageBody, deepLink,
							userDetail.getDeviceType(), null); // send Push Notification

				if (isSendNotifications == true) {

					long notificationTypeId = 6;
					inAppNotifications.createInAppNotifications(invitee, notificationTypeId, message, content, 9, false, openShift.getOpenShiftId()); // send inApp Notification
				}
	
				UserNotification userNotificationSetting = userNotificationRepository.findByNotificationTypeIdAndUserId(6, invitee);

				messageBody = messageBody+". Click on the link below to go to the Floatcare application. \n"+ConstantUtils.branchIOOpenShift;

				if (userNotificationSetting != null) {
					if (userNotificationSetting.isEnableEmail() == true) {
						try {
							EmailThread emailThread = new EmailThread();
							emailThread.setEmails(userDetail.getEmail());
							emailThread.setDynamicName(userDetail.getFirstName());
							emailThread.setMessage(message);
							emailThread.setMessageBody(messageBody);
							emailThread.setBusinessName(businessName);
							emailThread.setDepartmentName(departmentName);
							emailThread.setTemplate(ConstantUtils.openShiftInvite);
							emailThread.setTemplate(true);

							applicationContext.getAutowireCapableBeanFactory().autowireBean(emailThread);

							Thread emailthread = new Thread(emailThread); // send email on separate thread.
							
							emailthread.start(); // start the email thread
						}catch (Exception e){
							
						}
					}
					if (userNotificationSetting.isEnableSMS() == true) {
						
						if (userDetail.getPhone() != null) {
							try {
//								twillioCommunication.sendSMS(phoneNumber, messageBody); // send SMS
								SMSThread smsThread = new SMSThread();
								smsThread.setMessageBody(messageBody);
								smsThread.setPhone(userDetail.getPhone());
								
								applicationContext.getAutowireCapableBeanFactory().autowireBean(smsThread);

								Thread smsthread = new Thread(smsThread);
								smsthread.start(); // start the sms thread
							}catch (TwilioException e) {

							}
						}
					}
				}
			}
			return openShifts;
		}else
			return null;
	}

	@Override
	public OpenShifts updateOpenShifts(long userId, long openShiftId, boolean isAccepted, boolean isApproved, String status) throws Exception {

		Optional<OpenShiftInvitees> openShiftInviteesOptional = openShiftInviteesRepository.findByUserIdAndOpenShiftId(userId, openShiftId);

		long totalInvitees = openShiftInviteesRepository.countByOpenShiftIdAndIsApproved(openShiftId, true);

		OpenShiftInvitees inviteeResponse = new OpenShiftInvitees();

		Optional<OpenShifts> openShiftOptional = openShiftsRepository.findById(openShiftId);

		OpenShifts openShift = new OpenShifts();

		if ((!(openShiftOptional.get().getStatus().equals("Closed"))) && openShiftOptional.get().getQuota() >= totalInvitees+1) {

			if (isAccepted == true) { 									// mobile user
				if (openShiftInviteesOptional.isPresent()) {
	
					inviteeResponse = openShiftInviteesOptional.get();
					inviteeResponse.setAccepted(true);
	
					openShiftInviteesRepository.save(inviteeResponse);
				}
				
				if (openShiftOptional.get().isSendNotifications() == true) {
					inAppNotifications.removeByUserIdAndSourceId(openShiftInviteesOptional.get().getUserId(), openShiftOptional.get().getOpenShiftId()
							, "Open Shift Invite!"); // removes the older  inApp notification
				}
			}
	
			if (openShiftOptional.isPresent()) {
	
				if (openShiftId > 0 && status !=null  && userId == 0 && isAccepted == false && isApproved == false) {
					openShift = openShiftOptional.get();
					openShift.setStatus("Closed");

					openShiftsRepository.save(openShift);
				}

				if (status == null && userId > 0 && !(openShiftOptional.get().getStatus().equals("Closed")) 
						&& (isApproved == true || openShiftOptional.get().isAutoApprove() == true || openShiftOptional.get().isAutoClose() == true)) {
					
					if (isApproved == true) {	// dashboard user
						if (openShiftOptional.get().getQuota() == totalInvitees+1) {
							
							openShift = openShiftOptional.get();
							openShift.setStatus("Closed");
							openShiftsRepository.save(openShift);
							
							inAppNotifications.removeNotificationBySourceIdAndContent(openShiftOptional.get().getOpenShiftId(),"Open shift approved");
						}
						
						if (openShiftInviteesOptional.isPresent()) {
	
							inviteeResponse = openShiftInviteesOptional.get();
							inviteeResponse.setApproved(true);
	
							openShiftInviteesRepository.save(inviteeResponse);
								
							openShift = openShiftOptional.get();
	
						}if (openShiftOptional.get().isAutoClose() == true) {

							List<OpenShiftInvitees> invitee = openShiftInviteesDAO.findByOpenShiftIdAndIsApproved(openShiftId, true);

							if (openShiftOptional.get().getQuota() == invitee.size()) {
								
								openShift = openShiftOptional.get();
								openShift.setStatus("Closed");
								openShiftsRepository.save(openShift);
								
								inAppNotifications.removeNotificationBySourceIdAndContent(openShiftOptional.get().getOpenShiftId(),"Open shift approved");
							}
						}
					}else if (openShiftOptional.get().isAutoApprove() == true && isAccepted == true) {

						List<OpenShiftInvitees> openShiftInvitee = openShiftInviteesDAO.findByOpenShiftIdAndIsAccepted(openShiftId, true);
						if (openShiftOptional.get().isAutoClose() == false) {
							if (openShiftOptional.get().getQuota() == openShiftInvitee.size()) {
								inviteeResponse.setApproved(true);
								openShiftInviteesRepository.save(inviteeResponse);
							}else {
								inviteeResponse.setApproved(true);
								openShiftInviteesRepository.save(inviteeResponse);
							}
						} else if (openShiftOptional.get().isAutoClose() == true) {

							if (openShiftOptional.get().getQuota() == openShiftInvitee.size()) {

								inviteeResponse.setApproved(true);
								openShiftInviteesRepository.save(inviteeResponse);

								openShift = openShiftOptional.get();
								openShift.setStatus("Closed");
								openShiftsRepository.save(openShift);
								
							} else {
								inviteeResponse.setApproved(true);
								openShiftInviteesRepository.save(inviteeResponse);
							}
						}
					}

					openShift = openShiftOptional.get();

					Schedules schedules = new Schedules();

					LocalDate onDate = openShift.getOnDate();
					LocalTime startTime = openShift.getStartTime();
					LocalTime endTime = openShift.getEndTime();
		
					String dateString = onDate.format(DateTimeFormatter.ofPattern("MM-dd-yyyy"));
		
					schedules.setScheduleId(sequence.getNextSequenceValue("scheduleId"));
					schedules.setUserId(userId);
					schedules.setDepartmentId(openShift.getDepartmentId());
					schedules.setWorksiteId(openShift.getWorksiteId());
					schedules.setShiftDate(onDate);
					schedules.setStartTime(startTime);
					schedules.setEndTime(endTime);
					schedules.setShiftTypeId(openShift.getShiftTypeId());
					schedules.setEventType("OpenShift");
					schedules.setStatus("Active");
					schedules.setOnCallRequest(false);
		
					schedulesRepository.save(schedules);
		
					UserBasicInformation userDetail = userBasicInformationRepository.findUsersByUserId(userId);

					String departmentName = null;
					String businessName = null;

					try {
						departmentName = departmentRepository.findById(openShift.getDepartmentId()).get().getDepartmentTypeName();
						businessName = businessesRepository.findById(openShift.getWorksiteId()).get().getName();
					}catch(Exception e) {
						departmentName = "";
						businessName = "";
					}
					
					String message = "Open shift approved";
					String messageBody = "Your open shift application at "+ departmentName +" @ "+ businessName+ " on "
							+ dateString + " " + startTime + "-" + endTime + " has been approved.";
					String receiverFcmToken = userDetail.getFcmToken();
					String deepLink = "8"; // Deep Link when supervisor approves the request of user

					if (receiverFcmToken != null)
						googleFirebaseUtilityDAO.sendEventNotificationToUser(userId, receiverFcmToken, message, messageBody, deepLink,
								userDetail.getDeviceType(), null); // send Push Notification

					long notificationTypeId = 6;
					inAppNotifications.createInAppNotifications(userId, notificationTypeId, message, messageBody, 8, false, openShiftId); // send inApp Notification

					UserNotification userNotificationSetting = userNotificationRepository.findByNotificationTypeIdAndUserId(6, userId);

					messageBody = messageBody+". Click on the link below to go to the Floatcare application. \n"+ConstantUtils.branchIOOpenShift;

					if (userNotificationSetting != null) {
		
						if (userNotificationSetting.isEnableEmail() == true) {
							try {
								EmailThread emailThread = new EmailThread();
								emailThread.setEmails(userDetail.getEmail());
								emailThread.setDynamicName(userDetail.getFirstName());
								emailThread.setMessage(message);
								emailThread.setMessageBody(messageBody);
								emailThread.setBusinessName(businessName);
								emailThread.setDepartmentName(departmentName);
								emailThread.setTemplate(ConstantUtils.assignedOpenShift);
								emailThread.setTemplate(true);

								applicationContext.getAutowireCapableBeanFactory().autowireBean(emailThread);

								Thread emailthread = new Thread(emailThread); // send email on separate thread.
								
								emailthread.start(); // start the email thread
							}catch (Exception e){
								
							}
						}
						if (userNotificationSetting.isEnableSMS() == true) {
							
							if (userDetail.getPhone() != null) {
								try {
//									twillioCommunication.sendSMS(phoneNumber, messageBody); // send SMS
									SMSThread smsThread = new SMSThread();
									smsThread.setMessageBody(messageBody);
									smsThread.setPhone(userDetail.getPhone());
									
									applicationContext.getAutowireCapableBeanFactory().autowireBean(smsThread);

									Thread smsthread = new Thread(smsThread);
									smsthread.start(); // start the sms thread
								}catch (TwilioException e) {

								}
							}
						}
					}
				}
			}
			return openShift;
		}
		else
			throw new Exception("Open Shift Closed");
	}

	@Override
	public boolean deleteOpenShift(long openShiftId) {

		Optional<OpenShifts> openShiftsOptional = openShiftsRepository.findById(openShiftId);

		if (openShiftsOptional.isPresent()) {

			openShiftsRepository.deleteById(openShiftId);
			inAppNotifications.deleteBySourceId(Arrays.asList(openShiftId)); // removes users inapp notification
			List<OpenShiftInvitees> openShiftInvitees = openShiftInviteesRepository.findByOpenShiftId(openShiftId);

			if (openShiftInvitees.size() > 0)
				openShiftInviteesRepository.deleteAll(openShiftInvitees);

			return true;
		} else
			return false;
	}
}