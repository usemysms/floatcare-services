package com.java.floatcare.service;

import java.util.List;

import com.java.floatcare.model.AssignmentInvitees;
import com.java.floatcare.model.AssignmentSchedules;

public interface AssignmentSchedulesService {

	List<AssignmentSchedules> createAssignmentSchedules(long assignmentId, List<AssignmentSchedules> assignmentScheduleList);
	
	AssignmentInvitees updateAssignmentInvitee(long assignmentId, long userId, boolean isAccepted, boolean isApproved);
	
	List<AssignmentInvitees> finalizeAssignment (long assignmentId);

	boolean deleteAssignmentScheduleByAssignmentIdAndStaffCoreTypeId(long assignmentId, long staffUserCoreTypeId);

	

}