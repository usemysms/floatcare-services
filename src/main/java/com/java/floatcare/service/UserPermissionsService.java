package com.java.floatcare.service;

import java.util.List;

import com.java.floatcare.api.request.pojo.DepartmentsList;
import com.java.floatcare.api.request.pojo.PermissionSet;
import com.java.floatcare.api.request.pojo.WorksitesList;
import com.java.floatcare.model.UserPermissions;

public interface UserPermissionsService {

	UserPermissions createUserPermissions(long userId, long organizationId, String accessLevel, List<Long> organizations, List<WorksitesList> worksitesIds,
			List<DepartmentsList> departmentsIds, List<PermissionSet> permissionSet);

}