package com.java.floatcare.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java.floatcare.dao.CountersDAO;
import com.java.floatcare.dao.UserPrivacySettingsDAO;
import com.java.floatcare.model.UserPrivacySettings;
import com.java.floatcare.repository.UserPrivacySettingsRepository;

@Service
public class UserPrivacySettingsServiceImpl implements UserPrivacySettingsService{

	@Autowired
	private UserPrivacySettingsRepository userPrivacySettingsRepository;
	@Autowired
	private CountersDAO sequence;
	@Autowired
	private UserPrivacySettingsDAO userPrivacySettingsDAO;
	
	@Override
	public UserPrivacySettings createUserPrivacySettings(long userId,
			boolean isVisibleToPublic, boolean isVisibleToWorkspace, boolean isVisibleToCollegues) {
			
			List<UserPrivacySettings> user = userPrivacySettingsDAO.findUserPrivacySettingsByUserId(userId);
			UserPrivacySettings userPrivacySettings = new UserPrivacySettings();

			if (user.size() != 0) {
				userPrivacySettings.setUserPrivacySettingId((user.get(0).getUserPrivacySettingId()));
				// userNotificationRepository.deleteAll(user);
			}
			else
				userPrivacySettings.setUserPrivacySettingId(sequence.getNextSequenceValue("userNotificationId"));
				userPrivacySettings.setUserId(userId);
				userPrivacySettings.setVisibleToCollegues(isVisibleToCollegues);
				userPrivacySettings.setVisibleToPublic(isVisibleToPublic);
				userPrivacySettings.setVisibleToWorkspace(isVisibleToWorkspace);
			
				userPrivacySettingsRepository.save(userPrivacySettings);
			
				return userPrivacySettings;
			
	}
}
