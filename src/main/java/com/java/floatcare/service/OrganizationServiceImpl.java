package com.java.floatcare.service;

import java.io.File;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java.floatcare.dao.CountersDAO;
import com.java.floatcare.exceptions.OrganizationEmailAlreadyExist;
import com.java.floatcare.model.Businesses;
import com.java.floatcare.model.Organizations;
import com.java.floatcare.model.UserOrganizations;
import com.java.floatcare.model.UserWorkspaces;
import com.java.floatcare.repository.BusinessesRepository;
import com.java.floatcare.repository.DepartmentRepository;
import com.java.floatcare.repository.OrganizationRepository;
import com.java.floatcare.repository.UserOrganizationsRepository;
import com.java.floatcare.repository.UserWorkspacesRepository;
import com.java.floatcare.utils.UploadImageDAO;

@Service
public class OrganizationServiceImpl implements OrganizationService {

	@Autowired
	private CountersDAO sequence;

	@Autowired
	private OrganizationRepository organizationRepository;

	@Autowired
	private BusinessesRepository businessRepository;

	@Autowired
	private UserWorkspacesRepository userWorkspacesRepository;
	
	@Autowired
	private DepartmentRepository departmentRepository;

	@Autowired
	private UploadImageDAO setProfileImage;
	
	@Autowired
	private UserOrganizationsRepository userOrganizationsRepository;

	private String uniqueFileName;

	public String getUniqueFileName() {
		return uniqueFileName;
	}

	public void setUniqueFileName(String uniqueFileName) {
		this.uniqueFileName = uniqueFileName;
	}

	public Organizations createOrganization(String name, String organizationType, String address, String address2, String country, String logo, String status,
			String state, String license, String email, String phoneNumber, String city, String postalCode)
			throws Exception {

		Organizations organization = new Organizations();

		if (name != null)
			if (!(name.length() <= 1))
				organization.setName(name);
			else
				organization.setName(null);
		if (organizationType != null)
			if (!(organizationType.length() <= 1))
				organization.setOrganizationType(organizationType);
			else
				organization.setOrganizationType(organizationType);

		if (address != null)
			if (!(address.length() <= 1))
				organization.setAddress(address);
			else
				organization.setAddress(null);
		
		if (address2 != null)
			if (!(address2.length() <= 1))
				organization.setAddress2(address2);
			else
				organization.setAddress2(null);

		if (country != null)
			if (!(country.length() <= 1))
				organization.setCountry(country);
			else
				organization.setCountry(null);

		if (logo != null) {
		      if (logo.length() > 0) {
				String fileName = name + "_" + LocalDate.now().toString() + ".png";
				boolean check = setProfileImage.uploadImageFile(logo, fileName, 1);
	
				if (check) {
					organization.setLogo(uniqueFileName);
					File fileToDelete = FileUtils.getFile(fileName);
					fileToDelete.delete();
				} //else
					//System.out.println("File could not get uploaded");
		      } 
		}else
			organization.setLogo(null);

		if (status != null)
			if (!(status.length() <= 1))
				organization.setStatus(status);
			else
				organization.setStatus(null);

		if (state != null)
			if (!(state.length() <= 1))
				organization.setState(state);
			else
				organization.setState(null);

		if (license != null)
			if (!(license.length() <= 1))
				organization.setLicense(license);
			else
				organization.setLicense(null);

		if (phoneNumber != null)
			if (!((phoneNumber.length() <= 1)))
				organization.setPhoneNumber(phoneNumber);
			else
				organization.setPhoneNumber(null);

		if (city != null)
			if (!(city.length() <= 1))
				organization.setCity(city);
			else
				organization.setCity(null);

		if (postalCode != null)
			if (!(postalCode.length() <= 1))
				organization.setPostalCode(postalCode);
			else
				organization.setPostalCode(null);
		
		organization.setCreatedDate(LocalDate.now());

		//Optional<Organizations> optEmail = organizationRepository.findOrganizationByEmail(email);
		//if (optEmail.isPresent())
		//	throw new OrganizationEmailAlreadyExist("Organization email already exists.");

		 if (name == null)
			throw new OrganizationEmailAlreadyExist("Organization mandatory fields are missing");
		else {
			if (email!=null)
				organization.setEmail(email);

			organization.setOrganizationId(sequence.getNextSequenceValue("organizationId"));
			organizationRepository.save(organization);
		}

		return organization;
	}

	@Override
	public Organizations updateOrganization(long organizationId, String organizationType, String name, String address, String address2, String country,
			String logo, String status, String state, String license, String email, String phoneNumber, String city,
			String postalCode, long ownerId) {

		Optional<Organizations> optionalOrganization = organizationRepository.findById((organizationId));
		// Optional<Organizations> optEmail =
		// organizationRepository.findOrganizationByEmail(email);

		if (optionalOrganization.isPresent()) {
			Organizations theOrganization = optionalOrganization.get();

			theOrganization.setOrganizationId(organizationId);

			if (name != null)
				theOrganization.setName(name);
			
			if (organizationType != null)
					theOrganization.setOrganizationType(organizationType);
			
			if (country != null)
				theOrganization.setCountry(country);
			if (address != null)
				theOrganization.setAddress(address);
			if (address2 != null)
				theOrganization.setAddress2(address2);
			if (logo != null) {
				String fileName = optionalOrganization.get().getName().replace(" ", "_") + "_" + LocalDate.now().toString() + ".png";
				boolean check = false;
				try {
					check = setProfileImage.uploadImageFile(logo, fileName, 1);
				} catch (Exception e) {

					//System.out.println("File did not get uploaded");
				}
				if (check) {
					theOrganization.setLogo(uniqueFileName);
					File fileToDelete = FileUtils.getFile(fileName);
					fileToDelete.delete();
				} //else
					//System.out.println("File could not get uploaded");
			}

			if (status != null)
				theOrganization.setStatus(status);
			if (state != null)
				theOrganization.setState(state);
			if (license != null)
				theOrganization.setLicense(license);
			if (email != null)
				theOrganization.setEmail(email);
			/*
			 * if(optEmail.isPresent() == false) { if (email != null)
			 * theOrganization.setEmail(email); } else throw new
			 * Exception("Organization Email Already Exist");
			 */
			if (phoneNumber != null)
				theOrganization.setPhoneNumber(phoneNumber);
			if (city != null)
				theOrganization.setCity(city);
			if (postalCode != null)
				theOrganization.setPostalCode(postalCode);
			if (ownerId > 0)
				theOrganization.setOwnerId(ownerId);

			organizationRepository.save(theOrganization);

			return theOrganization;
		} else {
			//System.out.println("Organization is not present.");
			return null;
		}

	}

	@Override
	public boolean deleteOrganization(long organizationId) {

		List<Businesses> businesses = businessRepository.findBusinessesByOrganizationId(organizationId);
		
		for (Businesses business : businesses) {
			
			departmentRepository.deleteByBusinessId(business.getBusinessId()); //delete department
			
			businessRepository.deleteById(business.getBusinessId()); //delete business
			
		}
		
		List<UserWorkspaces> userWorkspaces = userWorkspacesRepository.findByOrganizationIdAndStatus(organizationId, "Active");
		
		for (UserWorkspaces eachWorkspace: userWorkspaces) {
			
			eachWorkspace.setStatus("Deleted"); //set each userworkspace status
			userWorkspacesRepository.save(eachWorkspace);
		}
		
		List<UserOrganizations> userOrganization = userOrganizationsRepository.findByOrganizationIdAndStatus(organizationId, "Active");
		
		if (userOrganization .size() > 0) {
			
			for (UserOrganizations eachOrganization :userOrganization) {
				
				eachOrganization.setStatus("Deleted"); //set userOrganization status
				
				userOrganizationsRepository.save(eachOrganization);
			}
		}
		
		organizationRepository.deleteById(organizationId);
		
		return true;
	}
}
