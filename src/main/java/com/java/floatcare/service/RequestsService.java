package com.java.floatcare.service;

import java.util.List;

import com.java.floatcare.model.Requests;

public interface RequestsService {

	Requests createARequest(long worksiteId, long requestTypeId, long departmentId, long userId, List<String> onDate, List<Long> shiftId, long collegueId, String notes);

	Requests updateARequest(long requestId, long worksiteId, long departmentId, long requestTypeId, long userId, List<String> onDate, List<Long> shiftId, long collegueId,
		String notes, String status) throws Exception;

	boolean deleteARequest(long requestId);

}
