package com.java.floatcare.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.java.floatcare.dao.CountersDAO;
import com.java.floatcare.model.DepartmentSettings;
import com.java.floatcare.repository.DepartmentSettingsRepository;

@Component
public class DepartmentSettingsServiceImpl implements DepartmentSettingsService {

	@Autowired
	private CountersDAO sequence;
	@Autowired
	private DepartmentSettingsRepository departmentSettingsRepository;

	@Override
	public DepartmentSettings createDepartmentSettings(long departmentId, String customName, String workWeekDay, String workPayPeriod, long workPayPeriodHours) {

		DepartmentSettings optionalDepartmentSettings = departmentSettingsRepository.findByDepartmentId(departmentId); 
			
		if (optionalDepartmentSettings == null) {
			
			DepartmentSettings departmentSettings = new DepartmentSettings();
	
			departmentSettings.setDepartmentSettingId(sequence.getNextSequenceValue("departmentSettingId"));
	
			departmentSettings.setDepartmentId(departmentId);
			
			if (customName != null)
				departmentSettings.setCustomName(customName);
			else
				departmentSettings.setCustomName(null);
	
			if (workWeekDay != null)
				departmentSettings.setWorkWeekDay(workWeekDay);
			else
				departmentSettings.setWorkWeekDay(null);
	
			if (workPayPeriod != null)
				departmentSettings.setWorkPayPeriod(workPayPeriod);
			else
				departmentSettings.setWorkPayPeriod(null);
	
			if (workPayPeriodHours > 0)
				departmentSettings.setWorkPayPeriodHours(workPayPeriodHours);
			
			departmentSettings.setRestPeriod("30 min");
	
			departmentSettingsRepository.save(departmentSettings);
	
			return departmentSettings;
		}
		else {
			DepartmentSettings departmentSettings = optionalDepartmentSettings;

			if (customName != null)
				departmentSettings.setCustomName(customName);
				
			if (workWeekDay != null)
				departmentSettings.setWorkWeekDay(workWeekDay);
			
			if (workPayPeriod != null)
				departmentSettings.setWorkPayPeriod(workPayPeriod);
			
			if (workPayPeriodHours > 0)
				departmentSettings.setWorkPayPeriodHours(workPayPeriodHours);
			
			departmentSettingsRepository.save(departmentSettings);
	
			return departmentSettings;
		}
			
	}

	@Override
	public DepartmentSettings updateDepartmentSettings(long departmentSettingId, long departmentId, String customName, String workWeekDay,
			String workPayPeriod, Long workPayPeriodHours) throws Exception {
		
		Optional<DepartmentSettings> departmentSettingsOptional = departmentSettingsRepository.findById(departmentSettingId);
		
		if (departmentSettingsOptional.isPresent()) {
			
			DepartmentSettings departmentSettings = departmentSettingsOptional.get();

			if (customName != null)
				departmentSettings.setCustomName(customName);
				
			if (workWeekDay != null)
				departmentSettings.setWorkWeekDay(workWeekDay);
			
			if (workPayPeriod != null)
				departmentSettings.setWorkPayPeriod(workPayPeriod);
			
			if (workPayPeriodHours > 0)
				departmentSettings.setWorkPayPeriodHours(workPayPeriodHours);
			
			departmentSettingsRepository.save(departmentSettings);
	
			return departmentSettings;
		}
		throw new Exception("DepartmentSettingId does not exist");
	}
}
