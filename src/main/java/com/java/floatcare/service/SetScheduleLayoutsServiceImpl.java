	package com.java.floatcare.service;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.java.floatcare.dao.CountersDAO;
import com.java.floatcare.dao.SetScheduleLayoutDAO;
import com.java.floatcare.dao.UserWorkspacesDAO;
import com.java.floatcare.model.Businesses;
import com.java.floatcare.model.Department;
import com.java.floatcare.model.SetScheduleLayouts;
import com.java.floatcare.model.StaffWeekOffs;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.model.UserWorkspaces;
import com.java.floatcare.repository.BusinessesRepository;
import com.java.floatcare.repository.DepartmentRepository;
import com.java.floatcare.repository.StaffWeekOffsRepository;
import com.java.floatcare.repository.UserBasicInformationRepository;
import com.java.floatcare.utils.ConstantUtils;
import com.java.floatcare.utils.EmailThread;
import com.java.floatcare.utils.GoogleFirebaseUtilityDAO;
import com.java.floatcare.utils.InAppNotifications;

import graphql.GraphQLException;

@Service
public class SetScheduleLayoutsServiceImpl implements SetScheduleLayoutsService {

	@Autowired
	private CountersDAO sequence;
	@Autowired
	private SetScheduleLayoutDAO setScheduleLayoutDAO;
	@Autowired
	private MongoTemplate mongoTemplate;
	@Autowired
	private UserWorkspacesDAO userWorkspacesDAO;
	@Autowired
	private GoogleFirebaseUtilityDAO flashNotification;
	@Autowired
	private UserBasicInformationRepository userRepository;
	@Autowired
	private ApplicationContext applicationContext;
	@Autowired
	private DepartmentRepository departmentRepository;
	@Autowired
	private BusinessesRepository businessesRepository;
	@Autowired
	private InAppNotifications inAppNotifications;
	@Autowired
	private StaffWeekOffsRepository staffWeekOffsRepository;
	

	@Override
	public SetScheduleLayouts createSetScheduleLayout(long worksiteId, long departmentId, String scheduleLength,
			String startDate, List<Long> roles, List<Long> shiftTypes, List<String> weekOffDays, List<StaffWeekOffs> staffWeekOffs) {
		
		SetScheduleLayouts setScheduleLayouts = new SetScheduleLayouts();
		
		if (staffWeekOffs.stream().map(emitter->emitter.getUserId()).collect(Collectors.toList()).size() > 0) {

			if (worksiteId > 0)
				setScheduleLayouts.setWorksiteId(worksiteId);
			
			if (departmentId > 0)
				setScheduleLayouts.setDepartmentId(departmentId);
			
			if (scheduleLength != null && scheduleLength.equalsIgnoreCase("INF"))
				setScheduleLayouts.setScheduleLength("INF");
			else if (scheduleLength != null && !(scheduleLength.equalsIgnoreCase("INF")))
				setScheduleLayouts.setScheduleLength(scheduleLength);
			else
				setScheduleLayouts.setScheduleLength(null);
			
			if (startDate != null) {
				if (startDate.length() > 0)
					setScheduleLayouts.setStartDate(LocalDate.parse(startDate));
			}
			else
				setScheduleLayouts.setStartDate(null);
			
			if (roles != null && roles.size() > 0)
				setScheduleLayouts.setRoles(roles);
			else
				setScheduleLayouts.setRoles(null);
			
			if (shiftTypes.size() > 0)
				setScheduleLayouts.setShiftTypes(shiftTypes);
			else
				setScheduleLayouts.setShiftTypes(null);
			
			if (weekOffDays != null && weekOffDays.size() > 0)
				setScheduleLayouts.setWeekOffDays(weekOffDays);
			else
				setScheduleLayouts.setWeekOffDays(null);
			
			setScheduleLayouts.setCreatedOn(LocalDate.now());
			
			if (scheduleLength != null) {
				
				if (scheduleLength.equalsIgnoreCase("INF"))
					setScheduleLayouts.setEndDate(null);
	
				else {
					String[] splitScheduleLength = scheduleLength.split("(?<=\\d)(?=\\D)");
		
					if (splitScheduleLength[1].equalsIgnoreCase("w")) {
						int number = Integer.parseInt(splitScheduleLength[0]);
						setScheduleLayouts.setEndDate(LocalDate.parse(startDate).plusWeeks(number).minusDays(1));
					}
					else if (splitScheduleLength[1].equalsIgnoreCase("m")) {
						int number = Integer.parseInt(splitScheduleLength[0]);
						setScheduleLayouts.setEndDate(LocalDate.parse(startDate).plusMonths(number).minusDays(1));
					}
				}
			}
			
			setScheduleLayouts.setStatus("active");
			setScheduleLayouts.setSetScheduleLayoutId(sequence.getNextSequenceValue("setScheduleLayoutId"));
			
			//save StaffWeekOff objects
			System.out.println(staffWeekOffs);
			for (StaffWeekOffs eachStaffWeekOff: staffWeekOffs) {
				
				StaffWeekOffs staffWeekOff = new StaffWeekOffs();
				
				staffWeekOff.setStaffWeekOffId(sequence.getNextSequenceValue("staffWeekOffId"));
				staffWeekOff.setWorksiteId(worksiteId);
				staffWeekOff.setDepartmentId(departmentId);
				staffWeekOff.setSetScheduleLayoutId(setScheduleLayouts.getSetScheduleLayoutId());
				staffWeekOff.setUserId(eachStaffWeekOff.getUserId());
				staffWeekOff.setStaffSetSchedules(eachStaffWeekOff.getStaffSetSchedules());
				//staffWeekOff.setStaffWeekOffDays(eachStaffWeekOff.getStaffWeekOffDays());
				staffWeekOff.setSetScheduleWeekOffDays(weekOffDays);
				System.out.println(eachStaffWeekOff.getStaffSetSchedules());
				staffWeekOffsRepository.save(staffWeekOff);
	
				String message = "Set schedule published";
				String messageBody = "Set Schedule has been published for ";
				String deepLink = "7";  // deep link for redirecting user to schedule/calendar screen
				
				try {
					Optional<Department> department = departmentRepository.findById(departmentId);
					Optional<Businesses> businesses = businessesRepository.findById(department.get().getBusinessId());
					
					String departmentName = department.get().getDepartmentTypeName();
					String businessName = businesses.get().getName();
					
					messageBody = messageBody + departmentName+", "+businessName;
				}catch(Exception e) {
					messageBody = "Set Schedule has been deleted in the hospital";
				}

				UserBasicInformation userDetails = userRepository.findById(eachStaffWeekOff.getUserId()).get();

				if (userDetails.getFcmToken() != null) // send push notification.
					flashNotification.sendEventNotificationToUser(eachStaffWeekOff.getUserId(), userDetails.getFcmToken(), message, messageBody, deepLink,
							userDetails.getDeviceType(), null);

				inAppNotifications.createInAppNotifications(eachStaffWeekOff.getUserId(), 6, message, messageBody, Long.parseLong(deepLink), false, setScheduleLayouts.getSetScheduleLayoutId());

				messageBody = messageBody+". Click on the link below to see the changes in the Floatcare Application. \n"+ ConstantUtils.branchIOMySchedule;

				EmailThread emailThread = new EmailThread();
				emailThread.setEmails(userDetails.getEmail());
				emailThread.setMessage(message);
				emailThread.setMessageBody(messageBody);
				applicationContext.getAutowireCapableBeanFactory().autowireBean(emailThread);
				Thread thread = new Thread(emailThread); // send email on separate thread.
				thread.start(); // start the thread
			}

			mongoTemplate.save(setScheduleLayouts, "set_schedule_layouts");
			
			return setScheduleLayouts;
		}else
			return setScheduleLayouts;
	}

	@Override
	public SetScheduleLayouts updateSetScheduleLayout(long setScheduleLayoutId, long worksiteId, long departmentId,
			String scheduleLength, String startDate, List<Long> roles, List<Long> shiftTypes, List<String> weekOffDays, 
			List<StaffWeekOffs> staffWeekOffs) throws Exception {
			
			SetScheduleLayouts setScheduleLayoutsOptional = setScheduleLayoutDAO.findBySetScheduleLayoutId(setScheduleLayoutId);
			
			if (setScheduleLayoutsOptional != null) {
			
				SetScheduleLayouts setScheduleLayouts = setScheduleLayoutsOptional;
				
				if (worksiteId > 0)
					setScheduleLayouts.setWorksiteId(worksiteId);
				
				if (departmentId > 0)
					setScheduleLayouts.setDepartmentId(departmentId);
				
				if (scheduleLength != null && scheduleLength.equalsIgnoreCase("INF"))
					setScheduleLayouts.setScheduleLength("INF");
	
				else
					setScheduleLayouts.setScheduleLength(scheduleLength);
				
				if (startDate != null) {
					if (startDate.length() > 0)
						setScheduleLayouts.setStartDate(LocalDate.parse(startDate));
				}
				
				if (roles != null) {
					if (roles.size() > 0) {
						setScheduleLayoutsOptional.getRoles().clear();
						setScheduleLayouts.setRoles(roles);
					}
				}
				
				if (shiftTypes != null) {
					if (shiftTypes.size() > 0) {
						setScheduleLayoutsOptional.getShiftTypes().clear();
						setScheduleLayouts.setShiftTypes(shiftTypes);
					}
				}
				
				if (weekOffDays != null) {
					
					setScheduleLayoutsOptional.getWeekOffDays().clear();
					setScheduleLayouts.setWeekOffDays(weekOffDays);
				}
				
				if (scheduleLength != null) {
					
					if (scheduleLength.equalsIgnoreCase("INF"))
						setScheduleLayouts.setEndDate(null);
					else {
						String[] splitScheduleLength = scheduleLength.split("(?<=\\d)(?=\\D)");
		
						if (splitScheduleLength[1].equalsIgnoreCase("w")) {
							int number = Integer.parseInt(splitScheduleLength[0]);
							setScheduleLayouts.setEndDate(LocalDate.parse(startDate).plusWeeks(number).minusDays(1));
						}
						else if (splitScheduleLength[1].equalsIgnoreCase("m")) {
							int number = Integer.parseInt(splitScheduleLength[0]);
							setScheduleLayouts.setEndDate(LocalDate.parse(startDate).plusMonths(number).minusDays(1));
						}
					}
				}

				//update with the new data
				departmentId = setScheduleLayoutsOptional.getDepartmentId();
				worksiteId = setScheduleLayoutsOptional.getDepartmentId();
				
				String message = "Set schedule updated";
				String messageBody = "Set schedule has been updated for ";
				String deepLink = "7";  // deep link for redirecting user to schedule/calendar screen
				
				try {
					Optional<Department> department = departmentRepository.findById(departmentId);
					Optional<Businesses> businesses = businessesRepository.findById(department.get().getBusinessId());
					
					String departmentName = department.get().getDepartmentTypeName();
					String businessName = businesses.get().getName();

					messageBody = messageBody + departmentName+", "+businessName;
				}catch(Exception e) {
					messageBody = "Set Schedule has been updated in the hospital";
				}

				List<UserWorkspaces> users = userWorkspacesDAO.findUserWorkspacesByStaffUserCoreTypeIdsAndDepartmentIdAndShiftIds(roles,departmentId,shiftTypes);
				List<Long> userIds = users.stream().map(UserWorkspaces::getUserId).collect(Collectors.toList());
				List<Long> userIdsInStaffWeekOffs = staffWeekOffs.stream().map(StaffWeekOffs::getUserId).collect(Collectors.toList());
				
                if (staffWeekOffs != null && staffWeekOffs.size() > 0)
                    mongoTemplate.findAllAndRemove(Query.query(Criteria.where("setScheduleLayoutId").is(setScheduleLayoutId)), StaffWeekOffs.class);
				
                // add the user from staff Week Offs data
                
				if (staffWeekOffs != null && staffWeekOffs.size() > 0) {
					
					for (StaffWeekOffs eachStaffWeekOff: staffWeekOffs) {

						if (userIds.contains(eachStaffWeekOff.getUserId())) {

							StaffWeekOffs staffWeekOff = new StaffWeekOffs();

							staffWeekOff.setWorksiteId(worksiteId);
							staffWeekOff.setDepartmentId(setScheduleLayouts.getDepartmentId());
							staffWeekOff.setSetScheduleLayoutId(setScheduleLayouts.getSetScheduleLayoutId());
							staffWeekOff.setUserId(eachStaffWeekOff.getUserId());
							staffWeekOff.setStaffSetSchedules(eachStaffWeekOff.getStaffSetSchedules());
							staffWeekOff.setSetScheduleWeekOffDays(weekOffDays);
							staffWeekOff.setStaffWeekOffId(sequence.getNextSequenceValue("staffWeekOffId"));
							
							mongoTemplate.save(staffWeekOff, "staff_week_offs");
							
							Optional<UserBasicInformation> userDetails = userRepository.findByUserIdAndStatus(staffWeekOff.getUserId(), "Active");

							if (userDetails.get().getFcmToken() != null) // send push notification.
								flashNotification.sendEventNotificationToUser(staffWeekOff.getUserId(), userDetails.get().getFcmToken(),
										message, messageBody, deepLink, userDetails.get().getDeviceType(), null);

							inAppNotifications.deleteBySourceId(Arrays.asList(setScheduleLayoutId)); // remove older notification based on setScheduleLayoutId
							inAppNotifications.createInAppNotifications(staffWeekOff.getUserId(), 6, message, messageBody, Long.parseLong(deepLink), false,
									setScheduleLayoutId);

							messageBody = messageBody+". Click on the link below to see the changes in the Floatcare Application. \n"+ ConstantUtils.branchIOMySchedule;
							
							EmailThread emailThread = new EmailThread();
							emailThread.setEmails(userDetails.get().getEmail());
							emailThread.setMessage(message);
							emailThread.setMessageBody(messageBody);
							applicationContext.getAutowireCapableBeanFactory().autowireBean(emailThread);
							Thread thread = new Thread(emailThread); // send email on separate thread.
							thread.start(); // start the thread
						}
					}
				}
				
				//add the user which does not have staff week offs data
				for (UserWorkspaces eachUser : users) {

					if (!userIdsInStaffWeekOffs.contains(eachUser.getUserId()) && shiftTypes.containsAll(eachUser.getDepartmentShiftIds())) {
					
						StaffWeekOffs staffWeekOff = new StaffWeekOffs();
	
						staffWeekOff.setWorksiteId(worksiteId);
						staffWeekOff.setDepartmentId(departmentId);
						staffWeekOff.setSetScheduleLayoutId(setScheduleLayouts.getSetScheduleLayoutId());
						staffWeekOff.setUserId(eachUser.getUserId());
						staffWeekOff.setStaffSetSchedules(null);
						staffWeekOff.setSetScheduleWeekOffDays(null);
						staffWeekOff.setStaffWeekOffId(sequence.getNextSequenceValue("staffWeekOffId"));
	
						mongoTemplate.save(staffWeekOff, "staff_week_offs");
						
						Optional<UserBasicInformation> userDetails = userRepository.findByUserIdAndStatus(eachUser.getUserId(), "Active");

						if (userDetails.isPresent() && userDetails.get().getFcmToken() != null) // send push notification.
							flashNotification.sendEventNotificationToUser(eachUser.getUserId(), userDetails.get().getFcmToken(), message, messageBody, deepLink,
									userDetails.get().getDeviceType(), null);

						inAppNotifications.deleteBySourceId(Arrays.asList(setScheduleLayoutId));
						inAppNotifications.createInAppNotifications(eachUser.getUserId(), 6, message, messageBody, Long.parseLong(deepLink), false,
								setScheduleLayoutId);

						if (!messageBody.contains(ConstantUtils.branchIOMySchedule))
							messageBody = messageBody+". Click on the link below to see the changes in the Floatcare Application. \n"+ ConstantUtils.branchIOMySchedule;

						EmailThread emailThread = new EmailThread();
						emailThread.setEmails(userDetails.get().getEmail());
						emailThread.setMessage(message);
						emailThread.setMessageBody(messageBody);
						applicationContext.getAutowireCapableBeanFactory().autowireBean(emailThread);
						Thread thread = new Thread(emailThread); // send email on separate thread.
						thread.start(); // start the thread
					}
				}
				
				setScheduleLayouts.setUpdatedOn(LocalDate.now());
	
				mongoTemplate.save(setScheduleLayouts, "set_schedule_layouts");
				
				return setScheduleLayouts;
			}
			else
				throw new GraphQLException("Set Schedule LayoutId is not present");
		}

	@Override
	public boolean deleteSetScheduleLayout(long setScheduleLayoutId) {
		
		String message = "Set schedule deleted";
		String messageBody = "Set Schedule has been deleted for ";
		String deepLink = "7";  // deep link for redirecting user to schedule/calendar screen
		
		SetScheduleLayouts setScheduleLayout = mongoTemplate.findOne(new Query().addCriteria(Criteria.where("_id").is(setScheduleLayoutId)), SetScheduleLayouts.class);
		
		try {
			Optional<Department> department = departmentRepository.findById(setScheduleLayout.getDepartmentId());
			Optional<Businesses> businesses = businessesRepository.findById(department.get().getBusinessId());
			
			String departmentName = department.get().getDepartmentTypeName();
			String businessName = businesses.get().getName();

			messageBody = messageBody+""+departmentName+" in "+businessName;
		}catch(Exception e) {
			messageBody = "Set Schedule has been deleted in a hospital";
		}
		List<Long> staffWeekOffs = mongoTemplate.find(new Query().addCriteria(Criteria.where("setScheduleLayoutId").is(setScheduleLayoutId)), StaffWeekOffs.class)
				.stream().map(e -> e.getUserId()).collect(Collectors.toList());

		for (Long eachUser : staffWeekOffs) {
			
			try {
				Optional<UserBasicInformation> userDetails = userRepository.findByUserIdAndStatus(eachUser, "Active");

				if (userDetails.get().getFcmToken() != null) // send push notification.
					flashNotification.sendEventNotificationToUser(eachUser, userDetails.get().getFcmToken(), message, messageBody, deepLink,
							userDetails.get().getDeviceType(), null);

				inAppNotifications.deleteBySourceId(Arrays.asList(setScheduleLayoutId)); // remove older notification based on setScheduleLayoutId
				inAppNotifications.createInAppNotifications(eachUser, 6, message, messageBody, Long.parseLong(deepLink), false, setScheduleLayoutId);

				messageBody = messageBody+". Click on the link below to see the changes in the Floatcare Application. \n"+ ConstantUtils.branchIOMySchedule;

				EmailThread emailThread = new EmailThread();
				emailThread.setEmails(userDetails.get().getEmail());
				emailThread.setMessage(message);
				emailThread.setMessageBody(messageBody);
				applicationContext.getAutowireCapableBeanFactory().autowireBean(emailThread);
				Thread thread = new Thread(emailThread); // send email on separate thread.
				thread.start(); // start the thread

			}catch (Exception e) {
				continue;
			}
		}

		mongoTemplate.findAllAndRemove(Query.query(Criteria.where("_id").is(setScheduleLayoutId)), SetScheduleLayouts.class);
		mongoTemplate.findAllAndRemove(Query.query(Criteria.where("setScheduleLayoutId").is(setScheduleLayoutId)), StaffWeekOffs.class);
		return true;
	}

}
