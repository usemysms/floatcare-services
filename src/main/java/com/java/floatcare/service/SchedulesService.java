package com.java.floatcare.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.java.floatcare.model.Schedules;

@Service
public interface SchedulesService {

	Schedules createASchedule(long worksiteId, long departmentId, long userId, long shiftTypeId, String shiftDate,
			String startTime, String endTime, boolean isOnCallRequest);
	
	Schedules updateASchedule(long scheduleId, long worksiteId, long departmentId, long userId, long shiftTypeId, String shiftDate,
			String startTime, String endTime, String status, boolean isOnCallRequest, boolean voluntaryLowCensus) throws Exception;

	boolean deleteASchedule (long scheduleId);
	
	boolean deleteShiftEvent (long userId, List<String> onDate, long departmentId, long requestId);
	
	boolean deleteShiftAndCreateOpenShift (long userId, List<String> onDate, long departmentId, long requestId);
	
}
