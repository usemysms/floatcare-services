package com.java.floatcare.service;

import org.springframework.stereotype.Component;

import com.java.floatcare.model.DepartmentSettings;

@Component
public interface DepartmentSettingsService {

	DepartmentSettings createDepartmentSettings (long departmentId, String customName, String workWeekDay, String workPayPeriod, long workPayPeriodHours);
	
	DepartmentSettings updateDepartmentSettings (long departmentSettingId, long departmentId, String customName, String workWeekDay, String workPayPeriod, Long workPayPeriodHours) throws Exception;

}
