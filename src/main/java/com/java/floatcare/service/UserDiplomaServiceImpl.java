package com.java.floatcare.service;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.java.floatcare.dao.CountersDAO;
import com.java.floatcare.model.UserDiploma;

import graphql.GraphQLException;

@Service
public class UserDiplomaServiceImpl implements UserDiplomaService {

	@Autowired
	private MongoTemplate mongoTemplate;
	@Autowired
	private CountersDAO sequence;

	@Override
	public UserDiploma createUserDiploma(long userDiplomaId, long userId, LocalDate issuedDate,
			LocalDate expirationDate, String documentNumber, String frontFilePhoto, String fileName,
			String backFilePhoto, String state) {

		if (userDiplomaId == 0) {

			UserDiploma userDiploma = new UserDiploma();

			userDiploma.setUserDiplomaId(sequence.getNextSequenceValue("userDiplomaId"));

			userDiploma.setUserId(userId);

			if (issuedDate != null)
				userDiploma.setIssuedDate(issuedDate);
			if (expirationDate != null)
				userDiploma.setExpirationDate(expirationDate);
			if (documentNumber != null)
				userDiploma.setDocumentNumber(documentNumber);
			if (frontFilePhoto != null)
				userDiploma.setFrontFilePhoto(frontFilePhoto);
			if (fileName != null)
				userDiploma.setFileName(fileName);
			if (backFilePhoto != null)
				userDiploma.setBackFilePhoto(backFilePhoto);
			if (state != null)
				userDiploma.setState(state);

			mongoTemplate.save(userDiploma, "user_diploma");

			return userDiploma;
			
		} else {
			
			UserDiploma userDiplomaOptional = mongoTemplate.findOne(Query.query(Criteria.where("userDiplomaId").is(userDiplomaId)), UserDiploma.class);

			if (userDiplomaOptional != null) {

				if (issuedDate != null)
					userDiplomaOptional.setIssuedDate(issuedDate);
				if (expirationDate != null)
					userDiplomaOptional.setExpirationDate(expirationDate);
				if (documentNumber != null)
					userDiplomaOptional.setDocumentNumber(documentNumber);
				if (frontFilePhoto != null)
					userDiplomaOptional.setFrontFilePhoto(frontFilePhoto);
				if (fileName != null)
					userDiplomaOptional.setFileName(fileName);
				if (backFilePhoto != null)
					userDiplomaOptional.setBackFilePhoto(backFilePhoto);
				if (state != null)
					userDiplomaOptional.setState(state);

				mongoTemplate.save(userDiplomaOptional, "user_diploma");

				return userDiplomaOptional;

			} else
				throw new GraphQLException("userDiplomaId not exists.");
		}
	}

	@Override
	public boolean deleteUserDiploma(long userDiplomaId) {
		
		UserDiploma userDiploma = mongoTemplate.findOne(Query.query(Criteria.where("userDiplomaId").is(userDiplomaId)), UserDiploma.class);
		
		if (userDiploma != null) {
			mongoTemplate.remove(userDiploma, "user_diploma");
			return true;
		}else
			return false;
	}
}