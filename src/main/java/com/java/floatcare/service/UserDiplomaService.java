package com.java.floatcare.service;

import java.time.LocalDate;

import com.java.floatcare.model.UserDiploma;

public interface UserDiplomaService {

	UserDiploma createUserDiploma (long userDiplomaId, long userId, LocalDate issuedDate, LocalDate expirationDate, String documentNumber, 
						String frontFilePhoto, String fileName, String backFilePhoto, String state);

	boolean deleteUserDiploma(long userDiplomaId);
}
