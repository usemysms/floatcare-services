package com.java.floatcare.service;

import org.springframework.stereotype.Service;

import com.java.floatcare.model.UserEducation;

@Service
public interface UserEducationService {

	public UserEducation createUserEducation(long userId, String school, String degreeTypeId, String degreeTypeName, String degreeOfStudy, String graduationDate,
			String location, String state);
	
	public UserEducation updateUserEducation(long userEducationId, long userId, String school, String degreeTypeId, String degreeTypeName, String degreeOfStudy, String graduationDate,
			String location, String state) throws Exception;
	
	public boolean deleteUserEducation(long userEducationId);
}
