package com.java.floatcare.service;

import java.io.File;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import com.java.floatcare.dao.CountersDAO;
import com.java.floatcare.dao.UserBasicInformationRepositoryDAO;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.model.UserCredentials;
import com.java.floatcare.repository.UserBasicInformationRepository;
import com.java.floatcare.repository.UserCredentialsRepository;
import com.java.floatcare.utils.ConstantUtils;
import com.java.floatcare.utils.EmailThread;
import com.java.floatcare.utils.InAppNotifications;
import com.java.floatcare.utils.UploadImageDAO;

@Service
public class UserCredentialsServiceImpl implements UserCredentialsService {

	@Autowired
	private UserCredentialsRepository userCredentialsRepository;
	@Autowired
	private UploadImageDAO setProfileImage;
	@Autowired
	private CountersDAO sequence;
	@Autowired
	private UserBasicInformationRepository userBasicInformationRepository;  
	@Autowired
	private UserBasicInformationRepositoryDAO userBasicInformationRepositoryDAO;
	@Autowired
	private InAppNotifications inAppNotifications;
	@Autowired
	private ApplicationContext applicationContext;
	
	private String uniqueFileName;

	private boolean check;
	
	public String getUniqueFileName() {
		return uniqueFileName;
	}

	public void setUniqueFileName(String uniqueFileName) {
		this.uniqueFileName = uniqueFileName;
	}

	public boolean isCheck() {
		return check;
	}

	public void setCheck(boolean check) {
		this.check = check;
	}

	@Override
	public UserCredentials createUserCredentials(long userId, String credentialIcon, String credentialName,
			long credentialTypeId, long credentialSubTypeId, String expirationDate, long credentialStatusId,
			String licenseNumber, String frontFilePhoto, String backFilePhoto, String state, boolean isCompactLicense, boolean isPending)
			throws ParseException {

		UserCredentials userCredentials = new UserCredentials();

		long credentialId = sequence.getNextSequenceValue("userCredentialId");
		userCredentials.setUserCredentialId(credentialId);

		userCredentials.setUserId(userId);

		if (credentialIcon != null)
			userCredentials.setCredentialIcon(credentialIcon);
		else
			userCredentials.setCredentialIcon(null);

		if (credentialName != null)
			userCredentials.setCredentialName(credentialName);
		else
			userCredentials.setCredentialName(null);

		userCredentials.setCredentialTypeId(credentialTypeId);

		userCredentials.setCredentialSubTypeId(credentialSubTypeId);

		if (expirationDate != null)
			userCredentials.setExpirationDate(LocalDate.parse(expirationDate));
		else
			userCredentials.setExpirationDate(null);

		userCredentials.setCredentialStatusId(credentialStatusId);

		if (licenseNumber != null)
			userCredentials.setLicenseNumber(licenseNumber);
		else
			userCredentials.setLicenseNumber(null);

		if (frontFilePhoto != null) {
			if (frontFilePhoto.length() > 0) {

				String fileName = credentialName + "_" + LocalDate.now().toString() + ".png";
				boolean check = false;
				try {
					check = setProfileImage.uploadImageFile(frontFilePhoto, fileName, 3);
				} catch (Exception e) {
					// System.out.println("File didn't changed to upload");
				}
				if (check) {
					userCredentials.setFrontFilePhoto(uniqueFileName);
					@SuppressWarnings("unused")
					File fileToDelete = FileUtils.getFile(fileName);
					FileUtils.deleteQuietly(fileToDelete);
				}
			}
		}
		else
			userCredentials.setFrontFilePhoto(null);

		if (backFilePhoto != null) {

			if (backFilePhoto.length() > 0) {

				String fileName = credentialName + "_" + LocalDate.now().toString() + ".png";
				boolean check = false;
				try {
					check = setProfileImage.uploadImageFile(backFilePhoto, fileName, 3);
				} catch (Exception e) {
					// System.out.println("File didn't changed to upload");
					e.printStackTrace();
				}
				if (check) {
					userCredentials.setBackFilePhoto(uniqueFileName);
					@SuppressWarnings("unused")
					File fileToDelete = FileUtils.getFile(fileName);
					FileUtils.deleteQuietly(fileToDelete);
				}
				// else
				// System.out.println("File could not get uploaded");
			}
		}
		else 
			userCredentials.setBackFilePhoto(null);
		
		if (state != null)
			userCredentials.setState(state);
		else
			userCredentials.setState(null);

		userCredentials.setIsCompactLicense(isCompactLicense);
		userCredentials.setVerified(false);
		userCredentials.setRequired(false);

		userCredentialsRepository.save(userCredentials);
		
		Optional<UserBasicInformation> updatedUser = userBasicInformationRepository.findUserByUserId(userId);
		   if(updatedUser.isPresent()) {
			   UserBasicInformation user = updatedUser.get();
			   List<UserBasicInformation> userAdmins =  userBasicInformationRepositoryDAO.findUsersByOrganizationIdAndUserTypeId(user.getOrganizationId(),ConstantUtils.adminUserTypeId);
			    
				   String message = "Credential Info!";
				   String content = "This credential '"+credentialName +"' has been created by '"+ user.getFirstName()+" "+user.getLastName()+"'";
				   String messageBody = "This credential '"+credentialName +"' has been created by "+ user.getFirstName()+" "+user.getLastName()+"'";
				   
				   if(userAdmins.size() > 0) {
					   for(UserBasicInformation admin : userAdmins) {
						   
						   long notificationTypeId = 5;
						   long deepLink = 10;
						   long sourceId = 0;
							inAppNotifications.createInAppNotifications(admin.getUserId(), notificationTypeId, message, content, deepLink, false,sourceId); // send inApp Notification
						   
							try {
								EmailThread emailThread = new EmailThread();
								emailThread.setEmails(admin.getEmail());
								emailThread.setMessage(message);
								emailThread.setMessageBody(messageBody);
								emailThread.setTemplate(false);
				
								applicationContext.getAutowireCapableBeanFactory().autowireBean(emailThread);
				
								Thread emailthread = new Thread(emailThread); // send email on separate thread.
								
								emailthread.start(); // start the email thread
							}catch (Exception e){
								
							}
		
					   }
			      }
		   }

		return userCredentials;
	}

	@Override
	public UserCredentials updateUserCredentials(long userCredentialId, long userId, String credentialIcon,
			String credentialName, long credentialTypeId, long credentialSubTypeId, String expirationDate,
			long credentialStatusId, String licenseNumber, String frontFilePhoto, String backFilePhoto, String state,
			boolean isCompactLicense, boolean isVerified, long verifiedBy, boolean isPending) throws Exception {

		Optional<UserCredentials> optUserCredentials = userCredentialsRepository.findById(userCredentialId);

		if (optUserCredentials.isPresent()) {
			UserCredentials userCredentials = optUserCredentials.get();
			
			if(isVerified)
				userCredentials.setVerified(true);

			userCredentials.setPending(false);
			
			if (credentialIcon != null)
				userCredentials.setCredentialIcon(credentialIcon);

			if (credentialName != null)
				userCredentials.setCredentialName(credentialName);

			if(credentialTypeId>0)
			userCredentials.setCredentialTypeId(credentialTypeId);

			if(credentialSubTypeId>0)
			userCredentials.setCredentialSubTypeId(credentialSubTypeId);

			if (expirationDate != null)
				userCredentials.setExpirationDate(LocalDate.parse(expirationDate));
			if(credentialStatusId>0)
			userCredentials.setCredentialStatusId(credentialStatusId);

			if (licenseNumber != null)
				userCredentials.setLicenseNumber(licenseNumber);

			if (frontFilePhoto != null) {

				if (frontFilePhoto.length() > 0) {
					String fileName = credentialName + "_" + LocalDate.now().toString() + ".png";
					boolean check = false;
					try {
						check = setProfileImage.uploadImageFile(frontFilePhoto, fileName, 3);
					} catch (Exception e) {
						// System.out.println("File didn't changed to upload");
					}
					if (check) {
						userCredentials.setFrontFilePhoto(uniqueFileName);
						@SuppressWarnings("unused")
						File fileToDelete = FileUtils.getFile(fileName);
						FileUtils.deleteQuietly(fileToDelete);
					}
					// else
					// System.out.println("File could not get uploaded");
				}
			}

			if (backFilePhoto != null) {

				if (backFilePhoto.length() > 0) {

					String fileName = credentialName + "_" + LocalDate.now().toString() + ".png";
					boolean check = false;
					try {
						check = setProfileImage.uploadImageFile(backFilePhoto, fileName, 3);
					} catch (Exception e) {
						// System.out.println("File didn't changed to upload");
					}
					if (check) {
						userCredentials.setBackFilePhoto(uniqueFileName);
						@SuppressWarnings("unused")
						File fileToDelete = FileUtils.getFile(fileName);
						FileUtils.deleteQuietly(fileToDelete);
					}
					// else
					// System.out.println("File could not get uploaded");
				}
			}

			if (state != null)
				userCredentials.setState(state);

			userCredentials.setIsCompactLicense(isCompactLicense);

			userCredentialsRepository.save(userCredentials);
			
			Optional<UserBasicInformation> updatedUser = userBasicInformationRepository.findUserByUserId(userId);
			   if(updatedUser.isPresent()) {
				   UserBasicInformation user = updatedUser.get();
				   List<UserBasicInformation> userAdmins =  userBasicInformationRepositoryDAO.findUsersByOrganizationIdAndUserTypeId(user.getOrganizationId(),ConstantUtils.adminUserTypeId);
				    
					   String message = "Credential Info!";
					   String content = "This credential '"+userCredentials.getUserCredentialId() +"' has been updated by '"+ user.getFirstName()+" "+user.getLastName()+"'";
					   String messageBody = "This credential '"+userCredentials.getUserCredentialId() +"' has been updated by "+ user.getFirstName()+" "+user.getLastName()+"'";
					   
					   if(userAdmins.size() > 0) {
						   for(UserBasicInformation admin : userAdmins) {
							   
							   long notificationTypeId = 5;
							   long deepLink = 10;
							   long sourceId = 0;
								inAppNotifications.createInAppNotifications(admin.getUserId(), notificationTypeId, message, content, deepLink, false,sourceId); // send inApp Notification
							   
								try {
									EmailThread emailThread = new EmailThread();
									emailThread.setEmails(admin.getEmail());
									emailThread.setMessage(message);
									emailThread.setMessageBody(messageBody);
									emailThread.setTemplate(false);
					
									applicationContext.getAutowireCapableBeanFactory().autowireBean(emailThread);
					
									Thread emailthread = new Thread(emailThread); // send email on separate thread.
									
									emailthread.start(); // start the email thread
								}catch (Exception e){
									
								}
			
						   }
				      }
			   }
			   
			return userCredentials;
		}

		throw new Exception("User Credential not inserted");

	}

	@Override
	public boolean deleteUserCredentials(long userCredentialId) {

		userCredentialsRepository.deleteById(userCredentialId);
		return true;
	}

}