package com.java.floatcare.service;

import org.springframework.stereotype.Service;

import com.java.floatcare.model.UserNotification;

@Service
public interface UserNotificationService {

	public UserNotification createUserNotification(long userId, long notificationTypeId, boolean isMuteAllEnable, boolean isEnablePush,
			boolean isEnableSMS, boolean isEnableEmail);
	
}
