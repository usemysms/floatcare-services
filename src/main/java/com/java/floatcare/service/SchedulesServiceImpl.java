package com.java.floatcare.service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import com.java.floatcare.dao.CountersDAO;
import com.java.floatcare.dao.ScheduleDAO;
import com.java.floatcare.model.Businesses;
import com.java.floatcare.model.Department;
import com.java.floatcare.model.RequestTypes;
import com.java.floatcare.model.Requests;
import com.java.floatcare.model.Schedules;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.repository.BusinessesRepository;
import com.java.floatcare.repository.DepartmentRepository;
import com.java.floatcare.repository.RequestTypesRepository;
import com.java.floatcare.repository.RequestsRepository;
import com.java.floatcare.repository.SchedulesRepository;
import com.java.floatcare.repository.UserBasicInformationRepository;
import com.java.floatcare.utils.ConstantUtils;
import com.java.floatcare.utils.EmailThread;
import com.java.floatcare.utils.GoogleFirebaseUtilityDAO;
import com.java.floatcare.utils.InAppNotifications;

@Service
public class SchedulesServiceImpl implements SchedulesService {

	@Autowired
	private CountersDAO sequence;
	@Autowired
	private SchedulesRepository schedulesRepository;
	@Autowired
	private RequestTypesRepository requestTypesRepository;
	@Autowired
	private ScheduleDAO scheduleDAO;
	@Autowired
	private RequestsRepository requestsRepository;
	@Autowired
	private UserBasicInformationRepository userRepository;
	@Autowired
	private GoogleFirebaseUtilityDAO flashNotification;
	@Autowired
	private ApplicationContext applicationContext;
	@Autowired
	private BusinessesRepository businessRepository;
	@Autowired
	private DepartmentRepository departmentRepository;
	@Autowired
	private InAppNotifications inAppNotification;

	@Override
	public Schedules createASchedule(long worksiteId, long departmentId, long userId, long shiftTypeId,
			String shiftDate, String startTime, String endTime, boolean isOnCallRequest) {

		Schedules schedules = new Schedules();

		schedules.setScheduleId(sequence.getNextSequenceValue("scheduleId"));

		if (worksiteId > 0)
			schedules.setWorksiteId(worksiteId);

		if (departmentId > 0)
			schedules.setDepartmentId(departmentId);

		if (userId > 0)
			schedules.setUserId(userId);

		if (shiftTypeId > 0)
			schedules.setShiftTypeId(shiftTypeId);

		if (shiftDate != null)
			schedules.setShiftDate(LocalDate.parse(shiftDate));
		else
			schedules.setShiftDate(null);

		if (startTime != null)
			schedules.setStartTime(LocalTime.parse(startTime));
		else
			schedules.setStartTime(null);

		if (endTime != null)
			schedules.setEndTime(LocalTime.parse(endTime));
		else
			schedules.setEndTime(null);
		
		schedules.setStatus("Active");

		schedules.setOnCallRequest(isOnCallRequest);
		
		schedules.setVoluntaryLowCensus(false);

		schedulesRepository.save(schedules);

		return schedules;
	}
	
	@Override
	public Schedules updateASchedule(long scheduleId, long worksiteId, long departmentId, long userId, long shiftTypeId,
			String shiftDate, String startTime, String endTime, String status, boolean isOnCallRequest, boolean voluntaryLowCensus) throws Exception {
		
		Optional<Schedules> scheduleOptional = schedulesRepository.findById(scheduleId);
		
		if (scheduleOptional.isPresent()) {
			
			Schedules schedules = scheduleOptional.get();
			
			if (worksiteId > 0)
				schedules.setWorksiteId(worksiteId);

			if (departmentId > 0)
				schedules.setDepartmentId(departmentId);

			if (userId > 0)
				schedules.setUserId(userId);

			if (shiftTypeId > 0)
				schedules.setShiftTypeId(shiftTypeId);

			if (shiftDate != null)
				schedules.setShiftDate(LocalDate.parse(shiftDate));

			if (startTime != null)
				schedules.setStartTime(LocalTime.parse(startTime));

			if (endTime != null)
				schedules.setEndTime(LocalTime.parse(endTime));
			
			if (status != null)
				schedules.setStatus(status);
			
			if (isOnCallRequest == true || isOnCallRequest == false)
				schedules.setOnCallRequest(isOnCallRequest);
			
			if (voluntaryLowCensus == true || voluntaryLowCensus == false)
				schedules.setVoluntaryLowCensus(voluntaryLowCensus);
			
			schedulesRepository.save(schedules);
			
			return schedules;
		}
		else
			throw new Exception("Schedule Id does not exist");
	}

	@Override
	public boolean deleteASchedule(long scheduleId) {

		schedulesRepository.deleteById(scheduleId);
		return true;
	}

	@Override
	public boolean deleteShiftEvent(long userId, List<String> onDate, long departmentId, long requestId) {

		long count = 0;
		
		for (String eachDate : onDate) {
			
			LocalDate date = LocalDate.parse(eachDate);
			
			Schedules scheduleFound = scheduleDAO.findScheduleByUserIdAndShiftDateAndDepartmentId(userId, date, departmentId);
			
			if (scheduleFound != null) {
				
				count += 1;
				Schedules schedule = scheduleFound;

				schedule.setStatus("Cancelled");
				schedulesRepository.save(schedule);
			}
		}
		
		Optional<Requests> requestOptional = requestsRepository.findByRequestIdAndStatus(requestId, "Pending");

		if (requestOptional.isPresent()) {
			
			count += 1;
			Requests requests = requestOptional.get();
			requests.setStatus("Approved");
			requestsRepository.save(requests);
				
			// send notification
			String message = "Request Approved.";
			String deepLink = "7";  // deep link for redirecting user to schedule/calendar screen
			String messageBody = "request has been approved";
			
			Optional<RequestTypes> requestTypeOptional = requestTypesRepository.findById(requestOptional.get().getRequestTypeId());
			Optional<Businesses> businessOptional = businessRepository.findById(requestOptional.get().getWorksiteId());
			Optional<Department> departmentOptional = departmentRepository.findById(requestOptional.get().getDepartmentId());
			
			String departmentName = departmentOptional.get().getDepartmentTypeName();
			String businessName = businessOptional.get().getName();

			if (requestTypeOptional.isPresent() && businessOptional.isPresent())
				messageBody = "Your "+requestTypeOptional.get().getLabel()+" "+messageBody+" in "+departmentName+" @ "+businessName;
			else
				messageBody = "Your "+ messageBody;

			UserBasicInformation userDetails = userRepository.findById(userId).get();

			if (userDetails.getFcmToken() != null) // send push notification.
				flashNotification.sendEventNotificationToUser(userId, userDetails.getFcmToken(), message, messageBody, deepLink, userDetails.getDeviceType(), null);

			inAppNotification.createInAppNotifications(userId, 6, message, messageBody, Long.parseLong(deepLink), false, requestId);

			messageBody = messageBody+". Click on the link below to go to the  Floatcare application. \n"+ConstantUtils.branchIOMySchedule;

			EmailThread emailThread = new EmailThread();
			emailThread.setEmails(userDetails.getEmail());
			emailThread.setMessage(message);
			emailThread.setMessageBody(messageBody);
			applicationContext.getAutowireCapableBeanFactory().autowireBean(emailThread);
			Thread thread = new Thread(emailThread); // send email on separate thread.
			thread.start(); // start the thread
		}

		if (count > 0)
			return true;
		else
			return false;
	}

	@Override
	public boolean deleteShiftAndCreateOpenShift(long userId, List<String> onDate, long departmentId, long requestId) {

		for (String eachDate : onDate) {
			
			LocalDate date = LocalDate.parse(eachDate);
			
			Schedules foundSchedule = scheduleDAO.findScheduleByUserIdAndShiftDateAndDepartmentId(userId, date, departmentId);
			
			if (foundSchedule != null) {
				
				Schedules schedule = foundSchedule;
				
				schedule.setStatus("Cancelled");
				
				schedulesRepository.save(schedule);
			} else
				continue;
		}
		
		Optional<Requests> requestOptional = requestsRepository.findById(requestId);

		if (requestOptional.isPresent()) {
			
			Requests requests = requestOptional.get();
			
			requests.setStatus("Approved");
			requestsRepository.save(requests);
			
			// send notification
			String message = "Request Approved.";
			String deepLink = "7";  // deep link for redirecting user to request screen
			String messageBody = "request has been approved.";
			
			Optional<RequestTypes> requestTypeOptional = requestTypesRepository.findById(requestOptional.get().getRequestTypeId());
			Optional<Businesses> businessOptional = businessRepository.findById(requestOptional.get().getWorksiteId());
			Optional<Department> departmentOptional = departmentRepository.findById(requestOptional.get().getDepartmentId());
			
			String departmentName = departmentOptional.get().getDepartmentTypeName();
			String businessName = businessOptional.get().getName();

			if (requestTypeOptional.isPresent() && businessOptional.isPresent() && departmentOptional.isPresent())
				messageBody = "Your "+requestTypeOptional.get().getLabel()+" request has been approved in "+departmentName+ " @ "+businessName;
			else
				messageBody = "Your "+ messageBody;

			UserBasicInformation userDetails = userRepository.findById(userId).get();

			if (userDetails.getFcmToken() != null) // send push notification.
				flashNotification.sendEventNotificationToUser(userId, userDetails.getFcmToken(), message, messageBody, deepLink, userDetails.getDeviceType(), null);

			inAppNotification.createInAppNotifications(userId, 6, message, messageBody, Long.parseLong(deepLink), false, requestId);

			messageBody = messageBody+". Click on the link below to go to the Floatcare application. \n"+ConstantUtils.branchIOMySchedule;

			EmailThread emailThread = new EmailThread();
			emailThread.setEmails(userDetails.getEmail());
			emailThread.setMessage(message);
			emailThread.setMessageBody(messageBody);
			applicationContext.getAutowireCapableBeanFactory().autowireBean(emailThread);
			Thread thread = new Thread(emailThread); // send email on separate thread.
			thread.start(); // start the thread
		}
		return true;
	}
}