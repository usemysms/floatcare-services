package com.java.floatcare.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.java.floatcare.dao.CountersDAO;
import com.java.floatcare.dao.UserWorkspacesDAO;
import com.java.floatcare.model.Businesses;
import com.java.floatcare.model.Department;
import com.java.floatcare.model.ScheduleLayout;
import com.java.floatcare.model.ScheduleLayoutGroups;
import com.java.floatcare.model.ScheduleLayoutQuota;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.model.UserWorkspaces;
import com.java.floatcare.repository.AvailabilityRepository;
import com.java.floatcare.repository.BusinessesRepository;
import com.java.floatcare.repository.DepartmentRepository;
import com.java.floatcare.repository.ScheduleLayoutGroupsRepository;
import com.java.floatcare.repository.ScheduleLayoutQuotaRepository;
import com.java.floatcare.repository.ScheduleLayoutRepository;
import com.java.floatcare.repository.SchedulesRepository;
import com.java.floatcare.repository.UserBasicInformationRepository;
import com.java.floatcare.utils.ConstantUtils;
import com.java.floatcare.utils.EmailThread;
import com.java.floatcare.utils.GoogleFirebaseUtilityDAO;
import com.java.floatcare.utils.InAppNotifications;
import com.java.floatcare.utils.SMSThread;
import com.java.floatcare.utils.SplitStringForCharacterAndNumric;
import com.twilio.exception.ApiException;

@Component
public class ScheduleLayoutServiceImpl implements ScheduleLayoutService {

	@Autowired
	private ScheduleLayoutRepository scheduleLayoutRepository;
	@Autowired
	private ScheduleLayoutQuotaRepository scheduleLayoutQuotaRepository;
	@Autowired
	private ScheduleLayoutGroupsRepository scheduleLayoutGroupsRepository;
	@Autowired
	private CountersDAO sequence;
	@Autowired
	private UserWorkspacesDAO userWorkspacesDAO;
	@Autowired
	private UserBasicInformationRepository userBasicInformationRepository;
	@Autowired
	private InAppNotifications inAppNotifications;
	@Autowired
	private GoogleFirebaseUtilityDAO googleFirebaseUtilityDAO;
	@Autowired
	private DepartmentRepository departmentRepository;
	@Autowired
	private BusinessesRepository businessRepository;
	@Autowired
	private ApplicationContext applicationContext;
	@Autowired
	private SchedulesRepository schedulesRepository;
	@Autowired
	private AvailabilityRepository availabilityRepository;

	public ScheduleLayout createScheduleLayout(long departmentId, String shiftPlanningStartDate,String scheduleStartDate, String scheduleLength, String shiftPlanningPhaseLength,
			String makeStaffAvailabilityDeadLineLength, String maxQuota, List<Long> staffCoreTypes, List<Long> shiftTypes, boolean autoAddProfessionalsToLayout) throws Exception {

		List<UserWorkspaces> userWorkspaces = userWorkspacesDAO.findByStaffUserCoreTypeIdsAndShiftTypeAndDepartmentId(staffCoreTypes, shiftTypes, departmentId);

		if (userWorkspaces.size() > 0) {

			ScheduleLayout scheduleLayout = new ScheduleLayout();
			
			long scheduleLayoutId = sequence.getNextSequenceValue("scheduleLayoutId");
			
			scheduleLayout.setScheduleLayoutId(scheduleLayoutId);
	
			if (departmentId > 0)
				scheduleLayout.setDepartmentId(departmentId);
	
			if (shiftPlanningStartDate != null)
			scheduleLayout.setShiftPlanningStartDate(DateTime.parse(shiftPlanningStartDate).withZone(DateTimeZone.forID("America/New_York")));
			else
				scheduleLayout.setShiftPlanningStartDate(null);
	
			if (scheduleStartDate != null) {
				scheduleLayout.setScheduleStartDate(DateTime.parse(scheduleStartDate).withZone(DateTimeZone.forID("America/New_York")));
			} else
				scheduleLayout.setScheduleStartDate(null);
	
			if (shiftPlanningPhaseLength != null)
				scheduleLayout.setShiftPlanningPhaseLength(shiftPlanningPhaseLength);
			else
				scheduleLayout.setShiftPlanningPhaseLength(shiftPlanningPhaseLength);
	
			if (makeStaffAvailabilityDeadLineLength != null)
				scheduleLayout.setMakeStaffAvailabilityDeadLineLength(makeStaffAvailabilityDeadLineLength);
			else
				scheduleLayout.setMakeStaffAvailabilityDeadLineLength(null);
	
			if (scheduleLength != null)
				scheduleLayout.setScheduleLength(scheduleLength);
			else
				scheduleLayout.setScheduleLength(null);
	
			if (maxQuota != null)
				scheduleLayout.setMaxQuota(maxQuota);
			else
				scheduleLayout.setMaxQuota(null);
	
			if (scheduleLength != null) {
				DateTime scheduleEndDate = SplitStringForCharacterAndNumric.splitString(scheduleStartDate, scheduleLength);
				scheduleLayout.setScheduleEndDate(scheduleEndDate);
			}else	
				scheduleLayout.setScheduleEndDate(null);

			if (shiftPlanningPhaseLength != null) {

				DateTime shiftPlanningEndDate = SplitStringForCharacterAndNumric.splitString(shiftPlanningStartDate, shiftPlanningPhaseLength);
	
				scheduleLayout.setShiftPlanningEndDate(shiftPlanningEndDate);
			}
			else
				scheduleLayout.setShiftPlanningEndDate(null);
			
			   DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");  
			   LocalDateTime now = LocalDateTime.now();  
			   String todaysDate = dtf.format(now);
	
			if (makeStaffAvailabilityDeadLineLength != null) {

				DateTime shiftPlanningEndDate = SplitStringForCharacterAndNumric.splitString(todaysDate, makeStaffAvailabilityDeadLineLength);
				
				scheduleLayout.setMakeStaffAvailabilityDeadLine(shiftPlanningEndDate);
			}
			else
				scheduleLayout.setMakeStaffAvailabilityDeadLine(null);
			
			if (autoAddProfessionalsToLayout == true)
				scheduleLayout.setAutoAddProfessionalsToLayout(autoAddProfessionalsToLayout);
			else
				scheduleLayout.setAutoAddProfessionalsToLayout(false);
			
			
			if (staffCoreTypes != null)
				scheduleLayout.setStaffUserCoreTypeIds(staffCoreTypes);
					
			scheduleLayout.setPublishedOn(null);
			
			if (shiftTypes != null)
				scheduleLayout.setShiftTypes(shiftTypes);

			scheduleLayout.setStatus("Self Schedule");

			 // if no staff member has been assigned to previous schedule layout then new schedule layout settings will be saved
	
			// create the user groups for the shift planning draft
			List<Long> userList = userWorkspaces.stream().map(e->e.getUserId()).distinct().collect(Collectors.toList()); // fetch users based on their roles and filter by staffCoreTypes choose for scheduleLayout example:20
			
			/*
			 * Equally distribute all staff members into groups and remaining add into any group
			 * 
			 * int equalDistribution = userList.size() / groups.length; // how many members need to be distributed in each groups 
			 * int remainder = userList.size() % groups.length;
			 *
			 * For eg 20 members, so for 3 groups, 6 members will be divided in each group so there will be remaining 2 members, stored in this variable
			*/
			
			if (userList != null && userList.size() > 0) {
				
				// create schedule layout quota for each role
				
				for (Long eachRole : staffCoreTypes) {
					
					ScheduleLayoutQuota scheduleLayoutQuota = new ScheduleLayoutQuota();
					scheduleLayoutQuota.setStaffCoreTypeId(eachRole);
					
					for (Long eachShiftType : shiftTypes) {
							
						scheduleLayoutQuota.setQuota(0);
						scheduleLayoutQuota.setScheduleLayoutQuotaId(sequence.getNextSequenceValue("scheduleLayoutQuotaId"));
						scheduleLayoutQuota.setScheduleLayoutId(scheduleLayoutId);
						scheduleLayoutQuota.setShiftTypeId(eachShiftType);
						scheduleLayoutQuotaRepository.save(scheduleLayoutQuota);
					}
				}
	
				String[] groups = {"Team"}; // Group name
	
				ScheduleLayoutGroups scheduleLayoutGroups = new ScheduleLayoutGroups(); // setting groupName and departmentId in this loop
			
					scheduleLayoutGroups.setDepartmentId(departmentId);
					scheduleLayoutGroups.setScheduleLayoutId(scheduleLayoutId);
					scheduleLayoutGroups.setScheduleLayoutGroupId(sequence.getNextSequenceValue("scheduleLayoutGroupId"));
					scheduleLayoutGroups.setName(groups[0]);
					scheduleLayoutGroups.setUserIds(userList);
					/*
					 * for (int j = 0; j < userList.size(); j++) { if (equalDistribution >
					 * scheduleLayoutGroups.getUserIds().size()) {
					 * scheduleLayoutGroups.setUserIds(userList.get(j)); // settings users in this
					 * loop } else { userList.removeAll(scheduleLayoutGroups.getUserIds()); //
					 * remove the existing users to avoid // repetition of users
					 * 
					 * if (userList.size() == remainder) { for (int l = 0; l < remainder; l++)
					 * scheduleLayoutGroups.setUserIds(userList.get(l)); // insert the remaining
					 * members if left // after even distribution } } }
					 *
					 * if (scheduleLayoutGroups.getUserIds().size() > 0) 
					 */
					scheduleLayoutGroupsRepository.save(scheduleLayoutGroups);
					scheduleLayoutRepository.save(scheduleLayout);
					
					for (Long userId: userList) {

						UserBasicInformation userDetails = userBasicInformationRepository.findUsersByUserId(userId);

						if (userDetails != null) {

							Department department = departmentRepository.findById(departmentId).get();
							Businesses business = businessRepository.findById(department.getBusinessId()).get();
	
							String departmentName = null;
							String businessName = null;
	
							try {
								departmentName = department.getDepartmentTypeName();
								businessName = business.getName();
							}catch(Exception e) {
								departmentName = "";
								businessName = "";
							}
	
							String receiverFcmToken = userDetails.getFcmToken();
							String message = "Self Scheduling Phase has Started";
							String messageBody = "Self Schedule is now open for your shift planning group in "+ departmentName+" @ "+businessName;
							
							String deepLink = "3"; // Deep Link for sending user to my request screen
							
							if (receiverFcmToken != null)
								googleFirebaseUtilityDAO.sendEventNotificationToUser(userId, receiverFcmToken, message, messageBody, deepLink, userDetails.getDeviceType(), null); // send Push Notification
							long notificationTypeId = 6;
							inAppNotifications.createInAppNotifications(userId, notificationTypeId, message, messageBody, 3, false, scheduleLayoutId); // send In App notification
	
							messageBody = messageBody + ". Click on the link below to view your shift planning.\n"+ConstantUtils.branchIOMyRequest;
	
							try {
		//						emailService.sendHTML(userDetails.getEmail(), message, messageBody, userDetails.getFirstName(), ConstantUtils.assignedSchedule); // send Email
		//						twillioCommunication.sendSMS(userDetails.getPhone(), messageBody); //send SMS
								EmailThread emailThread = new EmailThread();
								emailThread.setEmails(userDetails.getEmail());
								emailThread.setDynamicName(userDetails.getFirstName());
								emailThread.setMessage(message);
								emailThread.setMessageBody(messageBody);
								emailThread.setDepartmentName(departmentName);
								emailThread.setBusinessName(businessName);
								emailThread.setTemplate(ConstantUtils.assignedSchedule);
								emailThread.setTemplate(true);
		
								SMSThread smsThread = new SMSThread();
								smsThread.setMessageBody(messageBody);
								smsThread.setPhone(userDetails.getPhone());
		
								applicationContext.getAutowireCapableBeanFactory().autowireBean(emailThread);
								applicationContext.getAutowireCapableBeanFactory().autowireBean(smsThread);
		
								Thread emailthread = new Thread(emailThread); // send email on separate thread.
								Thread smsthread = new Thread(smsThread);
								emailthread.start(); // start the email thread
								smsthread.start(); // start the sms thread
							} catch (ApiException apiException) {
								continue;
							}
						}
					}
				}
			return scheduleLayout;
		}else
			return null;
	}
	
	@Override
	public ScheduleLayout updateScheduleLayout (long scheduleLayoutId, long departmentId, String shiftPlanningStartDate, String scheduleStartDate, 
			String scheduleLength, String shiftPlanningPhaseLength, String makeStaffAvailabilityDeadLineLength, String maxQuota, 
			List<Long> staffCoreTypes, List<Long> shiftTypes, boolean autoAddProfessionalsToLayout) throws Exception {

		List<UserWorkspaces> userWorkspaces = new ArrayList<>();

		if (staffCoreTypes != null && shiftTypes != null)// check for duplication
			userWorkspaces = userWorkspacesDAO.findByStaffUserCoreTypeIdsAndShiftTypeAndDepartmentId(staffCoreTypes, shiftTypes, departmentId);

		if (userWorkspaces != null && userWorkspaces.size() > 0) {

			Optional<ScheduleLayout> scheduleLayoutOpt = scheduleLayoutRepository.findById(scheduleLayoutId);

			if (scheduleLayoutOpt.isPresent()) {

				ScheduleLayout scheduleLayout = scheduleLayoutOpt.get();
				
				if (departmentId > 0)
					scheduleLayout.setDepartmentId(departmentId);
	
				if (shiftPlanningStartDate != null)
					scheduleLayout.setShiftPlanningStartDate(DateTime.parse(shiftPlanningStartDate).withTime(0,0,0,0).withZone(DateTimeZone.forID("America/New_York")));

				if (scheduleStartDate != null)
					scheduleLayout.setScheduleStartDate(DateTime.parse(scheduleStartDate).withTime(0,0,0,0).withZone(DateTimeZone.forID("America/New_York")));

				if (shiftPlanningPhaseLength != null)
					scheduleLayout.setShiftPlanningPhaseLength(shiftPlanningPhaseLength);

				if (makeStaffAvailabilityDeadLineLength != null)
					scheduleLayout.setMakeStaffAvailabilityDeadLineLength(makeStaffAvailabilityDeadLineLength);

				if (scheduleLength != null)
					scheduleLayout.setScheduleLength(scheduleLength);

				if (maxQuota != null)
					scheduleLayout.setMaxQuota(maxQuota);

				if (scheduleLength != null) {

					DateTime scheduleEndDate = SplitStringForCharacterAndNumric.splitString(scheduleStartDate, scheduleLength);
					scheduleLayout.setScheduleEndDate(scheduleEndDate);
				}

				if (shiftPlanningPhaseLength != null) {

					DateTime shiftPlanningEndDate = SplitStringForCharacterAndNumric.splitString(shiftPlanningStartDate, shiftPlanningPhaseLength);
					scheduleLayout.setShiftPlanningEndDate(shiftPlanningEndDate);
				}
		
				if (makeStaffAvailabilityDeadLineLength != null) {

					DateTime makeStaffAvailabilityDeadLineEndDate = SplitStringForCharacterAndNumric.splitString(shiftPlanningStartDate, makeStaffAvailabilityDeadLineLength);
					scheduleLayout.setMakeStaffAvailabilityDeadLine(makeStaffAvailabilityDeadLineEndDate);
				}

				if (autoAddProfessionalsToLayout)
					scheduleLayout.setAutoAddProfessionalsToLayout(autoAddProfessionalsToLayout);
						
				if (staffCoreTypes != null) {
					scheduleLayoutOpt.get().getStaffUserCoreTypeIds().clear();
					scheduleLayout.setStaffUserCoreTypeIds(staffCoreTypes);
				}
				
				if (shiftTypes != null) {
					scheduleLayoutOpt.get().getShiftTypes().clear();
					scheduleLayout.setShiftTypes(shiftTypes);
				}

				if (departmentId == 0)
					departmentId = scheduleLayoutOpt.get().getDepartmentId();
				
				if (staffCoreTypes != null && shiftTypes != null) {

					scheduleLayoutQuotaRepository.deleteByScheduleLayoutId(scheduleLayoutId); // remove existing schedule layout quota

					scheduleLayoutGroupsRepository.deleteByScheduleLayoutId(scheduleLayoutId);

					// create new user groups for the shift planning draft
					String[] groups = {"Team 1"};
					List<Long> userList = userWorkspaces.stream().map(e->e.getUserId()).distinct().collect(Collectors.toList()); // fetch users based on their roles and filter by staffCoreTypes choose for scheduleLayout example:20

					ScheduleLayoutGroups scheduleLayoutGroups = new ScheduleLayoutGroups(); // setting groupName and departmentId in this loop

					scheduleLayoutGroups.setDepartmentId(departmentId);
					scheduleLayoutGroups.setScheduleLayoutId(scheduleLayoutId);
					scheduleLayoutGroups.setScheduleLayoutGroupId(sequence.getNextSequenceValue("scheduleLayoutGroupId"));
					scheduleLayoutGroups.setName(groups[0]);
					scheduleLayoutGroups.setUserIds(userList);
	
					for (Long userId: userList) {
						
						Optional<UserBasicInformation> userDetails = userBasicInformationRepository.findByUserIdAndStatus(userId, "Active");

						if (userDetails.isPresent()) {

							Department department = departmentRepository.findById(departmentId).get();
							Businesses business = businessRepository.findById(department.getBusinessId()).get();
	
							String departmentName = null;
							String businessName = null;
	
							try {
								departmentName = department.getDepartmentTypeName();
								businessName = business.getName();
							}catch(Exception e) {
								departmentName = "";
								businessName = "";
							}

							String receiverFcmToken = userDetails.get().getFcmToken();
							String message = "Self Scheduling Phase has been updated.";
							String messageBody = "Self Schedule is now updated for your shift planning group in "+ departmentName+" @ "+ businessName;
	
							String deepLink = "3"; // Deep Link for sending user to my request screen
							
							if (receiverFcmToken != null)
								googleFirebaseUtilityDAO.sendEventNotificationToUser(userId, receiverFcmToken, message, messageBody, deepLink, userDetails.get().getDeviceType(), null); // send Push Notification
							
							long notificationTypeId = 6;
							inAppNotifications.deleteBySourceId(Arrays.asList(scheduleLayoutId));
							inAppNotifications.createInAppNotifications(userId, notificationTypeId, message, messageBody, 3, false, scheduleLayoutId); // send In App notification
	
							messageBody = messageBody + ". Click on the link below to view your shift planning.\n"+ConstantUtils.branchIOMySchedule;
	
							try {
		//						emailService.sendHTML(userDetails.getEmail(), message, messageBody, userDetails.getFirstName(), ConstantUtils.assignedSchedule); // send Email
		//						twillioCommunication.sendSMS(userDetails.getPhone(), messageBody); //send SMS
								EmailThread emailThread = new EmailThread();
								emailThread.setEmails(userDetails.get().getEmail());
								emailThread.setDynamicName(userDetails.get().getFirstName());
								emailThread.setMessage(message);
								emailThread.setMessageBody(messageBody);
								emailThread.setTemplate(false);
		
								SMSThread smsThread = new SMSThread();
								smsThread.setMessageBody(messageBody);
								smsThread.setPhone(userDetails.get().getPhone());
	
								applicationContext.getAutowireCapableBeanFactory().autowireBean(emailThread);
								applicationContext.getAutowireCapableBeanFactory().autowireBean(smsThread);
	
								Thread emailthread = new Thread(emailThread); // send email on separate thread.
								Thread smsthread = new Thread(smsThread);
								emailthread.start(); // start the email thread
								smsthread.start(); // start the sms thread
							} catch (ApiException apiException) {
								continue;
							}
						}
					}

					if (userList != null && userList.size() > 0) {
						scheduleLayoutGroupsRepository.save(scheduleLayoutGroups);

						scheduleLayoutRepository.save(scheduleLayout);

						// create new schedule layout quota for each role
						for (Long eachRole : staffCoreTypes) {
							ScheduleLayoutQuota scheduleLayoutQuota = new ScheduleLayoutQuota();
							scheduleLayoutQuota.setStaffCoreTypeId(eachRole);
							for (Long eachShiftType : shiftTypes) {
								scheduleLayoutQuota.setQuota(0);
								scheduleLayoutQuota.setScheduleLayoutQuotaId(sequence.getNextSequenceValue("scheduleLayoutQuotaId"));
								scheduleLayoutQuota.setScheduleLayoutId(scheduleLayoutId);
								scheduleLayoutQuota.setShiftTypeId(eachShiftType);
								
								scheduleLayoutQuotaRepository.save(scheduleLayoutQuota);
							}
						}
					}
				}
				return scheduleLayout;
			}else
				throw new Exception("Schedule Layout Id does not exists !!");
		}else
			return null;
	}
	
	@Override
	public boolean deleteScheduleLayout (long scheduleLayoutId) {
		
		Optional<ScheduleLayout> scheduleLayoutOptional = scheduleLayoutRepository.findById(scheduleLayoutId);
		
		if (scheduleLayoutOptional.isPresent()) {
			scheduleLayoutRepository.deleteById(scheduleLayoutId);
			scheduleLayoutGroupsRepository.deleteByScheduleLayoutId(scheduleLayoutId);
			scheduleLayoutQuotaRepository.deleteByScheduleLayoutId(scheduleLayoutId);
			inAppNotifications.deleteBySourceId(Arrays.asList(scheduleLayoutId));
			schedulesRepository.deleteSchedulesById(scheduleLayoutId);
			availabilityRepository.deleteByScheduleLayoutId(scheduleLayoutId);
			return true;
		}
		else
			return false;
	}	
}