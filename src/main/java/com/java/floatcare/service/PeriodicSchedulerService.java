package com.java.floatcare.service;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.Scheduled;

import com.java.floatcare.dao.UserBasicInformationRepositoryDAO;
import com.java.floatcare.dao.UserCredentialsDAO;
import com.java.floatcare.dao.UserWorkspacesDAO;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.repository.DepartmentRepository;
import com.java.floatcare.repository.StaffUserCoreTypesRepository;
import com.java.floatcare.repository.UserBasicInformationRepository;
import com.java.floatcare.utils.ConstantUtils;
import com.java.floatcare.utils.EmailThread;
import com.java.floatcare.utils.GoogleFirebaseUtilityDAO;
import com.java.floatcare.utils.InAppNotifications;

public class PeriodicSchedulerService {

	@Autowired
	UserWorkspacesDAO userWorkspaceDAO;
	@Autowired
	UserBasicInformationRepositoryDAO userBasicInformationDAO;
	@Autowired
	UserCredentialsDAO userCredentialsDAO;
	@Autowired
	StaffUserCoreTypesRepository staffUserCoreTypesRepository;
	@Autowired
	DepartmentRepository departmentRepository;
	@Autowired
	private InAppNotifications inAppNotification;
	@Autowired
	private GoogleFirebaseUtilityDAO pushNotification;
	@Autowired
	private UserBasicInformationRepository userRepository;
	@Autowired
	private ApplicationContext applicationContext;

	public void sendCredentialExpirationRemainders(int numOfDays) {
		LocalDate checkExpiryDate = LocalDate.now().plusDays(numOfDays);

		Set<Long> userExpiredCredentials = userCredentialsDAO.findUserCredentialsByExpiredDate(checkExpiryDate).stream()
				.map(e -> e.getUserId()).collect(Collectors.toSet());

		if (userExpiredCredentials.size() > 0) {
			inAppNotification.deleteBySourceId(Arrays.asList(Long.parseLong("0")));
			for (Long eachUser : userExpiredCredentials) {

				UserBasicInformation user = userRepository.findUsersByUserId(eachUser);

				if (user != null) {
					
					String message = "Don't forget to add your credentials!";
					String messageBody = ", please update your credentials in Float Care!";
					String deepLink = "10";

					messageBody = user.getFirstName() + messageBody;

					if (user.getFcmToken() != null)
						pushNotification.sendEventNotificationToUser(eachUser, user.getFcmToken(), message, messageBody, deepLink, user.getDeviceType(), null);

					inAppNotification.createInAppNotifications(eachUser, 3, message, messageBody, Long.parseLong(deepLink), false, 0);

					messageBody = messageBody+" Click on the link below to go to the Floatcare Application.\n"+ ConstantUtils.branchIOMySchedule;
					EmailThread emailThread = new EmailThread(); // sending email
					emailThread.setEmails(user.getEmail());
					emailThread.setMessage(message);
					emailThread.setMessageBody(messageBody);
					applicationContext.getAutowireCapableBeanFactory().autowireBean(emailThread);

					Thread thread = new Thread(emailThread); // send email on separate thread.
					thread.start(); // start the thread
				}
			}
		}
	}

	@Scheduled(fixedRate = 86400000)
	public void send30DaysCredentialExpirationRemainders() {
		System.out.println("credential expiration in 30 days remainder");
		this.sendCredentialExpirationRemainders(30);
	}

	@Scheduled(fixedRate = 86400000)
	public void send60DaysCredentialExpirationRemainders() {
		System.out.println("credential expiration in 60 days remainder");
		this.sendCredentialExpirationRemainders(60);

	}

	@Scheduled(fixedRate = 86400000)
	public void send90DaysCredentialExpirationRemainders() {
		System.out.println("credential expiration in 90 days remainder");
		this.sendCredentialExpirationRemainders(90);

	}

	@Scheduled(fixedRate = 86400000)
	public void sendExpiredCredentialsReminder() {
		Set<Long> expiredUserCredentials = userCredentialsDAO.findUserCredentialsByExpiredDate(LocalDate.now()).stream().map(e -> e.getUserId()).collect(Collectors.toSet());
		
		if (expiredUserCredentials.size() > 0) {
			inAppNotification.deleteBySourceId(Arrays.asList(Long.parseLong("0")));
			for (Long eachUser : expiredUserCredentials) {

				UserBasicInformation user = userRepository.findUsersByUserId(eachUser);

				if (user != null && user.getFcmToken() != null) {
					
					String message = "Don't forget to add your credentials!";
					String messageBody = ", please update your credentials in Float Care!";
					String deepLink = "10";
					
					messageBody = user.getFirstName() + messageBody;

					if (user.getFcmToken() != null)
						pushNotification.sendEventNotificationToUser(eachUser, user.getFcmToken(), message, messageBody, deepLink, user.getDeviceType(), null);
					
					inAppNotification.createInAppNotifications(eachUser, 3, message, messageBody, Long.getLong(deepLink), false, 0);

					messageBody = messageBody+" Click on the link below to go to the Floatcare Application.\n"+ ConstantUtils.branchIOMySchedule;
					EmailThread emailThread = new EmailThread(); // sending email
					emailThread.setEmails(user.getEmail());
					emailThread.setMessage(message);
					emailThread.setMessageBody(messageBody);
					applicationContext.getAutowireCapableBeanFactory().autowireBean(emailThread);

					Thread thread = new Thread(emailThread); // send email on separate thread.
					thread.start(); // start the thread
				}
			}
		}
	}

//	@Scheduled(fixedRate = 86400000)
	public void fetchNotFilledRequiredCredentialsOfUser() { // fetch all required credentials

		Set<Long> userCredentials = userCredentialsDAO.findUserCredentialsByIsRequiredAndIsPending(true, true).stream().map(e->e.getUserId()).collect(Collectors.toSet());

		if (userCredentials.size() > 0) {
			inAppNotification.deleteBySourceId(Arrays.asList(Long.parseLong("0")));
			for (Long eachUser : userCredentials) {

				UserBasicInformation user = userRepository.findUsersByUserId(eachUser);

				if (user != null && user.getFcmToken() != null) {
					
					String message = "Don't forget to add your credentials!";
					String messageBody = ", please update your credentials in Float Care!";
					String deepLink = "10";

					messageBody = user.getFirstName()+messageBody;

					if (user.getFcmToken() != null)
						pushNotification.sendEventNotificationToUser(eachUser, user.getFcmToken(), message, messageBody, deepLink, user.getDeviceType(), null);

					inAppNotification.createInAppNotifications(eachUser, 3, message, messageBody, 10, false, 0);

					messageBody = messageBody+" Click on the link below to go to the Floatcare Application.\n"+ ConstantUtils.branchIOMySchedule;
					EmailThread emailThread = new EmailThread(); // sending email
					emailThread.setEmails(user.getEmail());
					emailThread.setMessage(message);
					emailThread.setMessageBody(messageBody);
					applicationContext.getAutowireCapableBeanFactory().autowireBean(emailThread);

					Thread thread = new Thread(emailThread); // send email on separate thread.
					thread.start(); // start the thread
				}
			}
		}
	}
}