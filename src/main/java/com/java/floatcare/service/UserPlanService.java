package com.java.floatcare.service;

import java.util.Optional;

import com.java.floatcare.api.response.pojo.RestResponse;
import com.java.floatcare.model.UserPlan;

public interface UserPlanService {

	RestResponse registerPlan(UserPlan userPlan);
	RestResponse updatePlan(UserPlan userPlan);
	RestResponse getUserPlanDetails(long userId);
	Optional<UserPlan> findByPlanId(long planId);
	Optional<UserPlan> findByUserId(long userId);
}
