package com.java.floatcare.service;

import java.util.List;

import com.java.floatcare.model.Clients;
import com.java.floatcare.model.FamilyContacts;
import com.java.floatcare.model.Household;

public interface ClientsService {
	
	public Clients createClient (long organizationId, long businessId, String firstName, String lastName, String profilePhoto,String dateOfBirth, String phoneNumber, String medicareNumber,
				String medicaidNumber, String martialStatus, String religion, String ethnicity, String language, String status, List<Household> householdList);
	public Clients updateClient (long clientId,long organizationId, long businessId, String firstName, String lastName, String profilePhoto,String dateOfBirth, String phoneNumber, String medicareNumber,
			String medicaidNumber, String martialStatus, String religion, String ethnicity, String language, String status, List<Household> householdList);
	
	public FamilyContacts updateFamilyContact (long familyContactId, String firstName, String lastName, 
			String phoneNumber, String email, String relation, boolean isEmergencyContact) throws Exception;
	
	public boolean deleteClientProfile (long clientId);
	
	public boolean deleteFamilyContact (long familyContactId);
	
	public boolean deleteHouseHoldContact (long familyContactId);

}
