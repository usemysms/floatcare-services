package com.java.floatcare.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java.floatcare.api.request.pojo.DepartmentsList;
import com.java.floatcare.api.request.pojo.PermissionSet;
import com.java.floatcare.api.request.pojo.WorksitesList;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.model.UserPermissions;
import com.java.floatcare.repository.UserBasicInformationRepository;
import com.java.floatcare.repository.UserPermissionsRepository;

@Service
public class UserPermissionsServiceImpl implements UserPermissionsService{

	@Autowired
	private UserPermissionsRepository userPermissionsRepository;
	@Autowired
	private UserBasicInformationRepository userBasicInformationRepository;
	
	@Override
	public UserPermissions createUserPermissions(long userId, long organizationId, String accessLevel, List<Long> organizations, List<WorksitesList> worksiteIds,
			List<DepartmentsList> departmentIds, List<PermissionSet> permissionSet) {
		
		Optional<UserPermissions> foundUserPermissions = userPermissionsRepository.findById(userId);
		
		if (foundUserPermissions.isPresent()) { //update the record
			
			if (accessLevel != null)
				foundUserPermissions.get().setAccessLevel(accessLevel);
			if (organizations != null && organizations.size() > 0)
				foundUserPermissions.get().setOrganizationIds(organizations);
			//if (worksiteIds != null && worksiteIds.size() > 0)
				foundUserPermissions.get().setWorksiteIds(worksiteIds);
			//if (departmentIds != null && departmentIds.size() > 0)
				foundUserPermissions.get().setDepartmentIds(departmentIds);
			if (permissionSet != null && permissionSet.size() > 0)
				foundUserPermissions.get().setPermissionSet(permissionSet);
			
			userPermissionsRepository.save(foundUserPermissions.get());
			
			return foundUserPermissions.get();

		}else { // create a new record
			UserPermissions userPermission = new UserPermissions();

			userPermission.setUserId(userId);
			userPermission.setOrganizationId(organizationId);
			if (accessLevel != null)
				userPermission.setAccessLevel(accessLevel);
			if (organizations != null && organizations.size() > 0)
				userPermission.setOrganizationIds(organizations);
			if (worksiteIds != null && worksiteIds.size() > 0)
				userPermission.setWorksiteIds(worksiteIds);
			if (departmentIds != null && departmentIds.size() > 0)
				userPermission.setDepartmentIds(departmentIds);
			if (permissionSet != null && permissionSet.size() > 0)
				userPermission.setPermissionSet(permissionSet);

			userPermissionsRepository.save(userPermission);
			
			Optional<UserBasicInformation> optionalUser = userBasicInformationRepository.findUserByUserId(userId);
			if (optionalUser.isPresent()) {
				UserBasicInformation user = optionalUser.get();
				if (user.getUserTypeId() == 3) {
					long adminUserTypeId = 2;
					user.setUserTypeId(adminUserTypeId);
					userBasicInformationRepository.save(user);
				}
			}
          	return userPermission;
		}
	}
}
