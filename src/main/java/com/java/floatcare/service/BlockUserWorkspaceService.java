package com.java.floatcare.service;

import com.java.floatcare.model.BlockUserWorkspace;

public interface BlockUserWorkspaceService {

	public BlockUserWorkspace createBlockUserWorkspace (long workspaceId, long userId, long blockedUser);
	
	public boolean deleteBlockUserWorkspace (long blockedUser);
}
