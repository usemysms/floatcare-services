package com.java.floatcare.service;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.java.floatcare.api.response.pojo.RestResponse;
import com.java.floatcare.dao.CountersDAO;
import com.java.floatcare.model.UserPlan;
import com.java.floatcare.repository.UserPlanRepository;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class UserPlanServiceImpl implements UserPlanService {

	@Autowired
	private UserPlanRepository userPlanRepository;
	@Autowired
	private CountersDAO sequence;

	@Override
	public RestResponse registerPlan(UserPlan userPlan) {
		
		log.info("In UserPlanServiceImpl -> registerPlan {}");
		
		RestResponse restResponse = new RestResponse();

		findByUserId(userPlan.getUserId()).ifPresentOrElse((user) -> {
			restResponse.setMessage("User has already registered for plan "+user.getPlanType());
			restResponse.setSuccess(false);
			restResponse.setStatus(403);
		}, () -> {
			
			UserPlan plan = new UserPlan();
			setUserPlan(plan, userPlan);
			plan.setUserId(userPlan.getUserId());
			plan.setPlanId(sequence.getNextSequenceValue("planId"));
			userPlanRepository.save(plan);
			restResponse.setMessage("User registered for "+plan.getPlanType() + " successfully");
			restResponse.setSuccess(true);
			restResponse.setStatus(200);
		});
		log.info("Out UserPlanServiceImpl -> registerPlan {}");
		return restResponse;
	}

	@Override
	public RestResponse updatePlan(UserPlan userPlan) {
		
		log.info("In UserPlanServiceImpl -> updatePlan {}");
		
		RestResponse restResponse = new RestResponse();

		findByUserId(userPlan.getUserId()).ifPresentOrElse((user) -> {
			setUserPlan(user, userPlan);
			userPlanRepository.save(user);
			restResponse.setMessage("Updated plan successfully");
			restResponse.setSuccess(true);
			restResponse.setStatus(200);
		}, () -> {
			restResponse.setMessage("User is not exists");
			restResponse.setSuccess(false);
			restResponse.setStatus(403);
		});

		log.info("Out UserPlanServiceImpl -> updatePlan {}");
		
		return restResponse;
	}

	private void setUserPlan(UserPlan user, UserPlan userPlan) {
		
		log.info("In UserPlanServiceImpl -> setUserPlan {}");
		
		if(StringUtils.hasText(userPlan.getPlanType())) {
			user.setPlanType(userPlan.getPlanType());
			if (userPlan.getPlanType().equalsIgnoreCase("trial")) {
				if(userPlan.getExpirationDate() != null) {
					user.setExpirationDate(userPlan.getExpirationDate());
				} else {
					user.setExpirationDate(LocalDate.now().plusDays(90));
				}
			}
		}else {
			user.setPlanType("trial");
			user.setExpirationDate(LocalDate.now().plusDays(90));
		}
		
		if(userPlan.getReigisteredDate() != null) {
			user.setReigisteredDate(userPlan.getReigisteredDate());
		} else {
			user.setReigisteredDate(LocalDate.now());
		}
		
		if(userPlan.getPlanUpgradeDate() != null) {
			user.setPlanUpgradeDate(userPlan.getPlanUpgradeDate());
		} else {
			user.setPlanUpgradeDate(LocalDate.now());
		}
		
		if(StringUtils.hasText(userPlan.getActiveStatus())) {
			user.setActiveStatus(userPlan.getActiveStatus());
		} else {
			user.setActiveStatus("true");
		}
		
		log.info("Out UserPlanServiceImpl -> setUserPlan {}");
	}

	@Override
	public Optional<UserPlan> findByPlanId(long planId) {
		log.info("In UserPlanServiceImpl -> findByPlanId {}");
		return userPlanRepository.findByPlanId(planId);
	}

	@Override
	public Optional<UserPlan> findByUserId(long userId) {
		
		log.info("In UserPlanServiceImpl -> findByUserId {}");
		
		return userPlanRepository.findByUserId(userId);
	}

	@Override
	public RestResponse getUserPlanDetails(long userId) {
		
		log.info("In UserPlanServiceImpl -> getUserPlanDetails {}");

		RestResponse restResponse = new RestResponse();

		findByUserId(userId).filter(plan -> (getRemainingDays(plan) >= 0 && getRemainingDays(plan) < 90 && plan.getActiveStatus().equalsIgnoreCase("true")))
				.ifPresentOrElse((plan) -> {
					restResponse.setStatus(200);
					restResponse.setSuccess(true);
					restResponse.setMessage(getRemainingDays(plan)+" day(s) are remaining");
				}, () -> {

					restResponse.setStatus(403);
					restResponse.setSuccess(false);
					restResponse.setMessage("Plan is expired now");

				});
		
		log.info("Out UserPlanServiceImpl -> getUserPlanDetails {}");
		
		return restResponse;
	}

	private long getRemainingDays(UserPlan plan) {
		log.info("Plan upgrade date {}",plan.getPlanUpgradeDate());
		log.info("Current date {}",LocalDate.now());
		log.info("Plan expiration date {}",plan.getExpirationDate());
		long remainingDays = ChronoUnit.DAYS.between(LocalDate.now(), plan.getExpirationDate());
		log.info("Remaining Day(s) {}",remainingDays);
		return remainingDays;
	}

}
