package com.java.floatcare.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java.floatcare.dao.CountersDAO;
import com.java.floatcare.dao.UserNotificationDAO;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.model.UserNotification;
import com.java.floatcare.repository.NotificationTypesRepository;
import com.java.floatcare.repository.UserBasicInformationRepository;
import com.java.floatcare.repository.UserNotificationRepository;

@Service
public class UserNotificationServiceImpl implements UserNotificationService {

	@Autowired
	private UserNotificationRepository userNotificationRepository;
	@Autowired
	private CountersDAO sequence;
	@Autowired
	private UserNotificationDAO userNotificationDAO;
	@Autowired
	private NotificationTypesRepository notificationTypesRepository;
	@Autowired
	private UserBasicInformationRepository userBasicInformationRepository;

	public UserNotification createUserNotification(long userId, long notificationTypeId, boolean isMuteAllEnable, boolean isEnablePush, boolean isEnableSMS, boolean isEnableEmail) {
		
		List<UserNotification> user = new ArrayList<UserNotification>(); // to save notification settings in this list if settings already exists for a user
		
		UserNotification userNotification = new UserNotification();
		
		if (isMuteAllEnable == true) {					// all notificationTypes value will be set to false
			
			userNotificationRepository.deleteAllByUserId(userId);
				
			Optional<UserBasicInformation> userAvailable = userBasicInformationRepository.findByUserIdAndStatus(userId, "Active");
				
			if (userAvailable.isPresent()) {		//set muteAllNotification as true in user basic information
				UserBasicInformation userDetails = userAvailable.get();
				userDetails.setMuteAllNotifications(true);
				
				userBasicInformationRepository.save(userDetails);
				
				for	(int i = 1; i <= notificationTypesRepository.count() ; i++) {
						
					userNotification.setUserNotificationId(sequence.getNextSequenceValue("userNotificationId"));
					userNotification.setUserId(userId);
					userNotification.setNotificationTypeId(i);
					userNotification.setEnableEmail(false);
					userNotification.setEnablePush(false);
					userNotification.setEnableSMS(false);		
				
					userNotificationRepository.save(userNotification);
				}
			}
				return userNotification;
		}
			
		user = userNotificationDAO.findUserNotificationByUserIdAndNotificationTypeId(userId, notificationTypeId);
		
		if (user.size() > 0) { //to check if same setting is available for user
				
			userNotification.setUserNotificationId(user.get(0).getUserNotificationId());
			
			Optional<UserBasicInformation> userAvailable = userBasicInformationRepository.findByUserIdAndStatus(userId, "Active");
			
			if (userAvailable.isPresent()) {								// muteAllNotification will be disabled (false) for the user
				UserBasicInformation userDetails = userAvailable.get();
				userDetails.setMuteAllNotifications(false);
				
				userBasicInformationRepository.save(userDetails);
			}
			
		}
		else
			userNotification.setUserNotificationId(sequence.getNextSequenceValue("userNotificationId"));
			
		userNotification.setNotificationTypeId(notificationTypeId);
		userNotification.setEnableEmail(isEnableEmail);
		userNotification.setEnablePush(isEnablePush);
		userNotification.setEnableSMS(isEnableSMS);
		userNotification.setUserId(userId);
		
		userNotificationRepository.save(userNotification);
		
		return userNotification;

	}
}