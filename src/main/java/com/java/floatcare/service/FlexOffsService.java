package com.java.floatcare.service;

import java.text.ParseException;
import java.util.List;

import com.java.floatcare.model.FlexOffs;

public interface FlexOffsService {
	
	public FlexOffs createFlexOff (long worksiteId, long departmentId, long shiftTypeId, String onDate, String startTime, String endTime,
			long staffUserCoreTypeId, long quota, boolean isSendNotifications, boolean isAutoApprove, boolean fullFlexOff, String note, List<Integer> invitees) throws ParseException;
	
	public FlexOffs updateFlexOff (long userId, long flexOffId, boolean isAccepted, boolean isApproved, String status);
	
	public boolean deleteFlexOff (long flexOffId);

}
