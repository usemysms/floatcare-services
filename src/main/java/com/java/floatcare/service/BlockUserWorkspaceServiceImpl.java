package com.java.floatcare.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java.floatcare.dao.CountersDAO;
import com.java.floatcare.model.BlockUserWorkspace;
import com.java.floatcare.repository.BlockUserWorkspaceRepository;

@Service
public class BlockUserWorkspaceServiceImpl implements BlockUserWorkspaceService {

	@Autowired
	private CountersDAO sequence;
	@Autowired
	private BlockUserWorkspaceRepository blockUserWorkspaceRepository;
	
	@Override
	public BlockUserWorkspace createBlockUserWorkspace(long workspaceId, long userId, long blockedUserId) {
		
		BlockUserWorkspace blockUserWorkspace = new BlockUserWorkspace();
		
		blockUserWorkspace.setBlockUserWorkspaceId(sequence.getNextSequenceValue("blockUserWorkspaceId"));
		
		if (workspaceId > 0)
			blockUserWorkspace.setWorkspaceId(workspaceId);
		
		if (userId > 0)
			blockUserWorkspace.setUserId(userId);
		
		if (blockedUserId > 0)
			blockUserWorkspace.setBlockedUserId(blockedUserId);
		
		blockUserWorkspaceRepository.save(blockUserWorkspace);
		
		return blockUserWorkspace;
	}

	@Override
	public boolean deleteBlockUserWorkspace(long blockedUserId) {
		
		Optional<BlockUserWorkspace> blockUserWorkspaceOpt = blockUserWorkspaceRepository.findByBlockedUserId(blockedUserId);
		
		if (blockUserWorkspaceOpt.isPresent()) {
			blockUserWorkspaceRepository.deleteById(blockUserWorkspaceOpt.get().getBlockUserWorkspaceId());
			return true;
		}
		else
			return false; 		//if blockedUserId does not exist in block userWorkspace repository
		
	}

}
