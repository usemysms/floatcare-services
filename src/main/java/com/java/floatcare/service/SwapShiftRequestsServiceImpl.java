package com.java.floatcare.service;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java.floatcare.dao.CountersDAO;
import com.java.floatcare.model.Schedules;
import com.java.floatcare.model.SwapShiftRequests;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.repository.SchedulesRepository;
import com.java.floatcare.repository.SwapShiftRequestsRepository;
import com.java.floatcare.repository.UserBasicInformationRepository;
import com.java.floatcare.utils.GoogleFirebaseUtilityDAO;
import com.java.floatcare.utils.InAppNotifications;

@Service
public class SwapShiftRequestsServiceImpl implements SwapShiftRequestsService {

	@Autowired
	private SchedulesRepository schedulesRepository;
	@Autowired
	private SwapShiftRequestsRepository swapShiftRequestsRepository;
	@Autowired
	private CountersDAO sequence;
	@Autowired
	private GoogleFirebaseUtilityDAO flashNotification;
	@Autowired
	private UserBasicInformationRepository usersRepository;
	@Autowired
	private InAppNotifications inAppNotification;
	
	@Override
	public SwapShiftRequests createSwapShiftRequest(long senderId, long receiverId, long sourceShiftId, long requestedShiftId) {
		
		SwapShiftRequests swapShiftRequest = new SwapShiftRequests();
		
		if (senderId > 0)
			swapShiftRequest.setSenderId(senderId);
		
		if (sourceShiftId > 0)
			swapShiftRequest.setSourceShiftId(sourceShiftId);
		
		if (receiverId > 0)
			swapShiftRequest.setReceiverId(receiverId);
		
		if (requestedShiftId > 0)
			swapShiftRequest.setRequestedShiftId(requestedShiftId);
		
		swapShiftRequest.setStatus("pending");
		
		swapShiftRequest.setCreatedOn(LocalDate.now());
		
		if (swapShiftRequest.getSenderId() > 0 && swapShiftRequest.getReceiverId() > 0 && swapShiftRequest.getSourceShiftId() > 0 
					&& swapShiftRequest.getRequestedShiftId() > 0) {
			
			swapShiftRequest.setSwapShiftRequestId(sequence.getNextSequenceValue("swapShiftRequestId"));
			
			swapShiftRequestsRepository.save(swapShiftRequest);
			
			UserBasicInformation senderDetails = usersRepository.findById(senderId).get();
			UserBasicInformation receiverDetails = usersRepository.findById(receiverId).get();
			
			String message = "You've received a shift swap request";
			String messageBody = "You've received a shift swap request from "+senderDetails.getFirstName()+" "+senderDetails.getLastName()+ "! Visit the Float Care app to respond.";
			String deepLink = "3";

			if (receiverDetails.getFcmToken() != null)
				flashNotification.sendEventNotificationToUser(receiverId, receiverDetails.getFcmToken(), message, messageBody, deepLink,
						receiverDetails.getDeviceType(), null); //push notification.
			
			inAppNotification.createInAppNotifications(receiverId, 3, message, messageBody, 10, false, swapShiftRequest.getSwapShiftRequestId()); // in app notification.
		}
		return swapShiftRequest;
	}

	@Override
	public SwapShiftRequests updateSwapShiftRequest(long swapShiftRequestId, boolean isReceiverAccepted, boolean isSupervisorAccepted) throws Exception {
		
		Optional<SwapShiftRequests> swapShiftRequestOptional = swapShiftRequestsRepository.findById(swapShiftRequestId);
		
		if (swapShiftRequestOptional.isPresent() && (swapShiftRequestOptional.get().getStatus().equalsIgnoreCase("pending")
				|| swapShiftRequestOptional.get().getStatus().equalsIgnoreCase("admin_review"))) {
			
			SwapShiftRequests swapShiftRequest = swapShiftRequestOptional.get();
			
			long senderId =  swapShiftRequestOptional.get().getSenderId();
			
			if (String.valueOf(isReceiverAccepted) != null && isSupervisorAccepted == false) {
				
				swapShiftRequest.setReceiverAccepted(isReceiverAccepted);
				swapShiftRequest.setUpdatedOn(LocalDate.now());
				
				if (isReceiverAccepted == true)
					swapShiftRequest.setStatus("admin_review");
				else
					swapShiftRequest.setStatus("rejected_by_user");
				
				swapShiftRequestsRepository.save(swapShiftRequest);

				UserBasicInformation senderDetails = usersRepository.findById(senderId).get();

				String message = "We've received your shift swap request.";
				String messageBody = senderDetails.getFirstName()+", we have received your shift swap request.";
				
				if (senderDetails.getFcmToken() != null)
					flashNotification.sendEventNotificationToUser(senderId, senderDetails.getFcmToken(), message, messageBody, "10",
							senderDetails.getDeviceType(), null); //push notification.

				inAppNotification.removeByUserIdAndSourceId(swapShiftRequestOptional.get().getReceiverId(), swapShiftRequestId, "You've received a shift swap request");
				inAppNotification.createInAppNotifications(senderId, 3, message, messageBody, 10, false, swapShiftRequestId); // in app notification.

				return swapShiftRequest;
			}

			if (String.valueOf(isSupervisorAccepted) != null && swapShiftRequestOptional.get().isReceiverAccepted() == true) {

				swapShiftRequest.setSupervisorAccepted(isSupervisorAccepted);
				swapShiftRequest.setUpdatedOn(LocalDate.now());

				if (isSupervisorAccepted == true)
					swapShiftRequest.setStatus("approved");
				else
					swapShiftRequest.setStatus("rejected_by_admin");
				
				swapShiftRequestsRepository.save(swapShiftRequest);
				
				if (isSupervisorAccepted == true && swapShiftRequest.isReceiverAccepted() == true) {
					
					Optional<Schedules> sourceSchedule = schedulesRepository.findById(swapShiftRequestOptional.get().getSourceShiftId());
					Optional<Schedules> requestedSchedule = schedulesRepository.findById(swapShiftRequestOptional.get().getRequestedShiftId());
					
					UserBasicInformation senderDetails = usersRepository.findById(senderId).get();
					UserBasicInformation departmentSupervisor = usersRepository.findByOrganizationIdAndUserTypeId(senderDetails.getOrganizationId(), 2);
					String message = "Shift swap accepted!";
					String messageBody = "Your shift swap has been approved by "+departmentSupervisor.getFirstName()+ ". Visit the Float Care app to see your updated schedule!";
					String deepLink = "3"; // Deep Link for sending user to my request screen

					if (senderDetails.getFcmToken() != null) {
						flashNotification.sendEventNotificationToUser(senderId, senderDetails.getFcmToken(), message, messageBody, deepLink,
								senderDetails.getDeviceType(), null); //push notification.
					}
					inAppNotification.deleteBySourceId(Arrays.asList(swapShiftRequestId));
					inAppNotification.createInAppNotifications(senderId, 3, message, messageBody, 10, false, swapShiftRequestId); // in app notification.
					
					if (sourceSchedule.isPresent() && requestedSchedule.isPresent()) {
						
						long sourceUserId = sourceSchedule.get().getUserId();
						long requestedUserId = requestedSchedule.get().getUserId();

						// swap the schedules of the source and requested schedules.
						Schedules sourceScheduleModify = sourceSchedule.get();
						sourceScheduleModify.setUserId(requestedUserId);
						
						Schedules requestedScheduleModify = requestedSchedule.get();
						requestedScheduleModify.setUserId(sourceUserId);
						
						List<Schedules> swapSchedules = Arrays.asList(sourceScheduleModify, requestedScheduleModify);
						
						schedulesRepository.saveAll(swapSchedules);
					}
				}
			}
			return swapShiftRequest;
		} else
			throw new Exception("Swap Shift record does not exist");
	}

	@Override
	public boolean deleteSwapShiftRequest(long swapShiftRequestId) {
		
		swapShiftRequestsRepository.deleteById(swapShiftRequestId);
		inAppNotification.deleteBySourceId(Arrays.asList(swapShiftRequestId));
		return true;
	}
}
