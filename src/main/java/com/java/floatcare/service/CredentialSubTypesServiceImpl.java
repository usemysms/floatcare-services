package com.java.floatcare.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.java.floatcare.api.response.pojo.RequiredType;
import com.java.floatcare.dao.CountersDAO;
import com.java.floatcare.dao.UserWorkspacesDAO;
import com.java.floatcare.model.CredentialSubTypes;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.model.UserCredentials;
import com.java.floatcare.model.UserWorkspaces;
import com.java.floatcare.repository.CredentialSubTypeRepository;
import com.java.floatcare.repository.UserBasicInformationRepository;
import com.java.floatcare.repository.UserCredentialsRepository;

@Component
public class CredentialSubTypesServiceImpl implements CredentialSubTypesService {

	@Autowired
	private CountersDAO sequence;
	@Autowired
	private CredentialSubTypeRepository credentialSubTypeRepository;
	@Autowired
	private UserBasicInformationRepository userBasicInformationRepository;
	@Autowired
	private UserCredentialsRepository userCredentialsRepository;
	@Autowired
	private UserWorkspacesDAO userWorkspacesDAO;

	@Override
	public CredentialSubTypes createCredentialSubTypes(long credentialSubTypeId, long organizationId,
			long credentialTypeId, String label, List<RequiredType> isRequired, List<String> states,
			String credentialType, long creatorId, String status, String acronym) {

		CredentialSubTypes credentialSubTypes;
		List<Long> addingToGroup = new ArrayList<Long>();

		if (credentialSubTypeId > 0) {

			Optional<CredentialSubTypes> credentialSubTypesOptional = credentialSubTypeRepository
					.findByIdAndOrganizationId(credentialSubTypeId, organizationId);

			if (credentialSubTypesOptional.isPresent()) {
				credentialSubTypes = credentialSubTypesOptional.get();
			} else {
				Optional<CredentialSubTypes> credentialSubTypesOptionalById = credentialSubTypeRepository
						.findById(credentialSubTypeId);
				CredentialSubTypes existingCredentialSubTypes = credentialSubTypesOptionalById.get();
				// If the account owner is updating the credential which was created by the
				// super owner then we are
				// adding that account owners organization id into the removedFromOrganization
				// list in order to avoid duplication
				if (organizationId > 0) {
					if (existingCredentialSubTypes.getRemovedFromOrganizations() != null) {
						addingToGroup = existingCredentialSubTypes.getRemovedFromOrganizations().stream()
								.collect(Collectors.toList());
					}
					addingToGroup.add(organizationId);
					existingCredentialSubTypes.setRemovedFromOrganizations(addingToGroup);
					credentialSubTypeRepository.save(existingCredentialSubTypes);
				}
				credentialSubTypes = new CredentialSubTypes();
				credentialSubTypes.setCredentialSubTypeId(sequence.getNextSequenceValue("credentialSubTypeId"));
			}

		} else {
			credentialSubTypes = new CredentialSubTypes();
			credentialSubTypes.setCredentialSubTypeId(sequence.getNextSequenceValue("credentialSubTypeId"));

		}

		if (credentialTypeId > 0)
			credentialSubTypes.setCredentialTypeId(credentialTypeId);
		credentialSubTypes.setCreatorId(creatorId);
		credentialSubTypes.setOrganizationId(organizationId);

		if (label != null)
			credentialSubTypes.setLabel(label);
		else
			credentialSubTypes.setLabel(null);

		// Removing the duplicate objects from the list
		HashSet<Object> seen = new HashSet<>();
		isRequired.removeIf(e -> !seen.add(e.getStaffCoreTypeId()));

		if (isRequired != null && isRequired.size() > 0)
			credentialSubTypes.setRequiredType(isRequired);
		else
			credentialSubTypes.setRequiredType(null);

		if (states != null)
			credentialSubTypes.setStates(states);
		else
			credentialSubTypes.setStates(null);

		if (credentialType != null)
			credentialSubTypes.setCredentialType(credentialType);
		else
			credentialSubTypes.setCredentialType(null);

		if (status != null)
			credentialSubTypes.setStatus(status);
		else {
			Optional<UserBasicInformation> optionalUser = userBasicInformationRepository.findUserByUserId(creatorId);
			if (optionalUser.isPresent()) {
				UserBasicInformation user = optionalUser.get();
				if (user.getUserTypeId() == 1)
					credentialSubTypes.setStatus("active");
				else
					credentialSubTypes.setStatus("pending");
			}
		}

		credentialSubTypes.setRemovedFromOrganizations(null);

		if (acronym != null)
			credentialSubTypes.setAcronym(acronym);
		else
			credentialSubTypes.setAcronym(null);

		credentialSubTypeRepository.save(credentialSubTypes);

		// Adding the created credentialSubType to the existing staff users
		List<Long> staffUserCoreTypeIds = new ArrayList<Long>();
		for (RequiredType requiredType : isRequired) {
			staffUserCoreTypeIds.add(requiredType.getStaffCoreTypeId());
		}
		List<UserWorkspaces> userWorkSpaces = userWorkspacesDAO
				.findByStaffUserCoreTypeIdAndOrganizationId(staffUserCoreTypeIds, organizationId);
		HashSet<Object> distinctObjects = new HashSet<>();
		userWorkSpaces.removeIf(e -> !distinctObjects.add(e.getUserId()));

		for (UserWorkspaces userWorkSpace : userWorkSpaces) {

			List<UserCredentials> userCredentials = userCredentialsRepository.findByUserIdAndCredentialSubTypeId(
					userWorkSpace.getUserId(), credentialSubTypes.getCredentialSubTypeId());
			if (userCredentials.size() > 0) {
				continue; // Not inserting into the user credentials if the record is already available
							// with the user
			}

			UserCredentials userCredential = new UserCredentials();
			userCredential.setUserId(userWorkSpace.getUserId());
			userCredential.setUserCredentialId(sequence.getNextSequenceValue("userCredentialId"));
			userCredential.setPending(true);
			userCredential.setGenerated(true);
			userCredential.setCredentialTypeId(credentialSubTypes.getCredentialTypeId());
			userCredential.setCredentialSubTypeId(credentialSubTypes.getCredentialSubTypeId());
			Optional<RequiredType> requiredTypeOptional = isRequired.stream()
					.filter(item -> item != null && item.getStaffCoreTypeId() == userWorkSpace.getStaffUserCoreTypeId())
					.findFirst();
			if (requiredTypeOptional.isPresent()) {
				userCredential.setRequired(requiredTypeOptional.get().getIsRequired());
			}
			userCredentialsRepository.save(userCredential);
		}

		return credentialSubTypes;
	}

	@Override
	public boolean deleteCredentialSubTypes(long credentialSubTypeId) {

		credentialSubTypeRepository.deleteById(credentialSubTypeId);
		return false;
	}
}
