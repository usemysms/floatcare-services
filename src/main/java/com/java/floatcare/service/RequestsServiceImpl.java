package com.java.floatcare.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java.floatcare.dao.CountersDAO;
import com.java.floatcare.dao.RequestsDAO;
import com.java.floatcare.dao.ScheduleDAO;
import com.java.floatcare.model.Requests;
import com.java.floatcare.model.Schedules;
import com.java.floatcare.model.UserWorkspaces;
import com.java.floatcare.repository.RequestsRepository;
import com.java.floatcare.repository.UserWorkspacesRepository;

@Service
public class RequestsServiceImpl implements RequestsService {

	@Autowired
	private UserWorkspacesRepository userWorkspacesRepository;
	@Autowired
	private RequestsRepository requestsRepository;
	@Autowired
	private RequestsDAO requestsDAO;
	@Autowired
	private ScheduleDAO schedulesDAO;
	@Autowired
	private CountersDAO sequence;

	@Override
	public Requests createARequest(long worksiteId, long departmentId, long requestTypeId, long userId, List<String> onDate, List<Long> shiftIds,
			long colleagueId, String notes) {

		Requests requests = new Requests();

		for (String eachDate : new ArrayList<>(onDate)) { // check if the user already as marked the date for request

			List<Requests> request = requestsDAO.findByUserIdAndOnDateAndDepartmentId(userId, LocalDate.parse(eachDate), departmentId);

			if (request.size() > 0 && request.get(0).getStatus().equals("Pending"))
				onDate.remove(eachDate);
		}

		List<LocalDate> selectedDays = new ArrayList<LocalDate>();
		
		if (worksiteId > 0)
			requests.setWorksiteId(worksiteId);
		
		if (departmentId > 0)
			requests.setDepartmentId(departmentId);
		
		if (requestTypeId > 0)
			requests.setRequestTypeId(requestTypeId);
		
		if (userId > 0)
			requests.setUserId(userId);
		
		if (onDate != null) {
			for (String list : onDate) {
				if(selectedDays.size() == 0 )
					selectedDays.add(LocalDate.parse(list));
				else if(!(selectedDays.contains(LocalDate.parse(list))))
					selectedDays.add(LocalDate.parse(list));

			}
			requests.setOnDate(selectedDays);
		}
		else
			requests.setOnDate(null);
	
		UserWorkspaces userWorkspace = userWorkspacesRepository.findByUserIdAndWorkspaceIdAndStatusAndIsJoined(userId, departmentId, "Active", true);
		
		if (userWorkspace != null && userWorkspace.isPrimaryDepartment() == true) {
			
			requests.setShiftIds(userWorkspace.getDepartmentShiftIds());
		}
		else { // for secondary department user, fetch the shift from schedule
			Schedules schedules = schedulesDAO.findByUserIdAndDepartmentId(userId, departmentId);
			List<Long> shiftsList = new ArrayList<Long>();
			if (schedules != null) {
				
				shiftsList.add(schedules.getShiftTypeId());
				requests.setShiftIds(shiftsList);
			}
			else {
				requests.setShiftIds(shiftsList);
			}
		}
		
		if (colleagueId > 0)
			requests.setColleagueId(colleagueId);
		
		if (notes != null)
			requests.setNotes(notes);
		else
			requests.setNotes(null);
		
		requests.setStatus("Pending");

		if (requests.getUserId() > 0 && requests.getOnDate().size() > 0) {
			requests.setRequestId(sequence.getNextSequenceValue("requestId"));
			requestsRepository.save(requests);
		}

		return requests;
	}

	@Override
	public Requests updateARequest(long requestId, long worksiteId, long departmentId, long requestTypeId, long userId, List<String> onDate,
			List<Long> shiftIds, long colleagueId, String notes, String status) throws Exception {

		Optional<Requests> requestsOpt = requestsRepository.findById(requestId);
		
		if (requestsOpt.isPresent()) { // check if the user already as marked the date for request
			
			for (String eachDate : new ArrayList<>(onDate)) {
				
				List<Requests> request = requestsDAO.findByUserIdAndOnDateAndDepartmentId(userId, LocalDate.parse(eachDate), departmentId);
				
				if (request.size() > 0 && request.get(0).getStatus().equals("Pending"))
					onDate.remove(eachDate);
			}

			Requests requests = requestsOpt.get();
			
			List<LocalDate> selectedDays = new ArrayList<LocalDate>();

			if ( worksiteId > 0)
				requests.setWorksiteId(worksiteId);
			
			if (departmentId > 0)
				requests.setDepartmentId(departmentId);
			
			if (requestTypeId > 0)
				requests.setRequestTypeId(requestTypeId);
			
			if (userId > 0)
				requests.setUserId(userId);
			
			if (onDate != null) {
				
				requests.getOnDate().clear();
				for (String list : onDate)
					selectedDays.add(LocalDate.parse(list));
				requests.setOnDate(selectedDays);
			}
			
			if (shiftIds !=null)
				requests.setShiftIds(shiftIds);
			
			if (colleagueId > 0)
				requests.setColleagueId(colleagueId);
			
			if(notes != null)
				requests.setNotes(notes);
			
			if (status != null)
				requests.setStatus(status);
			
			if (requests.getUserId()>0 && requests.getOnDate().size() > 0)
				requestsRepository.save(requests);
			
			return requests;
		}

		throw new Exception("Request does not exists");
	}

	@Override
	public boolean deleteARequest(long requestId) {
		
		requestsRepository.deleteById(requestId);
		
		return true;
	}
}