package com.java.floatcare.service;

import java.time.LocalDate;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java.floatcare.dao.CountersDAO;
import com.java.floatcare.model.UserEducation;
import com.java.floatcare.repository.UserEducationRepository;

@Service
public class UserEducationServiceImpl implements UserEducationService {

	@Autowired
	private UserEducationRepository userEducationRepository;
	@Autowired
	private CountersDAO sequence;

	
	@Override
	public UserEducation createUserEducation(long userId, String school, String degreeTypeId, String degreeTypeName, String degreeOfStudy, String graduationDate,
			String location, String state) {

		UserEducation userEducation = new UserEducation();

		userEducation.setUserEducationId(sequence.getNextSequenceValue("userEducationId"));
		
		userEducation.setUserId(userId);
		
		if (school != null)
			userEducation.setSchool(school);
		else
			userEducation.setSchool(null);
		
		//if (degreeTypeId > 0)
			userEducation.setDegreeTypeId(degreeTypeId);

		if (degreeTypeName != null)
			userEducation.setDegreeTypeName(degreeTypeName);
		else
			userEducation.setDegreeTypeName(null);
		
		if (degreeOfStudy != null)
            userEducation.setDegreeOfStudy(degreeOfStudy);
		else
			userEducation.setDegreeOfStudy(null);
		
		if (graduationDate != null)
			userEducation.setGraduationDate(LocalDate.parse(graduationDate));
		else
			userEducation.setGraduationDate(null);
		
		if (location != null)
			userEducation.setLocation(location);
		else
			userEducation.setLocation(null);
		
		if (state != null)
			userEducation.setState(state);
		else
			userEducation.setState(null);

		userEducationRepository.save(userEducation);

		return userEducation;
	}

	@Override
	public UserEducation updateUserEducation(long userEducationId, long userId, String school, String degreeTypeId, String degreeTypeName, String degreeOfStudy, String graduationDate,
			String location, String state) throws Exception {

		Optional<UserEducation> optUserEducation = userEducationRepository.findById(userEducationId);

		if (optUserEducation.isPresent()) {
			UserEducation userEducation = optUserEducation.get();

			if (school != null)
				userEducation.setSchool(school);
			
			//if (degreeTypeId > 0)
				userEducation.setDegreeTypeId(degreeTypeId);

	        if (degreeOfStudy != null)
	            userEducation.setDegreeOfStudy(degreeOfStudy);
			
			//if (degreeTypeId > 0)
				userEducation.setDegreeTypeId(degreeTypeId);
			
			if (degreeTypeName != null)
				userEducation.setDegreeTypeName(degreeTypeName);

			if (graduationDate != null)
				userEducation.setGraduationDate(LocalDate.parse(graduationDate));
			
			if (location != null)
				userEducation.setLocation(location);
			
			if (state != null)
				userEducation.setState(state);
			
			userEducationRepository.save(userEducation);
			
			return userEducation;
		}
		throw new Exception("Not found any User Education to update!");
	}

	@Override
	public boolean deleteUserEducation(long userEducationId) {

		userEducationRepository.deleteById(userEducationId);
		return true;
	}

}
