package com.java.floatcare.service;

import com.java.floatcare.model.WorksiteSettings;

public interface WorksiteSettingsService {

	WorksiteSettings createWorksiteSettings(long userId, long worksiteId, long organizationId, String color);
	
	WorksiteSettings updateWorksiteSettings(long worksiteSettingId, long userId, long worksiteId, long organizationId, String color) throws Exception;
}
