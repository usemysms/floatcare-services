package com.java.floatcare.service;

import java.util.List;

import com.java.floatcare.api.response.pojo.UnreadMessagesAndNotificationsCount;
import com.java.floatcare.model.Messages;

public interface MessageService {

	Messages createMessages(String text, Long senderId, Long receiverId, String timeStamp, String timeZone,
			boolean isSeen, String date, long epochSeconds, String aesKey, String initializationVector);

	Messages updateMessage (long messageId, long userId, boolean isSeen);

	boolean deleteMessagesOfUser(long senderId, long receiverId);

	Messages createAGroupMessage(long chatGroupId, String text, long senderId, String timeStamp, String timeZone,
			String date, long epochSeconds, String aesKey, String initializationVector, String type,
			List<String> attachments, String sourceType);

	boolean deleteGroupChats(long groupId, long userId);

	boolean deleteSingleMessageForAUserInGroup(long groupId, long userId, long messageId);

	boolean deleteSingleMessageForAUserInPrivateMessage(long receiverId, long userId, long messageId);

	UnreadMessagesAndNotificationsCount getUnreadMessagesAndNotificationsCount(long userId);

}
