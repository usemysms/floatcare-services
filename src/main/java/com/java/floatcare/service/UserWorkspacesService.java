package com.java.floatcare.service;

import java.io.IOException;
import java.util.List;

import org.springframework.stereotype.Service;

import com.java.floatcare.api.request.HealthInsurances;
import com.java.floatcare.api.request.pojo.UserAvailability;
import com.java.floatcare.model.UserOrganizations;
import com.java.floatcare.model.UserWorkspaces;

@Service
public interface UserWorkspacesService {

	public UserWorkspaces createUserWorkspace(long userId, String workspaceType, long workspaceId, 
			String fteStatus, boolean isChargeRole, boolean isPreceptorRole, long organizationId, long businessId, 
            String scheduledHours, List<Long> departmentShiftId, long staffUserCoreTypeId, boolean isPrimaryDepartment, long subCoreTypeId, int sendsms, List<UserAvailability> userAvailability) throws IOException;

	public UserWorkspaces updateUserWorkspace(long userWorkspaceId, long userId, String workspaceType,
			long workspaceId, String fteStatus, String invitationCode, boolean isChargeRole, boolean isPreceptorRole, long organizationId, 
            long businessId, List<Long> departmentShiftId, long staffUserCoreTypeId, boolean isPrimaryDepartment, long subCoreTypeId, String status, List<UserAvailability> userAvailability, boolean isJoined) throws Exception;

	UserOrganizations updateUserOrganization(long userId, long organizationId, List<HealthInsurances> healthInsurances);
}