package com.java.floatcare.service;

import java.util.List;

import com.java.floatcare.model.ScheduleLayout;

public interface ScheduleLayoutService {

	public ScheduleLayout createScheduleLayout (long departmentId, String shiftPlanningStartDate, String scheduleStartDate, String scheduleLength,	
			String shiftPlanningPhaseLength, String makeStaffAvailabilityDeadLineLength, String maxQuota, 
			List<Long> staffCoreTypes, List<Long> shiftTypes, boolean autoAddProfessionalsToLayout) throws Exception;

	ScheduleLayout updateScheduleLayout(long scheduleLayoutId, long departmentId, String shiftPlanningStartDate, String scheduleStartDate, String scheduleLength, 
			String shiftPlanningPhaseLength, String makeStaffAvailabilityDeadLineLength, String maxQuota, 
			List<Long> staffCoreTypes, List<Long> shiftTypes, boolean autoAddProfessionalsToLayout) throws Exception;

	boolean deleteScheduleLayout(long scheduleLayoutId);


}
