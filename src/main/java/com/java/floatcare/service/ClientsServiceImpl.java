package com.java.floatcare.service;

import java.io.File;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.java.floatcare.dao.CountersDAO;
import com.java.floatcare.model.Clients;
import com.java.floatcare.model.FamilyContacts;
import com.java.floatcare.model.Household;

import com.java.floatcare.utils.OpenStreetMapUtils;
import com.java.floatcare.utils.UploadImageDAO;

@Service
public class ClientsServiceImpl implements ClientsService {

	@Autowired
	private MongoTemplate mongoTemplate;
	@Autowired
	private CountersDAO sequence;
	@Autowired
	private UploadImageDAO uploadImageDAO;
	
	private String uniqueFileName;

	public String getUniqueFileName() {
		return uniqueFileName;
	}

	public void setUniqueFileName(String uniqueFileName) {
		this.uniqueFileName = uniqueFileName;
	}

	@Override
	public Clients createClient(long organizationId, long businessId, String firstName, String lastName, String profilePhoto,
			String dateOfBirth, String phoneNumber, String medicareNumber, String medicaidNumber, String martialStatus,
			String religion, String ethnicity, String language, String status, List<Household> householdList) {

		Clients client = new Clients();

		if (organizationId > 0)
			client.setOrganizationId(organizationId);

		if (businessId > 0)
			client.setBusinessId(businessId);

		if (firstName != null)
			if (firstName.length() > 0)
				client.setFirstName(firstName);
			else
				client.setFirstName(null);
		else
			client.setFirstName(null);

		if (lastName != null)
			if (lastName.length() > 0)
				client.setLastName(lastName);
			else
				client.setLastName(null);
		else
			client.setLastName(null);

		if (dateOfBirth != null)
			client.setDateOfBirth(LocalDate.parse(dateOfBirth));
		else
			client.setDateOfBirth(null);

		if (phoneNumber != null)
			if (phoneNumber.length() >= 10)
				client.setPhoneNumber(phoneNumber);
			else
				client.setPhoneNumber(null);
		else
			client.setPhoneNumber(null);

		if (medicaidNumber != null)
			if (medicaidNumber.length() >= 10)
				client.setMedicareNumber(medicaidNumber);
			else
				client.setMedicareNumber(null);
		else
			client.setMedicareNumber(null);

		if (medicaidNumber != null)
			if (medicaidNumber.length() >= 10)
				client.setMedicaidNumber(medicaidNumber);
			else
				client.setMedicaidNumber(null);
		else
			client.setMedicaidNumber(null);

		if (martialStatus != null)
			client.setMartialStatus(martialStatus);
		else
			client.setMartialStatus(null);

		if (religion != null)
			client.setReligion(religion);
		else
			client.setReligion(null);

		if (ethnicity != null)
			client.setEthnicity(ethnicity);
		else
			client.setEthnicity(null);

		if (language != null)
			client.setLanguage(language);
		else
			client.setLanguage(null);

		client.setStatus("active");

		client.setClientId(sequence.getNextSequenceValue("clientId"));
		
		if (profilePhoto != null) {
			if (profilePhoto.length() > 0) {
				String fileName = firstName + "_" + lastName + "_" + LocalDate.now().toString() + ".png";
				fileName = fileName.replaceAll("\\s", ""); // removes spaces
				boolean check = false;
				try {
					check = uploadImageDAO.uploadImageFile(profilePhoto, fileName, 7);
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (check) {
					client.setProfilePhoto(uniqueFileName);
					File fileToDelete = FileUtils.getFile(fileName);
					FileUtils.deleteQuietly(fileToDelete);
				}
			}
		} else {
			client.setProfilePhoto(null);
		}

		if (householdList.size() > 0) {

			for (Household eachHouseHold : householdList) {

				Household household = new Household();

				household.setHouseholdId(sequence.getNextSequenceValue("householdId"));

				household.setClientId(client.getClientId());

				if (eachHouseHold.getAddress() != null)
					household.setAddress(eachHouseHold.getAddress());

				if (eachHouseHold.getAddress2() != null)
					household.setAddress2(eachHouseHold.getAddress2());

				if (eachHouseHold.getCountry() != null)
					household.setCountry(eachHouseHold.getCountry());
				else
					household.setCountry(null);

				if (eachHouseHold.getEmail() != null)
					household.setEmail(eachHouseHold.getEmail());
				else
					household.setEmail(null);

				if (eachHouseHold.getPhoneNumber() != null)
					household.setPhoneNumber(eachHouseHold.getPhoneNumber());
				else
					household.setPhoneNumber(null);

				if (eachHouseHold.getAddress() != null && eachHouseHold.getState() != null && eachHouseHold.getCountry() != null) {
					
					Map<String, Double> coords;
			        coords = OpenStreetMapUtils.getInstance().getCoordinates(eachHouseHold.getAddress()+" "+eachHouseHold.getAddress2()+" "
			        	+eachHouseHold.getState()+", "+eachHouseHold.getCountry()); //fetch the coordinates

			        if (coords.size() > 0) {
		        		household.setLongitude(""+coords.get("lat"));
				        household.setLatitude(""+coords.get("lon"));
			        }
			        else {
			        	coords = OpenStreetMapUtils.getInstance().getCoordinates(eachHouseHold.getState()+", "+eachHouseHold.getCountry());
			        	
			        	if (coords.size() > 0) {
			        		household.setLongitude(""+coords.get("lat"));
					        household.setLatitude(""+coords.get("lon"));
			        	} 
			        	else {
			        		coords = OpenStreetMapUtils.getInstance().getCoordinates(eachHouseHold.getAddress2()+" "+eachHouseHold.getState()+", "+eachHouseHold.getCountry());
			        		
			        		if (coords.size() > 0) {
				        		household.setLongitude(""+coords.get("lat"));
						        household.setLatitude(""+coords.get("lon"));
				        	}
			        	}
			        }
				}
				
				if (eachHouseHold.getState() != null)
					household.setState(eachHouseHold.getState());
				else
					household.setState(null);

				if (eachHouseHold.getZip() != null)
					household.setZip(eachHouseHold.getZip());
				else
					household.setZip(null);	

				if (eachHouseHold.getFamilyContacts().size() > 0) {
					
					for (FamilyContacts eachFamilyContact : eachHouseHold.getFamilyContacts()) {
						
						FamilyContacts familyContact = new FamilyContacts();

						familyContact.setFamilyContactId(sequence.getNextSequenceValue("familyContactId"));

						familyContact.setClientId(client.getClientId());

						if (eachFamilyContact.getEmail() != null)
							familyContact.setEmail(eachFamilyContact.getEmail());
						else
							familyContact.setEmail(null);

						if (eachFamilyContact.getFirstName() != null)
							familyContact.setFirstName(eachFamilyContact.getFirstName());
						else
							familyContact.setFirstName(null);

						if (eachFamilyContact.getLastName() != null)
							familyContact.setLastName(eachFamilyContact.getLastName());
						else
							familyContact.setLastName(null);

						if (eachFamilyContact.getPhoneNumber() != null)
							familyContact.setPhoneNumber(eachFamilyContact.getPhoneNumber());
						else
							familyContact.setPhoneNumber(null);

						if (eachFamilyContact.getRelation() != null)
							familyContact.setRelation(eachFamilyContact.getRelation());
						else
							familyContact.setRelation(null);

						familyContact.setIsEmergencyContact(eachFamilyContact.getIsEmergencyContact());

						mongoTemplate.save(familyContact, "family_contact");
					}
				}

				/*else
					household.setFamilyContacts(null);*/

				mongoTemplate.save(household, "household");

			}
		} else
			client.setHousehold(null);

		mongoTemplate.save(client, "clients");

		return client;
	}

	@Override
	public FamilyContacts updateFamilyContact(long familyContactId, String firstName, String lastName, 
			String phoneNumber, String email, String relation, boolean isEmergencyContact) throws Exception {
		
		FamilyContacts familyContactOptional = mongoTemplate.findOne(Query.query(Criteria.where("familyContactId").is(familyContactId)), FamilyContacts.class);
		
		if (familyContactOptional != null) {
			
			FamilyContacts familyContact = familyContactOptional;
			
			if (firstName != null)
				familyContact.setFirstName(firstName);
			
			if (lastName != null)
				familyContact.setLastName(lastName);
			
			if (phoneNumber != null)
				familyContact.setPhoneNumber(phoneNumber);
			
			if (email != null)
				familyContact.setEmail(email);
			
			if (relation != null)
				familyContact.setRelation(relation);
			
			if (familyContact.getIsEmergencyContact() != isEmergencyContact)
				familyContact.setIsEmergencyContact(isEmergencyContact);
			
			mongoTemplate.save(familyContact, "family_contact");
			
			return familyContact;
		}
		else {
			throw new Exception("familyContactId not found.");
		}
	}

	@Override
	public boolean deleteClientProfile(long clientId) {
		
		mongoTemplate.remove(Query.query(Criteria.where("_id").is(clientId)), "clients");
		return true;
	}

	@Override
	public boolean deleteFamilyContact(long clientId) {
		
		mongoTemplate.remove(Query.query(Criteria.where("clientId").is(clientId)), "family_contact");
		return true;
	}
	@Override
	public boolean deleteHouseHoldContact(long clientId) {
		
		mongoTemplate.remove(Query.query(Criteria.where("clientId").is(clientId)), "household");
		return true;
	}
	public Clients updateClient(long clientId, long organizationId, long businessId, String firstName, String lastName, String profilePhoto,
			String dateOfBirth, String phoneNumber, String medicareNumber, String medicaidNumber, String martialStatus,
			String religion, String ethnicity, String language, String status, List<Household> householdList) {
		
		Clients clientOptional = mongoTemplate.findOne(Query.query(Criteria.where("_id").is(clientId)), Clients.class);

		if (clientOptional != null) {

			Clients client = clientOptional;

			this.deleteFamilyContact(clientId);
	
			if (organizationId > 0)
				client.setOrganizationId(organizationId);
	
			if (businessId > 0)
				client.setBusinessId(businessId);
	
			if (firstName != null)
				if (firstName.length() > 0)
					client.setFirstName(firstName);
	
			else
				client.setFirstName(null);
	
			if (lastName != null)
				if (lastName.length() > 0)
					client.setLastName(lastName);
	
			if (dateOfBirth != null)
				client.setDateOfBirth(LocalDate.parse(dateOfBirth));
	
			if (phoneNumber != null)
				if (phoneNumber.length() >= 10)
					client.setPhoneNumber(phoneNumber);
	
			if (medicareNumber != null)
				if (medicareNumber.length() >= 10)
					client.setMedicareNumber(medicareNumber);
	
			if (medicaidNumber != null)
				
				if (medicaidNumber.length() >= 10)
					client.setMedicaidNumber(medicaidNumber);
	
			if (martialStatus != null)
				client.setMartialStatus(martialStatus);
	
			if (religion != null)
				client.setReligion(religion);
	
			if (ethnicity != null)
				client.setEthnicity(ethnicity);
	
			if (language != null)
				client.setLanguage(language);
	
			client.setStatus("active");
	
			//client.setClientId(sequence.getNextSequenceValue("clientId"));
			
			if (profilePhoto != null) {
				if (profilePhoto.length() > 0) {
					String fileName = firstName + "_" + lastName + "_" + LocalDate.now().toString() + ".png";
					fileName = fileName.replaceAll("\\s", ""); // removes spaces
					boolean check = false;
					try {
						check = uploadImageDAO.uploadImageFile(profilePhoto, fileName, 7);
					} catch (Exception e) {
						e.printStackTrace();
					}
	
					if (check) {
						client.setProfilePhoto(uniqueFileName);
						File fileToDelete = FileUtils.getFile(fileName);
						FileUtils.deleteQuietly(fileToDelete);
					}
				}
			}
	
			if (householdList.size() > 0) {
				
				this.deleteHouseHoldContact(client.getClientId());
	
				for (Household eachHouseHold : householdList) {
	
					Household household = new Household();
	
					household.setHouseholdId(sequence.getNextSequenceValue("householdId"));
	
					household.setClientId(client.getClientId());
	
					if (eachHouseHold.getAddress() != null)
						household.setAddress(eachHouseHold.getAddress());
	
					if (eachHouseHold.getAddress2() != null)
						household.setAddress2(eachHouseHold.getAddress2());
	
					if (eachHouseHold.getCountry() != null)
						household.setCountry(eachHouseHold.getCountry());
	
					if (eachHouseHold.getEmail() != null)
						household.setEmail(eachHouseHold.getEmail());
	
					if (eachHouseHold.getPhoneNumber() != null)
						household.setPhoneNumber(eachHouseHold.getPhoneNumber());
	
					if (eachHouseHold.getAddress() != null && eachHouseHold.getState() != null && eachHouseHold.getCountry() != null) {
						
						Map<String, Double> coords;
				        coords = OpenStreetMapUtils.getInstance().getCoordinates(eachHouseHold.getAddress()+" "+eachHouseHold.getAddress2()+" "
				        	+eachHouseHold.getState()+", "+eachHouseHold.getCountry()); //fetch the coordinates from address1, address2, state and country

				        if (coords.size() > 0) {
			        		household.setLongitude(""+coords.get("lat"));
					        household.setLatitude(""+coords.get("lon"));
				        }
				        else {// fetch by state and country
				        	coords = OpenStreetMapUtils.getInstance().getCoordinates(eachHouseHold.getState()+", "+eachHouseHold.getCountry());

				        	if (coords.size() > 0) {
				        		household.setLongitude(""+coords.get("lat"));
						        household.setLatitude(""+coords.get("lon"));
				        	} 
				        	else { // fetch by address 2, country and state
				        		coords = OpenStreetMapUtils.getInstance().getCoordinates(eachHouseHold.getAddress2()
				        				+" "+eachHouseHold.getState()+", "+eachHouseHold.getCountry()); // fetch by address 2 and state and country

				        		if (coords.size() > 0) {
					        		household.setLongitude(""+coords.get("lat"));
							        household.setLatitude(""+coords.get("lon"));
					        	}
				        	}
				        }
					}
					
					if (eachHouseHold.getState() != null)
						household.setState(eachHouseHold.getState());
	
					if (eachHouseHold.getZip() != null)
						household.setZip(eachHouseHold.getZip());
	
					if (eachHouseHold.getFamilyContacts().size() > 0) {
						
						for (FamilyContacts eachFamilyContact : eachHouseHold.getFamilyContacts()) {
							
							FamilyContacts familyContact = new FamilyContacts();
	
							familyContact.setFamilyContactId(sequence.getNextSequenceValue("familyContactId"));
	
							familyContact.setClientId(client.getClientId());
	
							if (eachFamilyContact.getEmail() != null)
								familyContact.setEmail(eachFamilyContact.getEmail());
	
							if (eachFamilyContact.getFirstName() != null)
								familyContact.setFirstName(eachFamilyContact.getFirstName());
	
							if (eachFamilyContact.getLastName() != null)
								familyContact.setLastName(eachFamilyContact.getLastName());
	
							if (eachFamilyContact.getPhoneNumber() != null)
								familyContact.setPhoneNumber(eachFamilyContact.getPhoneNumber());
	
							if (eachFamilyContact.getRelation() != null)
								familyContact.setRelation(eachFamilyContact.getRelation());
	
							familyContact.setIsEmergencyContact(eachFamilyContact.getIsEmergencyContact());
	
							mongoTemplate.save(familyContact, "family_contact");
						}
					}
	
					/*else
						household.setFamilyContacts(null);*/
					mongoTemplate.save(household, "household");
				}
			}
			mongoTemplate.save(client, "clients");
	
			return client;
		}else
			return null;
	}
}
