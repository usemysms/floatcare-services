package com.java.floatcare.service;

import java.util.List;

import com.java.floatcare.model.DepartmentShifts;

public interface DepartmentShiftsService {

	DepartmentShifts createDepartmentShift(long departmentId, String color, String icon, String startTime,
			String endTime, String label, List<Long> staffCoreTypes);

	DepartmentShifts updateDepartmentShift(long departmentShiftId, long departmentId, String color, String icon,
			String startTime, String endTime, String label, List<Long> staffCoreTypes) throws Exception;

	boolean deleteDepartmentShift(long departmentShiftId);

}
