package com.java.floatcare.service;

import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.java.floatcare.dao.CountersDAO;
import com.java.floatcare.dao.UserWorkspacesDAO;
import com.java.floatcare.model.DepartmentShifts;
import com.java.floatcare.repository.DepartmentShiftsRepository;

@Component
public class DepartmentShiftsServiceImpl implements DepartmentShiftsService {

	@Autowired
	private CountersDAO sequence;
	@Autowired
	private DepartmentShiftsRepository departmentShiftsRepository;
	@Autowired
	private UserWorkspacesDAO userWorkspacesDAO;

	@Override
	public DepartmentShifts createDepartmentShift(long departmentId, String color, String icon, String startTime,
			String endTime, String label,List<Long> staffCoreTypeIds) {

		DepartmentShifts departmentShifts = new DepartmentShifts();

		departmentShifts.setDepartmentShiftId(sequence.getNextSequenceValue("departmentShiftId"));

		if (departmentId > 0)
			departmentShifts.setDepartmentId(departmentId);

		if (color != null)
			departmentShifts.setColor(color);
		else
			departmentShifts.setColor(null);

		if (icon != null)
			departmentShifts.setIcon(icon);
		else
			departmentShifts.setIcon(null);

		if (startTime != null)
			departmentShifts.setStartTime(LocalTime.parse(startTime));
		else
			departmentShifts.setStartTime(null);

		if (endTime != null)
			departmentShifts.setEndTime(LocalTime.parse(endTime));
		else
			departmentShifts.setEndTime(null);

		if (label != null)
			departmentShifts.setLabel(label);
		if (staffCoreTypeIds != null)
			departmentShifts.setStaffCoreTypeIds(staffCoreTypeIds);
		
		departmentShifts.setStatus("Active");

		departmentShiftsRepository.save(departmentShifts);

		return departmentShifts;
	}

	@Override
	public DepartmentShifts updateDepartmentShift(long departmentShiftId, long departmentId, String color, String icon,
			String startTime, String endTime, String label,List<Long> staffCoreTypeIds) throws Exception {

		Optional<DepartmentShifts> departmentShiftOptional = departmentShiftsRepository.findById(departmentShiftId);

		if (departmentShiftOptional.isPresent()) {

			DepartmentShifts departmentShifts = departmentShiftOptional.get();

			if (departmentId > 0)
				departmentShifts.setDepartmentId(departmentId);

			if (color != null)
				departmentShifts.setColor(color);

			if (icon != null)
				departmentShifts.setIcon(icon);

			if (startTime != null)
				departmentShifts.setStartTime(LocalTime.parse(startTime));

			if (endTime != null)
				departmentShifts.setEndTime(LocalTime.parse(endTime));

			if (label != null)
				departmentShifts.setLabel(label);
			if (staffCoreTypeIds != null)
				departmentShifts.setStaffCoreTypeIds(staffCoreTypeIds);
			departmentShiftsRepository.save(departmentShifts);

			return departmentShifts;
		}
		throw new Exception("DepartmentShiftId does not exist");
	}

	@Override
	public boolean deleteDepartmentShift(long departmentShiftId) {

		Optional<DepartmentShifts> departmentShiftOptional = departmentShiftsRepository.findById(departmentShiftId);

		if (departmentShiftOptional.isPresent()) {

			departmentShiftOptional.get().setStatus("Deleted");
			departmentShiftsRepository.save(departmentShiftOptional.get());

			DepartmentShifts departmentShifts = departmentShiftsRepository.findFirstByIconAndDepartmentIdAndStatus(departmentShiftOptional.get().getIcon(), departmentShiftOptional.get().getDepartmentId(), "Active");

			if (departmentShifts != null) {//update the deleted departmentShiftId of users with new departmentShiftId

				long oldDepartmentShiftId = departmentShiftOptional.get().getDepartmentShiftId();
				long newDepartmentShiftId = departmentShifts.getDepartmentShiftId();

				userWorkspacesDAO.updateDepartmentShiftIdOfUsers(oldDepartmentShiftId, newDepartmentShiftId);
			}

			return true;
		}else
			return false;
	}
}