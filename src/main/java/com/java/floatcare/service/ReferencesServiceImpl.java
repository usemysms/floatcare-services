package com.java.floatcare.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.java.floatcare.dao.CountersDAO;
import com.java.floatcare.model.References;

import graphql.GraphQLException;

@Service
public class ReferencesServiceImpl implements ReferencesService{

	@Autowired
	private MongoTemplate mongoTemplate;
	@Autowired
	private CountersDAO sequence;
	
	@Override
	public References createReference(long referenceId, long userId, String name, String jobTitle, String phoneNumber, String email) {
		
		if (referenceId == 0) {
			
			References reference = new References();
			
			reference.setReferenceId(sequence.getNextSequenceValue("referenceId"));
			
			reference.setUserId(userId);
			
			if (name != null) 		
				reference.setName(name);
			
			if (jobTitle != null)
				reference.setJobTitle(jobTitle);
			
			if (phoneNumber != null)
				reference.setPhoneNumber(phoneNumber);
			
			if (email != null)
				reference.setEmailAddress(email);
			
			mongoTemplate.save(reference, "references");
			
			return reference;
		} else {
			
			References referenceOptional = mongoTemplate.findOne(Query.query(Criteria.where("referenceId").is(referenceId)), References.class);
			
			if (referenceOptional != null) {
				
				if (name != null)
					referenceOptional.setName(name);
				
				if (jobTitle != null)
					referenceOptional.setJobTitle(jobTitle);
				
				if (phoneNumber != null)
					referenceOptional.setPhoneNumber(phoneNumber);
				
				if (email != null)
					referenceOptional.setEmailAddress(email);
				
				mongoTemplate.save(referenceOptional, "references");
				
				return referenceOptional;
			}else
				throw new GraphQLException("referenceId not found.");
		}
	}

	@Override
	public boolean deleteReferences(long referenceId) {
		
		mongoTemplate.remove(Query.query(Criteria.where("referenceId").is(referenceId)), References.class);
		return true;
	}
}
