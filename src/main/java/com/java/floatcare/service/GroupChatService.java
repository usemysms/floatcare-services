package com.java.floatcare.service;

import java.util.List;

import com.java.floatcare.model.GroupChats;
import com.java.floatcare.model.Messages;

public interface GroupChatService {

	GroupChats createAGroup(long groupId, String groupName, long organizationId, List<Long> groupAdmins, List<Long> staffMembers, String createdOn) throws Exception;

	boolean removeGroupMember(long groupId, long adminStaffId, long staffId);

	boolean leaveAGroup(long groupId, long staffId);

	Messages updateUnSeenMessagesInGroup (long messageId, long staffId);

	boolean deleteAGroup(long chatGroupId, long staffUserId);

	GroupChats addAdminsInGroup(long chatGroupId, List<Long> adminIds, long adminId);

}
