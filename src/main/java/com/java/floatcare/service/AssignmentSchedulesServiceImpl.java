package com.java.floatcare.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import com.java.floatcare.dao.CountersDAO;
import com.java.floatcare.model.AssignmentInvitees;
import com.java.floatcare.model.AssignmentSchedules;
import com.java.floatcare.model.Assignments;
import com.java.floatcare.model.Businesses;
import com.java.floatcare.model.Clients;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.repository.AssignmentInviteesRepository;
import com.java.floatcare.repository.BusinessesRepository;
import com.java.floatcare.repository.UserBasicInformationRepository;
import com.java.floatcare.utils.ConstantUtils;
import com.java.floatcare.utils.EmailThread;
import com.java.floatcare.utils.GoogleFirebaseUtilityDAO;
import com.java.floatcare.utils.InAppNotifications;

import graphql.GraphQLException;

@Service
public class AssignmentSchedulesServiceImpl implements AssignmentSchedulesService {

	@Autowired
	private MongoTemplate mongoTemplate;
	@Autowired
	private CountersDAO sequence;
	@Autowired
	private GoogleFirebaseUtilityDAO flashNotification;
	@Autowired
	private UserBasicInformationRepository userRepository;
	@Autowired
	private AssignmentInviteesRepository assignmentInviteesRepository;
	@Autowired
	private BusinessesRepository businessesRepository;
	@Autowired
	private InAppNotifications inAppNotifications;
	@Autowired
	private ApplicationContext applicationContext;

	@Override
	public List<AssignmentSchedules> createAssignmentSchedules(long assignmentId, List<AssignmentSchedules> assignmentScheduleList) {

		List<AssignmentSchedules> assignmentScheduleLists = new ArrayList<>();

		for (AssignmentSchedules eachSchedules : assignmentScheduleList) {
			
			AssignmentSchedules assignmentSchedule = new AssignmentSchedules();

			assignmentSchedule.setAssignmentId(assignmentId);

			if (eachSchedules.getWeekDay() != null)
				assignmentSchedule.setWeekDay(eachSchedules.getWeekDay());
			else
				assignmentSchedule.setWeekDay(null);

			if (eachSchedules.getAcuityLevel() > 0)
				assignmentSchedule.setAcuityLevel(eachSchedules.getAcuityLevel());

			if (eachSchedules.getStartTime() != null)
				assignmentSchedule.setStartTime(eachSchedules.getStartTime());
			else
				assignmentSchedule.setStartTime(null);
			
			if (eachSchedules.getEndTime() != null)
				assignmentSchedule.setEndTime(eachSchedules.getEndTime());
			else
				assignmentSchedule.setEndTime(null);

			if (eachSchedules.getStaffCoreTypeId() > 0)
				assignmentSchedule.setStaffCoreTypeId(eachSchedules.getStaffCoreTypeId());
			
			if (eachSchedules.getSubCoreTypeId() > 0)
				assignmentSchedule.setSubCoreTypeId(eachSchedules.getSubCoreTypeId());

			if (eachSchedules.getStaff() != null && eachSchedules.getStaff().size() > 0) {
				assignmentSchedule.setStaff(eachSchedules.getStaff());
			}
			else
				assignmentSchedule.setStaff(null);

			for (Long eachStaff : eachSchedules.getStaff()) {

				if (mongoTemplate.findOne(Query.query(Criteria.where("staffId").is(eachStaff).and("assignmentId").is(assignmentId)), AssignmentInvitees.class) == null) {

					AssignmentInvitees assignmentInvitee = new AssignmentInvitees();

					assignmentInvitee.setAccepted(false);
					assignmentInvitee.setApproved(false);
					assignmentInvitee.setStaffId(eachStaff);
					assignmentInvitee.setAssignmentId(assignmentId);
					assignmentInvitee.setStaffCoreTypeId(eachSchedules.getStaffCoreTypeId());
					assignmentInvitee.setSubCoreTypeId(eachSchedules.getSubCoreTypeId());
					assignmentInvitee.setAssignmentInviteeId(sequence.getNextSequenceValue("assignmentInviteeId"));

					mongoTemplate.save(assignmentInvitee, "assignment_invitees");
					
					Assignments assignment = mongoTemplate.findOne(Query.query(Criteria.where("_id").is(assignmentId)), Assignments.class);
					Clients clientDetails = mongoTemplate.findOne(Query.query(Criteria.where("clientId").is(assignment.getClientId())), Clients.class);

					Optional<Businesses> hospital = businessesRepository.findById(assignment.getBusinessId());
					String hospitalName = null;

					if (hospital.isPresent())
						hospitalName = hospital.get().getName();
					else
						hospitalName = "Hospital";

					String message = "Assignment Created";
					String messageBody = "Assignment for the client, "+clientDetails.getFirstName()+" "+clientDetails.getLastName()+ ", has been created in "+hospitalName+ ". Please accept the response.";

					Optional<UserBasicInformation> userDetails = userRepository.findById(eachStaff);

					if (userDetails.isPresent() && userDetails.get().getFcmToken() != null)
						flashNotification.sendEventNotificationToUser(eachStaff, userDetails.get().getFcmToken(), message, messageBody, "3", userDetails.get().getDeviceType(), null);
					
					inAppNotifications.createInAppNotifications(eachStaff, 6, message, messageBody, 3, false, assignmentId);

					messageBody = messageBody + " Click on the link below \n"+ConstantUtils.branchIOMyRequest;
					try {
//						mailUtil.sendMail(userDetails.get().getEmail(), message, messageBody);
						EmailThread emailThread = new EmailThread();
						emailThread.setEmails(userDetails.get().getEmail());
						emailThread.setDynamicName(userDetails.get().getFirstName());
						emailThread.setMessage(message);
						emailThread.setMessageBody(messageBody);
						emailThread.setTemplate(false);
						
						applicationContext.getAutowireCapableBeanFactory().autowireBean(emailThread);

						Thread emailthread = new Thread(emailThread); // send email on separate thread.
						emailthread.start(); // start the email thread
					} catch (Exception e) {
						continue;
					}
				}else
					continue;
			}	

			assignmentSchedule.setAssignmentScheduleId(sequence.getNextSequenceValue("assignmentScheduleId"));

			assignmentScheduleLists.add(assignmentSchedule);

			mongoTemplate.save(assignmentSchedule, "assignment_schedules");

		}
		return assignmentScheduleLists;
	}

	@Override
	public AssignmentInvitees updateAssignmentInvitee(long assignmentId, long userId, boolean isAccepted, boolean isApproved) {

		if (userId > 0 && assignmentId > 0) {

			AssignmentInvitees assignmentInvitee = mongoTemplate.findOne(Query.query(Criteria.where("assignmentId").is(assignmentId))
							.addCriteria(Criteria.where("staffId").is(userId)), AssignmentInvitees.class);
			
			Assignments assignment = mongoTemplate.findOne(Query.query(Criteria.where("_id").is(assignmentId)), Assignments.class);

			Optional<Businesses> hospital = businessesRepository.findById(assignment.getBusinessId());
			String hospitalName = null;

			if (hospital.isPresent())
				hospitalName = hospital.get().getName();
			else
				hospitalName = "Hospital";
	
			UserBasicInformation userDetails = userRepository.findById(userId).get();
			Clients clientDetails = mongoTemplate.findOne(Query.query(Criteria.where("clientId").is(assignment.getClientId())), Clients.class);
			
			if (assignmentInvitee != null) {
				
				inAppNotifications.removeByUserIdAndSourceId(userId, assignmentId, "Assignment Created");
	
				if (isAccepted == true && isApproved == false)
					assignmentInvitee.setAccepted(true);
				
				else if (isAccepted == false && isApproved == true)
					assignmentInvitee.setApproved(true);
	
				else if (isAccepted == false && isApproved == false && assignmentInvitee.isAccepted() == true) {
					assignmentInvitee.setApproved(false);
					
					String message = "Assignment invitation declined";
					String messageBody = "Your assignment request for the client, "+clientDetails.getFirstName()+" "+clientDetails.getLastName()+", has been declined"+" in "+hospitalName+".";
					String deepLink = "3"; // Deep Link for sending user to my request screen

					if (userDetails.getFcmToken() != null)
						flashNotification.sendEventNotificationToUser(userId, userDetails.getFcmToken(), message, messageBody, deepLink, userDetails.getDeviceType(), null);
					
					inAppNotifications.createInAppNotifications(userId, 6, message, messageBody, 7, false, assignmentId);
	
					messageBody = messageBody + "Click on the link below to go to the app\n"+ConstantUtils.branchIOMyRequest;
	
					try {
	//					mailUtil.sendMail(userDetails.getEmail(), message, messageBody);
						EmailThread emailThread = new EmailThread();
						emailThread.setEmails(userDetails.getEmail());
						emailThread.setDynamicName(userDetails.getFirstName());
						emailThread.setMessage(message);
						emailThread.setMessageBody(messageBody);
						emailThread.setTemplate(false);
	
						applicationContext.getAutowireCapableBeanFactory().autowireBean(emailThread);
	
						Thread emailthread = new Thread(emailThread); // send email on separate thread.
						emailthread.start(); // start the email thread
					} catch (Exception e) {
	
					}
				}
				
				else if (isAccepted == false && isApproved == false && assignmentInvitee.isAccepted() == false)
					assignmentInvitee.setAccepted(false);
				
				assignmentInviteesRepository.save(assignmentInvitee);
				
				if (assignmentInvitee.isAccepted() == true && isApproved == true) {
					
					String message = "Assignment invitation approved";
					String messageBody = "Your assignment request for the client, "+clientDetails.getFirstName()+" "+clientDetails.getLastName()+", has been approved "+"in "+hospitalName+".";
					String deepLink = "7"; // Deep Link for sending user to my calendar screen

					if (userDetails.getFcmToken() != null)
						flashNotification.sendEventNotificationToUser(userId, userDetails.getFcmToken(), message, messageBody, deepLink, userDetails.getDeviceType(), null);
					
					inAppNotifications.createInAppNotifications(userId, 6, message, messageBody, 7, false, assignmentId);
	
					messageBody = messageBody + " Click on the link below to go to the app\n"+ConstantUtils.branchIOMyRequest;
					try {
	//					mailUtil.sendMail(userDetails.getEmail(), message, messageBody);
						EmailThread emailThread = new EmailThread();
						emailThread.setEmails(userDetails.getEmail());
						emailThread.setDynamicName(userDetails.getFirstName());
						emailThread.setMessage(message);
						emailThread.setMessageBody(messageBody);
						emailThread.setTemplate(false);
	
						applicationContext.getAutowireCapableBeanFactory().autowireBean(emailThread);
	
						Thread emailthread = new Thread(emailThread); // send email on separate thread.
						emailthread.start(); // start the email thread
					} catch (Exception e) {
	
					}
				}
				return assignmentInvitee;
			}
			else
				throw new GraphQLException("assignmentId and userId does not match");
		}else
			throw new GraphQLException("assignmentId or userId not provided.");
	}

	@Override
	public List<AssignmentInvitees> finalizeAssignment(long assignmentId) {

		Assignments assignment = mongoTemplate.findOne(Query.query(Criteria.where("assignmentId").is(assignmentId)), Assignments.class);
		Clients clientDetails = mongoTemplate.findOne(Query.query(Criteria.where("clientId").is(assignment.getClientId())), Clients.class);

		assignment.setStatus("Active");

		mongoTemplate.save(assignment, "assignments");

		List<AssignmentInvitees> assignmentInvitees = mongoTemplate.find(Query.query(Criteria.where("assignmentId").is(assignmentId)).addCriteria(Criteria.where("isAccepted").is(true))
						.addCriteria(Criteria.where("isApproved").is(true)), AssignmentInvitees.class);
		
		Optional<Businesses> hospital = businessesRepository.findById(assignment.getBusinessId());
		String hospitalName = null;
		
		if (hospital.isPresent())
			hospitalName = hospital.get().getName();
		else
			hospitalName = "Hospital";

		String message = "Assignment finalized";
		String messageBody = "Assignment for the client, "+clientDetails.getFirstName()+" "+clientDetails.getLastName()+" has been published in "+hospitalName+".";
		
		for (AssignmentInvitees eachInvitee : assignmentInvitees) {

			UserBasicInformation userDetails = userRepository.findById(eachInvitee.getStaffId()).get();

			if (userDetails.getFcmToken() != null)
				flashNotification.sendEventNotificationToUser(eachInvitee.getStaffId(), userDetails.getFcmToken(), message, messageBody, "7", userDetails.getDeviceType(), null);
			
			inAppNotifications.deleteBySourceId(Arrays.asList(assignmentId));
			inAppNotifications.createInAppNotifications(eachInvitee.getStaffId(), 6, message, messageBody, 7, false, assignmentId);

			messageBody = messageBody + " Click on the link below to go to the app\n"+ConstantUtils.branchIOMyRequest;

			try {
//				mailUtil.sendMail(userDetails.getEmail(), message, messageBody);

				EmailThread emailThread = new EmailThread();
				emailThread.setEmails(userDetails.getEmail());
				emailThread.setDynamicName(userDetails.getFirstName());
				emailThread.setMessage(message);
				emailThread.setMessageBody(messageBody);
				emailThread.setTemplate(false);

				applicationContext.getAutowireCapableBeanFactory().autowireBean(emailThread);

				Thread emailthread = new Thread(emailThread); // send email on separate thread.
				emailthread.start(); // start the email thread
				
			} catch (Exception e) {
				continue;
			}
		}
		return assignmentInvitees;
	}

	@Override
	public boolean deleteAssignmentScheduleByAssignmentIdAndStaffCoreTypeId(long assignmentId, long staffUserCoreTypeId) {
		
		List<AssignmentSchedules> assignmentScheduleList = mongoTemplate.find(Query.query(
					Criteria.where("assignmentId").is(assignmentId).and("staffCoreTypeId").is(staffUserCoreTypeId)), AssignmentSchedules.class);
		
		if (assignmentScheduleList != null && assignmentScheduleList.size() > 0) {
			inAppNotifications.deleteBySourceId(Arrays.asList(assignmentId));
			for (AssignmentSchedules assignmentSchedules: assignmentScheduleList) {
				
				mongoTemplate.remove(Query.query(Criteria.where("staffId").in(assignmentSchedules.getStaff())).
							addCriteria(Criteria.where("assignmentId").is(assignmentSchedules.getAssignmentId())), AssignmentInvitees.class);
				
				mongoTemplate.remove(Query.query(Criteria.where("_id").is(assignmentSchedules.getAssignmentScheduleId())), AssignmentSchedules.class);
			}
			return true;
		}else
			return false;
	}
}
