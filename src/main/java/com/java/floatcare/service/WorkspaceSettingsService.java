package com.java.floatcare.service;

import org.springframework.stereotype.Service;

import com.java.floatcare.model.WorkspaceSettings;

@Service
public interface WorkspaceSettingsService {

	WorkspaceSettings createWorkspaceSettings(long userId, long eventNotificationId, long onCallShiftReminderId, long meetingReminderId, long assignedWorkShiftsReminderId, 
			long educationReminderId, long eventsReminderId) throws Exception;

}
