package com.java.floatcare.service;

import com.java.floatcare.model.UserPrivacySettings;

public interface UserPrivacySettingsService {

	UserPrivacySettings createUserPrivacySettings(long userId, boolean isVisibleToPublic, boolean isVisibleToWorkspace, boolean isVisibleToCollegues);

}
