package com.java.floatcare.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java.floatcare.dao.CountersDAO;
import com.java.floatcare.model.WorksiteSettings;
import com.java.floatcare.repository.WorksiteSettingsRepository;

@Service
public class WorksiteSettingsServiceImpl implements WorksiteSettingsService {

	@Autowired
	private WorksiteSettingsRepository worksiteSettingsRepository;
	
	@Autowired
	private CountersDAO sequence;
	
	@Override
	public WorksiteSettings createWorksiteSettings(long userId, long worksiteId, long organizationId, String color) {

		WorksiteSettings worksiteSetting = new WorksiteSettings();
		
		if (userId > 0)
			worksiteSetting.setUserId(userId);
		if (worksiteId > 0)
			worksiteSetting.setWorksiteId(worksiteId);
		if (organizationId > 0)
			worksiteSetting.setOrganizationId(organizationId);
		if (color != null)
			worksiteSetting.setColor(color);
		else
			worksiteSetting.setColor(null);
			
		worksiteSetting.setWorksiteSettingId(sequence.getNextSequenceValue("worksiteSettingId"));
		
		worksiteSettingsRepository.save(worksiteSetting);
		
		return worksiteSetting;
	}

	@Override
	public WorksiteSettings updateWorksiteSettings(long worksiteSettingId, long userId, long worksiteId, long organizationId, String color) throws Exception {
		
		Optional<WorksiteSettings> worksiteSettingOptional = worksiteSettingsRepository.findById(worksiteSettingId);
		
		if (worksiteSettingOptional.isPresent()) {
			
			WorksiteSettings worksiteSetting = worksiteSettingOptional.get();
			
			if (userId > 0)
				worksiteSetting.setUserId(userId);
			
			if (worksiteId > 0)
				worksiteSetting.setWorksiteId(worksiteId);
			
			if (organizationId > 0)
				worksiteSetting.setOrganizationId(organizationId);
			
			if (color != null)
				worksiteSetting.setColor(color);
			
			worksiteSettingsRepository.save(worksiteSetting);
			
			return worksiteSetting;
			
		} else
				throw new Exception("Workspace setting not found");
	}

}
