package com.java.floatcare.service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java.floatcare.dao.CountersDAO;
import com.java.floatcare.model.Department;
import com.java.floatcare.model.DepartmentSettings;
import com.java.floatcare.model.DepartmentShifts;
import com.java.floatcare.model.DepartmentStaff;
import com.java.floatcare.model.UserWorkspaces;
import com.java.floatcare.repository.DepartmentRepository;
import com.java.floatcare.repository.DepartmentSettingsRepository;
import com.java.floatcare.repository.DepartmentShiftsRepository;
import com.java.floatcare.repository.DepartmentStaffRepository;
import com.java.floatcare.repository.UserWorkspacesRepository;

@Service
public class DepartmentServiceImpl implements DepartmentService {

	@Autowired
	private DepartmentStaffRepository departmentStaffRepository;
	@Autowired
	private DepartmentRepository departmentRepository;
	@Autowired
	private DepartmentShiftsRepository departmentShiftsRepository;
	@Autowired
	private DepartmentSettingsRepository departmentSettingsRepository;
	@Autowired
	private UserWorkspacesRepository userWorkspacesRepository;
	@Autowired
	private CountersDAO sequence;

	@Override
	public Department createDepartment(long businessId, String departmentTypeName, String phoneNumber, long extension,
			String email, String address, String address2, String description, String status, List<Long> staffRoles) {

		Department department = new Department();

		department.setDepartmentId(sequence.getNextSequenceValue("departmentId"));
		department.setBusinessId(businessId);
		department.setDepartmentTypeName(departmentTypeName);
		department.setPhoneNumber(phoneNumber);
		department.setExtension(extension);
		department.setEmail(email);
		department.setStatus(status);
		department.setAddress(address);
		department.setAddress2(address2);
		department.setDescription(description);
		department.setCreatedDate(LocalDate.now());

		department = departmentRepository.save(department);
		
		if(staffRoles!=null) {
		
			for (Long each : staffRoles) {
			DepartmentStaff departmentStaff = new DepartmentStaff();
			
			departmentStaff.setDepartmentStaffId(sequence.getNextSequenceValue("departmentStaffId"));
			departmentStaff.setDepartmentId(department.getDepartmentId());
			departmentStaff.setStaffUserCoreTypeId(each.longValue());
			departmentStaff.setStaffCount(0);
			departmentStaffRepository.save(departmentStaff);
			}
		}		
		
		
		DepartmentSettings departmentSettings = new DepartmentSettings();  //create default department settings
		
			departmentSettings.setDepartmentSettingId(sequence.getNextSequenceValue("departmentSettingId"));
			departmentSettings.setDepartmentId(department.getDepartmentId());
			departmentSettings.setCustomName(departmentTypeName);
			departmentSettings.setWorkPayPeriod("2w");
			departmentSettings.setWorkWeekDay("sunday");
			departmentSettings.setWorkPayPeriodHours(80);
		
		departmentSettingsRepository.save(departmentSettings); // saving default department settings
		
		List<HashMap<String, String>> shifts = new ArrayList<HashMap<String,String>>();  // creating default department shifts
		
		HashMap<String, String> dayShift = new HashMap<>();
		dayShift.put("color", "day");
		HashMap<String, String> swingShift = new HashMap<>();
		swingShift.put("color", "swing");
		HashMap<String, String> nightShift = new HashMap<>();
		nightShift.put("color", "night");

		shifts.add(dayShift);
		shifts.add(swingShift);
		shifts.add(nightShift);
		
		for (Map<String, String> each: shifts) {
			
			DepartmentShifts departmentShifts = new DepartmentShifts();

			departmentShifts.setDepartmentShiftId(sequence.getNextSequenceValue("departmentShiftId"));
			departmentShifts.setDepartmentId(department.getDepartmentId());
			departmentShifts.setColor(each.get("color"));
			departmentShifts.setStatus("Active");
			departmentShifts.setIcon("");
			if (each.get("color").equals("day")) {
				departmentShifts.setStartTime(LocalTime.parse("07:00:00"));
				departmentShifts.setEndTime(LocalTime.parse("19:00:00"));
				departmentShifts.setLabel("Day Shift");
				departmentShifts.setIcon("day");
			}
			else if (each.get("color").equals("swing")) {
				departmentShifts.setStartTime(LocalTime.parse("10:00:00"));
				departmentShifts.setEndTime(LocalTime.parse("22:00:00"));
				departmentShifts.setLabel("Swing Shift");
				departmentShifts.setIcon("swing");
			}
			else {
				departmentShifts.setStartTime(LocalTime.parse("19:00:00"));
				departmentShifts.setEndTime(LocalTime.parse("07:00:00"));
				departmentShifts.setLabel("Night Shift");
				departmentShifts.setIcon("night");
			}
			
			departmentShiftsRepository.save(departmentShifts); 	// saving default department shift
		}
		
		return department;
		
	}

	@Override
	public Department updateDepartment(long departmentId, long businessId, String departmentTypeName,
			String phoneNumber, long extension, String email, String address, String address2, String description,
			String status, List<Long> staffRoles) throws Exception {

		Optional<Department> optDepartment = departmentRepository.findById(departmentId);

		if (optDepartment.isPresent()) {
			Department department = optDepartment.get();

			department.setDepartmentId(departmentId);

			department.setBusinessId(businessId);

			department.setDepartmentTypeName(departmentTypeName);

			if (phoneNumber != null)
				department.setPhoneNumber(phoneNumber);

			department.setExtension(extension);

			if (email != null)
				department.setEmail(email);

			if (address != null)
				department.setAddress(address);
			
			if (status != null)
				department.setStatus(status);

			if (address2 != null)
				department.setAddress2(address2);

			if (description != null)
				department.setDescription(description);

			departmentRepository.save(department);

			departmentStaffRepository.deleteByDepartmentId(departmentId);
			if (staffRoles != null) {
				for (Long each : staffRoles) {

					DepartmentStaff departmentStaff = new DepartmentStaff();
					departmentStaff.setDepartmentStaffId(sequence.getNextSequenceValue("departmentStaffId"));
					departmentStaff.setDepartmentId(departmentId);
					departmentStaff.setStaffUserCoreTypeId(each.longValue());
					departmentStaff.setStaffCount(0);
					departmentStaffRepository.save(departmentStaff);
				}
			}

			return department;
		}
		throw new Exception("No Department Found");
	}

	@Override
	public boolean deleteDepartment(long departmentId) {

		Optional<Department> departmentOptional = departmentRepository.findById(departmentId);

		if (departmentOptional.isPresent()) {

			departmentRepository.deleteById(departmentId);

			List<UserWorkspaces> userWorkspaces = userWorkspacesRepository.findByWorkspaceId(departmentId);

			for (UserWorkspaces eachWorkspace : userWorkspaces) {

				eachWorkspace.setStatus("Deleted");
				userWorkspacesRepository.save(eachWorkspace);
			}

			return true;
		} else
			return false;
	}
}
