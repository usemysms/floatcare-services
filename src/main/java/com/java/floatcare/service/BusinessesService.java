package com.java.floatcare.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.java.floatcare.api.request.pojo.ContactInformation;
import com.java.floatcare.api.request.pojo.WorksiteAvailability;
import com.java.floatcare.model.Businesses;

@Service
public interface BusinessesService {

	Businesses createBusiness(long organizationId, String name, long businessTypeId, long businessSubTypeId,
			String phoneNumber, long extension, String email, String address, String address2, String city, String state,
            String postalCode, String country, String status, String coverPhoto, String description, List<WorksiteAvailability> worksiteAvailability, List<Long> preferredProviderId, 
            ContactInformation clientInfo, ContactInformation providerInfo, List<Long> preferredSpecialities) throws Exception;
	
	Businesses updateBusiness(long businessid, long organizationId, String name, long businessTypeId, long businessSubTypeId,
			String phoneNumber, long extension, String email, String address, String address2, String city, String state,
            String postalCode, String country, String status, String coverPhoto, String description, List<WorksiteAvailability> worksiteAvailability, List<Long> preferredProviderId,
            ContactInformation clientInfo, ContactInformation providerInfo, List<Long> preferredSpecialities) throws Exception;	

	boolean deleteBusiness(long businessId);
}
