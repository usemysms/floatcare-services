package com.java.floatcare.service;

import com.java.floatcare.model.References;

public interface ReferencesService {

	public References createReference(long referenceId, long userId, String name, String jobTitle, String phoneNumber, String email);

	public boolean deleteReferences(long referenceId);
}
