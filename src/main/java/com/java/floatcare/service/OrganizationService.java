package com.java.floatcare.service;

import org.springframework.stereotype.Service;

import com.java.floatcare.model.Organizations;

@Service
public interface OrganizationService {

	Organizations createOrganization(String name, String organizationType, String address, String address2, String country, String logo, String status,
			String state, String licence, String email, String phoneNumber, String city, String postalCode)
			throws Exception;

	Organizations updateOrganization(long organizationId, String organizationType, String name, String address, String address2, String country, String logo,
			String status, String state, String license, String email, String phoneNumber, String city,
			String postalCode,long ownerId) throws Exception;

	boolean deleteOrganization(long organizationId);

}
