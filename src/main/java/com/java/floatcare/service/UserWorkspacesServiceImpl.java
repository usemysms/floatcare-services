package com.java.floatcare.service;

import java.io.IOException;
import java.security.SecureRandom;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.java.floatcare.api.request.HealthInsurances;
import com.java.floatcare.api.request.pojo.UserAvailability;
import com.java.floatcare.api.response.pojo.RequiredType;
import com.java.floatcare.communication.MailUtils;
import com.java.floatcare.communication.TwilioCommunication;
import com.java.floatcare.dao.CountersDAO;
import com.java.floatcare.dao.CredentialSubTypesDAO;
import com.java.floatcare.model.Businesses;
import com.java.floatcare.model.CredentialSubTypes;
import com.java.floatcare.model.Department;
import com.java.floatcare.model.DepartmentStaff;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.model.UserCredentials;
import com.java.floatcare.model.UserOrganizations;
import com.java.floatcare.model.UserWorkspaces;
import com.java.floatcare.model.WorksiteSettings;
import com.java.floatcare.repository.BusinessesRepository;
import com.java.floatcare.repository.DepartmentRepository;
import com.java.floatcare.repository.DepartmentStaffRepository;
import com.java.floatcare.repository.UserBasicInformationRepository;
import com.java.floatcare.repository.UserCredentialsRepository;
import com.java.floatcare.repository.UserOrganizationsRepository;
import com.java.floatcare.repository.UserWorkspacesRepository;
import com.java.floatcare.repository.WorksiteSettingsRepository;
import com.java.floatcare.utils.ConstantUtils;
import com.java.floatcare.utils.EmailThread;
import com.java.floatcare.utils.GoogleFirebaseUtilityDAO;
import com.java.floatcare.utils.InAppNotifications;
import com.java.floatcare.utils.RandomString;
import com.java.floatcare.utils.SMSThread;
import com.twilio.exception.ApiException;

@Service
public class UserWorkspacesServiceImpl implements UserWorkspacesService {

	@Autowired
	private UserWorkspacesRepository userWorkspacesRepository;
	@Autowired
	private MongoTemplate mongoTemplate;
	@Autowired
	private DepartmentRepository departmentRepository;
	@Autowired
	private UserBasicInformationRepository userBasicInformationRepository;
	@Autowired
	private BusinessesRepository businessesRepository;
	@Autowired
	private DepartmentStaffRepository departmentStaffRepository;
	@Autowired
	private GoogleFirebaseUtilityDAO googleFirebaseUtilityDAO;
	@Autowired
	private UserOrganizationsRepository userOrganizationsRepository;
	@Autowired
	private WorksiteSettingsRepository worksiteSettingsRepository;
	@Autowired
	private CountersDAO sequence;
	@Autowired
	private MailUtils mailUtil;
	@Autowired
	private ApplicationContext applicationContext;
	@Autowired
	private CredentialSubTypesDAO credentialSubTypesDAO;
	@Autowired
	private UserCredentialsRepository userCredentialsRepository;
	@Autowired
	private InAppNotifications inAppNotification;
	@Autowired
	private TwilioCommunication twillioCommunication;

	@Override
	public UserWorkspaces createUserWorkspace(long userId, String workspaceType, long workspaceId, String fteStatus,
			boolean isChargeRole, boolean isPreceptorRole, long organizationId, long businessId, String scheduledHours,
			List<Long> departmentShiftId, long staffUserCoreTypeId, boolean isPrimaryDepartment,
			long staffUserSubCoreTypeId, int sendsms, List<UserAvailability> userAvailability) throws IOException {

		UserWorkspaces userWorkspaces = new UserWorkspaces();

		List<UserWorkspaces> foundedWorkspaces = userWorkspacesRepository.findByUserIdAndBusinessId(userId, businessId);

		if (foundedWorkspaces == null || foundedWorkspaces.size() == 0) {

			userWorkspaces.setUserWorkspaceId(sequence.getNextSequenceValue("userWorkspaceId"));
			// userWorkspacesRepository.save(userWorkspaces); // save the workspace object
		} else {
			userWorkspaces = foundedWorkspaces.get(0);
		}

		String invitationCode = null;

		userWorkspaces.setUserId(userId);

		if (workspaceType != null)
			userWorkspaces.setWorkspaceType(workspaceType);
		else
			userWorkspaces.setWorkspaceType(null);

		userWorkspaces.setWorkspaceId(workspaceId);

		if (fteStatus != null)
			userWorkspaces.setFteStatus(fteStatus);
		else
			userWorkspaces.setFteStatus(null);

		userWorkspaces.setStatus("Pending Workspace");

		if (staffUserCoreTypeId > 0)
			userWorkspaces.setStaffUserCoreTypeId(staffUserCoreTypeId);

		if (departmentShiftId != null)
			userWorkspaces.setDepartmentShiftIds(departmentShiftId);

		if (isPrimaryDepartment == true || isPrimaryDepartment == false)
			userWorkspaces.setPrimaryDepartment(isPrimaryDepartment);

		if (staffUserSubCoreTypeId > 0)
			userWorkspaces.setStaffUserSubCoreTypeId(staffUserSubCoreTypeId);
		
		/*
		 * if (userAvailability != null && userAvailability.size() > 0) {
		 * List<UserWorkspaces> userWorkspaceList =
		 * userWorkspacesRepository.findByUserIdAndBusinessId(userId, businessId);
		 * userWorkspaceList.stream().peek(e->e.setUserAvailability(userAvailability));
		 * userWorkspacesRepository.saveAll(userWorkspaceList);
		 * userWorkspaces.setUserAvailability(userAvailability); }
		 */

		userWorkspaces.setIsChargeRole(isChargeRole);

		userWorkspaces.setIsPreceptorRole(isPreceptorRole);

		userWorkspaces.setOrganizationId(organizationId);

		userWorkspaces.setHours(scheduledHours);

		userWorkspaces.setHiredOn(LocalDate.now());

		Optional<Department> department = departmentRepository.findById(workspaceId);
		String departmentName = null;
		// update businessId if not empty
		if (businessId > 0) {
			userWorkspaces.setBusinessId(businessId);
		}
		if (department.isPresent()) {
			businessId = department.get().getBusinessId();
			userWorkspaces.setBusinessId(businessId);
			departmentName = department.isPresent() ? department.get().getDepartmentTypeName() : "";

		}

		// check if user already exist in same organization or not
		List<UserWorkspaces> userWorkspacesList = userWorkspacesRepository
				.findUserWorkspacesByUserIdAndOrganizationId(userId, organizationId);

		if (userWorkspacesList.size() > 0)
			userWorkspaces.setInvitationCode(userWorkspacesList.get(0).getInvitationCode());

		else { // create invitation details if the user does not exist in same organization
			String easy = RandomString.digits + "ABCDEFGHIJKLMNOPQRSTUVVWXYZ0123456789";
			RandomString tickets = new RandomString(8, new SecureRandom(), easy);
			invitationCode = tickets.nextString();
			userWorkspaces.setInvitationCode(invitationCode);
		}
		
		userWorkspacesRepository.save(userWorkspaces);
		UserOrganizations userOrganization = userOrganizationsRepository.findByUserIdAndOrganizationIdAndStatus(userId,
				organizationId, "Active");

		if (userOrganization == null) {

			UserOrganizations userOrganizationObj = new UserOrganizations();

			userOrganizationObj.setUserOrganizationId(sequence.getNextSequenceValue("userOrganizationId"));
			userOrganizationObj.setOrganizationId(organizationId);
			userOrganizationObj.setUserId(userId);
			userOrganizationObj.setStaffUserCoreTypeId(staffUserCoreTypeId);
			userOrganizationObj.setStatus("Active");

			userOrganizationsRepository.save(userOrganizationObj);
		}

		DepartmentStaff departmentStaff = mongoTemplate
				.findOne(Query.query(Criteria.where("departmentId").is(workspaceId)).addCriteria(
						Criteria.where("staffUserCoreTypeId").is(staffUserCoreTypeId)), DepartmentStaff.class);

		if (departmentStaff != null) {
			departmentStaff.setStaffCount(userWorkspacesRepository.countByWorkspaceIdAndStaffUserCoreTypeId(workspaceId,
					staffUserCoreTypeId));
			departmentStaffRepository.save(departmentStaff);
		} else {

			DepartmentStaff newDepartmentStaff = new DepartmentStaff();

			newDepartmentStaff.setDepartmentId(workspaceId);
			newDepartmentStaff.setStaffCount(userWorkspacesRepository
					.countByWorkspaceIdAndStaffUserCoreTypeId(workspaceId, staffUserCoreTypeId));
			newDepartmentStaff.setStaffUserCoreTypeId(staffUserCoreTypeId);
			newDepartmentStaff.setDepartmentStaffId(sequence.getNextSequenceValue("departmentStaffId"));

			departmentStaffRepository.save(newDepartmentStaff);
		}

		UserBasicInformation userDetail = userBasicInformationRepository.findUsersByUserId(userId); // fetch the user
																									// detail to send
																									// notification

		String receiverFcmToken = null;

		if (userDetail != null && userDetail.getFcmToken() != null) {
			receiverFcmToken = userDetail.getFcmToken();
		}
		

		String businessName = "";

		if (businessId > 0) {
			Optional<Businesses> business = businessesRepository.findById(userWorkspaces.getBusinessId());
			businessName = business.isPresent() ? business.get().getName() : "";
		}
		String messageBody = "";
		String additionalMessage = "";
		String message = "";
		if (userWorkspaces.getStaffUserCoreTypeId() != ConstantUtils.Attorney
				&& userWorkspaces.getStaffUserCoreTypeId() != ConstantUtils.CaseManager) {
			message = "Workspace Invite";
			messageBody = "Hello from Float Care. You've been invited to join a workspace." + "\nTo join "
					+ departmentName + " @ " + businessName + ", please use this invite code "
					+ userWorkspaces.getInvitationCode() + "\nTo enter code please go to..."
					+ "My Profile > My Workspaces & then click +";

			additionalMessage = messageBody + "\nLogin email address is: " + userDetail.getEmail()
					+ "\nDefault password to login is Ftp@1234"
					+ "\nTo download the FloatCare application click on the link " + ConstantUtils.branchIODeepLink;
		} else {
			message = "Welcome aboard! We are proud to you with us.";
			messageBody = "Hello from Float Legal. You've succeffully joined @ " + businessName;

			additionalMessage = messageBody + "\nLogin email address is: " + userDetail.getEmail()
					+ "\nDefault password to login is Ftp@1234";
		}

		String deepLink = "1"; // deep link for redirecting user to my workspaces screen

		if (receiverFcmToken != null)
			googleFirebaseUtilityDAO.sendEventNotificationToUser(userId, receiverFcmToken, message, messageBody,
					deepLink, userDetail.getDeviceType(), invitationCode); // send Push Notification

		inAppNotification.createInAppNotifications(userId, 6, message, messageBody, Long.parseLong(deepLink), false,
				userWorkspaces.getUserWorkspaceId());
		try {

			EmailThread emailThread = new EmailThread();
			emailThread.setEmails(userDetail.getEmail());
			emailThread.setDynamicName(userDetail.getFirstName());
			emailThread.setMessage(message);
			emailThread.setMessageBody(messageBody);
			emailThread.setDepartmentName(departmentName);
			emailThread.setBusinessName(businessName);
			emailThread.setTemplate(ConstantUtils.welcomeProfessional);
			emailThread.setTemplate(true);

			applicationContext.getAutowireCapableBeanFactory().autowireBean(emailThread);
			Thread emailthread = new Thread(emailThread); // send email on separate thread.
			emailthread.start(); // start the email thread

			if (sendsms == 1) {

				SMSThread smsThread = new SMSThread();
				smsThread.setMessageBody(messageBody);
				smsThread.setPhone(userDetail.getPhone());
				applicationContext.getAutowireCapableBeanFactory().autowireBean(smsThread);
				Thread smsthread = new Thread(smsThread);
				smsthread.start(); // start the sms thread
			}

			mailUtil.sendMail(userDetail.getEmail(), "Floatcare credentials and other details.", additionalMessage);
			twillioCommunication.sendSMS(userDetail.getPhone(), additionalMessage);
		} catch (ApiException e) {

		}

		if (userDetail != null && (userWorkspaces.getStaffUserCoreTypeId() != ConstantUtils.Attorney
				&& userWorkspaces.getStaffUserCoreTypeId() != ConstantUtils.CaseManager)) {
			UserBasicInformation user = userDetail;

			if (user.getUserTypeId() == 2 && staffUserCoreTypeId > 0) {

				List<CredentialSubTypes> credentialsSubTypes = credentialSubTypesDAO
						.getCredentialSubTypesByCredentialTypeIdAndCoreTypeIdAndOrganizationId(0, staffUserCoreTypeId,
								organizationId);

				for (CredentialSubTypes each : credentialsSubTypes) {

					UserCredentials userCredential = new UserCredentials();
					userCredential.setUserId(user.getUserId());
					userCredential.setUserCredentialId(sequence.getNextSequenceValue("userCredentialId"));
					userCredential.setPending(true);
					userCredential.setGenerated(true);
					userCredential.setCredentialTypeId(each.getCredentialTypeId());
					userCredential.setCredentialSubTypeId(each.getCredentialSubTypeId());
					// userCredential.setState("");
					Optional<RequiredType> requiredTypeOptional = each.getRequiredType().stream()
							.filter(item -> item != null && item.getStaffCoreTypeId() == staffUserCoreTypeId)
							.findFirst();

					if (requiredTypeOptional.isPresent()) {
						userCredential.setRequired(requiredTypeOptional.get().getIsRequired());
					}
					userCredentialsRepository.save(userCredential);
				}
			}
		}
		return userWorkspaces;
	}

	@Override
	public UserWorkspaces updateUserWorkspace(long userWorkspaceId, long userId, String workspaceType, long workspaceId,
			String fteStatus, String invitationCode, boolean isChargeRole, boolean isPreceptorRole, long organizationId,
			long businessId, List<Long> departmentShiftId, long staffUserCoreTypeId, boolean isPrimaryDepartment,
			long staffUserSubCoreTypeId, String status, List<UserAvailability> userAvailability, boolean isJoined)
			throws Exception {

		if (userWorkspaceId != 0) {

			Optional<UserWorkspaces> userWorkspacesOpt = userWorkspacesRepository.findById(userWorkspaceId);

			if (userWorkspacesOpt.isPresent()) {

				UserWorkspaces userWorkspaces = userWorkspacesOpt.get();

				if (userId > 0)
					userWorkspaces.setUserId(userId);

				if (workspaceType != null)
					userWorkspaces.setWorkspaceType(workspaceType);

				if (workspaceId > 0)
					userWorkspaces.setWorkspaceId(workspaceId);

				if (fteStatus != null)
					userWorkspaces.setFteStatus(fteStatus);

				userWorkspaces.setIsChargeRole(isChargeRole);

				if (invitationCode != null)
					userWorkspaces.setInvitationCode(invitationCode);

				if (userAvailability != null && userAvailability.size() > 0)
					userWorkspaces.setUserAvailability(userAvailability);

				if (staffUserCoreTypeId > 0) {
					userWorkspaces.setStaffUserCoreTypeId(staffUserCoreTypeId);

					userId = userWorkspacesOpt.get().getUserId();
					organizationId = userWorkspacesOpt.get().getOrganizationId();
					UserOrganizations user = userOrganizationsRepository.findByUserIdAndOrganizationIdAndStatus(userId,
							organizationId, "Active");

					if (user != null) {
						user.setStaffUserCoreTypeId(staffUserCoreTypeId);
						userOrganizationsRepository.save(user);
					}
				}
				if (departmentShiftId != null)
					userWorkspaces.setDepartmentShiftIds(departmentShiftId);

				/*
				 * if (userAvailability != null && userAvailability.size() > 0) {
				 * List<UserWorkspaces> userWorkspaceList =
				 * userWorkspacesRepository.findByUserIdAndBusinessId(userId, businessId);
				 * 
				 * userWorkspaceList =
				 * userWorkspaceList.stream().peek(e->e.setUserAvailability(userAvailability)).
				 * collect(Collectors.toList());
				 * 
				 * userWorkspacesRepository.saveAll(userWorkspaceList);
				 * userWorkspaces.setUserAvailability(userAvailability); }
				 */

				if (isPrimaryDepartment == true || isPrimaryDepartment == false)
					userWorkspaces.setPrimaryDepartment(isPrimaryDepartment);

				if (staffUserSubCoreTypeId > 0)
					userWorkspaces.setStaffUserSubCoreTypeId(staffUserSubCoreTypeId);

				if (status != null)
					userWorkspaces.setStatus(status);

				Optional<Department> department = departmentRepository.findById(workspaceId);

				if (department.isPresent()) {
					userWorkspaces.setBusinessId(department.get().getBusinessId());
				}
				// update the organization id in user basic information
//				Optional<UserBasicInformation> userBasicInformation = userBasicInformationRepository.findById(userWorkspaces.getUserId());
//				if (userBasicInformation.isPresent()) {
//					UserBasicInformation userBasicInformationObj = userBasicInformation.get();
//					userBasicInformationObj.setOrganizationId(userWorkspaces.getOrganizationId());
//					userBasicInformationRepository.save(userBasicInformationObj);
//				}
				userWorkspacesRepository.save(userWorkspaces);

				return userWorkspaces;
			}
			throw new Exception("UserWorkspace Not Found !!");

		} /*
			 * else if (userId > 0 && userAvailability != null && userAvailability.size() >
			 * 0 && invitationCode == null && userWorkspaceId == 0) {
			 * 
			 * List<UserWorkspaces> userWorkspaceList =
			 * userWorkspacesRepository.findByUserIdAndBusinessId(userId, businessId);
			 * 
			 * userWorkspaceList =
			 * userWorkspaceList.stream().peek(e->e.setUserAvailability(userAvailability)).
			 * collect(Collectors.toList());
			 * 
			 * userWorkspacesRepository.saveAll(userWorkspaceList);
			 * 
			 * return new UserWorkspaces();
			 * 
			 * }
			 */else if (userId > 0 && (isJoined == true || isJoined == false) && workspaceId > 0 && userWorkspaceId == 0
				&& invitationCode == null) {

			UserWorkspaces foundUserWorkspace = userWorkspacesRepository.findByUserIdAndWorkspaceIdAndIsJoined(userId,
					workspaceId, false);

			if (foundUserWorkspace != null) {

				foundUserWorkspace.setJoined(isJoined); // True or false

				if (isJoined == true) {
					foundUserWorkspace.setJoinedDate(LocalDate.now());
					foundUserWorkspace.setStatus("Active");
				}

				this.updateWorksiteSettings(userId, foundUserWorkspace.getOrganizationId(),
						foundUserWorkspace.getBusinessId());
				userWorkspacesRepository.save(foundUserWorkspace);
				inAppNotification.deleteBySourceId(Arrays.asList(foundUserWorkspace.getUserWorkspaceId()));
				return foundUserWorkspace;

			} else {
				return foundUserWorkspace;
			}
		}

		else {
			List<UserWorkspaces> foundUsers = userWorkspacesRepository.findByInvitationCode(invitationCode);

			foundUsers = foundUsers.stream().filter(emitter -> emitter.isJoined() == false)
					.collect(Collectors.toList());

			UserWorkspaces userWorkspace = new UserWorkspaces();

			if (foundUsers.size() > 0) {

				for (UserWorkspaces eachWorkspace : foundUsers) {

					long userIdFound = eachWorkspace.getUserId();

					userWorkspace = eachWorkspace;

					userWorkspace.setInvitationCode(invitationCode);
					userWorkspace.setJoined(true);
					userWorkspace.setStatus("Active");
					userWorkspace.setJoinedDate(LocalDate.now());

					if (userIdFound > 0) {

						userWorkspace.setUserId(userIdFound);
						// update the organization id in user basic information and create default
						// worksite settings
//						Optional<UserBasicInformation> userBasicInformation = userBasicInformationRepository.findById(userWorkspace.getUserId());
//						if (userBasicInformation.isPresent()) {
//							UserBasicInformation userBasicInformationObj = userBasicInformation.get();
//							userBasicInformationObj.setOrganizationId(userWorkspace.getOrganizationId());
//							userBasicInformationRepository.save(userBasicInformationObj);

						// set worksite settings
						this.updateWorksiteSettings(userId, userWorkspace.getOrganizationId(),
								userWorkspace.getBusinessId());
					}
				}
				userWorkspacesRepository.save(userWorkspace);
			}

			inAppNotification.deleteBySourceId(
					foundUsers.stream().map(e -> e.getUserWorkspaceId()).collect(Collectors.toList()));

			return userWorkspace;
//			} else
//				throw new Exception("User already used the invitation code.");
		}
	}

	public void updateWorksiteSettings(long userId, long organizationId, long businessId) {

		WorksiteSettings worksiteSettingOptional = worksiteSettingsRepository.findByUserIdAndWorksiteId(userId,
				businessId);

		if (worksiteSettingOptional == null) {

			WorksiteSettings worksiteSetting = new WorksiteSettings();

			worksiteSetting.setUserId(userId);
			worksiteSetting.setColor("#F7B500");
			worksiteSetting.setOrganizationId(organizationId);
			worksiteSetting.setWorksiteId(businessId);
			worksiteSetting.setWorksiteSettingId(sequence.getNextSequenceValue("worksiteSettingId"));

			worksiteSettingsRepository.save(worksiteSetting);
		}
	}

	@Override
	public UserOrganizations updateUserOrganization(long userId, long organizationId,
			List<HealthInsurances> healthInsurances) {

		UserOrganizations userOrganizationObj = userOrganizationsRepository.findByUserIdAndOrganizationId(userId,
				organizationId);

		if (userOrganizationObj != null) {

			userOrganizationObj.setHealthInsurances(healthInsurances);
			userOrganizationsRepository.save(userOrganizationObj);

			return userOrganizationObj;
		} else
			return userOrganizationObj;
	}
}