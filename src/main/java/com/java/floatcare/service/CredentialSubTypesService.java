package com.java.floatcare.service;

import java.util.List;

import com.java.floatcare.api.response.pojo.RequiredType;
import com.java.floatcare.model.CredentialSubTypes;

public interface CredentialSubTypesService {

	CredentialSubTypes createCredentialSubTypes(long credentialSubTypeId,long organizationId, long credentialTypeId, String label, List<RequiredType> requiredTypes, List<String> states,String credentialType, long creatorId, String status, String acronym);

	boolean deleteCredentialSubTypes(long credentialSubTypeId);

}
