package com.java.floatcare.service;

import java.util.List;

import com.java.floatcare.model.SetScheduleLayouts;
import com.java.floatcare.model.StaffWeekOffs;

public interface SetScheduleLayoutsService {

	SetScheduleLayouts createSetScheduleLayout(long worksiteId, long departmentId, String scheduleLength, String startDate, List<Long> roles,
			List<Long> shiftTypes, List<String >weekOffDays, List<StaffWeekOffs> staffWeekOffs);
	
	SetScheduleLayouts updateSetScheduleLayout (long setScheduleLayoutId, long worksiteId, long departmentId, String scheduleLength, String startDate, List<Long> roles,
			List<Long> shiftTypes, List<String >weekOffDays, List<StaffWeekOffs> staffWeekOffs) throws Exception;
	
	boolean deleteSetScheduleLayout (long setScheduleLayoutId);
}
