package com.java.floatcare.service;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import com.java.floatcare.dao.CountersDAO;
import com.java.floatcare.dao.ScheduleDAO;
import com.java.floatcare.model.Businesses;
import com.java.floatcare.model.Department;
import com.java.floatcare.model.FlexOffInvitees;
import com.java.floatcare.model.FlexOffs;
import com.java.floatcare.model.Notifications;
import com.java.floatcare.model.Schedules;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.repository.BusinessesRepository;
import com.java.floatcare.repository.DepartmentRepository;
import com.java.floatcare.repository.FlexOffInviteesRepository;
import com.java.floatcare.repository.FlexOffsRepository;
import com.java.floatcare.repository.NotificationsRepository;
import com.java.floatcare.repository.SchedulesRepository;
import com.java.floatcare.repository.UserBasicInformationRepository;
import com.java.floatcare.utils.ConstantUtils;
import com.java.floatcare.utils.EmailThread;
import com.java.floatcare.utils.GoogleFirebaseUtilityDAO;
import com.java.floatcare.utils.InAppNotifications;
import com.java.floatcare.utils.SMSThread;
import com.java.floatcare.utils.ZonedDateTimeWriteConverter;

@Service
public class FlexOffsServiceImpl implements FlexOffsService{

	@Autowired
	private CountersDAO sequence;
	@Autowired
	private FlexOffsRepository flexOffsRepository;
	@Autowired
	private FlexOffInviteesRepository flexOffInviteesRepository;
	@Autowired
	private BusinessesRepository businessesRepository;
	@Autowired
	private ScheduleDAO scheduleDAO;
	@Autowired
	private NotificationsRepository notificationsRepository;
	@Autowired
	private SchedulesRepository schedulesRepository;
	@Autowired
	private UserBasicInformationRepository userRepository;
	@Autowired
	private DepartmentRepository departmentRepository;
	@Autowired
	private GoogleFirebaseUtilityDAO googleFirebaseUtilityDAO;
	@Autowired
	private ApplicationContext applicationContext;
	@Autowired
	private InAppNotifications inAppNotifications;
	
	@Override
	public FlexOffs createFlexOff(long worksiteId, long departmentId, long shiftTypeId, String onDate, String startTime,
			String endTime, long staffUserCoreTypeId, long quota, boolean isSendNotifications,
			boolean isAutoApprove, boolean fullFlexOff, String note, List<Integer> invitees) throws ParseException {
		
		FlexOffs flexOff = new FlexOffs();
		
		flexOff.setFlexOffId(sequence.getNextSequenceValue("flexOffId"));
		
		flexOff.setWorksiteId(worksiteId);
		
		flexOff.setDepartmentId(departmentId);
		
		flexOff.setShiftTypeId(shiftTypeId);

		if (onDate != null)
			flexOff.setOnDate(LocalDate.parse(onDate));
		else
			flexOff.setOnDate(null);

		if (startTime != "")
			flexOff.setStartTime(LocalTime.parse(startTime));
		else
			flexOff.setStartTime(null);

		if (endTime != "")
			flexOff.setEndTime(LocalTime.parse(endTime));
		else
			flexOff.setEndTime(null);
		
		flexOff.setStaffUserCoreTypeId(staffUserCoreTypeId);
		
		flexOff.setQuota(quota);
		
		if (isSendNotifications == true || isSendNotifications == false)
			flexOff.setSendNotifications(isSendNotifications);
		
		if (isAutoApprove == true || isAutoApprove == false)
			flexOff.setAutoApprove(isAutoApprove);
		
		if (fullFlexOff == true || fullFlexOff == false)
			flexOff.setFullFlexOff(fullFlexOff);
		
		if (note != null)
			flexOff.setNote(note);
		else
			flexOff.setNote(null);
		
		flexOff.setStatus("Active");
		
		if (invitees != null)
			flexOff.setInvitees(invitees);
		
		FlexOffs flexOffs = flexOffsRepository.save(flexOff);
		
		for (Integer invitee : invitees) {

			// create invitees records
			FlexOffInvitees flexOffInvitee = new FlexOffInvitees();

			flexOffInvitee.setFlexOffInviteeId(sequence.getNextSequenceValue("flexOffInviteeId"));
			flexOffInvitee.setAccepted(false);
			flexOffInvitee.setApproved(false);
			flexOffInvitee.setFlexOffId(flexOffs.getFlexOffId());
			flexOffInvitee.setUserId(invitee);

			flexOffInviteesRepository.save(flexOffInvitee);

			Schedules schedule = scheduleDAO.findScheduleByUserIdAndShiftDateAndDepartmentId(invitee, LocalDate.parse(onDate), departmentId);

			if (schedule != null) {

				List<Schedules> previousSchedule = scheduleDAO.findUserScheduleByUserIdAndDepartmentIdAndEventTypeAndOnDate(invitee, departmentId, "flexedOff", 
											"Cancelled", LocalDate.parse(onDate)); 

				if (previousSchedule != null)  // check for duplicate cancelled schedules
					schedulesRepository.deleteAll(previousSchedule);
				
				Schedules changeScheduleStatus = schedule;
				changeScheduleStatus.setEventType("flexedOff");
				changeScheduleStatus.setStatus("Cancelled");
				
				schedulesRepository.save(schedule);
			}

			// create the notification records for flex off invitation
			Optional<Businesses> worksite = businessesRepository.findById(worksiteId);

			Optional<Department> department = departmentRepository.findById(departmentId);

			UserBasicInformation userDetails = userRepository.findUsersByUserId(invitee);

			if (startTime == null && endTime == null && fullFlexOff == true) {
				startTime = "00:00";
				endTime = "00:00";
			}

			String dateString = LocalDate.parse(onDate).format(DateTimeFormatter.ofPattern("MM-dd-yyyy"));

			String message = "You have been Flexed-Off!";

			String departmentName = null;
			String businessName = null;

			try {
				departmentName = department.get().getDepartmentTypeName();
				businessName = worksite.get().getName();
			}catch(Exception e) {
				departmentName = "";
				businessName = "";
			}

			String messageBody = "You have been flexed off in " +  departmentName+" @ "+businessName+ " on " + dateString;

			String content = "You have been flexed off in " + businessName +" on " + onDate;

			String receiverFcmToken = userRepository.findUsersByUserId(invitee).getFcmToken();
			String deepLink = "7";  // deep link for redirecting user to schedule/calendar screen

			if (isSendNotifications == true) {

				Notifications notifications = new Notifications();
				ZonedDateTimeWriteConverter zonedDateTimeReadConverter = new ZonedDateTimeWriteConverter();

				notifications.setNotificationId(sequence.getNextSequenceValue("notificationId"));
				notifications.setUserId(invitee);
				notifications.setNotificationTypeId(6);
				notifications.setContent(content);
				notifications.setOnDate(zonedDateTimeReadConverter.convert(ZonedDateTime.now()));
				notifications.setRead(false);
				notifications.setResourceLink(null);
				
				notificationsRepository.save(notifications);
			}

			if (receiverFcmToken != null)
				googleFirebaseUtilityDAO.sendEventNotificationToUser(invitee, receiverFcmToken, message, messageBody, deepLink,
						userDetails.getDeviceType(), null); // send Push Notification

			messageBody = messageBody+". Click on the link below to go to the Floatcare application. \n"+ConstantUtils.branchIOMySchedule;

			try {
				EmailThread emailThread = new EmailThread();
				emailThread.setEmails(userDetails.getEmail());
				emailThread.setDynamicName(userDetails.getFirstName());
				emailThread.setMessage(message);
				emailThread.setMessageBody(messageBody);
				emailThread.setBusinessName(businessName);
				emailThread.setDepartmentName(departmentName);
				emailThread.setTemplate(ConstantUtils.flexedOffinvitation);
				emailThread.setTemplate(true);

				SMSThread smsThread = new SMSThread();
				smsThread.setMessageBody(messageBody);
				smsThread.setPhone(userDetails.getPhone());

				applicationContext.getAutowireCapableBeanFactory().autowireBean(emailThread);
				applicationContext.getAutowireCapableBeanFactory().autowireBean(smsThread);

				Thread emailthread = new Thread(emailThread); // send email on separate thread.
				Thread smsthread = new Thread(smsThread);
				emailthread.start(); // start the email thread
				smsthread.start(); // start the sms thread
			} catch (Exception e) {
				continue;
			}
		}
		return flexOff;
	}

	@Override
	public FlexOffs updateFlexOff(long userId, long flexOffId, boolean isAccepted, boolean isApproved, String status) {
		
		Optional<FlexOffInvitees> flexOffInviteesOptional = flexOffInviteesRepository.findByUserIdAndFlexOffId(userId, flexOffId);

		FlexOffs flexOffs = new FlexOffs();
		
		if (flexOffInviteesOptional.isPresent()) {

			FlexOffInvitees flexOffInvitees = flexOffInviteesOptional.get();

			flexOffInvitees.setFlexOffInviteeId(flexOffInvitees.getFlexOffInviteeId());
			flexOffInvitees.setFlexOffId(flexOffId);

			if (userId > 0)
				flexOffInvitees.setUserId(userId);

			if (String.valueOf(isApproved) != null && flexOffInvitees.isApproved() == false)
				flexOffInvitees.setApproved(isApproved);

			if (String.valueOf(isAccepted) != null && flexOffInvitees.isAccepted() == false)
				flexOffInvitees.setAccepted(isAccepted);

			flexOffInviteesRepository.save(flexOffInvitees);
		}

		Optional<FlexOffs> flexOffOptional = flexOffsRepository.findById(flexOffId);

		if (flexOffOptional.isPresent()) {

			flexOffs = flexOffOptional.get();

			if (status != null)
				flexOffs.setStatus(status);

			flexOffsRepository.save(flexOffs);

			if (isApproved == true) {
					
				Schedules schedules = new Schedules();

				schedules.setScheduleId(sequence.getNextSequenceValue("scheduleId"));
				schedules.setUserId(userId);
				schedules.setDepartmentId(flexOffs.getDepartmentId());
				schedules.setWorksiteId(flexOffs.getWorksiteId());
				schedules.setShiftDate(flexOffs.getOnDate());					
				schedules.setStartTime(flexOffs.getStartTime());
				schedules.setEndTime(flexOffs.getEndTime());
				schedules.setShiftTypeId(flexOffs.getShiftTypeId());
				schedules.setEventType("flexedOff");
				schedules.setStatus("Cancelled");
				schedules.setOnCallRequest(false);

				schedulesRepository.save(schedules);
			}
		}
		return flexOffs;
	}

	@Override
	public boolean deleteFlexOff(long flexOffId) {
		
		Optional<FlexOffs> flexOffOptional = flexOffsRepository.findById(flexOffId);
		
		if (flexOffOptional.isPresent()) {
			FlexOffs flexOff = flexOffOptional.get();
			flexOffsRepository.delete(flexOff);
			inAppNotifications.deleteBySourceId(Arrays.asList(flexOffId));
			flexOffInviteesRepository.deleteByFlexOffId(flexOffId);
			return true;
		}
		else
			return false;
	}
}