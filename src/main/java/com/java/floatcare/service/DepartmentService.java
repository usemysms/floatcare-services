package com.java.floatcare.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.java.floatcare.model.Department;

@Service
public interface DepartmentService {

	public Department createDepartment(long businessId, String departmentTypeName, String phoneNumber, long extension,
			String email,  String address, String address2, String description, String status,
			List<Long> roles);

	public Department updateDepartment(long departmentId, long businessId, String departmentTypeName,
			String phoneNumber, long extension, String email, String address, String address2,
			String description, String status, List<Long> staffRoles) throws Exception;
	
	public boolean deleteDepartment(long departmentId);
}
