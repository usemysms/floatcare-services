package com.java.floatcare.service;

import java.text.ParseException;
import java.time.LocalDate;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java.floatcare.dao.CountersDAO;
import com.java.floatcare.model.UserWorkExperience;
import com.java.floatcare.repository.UserWorkExperienceRepository;

@Service
public class UserWorkExperienceServiceImpl implements UserWorkExperienceService {

	@Autowired
	private UserWorkExperienceRepository userWorkExperienceRepository;
	@Autowired
	private CountersDAO sequence;

	@Override
	public UserWorkExperience createUserWorkExperience(long userId, String facilityName, String location, String facilityType,
			String startDate, String endDate, String positionTitle, 
			String unit, String description, boolean isCurrentlyWorking) throws ParseException {

		UserWorkExperience userWorkExperience = new UserWorkExperience();

		userWorkExperience.setUserWorkExperienceId(sequence.getNextSequenceValue("userWorkExperienceId"));
		
		userWorkExperience.setUserId(userId);
		
		if (facilityName != null)
			userWorkExperience.setFacilityName(facilityName);
		else
			userWorkExperience.setFacilityName(null);
		
		if (location != null)
			userWorkExperience.setLocation(location);
		else
			userWorkExperience.setLocation(null);
		
		if (facilityType != null)
			userWorkExperience.setFacilityType(facilityType);
		else
			userWorkExperience.setFacilityType(null);
		
		if (startDate != null)
			userWorkExperience.setStartDate(LocalDate.parse(startDate));
		
		if (endDate != "")
			userWorkExperience.setEndDate(LocalDate.parse(endDate));
		else
			userWorkExperience.setEndDate(null);
		
		if (positionTitle != null)
			userWorkExperience.setPositionTitle(positionTitle);
		else
			userWorkExperience.setPositionTitle(null);
		
		if (unit != null)
			userWorkExperience.setUnit(unit);
		else
			userWorkExperience.setUnit(null);
		
		if (description != null)
			userWorkExperience.setDescription(description);
		else
			userWorkExperience.setDescription(null);
		
		userWorkExperience.setIsCurrentlyWorking(isCurrentlyWorking);
		
		userWorkExperienceRepository.save(userWorkExperience);

		return userWorkExperience;

	}

	@Override
	public UserWorkExperience updateUserWorkExperience(long userWorkExperienceId, long userId, String facilityName, String location,
			String facilityType, String startDate, String endDate, String positionTitle, String unit,
			String description, boolean isCurrentlyWorking) throws Exception {

		Optional<UserWorkExperience> optUserWorkExperience = userWorkExperienceRepository.findById(userWorkExperienceId);

		if (optUserWorkExperience.isPresent()) {
			UserWorkExperience userWorkExperience = optUserWorkExperience.get();

			if (facilityName != null) {
				userWorkExperience.setFacilityName(facilityName);
			}

			if (location != null) {
				userWorkExperience.setLocation(location);
			}

			if (facilityType != null) {

				userWorkExperience.setFacilityType(facilityType);
			}

			if (startDate != null) {
				userWorkExperience.setStartDate(LocalDate.parse(startDate));
			}

			if (endDate != "") {
				userWorkExperience.setEndDate(LocalDate.parse(endDate));
			}

			if (positionTitle != null) {
				userWorkExperience.setPositionTitle(positionTitle);
			}

			if (unit != null) {
				userWorkExperience.setUnit(unit);
			}

			if (facilityName != null) {
				userWorkExperience.setFacilityName(facilityName);
			}

			if (description != null) {
				userWorkExperience.setDescription(description);
			}

			userWorkExperience.setIsCurrentlyWorking(isCurrentlyWorking);
			
			userWorkExperienceRepository.save(userWorkExperience);

			return userWorkExperience;
		}

		throw new Exception("User Work Experience not inserted");
	}

	@Override
	public boolean deleteUserWorkExperience(long userWorkExperienceId) {
		userWorkExperienceRepository.deleteById(userWorkExperienceId);
		return true;
	}
}
