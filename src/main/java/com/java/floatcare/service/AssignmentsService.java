package com.java.floatcare.service;

import com.java.floatcare.model.Assignments;

public interface AssignmentsService {

	Assignments createAssignment(long businessId, long clientId, String startDate, String endDate);

	Assignments updateAssignment(long assignmentId, long clientId, String startDate, String endDate);

	boolean deleteAssignment(long assignmentId);
	
}
