package com.java.floatcare.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.java.floatcare.api.request.UserRequest;
import com.java.floatcare.api.response.pojo.RequiredType;
import com.java.floatcare.api.response.pojo.SocialSecurityNumber;
import com.java.floatcare.api.response.pojo.UserLanguages;
import com.java.floatcare.api.response.pojo.UserNationalProviderIdentity;
import com.java.floatcare.dao.CountersDAO;
import com.java.floatcare.dao.UserBasicInformationRepositoryDAO;
import com.java.floatcare.dao.UserOrganizationDAO;
import com.java.floatcare.dao.UserWorkspacesDAO;
import com.java.floatcare.exceptions.UserAlreadyExistsException;
import com.java.floatcare.model.CredentialSubTypes;
import com.java.floatcare.model.DepartmentStaff;
import com.java.floatcare.model.EhrSystems;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.model.UserCredentials;
import com.java.floatcare.model.UserNotification;
import com.java.floatcare.model.UserOrganizations;
import com.java.floatcare.model.UserPlan;
import com.java.floatcare.model.UserWorkspaces;
import com.java.floatcare.model.WorkspaceSettings;
import com.java.floatcare.repository.CredentialSubTypeRepository;
import com.java.floatcare.repository.DepartmentStaffRepository;
import com.java.floatcare.repository.StaffUserCoreTypesRepository;
import com.java.floatcare.repository.UserBasicInformationRepository;
import com.java.floatcare.repository.UserCredentialsRepository;
import com.java.floatcare.repository.UserNotificationRepository;
import com.java.floatcare.repository.UserOrganizationsRepository;
import com.java.floatcare.repository.UserWorkspacesRepository;
import com.java.floatcare.repository.WorkspaceSettingsRepository;
import com.java.floatcare.security.JwtTokenUtil;
import com.java.floatcare.utils.ConfigProperties;
import com.java.floatcare.utils.ConstantUtils;
import com.java.floatcare.utils.DistanceCalculate;
import com.java.floatcare.utils.OpenStreetMapUtils;
import com.java.floatcare.utils.UploadImageDAO;

@Component
public class UserBasicInformationServiceImpl implements UserBasicInformationService {

	@Autowired
	private UserPlanService userPlanService;
	@Autowired
	private UserBasicInformationRepository userRepository;
	@Autowired
	private CountersDAO sequence;
	@Autowired
	private UploadImageDAO setProfileImage;
	@Autowired
	private UserOrganizationDAO userOrganizationsDAO;
	@Autowired
	private UserWorkspacesDAO userWorkspacesDAO;
	@Autowired
	private UserOrganizationsRepository userOrganizationsRepository;
	@Autowired
	private StaffUserCoreTypesRepository staffUserCoreTypesRepository;
	@Autowired
	private WorkspaceSettingsRepository workspaceSettingsRepository;
	@Autowired
	private UserWorkspacesRepository userWorkspacesRepository;
	@Autowired
	private UserNotificationRepository userNotificationRepository;
	@Autowired
	private CredentialSubTypeRepository credentialSubTypeRepository;
	@Autowired
	private UserCredentialsRepository userCredentialsRepository;
	@Autowired
	private DepartmentStaffRepository departmentStaffRepository;
	@Autowired
	private ConfigProperties configProperties;
	@Autowired
	private UserBasicInformationRepositoryDAO userDAO;
	@Autowired
	private UserWorkspacesService userWorkspacesService;
	
	private String uniqueFileName;

	public String getUniqueFileName() {
		return uniqueFileName;
	}

	public void setUniqueFileName(String uniqueFileName) {
		this.uniqueFileName = uniqueFileName;
	}

	@Override
	public UserBasicInformation createUserBasicInformation (String firstName, String lastName, String middleName,
			String email, String password, String defaultPassword, String preferredName, String address1, String address2, String city,
			String state, String country, String postalCode, String phone, String primaryContactCountryCode, String profilePhoto, String title,
			long userTypeId, long staffUserCoreTypeId, long staffUserSubCoreTypeId, String dateOfBirth, String linkedInProfile, long organizationId,
			String gender, String bioInformation, String createdOn, String secondaryPhoneNumber,
			String alternativeContactCountryCode, String secondaryEmail, String fcmToken, String deviceType, List<UserLanguages> userLanguages , 
			UserNationalProviderIdentity userNationalProviderIdentity, SocialSecurityNumber socialSecurityNumber,
			String publicKey, List<EhrSystems> ehrSystemId, String ethnicType, List<String> specialities,
			String invitedSource,long invitedFirmId,long invitedBy,String invitationStatus) throws Exception {

		// to check unique email address.
		Optional<UserBasicInformation> optEmail = userRepository.findUserByEmail(email); 
		// added && condition fix false positive case for null email
		if (optEmail.isPresent() && optEmail.get().getEmail()!=null 
				&& optEmail.get().getEmail().equalsIgnoreCase(email)) {
			throw new UserAlreadyExistsException("User with same email exists");
		}
		
		// to check unique phone number.
		UserBasicInformation optPhone = userRepository.findFirstUserByPhone(phone); 
		// added && condition fix false positive case for null email
		if (optPhone != null && optPhone.getPhone()!=null && optPhone.getPhone().equalsIgnoreCase(phone)) {
			throw new Exception("User with same Phone number already exists");
		}
		
		UserBasicInformation theUser = new UserBasicInformation();
		
		if (StringUtils.hasText(invitedSource))
			theUser.setInvitedSource(invitedSource);

		if (invitedFirmId > 0 )
			theUser.setInvitedFirmId(invitedFirmId);

		if (invitedBy > 0 )
			theUser.setInvitedBy(invitedBy);

		if (StringUtils.hasText(invitationStatus))
			theUser.setInvitationStatus(invitationStatus);
		
		if (firstName != null)
			theUser.setFirstName(firstName);
		else
			theUser.setFirstName(null);

		if (lastName != null)
			theUser.setLastName(lastName);
		else
			theUser.setLastName(null);

		if (middleName != null)
			theUser.setMiddleName(middleName);
		else
			theUser.setMiddleName(null);

		if (password != null) {
			try {
				String generatedSecuredPasswordHash = JwtTokenUtil.generateStrongPasswordHash(password);
				theUser.setPassword(generatedSecuredPasswordHash);
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			} catch (InvalidKeySpecException e) {
				e.printStackTrace();
			}
		}
		
		if (organizationId > 0 && userTypeId == 3 && defaultPassword != "") {
				String generatedSecuredPasswordHash = defaultPassword;
				theUser.setDefaultPassword(generatedSecuredPasswordHash);
		}

		else {
			try {
				String generatedSecuredPasswordHash = JwtTokenUtil.generateStrongPasswordHash("Ftp@1234");
				theUser.setPassword(generatedSecuredPasswordHash);
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			} catch (InvalidKeySpecException e) {
				e.printStackTrace();
			}
		}

		if (preferredName != null)
			theUser.setPreferredName(preferredName);
		else
			theUser.setPreferredName(null);

		if (address1 != null)
			theUser.setAddress1(address1);
		else
			theUser.setAddress1(null);

		if (address2 != null)
			theUser.setAddress2(address2);

		if (city != null)
			theUser.setCity(city);
		else
			theUser.setCity(null);

		if (state != null)
			theUser.setState(state);
		else
			theUser.setState(null);
		
		if (country != null)
			theUser.setCountry(country);
		
		if (postalCode != null)
			theUser.setPostalCode(postalCode);
		
		if (address1 != null) {

			theUser.setAddress1(address1);

			Map<String, Double> coords;

			String address = address2 != null ? address1.concat(" ").concat(address2) : address1;
			address = city != null ? address.concat(" ".concat(city)) : address;
			address = state != null ? address.concat(" ".concat(state)) : address;
			address = country != null ? address.concat(" ".concat(country)) : address;

			coords = OpenStreetMapUtils.getInstance().getCoordinates(address);

			if (coords != null && coords.size() > 0) {
				theUser.setLatitude(coords.get("lat").toString());
				theUser.setLongitude(coords.get("lon").toString());
			}
		}

		if (phone != null)
			theUser.setPhone(phone);
		else
			theUser.setPhone(null);

		theUser.setStatus("Active");

		if (primaryContactCountryCode != null)
			theUser.setPrimaryContactCountryCode(primaryContactCountryCode);
		else
			theUser.setPrimaryContactCountryCode(null);

		if (title != null)
			theUser.setTitle(title);
		else
			theUser.setTitle(null);

		if (userTypeId > 0)
			theUser.setUserTypeId(userTypeId);

		if (staffUserCoreTypeId > 0) {

			theUser.setStaffUserCoreTypeId(staffUserCoreTypeId);

			theUser.setStaffUserCoreTypeName(staffUserCoreTypesRepository.findStaffUserCoreTypesById(staffUserCoreTypeId).getLabel());
		}

		else
			theUser.setStaffUserCoreTypeName(null);
		
		if (staffUserSubCoreTypeId > 0) {
			theUser.setStaffUserSubCoreTypeId(staffUserSubCoreTypeId);
		}

		if (dateOfBirth != null)
			theUser.setDateOfBirth(LocalDate.parse(dateOfBirth));
		else
			theUser.setDateOfBirth(null);

		if (linkedInProfile != null)
			theUser.setLinkedInProfile(linkedInProfile);
		else
			theUser.setLinkedInProfile(null);

		if (gender != null)
			theUser.setGender(gender);
		else
			theUser.setGender(null);

		if (bioInformation != null)
			theUser.setBioInformation(bioInformation);
		else
			theUser.setBioInformation(null);

		if (secondaryPhoneNumber != null)
			theUser.setSecondaryPhoneNumber(secondaryPhoneNumber);
		else
			theUser.setSecondaryPhoneNumber(null);

		if (alternativeContactCountryCode != null)
			theUser.setAlternativeContactCountryCode(alternativeContactCountryCode);
		else
			theUser.setAlternativeContactCountryCode(null);

		if (secondaryEmail != null)
			theUser.setSecondaryEmail(secondaryEmail);
		else
			theUser.setSecondaryEmail(null);

		if (fcmToken != null)
			theUser.setFcmToken(fcmToken);
		else
			theUser.setFcmToken(null);

		if (specialities != null && specialities.size() > 0)
			theUser.setSpecialities(specialities);

		if (deviceType != null)
			theUser.setDeviceType(deviceType);
		else
			theUser.setDeviceType(null);
        
		if (userLanguages != null && userLanguages.size() > 0)
			theUser.setUserLanguages(userLanguages);
		/*else {
			
			UserLanguages userLanguage = new UserLanguages();
			
			userLanguage.setCanRead(false);
			userLanguage.setCanSpeak(false);
			userLanguage.setCanWrite(false);
			userLanguage.setName("");
			
			theUser.setUserLanguages(Arrays.asList(userLanguage));
		}*/
		if (userNationalProviderIdentity != null) 
			theUser.setUserNationalProviderIdentity(userNationalProviderIdentity);
		else {
			
			UserNationalProviderIdentity userNationalObj = new UserNationalProviderIdentity();
			
			userNationalObj.setExpirationDate(null);
			userNationalObj.setIssuedDate(null);
			userNationalObj.setNpiNumber("");

			theUser.setUserNationalProviderIdentity(userNationalObj);
		}
		if (socialSecurityNumber != null)
			theUser.setSocialSecurityNumber(socialSecurityNumber);
		else {
			
			SocialSecurityNumber socialSecurityObj = new SocialSecurityNumber();
			
			socialSecurityObj.setBackSide(null);
			socialSecurityObj.setExpirationDate(null);
			socialSecurityObj.setFrontSide(null);
			socialSecurityObj.setIssuedDate(null);
			socialSecurityObj.setSocialSecurityNumber("");
			socialSecurityObj.setState(null);
			
			theUser.setSocialSecurityNumber(socialSecurityObj);
		}	
		theUser.setOrganizationId(organizationId);

		theUser.setEthnicType(ethnicType);

			theUser.setUserId(sequence.getNextSequenceValue("userId"));
			theUser.setPublicKey(publicKey);
			theUser.setEmail(email);
			theUser.setFirstLogin(true);
			
			if (profilePhoto != null) {
				if (profilePhoto.length() > 0) {
					String fileName = firstName + "_" + lastName + "_" + LocalDate.now().toString() + ".png";
					fileName = fileName.replaceAll("\\s", ""); // removes spaces
					boolean check = false;
					try {
						check = setProfileImage.uploadImageFile(profilePhoto, fileName, 2);
					} catch (Exception e) {
						
					}

					if (check) {
						theUser.setProfilePhoto(uniqueFileName);
						File fileToDelete = FileUtils.getFile(fileName);
						FileUtils.deleteQuietly(fileToDelete);
					}
				}
			} else
				theUser.setProfilePhoto(null);
			if (ehrSystemId != null)
				theUser.setEhrSystems(ehrSystemId);

			theUser.setCreatedOn(LocalDate.now());

			userRepository.save(theUser);
			
			
			if (StringUtils.hasText(invitationStatus) && invitationStatus.equalsIgnoreCase("joined")) {
				registerUserPlan(theUser.getUserId());
			}

			// If userTypeId is 2 {Account Owner} send an Email and SMS
			//if (userTypeId == 2 && userTypeId == 3) {

				UserOrganizations userOrganization = new UserOrganizations(); //add the user in staff list and send credential notifications

				userOrganization.setOrganizationId(theUser.getOrganizationId());
				userOrganization.setUserId(theUser.getUserId());
				userOrganization.setStatus("Active");
				// If there is no staffcoretypeid then considering as a physician this case usually happens at the time account owner creation.
				if(staffUserCoreTypeId<=0)
					userOrganization.setStaffUserCoreTypeId(ConstantUtils.Physician);
				else
				userOrganization.setStaffUserCoreTypeId(staffUserCoreTypeId);
				userOrganization.setUserOrganizationId(sequence.getNextSequenceValue("userOrganizationId"));

				userOrganizationsRepository.save(userOrganization);

//				String description =  "Your email address for "+ organizationRepository.findById(theUser.getOrganizationId()).get().getName()+
//						"is "+theUser.getEmail()+" and password is: Ftp@1234";

				if (theUser.getEmail() != null)
//					mailUtil.sendMail(theUser.getEmail(), "Credential details for Member", description);
					if (theUser.getPhone() != null) {
						try {
//						twilioCommunication.sendSMS(theUser.getPhone(), description);
						}catch(Exception e) {

						}
					}
			//}

			
			// If the user is staff member then create default settings for notifications and remainders
			//System.out.println("userTypeId: "+userTypeId);
			if(userTypeId == 3) {			
				if (userNotificationRepository.findByUserId(theUser.getUserId()).size() == 0) {
					for	(int i = 1; i <= 6 ; i++) {
						
						UserNotification userNotification = new UserNotification();
						
						userNotification.setUserNotificationId(sequence.getNextSequenceValue("userNotificationId"));
						userNotification.setUserId(theUser.getUserId());
						userNotification.setNotificationTypeId(i);
						userNotification.setEnableEmail(true);
						userNotification.setEnablePush(false);
						userNotification.setEnableSMS(false);		
					
						userNotificationRepository.save(userNotification);
					}	
				}

				if (workspaceSettingsRepository.findWorkspaceSettingsByUserId(theUser.getUserId()).iterator().hasNext() == false) {

					WorkspaceSettings workspaceSettings = new WorkspaceSettings();

					workspaceSettings.setUserId(theUser.getUserId());
					workspaceSettings.setEventNotificationId(1);
					workspaceSettings.setOnCallShiftReminderId(1);
					workspaceSettings.setMeetingReminderId(1);
					workspaceSettings.setAssignedWorkShiftsReminderId(1);
					workspaceSettings.setEducationReminderId(1);
					workspaceSettings.setEventsReminderId(1);
					workspaceSettings.setWorkspaceSettingId(sequence.getNextSequenceValue("workspaceSettingId"));
						
					workspaceSettingsRepository.save(workspaceSettings);
				}
				// Create entries for all credentials required based on staff core type
				if(staffUserCoreTypeId > 0) {
					String credentialUrl = configProperties.getCredentialServiceEndPoint() + "/credential/insert";
					String result = insertIntoUserCredentials(credentialUrl, staffUserCoreTypeId, organizationId, theUser.getUserId(), state);
				}

				/*List<CredentialSubTypes> credentialsSubTypes = credentialSubTypesDAO.getCredentialSubTypesByCredentialTypeIdAndCoreTypeIdAndOrganizationId(0, staffUserCoreTypeId,organizationId);
			
				if(staffUserCoreTypeId > 0) {
					for(CredentialSubTypes each:credentialsSubTypes) {
						
						UserCredentials userCredential = new UserCredentials();
						userCredential.setUserId(theUser.getUserId());
						userCredential.setUserCredentialId(sequence.getNextSequenceValue("userCredentialId"));
						userCredential.setPending(true);
						userCredential.setGenerated(true);
						userCredential.setCredentialTypeId(each.getCredentialTypeId());
						userCredential.setCredentialSubTypeId(each.getCredentialSubTypeId());
						userCredential.setState(state);
						Optional<RequiredType> requiredTypeOptional = each.getRequiredType().stream().filter(item->item.getStaffCoreTypeId()==staffUserCoreTypeId).findFirst();
						if(requiredTypeOptional.isPresent()) {
							userCredential.setRequired(requiredTypeOptional.get().getIsRequired());
						}
						userCredentialsRepository.save(userCredential);
					}
				} */
			}

			return theUser;
	}

	private void registerUserPlan(long userId) {
		
		Optional<UserPlan> userPlan = userPlanService.findByUserId(userId);
		if(!userPlan.isPresent()) {
			UserPlan newUserPlan = new UserPlan();
			newUserPlan.setUserId(userId);
			newUserPlan.setPlanType("trial");
			newUserPlan.setActiveStatus("true");
			userPlanService.registerPlan(newUserPlan);
		}
		
		
	}

	@Override
	public UserBasicInformation updateUserBasicInformation(long userId, String firstName, String lastName,
			String middleName, String status, String email, String password, String preferredName, String address1,
			String address2, String city, String state, String country, String postalCode, String phone, String primaryContactCountryCode,
			String profilePhoto, String title, long userTypeId, long staffUserCoreTypeId, long staffUserSubCoreTypeId, String dateOfBirth,
			String linkedInProfile, long organizationId, String gender, String bioInformation, String createdOn,
			String secondaryPhoneNumber, String alternativeContactCountryCode, String secondaryEmail, String fcmToken,
			String deviceType, List <UserLanguages> userLanguages ,UserNationalProviderIdentity userNationalProviderIdentity, SocialSecurityNumber socialSecurityNumber,
			String cvFile, String publicKey, List<EhrSystems> ehrSystemId, String ethnicType, List<String> specialities,
			String invitedSource,long invitedFirmId,long invitedBy,String invitationStatus) throws Exception {

		Optional<UserBasicInformation> optionalUser = userRepository.findById(userId);
		Optional<UserBasicInformation> optEmail = userRepository.findUserByEmail(email);
		UserBasicInformation optPhone = userRepository.findFirstUserByPhone(phone); // to check unique phone number.

		if (optionalUser.isPresent()) {
			UserBasicInformation theUser = optionalUser.get();

			if (organizationId > 0)
				theUser.setOrganizationId(organizationId);

			if (firstName != null)
				theUser.setFirstName(firstName);

			if (lastName != null)
				theUser.setLastName(lastName);

			if (middleName != null)
				theUser.setMiddleName(middleName);

			if(email != null && !theUser.getEmail().equals(email)) {
				if(optEmail.isPresent() == false) {
					theUser.setEmail(email);
				} else 
					throw new UserAlreadyExistsException("User With The Same Email Exists");
			}

			if(phone != null && !theUser.getPhone().equals(phone)) {
				if(optPhone == null) {
					theUser.setPhone(phone); 
				}else 
					throw new Exception("User With The Same Phone Exists");
			}

			if (password != null) {
				try {
					String generatedSecuredPasswordHash = JwtTokenUtil.generateStrongPasswordHash(password);
					theUser.setPassword(generatedSecuredPasswordHash);
				} catch (NoSuchAlgorithmException e) {
					e.printStackTrace();
				} catch (InvalidKeySpecException e) {
					e.printStackTrace();
				}
			}
			if (preferredName != null)
				theUser.setPreferredName(preferredName);

			if (address1 != null) {
				
				theUser.setAddress1(address1);

				Map<String, Double> coords;

				String address = address2 != null ? address1.concat(" ").concat(address2) : address1;
				address = city != null ? address.concat(" ".concat(city)) : address;
				address = state != null ? address.concat(" ".concat(state)) : address;
				address = country != null ? address.concat(" ".concat(country)) : address;

				coords = OpenStreetMapUtils.getInstance().getCoordinates(address);

				if (coords != null && coords.size() > 0) {
					theUser.setLatitude(coords.get("lat").toString());
					theUser.setLongitude(coords.get("lon").toString());
				}
			}
			
			if (StringUtils.hasText(invitedSource))
				theUser.setInvitedSource(invitedSource);

			if (invitedFirmId > 0 )
				theUser.setInvitedFirmId(invitedFirmId);

			if (invitedBy > 0 )
				theUser.setInvitedBy(invitedBy);

			if (StringUtils.hasText(invitationStatus))
				theUser.setInvitationStatus(invitationStatus);
			
			

			if (address2 != null)
				theUser.setAddress2(address2);

			if (city != null)
				theUser.setCity(city);

			if (state != null)
				theUser.setState(state);

			if (country != null)
				theUser.setCountry(country);
			
			if (postalCode != null)
				theUser.setPostalCode(postalCode);

			if (phone != null)
				theUser.setPhone(phone);

			if (primaryContactCountryCode != null)
				theUser.setPrimaryContactCountryCode(primaryContactCountryCode);

			if (profilePhoto != null) {
				if (profilePhoto.length() > 0) {
					String fileName = firstName + "_" + lastName + "_" + LocalDate.now().toString() + ".png";
					fileName = fileName.replaceAll("\\s", ""); // removes spaces
					boolean check = false;
					try {
						check = setProfileImage.uploadImageFile(profilePhoto, fileName, 2);
					} catch (Exception e) {
						// System.out.println("File didn't changed to upload");
					}

					if (check) {
						theUser.setProfilePhoto(uniqueFileName);
						File fileToDelete = FileUtils.getFile(fileName);
						FileUtils.deleteQuietly(fileToDelete);
					}
					// else
					// System.out.println("File could not get uploaded");
				}
			}

			if (title != null)
				theUser.setTitle(title);

			if (userTypeId > 0)
				theUser.setUserTypeId(userTypeId);
			
			if (staffUserCoreTypeId > 0) {

				theUser.setStaffUserCoreTypeId(staffUserCoreTypeId);

				theUser.setStaffUserCoreTypeName(staffUserCoreTypesRepository.findStaffUserCoreTypesById(staffUserCoreTypeId).getLabel());
			}

			if (staffUserSubCoreTypeId > 0)
				theUser.setStaffUserSubCoreTypeId(staffUserSubCoreTypeId);

			if (dateOfBirth != null)
				theUser.setDateOfBirth(LocalDate.parse(dateOfBirth));

			if (gender != null)
				theUser.setGender(gender);

			if (bioInformation != null)
				theUser.setBioInformation(bioInformation);

			if (secondaryPhoneNumber != null)
				theUser.setSecondaryPhoneNumber(secondaryPhoneNumber);

			if (alternativeContactCountryCode != null)
				theUser.setAlternativeContactCountryCode(alternativeContactCountryCode);

			if (secondaryEmail != null)
				theUser.setSecondaryEmail(secondaryEmail);

			if (fcmToken != null)
				theUser.setFcmToken(fcmToken);

			if (deviceType != null)
				theUser.setDeviceType(deviceType);
			
			if (userLanguages != null && userLanguages.size() > 0) {
				optionalUser.get().getUserLanguages().clear(); // clear user's previous language records
				theUser.setUserLanguages(userLanguages);
			}
			
			if (userNationalProviderIdentity != null)
				theUser.setUserNationalProviderIdentity(userNationalProviderIdentity);
			
			if (status != null) {
				
				if (optionalUser.get().getStatusChangedBy() != null && optionalUser.get().getStatusChangedBy() == 3) { // to check who changed the last status

					theUser.setStatus(status);
					theUser.setStatusChangedBy(3); // user type for staff
					userWorkspacesDAO.updateStatusOfUser(userId, status); // set status for every workspace

				}else if (optionalUser.get().getStatusChangedBy() == null ) { // if nobody has changed the status before

					theUser.setStatus(status);
					theUser.setStatusChangedBy(3); // user type for staff
					userWorkspacesDAO.updateStatusOfUser(userId, status); // set status for every workspace

				} else if (optionalUser.get().getStatus().equals("Active") && status.equals("Inactive")) { // if user wants to make inactive

					theUser.setStatus(status);
					theUser.setStatusChangedBy(3); // user type for staff
					userWorkspacesDAO.updateStatusOfUser(userId, status); // set status for every workspace
				}
			}
			
			if (socialSecurityNumber != null)
				theUser.setSocialSecurityNumber(socialSecurityNumber);

			if (specialities != null && specialities.size() > 0)
				theUser.setSpecialities(specialities);
			
			if (cvFile != null)
				theUser.setCvFile(cvFile);
			
			if (publicKey != null)
				theUser.setPublicKey(publicKey);
			
			if (ehrSystemId != null)
				theUser.setEhrSystems(ehrSystemId);

			if (ethnicType != null)
				theUser.setEthnicType(ethnicType);

			userRepository.save(theUser);
			
			if (StringUtils.hasText(invitationStatus) && invitationStatus.equalsIgnoreCase("joined")) {
				registerUserPlan(theUser.getUserId());
			}
			
			if ((optionalUser.get().getUserTypeId() == 2 || optionalUser.get().getUserTypeId() == 3) && userCredentialsRepository.findByUserId(optionalUser.get().getUserId()).size() == 0) {

				List<CredentialSubTypes> credentialsSubTypes = credentialSubTypeRepository.findByStaffCoreType(optionalUser.get().getStaffUserCoreTypeId());

				if(staffUserCoreTypeId > 0) {
					for(CredentialSubTypes each:credentialsSubTypes) {

						UserCredentials userCredential = new UserCredentials();
						userCredential.setUserId(theUser.getUserId());
						userCredential.setUserCredentialId(sequence.getNextSequenceValue("userCredentialId"));
						userCredential.setPending(true);
						userCredential.setCredentialTypeId(each.getCredentialTypeId());
						userCredential.setCredentialSubTypeId(each.getCredentialSubTypeId());
						userCredential.setState(state);
						Optional<RequiredType> requiredTypeOptional = each.getRequiredType().stream().filter(item->item.getStaffCoreTypeId()==staffUserCoreTypeId).findFirst();
						if(requiredTypeOptional.isPresent()) {
							userCredential.setRequired(requiredTypeOptional.get().getIsRequired());
						}
						userCredentialsRepository.save(userCredential);
					}
				}
			}
			return theUser;
		}
		throw new UserAlreadyExistsException("No User found to Update Field!");
	}

	@Override
	public boolean deleteUser(long userId, long organizationId) {

		UserOrganizations user = userOrganizationsDAO.findByUserIdAndOrganizationId(userId, organizationId); // userOrganization

		List<UserWorkspaces> userWorkspacesOptional = userWorkspacesRepository.findUserWorkspacesByUserIdAndOrganizationId(user.getUserId(), organizationId);

		if (user != null) {
			user.setStatus("Deleted");
			userOrganizationsRepository.save(user);

			if (userWorkspacesOptional.size() > 0) {

				List<UserWorkspaces> userWorkspace = userWorkspacesOptional;
				
				for (UserWorkspaces eachUser : userWorkspace) {
					userWorkspacesRepository.delete(eachUser);
					
					DepartmentStaff departmentStaff = departmentStaffRepository.findByDepartmentIdAndStaffUserCoreTypeId(eachUser.getWorkspaceId(), eachUser.getStaffUserCoreTypeId());

					if (departmentStaff != null) {
						departmentStaff.setStaffCount(userWorkspacesRepository.countByWorkspaceIdAndStaffUserCoreTypeIdAndStatusNot(eachUser.getWorkspaceId(), eachUser.getStaffUserCoreTypeId(), "Active"));
						departmentStaffRepository.save(departmentStaff);
					}
				}
			}
		}
		return true;
	}
	
	@Override
	public UserBasicInformation updateUserStatus(long userId, long authorityId, String status) { // to change the status

		Optional<UserBasicInformation> theUser = userRepository.findById(userId);
		Optional<UserBasicInformation> authorityUser = userRepository.findById(authorityId);

		if (theUser.isPresent() && authorityUser.isPresent()) {

			Integer statusChangedBy = theUser.get().getStatusChangedBy(); // fetch the user who changed the status

			if (authorityUser.isPresent() && authorityUser.get().getUserTypeId() == 1 && status != null) {

				if (status.equals("Inactive")) {

					userWorkspacesDAO.updateStatusOfUser(userId, status); // change the status to Inactive for a user in all workspaces.
					theUser.get().setStatus(status);
					theUser.get().setStatusChangedBy(1); // user type for super admin

					userRepository.save(theUser.get());

					return theUser.get();
				}else if (status.equals("Active")) {
	
					userWorkspacesDAO.updateStatusOfUser(userId, status); // change the status to Active for a user in all workspaces.
					theUser.get().setStatus(status);
					theUser.get().setStatusChangedBy(1); // user type for super admin

					userRepository.save(theUser.get());
					
					return theUser.get();
				}else
					return null;
			} else if (authorityUser.get().getUserTypeId() == 2 && status != null) {

				if (status.equals("Inactive")) {

					userWorkspacesDAO.updateStatusOfUserInWorkspace(userId, status, authorityUser.get().getOrganizationId()); // change the status to Inactive for a user in all workspaces.
						
					theUser.get().setStatus(status);	
					theUser.get().setStatusChangedBy(2); // user type for account owner
					userRepository.save(theUser.get());

					return theUser.get();
				}else if (status.equals("Active") && statusChangedBy != null && statusChangedBy > 1 && statusChangedBy <= 3) {
	
					userWorkspacesDAO.updateStatusOfUserInWorkspace(userId, status, authorityUser.get().getOrganizationId()); // change the status to Active for a user in all workspaces.

					if (theUser.get().getStatus().equals("Inactive")) {

						theUser.get().setStatus(status);				
						theUser.get().setStatusChangedBy(2); // user type for account owner
						userRepository.save(theUser.get());
						return theUser.get();
					}else
						return null;
				}else
					return null;
			}else
				return null;
		}else
			return null;
	}

	
	@Override
	public boolean updatePreferredProviders(List<Long> userIds, long organizationId, long businessId, long departmentId) throws IOException {

		List<UserBasicInformation> users = userDAO.findByUserId(userIds);
		if(users.size() > 0) {
			for(UserBasicInformation user : users) {
				List<UserWorkspaces> userWorkSpaces = userWorkspacesRepository.findByUserIdAndBusinessId(user.getUserId(),businessId);
				if(userWorkSpaces.size() > 0) {
					continue;
				}
				
				userWorkspacesService.createUserWorkspace(user.getUserId(), "department", departmentId, "Full Time", false, false,
						organizationId, businessId, "", null, user.getStaffUserCoreTypeId(), false, user.getStaffUserSubCoreTypeId(),0 ,null);
			}
		}else {
		return false;
		}
		return true;
	}
	
@Override	
  public String insertIntoUserCredentials(String url, long staffUserCoreTypeId, long organizationId, long userId, String state) throws Exception {
		
		URL Url = new URL(url);
		URLConnection con = Url.openConnection();
		HttpURLConnection http = (HttpURLConnection)con;
		http.setRequestMethod("POST"); 
		http.setDoOutput(true);
		
		Map<String, Object> stringMap = new HashMap<>();
        stringMap.put("staffUserCoreTypeId", staffUserCoreTypeId);
        stringMap.put("organizationId", organizationId);
        stringMap.put("userId", userId);
        stringMap.put("state", state);
        JSONObject json = new JSONObject(stringMap);
        String jsonString = json.toString();
        
		byte[] out = jsonString.getBytes(StandardCharsets.UTF_8);
		int length = out.length;

		http.setFixedLengthStreamingMode(length);
		http.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
		http.connect();
		try(OutputStream os = http.getOutputStream()) {
		    os.write(out);
		}

		BufferedReader in = new BufferedReader(new InputStreamReader(http.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		return response.toString();
	}
/*public List<UserBasicInformation> findAllStaffMembers(String name, String email, String phone) {
	
	List<UserBasicInformation> listOfusers = new ArrayList<UserBasicInformation>();
	
	if (phone != null) {
		listOfusers.addAll(userDAO.findByPhone(phone));
	}
	
	if (name != null) {
		String[] splitted = name.split("[\\W]");
		if (splitted.length == 1) {
			listOfusers.addAll(userDAO.findByFirstName(splitted[0]));
		} else {
			listOfusers.addAll(userDAO.findByFirstNameAndLastName(splitted[0], splitted[1]));
		}
	}
	
	if (email != null) {
		listOfusers.addAll(userDAO.findUsersByEmail(email));
	} 
	Set<UserBasicInformation> setTemp = new HashSet<>(listOfusers);
	listOfusers.clear();
	listOfusers.addAll(setTemp);
	return listOfusers;
}*/
public List<UserBasicInformation> findAllStaffMembers(String name, String email, String phone, String firmType, long organizationId) {
	
	List<UserBasicInformation> listOfusers = new ArrayList<UserBasicInformation>();
	if(firmType!=null && firmType.equals(ConstantUtils.lawFirm)) {
		
		List<Long> staffUserCoreTypeIds = new ArrayList<Long>();
		staffUserCoreTypeIds.add(ConstantUtils.Attorney);
		staffUserCoreTypeIds.add(ConstantUtils.CaseManager);
		
		if (phone != null && !phone.isEmpty()) {
		listOfusers.addAll(userRepository.findByPhoneAndStaffUserCoreTypeIdAndOrganizationId(phone, staffUserCoreTypeIds, organizationId));
		}
		
		if (name != null && !name.isEmpty()) {
			String[] splitted = name.split("[\\W]");
			if (splitted.length == 1) {
				listOfusers.addAll(userRepository.findByFirstNameAndStaffUserCoreTypeIdAndOrganizationId(splitted[0],staffUserCoreTypeIds,organizationId));
			} else {
				listOfusers.addAll(userRepository.findByFirstNameAndLastNameAndStaffUserCoreTypeIdAndOrganizationId(splitted[0], splitted[1], staffUserCoreTypeIds, organizationId));
			}
		}
		
		if (email != null && !email.isEmpty()) {
			listOfusers.addAll(userRepository.findUsersByEmailAndStaffUserCoreTypeIdAndOrganizationId(email,staffUserCoreTypeIds, organizationId));
		}
		
	} else {
		System.out.println("outside..");
	if (phone != null && !phone.isEmpty()) {
		listOfusers.addAll(userDAO.findByPhone(phone));
	}
	
	if (name != null && !name.isEmpty()) {
		String[] splitted = name.split("[\\W]");
		if (splitted.length == 1) {
			listOfusers.addAll(userDAO.findByFirstName(splitted[0]));
		} else {
			listOfusers.addAll(userDAO.findByFirstNameAndLastName(splitted[0], splitted[1]));
		}
	}
	
	if (email != null && !email.isEmpty()) {
		listOfusers.addAll(userDAO.findUsersByEmail(email));
	} 
	}
	Set<UserBasicInformation> setTemp = new HashSet<>(listOfusers);
	HashSet<Object> seen=new HashSet<>();
	setTemp.removeIf(e->!seen.add(e.getUserId()));
	listOfusers.clear();
	listOfusers.addAll(setTemp);
	return listOfusers;
}
public List<UserWorkspaces> findProvidersAdvancedSearch(String name, String startTime, String endTime,
		List<String> weekDays, double distance, String language, String gender, long staffUserCoreTypeId, long subRoleTypeId, List<String> specialityTags,
		String patientLongitude, String patientLatitude, String ethnicGroup, boolean isPreferred, String postalCode) {
	


	String[] nameSplit = {};
	if(name!=null)
	nameSplit = name.split(" ");
	String firstName = null;
	String lastName = null;

	if (nameSplit.length == 1) {
		firstName = nameSplit[0];
	} else if(nameSplit.length > 1) {
		firstName = nameSplit[0];
		lastName = nameSplit[1];
	}


	if (ethnicGroup == null && gender == null && language == null && weekDays == null && isPreferred == false) {
		// basic filter
		List<UserBasicInformation> users = userDAO.findByStaffUserCoreTypeIdAndSubRoleTypeIdAndSpecialityTags(firstName, lastName, 
				staffUserCoreTypeId, subRoleTypeId, specialityTags, postalCode);

		if (users != null && users.size() > 0) {
			if (distance > 0 && patientLatitude != null && patientLongitude != null) {
				for (UserBasicInformation each : new ArrayList<>(users)) {
		
					if (each.getLatitude() != null && each.getLongitude() != null) {	
						String providerLat = each.getLatitude().split(" ")[0];
						String providerLon = each.getLongitude().split(" ")[0];
						
						if (DistanceCalculate.distance(Double.parseDouble(providerLat), Double.parseDouble(patientLatitude), 
								Double.parseDouble(providerLon), Double.parseDouble(patientLongitude)) <= distance)
							continue;
						else
							users.remove(each);
					}else
						users.remove(each);
				}
			}
			Set<Long> userIds = users.stream().map(e->e.getUserId()).collect(Collectors.toSet());

							return userWorkspacesRepository.findAllByUserId(userIds);
		}else
			return null;
	}else {
		// advance filter
		UserRequest users = userDAO.findByStaffUserCoreTypeIdAndSubRoleTypeIdAndSpecialityTagsAndIsPreferredAndLanguagesAndGenderAndEthnicGroupsAndAvailability(
				firstName, lastName, staffUserCoreTypeId, subRoleTypeId, specialityTags, isPreferred, language, gender, ethnicGroup, startTime, endTime, weekDays, postalCode);

		if (users != null && users.getUserBasicMap().size() > 0) {
			List<UserWorkspaces> userWorkspaces = null;
			if (distance > 0 && patientLatitude != null && patientLongitude != null) {
				for (UserBasicInformation each : new ArrayList<>(users.getUserBasicMap().values())) {

					if (each.getLatitude() != null && each.getLongitude() != null && patientLatitude != null && patientLongitude != null) {	
						if (DistanceCalculate.distance(Double.parseDouble(each.getLatitude()), Double.parseDouble(patientLatitude), 
								Double.parseDouble(each.getLongitude()), Double.parseDouble(patientLongitude)) <= distance)
							continue;
						else {
							userWorkspaces = users.getUserWorkspaces().stream().filter(e -> e.getUserId() == each.getUserId()).collect(Collectors.toList());
						}
					}else
						userWorkspaces = users.getUserWorkspaces().stream().filter(e -> e.getUserId() == each.getUserId()).collect(Collectors.toList());
				}
			}
			if (userWorkspaces != null && userWorkspaces.size() > 0)
				users.getUserWorkspaces().removeAll(userWorkspaces);
			return users.getUserWorkspaces();
		}else
			return null;
	}

}
}