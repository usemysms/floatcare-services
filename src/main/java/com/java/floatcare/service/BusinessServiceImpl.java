package com.java.floatcare.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java.floatcare.api.request.pojo.ContactInformation;
import com.java.floatcare.api.request.pojo.WorksiteAvailability;
import com.java.floatcare.dao.CountersDAO;
import com.java.floatcare.model.Businesses;
import com.java.floatcare.model.Department;
import com.java.floatcare.model.UserWorkspaces;
import com.java.floatcare.repository.BusinessesRepository;
import com.java.floatcare.repository.DepartmentRepository;
import com.java.floatcare.repository.UserWorkspacesRepository;

import graphql.GraphQLException;

@Service
public class BusinessServiceImpl implements BusinessesService {

	@Autowired
	private CountersDAO sequence;
	@Autowired
	private BusinessesRepository businessesRepository;
	@Autowired
	private UserWorkspacesRepository userWorkspacesRepository;
	@Autowired
	private DepartmentRepository departmentRepository;
	
	@Override
	public Businesses createBusiness(long organizationId, String name, long businessTypeId, long businessSubTypeId,
			String phoneNumber, long extension, String email, String address, String address2, String city, String state,
            String postalCode, String country, String status, String coverPhoto, String description, List<WorksiteAvailability> worksiteAvailability, List<Long> preferredProviderId, 
            ContactInformation clientInfo, ContactInformation providerInfo, List<Long> preferredSpecialities) throws Exception {

		Businesses foundBusiness = businessesRepository.findByNameAndAddress(name, address.trim());

		if (foundBusiness == null) {

			Businesses business = new Businesses();
	
			business.setBusinessId(sequence.getNextSequenceValue("businessId"));
	
			business.setOrganizationId(organizationId);
	
			if (name != null)
				business.setName(name);
			else
				business.setName(null);
	
			if (address != null)
				business.setAddress(address);
			
			if (address2 != null)
				business.setAddress2(address2);
			else
				business.setAddress2(null);
			
			business.setBusinessTypeId(businessTypeId);
			
			business.setBusinessSubTypeId(businessSubTypeId);
			
			if(phoneNumber != null)
				business.setPhoneNumber(phoneNumber);
			else
				business.setPhoneNumber(null);
			
			business.setExtension(extension);
			
			if(email != null)
				business.setEmail(email);
			else
				business.setEmail(null);
			
			if(address != null)
				business.setAddress(address);;
			
			if(city != null)
				business.setCity(city);
			else
				business.setCity(null);
			
			if(state != null)
				business.setState(state);
			else
				business.setState(null);
			
			if(postalCode != null)
				business.setPostalCode(postalCode);
			else
				business.setPostalCode(null);
			
			if(country != null)
				business.setCountry(country);
			else
				business.setCountry(null);
			
			if(status != null)
				business.setStatus(status);
			else
				business.setStatus(null);
			
			if(coverPhoto != null)
				business.setCoverPhoto(coverPhoto);
			else
				business.setCoverPhoto(null);
			
			if(description != null)
				business.setDescription(description);
			else
				business.setDescription(null);
			
			business.setCreatedDate(LocalDate.now());

            if (worksiteAvailability != null)
                business.setWorksiteAvailability(worksiteAvailability);

            if (preferredProviderId != null && preferredProviderId.size() > 0)
				business.setPreferredProviderIds(preferredProviderId);
            
            if(clientInfo != null)
            	business.setClientInfo(clientInfo);
            
            if(providerInfo != null)
            	business.setProviderInfo(providerInfo);
            
            if(preferredSpecialities != null && preferredSpecialities.size() > 0)
            	business.setPreferredSpecialities(preferredSpecialities);

			businessesRepository.save(business);
	
			return business;
		}else
			throw new GraphQLException("Same business already exist.");
	}

	@Override
	public Businesses updateBusiness(long businessId, long organizationId, String name, long businessTypeId, long businessSubTypeId,
			String phoneNumber, long extension, String email, String address, String address2, String city, String state,
            String postalCode, String country, String status, String coverPhoto, String description, List<WorksiteAvailability> worksiteAvailability, List<Long> preferredProviderId,
            ContactInformation clientInfo, ContactInformation providerInfo, List<Long> preferredSpecialities) throws Exception {
			
		Optional<Businesses> optBusiness = businessesRepository.findById((businessId));

		if (optBusiness.isPresent()) {
	
			
			Businesses business = optBusiness.get();
	
			//business.setOrganizationId(organizationId);
			
			if (name != null)
				business.setName(name);

			if (address != null && !optBusiness.get().getAddress().equalsIgnoreCase(address)) {

		        Businesses foundBusiness = businessesRepository.findByNameAndAddress(name, address.trim());

		        if (foundBusiness == null) // check if same address with same name of business already exists
					business.setAddress(address);
		        else
		        	throw new Exception("Similar business already exist");
			}
			
			if (address2 != null)
				business.setAddress2(address2);
			
			if (businessTypeId > 0)
				business.setBusinessTypeId(businessTypeId);
			
			if (businessSubTypeId > 0)
				business.setBusinessSubTypeId(businessSubTypeId);
			
			if(phoneNumber != null)
				business.setPhoneNumber(phoneNumber);
			
			business.setExtension(extension);
			
			if(email != null)
				business.setEmail(email);
			
			if(address != null)
				business.setAddress(address);
			
			if(city != null)
				business.setCity(city);
			
			if(state != null)
				business.setState(state);
			
			if(postalCode != null)
				business.setPostalCode(postalCode);
			
			if(country != null)
				business.setCountry(country);
			
			if(status != null)
				business.setStatus(status);
			
			if(description != null)
				business.setDescription(description);

            if (worksiteAvailability != null)
                business.setWorksiteAvailability(worksiteAvailability);

			if (preferredProviderId != null && preferredProviderId.size() > 0)
				business.setPreferredProviderIds(preferredProviderId);

			if(clientInfo != null)
            	business.setClientInfo(clientInfo);
            
            if(providerInfo != null)
            	business.setProviderInfo(providerInfo);
            
            if(preferredSpecialities != null && preferredSpecialities.size() > 0) {
            	List<Long> existingSpecialities = business.getPreferredSpecialities();
            	if(existingSpecialities != null && existingSpecialities.size() > 0) {
            	preferredSpecialities.addAll(existingSpecialities);
            	}
            	business.setPreferredSpecialities(preferredSpecialities);
            }
            
			businessesRepository.save(business);
			return business;
		}else
			throw new Exception("Not found any Business to update!");
	}

	@Override
	public boolean deleteBusiness(long businessId) {

		Optional<Businesses> businessesOptional = businessesRepository.findById(businessId);
		
		if (businessesOptional.isPresent()) {
			
			businessesRepository.deleteById(businessId);
			
			List<Department> departments = departmentRepository.findDepartmentByBusinessId(businessId);
			
			departmentRepository.deleteAll(departments);
			
			List<UserWorkspaces> userWorkspaces = userWorkspacesRepository.findByBusinessId(businessId);
			
			for (UserWorkspaces eachWorkspace: userWorkspaces) {
				
				eachWorkspace.setStatus("Deleted");
				userWorkspacesRepository.save(eachWorkspace);
			}
			
			return true;
		}
		else
			return false;
	}

}
