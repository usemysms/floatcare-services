package com.java.floatcare.service;

import java.text.ParseException;
import java.util.List;

import com.java.floatcare.model.OpenShifts;

public interface OpenShiftService {

	OpenShifts createOpenShift(long worksiteId, long departmentId, long shiftTypeId, String onDate, String startTime, String endTime, 
			long staffUserCoreTypeId, long quota, boolean isUrgent, boolean isSendNotifications, boolean isAutoApprove, boolean isAutoClose, 
			boolean isAllowPartial, Boolean isAllowOvertime, String note, List<Integer> invitees) throws ParseException;

	OpenShifts updateOpenShifts(long userId, long openShiftId, boolean isAccepted, boolean isApproved, String status) throws Exception;
	
	boolean deleteOpenShift(long openShiftId);
	
}

