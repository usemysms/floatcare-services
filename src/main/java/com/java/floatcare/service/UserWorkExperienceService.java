package com.java.floatcare.service;

import java.text.ParseException;

import org.springframework.stereotype.Service;

import com.java.floatcare.model.UserWorkExperience;


@Service
public interface UserWorkExperienceService {


	public UserWorkExperience createUserWorkExperience(long userId, String facilityName, String location, String facilityType,
			String startDate, String endDate, String positionTitle, 
			String unit, String description, boolean isCurrentlyWorking) throws ParseException;
	
	public UserWorkExperience updateUserWorkExperience(long userWorkExperienceId, long userId, String facilityName, String location,
			String facilityType, String startDate, String endDate, String positionTitle, String unit,
			String description, boolean isCurrentlyWorking) throws Exception;
	
	public boolean deleteUserWorkExperience(long userWorkExperienceId);
}
