package com.java.floatcare.service;

import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.TreeSet;
import java.util.stream.Collectors;

import com.java.floatcare.api.response.pojo.UnreadMessagesAndNotificationsCount;
import com.java.floatcare.dao.UserNotificationDAO;
import com.java.floatcare.utils.InAppNotifications;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.java.floatcare.dao.CountersDAO;
import com.java.floatcare.dao.MessagesDAO;
import com.java.floatcare.model.DeletedMessages;
import com.java.floatcare.model.GroupChats;
import com.java.floatcare.model.Messages;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.repository.DeletedMessagesRepository;
import com.java.floatcare.repository.MessagesRepository;
import com.java.floatcare.repository.UserBasicInformationRepository;
import com.java.floatcare.utils.GoogleFirebaseUtilityDAO;
import com.java.floatcare.utils.MessageNotificationCountDAO;

import graphql.GraphQLException;

@Service
public class MessageServiceImpl implements MessageService{

	@Autowired
	private CountersDAO sequence;
	@Autowired
	private MessageNotificationCountDAO countUtil;
	@Autowired
	private DeletedMessagesRepository deletedMessagesRepository;
	@Autowired
	private GoogleFirebaseUtilityDAO googleFirebaseUtilityDAO;
	@Autowired
	private UserBasicInformationRepository userBasicInformationRepository;
	@Autowired
	private MessagesRepository messageRepository;
	@Autowired
	private MessagesDAO messagesDAO;
	@Autowired
	private InAppNotifications inAppNotifications;
	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Override
	public Messages createMessages(String text, Long senderId, Long receiverId, String timeStamp, String timeZone, 
			boolean isSeen, String date, long epochSeconds, String aesKey, String initializationVector) {

		Messages messages = new Messages();

		messages.setText(text);
		
		if (senderId > 0)
			messages.setSenderId(senderId);
		if (receiverId > 0)
			messages.setReceiverId(receiverId);
		try {
			messages.setTimeStamp(LocalTime.parse(timeStamp));
		} catch(Exception e) {
			
		}
		messages.setTimeZone(timeZone);
		messages.setSeen(isSeen);
		messages.setDate(date);
		messages.setEpochSeconds(epochSeconds);
		messages.setAesKey(aesKey);
		messages.setInitializationVector(initializationVector);

		if (senderId > 0 && receiverId > 0) {
			
			messages.setMessageId(sequence.getNextSequenceValue("messageId"));	
			messageRepository.save(messages);
			
			DeletedMessages deletedMessage = messagesDAO.findByUserIdAndReceiverMessagesDeletedUserIdAndIsReplied(senderId, receiverId, false);
			
			if (deletedMessage != null) {
				
				deletedMessage.setReplied(true);
				deletedMessagesRepository.save(deletedMessage);
			}
			
			UserBasicInformation senderUser = userBasicInformationRepository.findUsersByUserId(senderId);
			UserBasicInformation receiverUser = userBasicInformationRepository.findUsersByUserId(receiverId);
			
			String userName = senderUser.getFirstName()+" "+senderUser.getLastName();
			String receiverFcmToken = receiverUser.getFcmToken();
			String message = userName+" has messaged you.";
			String messageBody = text;
			String profilePhoto = senderUser.getProfilePhoto();
			String deepLink = "2"; // Deep Link for sending new message to user
			if (receiverUser.getDeviceType() != null && receiverUser.getDeviceType().equalsIgnoreCase("ios")) { // to send the notification count.
				int badgeCount = countUtil.getMessageCount(receiverId);
				googleFirebaseUtilityDAO.sendNotificationToIosUser(userName, senderId, receiverId, profilePhoto, receiverFcmToken, message, messageBody,
							deepLink, aesKey, initializationVector, badgeCount);
			}else if (receiverUser.getDeviceType().equalsIgnoreCase("Android")) {
				googleFirebaseUtilityDAO.sendMessageNotificationToUser(userName, senderId, receiverId, 0, profilePhoto, receiverFcmToken, message,
						messageBody, deepLink, aesKey, initializationVector, receiverUser.getDeviceType());
			}
//			// Fetch the user notifications based on settings
//			UserNotification userNotificationSetting = userNotificationRepository.findByNotificationTypeIdAndUserId(6,receiverId);
//	
//			 if (userNotificationSetting != null && userNotificationSetting.isEnableEmail() == true) { 
//				String email = userBasicInformationRepository.findUsersByUserId(receiverId).getEmail();
//			 	mailUtil.sendMail(email, message, messageBody); // send Email }
//			 }
		}
		return messages;
	}
	
	@Override
	public Messages updateMessage (long messageId, long userId, boolean isSeen) {
		
		Optional<Messages> messageOptional = messageRepository.findById(messageId);
		
		if (messageOptional.isPresent()) {
			
			long senderId = messageOptional.get().getSenderId();
			long receiverId = messageOptional.get().getReceiverId();
			
			if (receiverId == userId) {

				List<Messages> messageList = messageRepository.findMessagesBySenderIdAndReceiverId(senderId, receiverId);

				messageList = messageList.stream().peek(emitter -> emitter.setSeen(true)).collect(Collectors.toList());

				messageRepository.saveAll(messageList);

				return messageList.get(messageList.size()-1);
			}else {

				messagesDAO.updateIsSeenMessages(receiverId, senderId);

				return messageOptional.get();
			}
		}else {
			throw new GraphQLException("Message Id does not exist");
		}
	}
	
	@Override
	public boolean deleteMessagesOfUser (long senderId, long receiverId) {
		
		if (receiverId > 0 && senderId > 0) {
			
			// collect all the messages between sender and receiver ids.
			List<Long> messageIds = messagesDAO.findAllMessagesBySenderIdAndReceiverId(senderId, receiverId).stream().map(Messages::getMessageId).collect(Collectors.toList());

			// create delete message instance.
			DeletedMessages deletedMessage = new DeletedMessages();
			
			// check if the sender and receiver message already exists in db.
			DeletedMessages deletedMessageData = deletedMessagesRepository.findByUserIdAndReceiverMessagesDeleteduserIdAndIsReplied(senderId, receiverId, true);

			// create a new record
			if (deletedMessageData == null) {
				
				deletedMessage.setUserId(senderId);
				deletedMessage.setReceiverMessagesDeleteduserId(receiverId);
				deletedMessage.setDeletedMessageId(sequence.getNextSequenceValue("deletedMessageId"));
				deletedMessage.setReplied(false);
				deletedMessage.setMessageIds(messageIds);

				if (deletedMessagesRepository.findByUserIdAndReceiverMessagesDeleteduserIdAndIsReplied(senderId, receiverId, false) == null) {
					deletedMessagesRepository.save(deletedMessage);
					return true;
				}else
					return false;

			} else { // update existing record
				
				deletedMessageData.getMessageIds().clear();

				deletedMessage = deletedMessageData;
				
				deletedMessage.setMessageIds(messageIds);
				deletedMessage.setReplied(false);

				deletedMessagesRepository.save(deletedMessage);
				return true;
			}
		} else
			return false;
	}
	
	@Override
	public Messages createAGroupMessage(long chatGroupId, String text, long senderId, String timeStamp, String timeZone, 
			String date, long epochSeconds, String aesKey, String initializationVector, String type, List<String> attachments, 
			String sourceType) {
		
		if (senderId > 0 && chatGroupId > 0) {
			
			Criteria criteria1 = Criteria.where("chatGroupId").is(chatGroupId);
			Criteria criteria2 = Criteria.where("members").in(senderId);
			Criteria criteria3 = Criteria.where("admins").in(senderId);

			GroupChats groupChatObj = mongoTemplate.findOne(Query.query(criteria1.orOperator(criteria3,criteria2)), GroupChats.class); // to check if the sender id exists in the group

			if (groupChatObj != null) {

				Messages message = new Messages();

				message.setText(text);

				message.setSenderId(senderId);
				try {
					message.setTimeStamp(LocalTime.parse(timeStamp));
				}catch (Exception e){
					
				}
				message.setTimeZone(timeZone);
				message.setDate(date);
				message.setEpochSeconds(epochSeconds);
				message.setAesKey(aesKey);
				message.setInitializationVector(initializationVector);
				message.setGroupId(chatGroupId);
				
				if (type != null)
					message.setText(text);
		
				if (attachments != null) {
					message.setAttachments(attachments);
				}
				message.setMessageId(sequence.getNextSequenceValue("messageId"));	

				messageRepository.save(message);
				
				TreeSet<Long> groupMembers = groupChatObj.getAdmins();
				groupMembers.addAll(groupChatObj.getMembers());

				messagesDAO.findAndUpdateIsRepliedByUserIdAndGroupId(groupMembers, message.getGroupId());


				UserBasicInformation senderUser = userBasicInformationRepository.findUsersByUserId(senderId);
				String userName = senderUser.getFirstName()+" "+senderUser.getLastName();
				String notificationHeader = userName+" has messaged in "+groupChatObj.getName();
				String profilePhoto = senderUser.getProfilePhoto();
				String messageBody = text;
				
				GroupChats foundGroupChat = mongoTemplate.findOne(Query.query(Criteria.where("chatGroupId").is(chatGroupId)), GroupChats.class);
				
				if (foundGroupChat != null) {
	
					TreeSet<Long> staffIds = foundGroupChat.getMembers();
					staffIds.addAll(foundGroupChat.getAdmins());
					staffIds.remove(senderId);
	
					Iterable<UserBasicInformation> user = userBasicInformationRepository.findAllById(staffIds);

					for (UserBasicInformation eachUser : user) {

						String receiverFcmToken = eachUser.getFcmToken();

						String deepLink = "2"; // Deep Link for sending new message to user
						if (eachUser.getDeviceType() != null && eachUser.getDeviceType().equalsIgnoreCase("ios")) { // to send the notification count.

							int badgeCount = countUtil.getMessageCount(eachUser.getUserId());
							googleFirebaseUtilityDAO.sendNotificationToIosUser(userName, senderId, eachUser.getUserId(), profilePhoto,
									receiverFcmToken, notificationHeader, messageBody, deepLink, aesKey, initializationVector, badgeCount);

						}else if (eachUser.getDeviceType() != null && eachUser.getDeviceType().equalsIgnoreCase("Android")) {

							googleFirebaseUtilityDAO.sendMessageNotificationToUser(userName, senderId, chatGroupId, eachUser.getUserId(), profilePhoto, 
									receiverFcmToken, notificationHeader, messageBody, deepLink, aesKey, initializationVector, eachUser.getDeviceType());

						}
					}
				}
				return message;
			}else
				return null;
		}else
			return null;
	}
	
	@Override
	public boolean deleteGroupChats (long groupId, long userId) {
		
		DeletedMessages deletedMessage = deletedMessagesRepository.findByUserIdAndGroupId(userId, groupId);

		List<Long> groupMessageIds = messageRepository.findByGroupIdAndSenderIdNot(groupId, Long.parseLong("0")).stream().map(e->e.getMessageId()).collect(Collectors.toList());

		if (deletedMessage == null) {

			DeletedMessages deletedMessageObj = new DeletedMessages();

			deletedMessageObj.setDeletedMessageId(sequence.getCurrentSequenceValue("deletedMessageId"));
			deletedMessageObj.setGroupId(groupId);
			deletedMessageObj.setUserId(userId);
			deletedMessageObj.setMessageIds(groupMessageIds);
			deletedMessageObj.setReplied(true);
			
			deletedMessagesRepository.save(deletedMessageObj);
			
			return true;
		}else {

			deletedMessage.getMessageIds().clear();

			deletedMessage.setMessageIds(groupMessageIds);
			deletedMessage.setReplied(false);

			deletedMessagesRepository.save(deletedMessage);

			return true;
		}
	}
	
	@Override
	public boolean deleteSingleMessageForAUserInGroup(long groupId, long userId, long messageId) {
		
		DeletedMessages deletedMessage = deletedMessagesRepository.findByUserIdAndGroupId(userId, groupId);
		
		if (deletedMessage != null) { // if record exist
			
			if (deletedMessage.getMessageIds() != null && deletedMessage.getMessageIds().size() > 0
					&& !deletedMessage.getMessageIds().contains(messageId)) {

				deletedMessage.addSingleDeletedMessage(messageId);
				deletedMessage.setReplied(false);
				
				deletedMessagesRepository.save(deletedMessage);
			
				return true;
			}else
				return false;
		}else { // if no record for userId and grouId exist

			DeletedMessages newDeletedMessageObj = new DeletedMessages();
			
			newDeletedMessageObj.setGroupId(groupId);
			newDeletedMessageObj.setUserId(userId);
			newDeletedMessageObj.setReplied(false);
			newDeletedMessageObj.setMessageIds(Arrays.asList(messageId));
			
			deletedMessagesRepository.save(newDeletedMessageObj);
			
			return true;
		}
	}

	@Override
	public boolean deleteSingleMessageForAUserInPrivateMessage(long receiverId, long userId, long messageId) {
		
		DeletedMessages deletedMessage = deletedMessagesRepository.findByUserIdAndReceiverMessagesDeleteduserId(userId, receiverId);

		if (deletedMessage != null) { // if record exist
			
			if (deletedMessage.getMessageIds() != null && deletedMessage.getMessageIds().size() > 0
					&& !deletedMessage.getMessageIds().contains(messageId)) {

				deletedMessage.addSingleDeletedMessage(messageId);
				deletedMessage.setReplied(true);

				deletedMessagesRepository.save(deletedMessage);
			
				return true;
			}else
				return false;
		}else { // if no record for userId and grouId exist

			DeletedMessages newDeletedMessageObj = new DeletedMessages();
			
			newDeletedMessageObj.setReceiverMessagesDeleteduserId(receiverId);
			newDeletedMessageObj.setUserId(userId);
			newDeletedMessageObj.setReplied(true);
			newDeletedMessageObj.setMessageIds(Arrays.asList(messageId));
			
			deletedMessagesRepository.save(newDeletedMessageObj);

			return true;
		}
	}
	@Override
	public UnreadMessagesAndNotificationsCount getUnreadMessagesAndNotificationsCount(long userId) {
		UnreadMessagesAndNotificationsCount unreadMessagesAndNotificationsCount = new UnreadMessagesAndNotificationsCount();
		unreadMessagesAndNotificationsCount.setUserId(userId);
		unreadMessagesAndNotificationsCount.setUnreadMessagesCount(messagesDAO.getUnreadMessagesCount(userId));
		unreadMessagesAndNotificationsCount.setUnreadNotificationsCount(inAppNotifications.getUnreadNotificationsCount(userId));
		return unreadMessagesAndNotificationsCount;
	}
}
