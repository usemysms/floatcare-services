package com.java.floatcare.service;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.java.floatcare.dao.CountersDAO;
import com.java.floatcare.model.Assignments;
import com.java.floatcare.utils.InAppNotifications;

import graphql.GraphQLException;

@Service
public class AssignmentsServiceImpl implements AssignmentsService {

	
	@Autowired
	private CountersDAO sequence;
	@Autowired
	private MongoTemplate mongoTemplate;
	@Autowired
	private InAppNotifications inAppNotifications;
	
	@Override
	public Assignments createAssignment(long businessId, long clientId, String startDate, String endDate) {
		
		Assignments assignments = new Assignments();
		
		assignments.setBusinessId(businessId);
		assignments.setClientId(clientId);
		assignments.setStatus("Draft");
		
		if (startDate != null && startDate.length() > 1)
			assignments.setStartDate(LocalDate.parse(startDate));
		else
			assignments.setStartDate(null);
		
		if (endDate != null && endDate.length() > 1)
			assignments.setEndDate(LocalDate.parse(endDate));
		else
			assignments.setEndDate(null);
		
		assignments.setAssignmentId(sequence.getNextSequenceValue("assignmentId"));
		
		mongoTemplate.save(assignments, "assignments");
		
		return assignments;
	}
	
	@Override
	public Assignments updateAssignment(long assignmentId, long clientId, String startDate, String endDate) {
		
		Assignments findAssignment = mongoTemplate.findOne(Query.query(Criteria.where("_id").is(assignmentId)), Assignments.class);
		
		if (findAssignment != null) {
			
			Assignments assignment = findAssignment;
			
			if (clientId > 0)
				assignment.setClientId(clientId);
			
			if (startDate != null && startDate.length() > 5)
				assignment.setStartDate(LocalDate.parse(startDate));
			
			if (endDate != null && endDate.length() > 5)
				assignment.setEndDate(LocalDate.parse(endDate));
			
			mongoTemplate.save(assignment, "assignments");
			
			return assignment;
		} else
			throw new GraphQLException("assignmentId is incorrect.");
	}
	
	@Override
	public boolean deleteAssignment (long assignmentId) {
		
		Assignments foundAssignment = mongoTemplate.findOne(Query.query(Criteria.where("_id").is(assignmentId)), Assignments.class);
		
		if (foundAssignment != null && foundAssignment.getEndDate() != null) {

			inAppNotifications.deleteBySourceId(Arrays.asList(assignmentId));

			long dateDifference = ChronoUnit.DAYS.between(LocalDate.now(), foundAssignment.getEndDate())+1;

			if ((dateDifference >= 0) && (foundAssignment.getStatus().equals("Draft") || foundAssignment.getStatus().equals("Active"))) {

				foundAssignment.setStatus("Inactive");

				mongoTemplate.save(foundAssignment, "assignments");

				return true;
			} else if ((dateDifference < 0 && dateDifference > -30) && (foundAssignment.getStatus().equals("draft") ||
					foundAssignment.getStatus().equals("Inactive") || foundAssignment.getStatus().equals("Active"))) {

				foundAssignment.setStatus("DeletedAssignment");

				mongoTemplate.save(foundAssignment, "assignments");

				return true;
			}else if (dateDifference < -30){
				
				mongoTemplate.remove(foundAssignment, "assignments");

				return true;
			}
			else
				return false;
		}else {		
			mongoTemplate.remove(foundAssignment, "assignments");
			return false;
		}
	}
}
