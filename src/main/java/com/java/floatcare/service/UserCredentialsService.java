package com.java.floatcare.service;

import java.text.ParseException;

import com.java.floatcare.model.UserCredentials;

public interface UserCredentialsService {

	public UserCredentials createUserCredentials(long userId, String credentialIcon, String credentialName, long credentialTypeId,
			long credentialSubTypeId, String expirationDate, long credentialStatusId,String licenseNumber, String frontFilePhoto, 
			String backFilePhoto, String state, boolean isCompactLicense, boolean isPending) throws ParseException;
	
	public UserCredentials updateUserCredentials(long userCredentialId, long userId, String credentialIcon, String credentialName, long credentialTypeId,
			long credentialSubTypeId, String expirationDate, long credentialStatusId,String licenseNumber, String frontFilePhoto, String backFilePhoto, 
			String state, boolean isCompactLicense, boolean isVerified, long verifiedBy,boolean isPending) throws Exception;
	
	public boolean deleteUserCredentials(long userCredentialId);
}
