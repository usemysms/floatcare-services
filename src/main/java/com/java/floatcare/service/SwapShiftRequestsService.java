package com.java.floatcare.service;

import com.java.floatcare.model.SwapShiftRequests;

public interface SwapShiftRequestsService {

	SwapShiftRequests createSwapShiftRequest(long senderId, long recieverId, long sourceShiftId, long requestedShiftId);
	
	SwapShiftRequests updateSwapShiftRequest(long swapShiftRequestId, boolean isRecieverAccepted, boolean isSupervisorAccepted) throws Exception;
	
	boolean deleteSwapShiftRequest(long swapShiftRequestId);
}
