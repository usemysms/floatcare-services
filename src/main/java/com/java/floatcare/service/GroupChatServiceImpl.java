package com.java.floatcare.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import com.java.floatcare.api.request.pojo.IsSeenByGroupMembers;
import com.java.floatcare.dao.CountersDAO;
import com.java.floatcare.dao.GroupChatsDAO;
import com.java.floatcare.dao.MessagesDAO;
import com.java.floatcare.model.DeletedMessages;
import com.java.floatcare.model.GroupChats;
import com.java.floatcare.model.Messages;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.repository.DeletedMessagesRepository;
import com.java.floatcare.repository.MessagesRepository;
import com.java.floatcare.repository.UserBasicInformationRepository;

@Service
public class GroupChatServiceImpl implements GroupChatService {

	@Autowired
	private MongoTemplate mongoTemplate;
	@Autowired
	private CountersDAO sequence;
	@Autowired
	private UserBasicInformationRepository userRepository;
	@Autowired
	private GroupChatsDAO groupChatsDAO;
	@Autowired
	private MessagesRepository messagesRepository;
	@Autowired
	private MessagesDAO messageDAO;
	@Autowired
	private DeletedMessagesRepository deletedMessagesRepository;
	@Autowired
	private MessagesDAO messagesDAO;
	
	@Override
	public GroupChats createAGroup(long groupId, String groupName, long organizationId, List<Long> groupAdmins, List<Long> staffMembers, String createdOn) throws Exception {
		
		if (groupId > 0) {

			GroupChats foundGroupChat = groupChatsDAO.findGroupChatByGroupChatId(groupId);

			if (foundGroupChat != null) {

				if (groupName != null) {

					if (groupAdmins != null || staffMembers != null) {
						Messages message = new Messages();
	
						if (groupAdmins != null && groupAdmins.size() > 0)
							message.setText(userRepository.findUsersByUserId(groupAdmins.get(0)).getFirstName()+" changed the group name to "+groupName);
						else if (staffMembers != null && staffMembers.size() > 0)
							message.setText(userRepository.findUsersByUserId(staffMembers.get(0)).getFirstName()+" changed the group name to "+groupName);
	
						message.setGroupId(groupId);
						message.setDate(LocalDate.now().toString());
						message.setSourceType("generated");
						message.setMessageId(sequence.getNextSequenceValue("messageId"));
	
						messagesRepository.save(message);
						
						foundGroupChat.setName(groupName);
					}
				}
				if (groupAdmins != null && groupAdmins.size() > 0) {

					if (groupAdmins.size() < foundGroupChat.getAdmins().size()) {

						foundGroupChat.getAdmins().removeAll(groupAdmins);
						TreeSet<Long> deletedAdmins = foundGroupChat.getAdmins();

						foundGroupChat.setAdmins(new TreeSet<>(groupAdmins)); // add admins

						for (Long groupAdmin : deletedAdmins) {
	
							Messages message = new Messages();
	
							message.setText(userRepository.findUsersByUserId(groupAdmin).getFirstName()+" is removed from the group.");
							message.setGroupId(groupId);
							message.setDate(LocalDate.now().toString());
							message.setSourceType("generated");
							message.setMessageId(sequence.getNextSequenceValue("messageId"));
							
							messagesRepository.save(message);
						}
					}else if (groupAdmins.size() > foundGroupChat.getAdmins().size()) {

						TreeSet<Long> membersInGroup = foundGroupChat.getAdmins();

						foundGroupChat.setAdmins(new TreeSet<>(groupAdmins)); // add admins
						groupAdmins.removeAll(membersInGroup);

						for (Long groupAdmin : groupAdmins) {
	
							Messages message = new Messages();
	
							message.setText(userRepository.findUsersByUserId(groupAdmin).getFirstName()+" is now the group admin.");
							message.setGroupId(groupId);
							message.setDate(LocalDate.now().toString());
							message.setSourceType("generated");
							message.setMessageId(sequence.getNextSequenceValue("messageId"));
							
							messagesRepository.save(message);
						}
					}
				}

				if (staffMembers != null && staffMembers.size() > 0) {

					if (staffMembers.size() > foundGroupChat.getMembers().size()) {

						TreeSet<Long> membersInGroup = foundGroupChat.getMembers();
						foundGroupChat.setMembers(new TreeSet<>(staffMembers));
						foundGroupChat.getMembers().removeAll(foundGroupChat.getAdmins());
						staffMembers.removeAll(membersInGroup);
						List<Long> membersAdded = staffMembers;

						for (Long each : membersAdded) {
							
							Messages messageForStaff = new Messages();
							
							messageForStaff.setText(userRepository.findUsersByUserId(each).getFirstName()+" is added in the group.");
							messageForStaff.setGroupId(groupId);
							messageForStaff.setDate(LocalDate.now().toString());
							messageForStaff.setSourceType("generated");
							messageForStaff.setMessageId(sequence.getNextSequenceValue("messageId"));

							messagesRepository.save(messageForStaff);
						}

					}else if (staffMembers.size() < foundGroupChat.getMembers().size()) {

						foundGroupChat.getMembers().removeAll(staffMembers);
						TreeSet<Long> membersRemoved = new TreeSet<>(foundGroupChat.getMembers());
						foundGroupChat.setMembers(new TreeSet<>(staffMembers));

						for (Long each : membersRemoved) {
							
							Messages messageForStaff = new Messages();
							
							messageForStaff.setText(userRepository.findUsersByUserId(each).getFirstName()+" is removed from the group.");
							messageForStaff.setGroupId(groupId);
							messageForStaff.setDate(LocalDate.now().toString());
							messageForStaff.setSourceType("generated");
							messageForStaff.setMessageId(sequence.getNextSequenceValue("messageId"));

							messagesRepository.save(messageForStaff);
						}
					}
				}

				foundGroupChat.getMembers().removeAll(groupAdmins);
				
				mongoTemplate.save(foundGroupChat, "group_chats");

				return foundGroupChat;
				
			}else
				throw new Exception("group id does not exist.");
			
		}else { //create new group chat
			
			GroupChats groupChat = new GroupChats();
			
			if (staffMembers != null && staffMembers.size() > 0) {
				staffMembers.removeAll(groupAdmins);
				groupChat.setMembers(new TreeSet<>(staffMembers));
			}

			if (groupName != null)
				groupChat.setName(groupName);
			else
				groupChat.setName(userRepository.findById(groupAdmins.get(0)).get().getFirstName()+" Group");

			groupChat.setAdmins(new TreeSet<>(groupAdmins));

			groupChat.setOrganizationId(organizationId);

			groupChat.setChatGroupId(sequence.getNextSequenceValue("chatGroupId"));

			groupChat.setCreatedOn(createdOn);

			UserBasicInformation admin = userRepository.findUsersByUserId(groupAdmins.get(0));

			Messages message = new Messages();

			message.setText(admin.getFirstName() +" created the group.");
			message.setGroupId(groupChat.getChatGroupId());
			message.setDate(LocalDate.now().toString());
			message.setSourceType("generated");
			message.setMessageId(sequence.getNextSequenceValue("messageId"));
			
			messagesRepository.save(message);

			for (Long each : groupChat.getMembers()) {

				UserBasicInformation staff = userRepository.findUsersByUserId(each);

				Messages messageForStaff = new Messages();

				messageForStaff.setText(admin.getFirstName()+" invited "+ staff.getFirstName());
				messageForStaff.setGroupId(groupChat.getChatGroupId());
				messageForStaff.setDate(LocalDate.now().toString());
				messageForStaff.setSourceType("generated");
				messageForStaff.setMessageId(sequence.getNextSequenceValue("messageId"));

				messagesRepository.save(messageForStaff);
			}

			if (groupAdmins.size() > 0)
				mongoTemplate.save(groupChat, "group_chats");

			return groupChat;
		}
	}

	@Override
	public Messages updateUnSeenMessagesInGroup (long messageId, long staffId) {

		Optional<Messages> message = messagesRepository.findById(messageId);

		if (message.isPresent()) {
			List<Messages> messagesToUpdate = messageDAO.findAllByGroupIdAndSenderIdAndMessageIds(message.get().getGroupId(), staffId, null);
			if (messagesToUpdate.size() > 0) {
	
				GroupChats groupChat = groupChatsDAO.findGroupChatsByGroupIdAndStaffId(message.get().getGroupId(), staffId);
				
				if (groupChat != null) {
	
					IsSeenByGroupMembers isSeenBy = new IsSeenByGroupMembers();
					isSeenBy.setUserId(staffId);
					isSeenBy.setTimeStamp(null);
					
					if (messagesToUpdate.size() > 0) {
	
						for (Messages each : messagesToUpdate) {

							if (each.getIsSeenByGroupMembers() != null || each.getIsSeenByGroupMembers().size() > 0) {
								each.setIsSeenByGroupMembers(isSeenBy);
								messagesRepository.save(each);
							}else if (each.getIsSeenByGroupMembers() == null ||each.getIsSeenByGroupMembers().size() == 0) {
								each.setIsSeenByGroupMembers(isSeenBy);
								messagesRepository.save(each);
							}
						}
					}
					return message.get();
				}else
					return message.get();
			}else
				return null;
		}else
			return null;
	}

	@Override
	public boolean removeGroupMember(long groupId, long adminStaffId, long staffId) {

		GroupChats foundGroupChat = groupChatsDAO.findGroupChatsByGroupIdAndAdminIdAndStaffId(groupId, adminStaffId, staffId) ;

		if (foundGroupChat != null) {
			
			UserBasicInformation adminInfo = userRepository.findUsersByUserId(adminStaffId);
			UserBasicInformation staffInfo = userRepository.findUsersByUserId(staffId);

			if (foundGroupChat.getAdmins().contains(staffId)) {
			
				foundGroupChat.getAdmins().remove(staffId); // remove as admin
				
				if (foundGroupChat.getAdmins().size() == 0) { // in case all admins are deleted
					if (foundGroupChat.getMembers().size() > 0) {
						long newAdmin = foundGroupChat.getMembers().stream().findFirst().get().longValue();
						foundGroupChat.setAdmin(newAdmin); // assign new admin from the staff
						foundGroupChat.getMembers().remove(newAdmin); // remove the staff from member add added to admins list
					}
				}

				Messages message = new Messages();
				
				message.setText(adminInfo.getFirstName()+" removed "+ staffInfo.getFirstName());
				message.setGroupId(groupId);
				message.setDate(LocalDate.now().toString());
				message.setSourceType("generated");
				message.setMessageId(sequence.getNextSequenceValue("messageId"));
				
				messagesRepository.save(message);

				mongoTemplate.save(foundGroupChat, "group_chats");

				return true;
			}else if (foundGroupChat.getMembers() != null && foundGroupChat.getMembers().contains(staffId)) {

				foundGroupChat.getMembers().remove(staffId); // remove as member
				
				Messages message = new Messages();
				
				message.setText(adminInfo.getFirstName()+" removed "+ staffInfo.getFirstName());
				message.setGroupId(groupId);
				message.setDate(LocalDate.now().toString());
				message.setSourceType("generated");
				message.setMessageId(sequence.getNextSequenceValue("messageId"));
				
				messagesRepository.save(message);

				mongoTemplate.save(foundGroupChat, "group_chats");

				return true;
			}else
				return false;
		}else
			return false;
	}

	@Override
	public boolean leaveAGroup(long groupId, long staffId) {
		
		GroupChats foundGroupChat = groupChatsDAO.findGroupChatsByGroupIdAndStaffId(groupId, staffId) ;

		if (foundGroupChat != null && (foundGroupChat.getMembers().size() > 0 || foundGroupChat.getAdmins().size() > 0)) {
			
			if (foundGroupChat.getMembers().contains(staffId)) { // if the staffid exists in members
				
				foundGroupChat.getMembers().remove(staffId);
				
				Messages message = new Messages();

				message.setText(userRepository.findUsersByUserId(staffId).getFirstName()+" has left the group");
				message.setGroupId(groupId);
				message.setDate(LocalDate.now().toString());
				message.setSourceType("generated");
				message.setMessageId(sequence.getNextSequenceValue("messageId"));

				messagesRepository.save(message);
				
				mongoTemplate.save(foundGroupChat, "group_chats");

				return true;
			}
			else {
				foundGroupChat.getAdmins().remove(staffId);

				Messages message = new Messages();
				
				message.setText(userRepository.findUsersByUserId(staffId).getFirstName()+" has left the group");
				message.setGroupId(groupId);
				message.setDate(LocalDate.now().toString());
				message.setSourceType("generated");
				message.setMessageId(sequence.getNextSequenceValue("messageId"));

				messagesRepository.save(message);

				if (foundGroupChat.getAdmins().size() == 0) { // in case all admins are deleted
					if (foundGroupChat.getMembers().size() > 0) {
						long newAdmin = foundGroupChat.getMembers().stream().findFirst().get().longValue();
						foundGroupChat.setAdmin(newAdmin); // assign new admin from the staff
						foundGroupChat.getMembers().remove(newAdmin); // remove the staff from member add added to admins list
					}
				}
				mongoTemplate.save(foundGroupChat, "group_chats");
				return true;
			}
		} else
			return false;
	}

	@Override
	public boolean deleteAGroup (long chatGroupId, long staffUserId) {
		
		GroupChats groupChat = groupChatsDAO.findGroupChatByGroupChatId(chatGroupId);

		if (groupChat != null) {

			if (groupChat.getAdmins().contains(staffUserId)) { // if admin deletes the group, delete messages, and group permanently

				messagesRepository.deleteAllByGroupId(chatGroupId);
				messagesDAO.removeDeletedMessagesEntryByGroupId(chatGroupId);
				groupChatsDAO.removeGroup(chatGroupId);

				return true;
			}else if (groupChat.getMembers().contains(staffUserId)) { // if normal staff member, then delete user's messages

				List<Long> groupMessageIds = messagesRepository.findAllByGroupId(chatGroupId).stream().map(e->e.getMessageId()).collect(Collectors.toList());

				Messages message = new Messages();

				message.setText(userRepository.findUsersByUserId(staffUserId).getFirstName()+" has left the group"); // generate a text
				message.setGroupId(chatGroupId);
				message.setDate(LocalDate.now().toString());
				message.setSourceType("generated");
				message.setMessageId(sequence.getNextSequenceValue("messageId"));

				messagesRepository.save(message);
				
				DeletedMessages deletedMessageObj = new DeletedMessages();

				deletedMessageObj.setDeletedMessageId(sequence.getNextSequenceValue("deletedMessageId"));
				deletedMessageObj.setGroupId(chatGroupId);
				deletedMessageObj.setUserId(staffUserId);
				deletedMessageObj.setMessageIds(groupMessageIds);
				deletedMessageObj.setReplied(false);

				deletedMessagesRepository.save(deletedMessageObj);

				groupChat.getMembers().remove(staffUserId);
				mongoTemplate.save(groupChat, "group_chats");
				return true;
			}else
				return false;
		}else
			return false;
	}

	@Override
	public GroupChats addAdminsInGroup (long chatGroupId, List<Long> adminIds, long adminId) {
		
		GroupChats foundGroupChat = groupChatsDAO.findGroupChatsByGroupIdAndStaffId(chatGroupId, adminId);
		
		if (foundGroupChat != null) {
			if (foundGroupChat.getAdmins().contains(adminId)) {
				TreeSet<Long> newAdminIds = new TreeSet<>();
				newAdminIds.addAll(adminIds);
				adminIds.removeAll(foundGroupChat.getAdmins()); //remove the existing admins from the list
				for (Long each : adminIds) {
					if (foundGroupChat.getMembers().contains(each)) {

						Messages message = new Messages();

						message.setText(userRepository.findUsersByUserId(each).getFirstName()+" is now the group admin.");
						message.setGroupId(chatGroupId);
						message.setDate(LocalDate.now().toString());
						message.setSourceType("generated");
						message.setMessageId(sequence.getNextSequenceValue("messageId"));

						messagesRepository.save(message);
					}
				}

				foundGroupChat.setAdmins(foundGroupChat.getAdmins());
				foundGroupChat.setAdmins(newAdminIds); //update the admins
				TreeSet<Long> staffMembers = foundGroupChat.getMembers();
				staffMembers.removeAll(adminIds);
				foundGroupChat.setMembers(staffMembers);
				mongoTemplate.save(foundGroupChat, "group_chats");

				return foundGroupChat;
			}else
				return null;
		}else
			return null;
	}
}