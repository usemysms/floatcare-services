package com.java.floatcare.service;

import java.io.IOException;
import java.util.List;

import org.springframework.stereotype.Service;

import com.java.floatcare.api.response.pojo.SocialSecurityNumber;
import com.java.floatcare.api.response.pojo.UserLanguages;
import com.java.floatcare.api.response.pojo.UserNationalProviderIdentity;
import com.java.floatcare.model.EhrSystems;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.model.UserWorkspaces;

@Service
public interface UserBasicInformationService {

	UserBasicInformation createUserBasicInformation (String firstName, String lastName, String middleName,
			String email, String password, String defaultPassword, String preferredName, String address1, String address2, String city,
			String state, String country, String postalCode, String phone, String primaryContactCountryCode, String profilePhoto, String title,
			long userTypeId, long staffUserCoreTypeId, long staffUserSubCoreTypeId, String dateOfBirth, String linkedInProfile, long organizationId,
			String gender, String bioInformation, String createdOn, String secondaryPhoneNumber,
			String alternativeContactCountryCode, String secondaryEmail, String fcmToken, String deviceType, List <UserLanguages> userLanguages ,
			UserNationalProviderIdentity userNationalProviderIdentity, SocialSecurityNumber socialSecurityNumber, String publicKey,
			List<EhrSystems> ehrSystemId, String ethnicType, List<String> specialities,
			String invitedSource,long invitedFirmId,long invitedBy,String invitationStatus) throws Exception;

	public UserBasicInformation updateUserBasicInformation(long userId, String firstName, String lastName, String middleName, String status, String email,
			String password, String preferredName, String address1, String address2, String city, String state, String country, String postalCode,
			String phone, String primaryContactCountryCode, String profilePhoto, String title, long userTypeId,
			long staffUserCoreTypeId, long staffUserSubCoreTypeId, String dateOfBirth, String linkedInProfile, long organizationId, String gender,
			String bioInformation, String createdOn, String secondaryPhoneNumber, String alternativeContactCountryCode,
			String secondaryEmail, String fcmToken, String deviceType, List <UserLanguages> userLanguages , 
			UserNationalProviderIdentity userNationalProviderIdentity, SocialSecurityNumber socialSecurityNumber, String cvFile,
			String publicKey, List<EhrSystems> ehrSystemId, String ethnicType, List<String> specialities,
			String invitedSource,long invitedFirmId,long invitedBy,String invitationStatus) throws Exception;
	
	public boolean deleteUser(long userId, long organizationId);

	UserBasicInformation updateUserStatus(long userId, long authorityId, String status);
	
	public boolean updatePreferredProviders(List<Long> userIds, long organizationId, long businessId, long departmentId) throws IOException;
	
    public String insertIntoUserCredentials(String url, long staffUserCoreTypeId, long organizationId, long userId, String state) throws Exception;
    
    public List<UserBasicInformation> findAllStaffMembers(String name, String email, String phone, String firmType, long organizationId);
    
    public List<UserWorkspaces> findProvidersAdvancedSearch(String name, String startTime, String endTime,
			List<String> weekDays, double distance, String language, String gender, long staffUserCoreTypeId, long subRoleTypeId, List<String> specialityTags,
			String patientLongitude, String patientLatitude, String ethnicGroup, boolean isPreferred, String postalCode);
    //public List<UserBasicInformation> findAllStaffMembersByFirmType(String name, String email, String phone, String firmType);
}