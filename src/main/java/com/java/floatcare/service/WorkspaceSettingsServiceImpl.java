package com.java.floatcare.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java.floatcare.dao.CountersDAO;
import com.java.floatcare.dao.WorkspaceSettingsDAO;
import com.java.floatcare.model.WorkspaceSettings;
import com.java.floatcare.repository.WorkspaceSettingsRepository;

@Service
public class WorkspaceSettingsServiceImpl implements WorkspaceSettingsService {

	@Autowired
	private WorkspaceSettingsRepository workspaceSettingsRepository;
	@Autowired
	private CountersDAO sequence;
	@Autowired
	private WorkspaceSettingsDAO workspaceSettingsDAO;

	@Override
	public WorkspaceSettings createWorkspaceSettings(long userId, long eventNotificationId, long onCallShiftReminderId, long meetingReminderId,
			long assignedWorkShiftsReminderId, long educationReminderId, long eventsReminderId) throws Exception {

		List<WorkspaceSettings> workspaceOptional = workspaceSettingsDAO.findWorkspaceSettingsByUserId(userId);

		WorkspaceSettings workspaceSettings = new WorkspaceSettings();

		if (workspaceOptional.size() == 0) {

			workspaceSettings.setUserId(userId);
			workspaceSettings.setEventNotificationId(eventNotificationId);
			workspaceSettings.setOnCallShiftReminderId(onCallShiftReminderId);
			workspaceSettings.setMeetingReminderId(meetingReminderId);
			workspaceSettings.setAssignedWorkShiftsReminderId(assignedWorkShiftsReminderId);
			workspaceSettings.setEducationReminderId(educationReminderId);
			workspaceSettings.setEventsReminderId(eventsReminderId);
			
			workspaceSettings.setWorkspaceSettingId(sequence.getNextSequenceValue("workspaceSettingId"));
			
			workspaceSettingsRepository.save(workspaceSettings);

			return workspaceSettings;
		}

		else {

			WorkspaceSettings updateWorkspaceSettings = workspaceOptional.get(0);
			
			if (eventNotificationId > 0)
				updateWorkspaceSettings.setEventNotificationId(eventNotificationId);
			if (onCallShiftReminderId > 0)
				updateWorkspaceSettings.setOnCallShiftReminderId(onCallShiftReminderId);
			if (meetingReminderId > 0)
				updateWorkspaceSettings.setMeetingReminderId(meetingReminderId);
			if (assignedWorkShiftsReminderId > 0)
				updateWorkspaceSettings.setAssignedWorkShiftsReminderId(assignedWorkShiftsReminderId);
			if (educationReminderId > 0)
				updateWorkspaceSettings.setEducationReminderId(educationReminderId);
			if (eventsReminderId > 0)
				updateWorkspaceSettings.setEventsReminderId(eventsReminderId);
			
			workspaceSettingsRepository.save(updateWorkspaceSettings);

			return updateWorkspaceSettings;
		}
	}
}