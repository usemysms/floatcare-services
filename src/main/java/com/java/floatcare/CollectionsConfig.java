package com.java.floatcare;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.index.Index;

import com.coxautodev.graphql.tools.SchemaParserOptions;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.java.floatcare.model.Availability;
import com.java.floatcare.model.Businesses;
import com.java.floatcare.model.DepartmentTypes;
import com.java.floatcare.model.FlexOffs;
import com.java.floatcare.model.GeneralAvailability;
import com.java.floatcare.model.Messages;
import com.java.floatcare.model.OpenShiftInvitees;
import com.java.floatcare.model.OpenShifts;
import com.java.floatcare.model.Requests;
import com.java.floatcare.model.ScheduleLayout;
import com.java.floatcare.model.ScheduleLayoutGroups;
import com.java.floatcare.model.ScheduleLayoutQuota;
import com.java.floatcare.model.Schedules;
import com.java.floatcare.model.ShiftPlanningDrafts;
import com.java.floatcare.model.StaffUserCoreTypes;
import com.java.floatcare.model.SwapShiftRequests;
import com.java.floatcare.model.UserBasicInformation;
import com.java.floatcare.model.UserOrganizations;
import com.java.floatcare.model.UserPlan;
import com.java.floatcare.model.UserWorkspaces;
import com.java.floatcare.model.WorksiteSettings;
import com.java.floatcare.model.WorkspaceSettings;

@Configuration
@DependsOn("mongoTemplate")
public class CollectionsConfig {

	@Autowired
	private MongoTemplate mongoTemplate;

	@PostConstruct
	public void initIndexes() {
		
			mongoTemplate.indexOps(Messages.class) // collection name string or .class
					.ensureIndex(new Index().on("senderId", Sort.Direction.ASC));
			mongoTemplate.indexOps(Messages.class) // collection name string or .class
					.ensureIndex(new Index().on("receiverId", Sort.Direction.ASC));
			mongoTemplate.indexOps(Messages.class) // collection name string or .class
			.ensureIndex(new Index().on("text", Sort.Direction.ASC));

			mongoTemplate.indexOps(UserBasicInformation.class) // collection name string or .class
					.ensureIndex(new Index().on("phoneNumber", Sort.Direction.ASC));
			mongoTemplate.indexOps(UserBasicInformation.class) // collection name string or .class
					.ensureIndex(new Index().on("fcmToken", Sort.Direction.ASC));
			mongoTemplate.indexOps(UserBasicInformation.class) // collection name string or .class
					.ensureIndex(new Index().on("email", Sort.Direction.ASC));
			mongoTemplate.indexOps(UserBasicInformation.class) // collection name string or .class
					.ensureIndex(new Index().on("deviceType", Sort.Direction.ASC));
			mongoTemplate.indexOps(UserBasicInformation.class) // collection name string or .class
					.ensureIndex(new Index().on("organizationId", Sort.Direction.ASC));
			mongoTemplate.indexOps(UserBasicInformation.class) // collection name string or .class
					.ensureIndex(new Index().on("phone", Sort.Direction.ASC));
			mongoTemplate.indexOps(UserBasicInformation.class) // collection name string or .class
					.ensureIndex(new Index().on("staffUserCoreTypeId", Sort.Direction.ASC));
			mongoTemplate.indexOps(UserBasicInformation.class) // collection name string or .class
					.ensureIndex(new Index().on("firstName", Sort.Direction.ASC));
			mongoTemplate.indexOps(UserBasicInformation.class) // collection name string or .class
					.ensureIndex(new Index().on("lastName", Sort.Direction.ASC));
			
			
			mongoTemplate.indexOps(Availability.class) // collection name string or .class
					.ensureIndex(new Index().on("userId", Sort.Direction.ASC));
			mongoTemplate.indexOps(Availability.class) // collection name string or .class
					.ensureIndex(new Index().on("onDate", Sort.Direction.ASC));
			mongoTemplate.indexOps(Availability.class) // collection name string or .class
					.ensureIndex(new Index().on("scheduleLayoutId", Sort.Direction.ASC));
			
			
			mongoTemplate.indexOps(GeneralAvailability.class) // collection name string or .class
					.ensureIndex(new Index().on("userId", Sort.Direction.ASC));
			mongoTemplate.indexOps(GeneralAvailability.class) // collection name string or .class
					.ensureIndex(new Index().on("startDate", Sort.Direction.ASC));
			mongoTemplate.indexOps(GeneralAvailability.class) // collection name string or .class
					.ensureIndex(new Index().on("endDate", Sort.Direction.ASC));
			
			mongoTemplate.indexOps(Schedules.class) // collection name string or .class
					.ensureIndex(new Index().on("userId", Sort.Direction.ASC));
			mongoTemplate.indexOps(Schedules.class) // collection name string or .class
					.ensureIndex(new Index().on("shiftDate", Sort.Direction.ASC));
			mongoTemplate.indexOps(Schedules.class) // collection name string or .class
					.ensureIndex(new Index().on("status", Sort.Direction.ASC));
			mongoTemplate.indexOps(Schedules.class) // collection name string or .class
					.ensureIndex(new Index().on("event", Sort.Direction.ASC));
			mongoTemplate.indexOps(Schedules.class) // collection name string or .class
					.ensureIndex(new Index().on("departmentId", Sort.Direction.ASC));

			mongoTemplate.indexOps(UserWorkspaces.class) // collection name string or .class
					.ensureIndex(new Index().on("workspaceId", Sort.Direction.DESC));
			mongoTemplate.indexOps(UserWorkspaces.class) // collection name string or .class
					.ensureIndex(new Index().on("userId", Sort.Direction.DESC));
			mongoTemplate.indexOps(UserWorkspaces.class) // collection name string or .class
					.ensureIndex(new Index().on("departmentShiftIds", Sort.Direction.ASC));
			mongoTemplate.indexOps(UserWorkspaces.class) // collection name string or .class
					.ensureIndex(new Index().on("staffUserCoreTypeId", Sort.Direction.DESC));
			mongoTemplate.indexOps(UserWorkspaces.class) // collection name string or .class
					.ensureIndex(new Index().on("isJoined", Sort.Direction.DESC));

			mongoTemplate.indexOps(ScheduleLayout.class) // collection name string or .class
					.ensureIndex(new Index().on("scheduleStartDate", Sort.Direction.DESC));
			mongoTemplate.indexOps(ScheduleLayout.class) // collection name string or .class
					.ensureIndex(new Index().on("scheduleEndDate", Sort.Direction.DESC));
			mongoTemplate.indexOps(ScheduleLayout.class) // collection name string or .class
					.ensureIndex(new Index().on("staffUserCoreTypeIds", Sort.Direction.ASC));
			mongoTemplate.indexOps(ScheduleLayout.class) // collection name string or .class
					.ensureIndex(new Index().on("shiftTypes", Sort.Direction.DESC));
			mongoTemplate.indexOps(ScheduleLayout.class) // collection name string or .class
					.ensureIndex(new Index().on("departmentId", Sort.Direction.DESC));

			mongoTemplate.indexOps(ScheduleLayoutGroups.class) // collection name string or .class
					.ensureIndex(new Index().on("userIds", Sort.Direction.ASC));
			mongoTemplate.indexOps(ScheduleLayoutGroups.class) // collection name string or .class
					.ensureIndex(new Index().on("scheduleLayoutId", Sort.Direction.ASC));
			mongoTemplate.indexOps(ScheduleLayout.class) // collection name string or .class
					.ensureIndex(new Index().on("departmentId", Sort.Direction.ASC));

			mongoTemplate.indexOps(ScheduleLayoutQuota.class) // collection name string or .class
					.ensureIndex(new Index().on("scheduleLayoutId", Sort.Direction.ASC));
			mongoTemplate.indexOps(ScheduleLayout.class) // collection name string or .class
					.ensureIndex(new Index().on("departmentId", Sort.Direction.ASC));

			mongoTemplate.indexOps(OpenShifts.class) // collection name string or .class
					.ensureIndex(new Index().on("onDate", Sort.Direction.ASC));
			mongoTemplate.indexOps(ScheduleLayout.class) // collection name string or .class
					.ensureIndex(new Index().on("departmentId", Sort.Direction.ASC));
			mongoTemplate.indexOps(OpenShifts.class) // collection name string or .class
					.ensureIndex(new Index().on("worksiteId", Sort.Direction.ASC));
			mongoTemplate.indexOps(ScheduleLayout.class) // collection name string or .class
					.ensureIndex(new Index().on("invitees", Sort.Direction.ASC));

			mongoTemplate.indexOps(OpenShiftInvitees.class) // collection name string or .class
					.ensureIndex(new Index().on("openShiftId", Sort.Direction.ASC));
			mongoTemplate.indexOps(OpenShiftInvitees.class) // collection name string or .class
					.ensureIndex(new Index().on("userId", Sort.Direction.ASC));
			mongoTemplate.indexOps(OpenShiftInvitees.class) // collection name string or .class
					.ensureIndex(new Index().on("isAccepted", Sort.Direction.ASC));
			mongoTemplate.indexOps(OpenShiftInvitees.class) // collection name string or .class
					.ensureIndex(new Index().on("isApproved", Sort.Direction.ASC));
		
			mongoTemplate.indexOps(Requests.class) // collection name string or .class
					.ensureIndex(new Index().on("departmentId", Sort.Direction.ASC));
			mongoTemplate.indexOps(Requests.class) // collection name string or .class
					.ensureIndex(new Index().on("userId", Sort.Direction.ASC));
			mongoTemplate.indexOps(Requests.class) // collection name string or .class
					.ensureIndex(new Index().on("status", Sort.Direction.ASC));
			mongoTemplate.indexOps(Requests.class) // collection name string or .class
					.ensureIndex(new Index().on("onDate", Sort.Direction.ASC));
			mongoTemplate.indexOps(Requests.class) // collection name string or .class
					.ensureIndex(new Index().on("shiftId", Sort.Direction.ASC));

			mongoTemplate.indexOps(StaffUserCoreTypes.class) // staffusercoretype class
					.ensureIndex(new Index().on("staffUserCoreTypeId", Sort.Direction.ASC));
			mongoTemplate.indexOps(StaffUserCoreTypes.class) // collection name string or .class
					.ensureIndex(new Index().on("label", Sort.Direction.ASC));
			
			mongoTemplate.indexOps(ShiftPlanningDrafts.class)
					.ensureIndex(new Index().on("scheduleLayoutId", Sort.Direction.ASC));
			
			mongoTemplate.indexOps(UserOrganizations.class)
					.ensureIndex(new Index().on("organizationId", Sort.Direction.ASC));	
			mongoTemplate.indexOps(UserOrganizations.class)
					.ensureIndex(new Index().on("userId", Sort.Direction.ASC));
			mongoTemplate.indexOps(UserOrganizations.class)
					.ensureIndex(new Index().on("status", Sort.Direction.ASC));
			mongoTemplate.indexOps(UserOrganizations.class)
					.ensureIndex(new Index().on("staffUserCoreTypeId", Sort.Direction.ASC));
			
			mongoTemplate.indexOps(WorksiteSettings.class)
					.ensureIndex(new Index().on("organizationId", Sort.Direction.ASC));	
			mongoTemplate.indexOps(WorksiteSettings.class)
					.ensureIndex(new Index().on("userId", Sort.Direction.ASC));
			mongoTemplate.indexOps(WorksiteSettings.class)
					.ensureIndex(new Index().on("worksiteId", Sort.Direction.ASC));
			
			mongoTemplate.indexOps(DepartmentTypes.class)
					.ensureIndex(new Index().on("label", Sort.Direction.ASC));
			
			mongoTemplate.indexOps(WorkspaceSettings.class)
					.ensureIndex(new Index().on("userId", Sort.Direction.ASC));
			mongoTemplate.indexOps(WorkspaceSettings.class)
					.ensureIndex(new Index().on("eventNotificationId", Sort.Direction.ASC));
			mongoTemplate.indexOps(WorkspaceSettings.class)
					.ensureIndex(new Index().on("onCallShiftReminderId", Sort.Direction.ASC));	
			mongoTemplate.indexOps(WorkspaceSettings.class)
					.ensureIndex(new Index().on("meetingReminderId", Sort.Direction.ASC));
			mongoTemplate.indexOps(WorkspaceSettings.class)
					.ensureIndex(new Index().on("assignedWorkShiftsReminderId", Sort.Direction.ASC));
			mongoTemplate.indexOps(WorkspaceSettings.class)
					.ensureIndex(new Index().on("eventsReminderId", Sort.Direction.ASC));
			mongoTemplate.indexOps(WorkspaceSettings.class)
					.ensureIndex(new Index().on("educationReminderId", Sort.Direction.ASC));
			
			mongoTemplate.indexOps(SwapShiftRequests.class)
				.ensureIndex(new Index().on("senderId", Sort.Direction.ASC));
			mongoTemplate.indexOps(SwapShiftRequests.class)
				.ensureIndex(new Index().on("receiverId", Sort.Direction.ASC));
			mongoTemplate.indexOps(SwapShiftRequests.class)
				.ensureIndex(new Index().on("sourceShiftId", Sort.Direction.ASC));	
			mongoTemplate.indexOps(WorkspaceSettings.class)
				.ensureIndex(new Index().on("requestedShiftId", Sort.Direction.ASC));
			mongoTemplate.indexOps(WorkspaceSettings.class)
				.ensureIndex(new Index().on("isRecieverAccepted", Sort.Direction.ASC));
			mongoTemplate.indexOps(WorkspaceSettings.class)
				.ensureIndex(new Index().on("isSupervisorAccepted", Sort.Direction.ASC));
			
			mongoTemplate.indexOps(FlexOffs.class)
				.ensureIndex(new Index().on("worksiteId", Sort.Direction.ASC));
			mongoTemplate.indexOps(FlexOffs.class)
				.ensureIndex(new Index().on("departmentId", Sort.Direction.ASC));
			mongoTemplate.indexOps(FlexOffs.class)
				.ensureIndex(new Index().on("worksiteId", Sort.Direction.ASC));
			mongoTemplate.indexOps(FlexOffs.class)
				.ensureIndex(new Index().on("shiftTypeId", Sort.Direction.ASC));
			mongoTemplate.indexOps(FlexOffs.class)
				.ensureIndex(new Index().on("onDate", Sort.Direction.ASC));
			mongoTemplate.indexOps(FlexOffs.class)
				.ensureIndex(new Index().on("status", Sort.Direction.ASC));
			mongoTemplate.indexOps(FlexOffs.class)
				.ensureIndex(new Index().on("staffUserCoreTypeId", Sort.Direction.ASC));
			
			mongoTemplate.indexOps(Businesses.class)
				.ensureIndex(new Index().on("postalCode", Sort.Direction.ASC));
			mongoTemplate.indexOps(Businesses.class)
				.ensureIndex(new Index().on("city", Sort.Direction.ASC));
			mongoTemplate.indexOps(Businesses.class)
				.ensureIndex(new Index().on("state", Sort.Direction.ASC));
			mongoTemplate.indexOps(Businesses.class)
				.ensureIndex(new Index().on("name", Sort.Direction.ASC));
			
			mongoTemplate.indexOps(UserPlan.class)
			.ensureIndex(new Index().on("planId", Sort.Direction.ASC));
			mongoTemplate.indexOps(UserPlan.class)
			.ensureIndex(new Index().on("userId", Sort.Direction.ASC));
	}
	
	@Bean
	public SchemaParserOptions schemaParserOptions(){
	   return SchemaParserOptions.newOptions().objectMapperConfigurer((mapper, context) -> {
	        mapper.registerModule(new JavaTimeModule());
	    }).build();
	}
}