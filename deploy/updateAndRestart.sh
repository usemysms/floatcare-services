#!/bin/bash

# any future command that fails will exit the script
set -e

# Delete the old repo
#rm -rf /home/ec2-user/cicddemo/floatcare-services
pwd
ls
cd /home/ec2-user/cicddemo/floatcare-services
# clone the repo again
git pull origin master



if test -f "/home/ec2-user/cicddemo/save_pid.txt"; then
	echo "/home/ec2-user/cicddemo/save_pid.txt exists."
	kill -9 `cat /home/ec2-user/cicddemo/save_pid.txt`
	rm /home/ec2-user/cicddemo/save_pid.txt
fi


nohup gradle bootRun &

echo $! > /home/ec2-user/cicddemo/save_pid.txt
